<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();

$mr_work_main_id = $req->get('mr_work_main_id');
$remark = $req->get('remark');
$remark_can = $req->get('remark_can');

if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_main_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $remark)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $remark_can)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}


$data['mr_work_remark'] = $remark." - ".$remark_can;
$data['mr_status_id'] = 6;

$resp = $work_main_Dao->save($data,$mr_work_main_id);
$save_log['mr_user_id'] 									= $auth->getUser();
$save_log['mr_status_id'] 									= 6;
$save_log['mr_work_main_id'] 								= $mr_work_main_id;
$save_log['remark'] 										= "Mailroom Tbank ยกเลิกการจัดส่ง";
$work_log_Dao->save($save_log);

if(!empty($resp)){
    echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ'));
}else{
    echo json_encode(array('status' => 500, 'message' => 'บันทึกไม่สำเร็จ'));
}

?>
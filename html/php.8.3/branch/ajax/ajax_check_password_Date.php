<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$userDao 			= new Dao_User();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$usrID = $req->get('userID');

if(preg_match('/<\/?[^>]+(>|$)/', $usrID)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$getDiff = $userDao->getDiffDatePassword(intval($usrID));

echo json_encode($getDiff);
?>
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';

$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();
$auth 			= new Pivot_Auth();
$branchDao		= new Dao_Branch();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$txt = $_GET['q'];

if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$branch_data = $branchDao->getBranchTXT($txt);

$json = array();
foreach ($branch_data as $key => $value) {
    $fullname = $value['mr_branch_code']." - " .$value['mr_branch_name'];
    //$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
	$json[] = array(
		"id" => $value['mr_branch_id'],
		"text" => $fullname
	);
}

echo json_encode($json);
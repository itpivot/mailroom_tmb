<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Department.php';

$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$departmentDao	= new Dao_Department();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$txt = $_GET['q'];

if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$department_data = $departmentDao->getDepartmentNotBranchTXT($txt);


$json = array();
foreach ( $department_data as $key => $value) {
        $fullname = $value['mr_department_code']." - " .$value['mr_department_name'];
        //$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
		$json[] = array(
			"id" => $value['mr_department_id'],
			"text" => $fullname
		);
}


echo json_encode($json);
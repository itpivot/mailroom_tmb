<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'nocsrf.php';


$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$employeeDao 	= new Dao_Employee();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.' ,'token'=>NoCSRF::generate( 'csrf_token' )));
    exit();
}
//echo $txt; 
    try
    {
        // Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
        NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
        // form parsing, DB inserts, etc.
        // ...
        $result = 'CSRF check passed. Form parsed.';
    }
    catch ( Exception $e )
    {
        // CSRF attack detected
        $result = $e->getMessage() . ' Form ignored.';
		 echo json_encode(array('status' => 500, 'message' => $result ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
    }


$txt = $req->get('q');

if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
    exit();
}

$emp_data = $employeeDao->getEmpDataWithTXTBranch($txt);


$json = array();
if(!empty($emp_data)){
	foreach ($emp_data as $key => $value) {
			$fullname = $value['mr_emp_code']." : " .$value['mr_emp_name']." ".$value['mr_emp_lastname'];
			//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
			$json[] = array(
				"id" => $value['mr_emp_id'],
				"text" => $fullname
			);
	}
}else{
	$json[] = array(
				"id" => '',
				"text" => 'ไม่พบข้อมูล'
			);
}

$result = array();
$result['status'] 	= '200';
$result['message'] 	= '';
$result['data'] 	= $json;
$result['token'] 	= NoCSRF::generate( 'csrf_token' );
echo json_encode($result);

<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'Dao/Work_post.php';
require_once 'Dao/Type_post.php';
require_once 'Dao/Department.php';
require_once 'Dao/Send_work.php';
require_once 'nocsrf.php';
//require_once 'PHPExcel.php';
ini_set('memory_limit', '-1');
set_time_limit(-1);


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา Log In' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
} else {	
try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*120, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
   exit();
}


$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();
$type_postDao 		= new Dao_Type_post();
$work_postDao 		= new Dao_Work_post();
$departmentDao 		= new Dao_Department();
$send_workDao 		= new Dao_Send_work();
$user_id			= $auth->getUser();

$page = $req->get('page');


if($page == "Uploadfile"){
		
	$name 			= $_FILES['file']['name'];
	$tmp_name 		= $_FILES['file']['tmp_name'];
	$size 			= $_FILES['file']['size'];
	$err 			= $_FILES['file']['error'];
	$file_type 		= $_FILES['file']['type'];
	$import_round = $req->get('import_round');
	$date_import =  $req->get('date_import');
	//ตรวจสอบ รอบ
	if($import_round == ''){
		echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ รอบ  </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	//ตรวจสอบ วันที่
	if($date_import == ''){
		echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ วันที่นำส่ง  </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}

	$file_name = explode( ".", $name );
	$extension = $file_name[count($file_name) - 1]; // xlsx|xls
	try {
		$inputFileType = PHPExcel_IOFactory::identify($tmp_name);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($tmp_name);

		$sql = "SELECT * FROM `mr_emp` WHERE mr_emp_code NOT LIKE '%Resign%' ORDER BY `mr_emp`.`mr_emp_id` ASC";
		$type_post 		= $type_postDao->fetchAll();
		$department 	= $departmentDao->fetchAll();
		$params 		= array();
		$emp 			= $work_mainDao->select_work_main($sql,$params);

		$department_arr  = array();
		foreach($department as $i => $val_i){
			$department_arr[$val_i['mr_department_code']]['id']		 	=  $val_i['mr_department_id']; 
			$department_arr[$val_i['mr_department_code']]['code'] 		= $val_i['mr_department_code'];
			$department_arr[$val_i['mr_department_code']]['name'] 		= $val_i['mr_department_name'];
			$department_arr[$val_i['mr_department_code']]['detail'] 	= $val_i['mr_department_code']." - ".$val_i['mr_department_name'];

		}
		$emp_arr  = array();
		foreach($emp as $j => $val_j){
			$emp_arr[$val_j['mr_emp_code']]['id'] =  $val_j['mr_emp_id']; 
			$emp_arr[$val_j['mr_emp_code']]['code'] =  $val_j['mr_emp_code']; 
			$emp_arr[$val_j['mr_emp_code']]['name'] =  $val_j['mr_emp_name']; 
			$emp_arr[$val_j['mr_emp_code']]['lname'] =  $val_j['mr_emp_lastname']; 
			$emp_arr[$val_j['mr_emp_code']]['detail'] =  $val_j['mr_emp_code']." - ".$val_j['mr_emp_name']."  ".$val_j['mr_emp_lastname']; 
		}
		// echo json_encode($emp_arr);
		// exit;
		$type_post_master  = array();
		foreach($type_post as $key=>$val){
			$type_post_master[$val['mr_type_post_name']] = $val['mr_type_post_id'];
		}
		

	} catch(Exception $e) {
		echo json_encode(array('status' => 505, 'message' => 'Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage() ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = $sheet->getHighestColumn();
	$ch_num = 0;
	//  Loop through each row of the worksheet in turn
	$store_barcode = array();
	$store_barcode_re = array();
	$data_dave = array();


	for ($row = 1; $row <= $highestRow; $row++){ //  Read a row of data into an array
		$no 				= $sheet->getCell('A'.$row)->getValue();
		$dep_code			= $sheet->getCell('B'.$row)->getValue();
		$emp_code 			= $sheet->getCell('C'.$row)->getValue();
		$emp_name 			= $sheet->getCell('D'.$row)->getValue();
		$resiver	 		= $sheet->getCell('E'.$row)->getValue();
		$post_type			= $sheet->getCell('F'.$row)->getValue();
		$barcode			= $sheet->getCell('G'.$row)->getValue();
		$address			= $sheet->getCell('H'.$row)->getValue();
		$remark 			= $sheet->getCell('I'.$row)->getValue();
		$weight				= $sheet->getCell('J'.$row)->getValue();
		$qty				= $sheet->getCell('K'.$row)->getValue();
		$peice				= $sheet->getCell('L'.$row)->getValue();
		$tal_peice			= $sheet->getCell('M'.$row)->getValue();
		$barcode_re			= $sheet->getCell('N'.$row)->getValue();
		

		if($row == 1){
			$arr_msg = '';
			$excollums = array('A','B','C','D','E','F','G','H','I','J','K','L','M','M','N');
			$collumsname = array('ลำดับ',	'รหัสหน่วยงาน',			'รหัสพนักงาน',	'ชื่อผู้ส่ง',	'ชื่อผู้รับ ',	'ประเภท',		'เลขที่ลงทะเบียน',	'ที่อยู่',	        'หมายเหตุ',	'น้ำหนัก',   'จำนวนซอง',	 'ราคา',		 'ราคารวม',	   'หมายเลขตอบรับ');
			$collumsval  = array($no,		$dep_code,			  $emp_code,    $emp_name,	$resiver,	$post_type,	   $barcode,         $address,		$remark,    $weight,	$qty,	       $peice,			$tal_peice,	  $barcode_re);
			foreach($collumsname as $key => $c_val){
				if($collumsval[$key] != $c_val){
					$arr_msg .= '<div class="alert alert-danger" role="alert"> ชื่อ collum '.$excollums[$key]." ไม่ตรงกับระบบ >>".$collumsval[$key]." != ".$c_val."</div>";
				}							
			}
			if(!empty($arr_msg)){
				echo json_encode(array('status' => 505, 'message' => $arr_msg ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}
			
		}else{
			if($no == ''){
				break;  
			}
			//เริ่ม import ข้อมูล
			//ตรวจสอบ ราคารวม
			if($tal_peice == ''){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ราคารวม : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				if(is_numeric($tal_peice)){
						if($tal_peice < 1){
							echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ราคารวมมากกว่าเท่ากับ 1 : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
							exit();
						}
				}else{
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ราคารวมเป็นตัวเลข : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}
			}
			//ตรวจสอบ ราคา
			if($peice == ''){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ราคา : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				if(is_numeric($peice)){
						if($peice < 1){
							echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ราคามากกว่าเท่ากับ 1 : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
							exit();
						}
				}else{
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ราคาเป็นตัวเลข : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}
			}

			//ตรวจสอบ จำนวน
			if($qty == ''){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ จำนวน : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				if(is_numeric($qty)){
						if($qty < 1){
							echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ จำนวนมากกว่าเท่ากับ 1 : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
							exit();
						}
				}else{
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ จำนวนเป็นตัวเลข : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}
			}
			//ตรวจสอบ น้ำหนัก
			if($weight == ''){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ น้ำหนัก : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				if(is_numeric($weight)){
						if($weight<0.001){
							echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ น้ำหนักมากกว่าเท่ากับ 0.001 : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
							exit();
						}
				}else{
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ น้ำหนักเป็นตัวเลข : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}
			}
			//ตรวจสอบ ที่อยู่ผู้รับ
			if($address == ''){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ที่อยู่ผู้รับ : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}
			//ตรวจสอบ ผู้รับ
			if($resiver == ''){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ผู้รับ : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}
			//ตรวจสอบ ประเถทการส้ง
			if($post_type == ''){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ประเถทการส่ง : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}


			//ตรวจสอบ รหัสหนักงาน
			if(!isset($emp_arr[$emp_code])){
				$mr_send_emp_id = null;
				// echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">ไม่พบข้อมูลรหัสหนักงาน หรื่อไม่ตรงกับระบบ: '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				// exit();
			}else{
				$mr_send_emp_id 	= $emp_arr[$emp_code]['id'];
				$mr_send_emp_detail = $emp_arr[$emp_code]['detail'];
			}
			//ตรวจสอบ หน่วยงาน
			if(!isset($department_arr[$dep_code])){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">ไม่พบข้อมูลรหัสหน่วยงาน หรื่อไม่ตรงกับระบบ: '.$barcode.'  ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				$mr_send_department_id 		= $department_arr[$dep_code]['id'];
				$mr_send_department_detail 	= $department_arr[$dep_code]['detail'];
			}
			//ตรวจสอบ ประเภทการส่ง
			if(!isset($type_post_master[$post_type])){
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">ไม่พบข้อมูลประเภทการส่ง หรื่อชื่อไม่ตรงกับระบบ: '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				$mr_type_post_id = $type_post_master[$post_type];
			}
			//  ตรวจสอบ barcode
			if($barcode != ''){
				//$last_barcode 						= $work_mainDao->checkBarcode($barcode);
				$last_barcode 						= array();
				if(isset($store_barcode[$barcode])){
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">เกิดข้อผิดพลาด Barcode: '.$barcode.' ซ้ำ ลำดับที่ '.$no.' และ '.$store_barcode[$barcode].' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}else{
					$store_barcode[$barcode] = $no; 
				}
				if(!empty($last_barcode)){
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">เกิดข้อผิดพลาด Barcode: '.$barcode.' ซ้ำ ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}
				$barcodeok	 		= trim($barcode);
			}else{
				if($mr_type_post_id == 1){
					$barcodeok = null;
				}else{
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ เลขที่เอกสาร ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}	
			}
			$type_re = array(3,5);
			if(in_array($mr_type_post_id,$type_re)){
				if($barcode_re == ''){
					echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ เลขที่เอกสาร ตอบรับ ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
					exit();
				}
			}
		
			$data_dave[$row]['mr_work_date_sent'] 			= $date_import; //วันนำส่ง
			$data_dave[$row]['mr_work_barcode']				= $barcodeok;
			$data_dave[$row]['mr_work_remark']				= $remark;
			$data_dave[$row]['mr_type_work_id']				= 6;
			$data_dave[$row]['mr_status_id']				= 3;
			$data_dave[$row]['mr_user_id']					= $user_id;
			$data_dave[$row]['mr_round_id']					= $req->get('import_round');
			$data_dave[$row]['quty']						= $qty;
			$data_dave[$row]['mr_send_emp_id']				= $mr_send_emp_id;
			$data_dave[$row]['sp_num_doc']					= $barcode_re;
			$data_dave[$row]['mr_send_department_id']		= $mr_send_department_id;
			$data_dave[$row]['mr_send_emp_detail']			= $mr_send_emp_detail;
			$data_dave[$row]['mr_cus_name']					= $resiver;
			$data_dave[$row]['mr_address']					= $address;
			$data_dave[$row]['mr_type_post_id']				= $mr_type_post_id;
			$data_dave[$row]['mr_post_weight']				= $weight;
			$data_dave[$row]['mr_post_price']				= $peice;
			$data_dave[$row]['mr_post_totalprice']			= $tal_peice;
			$data_dave[$row]['mr_post_amount']				= $qty;

		}
		//exit;
		
		
	}
	// echo json_encode($data_dave);
	// exit();
	// exit;
	try{
		foreach($data_dave as $key_save => $val_save){
			//date_import
			//import_round
			$save_main['mr_work_date_sent']				=$date_import; //วันนำส่ง
			$save_main['mr_work_barcode']				= $val_save['mr_work_barcode'];
			$save_main['mr_work_remark']				= $val_save['mr_work_remark'];
			$save_main['mr_type_work_id']				= 6;
			$save_main['mr_status_id']					= 1;
			$save_main['mr_user_id']					= $val_save['mr_user_id'];
			$save_main['mr_round_id']					= $val_save['mr_round_id'];
			$save_main['quty']							= $val_save['quty'];
			$mr_work_main_id 							= $work_mainDao->save($save_main);


			$save_post['mr_work_main_id']				= $mr_work_main_id;
			$save_post['sys_timestamp']					= date("Y-m-d H:i:s");
			if($val_save['mr_send_emp_id'] != ''){
				$save_post['mr_send_emp_id']			= $val_save['mr_send_emp_id'];
			}
			$save_post['sp_num_doc']			        = $val_save['sp_num_doc'];
			$save_post['mr_send_department_id']			= $val_save['mr_send_department_id'];
			$save_post['mr_send_emp_detail']			= $val_save['mr_send_emp_detail'];
			$save_post['mr_cus_name']					= $val_save['mr_cus_name'];
			$save_post['mr_cus_lname']					= '';
			$save_post['mr_address']					= $val_save['mr_address'];
			$save_post['mr_type_post_id']				= $val_save['mr_type_post_id'];
			$save_post['mr_post_weight']				= $val_save['mr_post_weight'];
			$save_post['mr_post_price']					= $val_save['mr_post_price'];
			$save_post['mr_post_totalprice']			= $val_save['mr_post_totalprice'];
			$save_post['mr_post_amount']				= $val_save['mr_post_amount'];
			$work_post_id								= $work_postDao->save($save_post);
		
			//exit();	
			$save_log['mr_user_id'] 									= $user_id;
			$save_log['mr_status_id'] 									= 1;
			$save_log['mr_work_main_id'] 								= $mr_work_main_id;
			$save_log['remark'] 										= "สร้างราายการนำส่ง ปณ. ส่งออก import Excel";
			$log_id = $work_logDao->save($save_log);


		}
		echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
			

	}catch ( Exception $e){
		if(!empty($mr_work_main_id)){
			$work_mainDao->remove($mr_work_main_id);
		}
		if(!empty($work_post_id)){
			$work_postDao->remove($work_post_id);
		}
		if(!empty($log_id)){
			$work_logDao->remove($log_id);
		}
		echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}	

}else{
	$time_start 		= microtime(true);
	//$barcode 			= $req->get('post_barcode');
	
		try{
	
			//$save_main['mr_work_date_sent']				= $req->get('date_send');
			//$save_main['mr_work_barcode']				= $barcodeok;
			$save_main['mr_work_remark']				= $req->get('work_remark');
			$save_main['mr_type_work_id']				= 6;
			$save_main['mr_status_id']					= 1;
			$save_main['mr_user_id']					= $user_id;
			$save_main['mr_branch_id']					= $req->get('mr_branch_id');
			$save_main['mr_floor_id']					= $req->get('floor_id_send');
			//$save_main['mr_round_id']					= $req->get('round');
			$save_main['mr_topic']						= $req->get('topic');
			$save_main['quty']							= $req->get('quty');
			$mr_work_main_id 							= $work_mainDao->save($save_main);

			if(strlen($mr_work_main_id)<=1){
				$Nbarcode['mr_work_barcode'] = "TPO".date("dmy")."000000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=2){
				$Nbarcode['mr_work_barcode'] = "TPO".date("dmy")."00000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=3){
				$Nbarcode['mr_work_barcode'] = "TPO".date("dmy")."0000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=4){
				$Nbarcode['mr_work_barcode'] = "TPO".date("dmy")."000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=5){
				$Nbarcode['mr_work_barcode'] = "TPO".date("dmy")."00".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=6){
				$Nbarcode['mr_work_barcode'] = "TPO".date("dmy")."0".$workmr_work_main_id_main_id;
			}else if(strlen($mr_work_main_id)<=7){
				$Nbarcode['mr_work_barcode'] = "TPO".date("dmy").$mr_work_main_id;
			}else{
					$num_run 			= substr($mr_work_main_id, -7); 
					$Nbarcode['mr_work_barcode'] = "TPO".date("dmy").$num_run;
			} 
			
			$barcodeok = $Nbarcode['mr_work_barcode'];
			$work_mainDao->save($Nbarcode,$mr_work_main_id);

	
			$save_post['mr_work_main_id']				= $mr_work_main_id;
			if($req->get('emp_id_send') != ''){
				$save_post['mr_send_emp_id']				= $req->get('emp_id_send');	
			}
	
			$save_post['sp_num_doc']			        = $req->get('post_barcode_re');
			$save_post['mr_send_department_id']			= $req->get('dep_id_send');
			$save_post['mr_send_emp_detail']			= $req->get('emp_send_data');
			$save_post['mr_cus_name']					= $req->get('name_re'); 
			$save_post['mr_cus_lname']					= $req->get('lname_re'); 
			$save_post['mr_address']					= $req->get('address_re'); 
			$save_post['mr_cus_tel']					= $req->get('tel_re'); 
			$save_post['mr_post_code']					= $req->get('post_code_re'); 
			$save_post['mr_type_post_id']				= $req->get('mr_type_post_id'); 
			$save_post['mr_post_weight']				= $req->get('weight'); 
			$save_post['mr_post_price']					= $req->get('price'); 
			$save_post['mr_post_totalprice']			= $req->get('total_price'); 
			$save_post['mr_post_amount']				= $req->get('quty'); 
			$sub_districts_code_re						= $req->get('sub_districts_code_re');
			$districts_code_re							= $req->get('districts_code_re');
			$provinces_code_re							= $req->get('provinces_code_re');
				
				if(!empty($sub_districts_code_re)) {
					$locate = $sub_districtDao->getLocation($sub_districts_code_re, $districts_code_re, $provinces_code_re);
					//echo json_encode($locate);
					//exit();	
					$save_post['mr_sub_district_id'] 					= (!empty($locate['mr_sub_districts_id']) ? (int)$locate['mr_sub_districts_id'] : NULL);;
					$save_post['mr_district_id'] 						= (!empty($locate['mr_districts_id']) ? (int)$locate['mr_districts_id'] : NULL);
					$save_post['mr_province_id'] 						= (!empty($locate['mr_provinces_id']) ? (int)$locate['mr_provinces_id'] : NULL);;
				}
				
				//echo json_encode($save_post);
			$work_post_id								= $work_postDao->save($save_post);
			
			//exit();	
	
			$save_log['mr_user_id'] 									= $user_id;
			$save_log['mr_status_id'] 									= 1;
			$save_log['mr_work_main_id'] 								= $mr_work_main_id;
			$save_log['remark'] 										= "สร้างราายการนำส่ง ปณ. ส่งออก";
			$log_id = $work_logDao->save($save_log);
			$time_end = microtime(true);
			$time =		$time_end - $time_start;

			echo json_encode(
				array('status' => 200, 
				'message' => 'บันทึกสำเร็จ' ,
				'barcodeok' => $barcodeok ,
				'microtime' => $time ,
				'token'=>NoCSRF::generate( 'csrf_token' )
				)
			);
			exit();
		}catch ( Exception $e){
			if(!empty($mr_work_main_id)){
				$work_mainDao->remove($mr_work_main_id);
			}
			if(!empty($work_post_id)){
				$work_postDao->remove($work_post_id);
			}
			if(!empty($log_id)){
				$work_logDao->remove($log_id);
			}
			echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล'.$e->getMessage() ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}	
	}
}

function checkBarcode($barcode){
	$send_workDao 		= new Dao_Send_work();
	$work_mainDao 		= new Dao_Work_main();

	$filename 	= '../../download/barcode.json';
	$startdate 	= Date("Y-m-d H:i:s", strtotime(date("Y-m-d")." -90 Day"));
	$end_time 	= Date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")." +10 minute "));
	$now 		= date("Y-m-d H:i:s");
	$txt='';
	$sql_select = "
	SELECT 
		mr_work_barcode
	FROM mr_work_main 
	WHERE sys_timestamp >= ? 
		AND mr_type_work_id = 6
		AND mr_status_id != 6
		AND  mr_work_barcode is not null
		order by mr_work_main_id desc	
		limit 0,30000				
	";
	//unset($_SESSION['reciever_branch']);
	//unset($_SESSION['reciever_branch']);
	$arrsection = array();
	if(!file_exists($filename)){
		$posts 		= array();
		$fp 		= fopen($filename, 'w');
		fwrite($fp, json_encode($posts, JSON_PRETTY_PRINT));   // here it will print the array pretty
		fclose($fp);
	}
	$string = file_get_contents($filename);
	$json_data = array();
	$json_data = json_decode($string, true);
	$timeout = (isset($json_data["barcode_time"]))?$json_data["barcode_time"]:null;

	//$fp = fopen('results.json', 'w');
	if($timeout < $now or empty($json_data)){
		////echo "ไม่มี";
		$params = array();
		array_push($params,$startdate);
		$data = $send_workDao->select($sql_select,$params);
		foreach($data as $key=>$val){
			$arrsection["barcode"][$val['mr_work_barcode']] = $val['mr_work_barcode'];
			$arrsection["barcode_time"] = $end_time;
		}
		$fp = fopen($filename, 'w');
		fwrite($fp, json_encode($arrsection, JSON_PRETTY_PRINT));   // here it will print the array pretty
		fclose($fp);
	}else{
		$arrsection = $json_data; 
	}


	if(isset($arrsection['barcode'][$barcode])){
		// return $arrsection['barcode'][$barcode];
		$last_barcode 						= $work_mainDao->checkBarcode($barcode);
		return $last_barcode;
	}else{
		$arrsection['barcode'][$barcode] = $barcode;
		$fp = fopen($filename, 'w');
		fwrite($fp, json_encode($arrsection, JSON_PRETTY_PRINT));   // here it will print the array pretty
		fclose($fp);
		return array();
	}

}

 ?>
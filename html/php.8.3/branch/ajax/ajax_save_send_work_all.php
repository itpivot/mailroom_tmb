<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';

$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();

if (!$auth->isAuth()) {
	$re['st'] = 'error';
	$re['re'] = 'Access denied.';
	echo json_encode($re);
	exit;
}

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();
$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$code_branch_sender = $user_data['mr_branch_id'];
$date = date('dmY');

$barcode = 'B'.$code_branch_sender.$date.'0001';

$params_check = array();
array_push($params_check, (string)$barcode."%");

$sql_check="SELECT count(mr_send_work_id)+1  as count FROM mr_send_work WHERE barcode like ?";	

	$count_barcode 		= $send_workDao->select_send_work($sql_check,$params_check);
	$num_run 			= $count_barcode[0]['count'];


	if( strlen($num_run) == 1 ){
		$num = "00".$num_run;
	}else if( strlen($num_run) == 2 ){
		$num = "0".$num_run;		
	}else{
		$num = $num_run;
	}
$barcode_ok	 		= $barcode.$num;	

$all_id 		= $req->get('all_id');
$tel 			= $req->get('tel');

if(preg_match('/<\/?[^>]+(>|$)/', $all_id)) {
    $re['st'] = 'error';
	$re['re'] = 'รูปแบบข้อมูลไม่ถูกต้อง';
	echo json_encode($re);
	exit;
}
if(preg_match('/<\/?[^>]+(>|$)/', $tel)) {
    $re['st'] = 'error';
	$re['re'] = 'รูปแบบข้อมูลไม่ถูกต้อง';
	echo json_encode($re);
	exit;
}



$savedata['mr_user_id']	=	$user_id;
$savedata['barcode']	=	$barcode_ok;
$savedata['sender_tel']	=	$tel;
$mr_send_work_id = null;

function decode_work_main_id($id)
{
	return base64_decode(urldecode($id));
}
$params 			= array();
$in_all_id_params 	= array(); // [1, 2, 3]
$in_all_id_params 	= explode(',',$all_id);
$params_all_id 		= array(); // condition: generate ?,?,?
$params_all_id 		= str_repeat('?,', count($in_all_id_params) - 1) . '?'; // example: ?,?,?

$sql_ch='
		select 
			* 
		from mr_work_main 
		WHERE 
			mr_status_id = 7 
			and mr_work_main_id in('.$params_all_id.')
	';
$params = array_map('decode_work_main_id', $in_all_id_params);
$checksave = $work_mainDao->select_work_main($sql_ch,$params);

if(count($checksave)>=1){
	$mr_send_work_id = $send_workDao->save($savedata);
}else{
	//echo ' ';
	$re['st'] = 'error';
	$re['re'] = 'หมายเลขงานที่เลือกถูกบันทีกและพิมพ์ใบคุมนำส่งแล้ว';
	echo json_encode($re);
	exit;
}
//$re = $send_workDao->query2($sql1,$sql2);
$main_id = $params;
$sqllog="INSERT INTO mr_work_log (`mr_work_main_id`, `mr_user_id`, `mr_status_id`) VALUES ";
$loop = 1;
foreach($main_id as $i => $data_i){
	if( $i>= (count($main_id)-1)){
		$sqllog.="(".$data_i.", ".$user_id.", '8');";
	}else{
		if($loop>= 1000){
			$loop = 0;
			$sqllog.="(".$data_i.", ".$user_id.", '8');";
			$sqllog.="INSERT INTO mr_work_log (`mr_work_main_id`, `mr_user_id`, `mr_status_id`) VALUES ";
		}else{
			$sqllog.="(".$data_i.", ".$user_id.", '8'),";
		}
	}
	$loop++;
}

$sqlupdate_mail = 'update mr_work_main 
					set mr_status_id = 8 , 
						mr_send_work_id = '.$mr_send_work_id.' 
					WHERE mr_work_main_id in('.implode(',', $params).')
					and mr_status_id = 7
					;';
$savedata[3]=$sqllog;
$savedata[4]=$sqlupdate_mail;
$re= array();
if(is_numeric($mr_send_work_id)){
	$re['st'] = $send_workDao->query2($sqllog,$sqlupdate_mail);
	$re['re'] = urlencode(base64_encode($mr_send_work_id));
}else{
	$re['st'] = 'error';
}
echo json_encode($re);
 ?>

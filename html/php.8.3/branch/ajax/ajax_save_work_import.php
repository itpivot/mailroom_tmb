<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_branch.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_inout.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_branchDao 	= new Dao_Work_branch();
$userDao 			= new Dao_User();
$branchDao 			= new Dao_Branch();
$employeeDao 		= new Dao_Employee();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$count = $req->get('count');

if(preg_match('/<\/?[^>]+(>|$)/', $count)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$user_data = $userDao->getEmpDataByuseridProfileBranch($auth->getUser());
$now = date('Y-m-d');
$year02 = date('Ymd');
$code_branch_sender = substr($user_data['mr_branch_code'], -3);
$barcode = $code_branch_sender.$year02;
$arr_id = array();
		
for($i=0;$i<$count;$i++){
		
	// $last_barcode 		= $work_mainDao->checkBarcode($barcode);
	// $num_run 			= substr( $last_barcode['mr_work_barcode'],11,4);

	// $num_run++;
	// $num = "0001";
	// if( strlen($num_run) == 1 ){
	// 	$num = "000".$num_run;
	// }else if( strlen($num_run) == 2 ){
	// 	$num = "00".$num_run;		
	// }else if( strlen($num_run) == 3 ){
	// 	$num = "0".$num_run;		
	// }else if( strlen($num_run) == 4 ){
	// 	$num = $num_run;
	// }
	$barcode = '';
	$arrdata[$i]['barcodeok']	= $barcode;
	$val_type 					= $req->get('val_type_'.$i);
	$title 						= $req->get('title_'.$i);
	$remark 					= $req->get('remark_'.$i);
	$strdep 					= $req->get('strdep_'.$i);
	$emp 						= $req->get('emp_'.$i);
	$floor 						= $req->get('floor_'.$i);

	if(preg_match('/<\/?[^>]+(>|$)/', $val_type)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $title)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $remark)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $strdep)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $emp)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $floor)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}

	$save_data_main['mr_work_barcode'] 							= $arrdata[$i]['barcodeok'];
	$save_data_main['mr_work_date_sent'] 						= $now;
	$save_data_main['mr_work_remark'] 							= $remark;
	$save_data_main['mr_type_work_id'] 							= $val_type;
	$save_data_main['mr_status_id']								= 7;
	$save_data_main['mr_user_id'] 								= $auth->getUser();
	$save_data_main['mr_topic'] 								= $title;
	//$save_data_main['mr_floor_id'] 								= $user_data['mr_floor_id'];
	$save_data_main['mr_branch_id'] 							= $user_data['mr_branch_id'];
	$work_main_id = $work_mainDao->save($save_data_main);
		//if($last_barcode['mr_work_barcode'] != ''){
			if(strlen($work_main_id)<=1){
				$Nbarcode['mr_work_barcode'] = $code_branch_sender.date("dmy")."000000".$work_main_id;
			}else if(strlen($work_main_id)<=2){
				$Nbarcode['mr_work_barcode'] = $code_branch_sender.date("dmy")."00000".$work_main_id;
			}else if(strlen($work_main_id)<=3){
				$Nbarcode['mr_work_barcode'] = $code_branch_sender.date("dmy")."0000".$work_main_id;
			}else if(strlen($work_main_id)<=4){
				$Nbarcode['mr_work_barcode'] = $code_branch_sender.date("dmy")."000".$work_main_id;
			}else if(strlen($work_main_id)<=5){
				$Nbarcode['mr_work_barcode'] = $code_branch_sender.date("dmy")."00".$work_main_id;
			}else if(strlen($work_main_id)<=6){
				$Nbarcode['mr_work_barcode'] = $code_branch_sender.date("dmy")."0".$work_main_id;
			}else{
				$Nbarcode['mr_work_barcode'] = $code_branch_sender.date("dmy").$work_main_id;
			} 
			$work_mainDao->save($Nbarcode,$work_main_id);
			
		//}
	$emp_data = $employeeDao->getEmpByIDBranch( $emp );
	if($arrdata[$i]['val_type'] == 3){
		$save_data_branch['mr_floor_id'] 						= $floor;
	}else{
		$save_data_branch['mr_branch_floor']					= $floor;
		$save_data_branch['mr_branch_id']						= $strdep;
	}
		$save_data_branch['mr_emp_id'] 								= $emp;
		$save_data_branch['mr_work_main_id'] 						= $work_main_id;
		
	$work_inout_id  =  $work_inoutDao->save($save_data_branch);

		$save_log['mr_user_id'] 									= $auth->getUser();
		$save_log['mr_status_id'] 									= 7;
		$save_log['mr_work_main_id'] 								= $work_main_id;
		$save_log['remark'] 										= "import";
		
	$work_logDao->save($save_log);
		
	array_push($arr_id,urlencode(base64_encode($work_main_id)));
	$out_[$i]['work_main_id'] = $work_main_id;
	$out_[$i]['barcodeok'] = $arrdata[$i]['barcodeok'];
		
}
;
$out_['arr_id'] = implode(",", $arr_id);
$out_['status'] = '';
echo json_encode($out_);
exit;

 ?>

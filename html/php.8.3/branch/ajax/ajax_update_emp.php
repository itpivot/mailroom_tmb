<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$userDao 			= new Dao_User();
$empDao 			= new Dao_Employee();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$emp_id 		    = $req->get('emp_id');

if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$mr_branch_floor    = $req->get('mr_branch_floor');
$floor_id    = $req->get('floor_id');
$name               		= $req->get('name');
$last_name          		= $req->get('last_name');
$pass_emp           		= $req->get('pass_emp');
$tel                		= $req->get('tel');
$tel_mobile         		= $req->get('tel_mobile');
$email              		= $req->get('email');
$department         		= $req->get('department');
$mr_branch_id       		= $req->get('mr_branch_id');
$mr_send_department_id      = $req->get('department');


if(!empty($floor_id) and $floor_id  != 0){
	$save_data_emp['mr_floor_id'] 	= $floor_id;
}
if(!empty($mr_branch_floor)){
	$save_data_emp['mr_branch_floor'] 	= $mr_branch_floor;
}
if(!empty($name)){
	$save_data_emp['mr_emp_name'] 		= $name;
}
if(!empty($last_name)){
	$save_data_emp['mr_emp_lastname'] 	= $last_name;
}
if(!empty($pass_emp)){
	$save_data_emp['mr_emp_code'] 		= $pass_emp;
}
if(!empty($tel)){
	$save_data_emp['mr_emp_tel'] 		= $tel;
}
if(!empty($tel_mobile)){
	$save_data_emp['mr_emp_mobile']		= $tel_mobile;
}
if(!empty($email)){
	$save_data_emp['mr_emp_email'] 		= $email;
}
if(!empty($department)){
	$save_data_emp['mr_department_id'] 	= $department;
}
if(!empty($mr_branch_id)){
	$save_data_emp['mr_branch_id'] 		= $mr_branch_id;
}
if(!empty($mr_send_department_id)){
	
	$save_data_emp['mr_department_id'] 		= $mr_send_department_id;
}


$id = $empDao->save($save_data_emp,$emp_id );
$result = array();
if(!empty($id)){
	$result['status'] = 200;
	$result['error']['message'] = "บันทึกสำเร็จ";
}else{
	$result['status'] = 500;
	$result['error']['message'] = "บันทึกไม่สำเร็จ";
}

echo json_encode($result);

 ?>

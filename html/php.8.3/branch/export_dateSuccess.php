<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';

require_once 'Dao/Work_main.php';

//require_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';
require_once 'PHPExcel.php';

ob_start();

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//
$req = new Pivot_Request();
$user_Dao = new Dao_User();
$userRole_Dao		= new Dao_UserRole();
$work_main_Dao      = new Dao_Work_main();

$user_id  = $auth->getUser();
$month = $req->get('month');
$year = $req->get('year');

$month_data = $work_main_Dao->getDataBranchSuccess($user_id, $month, $year);


$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma')->setSize(10);
$objPHPExcel->getProperties()->setCreator("Pivot Co.,Ltd")->setLastModifiedBy("Pivot")->setTitle("Report Agent")->setSubject("Report")->setDescription("Report document for Excel, generated using PHP classes.");

$columnIndexs = range('A','N');
$columnNames[0] = array(
		0 => 'No.',                           
		1 => 'Barcode',                            
		2 => 'ชื่อผู้รับ',                            
		3 => 'ที่อยู่',                            
		4 => 'ผู้ลงชื่อรับ',                            
		5 => 'ที่อยู่ ผู้ลงชื่อรับ',                            
		6 => 'ชื่อผู้ส่ง',                            
		7 => 'ที่อยู่',    
		8 => 'ชื่อเอกสาร',
		9 => 'วันที่ส่ง',                            
		10 => 'วันที่สำเร็จ',                            
		11 => 'สถานะงาน',                            
		12 => 'ประเภทการส่ง',                            
		13 => 'หมายเหตุ'     
);

// Set column widths
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setWidth(30);

$row = 1;
foreach ($columnNames as $key => $value) {
	for($index = 0; $index < count($columnIndexs); $index++) {
		$leadColumns[$key][$index]['index'] = $columnIndexs[$index].$row;
		$leadColumns[$key][$index]['name'] = $columnNames[$key][$index];
	}
	$row++;
}

foreach($leadColumns as $head) {
	foreach ($head as $headColumn) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($headColumn['index'], $headColumn['name'])->getStyle($headColumn['index']);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLACK);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->getStartColor()->setARGB('FFDCDCDC');
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);																																 
	}
}

 $indexs = 2;
// Add Data
foreach($month_data as $keys => $vals) {
			if( $vals['mr_work_date_sent'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_sent']);
				$date_order[$keys]['mr_work_date_sent'] = $d."/".$m."/".$y;
			}	
			
			if( $vals['mr_work_date_success'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_success']);
				$date_meet[$keys]['mr_work_date_success'] = $d."/".$m."/".$y;
			}	
			
			if(  $vals['receive_name'] ){
				$name_receive[$keys]['name_re'] 				= $vals['receive_name'];
				$name_receive[$keys]['depart_receive'] 		= $vals['receive_branch_code']." - ".$vals['receive_branch_name']." ชั้น ".$vals['receive_floor_name'];
					if($vals['receive_branch_code']==''){
						$name_receive[$keys]['depart_receive'] 		= $vals['re_department_code']." - ".$vals['re_department_name']." ชั้น ".$vals['receive_floor2'];
					}
			}
			
			if(  $vals['real_receive_name'] ){
				$name_receive[$keys]['real_name_re'] 				= $vals['real_receive_name'];
				$name_receive[$keys]['depart_real_receive'] 		= $vals['receive_branch_code']." - ".$vals['receive_branch_name'];
			}	
			
			if(  $vals['sender_name'] ){
				$name_send[$keys]['send_name'] 				= $vals['sender_name'];
				$name_send[$keys]['depart_send'] 				= $vals['sender_branch_code']." - ".$vals['sender_branch_name'];
				
			}		

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$indexs, ($keys+1))->getStyle('A'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$indexs, "'".$vals['mr_work_barcode'])->getStyle('B'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$indexs, $name_receive[$keys]['name_re'])->getStyle('C'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$indexs, $name_receive[$keys]['depart_receive'])->getStyle('D'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$indexs, $name_receive[$keys]['real_name_re'])->getStyle('E'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$indexs, $name_receive[$keys]['depart_real_receive'])->getStyle('F'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$indexs, $name_send[$keys]['send_name'])->getStyle('G'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$indexs, $name_send[$keys]['depart_send'])->getStyle('H'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$indexs, $vals['mr_topic'])->getStyle('I'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$indexs, $date_order[$keys]['mr_work_date_sent'])->getStyle('J'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$indexs, $date_meet[$keys]['mr_work_date_success'])->getStyle('K'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$indexs, $vals['mr_status_name'])->getStyle('L'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$indexs, $vals['mr_type_work_name'])->getStyle('M'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$indexs, $vals['mr_work_remark'])->getStyle('N'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet(0)->getStyle('A'.$indexs.':N'.$indexs)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$indexs++;
}

$nickname = "report_success";
$filename = $nickname.'.xls';

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Daily Report');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$filename.'"'); 
header('Cache-Control: max-age=0');
header('Pragma: public');
ob_end_clean();
$objWriter->save('php://output');
exit();

<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
ini_set('max_execution_time', 0);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id			= $auth->getUser();
$user_data 			= $userDao->getEmpDataByuserid($user_id);
$alertt 			= '';
$data  				= array();
$mr_send_work_id 	= $req->get('id');
$mr_send_work_id  	= base64_decode(urldecode($mr_send_work_id));

if(preg_match('/<\/?[^>]+(>|$)/', $mr_send_work_id)) {
	$alertt = "
	$.confirm({
    title: 'Alert!',
    content: 'รูปแบบข้อมูลไม่ถูกต้อง!',
    buttons: {
        OK: function () {
             location.href = 'send_work_all.php';
			}
		}
	});
		
	";
}

if($mr_send_work_id == ''){
	$alertt = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'send_work_all.php';
			}
		}
	});
		
	";
}else{
	$params_work_main = array();
	$sql_work_main ="
			SELECT 
					m.*,
					sw.sender_tel as sw_sender_tel,
					sw.barcode as sw_barcode,
					wb.mr_branch_floor ,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					d.mr_department_code as re_mr_department_code,
					d.mr_department_name as re_mr_department_name,
					b.mr_branch_code as re_mr_branch_code,
					b2.mr_branch_code as send_mr_branch_code,
					b.mr_branch_name as re_mr_branch_name,
					b2.mr_branch_name as send_mr_branch_name
			FROM mr_work_main m
			left join mr_send_work sw using(mr_send_work_id)
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = wb.mr_branch_id )
					Left join mr_branch b2 on ( b2.mr_branch_id = e.mr_branch_id )
					where m.mr_send_work_id = ?
					group by m.mr_work_main_id
			";	
		
	array_push($params_work_main, (int)$mr_send_work_id);
	$data = $work_mainDao->select_work_main($sql_work_main,$params_work_main);

	$params_send_work = array();
	$sql_send_work ="
			SELECT 
					*
			FROM  mr_send_work sw
			LEFT JOIN mr_user u ON ( u.mr_user_id = sw.mr_user_id )
			left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_work_inout b on ( b.mr_branch_id = e.mr_branch_id )
			Left join mr_branch bb on ( bb.mr_branch_id = e.mr_branch_id )
			where sw.mr_send_work_id = ?
			group by sw.mr_send_work_id
			";	
	array_push($params_send_work, (int)$mr_send_work_id);
	$data2 = $send_workDao->select_send_work($sql_send_work,$params_send_work);
	

	$apush['re_mr_branch_code']='';
	array_push($data,$apush);
	//echo print_r($data,true);
	$page 	= 1;
	$index 	= 1;
	$no=1;
	$newdata = array();
	$qty = 0;
	foreach($data as $i => $val_i){
		$qty +=$val_i['quty'];
		if(count($data)-1 == $i){
			$val_i=$data[($i-1)];
			$val_i['no']='all';
			$val_i['qty']=$qty;
		}else{
			$val_i['no']=$no;
			$val_i['qty']=$val_i['quty'];
		}
			$newdata[$page]['h']['d'] = $data2[count($data2)-1];
			$newdata[$page]['h']['p'] = $page;
			$newdata[$page]['h']['all'] = ceil(count($data)/10);  
			$newdata[$page]['d'][$index] = $val_i;
			if($index == 10){
				$index = 1;
				$page++;
			}else{
				$index++;
			}
			
		
			$no++;	
		}
}
//echo '<pre>'.print_r($newdata,true);
//exit;

$txt_html='<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      //font: 12pt "Times New Roman";
    font-family: "Garuda";
	//font-size: 14px

    }
	
body,tr,td,th{
	font-family: "Garuda";
	font-size: 16px
 }
 tr,td,th{
	font-family: "Garuda";
	font-size: 14px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      
     
      background: white;
      
	  position: relative;
	  height: 100%;
    }

    .subpage {
      padding: 0cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

   
.table1{
  border-bottom: 1px dotted black;
  border-collapse: collapse;
}


  </style>
</head>

<body>
  <div class="book">';
   foreach($newdata as $i => $td){
	   
    $txt_html.='<div class="page">
      <div class="subpage ">
        <div class="header mb-3">
          <h5 class="">ใบส่งเอกสารประจำวัน</h5>
          <h6 class="">(สำหรับขาส่งเข้าสำนักงานใหญ่)</h6>';
	//$txt_html.='<img src="https://barcode.tec-it.com/barcode.ashx?data='.$td['h']['d']['barcode'].'&code=Code39&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0" height="60" width="450" align="absmiddle">';
    $txt_html.='<center><barcode code="'.$td['h']['d']['barcode'].'" type="C128A"/></center>';
    $txt_html.=''.$td['h']['d']['barcode'].'';
    $txt_html.='</div>
		 
        <div class="form-group">
            <label class="">เรียน : สารบรรณกลางและโลจิสติกส์ / บริหารทรัพยากรอาคาร</label>
        </div>
		<table width="100%">
			<tr>
				<td colspan="2" align="right">หน้า  :&nbsp;&nbsp;&nbsp;<b><u>'.$td['h']['p'].'/'.$td['h']['all'].'&nbsp;&nbsp;&nbsp;</u></b></td>
              </tr>
              <tr>
				<td colspan="2" align="right"><p class="card-text">วันที่  <b><u>  '.date("Y-m-d H:i:s").' </u></b></p></td>
              </tr>
			  
              <tr>
			  <td><p class="card-text">สาขา / เขต /NOC / SME  <b><u>&nbsp;&nbsp;&nbsp;'.$td['h']['d']['mr_branch_name'].'&nbsp;&nbsp;&nbsp;</u></b></p> </td>
			  <td align="right"><p class="card-text">รหัสสาขา/หน่วยงาน  <b><u>&nbsp;&nbsp;&nbsp;'.$td['h']['d']['mr_branch_code'].'&nbsp;&nbsp;&nbsp;</u></b> 
			  โทรศัพท์ <b><u>&nbsp;&nbsp;&nbsp;'.$td['h']['d']['sender_tel'].'&nbsp;&nbsp;&nbsp;</u></b></p></td>
			  </tr>
			  <tr>
			  <td align=""><p class="card-text">ขอนำส่งเอกสารให้หน่วยงาน / สาขา ดังนี้  </p></td>
			  <td align="right"></td>
			  </tr>
		 </table>
        <div class=" mb-2 p-2">
           
        </div>

        <div class="table-responsive"><br>
          <table    class="table1" border="0">
            <thead class="table1">
              <tr>
				<th  class="table1">ลำดับ</th>
                <th class="table1">ผู้รับเอกสาร</th>
                <th class="table1">ชื่อเอกสาร</th>
                <th class="table1">จำนวนซอง</th>
                <th class="table1">Barcode</th>
              </tr>
            </thead>';
            foreach($td['d'] as $j => $subd){
            $txt_html.='<tbody>';
			if($subd['no'] == "all"){
			$txt_html.='<tr>
				<th class="table1"><u>รวมจำนวนซองทั้งสิ้น</u></th>
				<th class="table1"><u>'.$subd['qty'].'</u></th>
				<th class="table1"></th>
			</tr>';
			}else{
              $txt_html.='<tr>
                
                <td class="table1">'.$subd['no'].'</td>
				<td class="table1">'.$subd['re_name'].'  '.$subd['re_lname'].'  -  ';
				if($subd['mr_type_work_id'] != "2"){
					$txt_html.= $subd['re_mr_department_name'];
				}else{
					$txt_html.=$subd['re_mr_branch_name'];
						if($subd['mr_branch_floor'] != ''){
							$txt_html.='/ ชั้น  '.$subd['mr_branch_floor'];
						}
					
				}
				$mr_topic='';
				if(strlen ( $subd['mr_topic'] ) > 15 ){
					//$mr_topic = substr(ucfirst_utf8($subd['mr_topic']),0,15);
					$mr_topic = iconv_substr($subd['mr_topic'], 0,15, "UTF-8");;
					$mr_topic.='...';
				}else{
					$mr_topic='';
				}	
				$txt_html.='</td>
                <td class="table1">'.$mr_topic.'</td>
                <td class="table1">'.$subd['qty'].'</td>
                <td class="table1">';
				//$txt_html.='<img src="https://barcode.tec-it.com/barcode.ashx?data='.$subd['mr_work_barcode'].'&code=Code39&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0" height="30" width="250" align="absmiddle">';
				$txt_html.='<barcode code="'.$subd['mr_work_barcode'].'" type="C128A" height="0.66"/>';
				$txt_html.='<center>'.$subd['mr_work_barcode'].'</center>';
				$txt_html.='</td>
			  </tr>';
			 }
            $txt_html.='</tbody>';
            }
          $txt_html.='</table>
        </div>
		 <p>    หมายเหตุ: รับเอกสารหลังเวลาที่กำหนด  สารบรรณกลางและโลจีสติกส์  จะส่งให้หน่วยงานในวันถัดไป</p>
		
      </div>
	  <div class="footer">
	  <div class="row">
	  <div class="col col-md-12">
		<p>โปรดยืนยันการนำส่งเอกสารข้างต้นนี้ หากมีรายการผิดพลาดกรุณาแจ้งทันที</p><br>
	  </div>
	  </div>
	  <table width="100%">
		<tr>
			<td width="50%">
				<center>
				
					<p>ลงชื่อ....................................................ผู้ส่ง </p>
					<p>เลขที่พนักงาน......................................</p>
					<p></p>	      
				</center>	                          
		   </td>                             
		   <td width="50%">	          
		   	<center>	                      
		   	<p>ลงชื่อ....................................................ผู้รับรอง</p>
		   	<p>เลขที่พนักงาน......................................</p>
		   	<p>(ผู้ช่วยผู้จัดการสาขาขึ้นไป)</p>
		   	</center>
		   </td>
		   </tr>
	   </table>
	 
    
    </div>
    </div>';

    
   }
  $txt_html.='</div>
</body>

</html>';




//echo '<pre>'.print_r($newdata,true).'</pre>';
//echo $txt_html;
//exit;
function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}



require_once 'mpdf-development/vendor/autoload.php';
 $mpdf = new \Mpdf\Mpdf();
 $pdf_orientation = 'P';//L&P;
 $left=3;
 $right=3;
 $top=5;
 $bottom=5;
 $header=0;
 $footer=1;
$align = $__pdf_pgn_oalign;
$border = "border-top-";
$pgf_o = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
$mpdf->DefHTMLFooterByName("fo", $pgf_o);
// $align = $__pdf_pgn_ealign;
// $pgf_e = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fe", $pgf_e);
$mpdf->AddPage($pdf_orientation,'','','','',$left,$right,$top,$bottom,$header,$footer,'','','html_fo', 'html_fe', 0, 0, 1, 1);

 //$mpdf->AddPage('L'); 
 $mpdf->WriteHTML($txt_html);
 //$mpdf->setFooter('{PAGENO}');
 $mpdf->Output();
exit;


//fpr PHP5.6
// require_once 'ThaiPDF/thaipdf.php';
// $left=3;
// $right=3;
// $top=5;
// $bottom=5;
// $header=0;
// $footer=5;
// $filename='file.pdf';
// pdf_margin($left,$right,$top, $bottom,$header,$footer);
// pdf_html($txt_html);
// pdf_orientation('P');        
// pdf_echo();

// exit;



$template = Pivot_Template::factory('branch/print_send_work_all.tpl');
$template->display(array(
	//'debug' => print_r($newdata,true),
	'data' => $newdata,
	'alertt' => $alertt,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
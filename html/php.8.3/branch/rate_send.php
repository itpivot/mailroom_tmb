<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();


$id = $req->get('id');
$id = base64_decode(urldecode($id));

if(preg_match('/<\/?[^>]+(>|$)/', $id)) {
	$alert = "
			alertify.alert('ไม่พบข้อมูลสาขากรุณาอัปเดทข้อมูล  Profile', function (e) {
			if (e) {
				window.open('profile.php');
				}
			});
	";
}else{
	$w_main = $work_main_Dao->getBarcodeByid(intval($id));
}

$template = Pivot_Template::factory('branch/rate_send.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'userRoles' => $userRoles,
	'users' => $users,
	'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles(),
    'send_id' => $id,
    'alert' => $alert,
	'barcode' => $w_main['mr_work_barcode'],
	'serverPath' => $_CONFIG->site->serverPath
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Round.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$roundDao 		= new Dao_Round();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();


$barcode_full 		= "TM".date("dmy");
$user_id			= $auth->getUser();
$round				= $roundDao->getRoundBranch_tbank();
$user_data 			= $userDao->getempByuserid($user_id);
echo "<pre>".print_r($user_data,true)."</pre>";
$template = Pivot_Template::factory('branch/reciever_branch.tpl');
$template->display(array(
	//'debug' 		=> print_r($path_pdf,true),
	//'userRoles' 	=> $userRoles,
	'select' 		=> 0,
	//'success' 		=> $success,
	'barcode_full' 	=> $barcode_full,
	//'userRoles' 	=> $userRoles,
	'user_data'	 	=> $user_data,
	'round' 		=> $round,
	'today' 		=> date("Y-m-d"),
	//'users' 		=> $users,
	'role_id' 		=> $auth->getRole(),
	'roles' 		=> Dao_UserRole::getAllRoles()
));
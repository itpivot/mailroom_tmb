<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Status.php';
require_once 'Dao/Department.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$branchDao			= new Dao_Branch();
$statusDao			= new Dao_Status();
$departmentDao		= new Dao_Department();


//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();


$user_id 				= $auth->getUser();
$user_data 				= $userDao->getempByuserid($user_id);
//$status_data 			= $statusDao->getStatusBranch();

$status_data1 			= $userDao->select('SELECT * FROM `mr_status` where `mr_status_id` in(1,2,3,4,5)');
$status_data2 			= $userDao->select('SELECT * FROM `mr_status` where `mr_status_id` in(7,8,9,10,11,12,13,14,15)');
$status_data3 			= $userDao->select('SELECT * FROM `mr_status` where `mr_status_id` in(6)');

	


$template = Pivot_Template::factory('branch/search_work.tpl');
$template->display(array(
	// 'debug' => print_r($path_pdf,true),
	'success' => (isset($success) ? $success : null),
	'status_data1' => $status_data1,
	'status_data2' => $status_data2,
	'status_data3' => $status_data3,
	//'userRoles' => (isset($userRoles) ? $userRoles : null),
	'user_data' => $user_data,
	'select' => '1',
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
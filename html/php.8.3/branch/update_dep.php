<?php 
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Address.php';
require_once 'PHPExcel.php';
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL & ~E_NOTICE);




$auth 					= new Pivot_Auth();
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();
$addressDao 				= new Dao_Address();



try {
	$inputFileName = 'mr_department.xlsx';
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
	
	
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}

$sheet 				= $objPHPExcel->getSheet(0); 
$highestRow 		= $sheet->getHighestRow(); 
$highestColumn 		= $sheet->getHighestColumn();
$ch_num 			= 0;
//  Loop through each row of the worksheet in turn
$emp_clode 			= array();
$str_tble 			= array();
$row_no				= 0;

for ($row = 2; $row <= $highestRow; $row++){ 
	$update_data = array();
    $mr_department_id					= $sheet->getCell('A'.$row)->getValue();
    $mr_department_name					= $sheet->getCell('B'.$row)->getValue();
    $mr_department_code					= $sheet->getCell('C'.$row)->getValue();
    $mr_department_floor					= $sheet->getCell('D'.$row)->getValue();
		
	
	
		$update_data['mr_department_name']			= $mr_department_name;
		$update_data['mr_department_code']			= $mr_department_code;
		$update_data['mr_department_floor']			= $mr_department_floor;
	if($mr_department_id!=''){
		
		$mr_department_id 							= $departmentDao->save($update_data,$mr_department_id);
	}else{
		echo "<pre> ว่าง : ".print_r($mr_department_id, true)."</pre>";
	}
}
// UPDATE `mr_branch` SET `mr_branch_category_id` = '2' WHERE `mr_branch_code` LIKE '%_TBAK'
//echo json_encode($str_tble);
//echo "<pre>".print_r($branch, true)."</pre>";
exit;
?>
<?php 
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'PHPExcel.php';




$auth 					= new Pivot_Auth();
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();



try {
	$inputFileName = 'user_mess_hub.xlsx';
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
	
	
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$ch_num = 0;
//  Loop through each row of the worksheet in turn
$emp_clode = array();
$str_tble = array();
$row_no=0;
for ($row = 2; $row <= $highestRow; $row++){ 
    $code					= $sheet->getCell('A'.$row)->getValue();
    $pass					= $sheet->getCell('B'.$row)->getValue();
    $name					= $sheet->getCell('G'.$row)->getValue();
    $lname					= $sheet->getCell('H'.$row)->getValue();
    $hub_id					= $sheet->getCell('I'.$row)->getValue();
    //$a[$row]['w_type']				= $sheet->getCell('B'.$row)->getValue();
	$sql = "
		SELECT *
FROM `mr_emp`
WHERE `mr_emp_code` LIKE '". $code."'";
		
		//exit;
$mr_emp = $employeeDao->select($sql);
//echo "<pre>".print_r($mr_emp, true)."</pre>";
if(empty($mr_emp)){
	//echo  $code.": ไม่มี  <br>";
	 $mr_emp = array();
	$mr_emp['mr_emp_name'] 				= $name;
	$mr_emp['mr_emp_lastname'] 			= $lname;
	$mr_emp['mr_emp_code'] 				= $code;
	$mr_emp['mr_emp_tel'] 				= null;
	$mr_emp['mr_emp_mobile'] 			= NULL;
	$mr_emp['mr_emp_email'] 			= 'nathawut.sa@pivot.co.th';
	$mr_emp['mr_department_id'] 		= '457';
	$mr_emp['mr_position_id'] 			= NULL;
	$mr_emp['mr_floor_id'] 				= '1';
	$mr_emp['mr_cost_id'] 				= '0';
	$mr_emp['mr_branch_id'] 			= NULL;
	$mr_emp['mr_branch_floor'] 			= NULL;
	$mr_emp['mr_workplace'] 			= NULL;
	$mr_emp['mr_workarea'] 				= NULL;
	$mr_emp['mr_hub_id'] 				= $hub_id;
	$mr_emp_id = $employeeDao->save($mr_emp);

	//echo "<pre>".print_r($mr_emp, true)."</pre>";
}else{
	//echo  $code.": มี  <br>";
	$mr_emp_id 							= $mr_emp[0]['mr_emp_id'] ;
	$mr_em = array();
	$mr_em['mr_emp_name'] 				= $name;
	$mr_em['mr_emp_lastname'] 			= $lname;
	$mr_em['mr_emp_code'] 				= $code;
	$mr_em['mr_emp_email'] 			= 'nathawut.sa@pivot.co.th';
	$mr_em['mr_hub_id'] 				= $hub_id;
	if($mr_emp_id!=''){
		$mr_id = $employeeDao->save($mr_em,$mr_emp_id);
	}
	
	
	//echo "<pre>".print_r($mr_user, true)."</pre> mr_branch_code:".$mr_position_code;
 }	
 
 $sql = "
		SELECT *
FROM `mr_user`
WHERE `mr_user_username` LIKE '". $code."'";
		
		//exit;
$mr_u = $employeeDao->select($sql);

//echo "<pre>".print_r($mr_emp, true)."</pre>";
if(empty($mr_u)){
	echo  $code.": ไม่มี  <br>";
	$mr_user = array();
	$mr_user['mr_user_username'] 		= $code;
	$mr_user['mr_user_password'] 		= $auth->encryptAES((string)'pivot123');;
	$mr_user['is_first_login'] 			= '1';
	$mr_user['date_create'] 			= date('Y-m-d H:i:s');
	$mr_user['sys_timestamp'] 			= date('Y-m-d H:i:s');
	$mr_user['change_password_date'] 	= date('Y-m-d H:i:s');
	$mr_user['active'] 					= '1';
	$mr_user['mr_emp_id'] 				= $mr_emp_id;
	$mr_user['mr_user_role_id'] 		= '8';
	$mr_user['isLogin'] 				= '0';
	$mr_user['isFailed'] 				= '0';
	$mr_user['tocent'] 					=  Null;
	$mr_emp_id = $userDao->save($mr_user);
}else{
	echo  $code.": มี  <br>";
	$mr_user = array();
	$mr_uid = $mr_u[0]['mr_user_id'];
	$mr_user['mr_user_username'] 		= $code;
	$mr_user['mr_user_password'] 		= $auth->encryptAES((string)'pivot123');;
	$mr_user['is_first_login'] 			= '1';
	$mr_user['active'] 					= '1';
	$mr_user['mr_emp_id'] 				= $mr_emp_id;
	$mr_user['mr_user_role_id'] 		= '8';
	$mr_user['isLogin'] 				= '0';
	$mr_user['isFailed'] 				= '0';
	$mr_user['tocent'] 					=  Null;
	if($mr_uid!=''){
		$mr_emp_id = $userDao->save($mr_user,$mr_uid);
	}
}
 
}

//echo json_encode($str_tble);
//echo "<pre>".print_r($a, true)."</pre>";
exit;
?>
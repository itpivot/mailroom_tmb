<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/seting.byhandZone.tpl */
class __TwigTemplate_03937aeb41d01ce11066cbf2180b2d1e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/seting.byhandZone.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 25
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t

\$.Thailand({
\tdatabase: '../themes/jquery.Thailand.js/database/geodb.json',
  \$district: \$('#sub_district_re'), // input ของตำบล
\t\$amphoe: \$('#district_re'), // input ของอำเภอ
\t\$province: \$('#province_re'), // input ของจังหวัด
\t\$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
\tonDataFill: function (data) {
\t\t\$('#receiver_sub_districts_code').val('');
\t\t\$('#receiver_districts_code').val('');
\t\t\$('#receiver_provinces_code').val('');
  
\t\tif(data) {
\t\t\t\$('#sub_districts_code_re').val(data.district_code);
\t\t\t\$('#districts_code_re').val(data.amphoe_code);
\t\t\t\$('#provinces_code_re').val(data.province_code);
\t\t\tconsole.log(data);
\t\t}
\t\t
\t}
  });
  
var tbl_floor = \$('#tb_floor').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'sys_time'},
\t\t{'data': 'mr_sub_districts_name'},
\t\t{'data': 'zipcode'},
\t\t{'data': 'byhand_zone_name'},
\t\t{'data': 'action'}
    ]
});



  \$('#work_byhand_zone_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});

  \$('#mr_work_byhand_zone_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});


\$( \"#min_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight;
\t//console.log(description);
\t\$( \"#description_weight\" ).val(description);
  });
\$( \"#max_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight
\t\$( \"#description_weight\" ).val(description);
  });


  
  \$('#myform_data_price').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'add_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'min_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'max_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'description_weight': {
\t\t\trequired: true,
\t\t},
\t\t'post_price': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t  
\t},
\tmessages: {
\t\t'add_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภท',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'min_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'max_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'description_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'post_price': {
\t\t  required: 'กรุณาระบุ ราคาไปรษณีย์ไทย',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });

\$('#btn-add-zone-location').click(function() {
\tif(\$('#myform_data_price').valid()) {
\t \tvar form = \$('#myform_data_price');
\t \tvar csrf_token = \$('#csrf_token').val();
      \tvar serializeData = form.serialize()+\"&page=addZoneLocation&csrf_token=\"+csrf_token;
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.byhandZone.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t\$('#modaiAddPrice').modal('hide')
\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\$('#myform_data_price')[0].reset();
\t\t\t\t\tload_dataZone_location();
\t\t\t\t\t
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$('#btn-modaiAddPrice').click(function() {
\t\$('#add_type_post_id').val('').trigger('change');
\t\$('#modaiAddPrice').modal({
\t\tkeyboard: false,backdrop:'static'
\t});
\t\$('#myform_data_price')[0].reset();
\t\$('.div-btn-edit').hide();
\t\$('.div-btn-add').show();
\t
});



load_dataZone_location();
";
    }

    // line 266
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 267
        echo "


function load_dataZone_location() {
var data =  \$('#myform_data_floor').serialize()+\"&page=loadZone_location\";
//console.log(data);
\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_seting.byhandZone.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token']);
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_floor').DataTable().clear().draw();
\t\t\t\$('#tb_floor').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function remove_zone_location(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อลบ',
\tfunction(){ 
\t\tvar csrf_token = \$('#csrf_token').val();
\t\t\$.ajax({
\t\t\t\tmethod: \"POST\",
\t\t\t\tdataType:'json',
\t\t\t\turl: \"ajax/ajax_seting.byhandZone.php\",
\t\t\t\tdata:{
\t\t\t\t\tid \t\t\t: id,
\t\t\t\t\tpage \t\t: 'remove',
\t\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t\t},
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t// setting a timeout
\t\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t},
\t\t\t\terror: function (error) {
\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t// location.reload();
\t\t\t\t}
\t\t\t})
\t\t\t.done(function( res ) {
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\t\tload_dataZone_location();
\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t});
\t\t}, function(){ 
\t\t\talertify.error('Cancel')
\t\t});
}





";
    }

    // line 354
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 355
        echo "<div  class=\"container-fluid\">
\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"";
        // line 356
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">
<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">



\t<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class=\"modal fade\" id=\"modaiAddPrice\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modaiAddPriceTitle\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">เพิ่มราคาใหม่</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t  </button>
\t\t</div>
\t\t<div class=\"modal-body\">
\t\t\t<form id=\"myform_data_price\">
\t\t\t<div class=\"row\">
\t\t\t<div class=\"form-group col-12\">
\t\t\t\t<h5 class=\"card-title\">ประเภท <span class=\"box_error\" id=\"err_mr_work_byhand_zone_id\"></span></h5>
\t\t\t\t<select  name=\"mr_work_byhand_zone_id\" class=\"form-control \" id=\"mr_work_byhand_zone_id\"  data-error=\"#err_mr_work_byhand_zone_id\">
\t\t\t\t\t\t<option value=\"\" selected>Byhand Zone</option>
\t\t\t\t\t\t";
        // line 381
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["byhand_zoneData"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 382
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_work_byhand_zone_id", [], "any", false, false, false, 382), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "byhand_zone_name", [], "any", false, false, false, 382), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 384
        echo "\t\t\t\t</select>
\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t<div class=\"form-group col-12\">
\t\t\t\t<table width=\"100%\">
\t\t\t\t<tr>\t
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t</form>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-add\">
\t\t  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t  <button id=\"btn-add-zone-location\" type=\"button\" class=\"btn btn-primary\">บันทึก</button>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-edit\">
\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t\t<button id=\"btn-edit-price\" type=\"button\" class=\"btn btn-primary\">แก้ไข</button>
\t\t</div>
\t  </div>
\t</div>
  </div>



\t<br>
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>Byhand Zone</b></h3></label><br>
\t\t\t\t<label>Byhand Zone > อัปเดท Zone </label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"card\">
\t\t<div class=\"card-body\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<hr>
\t\t\t\t\t<form id=\"myform_data_floor\">
\t\t\t\t\t\t<div class=\"form-group col-12 col-md-3\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">Zone</h5>
\t\t\t\t\t\t\t<select  name=\"work_byhand_zone_id\" class=\"form-control \" id=\"work_byhand_zone_id\" >
\t\t\t\t\t\t\t<option value=\"\" selected>Byhand Zone</option>
\t\t\t\t\t\t\t\t";
        // line 460
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["byhand_zoneData"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 461
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_work_byhand_zone_id", [], "any", false, false, false, 461), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "byhand_zone_name", [], "any", false, false, false, 461), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 463
        echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-info\" onclick=\"load_dataZone_location()\">
\t\t\t\t\t\t\t<i class=\"material-icons\" style=\"padding-right:5px;\">search</i>
\t\t\t\t\t\t</button>
\t\t\t\t\t
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn-modaiAddPrice\" data-backdrop=\"static\">
\t\t\t\t\t\t\tเพิ่มข้อมูล Zone
\t\t\t\t\t\t  </button>
\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_floor\">
\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่อัปเดท</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">เขต</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">รหัสไปรษณีย์</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">โซน</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t  </table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t </div>

</div>
</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




";
    }

    // line 514
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 516
        if ((($context["debug"] ?? null) != "")) {
            // line 517
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 519
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/seting.byhandZone.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  647 => 519,  641 => 517,  639 => 516,  632 => 514,  580 => 463,  569 => 461,  565 => 460,  487 => 384,  476 => 382,  472 => 381,  444 => 356,  441 => 355,  437 => 354,  347 => 267,  343 => 266,  130 => 59,  95 => 26,  91 => 25,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}\t

\$.Thailand({
\tdatabase: '../themes/jquery.Thailand.js/database/geodb.json',
  \$district: \$('#sub_district_re'), // input ของตำบล
\t\$amphoe: \$('#district_re'), // input ของอำเภอ
\t\$province: \$('#province_re'), // input ของจังหวัด
\t\$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
\tonDataFill: function (data) {
\t\t\$('#receiver_sub_districts_code').val('');
\t\t\$('#receiver_districts_code').val('');
\t\t\$('#receiver_provinces_code').val('');
  
\t\tif(data) {
\t\t\t\$('#sub_districts_code_re').val(data.district_code);
\t\t\t\$('#districts_code_re').val(data.amphoe_code);
\t\t\t\$('#provinces_code_re').val(data.province_code);
\t\t\tconsole.log(data);
\t\t}
\t\t
\t}
  });
  
var tbl_floor = \$('#tb_floor').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'sys_time'},
\t\t{'data': 'mr_sub_districts_name'},
\t\t{'data': 'zipcode'},
\t\t{'data': 'byhand_zone_name'},
\t\t{'data': 'action'}
    ]
});



  \$('#work_byhand_zone_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});

  \$('#mr_work_byhand_zone_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});


\$( \"#min_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight;
\t//console.log(description);
\t\$( \"#description_weight\" ).val(description);
  });
\$( \"#max_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight
\t\$( \"#description_weight\" ).val(description);
  });


  
  \$('#myform_data_price').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'add_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'min_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'max_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'description_weight': {
\t\t\trequired: true,
\t\t},
\t\t'post_price': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t  
\t},
\tmessages: {
\t\t'add_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภท',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'min_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'max_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'description_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'post_price': {
\t\t  required: 'กรุณาระบุ ราคาไปรษณีย์ไทย',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });

\$('#btn-add-zone-location').click(function() {
\tif(\$('#myform_data_price').valid()) {
\t \tvar form = \$('#myform_data_price');
\t \tvar csrf_token = \$('#csrf_token').val();
      \tvar serializeData = form.serialize()+\"&page=addZoneLocation&csrf_token=\"+csrf_token;
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.byhandZone.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t\$('#modaiAddPrice').modal('hide')
\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\$('#myform_data_price')[0].reset();
\t\t\t\t\tload_dataZone_location();
\t\t\t\t\t
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$('#btn-modaiAddPrice').click(function() {
\t\$('#add_type_post_id').val('').trigger('change');
\t\$('#modaiAddPrice').modal({
\t\tkeyboard: false,backdrop:'static'
\t});
\t\$('#myform_data_price')[0].reset();
\t\$('.div-btn-edit').hide();
\t\$('.div-btn-add').show();
\t
});



load_dataZone_location();
{% endblock %}
{% block javaScript %}



function load_dataZone_location() {
var data =  \$('#myform_data_floor').serialize()+\"&page=loadZone_location\";
//console.log(data);
\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_seting.byhandZone.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token']);
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_floor').DataTable().clear().draw();
\t\t\t\$('#tb_floor').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function remove_zone_location(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อลบ',
\tfunction(){ 
\t\tvar csrf_token = \$('#csrf_token').val();
\t\t\$.ajax({
\t\t\t\tmethod: \"POST\",
\t\t\t\tdataType:'json',
\t\t\t\turl: \"ajax/ajax_seting.byhandZone.php\",
\t\t\t\tdata:{
\t\t\t\t\tid \t\t\t: id,
\t\t\t\t\tpage \t\t: 'remove',
\t\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t\t},
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t// setting a timeout
\t\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t},
\t\t\t\terror: function (error) {
\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t// location.reload();
\t\t\t\t}
\t\t\t})
\t\t\t.done(function( res ) {
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\t\tload_dataZone_location();
\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t});
\t\t}, function(){ 
\t\t\talertify.error('Cancel')
\t\t});
}





{% endblock %}
{% block Content2 %}
<div  class=\"container-fluid\">
\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"{{csrf_token}}\">
<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">



\t<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class=\"modal fade\" id=\"modaiAddPrice\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modaiAddPriceTitle\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">เพิ่มราคาใหม่</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t  </button>
\t\t</div>
\t\t<div class=\"modal-body\">
\t\t\t<form id=\"myform_data_price\">
\t\t\t<div class=\"row\">
\t\t\t<div class=\"form-group col-12\">
\t\t\t\t<h5 class=\"card-title\">ประเภท <span class=\"box_error\" id=\"err_mr_work_byhand_zone_id\"></span></h5>
\t\t\t\t<select  name=\"mr_work_byhand_zone_id\" class=\"form-control \" id=\"mr_work_byhand_zone_id\"  data-error=\"#err_mr_work_byhand_zone_id\">
\t\t\t\t\t\t<option value=\"\" selected>Byhand Zone</option>
\t\t\t\t\t\t{% for type in byhand_zoneData %}
\t\t\t\t\t\t<option value=\"{{type.mr_work_byhand_zone_id}}\"> {{type.byhand_zone_name}}</option>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t</select>
\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t<div class=\"form-group col-12\">
\t\t\t\t<table width=\"100%\">
\t\t\t\t<tr>\t
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t</form>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-add\">
\t\t  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t  <button id=\"btn-add-zone-location\" type=\"button\" class=\"btn btn-primary\">บันทึก</button>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-edit\">
\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t\t<button id=\"btn-edit-price\" type=\"button\" class=\"btn btn-primary\">แก้ไข</button>
\t\t</div>
\t  </div>
\t</div>
  </div>



\t<br>
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>Byhand Zone</b></h3></label><br>
\t\t\t\t<label>Byhand Zone > อัปเดท Zone </label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"card\">
\t\t<div class=\"card-body\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<hr>
\t\t\t\t\t<form id=\"myform_data_floor\">
\t\t\t\t\t\t<div class=\"form-group col-12 col-md-3\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">Zone</h5>
\t\t\t\t\t\t\t<select  name=\"work_byhand_zone_id\" class=\"form-control \" id=\"work_byhand_zone_id\" >
\t\t\t\t\t\t\t<option value=\"\" selected>Byhand Zone</option>
\t\t\t\t\t\t\t\t{% for type in byhand_zoneData %}
\t\t\t\t\t\t\t\t<option value=\"{{type.mr_work_byhand_zone_id}}\"> {{type.byhand_zone_name}}</option>
\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-info\" onclick=\"load_dataZone_location()\">
\t\t\t\t\t\t\t<i class=\"material-icons\" style=\"padding-right:5px;\">search</i>
\t\t\t\t\t\t</button>
\t\t\t\t\t
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn-modaiAddPrice\" data-backdrop=\"static\">
\t\t\t\t\t\t\tเพิ่มข้อมูล Zone
\t\t\t\t\t\t  </button>
\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_floor\">
\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่อัปเดท</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">เขต</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">รหัสไปรษณีย์</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">โซน</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t  </table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t </div>

</div>
</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/seting.byhandZone.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\seting.byhandZone.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/work_out.tpl */
class __TwigTemplate_0973cb79a1fc572e0be8782910efc9d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'styleReady' => [$this, 'block_styleReady'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/work_out.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 8
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 16
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "
\t\tcheck_name_re();
\t\t\$(\"#dataType_contact\").select2({ width: '100%' });
\t\t
\t\t
\t
var tb_con = \$('#tb_con').DataTable({ 
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'department'},
        {'data': 'floor'},
        {'data': 'mr_contact_name'},
        {'data': 'emp_code'},
        {'data': 'emp_tel'},
        {'data': 'mr_position_name'},
        {'data': 'remark'}
    ]
});


\$('#ClearFilter').on( 'click', function () {
\t\$('#search1').val('');
\t\$('#search2').val('');
\t\$('#search3').val('');
\t\$('#search4').val('');
\t\$('#search5').val('');
\t\$('#search6').val('');
\t\$('#search7').val('');
\ttb_con.search( '' ).columns().search( '' ).draw();
});

\$('#search1').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(1).search(this.value, true, false).draw();
});
\$('#search2').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(2).search(this.value, true, false).draw();
});
\$('#search3').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(3).search(this.value, true, false).draw();
});
\$('#search4').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(4).search(this.value, true, false).draw();
});
\$('#search5').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(5).search(this.value, true, false).draw();
});
\$('#search6').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(6).search(this.value, true, false).draw();
});
\$('#search7').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(7).search(this.value, true, false).draw();
});


\t\t\$(\"#btn_save\").click(function(){
\t\t\t\t\$( \".myErrorClass\" ).removeClass( \"myErrorClass\" );
\t\t\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\t\t\tvar name_receiver \t\t= \$(\"#name_receiver\").val();
\t\t\t\t\tvar pass_depart \t\t= \$(\"#pass_depart\").val();
\t\t\t\t\tvar floor \t\t\t\t= \$(\"#floor\").val();
\t\t\t\t\tvar depart \t\t\t\t= \$(\"#depart\").val();
\t\t\t\t\tvar remark \t\t\t\t= \$(\"#remark\").val();
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar floor_send \t\t\t= \$(\"#floor_send\").val();
\t\t\t\t\tvar floor_id \t\t\t= \$(\"#floor_id\").val();
\t\t\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic\").val();
\t\t\t\t\tvar mr_branch_id \t\t= \$(\"#mr_branch_id\").val();
\t\t\t\t\tvar branch_send \t\t= \$(\"#branch_send\").val();
\t\t\t\t\tvar branch_floor \t\t= \$(\"#branch_floor\").val();
\t\t\t\t\tvar send_user_id \t\t= \$(\"#send_user_id\").val();
\t\t\t\t\tvar csrf_token\t \t\t\t= \$('#csrf_token').val();
\t\t\t\t\tvar qty\t \t\t\t= \$('#qty').val();
\t\t\t\t\tvar status \t\t\t\t= true;

\t\t\t\t\tvar data = \$('#branch_floor').select2('data')
\t\t\t\t\tvar branch_floor_name \t\t= '';
\t\t\t\t\tif(\$('#branch_floor').val()){
\t\t\t\t\t\tbranch_floor_name \t\t= data[0].text;
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(pass_emp == \"\" || pass_emp == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\$('#select2-name_receiver_select-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลผู้รับ!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\$('#topic').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\tif(mr_branch_id == \"\" || mr_branch_id == null || mr_branch_id == 0 ){
\t\t\t\t\t\t\tif(branch_send == 0 || branch_send == null){
\t\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\t\$('#select2-branch_send-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลสาขาของผู้รับ!');
\t\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\tif(branch_send == 1){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t//\$('#mr_branch_name').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\$('#select2-branch_send-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','หน้าต่างนี้สำหรับส่งที่สาขาเท่านั้น!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}else\tif(branch_send == \"\" || branch_send == null || branch_send == 0 ){
\t\t\t\t\t\t\tif(mr_branch_id == 1){
\t\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\t//\$('#mr_branch_name').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\t\$('#select2-branch_send-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','หน้าต่างนี้สำหรับส่งที่สาขาเท่านั้น!');
\t\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(floor_id);
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tpass_emp: pass_emp,
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tfloor_send: floor_send,
\t\t\t\t\t\t\t\tfloor_id: floor_id,
\t\t\t\t\t\t\t\tmr_branch_id: mr_branch_id,
\t\t\t\t\t\t\t\tbranch_send: branch_send,
\t\t\t\t\t\t\t\tmr_branch_floor\t\t: branch_floor_name,
\t\t\t\t\t\t\t\tcsrf_token: csrf_token,
\t\t\t\t\t\t\t\tsend_user_id: send_user_id,
\t\t\t\t\t\t\t\tqty: qty,
\t\t\t\t\t\t\t\ttopic: topic
\t\t\t\t\t\t\t}


\t\t\t\t\t\t\talertify.confirm(\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง\", function() {

\t\t\t\t\t\t\t\t//\$.when(saveInOut(obj))
\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\turl: url+'ajax/ajax_save_workout.php',
\t\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\t\tdata: obj,
\t\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\t\t\t\t//return;
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\tif(res) {
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' + res['mr_work_barcode'] + '<b></font></h1>';
\t\t\t\t\t\t\t\t\t\t\talertify.confirm('เกิดข้อผิดพลาด','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg,
\t\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\t\t\t\t\t\t\talertify.success('print');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['encode'];
\t\t\t\t\t\t\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\t\tfunction() {
\t\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'ตกลง'});


\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\terror: function (error) {
\t\t\t\t\t\t\t\t\t alert('เกิดข้อผิดพลาด !!!');
\t\t\t\t\t\t\t\t\t window.location.reload();
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).setHeader('<h5> ยืนยันการบันทึกข้อมูล </h5> ');
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$('#name_receiver ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_depart ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#floor ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_emp ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#depart ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t}
\t\t});
\t\t\$(\"#pass_emp\").keyup(function(){
\t\t\tcheck_name_re();
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress2.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\tvar fullname = res['data']['mr_emp_name'] + \" \" + res['data']['mr_emp_lastname'];
\t\t\t\t\t\t\t\$(\"#name_receiver_select\").html('');
\t\t\t\t\t\t\tvar dataOption = {
\t\t\t\t\t\t\t\tid: res['data']['mr_emp_id'],
\t\t\t\t\t\t\t\ttext: res['data']['mr_emp_code'] + \" : \"+ res['data']['mr_emp_name'] + \" \" + res['data']['mr_emp_lastname']
\t\t\t\t\t\t\t};

\t\t\t\t\t\t\tvar newOption = new Option(dataOption.text, dataOption.id, false, false);
\t\t\t\t\t\t\t\$('#name_receiver_select').append(newOption).trigger('change');
\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};

\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(fullname);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\$(\"#mr_branch_code\").val(res['data']['mr_branch_code']);
\t\t\t\t\t\t\t\$(\"#mr_branch_name\").val(res['data']['mr_branch_name']);
\t\t\t\t\t\t\t\$(\"#mr_branch_id\").val(res['data']['mr_branch_id']);
\t\t\t\t\t\t\t\$(\"#send_user_id\").val(res['data']['mr_user_id']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\t\$(\"#mr_branch_code\").val(\"\");
\t\t\t\t\t\t\t\$(\"#mr_branch_name\").val(\"\");
\t\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t\t\t\$(\"#mr_branch_id\").val(\"\");
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\$(\"#mr_branch_code\").val(\"\");
\t\t\t\t\t\t\$(\"#mr_branch_name\").val(\"\");
\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t\t\$(\"#mr_branch_id\").val(\"\");
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t\$(\"#name_receiver_select\").click(function(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\t
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#mr_emp_code\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};

\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});

\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\turl: url+\"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});

\$(\"#modal_click\").click(function(){
\t\t\t\$('#modal_showdata').modal({ backdrop: false})
\t\t})
\t\t
\t\t\$(\"#dataType_contact\").on('select2:select', function(e) {
\t\t\t \$('#tb_con').DataTable().clear().draw();
\t\t\t load_contact(\$(this).val());
\t\t});
\t\t
load_contact(3);\t\t

\$('#remark').keydown(function(e) {
        
\tvar newLines = \$(this).val().split(\"\\\\n\").length;
\t  //</link>linesUsed.text(newLines);
\t
\tif(e.keyCode == 13 &&  newLines >= 3) {
\t\t\t//linesUsed.css('color', 'red');
\t\t\treturn  false;
\t}
\telse {
\t\t\t//linesUsed.css('color', '');
\t}
});



\$('#branch_send').on('select2:select', function (e) {
    console.log(\$('#branch_send').val());
\t
\t

    var branch_id = \$('#branch_send').val();
\t\$.ajax({
\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\turl: url+'ajax/ajax_autocompress_chang_branch.php',
\t\ttype: 'POST',
\t\tdata: {
\t\t\tbranch_id: branch_id
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res) {
\t\t\tif(res.status == 200) {
\t\t\t\t\$('#branch_floor').html(res.data);
\t\t\t}
\t\t\t
\t\t}
\t})

});


";
    }

    // line 413
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 414
        echo "var url = \"https://www.pivot-services.com/mailroom_tmb/employee/\";
url = \"\";
\t\tfunction check_name_re(){
\t\t\t\$(\"#div_re_text\").hide();
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t if(pass_emp == \"\" || pass_emp == null){ 
\t\t\t\t//\$(\"#div_re_text\").hide();
\t\t\t\t//\$(\"#div_re_select\").show();
\t\t\t }else{
\t\t\t\t//\$(\"#div_re_text\").show();
\t\t\t\t//\$(\"#div_re_select\").hide();
\t\t\t }
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\t\turl: url+'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\t\$(\"#mr_branch_code\").val(res['data']['mr_branch_code']);
\t\t\t\t\t\t\t\t\$(\"#mr_branch_name\").val(res['data']['mr_branch_name']);
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#mr_branch_id\").val(res['data']['mr_branch_id']);
\t\t\t\t\t\t\t\t\$(\"#send_user_id\").val(res['data']['mr_user_id']);
\t\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);

\t\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t\tfunction name_re_chang(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\t//console.log(res);
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false){
\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#branchname\").val(res['data']['mr_branch_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\$(\"#send_user_id\").val(res['data']['mr_user_id']);
\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t}


\t\tvar saveInOut = function(obj) {
\t\t\t//return \$.post('https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_save_inout.php', obj);
\t\t\treturn \$.post(url+'ajax/ajax_save_workout.php', obj);
\t\t}


function getemp(emp_code){
\t\t\$(\"#pass_emp\").val(emp_code);
\t\t\$('#modal_showdata').modal('hide');
\t\t\$(\"#pass_emp\").keyup();
}\t

function load_contact(type){\t\t
\$.ajax({
   url : 'ajax/ajax_load_dataContact.php',
   dataType : 'json',
   type : 'POST',
   data : {
\t\t\t'type': type ,
   },
   success : function(data) {
\t   if(data.status == 200) {
\t\t   if(data.data != false) {
\t\t\t\$('#tb_con').DataTable().clear().draw();
\t\t\t\$('#tb_con').DataTable().rows.add(data.data).draw();
\t\t   }
\t   } else {
\t\t\$('#tb_con').DataTable().clear().draw();
\t   }
\t  
\t}
});
}
";
    }

    // line 543
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 544
        echo "
.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}
.dataTables_filter {
display: none; 
}

\t#btn_save:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#46A6FB;
\t\t}
\t
\t#btn_save{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tpadding:50px;
\t\t}
 
.valit_error{
\tborder:2px solid red;
\tborder-color: red;
\tborder-radius: 5px;
} 
\t\t
";
    }

    // line 627
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 628
        echo "
\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4 >ข้อมูลผู้รับเอกสาร(สาขา)</h4></label>
\t\t\t</div>\t
\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 634
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 635
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 635), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"mr_branch_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"floor_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"send_user_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"";
        // line 640
        echo twig_escape_filter($this->env, ($context["barcode"] ?? null), "html", null, true);
        echo "\">
\t\t\t

\t\t\t<div class=\"form-group\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tชื่อผู้รับ : 
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"div_re_select\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อผู้รับ <span style='color:red; font-size: 15px'>&#42;</span>:
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8 input-group\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t ";
        // line 666
        echo "\t\t\t\t\t\t</select><div class=\"input-group-append\">
\t\t\t\t\t\t\t<button id=\"modal_click\" style=\"height: 28px;\" class=\"btn btn-outline-info btn-sm\" type=\"button\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"รายชื่อผู้ที่เกี่ยวข้องของแต่ละหน่วยงาน\"><i class=\"material-icons\">
\t\t\t\t\t\t\t\tmore_horiz
\t\t\t\t\t\t\t\t</i></button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน <span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" readonly placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสสาขา :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_branch_code\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อสาขา :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_branch_name\" placeholder=\"ชื่อหน่วยงาน\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อเอกสาร<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" maxlength=\"150\" class=\"form-control form-control-sm\" id=\"topic\" placeholder=\"ชื่อเรื่อง, หัวข้อเอกสาร\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tจำนวน<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"number\" value=\"1\" class=\"form-control form-control-sm\" id=\"qty\" placeholder=\"จำนวนเอกสาร 1,2,3,...\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tสาขาผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t<option value=\"0\" > เลือกสาขาปลายทาง</option>
\t\t\t\t\t\t\t\t\t";
        // line 742
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 743
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 743), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_code", [], "any", false, false, false, 743), "html", null, true);
            echo ":";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 743), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 744
        echo "\t\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div><br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้นผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<textarea maxlength=\"150\" class=\"form-control form-control-sm\" id=\"remark\" placeholder=\"รายละเอียดของเอกสาร\"></textarea>
\t\t\t</div>
\t\t\t";
        // line 770
        if ((($context["tomorrow"] ?? null) == 1)) {
            // line 771
            echo "\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-12\" style=\"color:red;\">
\t\t\t\t\t\t\t<center>เอกสารที่ส่งหลัง 16.00 น. จะทำการส่งในวันถัดไป </center>
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>\t
\t\t\t";
        }
        // line 779
        echo "\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\"  >Save</button>
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

\t
<div class=\"modal fade\" id=\"modal_showdata\">
  <div class=\"modal-dialog modal-dialog modal-xl\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\">
\t\t\t<select id=\"dataType_contact\" class=\"form-control\" width=\"2000px\">
\t\t\t\t\t\t\t\t<option value=\"3\" selected> รายชื่อตามหน่วยงาน  </option>
\t\t\t\t\t\t\t\t<option value=\"2\"> รายชื่อตามสาขา </option>
\t\t\t\t\t\t\t\t<option value=\"1\"> รายชื่อติดต่อ อื่นๆ  </option>
\t\t\t  </select>
\t\t</h5><br>
\t\t\t
\t\t
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t  <div style=\"overflow-x:auto;\">
        <table class=\"table\" id=\"tb_con\">
\t\t\t  <thead>
\t\t\t\t<tr>
\t\t\t\t  <td><button type=\"button\" id=\"ClearFilter\" class=\"btn btn-secondary\">ClearFilter</button></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search1\"  placeholder=\"ชื่อหน่วยงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search2\"  placeholder=\"ชั้น\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search3\"  placeholder=\"ผู้รับงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search4\"  placeholder=\"รหัสพนักงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search5\"  placeholder=\"เบอร์โทร\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search6\"  placeholder=\"ตำแหน่ง\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search7\"  placeholder=\"หมายเหตุ\"></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <td>ลำดับ</td>
\t\t\t\t  <td>ชื่อหน่วยงาน</td>
\t\t\t\t  <td>ชั้น</td>
\t\t\t\t  <td>ผู้รับงาน</td>
\t\t\t\t  <td>รหัสพนักงาน</td>
\t\t\t\t  <td>เบอร์โทร</td>
\t\t\t\t  <td>ตำแหน่ง</td>
\t\t\t\t  <td>หมายเหตุ</td>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t  ";
        // line 833
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contactdata"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 834
            echo "\t\t\t\t<tr>
\t\t\t\t  <td scope=\"row\"><button type=\"button\" class=\"btn btn-link\" onclick=\"getemp('";
            // line 835
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_code", [], "any", false, false, false, 835), "html", null, true);
            echo "')\">เลือก</button></td>
\t\t\t\t  <td>";
            // line 836
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "department_code", [], "any", false, false, false, 836), "html", null, true);
            echo ":";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "department_name", [], "any", false, false, false, 836), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 837
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "floor", [], "any", false, false, false, 837), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 838
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_contact_name", [], "any", false, false, false, 838), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 839
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_code", [], "any", false, false, false, 839), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 840
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_tel", [], "any", false, false, false, 840), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 841
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_position_name", [], "any", false, false, false, 841), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 842
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "remark", [], "any", false, false, false, 842), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 845
        echo "\t\t\t  </tbody>
\t\t\t</table>
\t\t</div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>
";
    }

    // line 858
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 860
        if ((($context["debug"] ?? null) != "")) {
            // line 861
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 863
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/work_out.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1020 => 863,  1014 => 861,  1012 => 860,  1005 => 858,  991 => 845,  982 => 842,  978 => 841,  974 => 840,  970 => 839,  966 => 838,  962 => 837,  956 => 836,  952 => 835,  949 => 834,  945 => 833,  889 => 779,  879 => 771,  877 => 770,  849 => 744,  836 => 743,  832 => 742,  754 => 666,  729 => 640,  721 => 635,  717 => 634,  709 => 628,  705 => 627,  620 => 544,  616 => 543,  485 => 414,  481 => 413,  82 => 17,  78 => 16,  71 => 9,  67 => 8,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}



{% block domReady %}

\t\tcheck_name_re();
\t\t\$(\"#dataType_contact\").select2({ width: '100%' });
\t\t
\t\t
\t
var tb_con = \$('#tb_con').DataTable({ 
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'department'},
        {'data': 'floor'},
        {'data': 'mr_contact_name'},
        {'data': 'emp_code'},
        {'data': 'emp_tel'},
        {'data': 'mr_position_name'},
        {'data': 'remark'}
    ]
});


\$('#ClearFilter').on( 'click', function () {
\t\$('#search1').val('');
\t\$('#search2').val('');
\t\$('#search3').val('');
\t\$('#search4').val('');
\t\$('#search5').val('');
\t\$('#search6').val('');
\t\$('#search7').val('');
\ttb_con.search( '' ).columns().search( '' ).draw();
});

\$('#search1').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(1).search(this.value, true, false).draw();
});
\$('#search2').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(2).search(this.value, true, false).draw();
});
\$('#search3').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(3).search(this.value, true, false).draw();
});
\$('#search4').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(4).search(this.value, true, false).draw();
});
\$('#search5').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(5).search(this.value, true, false).draw();
});
\$('#search6').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(6).search(this.value, true, false).draw();
});
\$('#search7').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(7).search(this.value, true, false).draw();
});


\t\t\$(\"#btn_save\").click(function(){
\t\t\t\t\$( \".myErrorClass\" ).removeClass( \"myErrorClass\" );
\t\t\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\t\t\tvar name_receiver \t\t= \$(\"#name_receiver\").val();
\t\t\t\t\tvar pass_depart \t\t= \$(\"#pass_depart\").val();
\t\t\t\t\tvar floor \t\t\t\t= \$(\"#floor\").val();
\t\t\t\t\tvar depart \t\t\t\t= \$(\"#depart\").val();
\t\t\t\t\tvar remark \t\t\t\t= \$(\"#remark\").val();
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar floor_send \t\t\t= \$(\"#floor_send\").val();
\t\t\t\t\tvar floor_id \t\t\t= \$(\"#floor_id\").val();
\t\t\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic\").val();
\t\t\t\t\tvar mr_branch_id \t\t= \$(\"#mr_branch_id\").val();
\t\t\t\t\tvar branch_send \t\t= \$(\"#branch_send\").val();
\t\t\t\t\tvar branch_floor \t\t= \$(\"#branch_floor\").val();
\t\t\t\t\tvar send_user_id \t\t= \$(\"#send_user_id\").val();
\t\t\t\t\tvar csrf_token\t \t\t\t= \$('#csrf_token').val();
\t\t\t\t\tvar qty\t \t\t\t= \$('#qty').val();
\t\t\t\t\tvar status \t\t\t\t= true;

\t\t\t\t\tvar data = \$('#branch_floor').select2('data')
\t\t\t\t\tvar branch_floor_name \t\t= '';
\t\t\t\t\tif(\$('#branch_floor').val()){
\t\t\t\t\t\tbranch_floor_name \t\t= data[0].text;
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(pass_emp == \"\" || pass_emp == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\$('#select2-name_receiver_select-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลผู้รับ!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\$('#topic').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\tif(mr_branch_id == \"\" || mr_branch_id == null || mr_branch_id == 0 ){
\t\t\t\t\t\t\tif(branch_send == 0 || branch_send == null){
\t\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\t\$('#select2-branch_send-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลสาขาของผู้รับ!');
\t\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\tif(branch_send == 1){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t//\$('#mr_branch_name').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\$('#select2-branch_send-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','หน้าต่างนี้สำหรับส่งที่สาขาเท่านั้น!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}else\tif(branch_send == \"\" || branch_send == null || branch_send == 0 ){
\t\t\t\t\t\t\tif(mr_branch_id == 1){
\t\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\t//\$('#mr_branch_name').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\t\$('#select2-branch_send-container').addClass( \"myErrorClass\" );
\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','หน้าต่างนี้สำหรับส่งที่สาขาเท่านั้น!');
\t\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(floor_id);
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tpass_emp: pass_emp,
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tfloor_send: floor_send,
\t\t\t\t\t\t\t\tfloor_id: floor_id,
\t\t\t\t\t\t\t\tmr_branch_id: mr_branch_id,
\t\t\t\t\t\t\t\tbranch_send: branch_send,
\t\t\t\t\t\t\t\tmr_branch_floor\t\t: branch_floor_name,
\t\t\t\t\t\t\t\tcsrf_token: csrf_token,
\t\t\t\t\t\t\t\tsend_user_id: send_user_id,
\t\t\t\t\t\t\t\tqty: qty,
\t\t\t\t\t\t\t\ttopic: topic
\t\t\t\t\t\t\t}


\t\t\t\t\t\t\talertify.confirm(\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง\", function() {

\t\t\t\t\t\t\t\t//\$.when(saveInOut(obj))
\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\turl: url+'ajax/ajax_save_workout.php',
\t\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\t\tdata: obj,
\t\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\t\t\t\t//return;
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\tif(res) {
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' + res['mr_work_barcode'] + '<b></font></h1>';
\t\t\t\t\t\t\t\t\t\t\talertify.confirm('เกิดข้อผิดพลาด','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg,
\t\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\t\t\t\t\t\t\talertify.success('print');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['encode'];
\t\t\t\t\t\t\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\t\tfunction() {
\t\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'ตกลง'});


\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\terror: function (error) {
\t\t\t\t\t\t\t\t\t alert('เกิดข้อผิดพลาด !!!');
\t\t\t\t\t\t\t\t\t window.location.reload();
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).setHeader('<h5> ยืนยันการบันทึกข้อมูล </h5> ');
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$('#name_receiver ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_depart ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#floor ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_emp ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#depart ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t}
\t\t});
\t\t\$(\"#pass_emp\").keyup(function(){
\t\t\tcheck_name_re();
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress2.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\tvar fullname = res['data']['mr_emp_name'] + \" \" + res['data']['mr_emp_lastname'];
\t\t\t\t\t\t\t\$(\"#name_receiver_select\").html('');
\t\t\t\t\t\t\tvar dataOption = {
\t\t\t\t\t\t\t\tid: res['data']['mr_emp_id'],
\t\t\t\t\t\t\t\ttext: res['data']['mr_emp_code'] + \" : \"+ res['data']['mr_emp_name'] + \" \" + res['data']['mr_emp_lastname']
\t\t\t\t\t\t\t};

\t\t\t\t\t\t\tvar newOption = new Option(dataOption.text, dataOption.id, false, false);
\t\t\t\t\t\t\t\$('#name_receiver_select').append(newOption).trigger('change');
\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};

\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(fullname);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\$(\"#mr_branch_code\").val(res['data']['mr_branch_code']);
\t\t\t\t\t\t\t\$(\"#mr_branch_name\").val(res['data']['mr_branch_name']);
\t\t\t\t\t\t\t\$(\"#mr_branch_id\").val(res['data']['mr_branch_id']);
\t\t\t\t\t\t\t\$(\"#send_user_id\").val(res['data']['mr_user_id']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\t\$(\"#mr_branch_code\").val(\"\");
\t\t\t\t\t\t\t\$(\"#mr_branch_name\").val(\"\");
\t\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t\t\t\$(\"#mr_branch_id\").val(\"\");
\t\t\t\t\t\t}
\t\t\t\t\t} else {
\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\$(\"#mr_branch_code\").val(\"\");
\t\t\t\t\t\t\$(\"#mr_branch_name\").val(\"\");
\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t\t\$(\"#mr_branch_id\").val(\"\");
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t\$(\"#name_receiver_select\").click(function(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\t
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#mr_emp_code\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};

\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});

\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\turl: url+\"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});

\$(\"#modal_click\").click(function(){
\t\t\t\$('#modal_showdata').modal({ backdrop: false})
\t\t})
\t\t
\t\t\$(\"#dataType_contact\").on('select2:select', function(e) {
\t\t\t \$('#tb_con').DataTable().clear().draw();
\t\t\t load_contact(\$(this).val());
\t\t});
\t\t
load_contact(3);\t\t

\$('#remark').keydown(function(e) {
        
\tvar newLines = \$(this).val().split(\"\\\\n\").length;
\t  //</link>linesUsed.text(newLines);
\t
\tif(e.keyCode == 13 &&  newLines >= 3) {
\t\t\t//linesUsed.css('color', 'red');
\t\t\treturn  false;
\t}
\telse {
\t\t\t//linesUsed.css('color', '');
\t}
});



\$('#branch_send').on('select2:select', function (e) {
    console.log(\$('#branch_send').val());
\t
\t

    var branch_id = \$('#branch_send').val();
\t\$.ajax({
\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\turl: url+'ajax/ajax_autocompress_chang_branch.php',
\t\ttype: 'POST',
\t\tdata: {
\t\t\tbranch_id: branch_id
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res) {
\t\t\tif(res.status == 200) {
\t\t\t\t\$('#branch_floor').html(res.data);
\t\t\t}
\t\t\t
\t\t}
\t})

});


{% endblock %}\t\t\t\t
{% block javaScript %}
var url = \"https://www.pivot-services.com/mailroom_tmb/employee/\";
url = \"\";
\t\tfunction check_name_re(){
\t\t\t\$(\"#div_re_text\").hide();
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t if(pass_emp == \"\" || pass_emp == null){ 
\t\t\t\t//\$(\"#div_re_text\").hide();
\t\t\t\t//\$(\"#div_re_select\").show();
\t\t\t }else{
\t\t\t\t//\$(\"#div_re_text\").show();
\t\t\t\t//\$(\"#div_re_select\").hide();
\t\t\t }
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\t\turl: url+'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\t\$(\"#mr_branch_code\").val(res['data']['mr_branch_code']);
\t\t\t\t\t\t\t\t\$(\"#mr_branch_name\").val(res['data']['mr_branch_name']);
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#mr_branch_id\").val(res['data']['mr_branch_id']);
\t\t\t\t\t\t\t\t\$(\"#send_user_id\").val(res['data']['mr_user_id']);
\t\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);

\t\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t\tfunction name_re_chang(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\t//console.log(res);
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false){
\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#branchname\").val(res['data']['mr_branch_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\$(\"#send_user_id\").val(res['data']['mr_user_id']);
\t\t\t\t\t\t\tvar data = {
\t\t\t\t\t\t\t\t\tid: res['data']['mr_floor_id'],
\t\t\t\t\t\t\t\t\ttext: res['data']['mr_department_floor']
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\tif(res['data']['mr_floor_id']){
\t\t\t\t\t\t\t\tvar newOption = new Option(data.text, data.id, false, false);
\t\t\t\t\t\t\t\t\$('#branch_floor').append(newOption).trigger('change');
\t\t\t\t\t\t\t\t\$('#branch_floor').val(res['data']['mr_floor_id']).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t}


\t\tvar saveInOut = function(obj) {
\t\t\t//return \$.post('https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_save_inout.php', obj);
\t\t\treturn \$.post(url+'ajax/ajax_save_workout.php', obj);
\t\t}


function getemp(emp_code){
\t\t\$(\"#pass_emp\").val(emp_code);
\t\t\$('#modal_showdata').modal('hide');
\t\t\$(\"#pass_emp\").keyup();
}\t

function load_contact(type){\t\t
\$.ajax({
   url : 'ajax/ajax_load_dataContact.php',
   dataType : 'json',
   type : 'POST',
   data : {
\t\t\t'type': type ,
   },
   success : function(data) {
\t   if(data.status == 200) {
\t\t   if(data.data != false) {
\t\t\t\$('#tb_con').DataTable().clear().draw();
\t\t\t\$('#tb_con').DataTable().rows.add(data.data).draw();
\t\t   }
\t   } else {
\t\t\$('#tb_con').DataTable().clear().draw();
\t   }
\t  
\t}
});
}
{% endblock %}\t\t

{% block styleReady %}

.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}
.dataTables_filter {
display: none; 
}

\t#btn_save:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#46A6FB;
\t\t}
\t
\t#btn_save{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tpadding:50px;
\t\t}
 
.valit_error{
\tborder:2px solid red;
\tborder-color: red;
\tborder-radius: 5px;
} 
\t\t
{% endblock %}\t\t\t\t\t
\t\t\t\t
{% block Content %}

\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4 >ข้อมูลผู้รับเอกสาร(สาขา)</h4></label>
\t\t\t</div>\t
\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\">
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"mr_branch_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"floor_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"send_user_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"{{ barcode }}\">
\t\t\t

\t\t\t<div class=\"form-group\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tชื่อผู้รับ : 
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"div_re_select\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อผู้รับ <span style='color:red; font-size: 15px'>&#42;</span>:
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8 input-group\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t {# < option value = \"0\" > ค้นหาผู้รับ < /option>
\t\t\t\t\t\t\t {% for e in emp_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ e.mr_emp_id }}\" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t {% endfor %} #}
\t\t\t\t\t\t</select><div class=\"input-group-append\">
\t\t\t\t\t\t\t<button id=\"modal_click\" style=\"height: 28px;\" class=\"btn btn-outline-info btn-sm\" type=\"button\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"รายชื่อผู้ที่เกี่ยวข้องของแต่ละหน่วยงาน\"><i class=\"material-icons\">
\t\t\t\t\t\t\t\tmore_horiz
\t\t\t\t\t\t\t\t</i></button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน <span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" readonly placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสสาขา :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_branch_code\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อสาขา :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_branch_name\" placeholder=\"ชื่อหน่วยงาน\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อเอกสาร<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" maxlength=\"150\" class=\"form-control form-control-sm\" id=\"topic\" placeholder=\"ชื่อเรื่อง, หัวข้อเอกสาร\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tจำนวน<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"number\" value=\"1\" class=\"form-control form-control-sm\" id=\"qty\" placeholder=\"จำนวนเอกสาร 1,2,3,...\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tสาขาผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t<option value=\"0\" > เลือกสาขาปลายทาง</option>
\t\t\t\t\t\t\t\t\t{% for b in branch_data %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{ b.mr_branch_id }}\" > {{ b.mr_branch_code }}:{{ b.mr_branch_name }}</option>
\t\t\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div><br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้นผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<textarea maxlength=\"150\" class=\"form-control form-control-sm\" id=\"remark\" placeholder=\"รายละเอียดของเอกสาร\"></textarea>
\t\t\t</div>
\t\t\t{% if tomorrow == 1 %}
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-12\" style=\"color:red;\">
\t\t\t\t\t\t\t<center>เอกสารที่ส่งหลัง 16.00 น. จะทำการส่งในวันถัดไป </center>
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>\t
\t\t\t{% endif %}
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\"  >Save</button>
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

\t
<div class=\"modal fade\" id=\"modal_showdata\">
  <div class=\"modal-dialog modal-dialog modal-xl\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\">
\t\t\t<select id=\"dataType_contact\" class=\"form-control\" width=\"2000px\">
\t\t\t\t\t\t\t\t<option value=\"3\" selected> รายชื่อตามหน่วยงาน  </option>
\t\t\t\t\t\t\t\t<option value=\"2\"> รายชื่อตามสาขา </option>
\t\t\t\t\t\t\t\t<option value=\"1\"> รายชื่อติดต่อ อื่นๆ  </option>
\t\t\t  </select>
\t\t</h5><br>
\t\t\t
\t\t
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t  <div style=\"overflow-x:auto;\">
        <table class=\"table\" id=\"tb_con\">
\t\t\t  <thead>
\t\t\t\t<tr>
\t\t\t\t  <td><button type=\"button\" id=\"ClearFilter\" class=\"btn btn-secondary\">ClearFilter</button></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search1\"  placeholder=\"ชื่อหน่วยงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search2\"  placeholder=\"ชั้น\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search3\"  placeholder=\"ผู้รับงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search4\"  placeholder=\"รหัสพนักงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search5\"  placeholder=\"เบอร์โทร\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search6\"  placeholder=\"ตำแหน่ง\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search7\"  placeholder=\"หมายเหตุ\"></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <td>ลำดับ</td>
\t\t\t\t  <td>ชื่อหน่วยงาน</td>
\t\t\t\t  <td>ชั้น</td>
\t\t\t\t  <td>ผู้รับงาน</td>
\t\t\t\t  <td>รหัสพนักงาน</td>
\t\t\t\t  <td>เบอร์โทร</td>
\t\t\t\t  <td>ตำแหน่ง</td>
\t\t\t\t  <td>หมายเหตุ</td>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t  {% for d in contactdata %}
\t\t\t\t<tr>
\t\t\t\t  <td scope=\"row\"><button type=\"button\" class=\"btn btn-link\" onclick=\"getemp('{{d.emp_code}}')\">เลือก</button></td>
\t\t\t\t  <td>{{d.department_code}}:{{d.department_name}}</td>
\t\t\t\t  <td>{{d.floor}}</td>
\t\t\t\t  <td>{{d.mr_contact_name}}</td>
\t\t\t\t  <td>{{d.emp_code}}</td>
\t\t\t\t  <td>{{d.emp_tel}}</td>
\t\t\t\t  <td>{{d.mr_position_name}}</td>
\t\t\t\t  <td>{{d.remark }}</td>
\t\t\t\t</tr>
\t\t\t\t{% endfor %}
\t\t\t  </tbody>
\t\t\t</table>
\t\t</div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/work_out.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\work_out.tpl");
    }
}

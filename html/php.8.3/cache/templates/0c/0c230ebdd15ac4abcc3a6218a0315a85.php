<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/login.tpl */
class __TwigTemplate_5e2265fc460d5c929801d6ccb59f9f23 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javascript' => [$this, 'block_javascript'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_login.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_login.tpl", "user/login.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot  - User Login";
    }

    // line 5
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo " #centered {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

 #img_logo {
   width: 100%;
   height: auto;
 }

 .btn, .form-control {
   border-radius: 0px;
   margin: 5px 0px;
 }

 .form-control {
   background-color: #f3f4f9;
   font-weight: bold;
   width: auto;
 }

 #btn-login{
   border-radius: 0px;
 }

#tste_id{
   color:red;
   font-size: 70px;
   position: absolute;
   top: -30px;
   left: -50px;
   -ms-transform: rotate(50deg); /* IE 9 */
  -webkit-transform: rotate(20deg); /* Safari 3-8 */
  transform: rotate(-30deg);
  
}

.txt-red{
  color:red;
  font-size: 30px;
}
@media screen and(max-width: 1080) {
 .form-control { 
    width: auto;
  }

}
";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "
\t\$(\"#re_password\").click(function(){
    
    // var password = prompt(\"กรุณากรอกรหัสพนักงานของท่าน\",\"\");

    // if (password != null || password != \"\") {
    //           \$.ajax({
    //                 url: \"./ajax/ajax_forget_password.php\",
    //                 type: 'POST',
    //                 data: {
    //                       emp_code: password
    //                 },
    //                 success: function (res) {
    //                         if (res == \"success\") {
    //                           alert(\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
    //                         } else {
    //                           alert(\"รหัสพนักงานของท่านไม่ถูกต้องค่ะ\");
    //                         }
    //                       }
    //                  });
    //   }

        alertify
              .prompt(\"ลืมรหัสผ่าน\",\"กรุณากรอกรหัสพนักงานของท่าน\",\"\", 
                function(ev,val) {
\t\t\t\t\tconsole.log(val);
                          //ev.preventDefault();
                        if(val != null || val != \"\") {
                                \$.ajax({
                                      url: \"./ajax/ajax_forget_password.php\",
                                      type: 'POST',
                                      data: {
                                            emp_code: val
                                      },
                                      success: function(res) {
                                        if (res == \"success\") {
                                                // alert(\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
                                                // alertify.alert(\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
                                                alertify.alert('success',\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
                                        } else {
                                                // alert(\"รหัสพนักงานของท่านไม่ถูกต้องค่ะ\");
                                                // alertify.alert(\"รหัสพนักงานของท่านไม่ถูกต้องค่ะ\");
                                                alertify.alert(\"เกิดข้อผิดพลาด\",\"กรุณาติดต่อห้องสารบัญกลาง\");
                                        }
                                      }
                                })
                        }
                      
                }, function() {
                       alertify.error('Cancel');
                });
\t\t});
";
    }

    // line 114
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 115
        echo "function logout_All(id){
\talertify.confirm('ยืนยันการออกจากระบบ', 'รหัสพนักงานนี้ได้ทำการเข้าใช้งานระบบอยู่แล้ว  ท่านต้องการออกจากระบบ', 
\t\tfunction(){ 
\t\t\t//alertify.success('Ok:'+id) ;
\t\t\t\$.ajax({
\t\t\t  method: \"POST\",
\t\t\t  dataType: \"json\",
\t\t\t  url: \"ajax/ajax_logout.php\",
\t\t\t  data: { 'user_id': id}
\t\t\t})
\t\t\t  .done(function( msg ) {
\t\t\t\t // window.location.reload();
\t\t\t\t if(msg['st']=='success'){
\t\t\t\t\t alertify.alert('สำเร็จ', 'กรุณาเข้าสู่ระบบ',function(){ 
\t\t\t\t\t\twindow.location.href='login.php';
\t\t\t\t\t });
\t\t\t\t }else{
\t\t\t\t   alertify.alert('เกิดข้อผิดพลาด', 'กรุณาลองใหม่อีกครั้ง!', function(){
\t\t\t\t\t   window.location.reload();
\t\t\t\t\t});
\t\t\t\t//console.log(msg);
\t\t\t\t }
\t\t\t
\t\t\t
\t\t\t
\t\t\t  })
\t\t},function(){ 
\t\t\talertify.error('Cancel'+id);
\t }).set('labels', {ok:'ออกจากระบบ!', cancel:'ยกเลิก'}); ;
}

";
    }

    // line 148
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 149
        echo "<div id='centered'>
  <!-- <div class='panel panel-default' id='login_tab'>
    <div class='panel-body'> -->
      <form class=\"form-signin\" action=\"login.php\" id=\"login\" method=\"post\" name=\"login\" autocomplete=\"off\">
        <center>
          <img src=\"../themes/images/logo-pv-ttb.png\" alt=\"logo\" id='img_logo'>
        </center>
        <br>
       

        <center>
         ";
        // line 161
        echo "          <b>เข้าระบบสมาชิก</b>
\t\t  <input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 162
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\">

          <label for=\"inputUsername\" class=\"sr-only\">รหัสพนักงาน</label>
          <input name=\"username\" type=\"username\" id=\"inputUsername\" class=\"form-control\" placeholder=\"รหัสพนักงาน\"  autocomplete=\"off\" required autofocus>
          <label for=\"inputPassword\" class=\"sr-only\">รหัสผ่าน</label>
          <input name=\"password\" type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"รหัสผ่าน\" required autocomplete=\"off\"></center>
          <div class=\"checkbox\">
            <label>
              <input type=\"checkbox\" value=\"remember-me\"> Remember me
            </label>
          </div>
        <label>
            <b style='color:red; width:auto;'>";
        // line 174
        echo ($context["error"] ?? null);
        echo "</b>
        </label>
        <input name=\"returnUrl\" type=\"hidden\" value=\"";
        // line 176
        echo twig_escape_filter($this->env, ($context["returnUrl"] ?? null), "html", null, true);
        echo "\" />
        <input name=\"action\" type=\"hidden\" value=\"save\" />
        <button class=\"btn btn-lg btn-primary btn-block btn-sm\" type=\"submit\" id='btn-login'><b>เข้าสู่ระบบ</b></button>
\t\t<br>
\t\t<center>
\t\t 
\t\t  <a href='register.php' ><span class='glyphicon glyphicon-user' ></span>&nbsp;ลงทะเบียน</a>&emsp; 
\t\t  <a href='#' id=\"re_password\"><span class='glyphicon glyphicon-repeat'></span>&nbsp;ลืมรหัสผ่าน กดที่นี่</a><br><br>
\t\t  <a href='./Doc.pdf' target=\"_blank\" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลดใบปะหน้าซอง</a><br><br>
\t\t  <a href='./TTBmailroom[HO-branch]V5.pdf.pdf' target=\"_blank\" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลดคู่มือการใช้งาน</a>
    </center>
\t\t
\t\t
\t\t
      </form>
     
\t\t\t

    </div>
  <!-- </div> -->

  
<!-- </div> -->
 

";
    }

    public function getTemplateName()
    {
        return "user/login.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 176,  248 => 174,  233 => 162,  230 => 161,  217 => 149,  213 => 148,  178 => 115,  174 => 114,  118 => 60,  114 => 59,  61 => 6,  57 => 5,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_login.tpl\" %}

{% block title %}Pivot  - User Login{% endblock %} 

{% block styleReady %}
 #centered {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

 #img_logo {
   width: 100%;
   height: auto;
 }

 .btn, .form-control {
   border-radius: 0px;
   margin: 5px 0px;
 }

 .form-control {
   background-color: #f3f4f9;
   font-weight: bold;
   width: auto;
 }

 #btn-login{
   border-radius: 0px;
 }

#tste_id{
   color:red;
   font-size: 70px;
   position: absolute;
   top: -30px;
   left: -50px;
   -ms-transform: rotate(50deg); /* IE 9 */
  -webkit-transform: rotate(20deg); /* Safari 3-8 */
  transform: rotate(-30deg);
  
}

.txt-red{
  color:red;
  font-size: 30px;
}
@media screen and(max-width: 1080) {
 .form-control { 
    width: auto;
  }

}
{% endblock %}



{% block domReady %}

\t\$(\"#re_password\").click(function(){
    
    // var password = prompt(\"กรุณากรอกรหัสพนักงานของท่าน\",\"\");

    // if (password != null || password != \"\") {
    //           \$.ajax({
    //                 url: \"./ajax/ajax_forget_password.php\",
    //                 type: 'POST',
    //                 data: {
    //                       emp_code: password
    //                 },
    //                 success: function (res) {
    //                         if (res == \"success\") {
    //                           alert(\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
    //                         } else {
    //                           alert(\"รหัสพนักงานของท่านไม่ถูกต้องค่ะ\");
    //                         }
    //                       }
    //                  });
    //   }

        alertify
              .prompt(\"ลืมรหัสผ่าน\",\"กรุณากรอกรหัสพนักงานของท่าน\",\"\", 
                function(ev,val) {
\t\t\t\t\tconsole.log(val);
                          //ev.preventDefault();
                        if(val != null || val != \"\") {
                                \$.ajax({
                                      url: \"./ajax/ajax_forget_password.php\",
                                      type: 'POST',
                                      data: {
                                            emp_code: val
                                      },
                                      success: function(res) {
                                        if (res == \"success\") {
                                                // alert(\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
                                                // alertify.alert(\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
                                                alertify.alert('success',\"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ\");
                                        } else {
                                                // alert(\"รหัสพนักงานของท่านไม่ถูกต้องค่ะ\");
                                                // alertify.alert(\"รหัสพนักงานของท่านไม่ถูกต้องค่ะ\");
                                                alertify.alert(\"เกิดข้อผิดพลาด\",\"กรุณาติดต่อห้องสารบัญกลาง\");
                                        }
                                      }
                                })
                        }
                      
                }, function() {
                       alertify.error('Cancel');
                });
\t\t});
{% endblock %}

{% block javascript %}
function logout_All(id){
\talertify.confirm('ยืนยันการออกจากระบบ', 'รหัสพนักงานนี้ได้ทำการเข้าใช้งานระบบอยู่แล้ว  ท่านต้องการออกจากระบบ', 
\t\tfunction(){ 
\t\t\t//alertify.success('Ok:'+id) ;
\t\t\t\$.ajax({
\t\t\t  method: \"POST\",
\t\t\t  dataType: \"json\",
\t\t\t  url: \"ajax/ajax_logout.php\",
\t\t\t  data: { 'user_id': id}
\t\t\t})
\t\t\t  .done(function( msg ) {
\t\t\t\t // window.location.reload();
\t\t\t\t if(msg['st']=='success'){
\t\t\t\t\t alertify.alert('สำเร็จ', 'กรุณาเข้าสู่ระบบ',function(){ 
\t\t\t\t\t\twindow.location.href='login.php';
\t\t\t\t\t });
\t\t\t\t }else{
\t\t\t\t   alertify.alert('เกิดข้อผิดพลาด', 'กรุณาลองใหม่อีกครั้ง!', function(){
\t\t\t\t\t   window.location.reload();
\t\t\t\t\t});
\t\t\t\t//console.log(msg);
\t\t\t\t }
\t\t\t
\t\t\t
\t\t\t
\t\t\t  })
\t\t},function(){ 
\t\t\talertify.error('Cancel'+id);
\t }).set('labels', {ok:'ออกจากระบบ!', cancel:'ยกเลิก'}); ;
}

{% endblock %}

{% block body %}
<div id='centered'>
  <!-- <div class='panel panel-default' id='login_tab'>
    <div class='panel-body'> -->
      <form class=\"form-signin\" action=\"login.php\" id=\"login\" method=\"post\" name=\"login\" autocomplete=\"off\">
        <center>
          <img src=\"../themes/images/logo-pv-ttb.png\" alt=\"logo\" id='img_logo'>
        </center>
        <br>
       

        <center>
         {#  <p class=\"txt-red\">ทดสอบระบบ</p> #}
          <b>เข้าระบบสมาชิก</b>
\t\t  <input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\">

          <label for=\"inputUsername\" class=\"sr-only\">รหัสพนักงาน</label>
          <input name=\"username\" type=\"username\" id=\"inputUsername\" class=\"form-control\" placeholder=\"รหัสพนักงาน\"  autocomplete=\"off\" required autofocus>
          <label for=\"inputPassword\" class=\"sr-only\">รหัสผ่าน</label>
          <input name=\"password\" type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"รหัสผ่าน\" required autocomplete=\"off\"></center>
          <div class=\"checkbox\">
            <label>
              <input type=\"checkbox\" value=\"remember-me\"> Remember me
            </label>
          </div>
        <label>
            <b style='color:red; width:auto;'>{{ error|raw }}</b>
        </label>
        <input name=\"returnUrl\" type=\"hidden\" value=\"{{ returnUrl }}\" />
        <input name=\"action\" type=\"hidden\" value=\"save\" />
        <button class=\"btn btn-lg btn-primary btn-block btn-sm\" type=\"submit\" id='btn-login'><b>เข้าสู่ระบบ</b></button>
\t\t<br>
\t\t<center>
\t\t 
\t\t  <a href='register.php' ><span class='glyphicon glyphicon-user' ></span>&nbsp;ลงทะเบียน</a>&emsp; 
\t\t  <a href='#' id=\"re_password\"><span class='glyphicon glyphicon-repeat'></span>&nbsp;ลืมรหัสผ่าน กดที่นี่</a><br><br>
\t\t  <a href='./Doc.pdf' target=\"_blank\" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลดใบปะหน้าซอง</a><br><br>
\t\t  <a href='./TTBmailroom[HO-branch]V5.pdf.pdf' target=\"_blank\" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลดคู่มือการใช้งาน</a>
    </center>
\t\t
\t\t
\t\t
      </form>
     
\t\t\t

    </div>
  <!-- </div> -->

  
<!-- </div> -->
 

{% endblock %}", "user/login.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\user\\login.tpl");
    }
}

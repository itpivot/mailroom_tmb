<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/create_work_post_out.tpl */
class __TwigTemplate_8c79624286378387b931eebe28689bcc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/create_work_post_out.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 29
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t

//console.log('55555');

\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_import').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: true
\t\t},
\t\t'cost_id': {
\t\t\trequired: true
\t\t},
\t\t'name_re': {
\t\t\trequired: true
\t\t},
\t\t'lname_re': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\t//required: true
\t\t},
\t\t'address_re': {
\t\t\trequired: true
\t\t},
\t\t'sub_district_re': {
\t\t\t//required: true
\t\t},
\t\t'district_re': {
\t\t\t//required: true
\t\t},
\t\t'province_re': {
\t\t\t//required: true
\t\t},
\t\t'post_code_re': {
\t\t\t//required: true
\t\t},
\t\t'topic': {
\t\t\t//required: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'weight': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'price': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'mr_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 1){
\t\t\t\t\t\treturn false;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn true;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t\t'sub_district_re': {
\t\t\trequired: true,
\t\t},
\t\t'district_re': {
\t\t\trequired: true,
\t\t},
\t\t'province_re': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 3 || mr_type_post_id == 5){
\t\t\t\t\t\treturn true;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn false;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'dep_id_send': {
\t\t  required: 'กรุณาระบุหน่วยงาน ผู้ส่ง'
\t\t},
\t\t'cost_id': {
\t\t  required: 'กรุณาระบุรหัสค่าใช้จ่าย'
\t\t},
\t\t'name_re': {
\t\t  required: 'กรุณาระบุ ชื่อผู้รับ'
\t\t},
\t\t'lname_re': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้รับ'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_re': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'sub_district_re': {
\t\t  //required: 'กรุณาระบุ แขวง/ตำบล'
\t\t},
\t\t'district_re': {
\t\t  //required: 'กรุณาระบุ เขต/อำเภอ'
\t\t},
\t\t'province_re': {
\t\t  //required: 'กรุณาระบุ จังหวัด'
\t\t},
\t\t'post_code_re': {
\t\t  //required: 'กรุณาระบุ รหัสไปรษณีย์'
\t\t},
\t\t'topic': {
\t\t  required: 'กรุณาระบุ หัวเรื่อง'
\t\t},
\t\t'mr_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'post_barcode': {
\t\t  required: 'กรุณาระบุ เลขที่เอกสาร'
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'price': {
\t\t  required: 'ไม่พบข้อมูลราคา',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'sub_district_re': {
\t\t\trequired:  'กรุณาระบุ แขวง/ตำบล:',
\t\t},
\t\t'district_re': {
\t\t\trequired:  'กรุณาระบุ เขต/อำเภอ:',
\t\t},
\t\t'province_re': {
\t\t\trequired:  'กรุณาระบุ จังหวัด:',
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  'กรุณาระบุ เลขตอบรับ :',
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });
  

\$('#myform_data_add_cost_center').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'add_cost_code': {
\t\t\trequired: true
\t\t},
\t\t'add_cost_name': {
\t\t\trequired: true
\t\t}
\t  
\t},
\tmessages: {
\t\t'add_cost_code': {
\t\t  required: 'กรุณาระบุ รหัสค่าใช้จ่าย.'
\t\t},
\t\t'add_cost_name': {
\t\t  required: 'กรุณาระบุ ชื่อ'
\t\t}
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });



\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});

\$('#dep_id_send').select2();
\$('#cost_id').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'mr_type_post_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'num_doc'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_cost_code'},
        {'data': 'mr_round_name'},
        {'data': 'name_resive'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'}
    ]
});
\$('#select-all').on('click', function(){
\t// Check/uncheck all checkboxes in the table
\tvar rows = tbl_data.rows({ 'search': 'applied' }).nodes();
\t\$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\"+res.mr_cost_id);
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#cost_id\").val(res.data.mr_cost_id).trigger('change');
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.name); // display the selected text
                \$('#lname_re').val(ui.item.lname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t\$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
\t\t\t\t\$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
\t\t\t\t\$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
\t\t\t\t\$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
\t\t\t\t\$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
\t\t\t\t\$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
\t\t\t\t\$('#post_code_re').val(ui.item.zipcode); // save selected id to input
\t\t\t\t\$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
\t\t
\t
  \$('#btn_add_cost').click(function() {
\t
\tif(\$('#myform_data_add_cost_center').valid()) {
\t \tvar form = \$('#myform_data_add_cost_center');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_cost.php\",
\t\t\tdata: serializeData+\"&page=add\",
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t //location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {

\t\t\t\$(\"#bg_loader\").hide();
\t\t\tsetTimeout(function(){ 
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').hide();
\t\t\t}, 3000);

\t\t\tif(res['status'] == '200'){
\t\t\t\t\$('#success').text(res['message']);
\t\t\t\t\$('#success').show();
\t\t\t\t\$('#error').hide();
\t\t\t}else{
\t\t\t\t\$('#error').text(res['message']);
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').show();
\t\t\t}
\t\t  });
     
\t}
});\t
\t\t
";
    }

    // line 583
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 584
        echo "
function load_data_bydate() {

\tvar form = \$('#form_print');
\tvar serializeData = form.serialize();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:serializeData,
\t\turl: \"ajax/ajax_load_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function reset_send_form() {
\t\$('input[name=\"type_send\"]').attr('checked', false);
\t\$('#round').val(\"\");
\t\$('#emp_id_send').val(\"\").trigger('change');
\t\$('#emp_send_data').val(\"\");
}

function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}
function reset_resive_form() {
\t\$(\"#name_re\").val(\"\");
\t\$(\"#lname_re\").val(\"\");
\t\$(\"#tel_re\").val(\"\");
\t\$(\"#address_re\").val(\"\");
\t\$(\"#sub_districts_code_re\").val(\"\");
\t\$(\"#districts_code_re\").val(\"\");
\t\$(\"#provinces_code_re\").val(\"\");
\t\$(\"#sub_district_re\").val(\"\");
\t\$(\"#district_re\").val(\"\");
\t\$(\"#province_re\").val(\"\");
\t\$(\"#post_code_re\").val(\"\");
\t\$(\"#quty\").val(\"1\");
\t\$(\"#topic\").val(\"\");
\t\$(\"#work_remark\").val(\"\");
}
function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\t//console.log(11);
\t\t\$(\"#form_print\").attr('action', 'print_peper_thai_post.php');
\t\t\$('#form_print').submit();
\t}else if(type == 2){
\t\t//console.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_peper_thai_post_sort_sender.php');
\t\t\$('#form_print').submit();
\t}else if(type == 3){
\t\t//console.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_peper_post_in_sort_resive.php');
\t\t\$('#form_print').submit();
\t}else{
\t\talert('----');
\t}

}

function chang_dep_id() {
\tvar dep = \$('#dep_id_send').select2('data'); 
\tvar emp = \$('#emp_id_send').select2('data'); 
\t
\tconsole.log(emp);
\tif(emp[0].id!=''){
\t\t\$(\"#emp_send_data\").val(emp[0].text+'\\\\n'+dep[0].text);
\t}else{
\t\t\$(\"#emp_send_data\").val(dep[0].text);
\t}
\t
}

function changPost_Type(type_id){
\t//console.log(type_id);
\tif(type_id == 3 || type_id == 5){
\t\t\$('#tr-post-barcode-re').show();
\t\tconsole.log(\"if ::\"+type_id);
\t}else{
\t\t\$('#tr-post-barcode-re').hide();
\t\tconsole.log(\"else ::\"+type_id);
\t}
\tsetPost_Price();
}
function setPost_Price(){
\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\tvar quty \t\t\t= \$('#quty').val();
\tvar weight \t\t\t= \$('#weight').val();
\tvar price \t\t\t= \$('#price').val();
\tvar total_price \t= \$('#totsl_price').val();

\tif(mr_type_post_id != '' && mr_type_post_id != null && weight != '' && weight != null){
\t\t//console.log('เข้า');
\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_get_post_price.php\",
\t\t\tdata :{
\t\t\t\tmr_type_post_id : mr_type_post_id,
\t\t\t\tquty \t\t \t: quty \t\t,
\t\t\t\tweight \t\t \t: weight \t\t,
\t\t\t\tprice \t\t \t: price \t\t,
\t\t\t\ttotal_price \t: total_price
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t\$('#total_price').val('');
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t  \$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 200){
\t\t\t\t\t\$('#price').val(res['data']['post_price']);
\t\t\t\t\t\$('#total_price').val(res['data']['totalprice']);
\t\t\t\t}else{
\t\t\t\t\t\$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t\t}
\t\t  });
\t}else{
\t\t//console.log('ไม่เข้า');
\t\t
\t\t\$('#price').val('');
\t\t\$('#totsl_price').val('');
\t}
\t
}

function import_excel() {
\t\$('#div_error').hide();\t
\tvar formData = new FormData();
\t
\tformData.append('file', \$('#file')[0].files[0]);
\tformData.append('csrf_token', \$('#csrf_token').val());
\tformData.append('date_import', \$('#date_import').val());
\tformData.append('import_round', \$('#import_round').val());
\tformData.append('page', 'Uploadfile');

\tif(\$('#file').val() == ''){
\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\$('#div_error').show();
\t\treturn;
\t}else{
\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\$('#div_error').show();
\t\t// console.log(extension);
\t\treturn;
\t }
\t}
\t\$.ajax({
\t\t   url : 'ajax/ajax_save_Work_post_out.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : formData,
\t\t   processData: false,  // tell jQuery not to process the data
\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t   success : function(res) {
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t}
\t\t\t   \$('#bg_loader').hide();
\t\t   }, beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t\t
\t\t\t},
\t\t\terror: function (error) {
\t\t\t\talert(\"เกิดข้อผิดหลาด\");
\t\t\t\t//location.reload();
\t\t\t}
\t\t});

}



function cancelwork() {
\tvar dataall = [];
\tvar tbl_data = \$('#tb_keyin').DataTable();
\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\tdataall.push(this.value);
\t});

\tif(dataall.length < 1){
\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกรายการ\"); 
\t\treturn;
\t}
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: newdataall,
\t\t\tpage\t:'cancle_multiple'
\t\t},
\t\turl: \"ajax/ajax_load_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
\t

}\t


function dowload_excel(){
\tvar r = confirm(\"ต้องการโหลดไฟล์!\");
\tif (r == true) {
\t\twindow.open(\"../themes/thai_post_template.xlsx\", \"_blank\");
\t} else {
\t\talertify.error('Cancel')
\t}
\t

}\t

";
    }

    // line 895
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 896
        echo "
<div  class=\"container-fluid\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ส่งออกไปรษณีย์ไทย</b></h3></label><br>
\t\t\t\t
\t\t   </div>\t
\t\t</div>
\t</div>
\t  
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t\t\t<li class=\"nav-item\">
\t\t\t\t  <a class=\"nav-link active\" id=\"key-tab\" data-toggle=\"tab\" href=\"#key\" role=\"tab\" aria-controls=\"key\" aria-selected=\"true\"><label> คีย์คำสั่งงาน > ส่งออกไปรษณีย์ไทย </label></a>
\t\t\t\t</li>
\t\t\t\t<li class=\"nav-item\">
\t\t\t\t  <a class=\"nav-link\" id=\"import-tab\" data-toggle=\"tab\" href=\"#import\" role=\"tab\" aria-controls=\"import\" aria-selected=\"false\"><label> import excel คำสั่งงาน > ส่งออกไปรษณีย์ไทย </label></a>
\t\t\t\t</li>
\t\t\t  </ul>
\t\t\t  <br>
\t\t\t  <br>
\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t<div class=\"tab-pane fade show active\" id=\"key\" role=\"tabpanel\" aria-labelledby=\"key-tab\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 934
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 944
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 945
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 945), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 945), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 947
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_send\" class=\"form-control form-control-sm\" id=\"emp_id_send\" name=\"emp_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">1</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\">2</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสค่าใช้จ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_cost\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_cost\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"cost_id\" name=\"cost_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 969
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cost"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 970
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_id", [], "any", false, false, false, 970), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_code", [], "any", false, false, false, 970), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_name", [], "any", false, false, false, 970), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 972
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\" id=\"basic-addon2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Button trigger modal -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button id=\"btn_add_cost_center\" type=\"button\" class=\"btn btn-sm btn-primary\" data-toggle=\"modal\" data-target=\"#add_cost_center\" data-keyboard=\"false\" data-backdrop=\"static\" title=\"เพิ่มข้อมูลพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t+
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >หน่วยงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_id_send\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"dep_id_send\" name=\"dep_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 990
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 991
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_id", [], "any", false, false, false, 991), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_code", [], "any", false, false, false, 991), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_name", [], "any", false, false, false, 991), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 993
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"emp_send_data\" name=\"emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t



\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้รับ</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการส่ง: </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"box_error\" id=\"err_mr_type_post_id\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select onchange=\"changPost_Type(\$(this).val());\"  id=\"mr_type_post_id\" name=\"mr_type_post_id\" data-error=\"#err_mr_type_post_id\" class=\"form-control form-control-sm\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกประเภทการส่ง</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 1029
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["type_post"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 1030
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["t"], "mr_type_post_id", [], "any", false, false, false, 1030), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["t"], "mr_type_post_name", [], "any", false, false, false, 1030), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1032
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_re\" id=\"name_re\" data-error=\"#err_name_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"ชื่อ นามสกุล\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์มือถือ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_re\" name=\"address_re\" data-error=\"#err_address_re\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"";
        // line 1059
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >เลขที่เอกสาร :</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode\" name=\"post_barcode\" data-error=\"#err_post_barcode\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr id=\"tr-post-barcode-re\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >เลขตอบรับ : </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode_re\" name=\"post_barcode_re\" data-error=\"#err_post_barcode_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >น้ำหนัก/กรัม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_weight\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"weight\" name=\"weight\" data-error=\"#err_weight\"  value=\"0\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\"  class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคา:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"price\" name=\"price\" data-error=\"#err_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\"  placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคารวม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_total_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"total_price\" name=\"total_price\" data-error=\"#err_total_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\"  placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t<!-- \t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t-->
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t</div><br>


\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ
\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 1157
        echo "
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"tab-pane fade\" id=\"import\" role=\"tabpanel\" aria-labelledby=\"import-tab\">
\t\t\t\t\t\t<form id=\"form_import_excel\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"><i class=\"material-icons\">help</i></a>
\t\t\t\t\t\t\t\t<label for=\"file\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group\" id=\"detail_receiver_head\">
\t\t\t\t\t\t\t\t\t\t<h4>Import File <button class=\"btn btn-link\" onclick=\"dowload_excel();\">Doowload Template</button></h4>  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</label><br>
\t\t\t\t\t\t\t\t<label for=\"date_import\">วันที่นำส่ง</label>
\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_import\"></span>
\t\t\t\t\t\t\t\t<input data-error=\"#err_date_import\" name=\"date_import\" id=\"date_import\" class=\"form-control form-control-sm col-md-2 c0l-12\" type=\"text\" value=\"";
        // line 1176
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_import_round\"></span>
\t\t\t\t\t\t\t\t\t<select data-error=\"#err_import_round\" class=\"form-control form-control-sm col-md-2 c0l-12\" id=\"import_round\" name=\"import_round\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t";
        // line 1182
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 1183
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 1183), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 1183), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1185
        echo "\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t\t\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 1192
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
\t</div>
\t

<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t";
        // line 1218
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 1219
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 1219), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 1219), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1221
        echo "\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 1226
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<div class=\"btn-group\" role=\"group\">
\t\t\t\t\t\t<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-sm btn-secondary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\tพิมพ์ใบงาน
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\">
\t\t\t\t\t\t\t<a onclick=\"print_option(1);\"  class=\"dropdown-item\" href=\"#\">แยกตามหน่วยงาน</a>
\t\t\t\t\t\t\t<a onclick=\"print_option(2);\"  class=\"dropdown-item\" href=\"#\">แยกตามผู้ส่ง</a>
\t\t\t\t\t\t\t<a onclick=\"print_option(3);\"  class=\"dropdown-item\" href=\"#\">แยกตามผู้รับ</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t<!-- \t\t\t\t
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t-->
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\"></span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t<button onclick=\"cancelwork();\" type=\"button\" class=\"btn btn-danger btn-sm\">
\t\t\t\t\t\tลบรายการ
\t\t\t\t\t</button>
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">ประเภท</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่ ปณ.</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รหัสค่าใช้จ่าย</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ผู้รับ</th>
\t\t\t\t<th width=\"15%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">เบอร์โทร</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">หมายเหตุ</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>



<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>







<!-- Modal -->
<div class=\"modal fade\" id=\"add_cost_center\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLabel\">เพิ่มข้อมูลรหัสค่าใช้จ่าย</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t<form id=\"myform_data_add_cost_center\">
\t\t\t
\t\t\t
\t\t\t<div class=\"form-row\">
\t\t\t\t
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_add_cost_code\"></span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_add_cost_name\"></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t<input id=\"add_cost_code\" name=\"add_cost_code\" data-error=\"#err_add_cost_code\"  type=\"text\" class=\"form-control form-control-sm\" placeholder=\"รหัสค่าใช้จ่าย\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-9\">
\t\t\t\t\t<input id=\"add_cost_name\" name=\"add_cost_name\" data-error=\"#err_add_cost_name\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"ชื่อ\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t</form>
\t\t
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
        <button id=\"btn_add_cost\" type=\"button\" class=\"btn btn-primary\">บันทึกข้อมูล</button>
\t\t\t 
      </div>
\t  <div class=\"form-row\">
\t\t<div class=\"col\">
\t\t\t<div id=\"error\" class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t  </div>
\t\t\t<div id=\"success\" class=\"alert alert-success\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t</div>
\t\t</div>
\t</div>
    </div>
  </div>
</div>





";
    }

    // line 1372
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 1374
        if ((($context["debug"] ?? null) != "")) {
            // line 1375
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 1377
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/create_work_post_out.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1581 => 1377,  1575 => 1375,  1573 => 1374,  1566 => 1372,  1416 => 1226,  1409 => 1221,  1398 => 1219,  1394 => 1218,  1365 => 1192,  1356 => 1185,  1345 => 1183,  1341 => 1182,  1330 => 1176,  1309 => 1157,  1212 => 1059,  1183 => 1032,  1172 => 1030,  1168 => 1029,  1130 => 993,  1117 => 991,  1113 => 990,  1093 => 972,  1080 => 970,  1076 => 969,  1052 => 947,  1041 => 945,  1037 => 944,  1022 => 934,  982 => 896,  978 => 895,  664 => 584,  660 => 583,  130 => 59,  99 => 30,  95 => 29,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

{% endblock %}

{% block domReady %}\t

//console.log('55555');

\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_import').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: true
\t\t},
\t\t'cost_id': {
\t\t\trequired: true
\t\t},
\t\t'name_re': {
\t\t\trequired: true
\t\t},
\t\t'lname_re': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\t//required: true
\t\t},
\t\t'address_re': {
\t\t\trequired: true
\t\t},
\t\t'sub_district_re': {
\t\t\t//required: true
\t\t},
\t\t'district_re': {
\t\t\t//required: true
\t\t},
\t\t'province_re': {
\t\t\t//required: true
\t\t},
\t\t'post_code_re': {
\t\t\t//required: true
\t\t},
\t\t'topic': {
\t\t\t//required: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'weight': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'price': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'mr_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 1){
\t\t\t\t\t\treturn false;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn true;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t\t'sub_district_re': {
\t\t\trequired: true,
\t\t},
\t\t'district_re': {
\t\t\trequired: true,
\t\t},
\t\t'province_re': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 3 || mr_type_post_id == 5){
\t\t\t\t\t\treturn true;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn false;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'dep_id_send': {
\t\t  required: 'กรุณาระบุหน่วยงาน ผู้ส่ง'
\t\t},
\t\t'cost_id': {
\t\t  required: 'กรุณาระบุรหัสค่าใช้จ่าย'
\t\t},
\t\t'name_re': {
\t\t  required: 'กรุณาระบุ ชื่อผู้รับ'
\t\t},
\t\t'lname_re': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้รับ'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_re': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'sub_district_re': {
\t\t  //required: 'กรุณาระบุ แขวง/ตำบล'
\t\t},
\t\t'district_re': {
\t\t  //required: 'กรุณาระบุ เขต/อำเภอ'
\t\t},
\t\t'province_re': {
\t\t  //required: 'กรุณาระบุ จังหวัด'
\t\t},
\t\t'post_code_re': {
\t\t  //required: 'กรุณาระบุ รหัสไปรษณีย์'
\t\t},
\t\t'topic': {
\t\t  required: 'กรุณาระบุ หัวเรื่อง'
\t\t},
\t\t'mr_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'post_barcode': {
\t\t  required: 'กรุณาระบุ เลขที่เอกสาร'
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'price': {
\t\t  required: 'ไม่พบข้อมูลราคา',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'sub_district_re': {
\t\t\trequired:  'กรุณาระบุ แขวง/ตำบล:',
\t\t},
\t\t'district_re': {
\t\t\trequired:  'กรุณาระบุ เขต/อำเภอ:',
\t\t},
\t\t'province_re': {
\t\t\trequired:  'กรุณาระบุ จังหวัด:',
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  'กรุณาระบุ เลขตอบรับ :',
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });
  

\$('#myform_data_add_cost_center').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'add_cost_code': {
\t\t\trequired: true
\t\t},
\t\t'add_cost_name': {
\t\t\trequired: true
\t\t}
\t  
\t},
\tmessages: {
\t\t'add_cost_code': {
\t\t  required: 'กรุณาระบุ รหัสค่าใช้จ่าย.'
\t\t},
\t\t'add_cost_name': {
\t\t  required: 'กรุณาระบุ ชื่อ'
\t\t}
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });



\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});

\$('#dep_id_send').select2();
\$('#cost_id').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'mr_type_post_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'num_doc'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_cost_code'},
        {'data': 'mr_round_name'},
        {'data': 'name_resive'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'}
    ]
});
\$('#select-all').on('click', function(){
\t// Check/uncheck all checkboxes in the table
\tvar rows = tbl_data.rows({ 'search': 'applied' }).nodes();
\t\$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\"+res.mr_cost_id);
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#cost_id\").val(res.data.mr_cost_id).trigger('change');
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.name); // display the selected text
                \$('#lname_re').val(ui.item.lname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t\$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
\t\t\t\t\$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
\t\t\t\t\$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
\t\t\t\t\$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
\t\t\t\t\$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
\t\t\t\t\$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
\t\t\t\t\$('#post_code_re').val(ui.item.zipcode); // save selected id to input
\t\t\t\t\$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
\t\t
\t
  \$('#btn_add_cost').click(function() {
\t
\tif(\$('#myform_data_add_cost_center').valid()) {
\t \tvar form = \$('#myform_data_add_cost_center');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_cost.php\",
\t\t\tdata: serializeData+\"&page=add\",
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t //location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {

\t\t\t\$(\"#bg_loader\").hide();
\t\t\tsetTimeout(function(){ 
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').hide();
\t\t\t}, 3000);

\t\t\tif(res['status'] == '200'){
\t\t\t\t\$('#success').text(res['message']);
\t\t\t\t\$('#success').show();
\t\t\t\t\$('#error').hide();
\t\t\t}else{
\t\t\t\t\$('#error').text(res['message']);
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').show();
\t\t\t}
\t\t  });
     
\t}
});\t
\t\t
{% endblock %}
{% block javaScript %}

function load_data_bydate() {

\tvar form = \$('#form_print');
\tvar serializeData = form.serialize();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:serializeData,
\t\turl: \"ajax/ajax_load_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function reset_send_form() {
\t\$('input[name=\"type_send\"]').attr('checked', false);
\t\$('#round').val(\"\");
\t\$('#emp_id_send').val(\"\").trigger('change');
\t\$('#emp_send_data').val(\"\");
}

function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}
function reset_resive_form() {
\t\$(\"#name_re\").val(\"\");
\t\$(\"#lname_re\").val(\"\");
\t\$(\"#tel_re\").val(\"\");
\t\$(\"#address_re\").val(\"\");
\t\$(\"#sub_districts_code_re\").val(\"\");
\t\$(\"#districts_code_re\").val(\"\");
\t\$(\"#provinces_code_re\").val(\"\");
\t\$(\"#sub_district_re\").val(\"\");
\t\$(\"#district_re\").val(\"\");
\t\$(\"#province_re\").val(\"\");
\t\$(\"#post_code_re\").val(\"\");
\t\$(\"#quty\").val(\"1\");
\t\$(\"#topic\").val(\"\");
\t\$(\"#work_remark\").val(\"\");
}
function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\t//console.log(11);
\t\t\$(\"#form_print\").attr('action', 'print_peper_thai_post.php');
\t\t\$('#form_print').submit();
\t}else if(type == 2){
\t\t//console.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_peper_thai_post_sort_sender.php');
\t\t\$('#form_print').submit();
\t}else if(type == 3){
\t\t//console.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_peper_post_in_sort_resive.php');
\t\t\$('#form_print').submit();
\t}else{
\t\talert('----');
\t}

}

function chang_dep_id() {
\tvar dep = \$('#dep_id_send').select2('data'); 
\tvar emp = \$('#emp_id_send').select2('data'); 
\t
\tconsole.log(emp);
\tif(emp[0].id!=''){
\t\t\$(\"#emp_send_data\").val(emp[0].text+'\\\\n'+dep[0].text);
\t}else{
\t\t\$(\"#emp_send_data\").val(dep[0].text);
\t}
\t
}

function changPost_Type(type_id){
\t//console.log(type_id);
\tif(type_id == 3 || type_id == 5){
\t\t\$('#tr-post-barcode-re').show();
\t\tconsole.log(\"if ::\"+type_id);
\t}else{
\t\t\$('#tr-post-barcode-re').hide();
\t\tconsole.log(\"else ::\"+type_id);
\t}
\tsetPost_Price();
}
function setPost_Price(){
\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\tvar quty \t\t\t= \$('#quty').val();
\tvar weight \t\t\t= \$('#weight').val();
\tvar price \t\t\t= \$('#price').val();
\tvar total_price \t= \$('#totsl_price').val();

\tif(mr_type_post_id != '' && mr_type_post_id != null && weight != '' && weight != null){
\t\t//console.log('เข้า');
\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_get_post_price.php\",
\t\t\tdata :{
\t\t\t\tmr_type_post_id : mr_type_post_id,
\t\t\t\tquty \t\t \t: quty \t\t,
\t\t\t\tweight \t\t \t: weight \t\t,
\t\t\t\tprice \t\t \t: price \t\t,
\t\t\t\ttotal_price \t: total_price
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t\$('#total_price').val('');
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t  \$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 200){
\t\t\t\t\t\$('#price').val(res['data']['post_price']);
\t\t\t\t\t\$('#total_price').val(res['data']['totalprice']);
\t\t\t\t}else{
\t\t\t\t\t\$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t\t}
\t\t  });
\t}else{
\t\t//console.log('ไม่เข้า');
\t\t
\t\t\$('#price').val('');
\t\t\$('#totsl_price').val('');
\t}
\t
}

function import_excel() {
\t\$('#div_error').hide();\t
\tvar formData = new FormData();
\t
\tformData.append('file', \$('#file')[0].files[0]);
\tformData.append('csrf_token', \$('#csrf_token').val());
\tformData.append('date_import', \$('#date_import').val());
\tformData.append('import_round', \$('#import_round').val());
\tformData.append('page', 'Uploadfile');

\tif(\$('#file').val() == ''){
\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\$('#div_error').show();
\t\treturn;
\t}else{
\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\$('#div_error').show();
\t\t// console.log(extension);
\t\treturn;
\t }
\t}
\t\$.ajax({
\t\t   url : 'ajax/ajax_save_Work_post_out.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : formData,
\t\t   processData: false,  // tell jQuery not to process the data
\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t   success : function(res) {
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t}
\t\t\t   \$('#bg_loader').hide();
\t\t   }, beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t\t
\t\t\t},
\t\t\terror: function (error) {
\t\t\t\talert(\"เกิดข้อผิดหลาด\");
\t\t\t\t//location.reload();
\t\t\t}
\t\t});

}



function cancelwork() {
\tvar dataall = [];
\tvar tbl_data = \$('#tb_keyin').DataTable();
\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\tdataall.push(this.value);
\t});

\tif(dataall.length < 1){
\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกรายการ\"); 
\t\treturn;
\t}
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: newdataall,
\t\t\tpage\t:'cancle_multiple'
\t\t},
\t\turl: \"ajax/ajax_load_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
\t

}\t


function dowload_excel(){
\tvar r = confirm(\"ต้องการโหลดไฟล์!\");
\tif (r == true) {
\t\twindow.open(\"../themes/thai_post_template.xlsx\", \"_blank\");
\t} else {
\t\talertify.error('Cancel')
\t}
\t

}\t

{% endblock %}
{% block Content2 %}

<div  class=\"container-fluid\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ส่งออกไปรษณีย์ไทย</b></h3></label><br>
\t\t\t\t
\t\t   </div>\t
\t\t</div>
\t</div>
\t  
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t\t\t<li class=\"nav-item\">
\t\t\t\t  <a class=\"nav-link active\" id=\"key-tab\" data-toggle=\"tab\" href=\"#key\" role=\"tab\" aria-controls=\"key\" aria-selected=\"true\"><label> คีย์คำสั่งงาน > ส่งออกไปรษณีย์ไทย </label></a>
\t\t\t\t</li>
\t\t\t\t<li class=\"nav-item\">
\t\t\t\t  <a class=\"nav-link\" id=\"import-tab\" data-toggle=\"tab\" href=\"#import\" role=\"tab\" aria-controls=\"import\" aria-selected=\"false\"><label> import excel คำสั่งงาน > ส่งออกไปรษณีย์ไทย </label></a>
\t\t\t\t</li>
\t\t\t  </ul>
\t\t\t  <br>
\t\t\t  <br>
\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t<div class=\"tab-pane fade show active\" id=\"key\" role=\"tabpanel\" aria-labelledby=\"key-tab\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_send\" class=\"form-control form-control-sm\" id=\"emp_id_send\" name=\"emp_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">1</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\">2</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสค่าใช้จ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_cost\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_cost\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"cost_id\" name=\"cost_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for c in cost %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ c.mr_cost_id }}\">{{ c.mr_cost_code }} - {{ c.mr_cost_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\" id=\"basic-addon2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Button trigger modal -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button id=\"btn_add_cost_center\" type=\"button\" class=\"btn btn-sm btn-primary\" data-toggle=\"modal\" data-target=\"#add_cost_center\" data-keyboard=\"false\" data-backdrop=\"static\" title=\"เพิ่มข้อมูลพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t+
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >หน่วยงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_id_send\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"dep_id_send\" name=\"dep_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for d in department %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ d.mr_department_id }}\">{{ d.mr_department_code }} - {{ d.mr_department_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"emp_send_data\" name=\"emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t



\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้รับ</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการส่ง: </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"box_error\" id=\"err_mr_type_post_id\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select onchange=\"changPost_Type(\$(this).val());\"  id=\"mr_type_post_id\" name=\"mr_type_post_id\" data-error=\"#err_mr_type_post_id\" class=\"form-control form-control-sm\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกประเภทการส่ง</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for t in type_post %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{t.mr_type_post_id}}\">{{t.mr_type_post_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_re\" id=\"name_re\" data-error=\"#err_name_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"ชื่อ นามสกุล\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์มือถือ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_re\" name=\"address_re\" data-error=\"#err_address_re\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"{{csrf_token}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >เลขที่เอกสาร :</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode\" name=\"post_barcode\" data-error=\"#err_post_barcode\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr id=\"tr-post-barcode-re\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >เลขตอบรับ : </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode_re\" name=\"post_barcode_re\" data-error=\"#err_post_barcode_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >น้ำหนัก/กรัม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_weight\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"weight\" name=\"weight\" data-error=\"#err_weight\"  value=\"0\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\"  class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคา:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"price\" name=\"price\" data-error=\"#err_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\"  placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคารวม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_total_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"total_price\" name=\"total_price\" data-error=\"#err_total_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\"  placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t<!-- \t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t-->
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t</div><br>


\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ
\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_send_form();\">ล้างข้อมูลผู้ส่ง </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_resive_form();\">ล้างข้อมูลผู้รับ </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t#}

\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"tab-pane fade\" id=\"import\" role=\"tabpanel\" aria-labelledby=\"import-tab\">
\t\t\t\t\t\t<form id=\"form_import_excel\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"><i class=\"material-icons\">help</i></a>
\t\t\t\t\t\t\t\t<label for=\"file\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group\" id=\"detail_receiver_head\">
\t\t\t\t\t\t\t\t\t\t<h4>Import File <button class=\"btn btn-link\" onclick=\"dowload_excel();\">Doowload Template</button></h4>  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</label><br>
\t\t\t\t\t\t\t\t<label for=\"date_import\">วันที่นำส่ง</label>
\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_import\"></span>
\t\t\t\t\t\t\t\t<input data-error=\"#err_date_import\" name=\"date_import\" id=\"date_import\" class=\"form-control form-control-sm col-md-2 c0l-12\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_import_round\"></span>
\t\t\t\t\t\t\t\t\t<select data-error=\"#err_import_round\" class=\"form-control form-control-sm col-md-2 c0l-12\" id=\"import_round\" name=\"import_round\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t\t\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\"></input>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
\t</div>
\t

<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<div class=\"btn-group\" role=\"group\">
\t\t\t\t\t\t<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-sm btn-secondary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\tพิมพ์ใบงาน
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\">
\t\t\t\t\t\t\t<a onclick=\"print_option(1);\"  class=\"dropdown-item\" href=\"#\">แยกตามหน่วยงาน</a>
\t\t\t\t\t\t\t<a onclick=\"print_option(2);\"  class=\"dropdown-item\" href=\"#\">แยกตามผู้ส่ง</a>
\t\t\t\t\t\t\t<a onclick=\"print_option(3);\"  class=\"dropdown-item\" href=\"#\">แยกตามผู้รับ</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t<!-- \t\t\t\t
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t-->
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\"></span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t<button onclick=\"cancelwork();\" type=\"button\" class=\"btn btn-danger btn-sm\">
\t\t\t\t\t\tลบรายการ
\t\t\t\t\t</button>
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">ประเภท</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่ ปณ.</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รหัสค่าใช้จ่าย</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ผู้รับ</th>
\t\t\t\t<th width=\"15%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">เบอร์โทร</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">หมายเหตุ</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>



<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>







<!-- Modal -->
<div class=\"modal fade\" id=\"add_cost_center\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLabel\">เพิ่มข้อมูลรหัสค่าใช้จ่าย</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t<form id=\"myform_data_add_cost_center\">
\t\t\t
\t\t\t
\t\t\t<div class=\"form-row\">
\t\t\t\t
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_add_cost_code\"></span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_add_cost_name\"></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t<input id=\"add_cost_code\" name=\"add_cost_code\" data-error=\"#err_add_cost_code\"  type=\"text\" class=\"form-control form-control-sm\" placeholder=\"รหัสค่าใช้จ่าย\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-9\">
\t\t\t\t\t<input id=\"add_cost_name\" name=\"add_cost_name\" data-error=\"#err_add_cost_name\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"ชื่อ\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t</form>
\t\t
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
        <button id=\"btn_add_cost\" type=\"button\" class=\"btn btn-primary\">บันทึกข้อมูล</button>
\t\t\t 
      </div>
\t  <div class=\"form-row\">
\t\t<div class=\"col\">
\t\t\t<div id=\"error\" class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t  </div>
\t\t\t<div id=\"success\" class=\"alert alert-success\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t</div>
\t\t</div>
\t</div>
    </div>
  </div>
</div>





{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/create_work_post_out.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/create_work_post_out.tpl");
    }
}

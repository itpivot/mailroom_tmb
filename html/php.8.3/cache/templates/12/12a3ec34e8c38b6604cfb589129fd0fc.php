<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/search_detail.tpl */
class __TwigTemplate_f56da9cd2e8d52255a0d12061c2a061d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/search_detail.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }

\t  #loader{
\t\twidth:100px;
\t\theight:100px;
\t\tdisplay:table;
\t\tmargin: auto;
\t\tborder-radius:50%;
\t  }#bg_loader{
\t\t  position:absolute;
\t\t  top:0px;
\t\t  background-color:rgba(255,255,255,0.7);
\t\t  height:100%;
\t\t  width:100%;
\t\t  z-index : 999;
\t  }
";
    }

    // line 48
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 57
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "\t  \$('#bg_loader').hide();
\t\t//var users_login = JSON.parse('";
        // line 59
        echo twig_escape_filter($this->env, ($context["user_arr"] ?? null), "html", null, true);
        echo "');
\t
\t\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
\t\t
\t\t
\t\t
var table = \$('#tb_work_order').DataTable({ 
    \"responsive\": true,
\t\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
\t\t{ 'data':'no' },
\t\t{ 'data':'time_test'},
\t\t{ 'data':'mr_work_barcode' },
\t\t{ 'data':'name_receive' },
\t\t{ 'data':'name_send' },
\t\t{ 'data':'mr_type_work_name' },
\t\t{ 'data':'mr_status_name' },
\t\t{ 'data':'mr_work_remark' },
\t\t{ 'data':'ch_data' }
    ]
});

\t\t\$('#select-all').on('click', function(){
\t\t   // Check/uncheck all checkboxes in the table
\t\t   var rows = table.rows({ 'search': 'applied' }).nodes();
\t\t   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
\t\t});
\t\t
\t\t<!-- Search 'csrf_token' : \$('#csrf_token').val() -->
\t\t\$(\"#btn_search\").click(function(){
\t\t\tvar barcode \t\t= \$(\"#barcode\").val();
\t\t\tvar pass_emp_send \t= \$(\"#pass_emp_send\").val();
\t\t\tvar pass_emp_re \t= \$(\"#pass_emp_re\").val();
\t\t\tvar start_date \t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t= \$(\"#end_date\").val();
\t\t\tvar status \t\t\t= \$(\"#status\").val();
\t\t\t
\t\t\t\$.ajax({
\t\t\t\tdataType: \"json\",
\t\t\t\tmethod: \"POST\",
\t\t\t\turl: \"ajax/ajax_search_work_mailroom.php\",
\t\t\t\tdata: { \t
\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'receiver': pass_emp_re,
\t\t\t\t\t\t'sender': pass_emp_send,
\t\t\t\t\t\t'start_date': start_date,
\t\t\t\t\t\t'end_date': end_date,
\t\t\t\t\t\t'csrf_token' : \$('#csrf_token').val(),
\t\t\t\t\t\t'status': status
\t\t\t\t  },
\t\t\t\t beforeSend: function( xhr ) {
\t\t\t\t\t  \$('#bg_loader').show();
\t\t\t\t  }
\t\t\t  }).done(function( msg ) {
\t\t\t\t\$('#csrf_token').val(msg.token);
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t  if(msg.status == 200){
\t\t\t\t\t  \$('#bg_loader').hide();
\t\t\t\t\t  \$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t  \$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t  }else{
\t\t\t\t\t  alertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\t\t\t  return;
\t\t\t\t  }
\t\t\t  });

\t\t\t});
\t\t\t<!-- end Search -->
\t\t
\t\t
\t\t
\t\t\$(\"#pass_emp_send\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_send\").val();
\t\t\t
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#name_sender\").val(res['data']['mr_emp_name']);
\t\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor_send\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#depart_send\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id_send\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t\$(\"#pass_emp_re\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_re\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(res['data']['mr_emp_name']);
\t\t\t\t\t\t\t\$(\"#pass_depart_re\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor_re\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#depart_re\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id_re\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t\t//setUsersFrom(users_login, 'send');
\t\t});

\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_depart_re\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\t\$(\"#floor_re\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t//\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\t\$(\"#depart_re\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\t//\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#pass_emp_re\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t\$('#name_send_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้ส่ง\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetFormSend(e.params.data);
\t\t\t//setUsersFrom(users_login, 're');
\t\t});

\t\tfunction setFormSend(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t//\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\t\$(\"#depart_send\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\t//\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#pass_emp_send\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t\t
\t\t
\t\t
\t\t\$(\"#btn_excel\").click(function() {
\t\t\tvar data = new Object();
\t\t\tdata['barcode'] \t\t\t= \$(\"#barcode\").val();
\t\t\tdata['sender'] \t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tdata['receiver'] \t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tdata['start_date'] \t\t\t= \$(\"#start_date\").val();
\t\t\tdata['end_date'] \t\t\t= \$(\"#end_date\").val();
\t\t\tdata['status'] \t\t\t\t= \$(\"#status\").val();
\t\t\tvar param = JSON.stringify(data);
\t\t
\t\t\twindow.open('excel.report.php?params='+param);
\t\t
\t\t});
\t\t
\t\tfunction setUsersFrom(users, type) {
\t\t\t\$(\"#pass_depart_\" + type).val(\"\");
\t\t\t\$(\"#floor_\" + type).val(\"\");
\t\t\t\$(\"#depart_\" + type).val(\"\");
\t\t\t\$(\"#pass_emp_\" + type).val(\"\");

\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: parseInt(users.mr_emp_id)
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function (res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\tvar users = {
\t\t\t\t\t\t\t\t\tid: parseInt(res.data.mr_emp_id),
\t\t\t\t\t\t\t\t\ttext: res.data.mr_emp_code+\" - \"+res.data.mr_emp_name+\" \"+res.data.mr_emp_lastname
\t\t\t\t\t\t\t\t};
\t\t
\t\t\t\t\t\t\t\tvar option = new Option(users.text, users.id, true, true);
\t\t\t\t\t\t\t\tif(type == \"send\") {
\t\t\t\t\t\t\t\t\t\$('#name_send_select').append(option).trigger('change');
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\t\$('#name_receiver_select').append(option).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\$(\"#pass_depart_\" + type).val(res.data.mr_department_code);
\t\t\t\t\t\t\t\t\$(\"#floor_\" + type).val(res.data.mr_department_floor);
\t\t\t\t\t\t\t\t\$(\"#depart_\" + type).val(res.data.mr_department_name);
\t\t\t\t\t\t\t\t\$(\"#pass_emp_\" + type).val(res.data.mr_emp_code);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t

\t\t\t\t\t}
\t\t\t});
\t\t}
\t\t
\t\t
\t\t
";
    }

    // line 348
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 349
        echo "\t
\t
\tfunction print_click() {
\t\t\t\tvar dataall = [];
\t

\t\t\t\tvar tel_receiver = \$('#tel_receiver').val();
\t\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t//  dataall.push(this.value);
\t\t\t\t
\t\t\t\t\tvar token = encodeURIComponent(window.btoa(this.value));
\t\t\t\t\t
\t\t\t\t\tdataall.push(token);
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }

\t\t\t\t
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");

\t\t\t\twindow.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t}\t

";
    }

    // line 380
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 381
        echo "
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-10\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ค้นหา</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<div class=\"row justify-content-between\">
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-left:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้ส่ง</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_send_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 403
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_send\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_send\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_sender\" placeholder=\"ชื่อผู้ส่ง\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_send\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_send\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_send\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-right:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้รับ</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 454
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_re\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_re\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_re\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_re\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_re\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:40px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 495
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t\t\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\"/>
\t\t\t\t\t\t\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">to</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\"/>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"barcode\" placeholder=\"Tracking number\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"status\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">สถานะงานทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งภายไนสำนักงานใหญ่\">
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 517
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["status_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 518
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_id", [], "any", false, false, false, 518), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_name", [], "any", false, false, false, 518), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 520
        echo "\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งที่สาขา\">
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 522
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["status_data2"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 523
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_id", [], "any", false, false, false, 523), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_name", [], "any", false, false, false, 523), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 525
        echo "\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_search\">ค้นหา</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-dark btn-block\" id=\"btn_excel\">Export Excel</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t
\t\t <div class=\"panel panel-default\" style=\"margin-top:50px;\">
                  <div class=\"panel-body\">
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date/Time</th>
                            <th>Bacode</th>
                            <th>Receiver</th>
                            <th>Sender</th>
                            <th>Type Work</th>
                            <th>Status</th>
                            <th>Remark</th>
                            <th>
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">ทั้งหมด</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<button type=\"button\" onclick=\"print_click();\" class=\"btn btn-outline-dark btn-sm\">&nbsp;&nbsp;&nbsp; print &nbsp;&nbsp;&nbsp;&nbsp;</button>
\t\t\t\t\t\t\t</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
\t
\t
\t\t\t\t<div id=\"bg_loader\" class=\"card\">
\t\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t\t</div>\t

";
    }

    // line 596
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 598
        if ((($context["debug"] ?? null) != "")) {
            // line 599
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 601
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/search_detail.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  726 => 601,  720 => 599,  718 => 598,  711 => 596,  639 => 525,  628 => 523,  624 => 522,  620 => 520,  609 => 518,  605 => 517,  580 => 495,  537 => 454,  488 => 403,  468 => 381,  464 => 380,  431 => 349,  427 => 348,  136 => 59,  133 => 58,  129 => 57,  119 => 49,  115 => 48,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }

\t  #loader{
\t\twidth:100px;
\t\theight:100px;
\t\tdisplay:table;
\t\tmargin: auto;
\t\tborder-radius:50%;
\t  }#bg_loader{
\t\t  position:absolute;
\t\t  top:0px;
\t\t  background-color:rgba(255,255,255,0.7);
\t\t  height:100%;
\t\t  width:100%;
\t\t  z-index : 999;
\t  }
{% endblock %}
{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}

{% block domReady %}
\t  \$('#bg_loader').hide();
\t\t//var users_login = JSON.parse('{{ user_arr }}');
\t
\t\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
\t\t
\t\t
\t\t
var table = \$('#tb_work_order').DataTable({ 
    \"responsive\": true,
\t\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
\t\t{ 'data':'no' },
\t\t{ 'data':'time_test'},
\t\t{ 'data':'mr_work_barcode' },
\t\t{ 'data':'name_receive' },
\t\t{ 'data':'name_send' },
\t\t{ 'data':'mr_type_work_name' },
\t\t{ 'data':'mr_status_name' },
\t\t{ 'data':'mr_work_remark' },
\t\t{ 'data':'ch_data' }
    ]
});

\t\t\$('#select-all').on('click', function(){
\t\t   // Check/uncheck all checkboxes in the table
\t\t   var rows = table.rows({ 'search': 'applied' }).nodes();
\t\t   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
\t\t});
\t\t
\t\t<!-- Search 'csrf_token' : \$('#csrf_token').val() -->
\t\t\$(\"#btn_search\").click(function(){
\t\t\tvar barcode \t\t= \$(\"#barcode\").val();
\t\t\tvar pass_emp_send \t= \$(\"#pass_emp_send\").val();
\t\t\tvar pass_emp_re \t= \$(\"#pass_emp_re\").val();
\t\t\tvar start_date \t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t= \$(\"#end_date\").val();
\t\t\tvar status \t\t\t= \$(\"#status\").val();
\t\t\t
\t\t\t\$.ajax({
\t\t\t\tdataType: \"json\",
\t\t\t\tmethod: \"POST\",
\t\t\t\turl: \"ajax/ajax_search_work_mailroom.php\",
\t\t\t\tdata: { \t
\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'receiver': pass_emp_re,
\t\t\t\t\t\t'sender': pass_emp_send,
\t\t\t\t\t\t'start_date': start_date,
\t\t\t\t\t\t'end_date': end_date,
\t\t\t\t\t\t'csrf_token' : \$('#csrf_token').val(),
\t\t\t\t\t\t'status': status
\t\t\t\t  },
\t\t\t\t beforeSend: function( xhr ) {
\t\t\t\t\t  \$('#bg_loader').show();
\t\t\t\t  }
\t\t\t  }).done(function( msg ) {
\t\t\t\t\$('#csrf_token').val(msg.token);
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t  if(msg.status == 200){
\t\t\t\t\t  \$('#bg_loader').hide();
\t\t\t\t\t  \$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t  \$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t  }else{
\t\t\t\t\t  alertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\t\t\t  return;
\t\t\t\t  }
\t\t\t  });

\t\t\t});
\t\t\t<!-- end Search -->
\t\t
\t\t
\t\t
\t\t\$(\"#pass_emp_send\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_send\").val();
\t\t\t
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#name_sender\").val(res['data']['mr_emp_name']);
\t\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor_send\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#depart_send\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id_send\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t\$(\"#pass_emp_re\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_re\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(res['data']['mr_emp_name']);
\t\t\t\t\t\t\t\$(\"#pass_depart_re\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor_re\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#depart_re\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id_re\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t\t//setUsersFrom(users_login, 'send');
\t\t});

\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_depart_re\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\t\$(\"#floor_re\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t//\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\t\$(\"#depart_re\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\t//\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#pass_emp_re\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t\$('#name_send_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้ส่ง\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetFormSend(e.params.data);
\t\t\t//setUsersFrom(users_login, 're');
\t\t});

\t\tfunction setFormSend(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t//\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\t\$(\"#depart_send\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\t//\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#pass_emp_send\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t\t
\t\t
\t\t
\t\t\$(\"#btn_excel\").click(function() {
\t\t\tvar data = new Object();
\t\t\tdata['barcode'] \t\t\t= \$(\"#barcode\").val();
\t\t\tdata['sender'] \t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tdata['receiver'] \t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tdata['start_date'] \t\t\t= \$(\"#start_date\").val();
\t\t\tdata['end_date'] \t\t\t= \$(\"#end_date\").val();
\t\t\tdata['status'] \t\t\t\t= \$(\"#status\").val();
\t\t\tvar param = JSON.stringify(data);
\t\t
\t\t\twindow.open('excel.report.php?params='+param);
\t\t
\t\t});
\t\t
\t\tfunction setUsersFrom(users, type) {
\t\t\t\$(\"#pass_depart_\" + type).val(\"\");
\t\t\t\$(\"#floor_\" + type).val(\"\");
\t\t\t\$(\"#depart_\" + type).val(\"\");
\t\t\t\$(\"#pass_emp_\" + type).val(\"\");

\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: parseInt(users.mr_emp_id)
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function (res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\tvar users = {
\t\t\t\t\t\t\t\t\tid: parseInt(res.data.mr_emp_id),
\t\t\t\t\t\t\t\t\ttext: res.data.mr_emp_code+\" - \"+res.data.mr_emp_name+\" \"+res.data.mr_emp_lastname
\t\t\t\t\t\t\t\t};
\t\t
\t\t\t\t\t\t\t\tvar option = new Option(users.text, users.id, true, true);
\t\t\t\t\t\t\t\tif(type == \"send\") {
\t\t\t\t\t\t\t\t\t\$('#name_send_select').append(option).trigger('change');
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\t\$('#name_receiver_select').append(option).trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\$(\"#pass_depart_\" + type).val(res.data.mr_department_code);
\t\t\t\t\t\t\t\t\$(\"#floor_\" + type).val(res.data.mr_department_floor);
\t\t\t\t\t\t\t\t\$(\"#depart_\" + type).val(res.data.mr_department_name);
\t\t\t\t\t\t\t\t\$(\"#pass_emp_\" + type).val(res.data.mr_emp_code);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t

\t\t\t\t\t}
\t\t\t});
\t\t}
\t\t
\t\t
\t\t
{% endblock %}


{% block javaScript %}
\t
\t
\tfunction print_click() {
\t\t\t\tvar dataall = [];
\t

\t\t\t\tvar tel_receiver = \$('#tel_receiver').val();
\t\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t//  dataall.push(this.value);
\t\t\t\t
\t\t\t\t\tvar token = encodeURIComponent(window.btoa(this.value));
\t\t\t\t\t
\t\t\t\t\tdataall.push(token);
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }

\t\t\t\t
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");

\t\t\t\twindow.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t}\t

{% endblock %}

{% block Content2 %}

\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-10\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ค้นหา</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<div class=\"row justify-content-between\">
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-left:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้ส่ง</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_send_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{# < option value = \"0\" > ค้นหาผู้ส่ง < /option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for e in emp_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ e.mr_emp_id }}\" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %} #}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_send\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_send\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_sender\" placeholder=\"ชื่อผู้ส่ง\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_send\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_send\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_send\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-right:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้รับ</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{# < option value = \"0\" > ค้นหาผู้รับ< /option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for e in emp_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ e.mr_emp_id }}\" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %} #}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_re\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_re\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_re\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_re\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_re\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:40px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\"></input>
\t\t\t\t\t\t\t\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\"/>
\t\t\t\t\t\t\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">to</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\"/>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"barcode\" placeholder=\"Tracking number\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"status\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">สถานะงานทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งภายไนสำนักงานใหญ่\">
\t\t\t\t\t\t\t\t\t\t\t\t{% for s in status_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_status_id }}\">{{ s.mr_status_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งที่สาขา\">
\t\t\t\t\t\t\t\t\t\t\t\t{% for s in status_data2 %}
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_status_id }}\">{{ s.mr_status_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_search\">ค้นหา</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-dark btn-block\" id=\"btn_excel\">Export Excel</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t
\t\t <div class=\"panel panel-default\" style=\"margin-top:50px;\">
                  <div class=\"panel-body\">
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date/Time</th>
                            <th>Bacode</th>
                            <th>Receiver</th>
                            <th>Sender</th>
                            <th>Type Work</th>
                            <th>Status</th>
                            <th>Remark</th>
                            <th>
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">ทั้งหมด</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<button type=\"button\" onclick=\"print_click();\" class=\"btn btn-outline-dark btn-sm\">&nbsp;&nbsp;&nbsp; print &nbsp;&nbsp;&nbsp;&nbsp;</button>
\t\t\t\t\t\t\t</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
\t
\t
\t\t\t\t<div id=\"bg_loader\" class=\"card\">
\t\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t\t</div>\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/search_detail.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\search_detail.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/receive_work.tpl */
class __TwigTemplate_5ae91fe78c16fcf5424d76e0567ee8ae extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_6' => [$this, 'block_menu_6'],
            'menu_7_maim' => [$this, 'block_menu_7_maim'],
            'menu_7_1' => [$this, 'block_menu_7_1'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/receive_work.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 3
    public function block_menu_6($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 4
    public function block_menu_7_maim($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 5
    public function block_menu_7_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 17
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "#btn_save:hover{
\tcolor: #FFFFFF;
\tbackground-color: #055d97;

}

#btn_save{
\tborder-color: #0074c0;
\tcolor: #FFFFFF;
\tbackground-color: #0074c0;
}

#detail_sender_head h4{
\ttext-align:left;
\tcolor: #006cb7;
\tborder-bottom: 3px solid #006cb7;
\tdisplay: inline;
}

#detail_sender_head {
\tborder-bottom: 3px solid #eee;
\tmargin-bottom: 20px;
\tmargin-top: 20px;
}
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
";
    }

    // line 58
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo twig_escape_filter($this->env, ($context["alertt"] ?? null), "html", null, true);
        echo "
\$('#barcode').keydown(function (e){
    if(e.keyCode == 13){
\t\tvar barcode =\$('#barcode').val();
\t\tvar tel_receiver = \$('#tel_receiver').val();
       \$.ajax({
\t\t\tdataType: \"json\",
\t\t\tmethod: \"POST\",
\t\t\turl: \"ajax/ajax_save_receive.php\",
\t\t\tdata: { 
\t\t\t\tbarcode: barcode, 
\t\t\t\ttel: tel_receiver ,
\t\t\t\tisEndcode  : 'yes' ,
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t
\t\t\tif(msg.status == 200){
\t\t\t\tloaddata();
\t\t\t\t\$( \"#succ\" ).html('รับเรียบร้อย');
\t\t\t\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\t\t\t}, 3000);
\t\t\t\t
\t\t\t}else{
\t\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t\t}
\t\t\t\$('#bg_loader').hide();
\t\t});
    }
})

var tbl_data = \$('#tableData').DataTable({ 
    \"responsive\": true,
\t \"scrollX\": true,
\t//\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'btn'},
        {'data': 'mr_status_name'},
        {'data': 'send_work_no'},
        {'data': 'send_name'},
        {'data': 'tnt_tracking_no'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
\t\t{'data': 'update_date'},
        {'data': 'action'}
    ]
});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
loaddata();
//\$('#bg_loader').hide();
";
    }

    // line 123
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 124
        echo "
function click_resave(barcode) {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar tbl_data = \$('#tableData').DataTable();

\tif(barcode == '' ){
\t\talertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\treturn;
\t}

\t\$.ajax({
\t\tdataType: \"json\",
\t\tmethod: \"POST\",
\t\turl: \"ajax/ajax_save_receive.php\",
\t\tdata: { 
\t\t\tbarcode: barcode, 
\t\t\ttel: tel_receiver 
\t\t},
\t\tbeforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\t
\t\tif(msg.status == 200){
\t\t\tlocation.href = \"rate_send.php?id=\"+msg.mr_work_main_id;
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}
\t\t\$('#bg_loader').hide();
\t});
}

function save_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar barcode = \$('#barcode').val();
\tvar tbl_data = \$('#tableData').DataTable();

\tif(barcode == '' ){
\t\talertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\treturn;
\t}

\t\$.ajax({
\tdataType: \"json\",
\tmethod: \"POST\",
\turl: \"ajax/ajax_save_receive.php\",
\tdata: { 
\t\tbarcode: barcode, 
\t\ttel: tel_receiver 
\t},
\tbeforeSend: function( xhr ) {
\t\t\$('#bg_loader').show();\t\t
\t},
\terror: function(xhr, error){
\t\talert('เกิดข้อผิดพลาด !');
\t\tlocation.reload();
\t}
\t}).done(function( msg ) {

\t\tif(msg.status == 200){
\t\t\tloaddata();
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}

\t\t\$('#bg_loader').hide();
\t});
}

function onseart_receive() {
\t\$.ajax({
\tdataType: \"json\",
\tmethod: \"POST\",
\turl: \"ajax/ajax_search_receive.php\",
\tdata: { \t
\t\tbarcode: \$('#seart_barcode').val(),
\t\tstatus: '' 
\t},beforeSend: function( xhr ) {
\t\t\$('#bg_loader').show();\t
\t},error: function(xhr, error){
\t\talert('เกิดข้อผิดพลาด !');
\t\tlocation.reload();
\t}
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tableData').DataTable().clear().draw();
\t\t\t\$('#tableData').DataTable().rows.add(msg.data).draw();
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}
\t});
}
function onseart() {
 var barcode = \$('#barcode').val();
 var ch_b = \$('#b_'+barcode).val();
 if(ch_b){
\t\$('#b_'+barcode).prop(\"checked\", true);
\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\t}, 3000);
 }else{
\t\$( \"#err\" ).fadeToggle( \"slow\", \"linear\" );
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#err\" ).fadeToggle( \"slow\", \"linear\" );
\t\t}, 3000);
 }

}
function loaddata() {
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_load_receive_work.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\t//location.reload();
\t }
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#bg_loader').show();
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tableData').DataTable().clear().draw();
\t\t\t\$('#tableData').DataTable().rows.add(msg.data).draw();
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}
\t  });
}
";
    }

    // line 264
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 265
        echo "
\t
\t\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>รายการเอกสาร</b></h3></label>
\t\t\t</div>\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4> ค้นหา </H4>
\t\t\t</div>
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t  <input type=\"text\" class=\"form-control mb-3\" id=\"seart_barcode\" placeholder=\"Barcode\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t\t<button onclick=\"onseart_receive();\" type=\"button\" class=\"btn btn-primary mb-2\">ค้นหา</button>
\t\t\t\t\t<button onclick=\"loaddata();\" type=\"button\" class=\"btn btn-secondary mb-2\">ยกเลิก</button>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4> รับเอกสาร </H4>
\t\t\t</div>
\t\t\t<div class=\"form-row align-items-center\">
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t  <input type=\"text\" class=\"form-control mb-3\" id=\"barcode\" placeholder=\"Enter Barcode\">
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t";
        // line 295
        echo "\t\t\t\t</div>
\t\t\t\t<div id=\"err\" class=\"col-md-12 alert alert-danger\" style=\"display:none;\"role=\"alert\">
\t\t\t\t ไม่พบข้อมูลงานที่กำลังนำส่ง
\t\t\t\t</div>
\t\t\t\t<div id=\"succ\"class=\"col-md-12 alert alert-success \" style=\"display:none;\" role=\"alert\">
\t\t\t\t ok!!
\t\t\t\t</div>
\t\t\t </div>
\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tableData\">
\t\t\t\t\t<thead class=\"\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>ลำดับ</th>
\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t<th>สถานะงาน</th>
\t\t\t\t\t\t<th>เลขที่เอกสาร</th>
\t\t\t\t\t\t<th>ชื่อผู้ส่ง</th>
\t\t\t\t\t\t<th>TNT</th>
\t\t\t\t\t\t<th>ชื่อผู้รับ</th>
\t\t\t\t\t\t<th>วันที่สร้างรายการ</th>
\t\t\t\t\t\t<th>วันที่แก้ไขล่าสุด</th>
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\tรับเอกสารแล้ว
\t\t\t\t\t\t</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t</div>
\t\t
\t\t<div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
\t\t<div id=\"bog_link\">
\t\t</div>\t\t
";
    }

    // line 360
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 362
        if ((($context["debug"] ?? null) != "")) {
            // line 363
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 365
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/receive_work.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  475 => 365,  469 => 363,  467 => 362,  460 => 360,  394 => 295,  365 => 265,  361 => 264,  218 => 124,  214 => 123,  146 => 59,  142 => 58,  101 => 18,  97 => 17,  87 => 8,  83 => 7,  76 => 5,  69 => 4,  62 => 3,  55 => 2,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}
{% block title %}Pivot- List{% endblock %}
{% block menu_6 %} active {% endblock %}
{% block menu_7_maim %} active {% endblock %}
{% block menu_7_1 %} active {% endblock %}

{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}


{% block styleReady %}
#btn_save:hover{
\tcolor: #FFFFFF;
\tbackground-color: #055d97;

}

#btn_save{
\tborder-color: #0074c0;
\tcolor: #FFFFFF;
\tbackground-color: #0074c0;
}

#detail_sender_head h4{
\ttext-align:left;
\tcolor: #006cb7;
\tborder-bottom: 3px solid #006cb7;
\tdisplay: inline;
}

#detail_sender_head {
\tborder-bottom: 3px solid #eee;
\tmargin-bottom: 20px;
\tmargin-top: 20px;
}
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
{% endblock %}


{% block domReady %}
{{alertt}}
\$('#barcode').keydown(function (e){
    if(e.keyCode == 13){
\t\tvar barcode =\$('#barcode').val();
\t\tvar tel_receiver = \$('#tel_receiver').val();
       \$.ajax({
\t\t\tdataType: \"json\",
\t\t\tmethod: \"POST\",
\t\t\turl: \"ajax/ajax_save_receive.php\",
\t\t\tdata: { 
\t\t\t\tbarcode: barcode, 
\t\t\t\ttel: tel_receiver ,
\t\t\t\tisEndcode  : 'yes' ,
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t
\t\t\tif(msg.status == 200){
\t\t\t\tloaddata();
\t\t\t\t\$( \"#succ\" ).html('รับเรียบร้อย');
\t\t\t\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\t\t\t}, 3000);
\t\t\t\t
\t\t\t}else{
\t\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t\t}
\t\t\t\$('#bg_loader').hide();
\t\t});
    }
})

var tbl_data = \$('#tableData').DataTable({ 
    \"responsive\": true,
\t \"scrollX\": true,
\t//\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'btn'},
        {'data': 'mr_status_name'},
        {'data': 'send_work_no'},
        {'data': 'send_name'},
        {'data': 'tnt_tracking_no'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
\t\t{'data': 'update_date'},
        {'data': 'action'}
    ]
});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
loaddata();
//\$('#bg_loader').hide();
{% endblock %}
{% block javaScript %}

function click_resave(barcode) {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar tbl_data = \$('#tableData').DataTable();

\tif(barcode == '' ){
\t\talertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\treturn;
\t}

\t\$.ajax({
\t\tdataType: \"json\",
\t\tmethod: \"POST\",
\t\turl: \"ajax/ajax_save_receive.php\",
\t\tdata: { 
\t\t\tbarcode: barcode, 
\t\t\ttel: tel_receiver 
\t\t},
\t\tbeforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\t
\t\tif(msg.status == 200){
\t\t\tlocation.href = \"rate_send.php?id=\"+msg.mr_work_main_id;
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}
\t\t\$('#bg_loader').hide();
\t});
}

function save_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar barcode = \$('#barcode').val();
\tvar tbl_data = \$('#tableData').DataTable();

\tif(barcode == '' ){
\t\talertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\treturn;
\t}

\t\$.ajax({
\tdataType: \"json\",
\tmethod: \"POST\",
\turl: \"ajax/ajax_save_receive.php\",
\tdata: { 
\t\tbarcode: barcode, 
\t\ttel: tel_receiver 
\t},
\tbeforeSend: function( xhr ) {
\t\t\$('#bg_loader').show();\t\t
\t},
\terror: function(xhr, error){
\t\talert('เกิดข้อผิดพลาด !');
\t\tlocation.reload();
\t}
\t}).done(function( msg ) {

\t\tif(msg.status == 200){
\t\t\tloaddata();
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}

\t\t\$('#bg_loader').hide();
\t});
}

function onseart_receive() {
\t\$.ajax({
\tdataType: \"json\",
\tmethod: \"POST\",
\turl: \"ajax/ajax_search_receive.php\",
\tdata: { \t
\t\tbarcode: \$('#seart_barcode').val(),
\t\tstatus: '' 
\t},beforeSend: function( xhr ) {
\t\t\$('#bg_loader').show();\t
\t},error: function(xhr, error){
\t\talert('เกิดข้อผิดพลาด !');
\t\tlocation.reload();
\t}
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tableData').DataTable().clear().draw();
\t\t\t\$('#tableData').DataTable().rows.add(msg.data).draw();
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}
\t});
}
function onseart() {
 var barcode = \$('#barcode').val();
 var ch_b = \$('#b_'+barcode).val();
 if(ch_b){
\t\$('#b_'+barcode).prop(\"checked\", true);
\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#succ\" ).fadeToggle( \"slow\", \"linear\" );
\t\t}, 3000);
 }else{
\t\$( \"#err\" ).fadeToggle( \"slow\", \"linear\" );
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#err\" ).fadeToggle( \"slow\", \"linear\" );
\t\t}, 3000);
 }

}
function loaddata() {
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_load_receive_work.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\t//location.reload();
\t }
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#bg_loader').show();
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tableData').DataTable().clear().draw();
\t\t\t\$('#tableData').DataTable().rows.add(msg.data).draw();
\t\t}else{
\t\t\talertify.alert('แจ้งเตือน',msg.message); 
\t\t}
\t  });
}
{% endblock %}\t
{% block Content %}

\t
\t\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>รายการเอกสาร</b></h3></label>
\t\t\t</div>\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4> ค้นหา </H4>
\t\t\t</div>
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t  <input type=\"text\" class=\"form-control mb-3\" id=\"seart_barcode\" placeholder=\"Barcode\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t\t<button onclick=\"onseart_receive();\" type=\"button\" class=\"btn btn-primary mb-2\">ค้นหา</button>
\t\t\t\t\t<button onclick=\"loaddata();\" type=\"button\" class=\"btn btn-secondary mb-2\">ยกเลิก</button>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4> รับเอกสาร </H4>
\t\t\t</div>
\t\t\t<div class=\"form-row align-items-center\">
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t  <input type=\"text\" class=\"form-control mb-3\" id=\"barcode\" placeholder=\"Enter Barcode\">
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t{#
\t\t\t\t  <button onclick=\"save_click();\" type=\"button\" class=\"btn btn-primary mb-2\">บันทึกรับ</button>
\t\t\t\t  #}
\t\t\t\t</div>
\t\t\t\t<div id=\"err\" class=\"col-md-12 alert alert-danger\" style=\"display:none;\"role=\"alert\">
\t\t\t\t ไม่พบข้อมูลงานที่กำลังนำส่ง
\t\t\t\t</div>
\t\t\t\t<div id=\"succ\"class=\"col-md-12 alert alert-success \" style=\"display:none;\" role=\"alert\">
\t\t\t\t ok!!
\t\t\t\t</div>
\t\t\t </div>
\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tableData\">
\t\t\t\t\t<thead class=\"\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>ลำดับ</th>
\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t<th>สถานะงาน</th>
\t\t\t\t\t\t<th>เลขที่เอกสาร</th>
\t\t\t\t\t\t<th>ชื่อผู้ส่ง</th>
\t\t\t\t\t\t<th>TNT</th>
\t\t\t\t\t\t<th>ชื่อผู้รับ</th>
\t\t\t\t\t\t<th>วันที่สร้างรายการ</th>
\t\t\t\t\t\t<th>วันที่แก้ไขล่าสุด</th>
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\tรับเอกสารแล้ว
\t\t\t\t\t\t</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t</div>
\t\t
\t\t<div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
\t\t<div id=\"bog_link\">
\t\t</div>\t\t
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "branch/receive_work.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\receive_work.tpl");
    }
}

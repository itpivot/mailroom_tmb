<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/search_follow.tpl */
class __TwigTemplate_e7e4cc836f40f14838471cd5eb474b63 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/search_follow.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "

.card {
       margin: 8px 0px;
    }

    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
    }
    
    .btn {
        border-radius: 0px;
    }
";
    }

    // line 34
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "\t\t\t\t

\tloadFollowEmp();
\t
\t
\t

    \$('#txt_search').on('keyup', function() {
        let txt = \$(this).val();
        let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_follow_work_emp.php\",
            type: \"POST\",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {\t
\t\t\t\t\t\t
\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\tstr += '<div class=\"card-body \">';
\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\tstr += '<b>ผู้รับ : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif( res[i]['mr_emp_tel'] == \"\" || res[i]['mr_emp_tel'] == null ){
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+'</a><br>';
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a>/ <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a><br>';
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tstr += '<b>แผนก : </b>';
\t\t\t\t\t\t\t\tstr += ''+res[i]['mr_department_name']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชั้น : </b>'+res[i]['floor']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>สถานะ : </b>'+res[i]['mr_status_name']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif ( res[i]['mr_status_name'] == 'Success' ){
\t\t\t\t\t\t\t\t\tstr += '<a href=\"detail_receive.php?barcode='+res[i]['mr_work_barcode']+'\" class=\"btn btn-info\"  >รายละเอียดการรับเอกสาร</a>';
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\t
                    \$('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    \$('#data_list').html(str);
                }
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
    });

\t
\t
\t
\t
\t
\t
\t
";
    }

    // line 109
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 110
        echo "function loadFollowEmp() {
     let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_follow_work_emp_curdate.php\",
            type: \"POST\",
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {\t
\t\t\t\t\t\t
\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\tstr += '<div class=\"card-body\">';
\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\tstr += '<b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif( res[i]['tel_re'] == \"\" || res[i]['tel_re'] == null ){
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+'</a><br>';
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a><br>';
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tstr += '<b>แผนก : </b>';
\t\t\t\t\t\t\t\tstr += ''+res[i]['mr_department_name']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชั้น : </b>'+res[i]['mr_department_floor']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>สถานะ : </b>'+res[i]['mr_status_name']+'<br>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tif ( res[i]['mr_status_name'] == 'Success' ){
\t\t\t\t\t\t\t\t\t\tstr += '<a href=\"detail_receive.php?barcode='+res[i]['mr_work_barcode']+'\" class=\"btn btn-info\"  >รายละเอียดการรับเอกสาร</a>';
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t}\t
\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\t
                    \$('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    \$('#data_list').html(str);
                }
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
    }
\t
\t
\t
\t
";
    }

    // line 170
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 171
        echo "<div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหาจาก Barcode, ชื่อ-สกุล, เบอร์ติดต่อ, วันที่\">
            </div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<center>จำนวนเอกสาร :  ";
        // line 179
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["count_curdate"] ?? null), "count_curday", [], "any", false, false, false, 179), "html", null, true);
        echo "  ฉบับ </center>
            </div>
        </form>
    </div>
\t<div id=\"data_list\">
\t</div>
\t
\t
\t

";
    }

    // line 192
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 194
        if ((($context["debug"] ?? null) != "")) {
            // line 195
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 197
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/search_follow.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 197,  268 => 195,  266 => 194,  259 => 192,  244 => 179,  234 => 171,  230 => 170,  168 => 110,  164 => 109,  87 => 35,  83 => 34,  54 => 5,  50 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}


{% block styleReady %}


.card {
       margin: 8px 0px;
    }

    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
    }
    
    .btn {
        border-radius: 0px;
    }
{% endblock %}



{% block domReady %}
\t\t\t\t

\tloadFollowEmp();
\t
\t
\t

    \$('#txt_search').on('keyup', function() {
        let txt = \$(this).val();
        let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_follow_work_emp.php\",
            type: \"POST\",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {\t
\t\t\t\t\t\t
\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\tstr += '<div class=\"card-body \">';
\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\tstr += '<b>ผู้รับ : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif( res[i]['mr_emp_tel'] == \"\" || res[i]['mr_emp_tel'] == null ){
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+'</a><br>';
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a>/ <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a><br>';
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tstr += '<b>แผนก : </b>';
\t\t\t\t\t\t\t\tstr += ''+res[i]['mr_department_name']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชั้น : </b>'+res[i]['floor']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>สถานะ : </b>'+res[i]['mr_status_name']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif ( res[i]['mr_status_name'] == 'Success' ){
\t\t\t\t\t\t\t\t\tstr += '<a href=\"detail_receive.php?barcode='+res[i]['mr_work_barcode']+'\" class=\"btn btn-info\"  >รายละเอียดการรับเอกสาร</a>';
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\t
                    \$('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    \$('#data_list').html(str);
                }
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
    });

\t
\t
\t
\t
\t
\t
\t
{% endblock %}
{% block javaScript %}
function loadFollowEmp() {
     let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_follow_work_emp_curdate.php\",
            type: \"POST\",
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {\t
\t\t\t\t\t\t
\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\tstr += '<div class=\"card-body\">';
\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\tstr += '<b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif( res[i]['tel_re'] == \"\" || res[i]['tel_re'] == null ){
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+'</a><br>';
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\tstr += '<b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a><br>';
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tstr += '<b>แผนก : </b>';
\t\t\t\t\t\t\t\tstr += ''+res[i]['mr_department_name']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชั้น : </b>'+res[i]['mr_department_floor']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>สถานะ : </b>'+res[i]['mr_status_name']+'<br>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tif ( res[i]['mr_status_name'] == 'Success' ){
\t\t\t\t\t\t\t\t\t\tstr += '<a href=\"detail_receive.php?barcode='+res[i]['mr_work_barcode']+'\" class=\"btn btn-info\"  >รายละเอียดการรับเอกสาร</a>';
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t}\t
\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\t
                    \$('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    \$('#data_list').html(str);
                }
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
    }
\t
\t
\t
\t
{% endblock %}

{% block Content %}
<div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหาจาก Barcode, ชื่อ-สกุล, เบอร์ติดต่อ, วันที่\">
            </div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<center>จำนวนเอกสาร :  {{ count_curdate.count_curday }}  ฉบับ </center>
            </div>
        </form>
    </div>
\t<div id=\"data_list\">
\t</div>
\t
\t
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "messenger/search_follow.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\messenger\\search_follow.tpl");
    }
}

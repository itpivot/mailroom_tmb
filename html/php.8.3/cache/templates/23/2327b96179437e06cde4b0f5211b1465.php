<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/rate_send.tpl */
class __TwigTemplate_7527b81b71ec007c82339f0052fd485b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/rate_send.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 4
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    
\t#img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1050;
    }
\t#pic_loading {
        width: 600px;
        height: auto;
    }
\t
\t
\t#rating_mess_0{
\t\tmargin: auto;
\t\twidth: 50%;
\t\t
\t}\t
\t
\t#rating_mess_5{
\t\tmargin: auto;
\t\twidth: 50%;
\t
\t}\t
\t

";
    }

    // line 35
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "
\t
";
    }

    // line 43
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "
\$('#rating_mess_0').on('click', function() {
\t\t\t
\t\t\tvar send_id \t\t\t\t\t= \$(\"#send_id\").val();
\t\t\tvar remark \t\t\t\t\t\t= \$(\"#remark\").val();
\t\t\tvar rating_mess \t\t\t\t= 0;

\t\t\t
\t\t//alert(remark);
           if ( remark == \"\" ) {
\t\t\t\talert(\"โปรดกรอกคำแนะนำเพิ่มเติม\");
\t\t\t\treturn;
\t\t   }else{
\t\t\t\t\$.ajax({
\t\t\t\t\turl: '../messenger/ajax/ajax_rate_send.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tid\t\t\t: send_id,
\t\t\t\t\t\trating_mess : rating_mess,
\t\t\t\t\t\tremark\t\t: remark,
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tcache: false,
\t\t\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\t\t\$('#img_loading').show();
\t\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\t//if(res == \"success\") {
\t\t\t\t\t\t\t//\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t//}
\t\t\t\t\t\t\tif(res['status'] == 200) {
\t\t\t\t\t\t\t\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\talertify.alert('แจ้งเตือน',res['message']); 
\t\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\tcomplete: function() {
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t\t}\t\t\t\t
                
        });
\t\t\t
\t\t\t
   \$('#rating_mess_5').on('click', function() {
\t\t\t
\t\t\tvar send_id \t\t\t\t\t= \$(\"#send_id\").val();
\t\t\tvar remark \t\t\t\t\t\t= \$(\"#remark\").val();
\t\t\tvar rating_mess \t\t\t\t= 5;
\t\t\t
\t\t\t
\t\t\t//alert(rating_mess);
\t\t\t\t \$.ajax({
\t\t\t\t\turl: '../messenger/ajax/ajax_rate_send.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tid\t\t\t: send_id,
\t\t\t\t\t\trating_mess : rating_mess,
\t\t\t\t\t\tremark\t\t: remark,
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tcache: false,
\t\t\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\t\t
\t\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t\t//if(res == \"success\") {
\t\t\t\t\t\t\t//\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t//}
\t\t\t\t\t\t\tif(res['status'] == 200) {
\t\t\t\t\t\t\t\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\talertify.alert('แจ้งเตือน',res['message']); 
\t\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\tcomplete: function() {
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t\t\t\t\t\t
                
            });
        
\t
";
    }

    // line 135
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 136
        echo "
\t\t<div class=\"page-header\">
             <h5> คุณพอใจต่อการบริการของเราในครั้งนี้หรือไม่</h5>
        </div>
\t\t<div class=\"card\">
\t\t\t<div class=\"card-body space-height\">
\t\t\t\t<input type=\"hidden\" id=\"send_id\" value=\"";
        // line 142
        echo twig_escape_filter($this->env, ($context["send_id"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t<h5>คำแนะนำเพิ่มเติม</h5>
\t\t\t\t<textarea style=\"height:50px;width:345px;margin-bottom:15px\" id=\"remark\" value=\"\"></textarea>\t

\t\t\t\t<br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-6\" align=\"center\" >
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-success\" id=\"rating_mess_5\" value=\"5\">
\t\t\t\t\t\t<img src=\"../themes/images/smiling.png\" height=\"42\" width=\"42\" />
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t\t<h5 style=\"margin-top:7px;\">พอใจ</h5>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-6\" align=\"center\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-warning\" id=\"rating_mess_0\" value=\"0\">
\t\t\t\t\t\t<img src=\"../themes/images/sad.png\" height=\"42\" width=\"42\" />
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<h5 style=\"margin-top:7px;\">ไม่พอใจ</h5>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>

";
    }

    // line 175
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 177
        if ((($context["debug"] ?? null) != "")) {
            // line 178
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 180
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/rate_send.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 180,  255 => 178,  253 => 177,  246 => 175,  211 => 142,  203 => 136,  199 => 135,  108 => 44,  104 => 43,  98 => 36,  94 => 35,  62 => 5,  58 => 4,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}
{% block styleReady %}
    
\t#img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1050;
    }
\t#pic_loading {
        width: 600px;
        height: auto;
    }
\t
\t
\t#rating_mess_0{
\t\tmargin: auto;
\t\twidth: 50%;
\t\t
\t}\t
\t
\t#rating_mess_5{
\t\tmargin: auto;
\t\twidth: 50%;
\t
\t}\t
\t

{% endblock %}

{% block scriptImport %}

\t
{% endblock %}




{% block domReady %}

\$('#rating_mess_0').on('click', function() {
\t\t\t
\t\t\tvar send_id \t\t\t\t\t= \$(\"#send_id\").val();
\t\t\tvar remark \t\t\t\t\t\t= \$(\"#remark\").val();
\t\t\tvar rating_mess \t\t\t\t= 0;

\t\t\t
\t\t//alert(remark);
           if ( remark == \"\" ) {
\t\t\t\talert(\"โปรดกรอกคำแนะนำเพิ่มเติม\");
\t\t\t\treturn;
\t\t   }else{
\t\t\t\t\$.ajax({
\t\t\t\t\turl: '../messenger/ajax/ajax_rate_send.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tid\t\t\t: send_id,
\t\t\t\t\t\trating_mess : rating_mess,
\t\t\t\t\t\tremark\t\t: remark,
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tcache: false,
\t\t\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\t\t\$('#img_loading').show();
\t\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\t//if(res == \"success\") {
\t\t\t\t\t\t\t//\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t//}
\t\t\t\t\t\t\tif(res['status'] == 200) {
\t\t\t\t\t\t\t\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\talertify.alert('แจ้งเตือน',res['message']); 
\t\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\tcomplete: function() {
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t\t}\t\t\t\t
                
        });
\t\t\t
\t\t\t
   \$('#rating_mess_5').on('click', function() {
\t\t\t
\t\t\tvar send_id \t\t\t\t\t= \$(\"#send_id\").val();
\t\t\tvar remark \t\t\t\t\t\t= \$(\"#remark\").val();
\t\t\tvar rating_mess \t\t\t\t= 5;
\t\t\t
\t\t\t
\t\t\t//alert(rating_mess);
\t\t\t\t \$.ajax({
\t\t\t\t\turl: '../messenger/ajax/ajax_rate_send.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tid\t\t\t: send_id,
\t\t\t\t\t\trating_mess : rating_mess,
\t\t\t\t\t\tremark\t\t: remark,
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tcache: false,
\t\t\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\t\t
\t\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t\t//if(res == \"success\") {
\t\t\t\t\t\t\t//\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t//}
\t\t\t\t\t\t\tif(res['status'] == 200) {
\t\t\t\t\t\t\t\twindow.location.href = \"../employee/receive_work.php\";
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\talertify.alert('แจ้งเตือน',res['message']); 
\t\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\tcomplete: function() {
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t\t\t\t\t\t
                
            });
        
\t
{% endblock %}



{% block Content %}

\t\t<div class=\"page-header\">
             <h5> คุณพอใจต่อการบริการของเราในครั้งนี้หรือไม่</h5>
        </div>
\t\t<div class=\"card\">
\t\t\t<div class=\"card-body space-height\">
\t\t\t\t<input type=\"hidden\" id=\"send_id\" value=\"{{ send_id }}\">
\t\t\t\t<h5>คำแนะนำเพิ่มเติม</h5>
\t\t\t\t<textarea style=\"height:50px;width:345px;margin-bottom:15px\" id=\"remark\" value=\"\"></textarea>\t

\t\t\t\t<br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-6\" align=\"center\" >
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-success\" id=\"rating_mess_5\" value=\"5\">
\t\t\t\t\t\t<img src=\"../themes/images/smiling.png\" height=\"42\" width=\"42\" />
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t\t<h5 style=\"margin-top:7px;\">พอใจ</h5>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-6\" align=\"center\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-warning\" id=\"rating_mess_0\" value=\"0\">
\t\t\t\t\t\t<img src=\"../themes/images/sad.png\" height=\"42\" width=\"42\" />
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<h5 style=\"margin-top:7px;\">ไม่พอใจ</h5>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/rate_send.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\rate_send.tpl");
    }
}

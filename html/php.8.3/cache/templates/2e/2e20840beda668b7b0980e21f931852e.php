<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/seting.round.tpl */
class __TwigTemplate_77a01fcba3426b54e824b89b76cf1228 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/seting.round.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 25
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_round_name'},
\t\t{'data': 'mr_type_work_name'},
\t\t{'data': 'sys_date'}
    ]
});
 \$('#tb_keyin').on('click', 'tr', function () {
        var data = tbl_data.row( this ).data();
        //alert( 'You clicked on '+data['mr_round_name']+'\\'s row' );
\t\t\$('#round_name').val(data['mr_round_name']);
\t\t\$('#type_work').val(data['mr_type_work_id']);
\t\t\$('#mr_round_id').val(data['mr_round_id']);
    } );

load_data_bydate();
function load_data_bydate() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_round.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}



";
    }

    // line 114
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 115
        echo "

function save_round() {
var type_work = \$('#type_work').val();
var round_name = \$('#round_name').val();
var mr_round_id = \$('#mr_round_id').val();

\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_round.php\",
\t\tdata:{
\t\t\tpage\t\t\t:\t'save',
\t\t\ttype_work\t\t:\ttype_work,
\t\t\tround_name\t\t:\tround_name,
\t\t\tmr_round_id\t\t:\tmr_round_id
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();
\t\t\t\$('#round_name').val('');
\t\t\t\$('#type_work').val('');
\t\t\t\$('#mr_round_id').val('');
\t\t}
\t  });
}


";
    }

    // line 156
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 157
        echo "
<div  class=\"container-fluid\">
<form id=\"myform_data_senderandresive\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>รอบ รับ-ส่ง เอกสาร</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"card\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการรอบ รับ-ส่ง เอกสารทั้งหมด</h5>
\t\t\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_keyin\">
\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\" colspan=\"2\">
\t\t\t\t\t\t\t\t\t\t<input id=\"round_name\" class=\"form-control\" type=\"text\" placeholder=\"ชื่อรอบ\">
\t\t\t\t\t\t\t\t\t\t<input id=\"mr_round_id\" type=\"hidden\">
\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control type_work\" id=\"type_work\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>ประเภท</option>
\t\t\t\t\t\t\t\t\t\t  ";
        // line 186
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["type_work"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 187
            echo "\t\t\t\t\t\t\t\t\t\t  <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_type_work_id", [], "any", false, false, false, 187), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_type_work_name", [], "any", false, false, false, 187), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 189
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\"><button type=\"button\" class=\"btn btn-success\" onclick=\"save_round()\">บันทึก</button></th>
\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ประเภท</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่ Update</th>
\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t  </table>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t

\t\t\t\t</div>
\t\t\t  </div>
\t\t
\t\t</div>
\t</div>
</form>
</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




";
    }

    // line 234
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 236
        if ((($context["debug"] ?? null) != "")) {
            // line 237
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 239
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/seting.round.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  350 => 239,  344 => 237,  342 => 236,  335 => 234,  289 => 189,  278 => 187,  274 => 186,  243 => 157,  239 => 156,  195 => 115,  191 => 114,  130 => 59,  95 => 26,  91 => 25,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}\t

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_round_name'},
\t\t{'data': 'mr_type_work_name'},
\t\t{'data': 'sys_date'}
    ]
});
 \$('#tb_keyin').on('click', 'tr', function () {
        var data = tbl_data.row( this ).data();
        //alert( 'You clicked on '+data['mr_round_name']+'\\'s row' );
\t\t\$('#round_name').val(data['mr_round_name']);
\t\t\$('#type_work').val(data['mr_type_work_id']);
\t\t\$('#mr_round_id').val(data['mr_round_id']);
    } );

load_data_bydate();
function load_data_bydate() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_round.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}



{% endblock %}
{% block javaScript %}


function save_round() {
var type_work = \$('#type_work').val();
var round_name = \$('#round_name').val();
var mr_round_id = \$('#mr_round_id').val();

\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_round.php\",
\t\tdata:{
\t\t\tpage\t\t\t:\t'save',
\t\t\ttype_work\t\t:\ttype_work,
\t\t\tround_name\t\t:\tround_name,
\t\t\tmr_round_id\t\t:\tmr_round_id
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();
\t\t\t\$('#round_name').val('');
\t\t\t\$('#type_work').val('');
\t\t\t\$('#mr_round_id').val('');
\t\t}
\t  });
}


{% endblock %}
{% block Content2 %}

<div  class=\"container-fluid\">
<form id=\"myform_data_senderandresive\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>รอบ รับ-ส่ง เอกสาร</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"card\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการรอบ รับ-ส่ง เอกสารทั้งหมด</h5>
\t\t\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_keyin\">
\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\" colspan=\"2\">
\t\t\t\t\t\t\t\t\t\t<input id=\"round_name\" class=\"form-control\" type=\"text\" placeholder=\"ชื่อรอบ\">
\t\t\t\t\t\t\t\t\t\t<input id=\"mr_round_id\" type=\"hidden\">
\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control type_work\" id=\"type_work\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>ประเภท</option>
\t\t\t\t\t\t\t\t\t\t  {% for type in type_work %}
\t\t\t\t\t\t\t\t\t\t  <option value=\"{{type.mr_type_work_id}}\">{{type.mr_type_work_name}}</option>
\t\t\t\t\t\t\t\t\t\t  {% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\"><button type=\"button\" class=\"btn btn-success\" onclick=\"save_round()\">บันทึก</button></th>
\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ประเภท</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่ Update</th>
\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t  </table>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t

\t\t\t\t</div>
\t\t\t  </div>
\t\t
\t\t</div>
\t</div>
</form>
</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/seting.round.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\seting.round.tpl");
    }
}

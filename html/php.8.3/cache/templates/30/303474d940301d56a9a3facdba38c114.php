<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/confirmSendBranch_no.tpl */
class __TwigTemplate_daa89db9f5c8021add91c3ba478ec42e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg3' => [$this, 'block_menu_msg3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/confirmSendBranch_no.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PivotSend List ";
    }

    // line 5
    public function block_menu_msg3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        height: 100%;
        
    }

    


    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 800px;
        overflow: auto;
        margin-bottom: 50px;
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    .fixedContainer {
        position: fixed;
        width: 100%;
        padding: 10px 10px;
        left: 0;
        bottom: 0;
    }
  

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }

";
    }

    // line 111
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 112
        echo "
    \$('#frmConfirm').submit(function(e) {
        e.preventDefault();
        const main_id_json = htmlDecode('";
        // line 115
        echo twig_escape_filter($this->env, ($context["main_id"] ?? null), "html", null, true);
        echo "');
        const main_id = JSON.parse(main_id_json);
        const type = Array.isArray(main_id) && main_id.length > 1 ? 'saveall' : 'saveone';
        \$.ajax({
            url: './ajax/ajax_save_image_sendbranch.php', 
            type: 'POST',
            data: {
                filename : FileNameimg,
                main_id : main_id,
                type : type
            },
            success: function(response) {
                response = JSON.parse(response);
                if(response.status == \"success\") {
                    window.location.href = \"send_branch_list.php\";
                }else{
                    alert('บันทึกข้อมูลไม่สำเร็จ');
                }
            }
        });
    });
";
    }

    // line 138
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 139
        echo "function htmlDecode(input) {
    var doc = new DOMParser().parseFromString(input, \"text/html\");
    return doc.documentElement.textContent;
}
    function updateWorkOrders(arr) {
        return \$.post('./ajax/ajax_updateBranchReceived.php', { data: JSON.stringify(arr) });
    }

let FileNameimg = '';

function displaySelectedImage(event, elementId) {
    const selectedImage = document.getElementById(elementId);
    const fileInput = event.target;

    if (fileInput.files && fileInput.files[0]) {
        const reader = new FileReader();
        const file = fileInput.files[0];
        
        reader.onload = function(e) {
            selectedImage.src = e.target.result;
            selectedImage.style.display = 'block'; 

            // Display the file name
            // const fileName = file.name;
            // FileNameimg = fileName;
            const base64String = e.target.result.replace(/^data:image\\/[a-zA-Z]+;base64,/, '');
            FileNameimg = base64String;
        };

        reader.readAsDataURL(file);
    } else {
        selectedImage.style.display = 'none'; 
    }
}
";
    }

    // line 175
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 176
        echo "
<div class=\"container\">
    <form action=\"confirmSendbranch_no.php\" method=\"POST\" id=\"frmConfirm\">
        <h3 class= \"text-center\">อัพโหลดใบคุม</h3>
        <div class=\"form-group\">
            <div>
                <div class=\"mb-4 d-flex justify-content-center\">
                    <img id=\"selectedImage\" src=\"../themes/images/no-image.jpg\"
                    alt=\"example placeholder\" style=\"width: 300px;\" />
                </div>
                <div class=\"d-flex justify-content-center\">
                    <div data-mdb-ripple-init class=\"btn btn-primary btn-rounded\">
                        <label class=\"form-label text-white m-1\" for=\"customFile1\">Choose file</label>
                        <input type=\"file\" class=\"form-control d-none\" id=\"customFile1\" onchange=\"displaySelectedImage(event, 'selectedImage')\" />
                    </div>
                </div>
            </div>
            <br>
            <div class=\"d-flex justify-content-center\">
             ";
        // line 195
        if ((($context["time_dif"] ?? null) > 0)) {
            echo " 
                <button type=\"submit\" class=\"btn btn-success\">ยืนยันการรับเอกสาร</button>
             ";
        } else {
            // line 197
            echo " 
            </div>
             <div class=\"alert alert-danger\" role=\"alert\">
\t\t        ไม่สามารถส่งได้ : <b> เนื่องจากเลยเวลา  ";
            // line 200
            echo twig_escape_filter($this->env, ($context["time_end"] ?? null), "html", null, true);
            echo "</b>
\t\t    </div>
\t\t    ";
        }
        // line 202
        echo " 
        </div>
    </form>
</div>

";
    }

    // line 210
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 212
        if ((($context["debug"] ?? null) != "")) {
            // line 213
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 215
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/confirmSendBranch_no.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  323 => 215,  317 => 213,  315 => 212,  308 => 210,  299 => 202,  293 => 200,  288 => 197,  282 => 195,  261 => 176,  257 => 175,  219 => 139,  215 => 138,  189 => 115,  184 => 112,  180 => 111,  77 => 10,  73 => 9,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        height: 100%;
        
    }

    


    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 800px;
        overflow: auto;
        margin-bottom: 50px;
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    .fixedContainer {
        position: fixed;
        width: 100%;
        padding: 10px 10px;
        left: 0;
        bottom: 0;
    }
  

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }

{% endblock %}

{% block domReady %}

    \$('#frmConfirm').submit(function(e) {
        e.preventDefault();
        const main_id_json = htmlDecode('{{main_id}}');
        const main_id = JSON.parse(main_id_json);
        const type = Array.isArray(main_id) && main_id.length > 1 ? 'saveall' : 'saveone';
        \$.ajax({
            url: './ajax/ajax_save_image_sendbranch.php', 
            type: 'POST',
            data: {
                filename : FileNameimg,
                main_id : main_id,
                type : type
            },
            success: function(response) {
                response = JSON.parse(response);
                if(response.status == \"success\") {
                    window.location.href = \"send_branch_list.php\";
                }else{
                    alert('บันทึกข้อมูลไม่สำเร็จ');
                }
            }
        });
    });
{% endblock %}

{% block javaScript %}
function htmlDecode(input) {
    var doc = new DOMParser().parseFromString(input, \"text/html\");
    return doc.documentElement.textContent;
}
    function updateWorkOrders(arr) {
        return \$.post('./ajax/ajax_updateBranchReceived.php', { data: JSON.stringify(arr) });
    }

let FileNameimg = '';

function displaySelectedImage(event, elementId) {
    const selectedImage = document.getElementById(elementId);
    const fileInput = event.target;

    if (fileInput.files && fileInput.files[0]) {
        const reader = new FileReader();
        const file = fileInput.files[0];
        
        reader.onload = function(e) {
            selectedImage.src = e.target.result;
            selectedImage.style.display = 'block'; 

            // Display the file name
            // const fileName = file.name;
            // FileNameimg = fileName;
            const base64String = e.target.result.replace(/^data:image\\/[a-zA-Z]+;base64,/, '');
            FileNameimg = base64String;
        };

        reader.readAsDataURL(file);
    } else {
        selectedImage.style.display = 'none'; 
    }
}
{% endblock %}

{% block Content %}

<div class=\"container\">
    <form action=\"confirmSendbranch_no.php\" method=\"POST\" id=\"frmConfirm\">
        <h3 class= \"text-center\">อัพโหลดใบคุม</h3>
        <div class=\"form-group\">
            <div>
                <div class=\"mb-4 d-flex justify-content-center\">
                    <img id=\"selectedImage\" src=\"../themes/images/no-image.jpg\"
                    alt=\"example placeholder\" style=\"width: 300px;\" />
                </div>
                <div class=\"d-flex justify-content-center\">
                    <div data-mdb-ripple-init class=\"btn btn-primary btn-rounded\">
                        <label class=\"form-label text-white m-1\" for=\"customFile1\">Choose file</label>
                        <input type=\"file\" class=\"form-control d-none\" id=\"customFile1\" onchange=\"displaySelectedImage(event, 'selectedImage')\" />
                    </div>
                </div>
            </div>
            <br>
            <div class=\"d-flex justify-content-center\">
             {% if time_dif > 0 %} 
                <button type=\"submit\" class=\"btn btn-success\">ยืนยันการรับเอกสาร</button>
             {% else %} 
            </div>
             <div class=\"alert alert-danger\" role=\"alert\">
\t\t        ไม่สามารถส่งได้ : <b> เนื่องจากเลยเวลา  {{  time_end }}</b>
\t\t    </div>
\t\t    {% endif %} 
        </div>
    </form>
</div>

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}

", "messenger/confirmSendBranch_no.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\messenger\\confirmSendBranch_no.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/mailroom_sendWork.tpl */
class __TwigTemplate_63168b5b11c10055846ef9a80be18a2c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/mailroom_sendWork.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        height:100%;
      }

      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}
      #tb_work_order {
        font-size: 13px;
      }
\t  .panel {
\t\tmargin-bottom : 5px;
\t  }
#loader{
\t  width:5%;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

";
    }

    // line 44
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>

   <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\" integrity=\"sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i\" crossorigin=\"anonymous\"></script>
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap v4.6.1.css\">

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "\$('#bg_loader').hide()
\t\t//load_data();
\t\tload_data_Byround_resive();
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'searching': true,
\t\t\t'lengthChange': false,
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'remove'},
\t\t\t\t{ 'data':'sys_timestamp'},
\t\t\t\t{ 'data':'st_name'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'branch' },
\t\t\t\t{ 'data':'send_round' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
\t\t\t
\t\$('#mr_branch_id').select2({
\t\ttheme: 'bootstrap4',
\t});
\t\$('#hub_id').select2({
\t\ttheme: 'bootstrap4',
\t});

\t\$('#scan_round_id').select2({
\t\ttheme: 'bootstrap4',
\t});

\t\$('#date').datepicker({
\t\ttodayHighlight\t: true,
\t\tformat\t\t\t: \"yyyy-mm-dd\",
\t\tautoclose\t\t: true,
\t\tsetDate\t\t\t: new Date(),
\t\t});

\t\$('#date_send').datepicker({
\t\ttodayHighlight\t: true,
\t\tformat\t\t\t: \"yyyy-mm-dd\",
\t\tautoclose\t\t: true,
\t\tsetDate\t\t\t: new Date(),
\t\t});



\$('#mr_round_id').select2(
\t{ width: '100%' }
);
\t\t
";
    }

    // line 123
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 124
        echo "\t\t
\tfunction load_data_Byround_resive() {
\t\tvar scan_round_id = \$('#scan_round_id').val();
\t\tvar barcose_scan = \$('#barcose_scan').val();

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_mailroom_send_round.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'scan_round_id'\t: scan_round_id,
\t\t\t\t'barcose_scan'\t\t: barcose_scan,
\t\t\t\t'page'\t\t: 'load_data',
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\$('#bg_loader').hide();
\t\t\tif(msg['status']==200){
\t\t\t\t
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t}else{
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t}
\t\t});
\t}
\tfunction save_barcode() {
\t\talertify.confirm('บันทึกข้อมูล','กดปุ่ม \"OK\" เพื่อยืนยัน', function(){
\t\t\tif (\$('#is_status_update').is(':checked')) {
\t\t\t\tvar is_status_update = 1;
\t\t\t}else{
\t\t\t\tvar is_status_update = 0;
\t\t\t}
\t\t\tvar scan_round_id = \$('#scan_round_id').val();
\t\t\tvar barcose_scan = \$('#barcose_scan').val();
\t\t\tvar date_send = \$('#date_send').val();
\t\t\t\$('#barcose_scan').val('');

\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_load_data_mailroom_send_round.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdataType:'json',
\t\t\t\tdata: {
\t\t\t\t\t'scan_round_id'\t\t\t: scan_round_id,
\t\t\t\t\t'barcose_scan'\t\t\t: barcose_scan,
\t\t\t\t\t'date_send'\t\t\t\t: date_send,
\t\t\t\t\t'is_status_update'\t\t: is_status_update,
\t\t\t\t\t'page'\t\t\t\t\t: 'save',
\t\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t}
\t\t\t}).done(function( msg ) {
\t\t\t\tsetTimeout(function(){ \$('.alert ').hide(); }, 3000);
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\tload_data_Byround_resive();
\t\t\t\tif(msg['status']==200){
\t\t\t\t\t\$('#success').html(msg['msg']);
\t\t\t\t\t\$('#success').show();
\t\t\t\t}else{
\t\t\t\t\t\$('#error').html(msg['msg']);
\t\t\t\t\t\$('#error').show();
\t\t\t\t}
\t\t\t});
\t\t}, function(){ alertify.error('Cancel')});
\t}
\tfunction keyEvent_subbarcode( evt )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tsave_barcode();
\t\t\t}
\t\t}
\t\tfunction remove_mr_round_resive_work(id){
\t\t\tconsole.log(id);
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_load_data_mailroom_send_round.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdataType:'json',
\t\t\t\tdata: {
\t\t\t\t\t'id'\t\t\t\t: id,
\t\t\t\t\t'page'\t\t\t\t: 'remove',
\t\t\t\t},
\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t}
\t\t\t}).done(function( msg ) {
\t\t\t\tsetTimeout(function(){ \$('.alert ').hide(); }, 3000);

\t\t\t\tif(msg['status']==200){
\t\t\t\t\t\$('#success').html(msg['msg']);
\t\t\t\t\t\$('#success').show();
\t\t\t\t}else{
\t\t\t\t\t\$('#error').html(msg['msg']);
\t\t\t\t\t\$('#error').show();
\t\t\t\t}
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\tload_data_Byround_resive();
\t\t\t});
\t\t}

\tfunction saveround() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t //console.log(this.value);
\t\t\t dataall.push(this.value);
\t\t  });
\t\t  //console.log(tbl_data);
\t\t  if(dataall.length < 1){
\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t return;
\t\t  }
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_saveround.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'main_id': newdataall
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();\t
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\tload_data();
\t\t\t}else{
\t\t\t\talertify.alert(\"แจ้งเตือน\",msg.message); 
\t\t\t}
\t\t\t
\t\t});
\t}
\t
\tfunction print_all() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});

\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data1').val(newdataall);
\t\t\$('#print_1').submit();

\t}
\t
\tfunction print_byHub() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t //console.log(this.value);
\t\t\tdataall.push(this.value);
\t\t});

\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data2').val(newdataall);
\t\t\$('#print_2').submit();
\t}\t
\tfunction changHub(){

\t\tvar hub_id = \$('#hub_id').val();
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_branch_Byhub.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'hub_id': hub_id
\t\t\t},
\t\tbeforeSend: function( xhr ) {
\t\t\t//\$('#bg_loader').show();
\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('#mr_branch_id').html(msg.data);
\t\t\t\tload_data();
\t\t\t}else{
\t\t\t\talertify.alert(\"แจ้งเตือน\",msg.message); 
\t\t\t}
\t\t});
\t}

\tfunction load_data(){
\t\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\tvar hub_id = \$('#hub_id').val();
\t\tvar data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_send_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_branch_id'\t: mr_branch_id,
\t\t\t\t'data_all'\t\t: data_all,
\t\t\t\t'hub_id'\t\t: hub_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t});
\t}
\t
\tfunction click_load_data(){
\t\tvar date = \$('#date').val();
\t\tvar mr_round_id = \$('#mr_round_id').val();
\t\t// var data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_send_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'date'\t: date,
\t\t\t\t'mr_round_id'\t\t: mr_round_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t});
\t}

\tfunction new_load() {
\t\tvar date \t\t\t\t= \$('#date').val();
\t\tvar mr_branch_id \t\t= \$('#mr_branch_id').val();
\t\tvar hub_id \t\t\t\t= \$('#hub_id').val();
\t\tvar mr_round_id \t\t= ( JSON.stringify(\$('#mr_round_id').select2('val')) );

\t\t//var mr_round_id = \$('#mr_round_id').val();
\t\t// var data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_print_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'date'\t\t\t\t: date,
\t\t\t\t'hub_id'\t\t\t: hub_id,
\t\t\t\t'mr_branch_id'\t\t: mr_branch_id,
\t\t\t\t'mr_round_id'\t\t: mr_round_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t});
\t}


\tfunction print_option(type){
\t\t\tvar dataall = [];
\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t //console.log(this.value);
\t\t\t\tdataall.push(this.value);
\t\t\t});
\t
\t\t\tif(dataall.length < 1){
\t\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\treturn;
\t\t\t}

\t\t\tvar round = \$('#mr_round_id').select2('data')
\t\t\t//console.log(\$('#mr_round_id').val());
            if(\$('#mr_round_id').val()){
\t\t\t\t\$('#round_name').val(round[0].text);
\t\t\t}

\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\$('#data_option').val(newdataall);
\t\t\t
\t\t\tif(type==0){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_sort_branch_all.php');
\t\t\t}else if(type==1){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_sort_resive.php');
\t\t\t}else if(type==2){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_peper_all_data.php');
\t\t\t}else if(type==3){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_hub_all.php');
\t\t\t}

\t\t\t\$('#print_option').submit();
\t\t
\t}
";
    }

    // line 439
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 440
        echo "<form id=\"print_1\" action=\"print_sort_branch_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data1\" name=\"data1\">
</form>
<form id=\"print_2\" action=\"print_hab_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data2\" name=\"data2\">
</form>
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">พิมพ์ใบคุมเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form id=\"print_option\" action=\"print_option.php\" method=\"post\" target=\"_blank\">
\t\t\t\t\t\t<input type=\"hidden\" value=\"\" id=\"round_name\" name=\"round_name\">
\t\t\t\t\t\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t  <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">Scan Barcode</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t  <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">ค้นหาใบคุม</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t  </ul>
\t\t\t\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t\t\t\t<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">วันที่ส่งออก : </label>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t  <input type=\"text\" id=\"date_send\" name=\"date_send\" class=\"form-control\" autocomplete=\"off\" value=\"";
        // line 468
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t  <div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">รอบ :  </label>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"scan_round_id\" onchange=\"load_data_Byround_resive();\">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 479
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 480
            echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 480), "html", null, true);
            echo "\">รับ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_type_work_name", [], "any", false, false, false, 480), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 480), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 482
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">Scan Barcode :  </label>
\t\t\t\t\t\t\t\t\t\t\t<input onkeyup=\"keyEvent_subbarcode(event);\" type=\"text\" class=\"form-control\" id=\"barcose_scan\" placeholder=\"Scan Barcode\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"custom-control custom-switch switch-certificate\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" class=\"custom-control-input\" id=\"is_status_update\" name=\"is_status_update\" value=\"1\">
\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"custom-control-label\" for=\"is_status_update\">อัปดทสถานะงานหลังจากยิงบาร์โค้ด</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"data_option\" name=\"data_option\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">วันที่ : </label>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t  <input type=\"text\" id=\"date\" name=\"date\" class=\"form-control\" autocomplete=\"off\" value=\"";
        // line 517
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">ศูนย์</label>
\t\t\t\t\t\t\t\t\t\t\t<select";
        // line 525
        echo " name=\"hub_id\" class=\"form-control\" id=\"hub_id\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"all_hub\">ทุก Hub</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"all_branch\">สาขาต่างจังหวัดทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t";
        // line 529
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hub"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 530
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_hub_id", [], "any", false, false, false, 530), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_hub_name", [], "any", false, false, false, 530), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 532
        echo "\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">สาขา</label>
\t\t\t\t\t\t\t\t\t\t\t<select ";
        // line 538
        echo " name=\"mr_branch_id\" class=\"form-control\" id=\"mr_branch_id\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ทุกสาขา</option>
\t\t\t\t\t\t\t\t\t\t\t";
        // line 540
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 541
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 541), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_code", [], "any", false, false, false, 541), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 541), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 543
        echo "\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">รอบการยิงรับ :  </label>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"mr_round_id\" multiple=\"multiple\"  name=\"states[]\">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 554
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 555
            echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 555), "html", null, true);
            echo "\">รับ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_type_work_name", [], "any", false, false, false, 555), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 555), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 557
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t";
        // line 570
        echo "\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\t
\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t<button onclick=\"new_load();\" type=\"button\" class=\"btn btn-info\">ค้นหา</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(3);\"type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมใหญ่</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(0);\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมย่อย</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบนำส่ง(แยกตามผู้รับ)</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบนำส่ง(รวมรายการ)</button>
\t\t
\t\t\t\t\t\t\t\t\t\t";
        // line 589
        echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t  </div>




\t\t\t\t\t\t\t
\t\t\t\t\t\t  
\t\t\t\t\t\t<div class=\"collapse\" id=\"collapseExample\">
\t\t\t\t\t\t  <div class=\"card card-body\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\t
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t";
        // line 618
        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t


\t\t\t\t\t\t\t\t
\t\t\t\t\t\t  
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t<div class=\"alert alert-success\" role=\"alert\" id=\"success\" style=\"display: none;\">
\t\t\t\t\t\t\t\t....
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  <div class=\"alert alert-danger\" role=\"alert\" id=\"error\"  style=\"display: none;\">
\t\t\t\t\t\t\t\t''''
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t\t\t<th>Date/Time</th>
\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t<th>Bacode</th>
\t\t\t\t\t\t\t\t<th>Sender</th>
\t\t\t\t\t\t\t\t<th>Receiver</th>
\t\t\t\t\t\t\t\t<th>Type Work</th>
\t\t\t\t\t\t\t\t<th>Branch</th>
\t\t\t\t\t\t\t\t<th>SendRound</th>
\t\t\t\t\t\t\t\t<th class=\"text-center\">
\t\t\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือกทั้งหมด</span>
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
";
    }

    // line 689
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 691
        if ((($context["debug"] ?? null) != "")) {
            // line 692
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 694
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/mailroom_sendWork.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  845 => 694,  839 => 692,  837 => 691,  830 => 689,  758 => 618,  736 => 589,  718 => 570,  713 => 557,  700 => 555,  696 => 554,  683 => 543,  670 => 541,  666 => 540,  662 => 538,  654 => 532,  643 => 530,  639 => 529,  633 => 525,  622 => 517,  585 => 482,  572 => 480,  568 => 479,  554 => 468,  524 => 440,  520 => 439,  203 => 124,  199 => 123,  135 => 60,  131 => 59,  115 => 45,  111 => 44,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }

      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}
      #tb_work_order {
        font-size: 13px;
      }
\t  .panel {
\t\tmargin-bottom : 5px;
\t  }
#loader{
\t  width:5%;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>

   <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\" integrity=\"sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i\" crossorigin=\"anonymous\"></script>
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap v4.6.1.css\">

{% endblock %}

{% block domReady %}
\$('#bg_loader').hide()
\t\t//load_data();
\t\tload_data_Byround_resive();
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'searching': true,
\t\t\t'lengthChange': false,
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'remove'},
\t\t\t\t{ 'data':'sys_timestamp'},
\t\t\t\t{ 'data':'st_name'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'branch' },
\t\t\t\t{ 'data':'send_round' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
\t\t\t
\t\$('#mr_branch_id').select2({
\t\ttheme: 'bootstrap4',
\t});
\t\$('#hub_id').select2({
\t\ttheme: 'bootstrap4',
\t});

\t\$('#scan_round_id').select2({
\t\ttheme: 'bootstrap4',
\t});

\t\$('#date').datepicker({
\t\ttodayHighlight\t: true,
\t\tformat\t\t\t: \"yyyy-mm-dd\",
\t\tautoclose\t\t: true,
\t\tsetDate\t\t\t: new Date(),
\t\t});

\t\$('#date_send').datepicker({
\t\ttodayHighlight\t: true,
\t\tformat\t\t\t: \"yyyy-mm-dd\",
\t\tautoclose\t\t: true,
\t\tsetDate\t\t\t: new Date(),
\t\t});



\$('#mr_round_id').select2(
\t{ width: '100%' }
);
\t\t
{% endblock %}


{% block javaScript %}
\t\t
\tfunction load_data_Byround_resive() {
\t\tvar scan_round_id = \$('#scan_round_id').val();
\t\tvar barcose_scan = \$('#barcose_scan').val();

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_mailroom_send_round.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'scan_round_id'\t: scan_round_id,
\t\t\t\t'barcose_scan'\t\t: barcose_scan,
\t\t\t\t'page'\t\t: 'load_data',
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\$('#bg_loader').hide();
\t\t\tif(msg['status']==200){
\t\t\t\t
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t}else{
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t}
\t\t});
\t}
\tfunction save_barcode() {
\t\talertify.confirm('บันทึกข้อมูล','กดปุ่ม \"OK\" เพื่อยืนยัน', function(){
\t\t\tif (\$('#is_status_update').is(':checked')) {
\t\t\t\tvar is_status_update = 1;
\t\t\t}else{
\t\t\t\tvar is_status_update = 0;
\t\t\t}
\t\t\tvar scan_round_id = \$('#scan_round_id').val();
\t\t\tvar barcose_scan = \$('#barcose_scan').val();
\t\t\tvar date_send = \$('#date_send').val();
\t\t\t\$('#barcose_scan').val('');

\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_load_data_mailroom_send_round.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdataType:'json',
\t\t\t\tdata: {
\t\t\t\t\t'scan_round_id'\t\t\t: scan_round_id,
\t\t\t\t\t'barcose_scan'\t\t\t: barcose_scan,
\t\t\t\t\t'date_send'\t\t\t\t: date_send,
\t\t\t\t\t'is_status_update'\t\t: is_status_update,
\t\t\t\t\t'page'\t\t\t\t\t: 'save',
\t\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t}
\t\t\t}).done(function( msg ) {
\t\t\t\tsetTimeout(function(){ \$('.alert ').hide(); }, 3000);
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\tload_data_Byround_resive();
\t\t\t\tif(msg['status']==200){
\t\t\t\t\t\$('#success').html(msg['msg']);
\t\t\t\t\t\$('#success').show();
\t\t\t\t}else{
\t\t\t\t\t\$('#error').html(msg['msg']);
\t\t\t\t\t\$('#error').show();
\t\t\t\t}
\t\t\t});
\t\t}, function(){ alertify.error('Cancel')});
\t}
\tfunction keyEvent_subbarcode( evt )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tsave_barcode();
\t\t\t}
\t\t}
\t\tfunction remove_mr_round_resive_work(id){
\t\t\tconsole.log(id);
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_load_data_mailroom_send_round.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdataType:'json',
\t\t\t\tdata: {
\t\t\t\t\t'id'\t\t\t\t: id,
\t\t\t\t\t'page'\t\t\t\t: 'remove',
\t\t\t\t},
\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t}
\t\t\t}).done(function( msg ) {
\t\t\t\tsetTimeout(function(){ \$('.alert ').hide(); }, 3000);

\t\t\t\tif(msg['status']==200){
\t\t\t\t\t\$('#success').html(msg['msg']);
\t\t\t\t\t\$('#success').show();
\t\t\t\t}else{
\t\t\t\t\t\$('#error').html(msg['msg']);
\t\t\t\t\t\$('#error').show();
\t\t\t\t}
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\tload_data_Byround_resive();
\t\t\t});
\t\t}

\tfunction saveround() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t //console.log(this.value);
\t\t\t dataall.push(this.value);
\t\t  });
\t\t  //console.log(tbl_data);
\t\t  if(dataall.length < 1){
\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t return;
\t\t  }
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_saveround.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'main_id': newdataall
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();\t
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\tload_data();
\t\t\t}else{
\t\t\t\talertify.alert(\"แจ้งเตือน\",msg.message); 
\t\t\t}
\t\t\t
\t\t});
\t}
\t
\tfunction print_all() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});

\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data1').val(newdataall);
\t\t\$('#print_1').submit();

\t}
\t
\tfunction print_byHub() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t //console.log(this.value);
\t\t\tdataall.push(this.value);
\t\t});

\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data2').val(newdataall);
\t\t\$('#print_2').submit();
\t}\t
\tfunction changHub(){

\t\tvar hub_id = \$('#hub_id').val();
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_branch_Byhub.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'hub_id': hub_id
\t\t\t},
\t\tbeforeSend: function( xhr ) {
\t\t\t//\$('#bg_loader').show();
\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('#mr_branch_id').html(msg.data);
\t\t\t\tload_data();
\t\t\t}else{
\t\t\t\talertify.alert(\"แจ้งเตือน\",msg.message); 
\t\t\t}
\t\t});
\t}

\tfunction load_data(){
\t\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\tvar hub_id = \$('#hub_id').val();
\t\tvar data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_send_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_branch_id'\t: mr_branch_id,
\t\t\t\t'data_all'\t\t: data_all,
\t\t\t\t'hub_id'\t\t: hub_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t});
\t}
\t
\tfunction click_load_data(){
\t\tvar date = \$('#date').val();
\t\tvar mr_round_id = \$('#mr_round_id').val();
\t\t// var data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_send_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'date'\t: date,
\t\t\t\t'mr_round_id'\t\t: mr_round_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t});
\t}

\tfunction new_load() {
\t\tvar date \t\t\t\t= \$('#date').val();
\t\tvar mr_branch_id \t\t= \$('#mr_branch_id').val();
\t\tvar hub_id \t\t\t\t= \$('#hub_id').val();
\t\tvar mr_round_id \t\t= ( JSON.stringify(\$('#mr_round_id').select2('val')) );

\t\t//var mr_round_id = \$('#mr_round_id').val();
\t\t// var data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_print_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'date'\t\t\t\t: date,
\t\t\t\t'hub_id'\t\t\t: hub_id,
\t\t\t\t'mr_branch_id'\t\t: mr_branch_id,
\t\t\t\t'mr_round_id'\t\t: mr_round_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t});
\t}


\tfunction print_option(type){
\t\t\tvar dataall = [];
\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t //console.log(this.value);
\t\t\t\tdataall.push(this.value);
\t\t\t});
\t
\t\t\tif(dataall.length < 1){
\t\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\treturn;
\t\t\t}

\t\t\tvar round = \$('#mr_round_id').select2('data')
\t\t\t//console.log(\$('#mr_round_id').val());
            if(\$('#mr_round_id').val()){
\t\t\t\t\$('#round_name').val(round[0].text);
\t\t\t}

\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\$('#data_option').val(newdataall);
\t\t\t
\t\t\tif(type==0){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_sort_branch_all.php');
\t\t\t}else if(type==1){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_sort_resive.php');
\t\t\t}else if(type==2){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_peper_all_data.php');
\t\t\t}else if(type==3){
\t\t\t\t\$(\"#print_option\").attr('action', 'print_hub_all.php');
\t\t\t}

\t\t\t\$('#print_option').submit();
\t\t
\t}
{% endblock %}

{% block Content2 %}
<form id=\"print_1\" action=\"print_sort_branch_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data1\" name=\"data1\">
</form>
<form id=\"print_2\" action=\"print_hab_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data2\" name=\"data2\">
</form>
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">พิมพ์ใบคุมเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form id=\"print_option\" action=\"print_option.php\" method=\"post\" target=\"_blank\">
\t\t\t\t\t\t<input type=\"hidden\" value=\"\" id=\"round_name\" name=\"round_name\">
\t\t\t\t\t\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t  <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">Scan Barcode</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t  <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">ค้นหาใบคุม</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t  </ul>
\t\t\t\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t\t\t\t<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">วันที่ส่งออก : </label>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t  <input type=\"text\" id=\"date_send\" name=\"date_send\" class=\"form-control\" autocomplete=\"off\" value=\"{{today}}\">
\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t  <div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">รอบ :  </label>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"scan_round_id\" onchange=\"load_data_Byround_resive();\">
\t\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">Scan Barcode :  </label>
\t\t\t\t\t\t\t\t\t\t\t<input onkeyup=\"keyEvent_subbarcode(event);\" type=\"text\" class=\"form-control\" id=\"barcose_scan\" placeholder=\"Scan Barcode\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"custom-control custom-switch switch-certificate\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" class=\"custom-control-input\" id=\"is_status_update\" name=\"is_status_update\" value=\"1\">
\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"custom-control-label\" for=\"is_status_update\">อัปดทสถานะงานหลังจากยิงบาร์โค้ด</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"data_option\" name=\"data_option\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">วันที่ : </label>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t  <input type=\"text\" id=\"date\" name=\"date\" class=\"form-control\" autocomplete=\"off\" value=\"{{today}}\">
\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">ศูนย์</label>
\t\t\t\t\t\t\t\t\t\t\t<select{# onchange=\"changHub();\"#} name=\"hub_id\" class=\"form-control\" id=\"hub_id\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"all_hub\">ทุก Hub</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"all_branch\">สาขาต่างจังหวัดทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t{% for b in hub %}
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{b.mr_hub_id}}\">{{b.mr_hub_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">สาขา</label>
\t\t\t\t\t\t\t\t\t\t\t<select {#  onchange=\"load_data();\" #} name=\"mr_branch_id\" class=\"form-control\" id=\"mr_branch_id\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ทุกสาขา</option>
\t\t\t\t\t\t\t\t\t\t\t{% for b in branch %}
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{b.mr_branch_id}}\">{{b.mr_branch_code}} : {{b.mr_branch_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">\t
\t\t\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">รอบการยิงรับ :  </label>
\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"mr_round_id\" multiple=\"multiple\"  name=\"states[]\">
\t\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t\t\t <label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t\t\t<input onclick=\"load_data();\"id=\"data_all\" name=\"data_all\" type=\"checkbox\" class=\"custom-control-input\" value=\"1\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">แสดงงานที่สั่งวันนี้เท่านั้น</span>
\t\t\t\t\t\t\t\t\t\t\t</label>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>#}
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\t
\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t<button onclick=\"new_load();\" type=\"button\" class=\"btn btn-info\">ค้นหา</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(3);\"type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมใหญ่</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(0);\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมย่อย</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบนำส่ง(แยกตามผู้รับ)</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบนำส่ง(รวมรายการ)</button>
\t\t
\t\t\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t\t\t<button onclick=\"saveround();\"type=\"button\" class=\"btn btn-outline-primary\" id=\"\" onclick=\"\">บันทึกข้อมูลรอบ</button> 
\t\t\t\t\t\t\t\t\t\t#}
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>


\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t  </div>




\t\t\t\t\t\t\t
\t\t\t\t\t\t  
\t\t\t\t\t\t<div class=\"collapse\" id=\"collapseExample\">
\t\t\t\t\t\t  <div class=\"card card-body\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\t
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t{#

\t\t\t\t\t\t\t\t<label class=\"\" for=\"\">พิมพ์ใบคุมแยกตาม : </label>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t  <input type=\"radio\" id=\"\" name=\"customRadio\" class=\"\">  หน่วยงาน/ชั้น
\t\t\t\t\t\t\t\t  <br>
\t\t\t\t\t\t\t\t  <input type=\"radio\" id=\"\" name=\"customRadio\" class=\"\">  ชื่อผู้รับ
\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t  #}
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t


\t\t\t\t\t\t\t\t
\t\t\t\t\t\t  
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t<div class=\"alert alert-success\" role=\"alert\" id=\"success\" style=\"display: none;\">
\t\t\t\t\t\t\t\t....
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  <div class=\"alert alert-danger\" role=\"alert\" id=\"error\"  style=\"display: none;\">
\t\t\t\t\t\t\t\t''''
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t\t\t<th>Date/Time</th>
\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t<th>Bacode</th>
\t\t\t\t\t\t\t\t<th>Sender</th>
\t\t\t\t\t\t\t\t<th>Receiver</th>
\t\t\t\t\t\t\t\t<th>Type Work</th>
\t\t\t\t\t\t\t\t<th>Branch</th>
\t\t\t\t\t\t\t\t<th>SendRound</th>
\t\t\t\t\t\t\t\t<th class=\"text-center\">
\t\t\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือกทั้งหมด</span>
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/mailroom_sendWork.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\mailroom_sendWork.tpl");
    }
}

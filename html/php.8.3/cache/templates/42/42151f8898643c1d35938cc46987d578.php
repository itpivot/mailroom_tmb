<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/work_info.tpl */
class __TwigTemplate_ea7a0bc4828c0461238487e4c3c800e6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_2' => [$this, 'block_menu_2'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/work_info.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "#btn_update:hover,
#btn_cancel:hover,
#btn_back:hover {
    color: #FFFFFF;
    background-color: #055d97;
}



#btn_update,
#btn_cancel,
#btn_back {
    border-color: #0074c0;
    color: #FFFFFF;
    background-color: #0074c0;
}

#detail_sender_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_sender_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 20px;
}

#workNumber {
    color: #006cb7;
}

#detail_receiver_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_receiver_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 40px;
}
.divTimes{
\tposition: absolute;
    top: -30px;
\twidth: 100%;
    //right: -20px;
\t//transform: translateX(-5px) rotateZ(-45deg);
}

.coloe_compress:before{
\tcolor:#fff;
\tbackground-color:#2196F3 !important ;
\t
}
.multi-steps > li.is-active:before, .multi-steps > li.is-active ~ li:before {
  content: counter(stepNum);
  font-family: inherit;
  font-weight: 700;
}
.multi-steps > li.is-active:after, .multi-steps > li.is-active ~ li:after {
  background-color: #ededed;
}

.multi-steps {
  display: table;
  table-layout: fixed;
  width: 100%;
}
.multi-steps > li {
  counter-increment: stepNum;
  text-align: center;
  display: table-cell;
  position: relative;
  color: #2196F3;
}
.multi-steps > li:before {
  content: counter(stepNum);
  display: block;
  margin: 0 auto 4px;
  background-color: #fff;
  width: 36px;
  height: 36px;
  line-height: 32px;
  text-align: center;
  font-weight: bold;
  border-width: 2px;
  border-style: solid;
  border-color: #2196F3;
  border-radius: 50%;
}
.multi-steps > li:after {
  content: '';
  height: 2px;
  width: 100%;
  background-color: #2196F3 ;
  position: absolute;
  top: 16px;
  left: 50%;
  z-index: -1;
}
.multi-steps > li:last-child:after {
  display: none;
}
.multi-steps > li.is-active:before {
  background-color: #fff;
  border-color: #2196F3 ;
}
.multi-steps > li.is-active ~ li {
  color: #808080;
}
.multi-steps > li.is-active ~ li:before {
  background-color: #ededed;
  border-color: #ededed;
}

";
    }

    // line 130
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 131
        echo "moment.locale('th');

var init = { id: '";
        // line 133
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_id", [], "any", false, false, false, 133), "html", null, true);
        echo "', text: '";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_name", [], "any", false, false, false, 133), "html", null, true);
        echo "' };
var init_replace = { id: '";
        // line 134
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_id", [], "any", false, false, false, 134), "html", null, true);
        echo "', text: '";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_name", [], "any", false, false, false, 134), "html", null, true);
        echo "' };
";
        // line 135
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 135) == 2)) {
            // line 136
            echo "var type_work = '2';
";
        } else {
            // line 138
            echo "var type_work = '1';
";
        }
        // line 140
        echo "
var mr_branch_floor = '";
        // line 141
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 141), "html", null, true);
        echo "';
var real_branch_floor = '";
        // line 142
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 142), "html", null, true);
        echo "';

var branch_id = '";
        // line 144
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_branch_id", [], "any", false, false, false, 144), "html", null, true);
        echo "';
var real_branch_id = '";
        // line 145
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_branch_id3", [], "any", false, false, false, 145), "html", null, true);
        echo "';
";
        // line 146
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_floor_id", [], "any", false, false, false, 146) == "")) {
            // line 147
            echo "\tvar floor_id  = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_floor_id", [], "any", false, false, false, 147), "html", null, true);
            echo "';
";
        } else {
            // line 149
            echo "\tvar floor_id  = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_floor_id", [], "any", false, false, false, 149), "html", null, true);
            echo "';
";
        }
        // line 151
        echo "var receiver_id = '";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_id", [], "any", false, false, false, 151), "html", null, true);
        echo "';

var real_receive_id = '";
        // line 153
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_id", [], "any", false, false, false, 153), "html", null, true);
        echo "'
var real_receiver_name = '";
        // line 154
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_name", [], "any", false, false, false, 154), "html", null, true);
        echo "';

var receiver_name = '";
        // line 156
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_name", [], "any", false, false, false, 156), "html", null, true);
        echo "';
var status = '";
        // line 157
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_status_id", [], "any", false, false, false, 157), "html", null, true);
        echo "';

// console.log(d.days()+\" วัน \"+ d.hours()+\" ชม. \"+d.minutes()+\" นาที \"+ d.seconds() + \" วินาที\");

// initial Page
initialPage(status);
setForm(init);
setFormReplace(init_replace);
check_type_send(parseInt(type_work));


//console.log(hoursArr);
//calculateDuration(timeArr,hoursArr);


if (!!branch_id) {
    \$('#branch_send').val(parseInt(branch_id)).trigger('change');
}

if (!!real_branch_id) {
    \$('#real_branch_send').val(parseInt(real_branch_id)).trigger('change');
}

if (!!floor_id) {
    \$('#floor_send').val(parseInt(branch_id)).trigger('change');
}
if (!!mr_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}

if (!!real_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}


\$(\".alert\").alert();

\$('#name_receiver_select').select2({
    placeholder: \"ค้นหาผู้รับ\",
   
    ajax: {
        url: \"ajax/ajax_getdataemployee_select.php\",
        dataType: \"json\",
        delay: 250,
        processResults: function (data) {
            return {
                results : data
            };
        },
        cache: true
    },
    
}).on('select2:select', function(e) {
    //console.log(e.params.data)
    setForm(e.params.data);
});

var frmValid = \$('#frmBranch').validate({
    onsubmit: false,
    onkeyup: false,
    errorClass: \"is-invalid\",
    highlight: function (element) {
        if (element.type == \"radio\" || element.type == \"checkbox\") {
            \$(element).removeClass('is-invalid')
        } else {
            \$(element).addClass('is-invalid')
        }
    },
    rules: {
        telSender: {
            required: false
        },
        type_send: {
            required: true
        },
        nameReceiver: {
            required: true
        },
        telReceiver: {
            required: false
        },
        floorRealReceiver: {
            required: {
                depends: function (elm) {
                    var f = \$('#floor_name').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if( (f == \" - \" || f == \"\") && type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        branchRealReceiver: {
            required: {
                depends: function (elm) {
                    var b = \$('#branch_receiver').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if((b == \" - \" || b == \"\") && type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topic: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topicBranch: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
    },
    messages: {
        telSender: {
            required: 'ระบุเบอร์ติดต่อ'
        },
        type_send: {
            required: 'ระบุประเภทผู้รับ'
        },
        nameReceiver: {
            required: 'กรุณาเลือกผู้รับ'
        },
        telReceiver: {
            required: 'ระบุเบอร์ติดต่อผู้รับ'
        },
        branchRealReceiver: {
            required: 'ระบุสาขาผู้รับ'
        },
        topic: {
            required: 'ระบุชื่อเอกสาร'
        },
        topicBranch: {
            required: 'ระบุชื่อเอกสาร'
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        var placement = \$(element).data('error');
        if (placement) {
            \$(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});

\$('#btn_update').click(function() {
     var _frm = \$('#frmBranch');
    if(\$('#frmBranch').valid()) {
       
        \$.ajax({
            url: _frm.attr('action'),
            type: 'POST',
            data: {
                form: _frm.serializeArray(),
                actions: 'update',
                wid: '";
        // line 333
        echo twig_escape_filter($this->env, ($context["wid"] ?? null), "html", null, true);
        echo "'
            },
            dataType: 'json',
            success: function(res) {
                if(res.status == \"success\") {
                    alertify.alert(res['messeges'], function() {
                        location.reload();
                    });
                }
            }
        })
    }
});


\$(\"#btn_cancel\").click(function(){
    alertify.confirm('ยืนยันจะยกเลิกรายการนำส่งนี้', function() {
        \$.post('./ajax/ajax_updateWorkInfo.php', { actions: 'cancel', wid: '";
        // line 350
        echo twig_escape_filter($this->env, ($context["wid"] ?? null), "html", null, true);
        echo "'}, function(res){
            if(res.status == \"success\") {
                        alertify.alert(res['messeges'], function() {
                            location.reload();
                        });
                    }
        }, 'json');
    })
   
});


";
    }

    // line 366
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 367
        echo "

// \$('#type_send_1').prop('checked',true);

function check_type_send( type_send ){
    if( type_send == 1 ){
        \$(\"#type_1\").show();
        \$(\"#type_2\").hide();

        \$('#type_send_1').prop('checked', true);
        \$('#type_send_2').prop('checked', false);
    }else{
        \$(\"#type_1\").hide();
        \$(\"#type_2\").show();
        
        \$('#type_send_2').prop('checked', true);
        \$('#type_send_1').prop('checked', false);
    }
}

function check_name_re() {
    var pass_emp = \$(\"#pass_emp\").val();
    if(pass_emp == \"\" || pass_emp == null){
        \$(\"#div_re_text\").hide();
        \$(\"#div_re_select\").show();
    }else{
        \$(\"#div_re_text\").show();
        \$(\"#div_re_select\").hide();
    }
}

function setForm(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            \$(\"#depart_receiver\").val(res['department']);
            \$(\"#emp_id\").val(res['mr_emp_id']);
            \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#place_receiver\").val(res['mr_workarea']);
            \$(\"#floor_name\").val(res['mr_department_floor']);
            \$(\"#branch_receiver\").val(res['branch']);
        }
    })
}

function setFormReplace(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            console.log(res);
            // \$(\"#depart_receiver\").val(res['department']);
            // \$(\"#emp_id\").val(res['mr_emp_id']);
            // \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#real_place_receiver\").val(res['mr_workarea']);
            \$(\"#real_floor_name\").val(res['mr_department_floor']);
            \$(\"#real_branch_receiver\").val(res['branch']);
        }
    })
}

var saveBranch = function (obj) {
    return \$.post('ajax/ajax_save_work_branch.php', obj);
}

function initialPage(status) {
    //console.log(status)
    if(parseInt(status) != 7) {
        \$('input').prop('disabled', true);
        \$('select').prop('disabled', true);
        \$('textarea').prop('disabled', true);
        \$('#btn_update').prop('disabled', true);
        \$('#btn_cancel').prop('disabled', true);
    }
}

";
    }

    // line 457
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 458
        echo "
<div class=\"container\">



<br>
<br>
<br>

  
  
  
  





    <div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
        <label>
            <h3><b>รายละเอียดรายการนำส่ง</b></h3>
        </label>
    </div>
    <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 481
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 481), "html", null, true);
        echo "\">
    <input type=\"hidden\" id=\"emp_id\" value=\"\">
    <input type=\"hidden\" id=\"barcode\" name=\"barcode\" value=\"";
        // line 483
        echo twig_escape_filter($this->env, ($context["barcode"] ?? null), "html", null, true);
        echo "\">
    
    <div>
        <p class=\"font-weight-bold\"><span class=\"text-muted\">เลขที่เอกสาร: </span> <span id=\"workNumber\">";
        // line 486
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_work_barcode", [], "any", false, false, false, 486), "html", null, true);
        echo "</span> <span class=\"text-muted\">( ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_status_name", [], "any", false, false, false, 486), "html", null, true);
        echo " )</span></p>
    </div>
    

    <div class=\"form-group\"  id=\"detail_sender_head\">
        <h4>สถานะเอกสาร </h4>
    </div>

    ";
        // line 494
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_status_id", [], "any", false, false, false, 494) != 6)) {
            // line 495
            echo "    <div class=\"demo\"><br><br><br><br>
        <ul class=\"list-unstyled multi-steps\">
\t\t\t ";
            // line 497
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["status"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["st"]) {
                // line 498
                echo "\t\t\t\t\t\t\t
\t\t\t\t ";
                // line 500
                echo "\t\t\t\t\t<li class=\"";
                if ((($context["end_step"] ?? null) == twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 500))) {
                    echo " is-active ";
                }
                if ((twig_get_attribute($this->env, $this->source, $context["st"], "active", [], "any", false, false, false, 500) != "")) {
                    echo " coloe_compress ";
                }
                echo "\">
\t\t\t\t\t\t<strong>";
                // line 501
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["st"], "emp_code", [], "any", false, false, false, 501), "html", null, true);
                echo "</strong><br />
\t\t\t\t\t\t<small>";
                // line 502
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["st"], "name", [], "any", false, false, false, 502), "html", null, true);
                echo "</small>
\t\t\t\t\t\t<div class=\"divTimes\">
\t\t\t\t\t\t\t\t<small>";
                // line 504
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["st"], "date", [], "any", false, false, false, 504), "html", null, true);
                echo "</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['st'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 509
            echo "\t\t\t </ul>
    </div>
    ";
        } else {
            // line 512
            echo "    <div style=\"text-align:center;\">
        <p class=\"text-muted\">
            <i class=\"material-icons\">block</i>
            <br/>
            <strong>ยกเลิกรายการนำส่ง</strong>
        </p>
    </div>
\t<br>
\t<br>
    ";
        }
        // line 522
        echo "        
     <!-- <div class='clearfix'></div> -->
    
     <form action=\"./ajax/ajax_updateWorkInfo.php\" method=\"POST\" id=\"frmBranch\">
        <div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
            <h4>รายละเอียดผู้ส่ง </h4>
        </div>
\t\t";
        // line 529
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 529), "mr_user_role_id", [], "any", false, false, false, 529) == 5)) {
            // line 530
            echo "        <div class=\"form-group\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    สาขาผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"placeholder=\"ชื่อสาขา\" value=\"";
            // line 536
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 536), "mr_branch_code", [], "any", false, false, false, 536), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 536), "mr_branch_name", [], "any", false, false, false, 536), "html", null, true);
            echo "\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"เบอร์โทร\" value=\"";
            // line 543
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 543), "tell", [], "any", false, false, false, 543), "html", null, true);
            echo "\" readonly>
                </div>
            </div>
\t\t\t<div class=\"form-group\" id=\"div_re_select\"><br>
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tชั้น :             </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 551
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 551), "mr_branch_floor", [], "any", false, false, false, 551), "html", null, true);
            echo "\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tตำแหน่ง
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 558
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 558), "mr_position_code", [], "any", false, false, false, 558), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 558), "mr_position_name", [], "any", false, false, false, 558), "html", null, true);
            echo "\" readonly>
                </div>
            </div>
        </div>
        </div>
\t\t";
        } else {
            // line 564
            echo "\t\t <div class=\"form-group\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    แผนก : 
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"  placeholder=\"ชื่อสาขา\" value=\"";
            // line 570
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 570), "mr_department_code", [], "any", false, false, false, 570), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 570), "mr_department_name", [], "any", false, false, false, 570), "html", null, true);
            echo "\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"  placeholder=\"เบอร์โทร\" value=\"";
            // line 577
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 577), "mr_emp_tel", [], "any", false, false, false, 577), "html", null, true);
            echo "\" readonly>
                </div>
            </div>
\t\t\t<div class=\"form-group\" id=\"div_re_select\"><br>
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tชั้น :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 586
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 586), "floor_name", [], "any", false, false, false, 586), "html", null, true);
            echo "\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tตำแหน่ง
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 593
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 593), "mr_position_code", [], "any", false, false, false, 593), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 593), "mr_position_name", [], "any", false, false, false, 593), "html", null, true);
            echo "\" readonly>
                </div>
            </div>
        </div>
        </div>
\t\t
\t\t";
        }
        // line 600
        echo "
        <div class=\"form-group\" id=\"div_re_select\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tชื่อผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" value=\"";
        // line 607
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 607), "mr_emp_code", [], "any", false, false, false, 607), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 607), "mr_emp_name", [], "any", false, false, false, 607), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 607), "mr_emp_lastname", [], "any", false, false, false, 607), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 607), "mr_cus_name", [], "any", false, false, false, 607), "html", null, true);
        echo "\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                </div>
            </div>
        </div>
\t\t
\t\t
\t\t
\t\t

        <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
            <h4>รายละเอียดผู้รับ</h4>
        </div>
        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-2\" style=\"padding-right:0px\">
                    ประเภทการส่ง :
                </div>

                <div class=\"col-2\" style=\"padding-left:0px\">
\t\t\t\t";
        // line 631
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["send_data"] ?? null), 0, [], "any", false, false, false, 631), "mr_type_work_name", [], "any", false, false, false, 631), "html", null, true);
        echo "
                </div>
            </div>
        </div>

        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ผู้รับ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <span class=\"box_error\" id=\"err_receiver_select\"></span>
\t\t\t\t\t
\t\t\t\t\t <input type=\"text\" class=\"form-control form-control-sm\"   value=\"";
        // line 644
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 644), "mr_emp_code", [], "any", false, false, false, 644), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 644), "mr_emp_name", [], "any", false, false, false, 644), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 644), "mr_emp_lastname", [], "any", false, false, false, 644), "html", null, true);
        echo "\"
                    placeholder=\"เบอร์โทร\" readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"   value=\"";
        // line 651
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 651), "tell", [], "any", false, false, false, 651), "html", null, true);
        echo "\"
                        placeholder=\"เบอร์โทร\" readonly>
                </div>
            </div>
\t\t\t<br>
            
        </div>

        <div id=\"type_1\" style=\"display:;\">
\t\t";
        // line 660
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 660), "mr_type_work_id", [], "any", false, false, false, 660) == 1)) {
            // line 661
            echo "        <div class=\"row\">
            <div class=\"col-1\" style=\"padding-right:0px\">
                ตำแหน่ง :
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
                <span class=\"box_error\" id=\"err_receiver_select\"></span>
                
                 <input type=\"text\" class=\"form-control form-control-sm\"   value=\"";
            // line 668
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 668), "mr_position_code", [], "any", false, false, false, 668), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 668), "mr_position_name", [], "any", false, false, false, 668), "html", null, true);
            echo "\"
                placeholder=\"เบอร์โทร\" readonly>
            </div>
            <div class=\"col-1\" style=\"padding-right:0px\">
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
            </div>
        </div><br>
            <div class=\"form-group\" id=\"\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tแผนก :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 683
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 683), "mr_department_code", [], "any", false, false, false, 683), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 683), "mr_department_name", [], "any", false, false, false, 683), "html", null, true);
            echo "\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 690
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 690), "floor_name", [], "any", false, false, false, 690), "html", null, true);
            echo "\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>
\t\t";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 695
($context["resive_data"] ?? null), 0, [], "any", false, false, false, 695), "mr_type_work_id", [], "any", false, false, false, 695) == 3)) {
            // line 696
            echo "        <div class=\"row\">
            <div class=\"col-1\" style=\"padding-right:0px\">
                ตำแหน่ง :
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
                <span class=\"box_error\" id=\"err_receiver_select\"></span>
                
                 <input type=\"text\" class=\"form-control form-control-sm\"   value=\"";
            // line 703
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 703), "mr_position_code", [], "any", false, false, false, 703), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 703), "mr_position_name", [], "any", false, false, false, 703), "html", null, true);
            echo "\"
                placeholder=\"เบอร์โทร\" readonly>
            </div>
            <div class=\"col-1\" style=\"padding-right:0px\">
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
            </div>
        </div><br>
\t\t\t<div class=\"form-group\" id=\"\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tแผนก :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\"  value=\"";
            // line 718
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 718), "mr_department_code", [], "any", false, false, false, 718), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 718), "mr_department_name", [], "any", false, false, false, 718), "html", null, true);
            echo "\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 725
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 725), "floor_name", [], "any", false, false, false, 725), "html", null, true);
            echo "\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>
        ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 730
($context["resive_data"] ?? null), 0, [], "any", false, false, false, 730), "mr_type_work_id", [], "any", false, false, false, 730) == 4)) {
            // line 731
            echo "            
\t\t";
        } else {
            // line 733
            echo "        <div class=\"row\">
            <div class=\"col-1\" style=\"padding-right:0px\">
                ตำแหน่ง :
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
                <span class=\"box_error\" id=\"err_receiver_select\"></span>
                
                 <input type=\"text\" class=\"form-control form-control-sm\"   value=\"";
            // line 740
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 740), "mr_position_code", [], "any", false, false, false, 740), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 740), "mr_position_name", [], "any", false, false, false, 740), "html", null, true);
            echo "\"
                placeholder=\"เบอร์โทร\" readonly>
            </div>
            <div class=\"col-1\" style=\"padding-right:0px\">
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
            </div>
        </div><br>
\t\t<div class=\"form-group\" id=\"\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tสาขา :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\"  value=\"";
            // line 755
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 755), "mr_branch_code", [], "any", false, false, false, 755), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 755), "mr_branch_name", [], "any", false, false, false, 755), "html", null, true);
            echo "\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"";
            // line 762
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 762), "mr_branch_floor", [], "any", false, false, false, 762), "html", null, true);
            echo "\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>
       ";
        }
        // line 768
        echo "            <div class=\"form-group\" id=\"\">
                <div class=\"row\">                
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>";
        // line 774
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 774), "mr_topic", [], "any", false, false, false, 774), "html", null, true);
        echo "</textarea>
                    </div>
                
\t\t\t\t\t<div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\tหมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>";
        // line 781
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 781), "mr_work_remark", [], "any", false, false, false, 781), "html", null, true);
        echo "</textarea>
                    </div>

            </div> <br>  
                <div class=\"row\">  
                               
                    <br>              
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\tจำนวน :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly value=\"";
        // line 792
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 792), "quty", [], "any", false, false, false, 792), "html", null, true);
        echo "\">
                    </div>
                
\t\t\t\t\t

            </div>
        ";
        // line 798
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 798), "mr_type_work_id", [], "any", false, false, false, 798) == 4)) {
            // line 799
            echo "        <br>
            <div class=\"row\">  
               
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ที่อยู่ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>";
            // line 806
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 806), "mr_address", [], "any", false, false, false, 806), "html", null, true);
            echo "</textarea>
                </div>
            </div>
            
        
            <div class=\"row\">
                <hr>
                <br>
                <br>
                <div class=\"col-12\" style=\"padding-left:0px\">
                    <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
                        <h4>ข้อมูลผู้ลงชื่อรับ (ลายเซ็น)</h4>
                    </div>\t
                     <div class=\"col-12\" style=\"padding-right:0px\">
                        สถานะจักส่ง :
                    </div>
                    <div class=\"col-12\">
                        <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly value=\"";
            // line 823
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 823), "mr_work_byhand_status_send_name", [], "any", false, false, false, 823), "html", null, true);
            echo "\">
                    </div>
                    <div class=\"col-12\" style=\"padding-right:0px\">
                        หมายเหตุ(จากพนักงานส่ง) 
                     </div>
                    <div class=\"col-12\" style=\"padding-right:0px\">
                            <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>";
            // line 829
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 829), "send_remark", [], "any", false, false, false, 829), "html", null, true);
            echo "</textarea>
                        </div>
                    <hr>
                    ";
            // line 832
            if ((($context["signatures"] ?? null) != "")) {
                // line 833
                echo "                     <div class=\"text-center\">
                        <img src=\"";
                // line 834
                echo twig_escape_filter($this->env, ($context["signatures"] ?? null), "html", null, true);
                echo "\" class=\"rounded border border-dark\" alt=\"...\" width=\"400px\">
                      </div>
                    ";
            }
            // line 837
            echo "                </div>
            </div>
        ";
        } else {
            // line 840
            echo "\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t<h4>ข้อมูลผู้ลงชื่อรับ</h4>
\t\t\t</div>\t

\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\"  value=\"";
            // line 845
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["log_conf"] ?? null), 0, [], "any", false, false, false, 845), "mr_emp_code", [], "any", false, false, false, 845), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["log_conf"] ?? null), 0, [], "any", false, false, false, 845), "mr_emp_name", [], "any", false, false, false, 845), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["log_conf"] ?? null), 0, [], "any", false, false, false, 845), "mr_emp_lastname", [], "any", false, false, false, 845), "html", null, true);
            echo "\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
       ";
        }
        // line 849
        echo "
        </div>
\t\t<!-- end type 1-->
        <hr/>
\t\t";
        // line 854
        echo "\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t<h4>ดูประวัติ</h4>
\t\t</div>\t
\t\t<div id=\"history_work\">
\t\t\t<table class=\"table\">
\t\t\t  <thead>
\t\t\t\t<tr>
\t\t\t\t  <th scope=\"col\">NO</th>
\t\t\t\t  <th scope=\"col\">วันที่</th>
\t\t\t\t  <th scope=\"col\">ผู้ใช้</th>
\t\t\t\t  <th scope=\"col\">สถานะ</th>
\t\t\t\t  <th scope=\"col\">รอบ</th>
\t\t\t\t  <th scope=\"col\">หมายเหตุ</th>
\t\t\t\t</tr>
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t\t<tr>
\t\t\t\t";
        // line 871
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["log"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 872
            echo "\t\t\t\t  <th scope=\"row\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 872), "html", null, true);
            echo "</th>
\t\t\t\t  <td>";
            // line 873
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "sys_timestamp", [], "any", false, false, false, 873), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 874
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "mr_emp_code", [], "any", false, false, false, 874), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "mr_emp_name", [], "any", false, false, false, 874), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "mr_emp_lastname", [], "any", false, false, false, 874), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 875
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "mr_status_name", [], "any", false, false, false, 875), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 876
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "mr_round_name", [], "any", false, false, false, 876), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 877
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["l"], "remark", [], "any", false, false, false, 877), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 880
        echo "\t\t\t  </tbody>
\t\t\t</table>
            ";
        // line 882
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resive_data"] ?? null), 0, [], "any", false, false, false, 882), "mr_type_work_id", [], "any", false, false, false, 882) == 2)) {
            // line 883
            echo "                ";
            if ((($context["signatures_sendbranch"] ?? null) != "")) {
                // line 884
                echo "                     <div class=\"text-center\">
                        <img src=\"";
                // line 885
                echo twig_escape_filter($this->env, ($context["signatures_sendbranch"] ?? null), "html", null, true);
                echo "\" class=\"rounded border border-dark\" alt=\"...\" width=\"400px\">
                      </div>
                ";
            } else {
                // line 888
                echo "                    <div class=\"text-center\">
                           
                    </div>
                ";
            }
            // line 892
            echo "            ";
        }
        // line 893
        echo "\t\t</div>
\t\t
<br>
<br>
<br>
<br>
     </form>
</div>
";
    }

    // line 904
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 905
        echo "
";
        // line 906
        if ((($context["debug"] ?? null) != "")) {
            // line 907
            echo "<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
";
        }
        // line 909
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/work_info.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1342 => 909,  1336 => 907,  1334 => 906,  1331 => 905,  1327 => 904,  1315 => 893,  1312 => 892,  1306 => 888,  1300 => 885,  1297 => 884,  1294 => 883,  1292 => 882,  1288 => 880,  1271 => 877,  1267 => 876,  1263 => 875,  1255 => 874,  1251 => 873,  1246 => 872,  1229 => 871,  1210 => 854,  1204 => 849,  1193 => 845,  1186 => 840,  1181 => 837,  1175 => 834,  1172 => 833,  1170 => 832,  1164 => 829,  1155 => 823,  1135 => 806,  1126 => 799,  1124 => 798,  1115 => 792,  1101 => 781,  1091 => 774,  1083 => 768,  1074 => 762,  1062 => 755,  1042 => 740,  1033 => 733,  1029 => 731,  1027 => 730,  1019 => 725,  1007 => 718,  987 => 703,  978 => 696,  976 => 695,  968 => 690,  956 => 683,  936 => 668,  927 => 661,  925 => 660,  913 => 651,  899 => 644,  883 => 631,  850 => 607,  841 => 600,  829 => 593,  819 => 586,  807 => 577,  795 => 570,  787 => 564,  776 => 558,  766 => 551,  755 => 543,  743 => 536,  735 => 530,  733 => 529,  724 => 522,  712 => 512,  707 => 509,  688 => 504,  683 => 502,  679 => 501,  669 => 500,  666 => 498,  649 => 497,  645 => 495,  643 => 494,  630 => 486,  624 => 483,  619 => 481,  594 => 458,  590 => 457,  497 => 367,  493 => 366,  476 => 350,  456 => 333,  277 => 157,  273 => 156,  268 => 154,  264 => 153,  258 => 151,  252 => 149,  246 => 147,  244 => 146,  240 => 145,  236 => 144,  231 => 142,  227 => 141,  224 => 140,  220 => 138,  216 => 136,  214 => 135,  208 => 134,  202 => 133,  198 => 131,  194 => 130,  70 => 8,  66 => 7,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_2 %} active {% endblock %}

{% block styleReady %}
#btn_update:hover,
#btn_cancel:hover,
#btn_back:hover {
    color: #FFFFFF;
    background-color: #055d97;
}



#btn_update,
#btn_cancel,
#btn_back {
    border-color: #0074c0;
    color: #FFFFFF;
    background-color: #0074c0;
}

#detail_sender_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_sender_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 20px;
}

#workNumber {
    color: #006cb7;
}

#detail_receiver_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_receiver_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 40px;
}
.divTimes{
\tposition: absolute;
    top: -30px;
\twidth: 100%;
    //right: -20px;
\t//transform: translateX(-5px) rotateZ(-45deg);
}

.coloe_compress:before{
\tcolor:#fff;
\tbackground-color:#2196F3 !important ;
\t
}
.multi-steps > li.is-active:before, .multi-steps > li.is-active ~ li:before {
  content: counter(stepNum);
  font-family: inherit;
  font-weight: 700;
}
.multi-steps > li.is-active:after, .multi-steps > li.is-active ~ li:after {
  background-color: #ededed;
}

.multi-steps {
  display: table;
  table-layout: fixed;
  width: 100%;
}
.multi-steps > li {
  counter-increment: stepNum;
  text-align: center;
  display: table-cell;
  position: relative;
  color: #2196F3;
}
.multi-steps > li:before {
  content: counter(stepNum);
  display: block;
  margin: 0 auto 4px;
  background-color: #fff;
  width: 36px;
  height: 36px;
  line-height: 32px;
  text-align: center;
  font-weight: bold;
  border-width: 2px;
  border-style: solid;
  border-color: #2196F3;
  border-radius: 50%;
}
.multi-steps > li:after {
  content: '';
  height: 2px;
  width: 100%;
  background-color: #2196F3 ;
  position: absolute;
  top: 16px;
  left: 50%;
  z-index: -1;
}
.multi-steps > li:last-child:after {
  display: none;
}
.multi-steps > li.is-active:before {
  background-color: #fff;
  border-color: #2196F3 ;
}
.multi-steps > li.is-active ~ li {
  color: #808080;
}
.multi-steps > li.is-active ~ li:before {
  background-color: #ededed;
  border-color: #ededed;
}

{% endblock %}

{% block domReady %}
moment.locale('th');

var init = { id: '{{ data.receive_id }}', text: '{{ data.receive_name }}' };
var init_replace = { id: '{{ data.real_receive_id }}', text: '{{ data.real_receive_name }}' };
{% if data.mr_type_work_id == 2 %}
var type_work = '2';
{% else %}
var type_work = '1';
{% endif %}

var mr_branch_floor = '{{ data.mr_branch_floor }}';
var real_branch_floor = '{{ data.mr_branch_floor }}';

var branch_id = '{{ data.receive_branch_id }}';
var real_branch_id = '{{ data.receive_branch_id3 }}';
{% if data.mr_floor_id=='' %}
\tvar floor_id  = '{{ data.receive_floor_id }}';
{% else %}
\tvar floor_id  = '{{ data.mr_floor_id }}';
{% endif %}
var receiver_id = '{{ data.receive_id }}';

var real_receive_id = '{{ data.real_receive_id }}'
var real_receiver_name = '{{ data.real_receive_name }}';

var receiver_name = '{{ data.receive_name }}';
var status = '{{ data.mr_status_id }}';

// console.log(d.days()+\" วัน \"+ d.hours()+\" ชม. \"+d.minutes()+\" นาที \"+ d.seconds() + \" วินาที\");

// initial Page
initialPage(status);
setForm(init);
setFormReplace(init_replace);
check_type_send(parseInt(type_work));


//console.log(hoursArr);
//calculateDuration(timeArr,hoursArr);


if (!!branch_id) {
    \$('#branch_send').val(parseInt(branch_id)).trigger('change');
}

if (!!real_branch_id) {
    \$('#real_branch_send').val(parseInt(real_branch_id)).trigger('change');
}

if (!!floor_id) {
    \$('#floor_send').val(parseInt(branch_id)).trigger('change');
}
if (!!mr_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}

if (!!real_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}


\$(\".alert\").alert();

\$('#name_receiver_select').select2({
    placeholder: \"ค้นหาผู้รับ\",
   
    ajax: {
        url: \"ajax/ajax_getdataemployee_select.php\",
        dataType: \"json\",
        delay: 250,
        processResults: function (data) {
            return {
                results : data
            };
        },
        cache: true
    },
    
}).on('select2:select', function(e) {
    //console.log(e.params.data)
    setForm(e.params.data);
});

var frmValid = \$('#frmBranch').validate({
    onsubmit: false,
    onkeyup: false,
    errorClass: \"is-invalid\",
    highlight: function (element) {
        if (element.type == \"radio\" || element.type == \"checkbox\") {
            \$(element).removeClass('is-invalid')
        } else {
            \$(element).addClass('is-invalid')
        }
    },
    rules: {
        telSender: {
            required: false
        },
        type_send: {
            required: true
        },
        nameReceiver: {
            required: true
        },
        telReceiver: {
            required: false
        },
        floorRealReceiver: {
            required: {
                depends: function (elm) {
                    var f = \$('#floor_name').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if( (f == \" - \" || f == \"\") && type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        branchRealReceiver: {
            required: {
                depends: function (elm) {
                    var b = \$('#branch_receiver').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if((b == \" - \" || b == \"\") && type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topic: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topicBranch: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
    },
    messages: {
        telSender: {
            required: 'ระบุเบอร์ติดต่อ'
        },
        type_send: {
            required: 'ระบุประเภทผู้รับ'
        },
        nameReceiver: {
            required: 'กรุณาเลือกผู้รับ'
        },
        telReceiver: {
            required: 'ระบุเบอร์ติดต่อผู้รับ'
        },
        branchRealReceiver: {
            required: 'ระบุสาขาผู้รับ'
        },
        topic: {
            required: 'ระบุชื่อเอกสาร'
        },
        topicBranch: {
            required: 'ระบุชื่อเอกสาร'
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        var placement = \$(element).data('error');
        if (placement) {
            \$(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});

\$('#btn_update').click(function() {
     var _frm = \$('#frmBranch');
    if(\$('#frmBranch').valid()) {
       
        \$.ajax({
            url: _frm.attr('action'),
            type: 'POST',
            data: {
                form: _frm.serializeArray(),
                actions: 'update',
                wid: '{{ wid }}'
            },
            dataType: 'json',
            success: function(res) {
                if(res.status == \"success\") {
                    alertify.alert(res['messeges'], function() {
                        location.reload();
                    });
                }
            }
        })
    }
});


\$(\"#btn_cancel\").click(function(){
    alertify.confirm('ยืนยันจะยกเลิกรายการนำส่งนี้', function() {
        \$.post('./ajax/ajax_updateWorkInfo.php', { actions: 'cancel', wid: '{{ wid }}'}, function(res){
            if(res.status == \"success\") {
                        alertify.alert(res['messeges'], function() {
                            location.reload();
                        });
                    }
        }, 'json');
    })
   
});


{% endblock %}



{% block javaScript %}


// \$('#type_send_1').prop('checked',true);

function check_type_send( type_send ){
    if( type_send == 1 ){
        \$(\"#type_1\").show();
        \$(\"#type_2\").hide();

        \$('#type_send_1').prop('checked', true);
        \$('#type_send_2').prop('checked', false);
    }else{
        \$(\"#type_1\").hide();
        \$(\"#type_2\").show();
        
        \$('#type_send_2').prop('checked', true);
        \$('#type_send_1').prop('checked', false);
    }
}

function check_name_re() {
    var pass_emp = \$(\"#pass_emp\").val();
    if(pass_emp == \"\" || pass_emp == null){
        \$(\"#div_re_text\").hide();
        \$(\"#div_re_select\").show();
    }else{
        \$(\"#div_re_text\").show();
        \$(\"#div_re_select\").hide();
    }
}

function setForm(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            \$(\"#depart_receiver\").val(res['department']);
            \$(\"#emp_id\").val(res['mr_emp_id']);
            \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#place_receiver\").val(res['mr_workarea']);
            \$(\"#floor_name\").val(res['mr_department_floor']);
            \$(\"#branch_receiver\").val(res['branch']);
        }
    })
}

function setFormReplace(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            console.log(res);
            // \$(\"#depart_receiver\").val(res['department']);
            // \$(\"#emp_id\").val(res['mr_emp_id']);
            // \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#real_place_receiver\").val(res['mr_workarea']);
            \$(\"#real_floor_name\").val(res['mr_department_floor']);
            \$(\"#real_branch_receiver\").val(res['branch']);
        }
    })
}

var saveBranch = function (obj) {
    return \$.post('ajax/ajax_save_work_branch.php', obj);
}

function initialPage(status) {
    //console.log(status)
    if(parseInt(status) != 7) {
        \$('input').prop('disabled', true);
        \$('select').prop('disabled', true);
        \$('textarea').prop('disabled', true);
        \$('#btn_update').prop('disabled', true);
        \$('#btn_cancel').prop('disabled', true);
    }
}

{% endblock %}
{% block Content %}

<div class=\"container\">



<br>
<br>
<br>

  
  
  
  





    <div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
        <label>
            <h3><b>รายละเอียดรายการนำส่ง</b></h3>
        </label>
    </div>
    <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
    <input type=\"hidden\" id=\"emp_id\" value=\"\">
    <input type=\"hidden\" id=\"barcode\" name=\"barcode\" value=\"{{ barcode }}\">
    
    <div>
        <p class=\"font-weight-bold\"><span class=\"text-muted\">เลขที่เอกสาร: </span> <span id=\"workNumber\">{{ data.mr_work_barcode }}</span> <span class=\"text-muted\">( {{ data.mr_status_name }} )</span></p>
    </div>
    

    <div class=\"form-group\"  id=\"detail_sender_head\">
        <h4>สถานะเอกสาร </h4>
    </div>

    {% if data.mr_status_id != 6 %}
    <div class=\"demo\"><br><br><br><br>
        <ul class=\"list-unstyled multi-steps\">
\t\t\t {% for st in status %}
\t\t\t\t\t\t\t
\t\t\t\t {# {% if st.active != \"\" %} #}
\t\t\t\t\t<li class=\"{% if end_step == loop.index %} is-active {% endif %}{% if st.active != \"\" %} coloe_compress {% endif %}\">
\t\t\t\t\t\t<strong>{{ st.emp_code }}</strong><br />
\t\t\t\t\t\t<small>{{ st.name }}</small>
\t\t\t\t\t\t<div class=\"divTimes\">
\t\t\t\t\t\t\t\t<small>{{st.date}}</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t{% endfor %}
\t\t\t </ul>
    </div>
    {% else %}
    <div style=\"text-align:center;\">
        <p class=\"text-muted\">
            <i class=\"material-icons\">block</i>
            <br/>
            <strong>ยกเลิกรายการนำส่ง</strong>
        </p>
    </div>
\t<br>
\t<br>
    {% endif %}
        
     <!-- <div class='clearfix'></div> -->
    
     <form action=\"./ajax/ajax_updateWorkInfo.php\" method=\"POST\" id=\"frmBranch\">
        <div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
            <h4>รายละเอียดผู้ส่ง </h4>
        </div>
\t\t{% if send_data.0.mr_user_role_id == 5%}
        <div class=\"form-group\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    สาขาผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"placeholder=\"ชื่อสาขา\" value=\"{{ send_data.0.mr_branch_code }} - {{ send_data.0.mr_branch_name }}\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"เบอร์โทร\" value=\"{{  send_data.0.tell  }}\" readonly>
                </div>
            </div>
\t\t\t<div class=\"form-group\" id=\"div_re_select\"><br>
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tชั้น :             </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" value=\"{{  send_data.0.mr_branch_floor  }}\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tตำแหน่ง
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" value=\"{{  send_data.0.mr_position_code  }} - {{  send_data.0.mr_position_name  }}\" readonly>
                </div>
            </div>
        </div>
        </div>
\t\t{% else %}
\t\t <div class=\"form-group\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    แผนก : 
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"  placeholder=\"ชื่อสาขา\" value=\"{{ send_data.0.mr_department_code }} - {{ send_data.0.mr_department_name }}\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"  placeholder=\"เบอร์โทร\" value=\"{{  send_data.0.mr_emp_tel  }}\" readonly>
                </div>
            </div>
\t\t\t<div class=\"form-group\" id=\"div_re_select\"><br>
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tชั้น :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" value=\"{{  send_data.0.floor_name  }}\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tตำแหน่ง
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" value=\"{{  send_data.0.mr_position_code  }} - {{  send_data.0.mr_position_name  }}\" readonly>
                </div>
            </div>
        </div>
        </div>
\t\t
\t\t{% endif %}

        <div class=\"form-group\" id=\"div_re_select\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\tชื่อผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" value=\"{{  send_data.0.mr_emp_code  }} - {{  send_data.0.mr_emp_name  }} {{  send_data.0.mr_emp_lastname  }} {{  send_data.0.mr_cus_name  }}\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                </div>
            </div>
        </div>
\t\t
\t\t
\t\t
\t\t

        <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
            <h4>รายละเอียดผู้รับ</h4>
        </div>
        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-2\" style=\"padding-right:0px\">
                    ประเภทการส่ง :
                </div>

                <div class=\"col-2\" style=\"padding-left:0px\">
\t\t\t\t{{send_data.0.mr_type_work_name}}
                </div>
            </div>
        </div>

        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ผู้รับ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <span class=\"box_error\" id=\"err_receiver_select\"></span>
\t\t\t\t\t
\t\t\t\t\t <input type=\"text\" class=\"form-control form-control-sm\"   value=\"{{ resive_data.0.mr_emp_code }} - {{ resive_data.0.mr_emp_name }} {{ resive_data.0.mr_emp_lastname }}\"
                    placeholder=\"เบอร์โทร\" readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\"   value=\"{{ resive_data.0.tell }}\"
                        placeholder=\"เบอร์โทร\" readonly>
                </div>
            </div>
\t\t\t<br>
            
        </div>

        <div id=\"type_1\" style=\"display:;\">
\t\t{% if resive_data.0.mr_type_work_id == 1 %}
        <div class=\"row\">
            <div class=\"col-1\" style=\"padding-right:0px\">
                ตำแหน่ง :
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
                <span class=\"box_error\" id=\"err_receiver_select\"></span>
                
                 <input type=\"text\" class=\"form-control form-control-sm\"   value=\"{{ resive_data.0.mr_position_code }} - {{ resive_data.0.mr_position_name }}\"
                placeholder=\"เบอร์โทร\" readonly>
            </div>
            <div class=\"col-1\" style=\"padding-right:0px\">
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
            </div>
        </div><br>
            <div class=\"form-group\" id=\"\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tแผนก :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"{{resive_data.0.mr_department_code}}-{{resive_data.0.mr_department_name}}\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"{{resive_data.0.floor_name}}\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>
\t\t{% elseif resive_data.0.mr_type_work_id == 3 %}
        <div class=\"row\">
            <div class=\"col-1\" style=\"padding-right:0px\">
                ตำแหน่ง :
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
                <span class=\"box_error\" id=\"err_receiver_select\"></span>
                
                 <input type=\"text\" class=\"form-control form-control-sm\"   value=\"{{ resive_data.0.mr_position_code }} - {{ resive_data.0.mr_position_name }}\"
                placeholder=\"เบอร์โทร\" readonly>
            </div>
            <div class=\"col-1\" style=\"padding-right:0px\">
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
            </div>
        </div><br>
\t\t\t<div class=\"form-group\" id=\"\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tแผนก :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\"  value=\"{{resive_data.0.mr_department_code}}-{{resive_data.0.mr_department_name}}\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"{{resive_data.0.floor_name}}\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>
        {% elseif resive_data.0.mr_type_work_id == 4 %}
            
\t\t{% else %}
        <div class=\"row\">
            <div class=\"col-1\" style=\"padding-right:0px\">
                ตำแหน่ง :
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
                <span class=\"box_error\" id=\"err_receiver_select\"></span>
                
                 <input type=\"text\" class=\"form-control form-control-sm\"   value=\"{{ resive_data.0.mr_position_code }} - {{ resive_data.0.mr_position_name }}\"
                placeholder=\"เบอร์โทร\" readonly>
            </div>
            <div class=\"col-1\" style=\"padding-right:0px\">
            </div>
            <div class=\"col-5\" style=\"padding-left:0px\">
            </div>
        </div><br>
\t\t<div class=\"form-group\" id=\"\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tสาขา :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\"  value=\"{{resive_data.0.mr_branch_code}}-{{resive_data.0.mr_branch_name}}\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" value=\"{{resive_data.0.mr_branch_floor}}\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>
       {% endif %}
            <div class=\"form-group\" id=\"\">
                <div class=\"row\">                
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>{{ resive_data.0.mr_topic }}</textarea>
                    </div>
                
\t\t\t\t\t<div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\tหมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>{{ resive_data.0.mr_work_remark }}</textarea>
                    </div>

            </div> <br>  
                <div class=\"row\">  
                               
                    <br>              
                    <div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\tจำนวน :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly value=\"{{ resive_data.0.quty }}\">
                    </div>
                
\t\t\t\t\t

            </div>
        {% if resive_data.0.mr_type_work_id == 4 %}
        <br>
            <div class=\"row\">  
               
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ที่อยู่ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>{{ resive_data.0.mr_address }}</textarea>
                </div>
            </div>
            
        
            <div class=\"row\">
                <hr>
                <br>
                <br>
                <div class=\"col-12\" style=\"padding-left:0px\">
                    <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
                        <h4>ข้อมูลผู้ลงชื่อรับ (ลายเซ็น)</h4>
                    </div>\t
                     <div class=\"col-12\" style=\"padding-right:0px\">
                        สถานะจักส่ง :
                    </div>
                    <div class=\"col-12\">
                        <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly value=\"{{ resive_data.0.mr_work_byhand_status_send_name }}\">
                    </div>
                    <div class=\"col-12\" style=\"padding-right:0px\">
                        หมายเหตุ(จากพนักงานส่ง) 
                     </div>
                    <div class=\"col-12\" style=\"padding-right:0px\">
                            <textarea class=\"form-control form-control-sm\" placeholder=\"หมายเหตุ\" readonly>{{ resive_data.0.send_remark }}</textarea>
                        </div>
                    <hr>
                    {% if signatures != '' %}
                     <div class=\"text-center\">
                        <img src=\"{{signatures}}\" class=\"rounded border border-dark\" alt=\"...\" width=\"400px\">
                      </div>
                    {% endif %}
                </div>
            </div>
        {% else %}
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t<h4>ข้อมูลผู้ลงชื่อรับ</h4>
\t\t\t</div>\t

\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\"  value=\"{{log_conf.0.mr_emp_code}}-{{log_conf.0.mr_emp_name}}  {{log_conf.0.mr_emp_lastname}}\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
       {% endif %}

        </div>
\t\t<!-- end type 1-->
        <hr/>
\t\t{# <a href=\"#\">ดูประวัติ</a> #}
\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t<h4>ดูประวัติ</h4>
\t\t</div>\t
\t\t<div id=\"history_work\">
\t\t\t<table class=\"table\">
\t\t\t  <thead>
\t\t\t\t<tr>
\t\t\t\t  <th scope=\"col\">NO</th>
\t\t\t\t  <th scope=\"col\">วันที่</th>
\t\t\t\t  <th scope=\"col\">ผู้ใช้</th>
\t\t\t\t  <th scope=\"col\">สถานะ</th>
\t\t\t\t  <th scope=\"col\">รอบ</th>
\t\t\t\t  <th scope=\"col\">หมายเหตุ</th>
\t\t\t\t</tr>
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t\t<tr>
\t\t\t\t{% for l in log %}
\t\t\t\t  <th scope=\"row\">{{loop.index}}</th>
\t\t\t\t  <td>{{ l.sys_timestamp}}</td>
\t\t\t\t  <td>{{ l.mr_emp_code}} {{l.mr_emp_name}}  {{l.mr_emp_lastname}}</td>
\t\t\t\t  <td>{{ l.mr_status_name }}</td>
\t\t\t\t  <td>{{ l.mr_round_name}}</td>
\t\t\t\t  <td>{{ l.remark}}</td>
\t\t\t\t</tr>
\t\t\t\t{% endfor%}
\t\t\t  </tbody>
\t\t\t</table>
            {% if resive_data.0.mr_type_work_id == 2 %}
                {% if signatures_sendbranch != '' %}
                     <div class=\"text-center\">
                        <img src=\"{{signatures_sendbranch}}\" class=\"rounded border border-dark\" alt=\"...\" width=\"400px\">
                      </div>
                {% else %}
                    <div class=\"text-center\">
                           
                    </div>
                {% endif %}
            {% endif %}
\t\t</div>
\t\t
<br>
<br>
<br>
<br>
     </form>
</div>
{% endblock %}


{% block debug %}

{% if debug != '' %}
<pre>{{ debug }}</pre>
{% endif %}

{% endblock %}
", "mailroom/work_info.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\mailroom\\work_info.tpl");
    }
}

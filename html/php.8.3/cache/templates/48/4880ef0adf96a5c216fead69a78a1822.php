<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/summary_HO_to_branch.tpl */
class __TwigTemplate_86b8481255817dc1078c9b7d24c82421 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/summary_HO_to_branch.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
       width: 100%;
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
      }
    
      .input-daterange {
          width: 450px;
      }

      #tb_summary,
      #tb_summary th{
          text-align:center;
          font-weight: bold;
      }

    .loading {
          position: fixed;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          z-index: 1000;
      }


      .img_loading {
            width: 350px;
            height: auto;
            
      }
      
";
    }

    // line 62
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "
<link rel=\"stylesheet\" href=\"../themes/datepicker/bootstrap-datepicker3.min.css\">
<script type='text/javascript' src=\"../themes/datepicker/bootstrap-datepicker.min.js\"></script>
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
";
    }

    // line 70
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "\t\$('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
\t});

    var start = '';
    var end = '';
    getSummary(start, end);

    \$('#btn_search').on('click', function() {
        var start = \$('#date_start').val();
        var end = \$('#date_end').val();
        getSummary(start, end);
    }); 

    \$('#btn_clear').on('click', function() {
        \$('#date_start').val(\"\");
        \$('#date_end').val(\"\");
        var start = '';
        var end = '';
         getSummary(start, end);
    });
\t
var tbl_data = \$('#tb_summary').DataTable({ 
\t\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'floor_name_main'},
        {'data': '1'},
        {'data': '2'},
        {'data': '3'},
        {'data': '4'},
        {'data': '13'},
        {'data': '5'},
        {'data': '6'},
        {'data': '15'},
        {'data': 'sum'}
    ]
});
";
    }

    // line 118
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 119
        echo "    var getSummary = function(start, end) {
        \$.ajax({
            url: './ajax/ajax_get_summary_HO_to_branch.php',
            method: 'POST',
            data: { start_date: start, end_date: end },
            dataType: 'json',
            beforeSend: function() {
                 \$('.loading').show();
            },
            success: function(res) {
                \$('.loading').hide();
                if(res.status == 200){
                    \$('#tb_summary').DataTable().clear().draw();
\t\t\t\t\t\$('#tb_summary').DataTable().rows.add(res.data).draw();
                }else{
                    alertify.alert('ผิดพลาด',res.message,function(){window.location.reload();});
                }
            }
        });
    }

    var getDetail = function(floor, status) {
        var obj = {};
        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        const uri = 'detail_work_sender_inout.php?param=';
        obj.mr_department_floor = floor;
        obj.status_id = status;
        obj.start_date = start_date;
        obj.end_date = end_date;
        param = JSON.stringify(obj); // encode parameter base64
        //window.open(uri+param, '_blank', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1000,height=350');
        window.open(uri+param, '_blank');
    }
\t
";
    }

    // line 156
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 157
        echo "      <div class=\"card\">
            <div class='card-header'>
               <b>ติดตามงาน</b>
            </div>
               
            <div class=\"card-body\">
               <form class='form-inline'>
                   <label><b>ช่วงวันที่ : </b>&emsp;</label>
                   <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">
                        <div class=\"input-group input-daterange\">
                            <input type=\"text\" class=\"form-control\" id='date_start' placeholder='วันที่เริ่มต้น'>
                            <div class=\"input-group-addon\" style='background-color: #fff; border-color: #fff; padding: 0px 10px;'><b>ถึง</b></div>
                            <input type=\"text\" class=\"form-control\" id='date_end' placeholder='วันที่สิ้นสุด'>
                        </div>
                   </div>
                   <button type='button' class='btn btn-primary' id='btn_search'><b>ค้นหา</b></button>&nbsp;&nbsp;
                   <button type='button' class='btn btn-secondary' id='btn_clear'><b>เคลียร์</b></button>
               </form>
            </div>
      </div>

      <div class='card' style='margin-top: 20px;'>
        <div class='card-header'>
            <b>สถานะงาน</b>
        </div>        
        <div class='card-body'>
            <table id='tb_summary' class=\"table table-responsive table-bordered table-striped table-sm\" cellspacing=\"0\">
                <thead class=\"thead-inverse\">
                    <tr>
                        <th>ลำดับ</th>
                        <th width=\"200\">ชั้น</th>
                        <th>รอพนักงานเดินเอกสาร</th>
                        <th>พนักงานเดินเอกสารรับเอกสารแล้ว</th>
                        <th>เอกสารถึงห้อง Mailroom</th>
                        <th>อยู่ในระหว่างนำส่ง</th>
                       
                        <th>Mailroom ถึง Hub</th>
\t\t\t\t\t\t<th>เอกสารถึงมือผู้รับแล้ว</th>
                        <th>ยกเลิกการจัดส่ง</th>
                        <th>เอกสารตีกลับ</th>
                        <th>รวม</th>
                    </tr>
                </thead>
                <tbody id='show_data'>
\t\t\t\t</tbody>
            </table>
           <div class='loading'>
                <img src=\"../themes/images/loading.gif\" class='img_loading'>
            </div>
        </div>
      </div>
\t
";
    }

    // line 212
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 214
        if ((($context["debug"] ?? null) != "")) {
            // line 215
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 217
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/summary_HO_to_branch.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 217,  306 => 215,  304 => 214,  297 => 212,  241 => 157,  237 => 156,  198 => 119,  194 => 118,  146 => 71,  142 => 70,  133 => 63,  129 => 62,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
       width: 100%;
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
      }
    
      .input-daterange {
          width: 450px;
      }

      #tb_summary,
      #tb_summary th{
          text-align:center;
          font-weight: bold;
      }

    .loading {
          position: fixed;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          z-index: 1000;
      }


      .img_loading {
            width: 350px;
            height: auto;
            
      }
      
{% endblock %}
{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/datepicker/bootstrap-datepicker3.min.css\">
<script type='text/javascript' src=\"../themes/datepicker/bootstrap-datepicker.min.js\"></script>
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
{% endblock %}

{% block domReady %}
\t\$('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
\t});

    var start = '';
    var end = '';
    getSummary(start, end);

    \$('#btn_search').on('click', function() {
        var start = \$('#date_start').val();
        var end = \$('#date_end').val();
        getSummary(start, end);
    }); 

    \$('#btn_clear').on('click', function() {
        \$('#date_start').val(\"\");
        \$('#date_end').val(\"\");
        var start = '';
        var end = '';
         getSummary(start, end);
    });
\t
var tbl_data = \$('#tb_summary').DataTable({ 
\t\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'floor_name_main'},
        {'data': '1'},
        {'data': '2'},
        {'data': '3'},
        {'data': '4'},
        {'data': '13'},
        {'data': '5'},
        {'data': '6'},
        {'data': '15'},
        {'data': 'sum'}
    ]
});
{% endblock %}


{% block javaScript %}
    var getSummary = function(start, end) {
        \$.ajax({
            url: './ajax/ajax_get_summary_HO_to_branch.php',
            method: 'POST',
            data: { start_date: start, end_date: end },
            dataType: 'json',
            beforeSend: function() {
                 \$('.loading').show();
            },
            success: function(res) {
                \$('.loading').hide();
                if(res.status == 200){
                    \$('#tb_summary').DataTable().clear().draw();
\t\t\t\t\t\$('#tb_summary').DataTable().rows.add(res.data).draw();
                }else{
                    alertify.alert('ผิดพลาด',res.message,function(){window.location.reload();});
                }
            }
        });
    }

    var getDetail = function(floor, status) {
        var obj = {};
        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        const uri = 'detail_work_sender_inout.php?param=';
        obj.mr_department_floor = floor;
        obj.status_id = status;
        obj.start_date = start_date;
        obj.end_date = end_date;
        param = JSON.stringify(obj); // encode parameter base64
        //window.open(uri+param, '_blank', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1000,height=350');
        window.open(uri+param, '_blank');
    }
\t
{% endblock %}

{% block Content2 %}
      <div class=\"card\">
            <div class='card-header'>
               <b>ติดตามงาน</b>
            </div>
               
            <div class=\"card-body\">
               <form class='form-inline'>
                   <label><b>ช่วงวันที่ : </b>&emsp;</label>
                   <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">
                        <div class=\"input-group input-daterange\">
                            <input type=\"text\" class=\"form-control\" id='date_start' placeholder='วันที่เริ่มต้น'>
                            <div class=\"input-group-addon\" style='background-color: #fff; border-color: #fff; padding: 0px 10px;'><b>ถึง</b></div>
                            <input type=\"text\" class=\"form-control\" id='date_end' placeholder='วันที่สิ้นสุด'>
                        </div>
                   </div>
                   <button type='button' class='btn btn-primary' id='btn_search'><b>ค้นหา</b></button>&nbsp;&nbsp;
                   <button type='button' class='btn btn-secondary' id='btn_clear'><b>เคลียร์</b></button>
               </form>
            </div>
      </div>

      <div class='card' style='margin-top: 20px;'>
        <div class='card-header'>
            <b>สถานะงาน</b>
        </div>        
        <div class='card-body'>
            <table id='tb_summary' class=\"table table-responsive table-bordered table-striped table-sm\" cellspacing=\"0\">
                <thead class=\"thead-inverse\">
                    <tr>
                        <th>ลำดับ</th>
                        <th width=\"200\">ชั้น</th>
                        <th>รอพนักงานเดินเอกสาร</th>
                        <th>พนักงานเดินเอกสารรับเอกสารแล้ว</th>
                        <th>เอกสารถึงห้อง Mailroom</th>
                        <th>อยู่ในระหว่างนำส่ง</th>
                       
                        <th>Mailroom ถึง Hub</th>
\t\t\t\t\t\t<th>เอกสารถึงมือผู้รับแล้ว</th>
                        <th>ยกเลิกการจัดส่ง</th>
                        <th>เอกสารตีกลับ</th>
                        <th>รวม</th>
                    </tr>
                </thead>
                <tbody id='show_data'>
\t\t\t\t</tbody>
            </table>
           <div class='loading'>
                <img src=\"../themes/images/loading.gif\" class='img_loading'>
            </div>
        </div>
      </div>
\t
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/summary_HO_to_branch.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\summary_HO_to_branch.tpl");
    }
}

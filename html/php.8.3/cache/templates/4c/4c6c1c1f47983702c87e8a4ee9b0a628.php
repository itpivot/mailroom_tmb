<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/report_receive.tpl */
class __TwigTemplate_c8286c88d0e509d1ecbdcaef7c04c805 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/report_receive.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 8
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "\t<div class=\"row\" >
\t\t<div class=\"col-1\">
\t\t</div>
\t\t<div class=\"col-10\">
\t\t\t<div class=\"card\" style=\"width:100%\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">รายงานสรุปการรับเอกสาร</h4>
\t\t\t\t";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["month"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 17
            echo "\t\t\t\t\t<a href=\"excel.report_receive.php?&month=";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "month_number", [], "any", false, false, false, 17), "html", null, true);
            echo "&year=";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "year", [], "any", false, false, false, 17), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "month", [], "any", false, false, false, 17), "html", null, true);
            echo "</a>
\t\t\t\t\t<br />
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>\t
\t</div>\t
\t

";
    }

    // line 29
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 31
        if ((($context["debug"] ?? null) != "")) {
            // line 32
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 34
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/report_receive.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 34,  113 => 32,  111 => 31,  104 => 29,  94 => 20,  80 => 17,  76 => 16,  67 => 9,  63 => 8,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block Content %}
\t<div class=\"row\" >
\t\t<div class=\"col-1\">
\t\t</div>
\t\t<div class=\"col-10\">
\t\t\t<div class=\"card\" style=\"width:100%\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">รายงานสรุปการรับเอกสาร</h4>
\t\t\t\t{% for m in month%}
\t\t\t\t\t<a href=\"excel.report_receive.php?&month={{m.month_number}}&year={{m.year}}\" target=\"_blank\">{{ m.month }}</a>
\t\t\t\t\t<br />
\t\t\t\t{% endfor %}
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>\t
\t</div>\t
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/report_receive.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\report_receive.tpl");
    }
}

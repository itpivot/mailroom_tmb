<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/reciever_branch.tpl */
class __TwigTemplate_5ea77de26d83318907a3855159c1d304 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/reciever_branch.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }


";
    }

    // line 35
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
";
    }

    // line 44
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "\t\t\$(\"#barcode\").focus();
\t\t\$(\"#btn_save\").click(function(){
\t\t\tload_data();
\t\t});\t
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'pageLength': '100',
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'sys_timestamp'},
\t\t\t\t{ 'data':'supper_barcode' },
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'mr_status_name' },
\t\t\t\t{ 'data':'acctiom' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});

\t\t\t
\t\t\t
\t\t
\t\t//load_all_data();
";
    }

    // line 72
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "\t\tfunction cancle_work(main_id,remark){\t\t\t\t\t\t
\t\t\tvar r = confirm(\"ยืนยันการยกเลิกการส่งเอกสาร!\");
\t\t\tif (r == true) {
\t\t\t\t\$.post(
\t\t\t\t\t'./ajax/ajax_cancle_by_mailroom.php',
\t\t\t\t\t{
\t\t\t\t\t\tmr_work_main_id \t\t : main_id,
\t\t\t\t\t\tremark_can \t\t\t\t : 'ยกเลิกการจัดส่ง(ไม่พบเอกสารมาจากสาขา)',
\t\t\t\t\t\tremark \t\t\t\t\t : remark,
\t\t\t\t\t},
\t\t\t\t\tfunction(res) {
\t\t\t\t\t\talert(res.message);
\t\t\t\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\t\t
\t\t\t\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\t\t\t//load_all_data();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t}
\t\t\t\t\t},'json'
\t\t\t\t);
\t\t\t}\t\t\t\t
\t\t}

\t\tfunction update_send_work(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});

\t\t\t\t}
\t\t\tif( status === true ){
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: \"ajax/ajax_update_send_work.php\",
\t\t\t\t\t\ttype: \"post\",
\t\t\t\t\t\tdataType:'json',
\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t\t},
\t\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t\t}).done(function( msg ) {

\t\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert(msg.message);
\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t\t
\t\t\t\t}
\t\t}
\t\t
\tfunction load_data(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\tstatus = false;
\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t}
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_load_data_reciever_branch.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType:'json',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t},
\t\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();\t\t
\t\t\t\t\t}
\t\t\t\t}).done(function( msg ) {
\t\t\t\t\t
\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t\t\t//alert('22222');
\t\t\t\t\t}else{
\t\t\t\t\t\t\$('#barcode').val('');
\t\t\t\t\t\t\$('#barcode').focus();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t//alertify.alert(msg.message);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}

\t\tfunction load_all_data(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_load_data_reciever_branch_all.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType:'json',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t},
\t\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t}).done(function( msg ) {
\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t//\$('#bg_loader').hide();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert(msg.message);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}
\t\t
\t\tfunction update_barcode() {
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar sub_barcode \t\t= \$(\"#sub_barcode\").val();
\t\t\tvar tnt_barcode \t\t= \$(\"#tnt_barcode\").val();
\t\t\tvar mr_round_id \t\t= \$(\"#mr_round_id\").val();
\t\t\tvar status \t\t\t\t= true;

\t\t\t
\t\t\tif(sub_barcode == \"\" || sub_barcode == null){
\t\t\t\t\$('#sub_barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\tstatus = false;
\t\t\t}else{
\t\t\t\tstatus = true;
\t\t\t}
\t\t\t\$(\"#sub_barcode\").val('');
\t\t
\t\t\t//console.log(status);
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_update_work_branch_by_barcode.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'mr_round_id': mr_round_id,
\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'sub_barcode': sub_barcode,
\t\t\t\t\t\t'tnt_barcode': tnt_barcode
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tvar str = '';
\t\t\t\t\t\tvar str0 = '';
\t\t\t\t\t\tvar str1 = '<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\tvar str2 = '<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\t\tstr0+='<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">';
\t\t\t\t\t\t\tstr0+='<span aria-hidden=\"true\">&times;</span>';
\t\t\t\t\t\t\tstr0+='</button>'
\t\t\t\t\t\t\tstr0+='<strong>  '+res.title+'  </strong>';
\t\t\t\t\t\t\tstr0+= res.message;
\t\t\t\t\t\t\tstr0+='</div>';
\t\t\t\t\t\t\tif(res.status == 200){\t
\t\t\t\t\t\t\t\t\$(\"#sub_barcode\").val('');
\t\t\t\t\t\t\t\tstr = str1+str0;
\t\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\tstr = str2+str0;
\t\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\$('#alert_').html(str);\t
\t\t\t\t\t\t// if(!!res){
\t\t\t\t\t\t// \tvar str = '';
\t\t\t\t\t\t// \tvar str0 = '';
\t\t\t\t\t\t// \tvar str1 = '<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\t// \tvar str2 = '<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\t// \t\tstr0+='<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">';
\t\t\t\t\t\t// \t\tstr0+='<span aria-hidden=\"true\">&times;</span>';
\t\t\t\t\t\t// \t\tstr0+='</button>'
\t\t\t\t\t\t// \t\tstr0+='<strong>  '+res['st']+'  </strong>';
\t\t\t\t\t\t// \t\tstr0+= res['msg'];
\t\t\t\t\t\t// \t\tstr0+='</div>';
\t\t\t\t\t\t// \t\tif(res['st']=='success'){\t
\t\t\t\t\t\t// \t\t\t\$(\"#sub_barcode\").val('');
\t\t\t\t\t\t// \t\t\tstr = str1+str0;
\t\t\t\t\t\t// \t\t\tload_data();
\t\t\t\t\t\t// \t\t}else{
\t\t\t\t\t\t// \t\t\tstr = str2+str0;
\t\t\t\t\t\t// \t\t}
\t\t\t\t\t\t// \t\t\$('#alert_').html(str);\t
\t\t\t\t\t\t// }
\t\t\t\t\t\t\$('#sub_barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t}\t\t\t\t\t\t\t\t
\t\t\t\t});\t
\t\t\t}
\t\t}

\t\tfunction keyEvent( evt, barcode )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tload_data();
\t\t\t\t\$(\"#sub_barcode\").focus();
\t\t\t}
\t\t}
\t\tfunction keyEvent_subbarcode( evt )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tupdate_barcode();
\t\t\t}
\t\t}


";
    }

    // line 295
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 296
        echo "\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t</div>
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">รับเอกสารจากสาขา</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t<label>วันที่</label>
\t\t\t\t\t\t\t\t<input type=\"text\" value=\"";
        // line 306
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" class=\"form-control mb-sm-0\" id=\"today\" placeholder=\"To day\" readonly>\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<label>รอบเอกสาร</label>
\t\t\t\t\t\t\t\t\t <select class=\"form-control\" id=\"mr_round_id\">
\t\t\t\t\t\t\t\t\t\t ";
        // line 311
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 312
            echo "\t\t\t\t\t\t\t\t\t\t <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 312), "html", null, true);
            echo "\">รับ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_type_work_name", [], "any", false, false, false, 312), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 312), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 314
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t\t\t\t\t<label>เลขที่ TNT</label>
\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"tnt_barcode\" placeholder=\"TNT Barcode\"><br>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-start\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t\t\t\t<label>รหัสบาร์โค้ดใบปะหน้าซองใหญ่</label>
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-sm-0\" id=\"barcode\" placeholder=\"Barcode\" onkeyup=\"keyEvent(event,this.value);\">\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_save\">ค้นหา</button>
\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t\t\t\t<label>เลขที่เอกสาร</label>
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"sub_barcode\" placeholder=\"Barcode\" onkeyup=\"keyEvent_subbarcode(event);\"><br>
\t\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" onclick=\"update_barcode();\">บันทึก</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<div class=\"col\" id=\"alert_\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>Date/Time</th>
\t\t\t\t\t\t\t\t<th>ใบคุม</th>
\t\t\t\t\t\t\t\t<th>Bacode</th>
\t\t\t\t\t\t\t\t<th>Sender</th>
\t\t\t\t\t\t\t\t<th>Receiver</th>
\t\t\t\t\t\t\t\t<th>Type Work</th>
\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t<th>Actiom</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t\t";
        // line 380
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t

";
    }

    // line 397
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 399
        if ((($context["debug"] ?? null) != "")) {
            // line 400
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 402
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/reciever_branch.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  516 => 402,  510 => 400,  508 => 399,  501 => 397,  483 => 380,  419 => 314,  406 => 312,  402 => 311,  394 => 306,  382 => 296,  378 => 295,  154 => 73,  150 => 72,  120 => 45,  116 => 44,  106 => 36,  102 => 35,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }


{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
{% endblock %}

{% block domReady %}
\t\t\$(\"#barcode\").focus();
\t\t\$(\"#btn_save\").click(function(){
\t\t\tload_data();
\t\t});\t
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'pageLength': '100',
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'sys_timestamp'},
\t\t\t\t{ 'data':'supper_barcode' },
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'mr_status_name' },
\t\t\t\t{ 'data':'acctiom' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});

\t\t\t
\t\t\t
\t\t
\t\t//load_all_data();
{% endblock %}
{% block javaScript %}
\t\tfunction cancle_work(main_id,remark){\t\t\t\t\t\t
\t\t\tvar r = confirm(\"ยืนยันการยกเลิกการส่งเอกสาร!\");
\t\t\tif (r == true) {
\t\t\t\t\$.post(
\t\t\t\t\t'./ajax/ajax_cancle_by_mailroom.php',
\t\t\t\t\t{
\t\t\t\t\t\tmr_work_main_id \t\t : main_id,
\t\t\t\t\t\tremark_can \t\t\t\t : 'ยกเลิกการจัดส่ง(ไม่พบเอกสารมาจากสาขา)',
\t\t\t\t\t\tremark \t\t\t\t\t : remark,
\t\t\t\t\t},
\t\t\t\t\tfunction(res) {
\t\t\t\t\t\talert(res.message);
\t\t\t\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\t\t
\t\t\t\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\t\t\t//load_all_data();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t}
\t\t\t\t\t},'json'
\t\t\t\t);
\t\t\t}\t\t\t\t
\t\t}

\t\tfunction update_send_work(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});

\t\t\t\t}
\t\t\tif( status === true ){
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: \"ajax/ajax_update_send_work.php\",
\t\t\t\t\t\ttype: \"post\",
\t\t\t\t\t\tdataType:'json',
\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t\t},
\t\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t\t}).done(function( msg ) {

\t\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert(msg.message);
\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t\t
\t\t\t\t}
\t\t}
\t\t
\tfunction load_data(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\tstatus = false;
\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t}
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_load_data_reciever_branch.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType:'json',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t},
\t\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();\t\t
\t\t\t\t\t}
\t\t\t\t}).done(function( msg ) {
\t\t\t\t\t
\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t\t\t//alert('22222');
\t\t\t\t\t}else{
\t\t\t\t\t\t\$('#barcode').val('');
\t\t\t\t\t\t\$('#barcode').focus();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t//alertify.alert(msg.message);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}

\t\tfunction load_all_data(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_load_data_reciever_branch_all.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType:'json',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t},
\t\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t}).done(function( msg ) {
\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t//\$('#bg_loader').hide();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert(msg.message);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}
\t\t
\t\tfunction update_barcode() {
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar sub_barcode \t\t= \$(\"#sub_barcode\").val();
\t\t\tvar tnt_barcode \t\t= \$(\"#tnt_barcode\").val();
\t\t\tvar mr_round_id \t\t= \$(\"#mr_round_id\").val();
\t\t\tvar status \t\t\t\t= true;

\t\t\t
\t\t\tif(sub_barcode == \"\" || sub_barcode == null){
\t\t\t\t\$('#sub_barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\tstatus = false;
\t\t\t}else{
\t\t\t\tstatus = true;
\t\t\t}
\t\t\t\$(\"#sub_barcode\").val('');
\t\t
\t\t\t//console.log(status);
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_update_work_branch_by_barcode.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'mr_round_id': mr_round_id,
\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'sub_barcode': sub_barcode,
\t\t\t\t\t\t'tnt_barcode': tnt_barcode
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tvar str = '';
\t\t\t\t\t\tvar str0 = '';
\t\t\t\t\t\tvar str1 = '<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\tvar str2 = '<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\t\tstr0+='<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">';
\t\t\t\t\t\t\tstr0+='<span aria-hidden=\"true\">&times;</span>';
\t\t\t\t\t\t\tstr0+='</button>'
\t\t\t\t\t\t\tstr0+='<strong>  '+res.title+'  </strong>';
\t\t\t\t\t\t\tstr0+= res.message;
\t\t\t\t\t\t\tstr0+='</div>';
\t\t\t\t\t\t\tif(res.status == 200){\t
\t\t\t\t\t\t\t\t\$(\"#sub_barcode\").val('');
\t\t\t\t\t\t\t\tstr = str1+str0;
\t\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\tstr = str2+str0;
\t\t\t\t\t\t\t\tload_data();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\$('#alert_').html(str);\t
\t\t\t\t\t\t// if(!!res){
\t\t\t\t\t\t// \tvar str = '';
\t\t\t\t\t\t// \tvar str0 = '';
\t\t\t\t\t\t// \tvar str1 = '<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\t// \tvar str2 = '<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">';
\t\t\t\t\t\t// \t\tstr0+='<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">';
\t\t\t\t\t\t// \t\tstr0+='<span aria-hidden=\"true\">&times;</span>';
\t\t\t\t\t\t// \t\tstr0+='</button>'
\t\t\t\t\t\t// \t\tstr0+='<strong>  '+res['st']+'  </strong>';
\t\t\t\t\t\t// \t\tstr0+= res['msg'];
\t\t\t\t\t\t// \t\tstr0+='</div>';
\t\t\t\t\t\t// \t\tif(res['st']=='success'){\t
\t\t\t\t\t\t// \t\t\t\$(\"#sub_barcode\").val('');
\t\t\t\t\t\t// \t\t\tstr = str1+str0;
\t\t\t\t\t\t// \t\t\tload_data();
\t\t\t\t\t\t// \t\t}else{
\t\t\t\t\t\t// \t\t\tstr = str2+str0;
\t\t\t\t\t\t// \t\t}
\t\t\t\t\t\t// \t\t\$('#alert_').html(str);\t
\t\t\t\t\t\t// }
\t\t\t\t\t\t\$('#sub_barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t}\t\t\t\t\t\t\t\t
\t\t\t\t});\t
\t\t\t}
\t\t}

\t\tfunction keyEvent( evt, barcode )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tload_data();
\t\t\t\t\$(\"#sub_barcode\").focus();
\t\t\t}
\t\t}
\t\tfunction keyEvent_subbarcode( evt )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tupdate_barcode();
\t\t\t}
\t\t}


{% endblock %}

{% block Content2 %}
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t</div>
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">รับเอกสารจากสาขา</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t<label>วันที่</label>
\t\t\t\t\t\t\t\t<input type=\"text\" value=\"{{today}}\" class=\"form-control mb-sm-0\" id=\"today\" placeholder=\"To day\" readonly>\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<label>รอบเอกสาร</label>
\t\t\t\t\t\t\t\t\t <select class=\"form-control\" id=\"mr_round_id\">
\t\t\t\t\t\t\t\t\t\t {% for r in round %}
\t\t\t\t\t\t\t\t\t\t <option value=\"{{r.mr_round_id}}\">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t {% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t\t\t\t\t<label>เลขที่ TNT</label>
\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"tnt_barcode\" placeholder=\"TNT Barcode\"><br>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-start\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t\t\t\t<label>รหัสบาร์โค้ดใบปะหน้าซองใหญ่</label>
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-sm-0\" id=\"barcode\" placeholder=\"Barcode\" onkeyup=\"keyEvent(event,this.value);\">\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_save\">ค้นหา</button>
\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t\t\t\t<label>เลขที่เอกสาร</label>
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"sub_barcode\" placeholder=\"Barcode\" onkeyup=\"keyEvent_subbarcode(event);\"><br>
\t\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" onclick=\"update_barcode();\">บันทึก</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<div class=\"col\" id=\"alert_\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>Date/Time</th>
\t\t\t\t\t\t\t\t<th>ใบคุม</th>
\t\t\t\t\t\t\t\t<th>Bacode</th>
\t\t\t\t\t\t\t\t<th>Sender</th>
\t\t\t\t\t\t\t\t<th>Receiver</th>
\t\t\t\t\t\t\t\t<th>Type Work</th>
\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t<th>Actiom</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t\t{#<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" id=\"\" onclick=\"update_send_work();\">บันทึก</button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">ยกเลิก</button>
\t\t\t\t\t\t\t</div>#}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/reciever_branch.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/reciever_branch.tpl");
    }
}

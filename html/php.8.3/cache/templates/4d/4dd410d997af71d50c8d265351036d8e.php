<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/user.tpl */
class __TwigTemplate_4b662768a15c42a9cede4aa09a106935 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/user.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    body {
      height:100%;
    }
    .table-hover tbody tr.hilight:hover {
        background-color: #555;
        color: #fff;
    }

    table.dataTable tbody tr.hilight {
\t\tbackground-color: #7fff7f;
    }

    table.dataTable tbody tr.selected {
\t\tbackground-color: #555;
\t\tcolor: #fff;
\t\tcursor:pointer;
\t}

    #tb_work_order {
\t\tfont-size: 13px;
    }

\t.panel {
\t\tmargin-bottom : 5px;
\t}
\t.loading {
\t\tposition: fixed;
\t\ttop: 40%;
\t\tleft: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\t-ms-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1000;
\t}
\t
\t.img_loading {
\t\twidth: 350px;
\t\theight: auto;
\t}
\t
\t
\t
\t.btn-default.btn-on.active{background-color: #5BB75B;color: white;}
\t.btn-default.btn-off.active{background-color: #DA4F49;color: white;}
\t
\t.btn-default.btn-on-1.active{background-color: #006FFC;color: white;}
\t.btn-default.btn-off-1.active{background-color: #DA4F49;color: white;}
\t
\t.btn-default.btn-on-2.active{background-color: #00D590;color: white;}
\t.btn-default.btn-off-2.active{background-color: #A7A7A7;color: white;}
\t
\t.btn-default.btn-on-3.active{color: #5BB75B;font-weight:bolder;}
\t.btn-default.btn-off-3.active{color: #DA4F49;font-weight:bolder;}
\t
\t.btn-default.btn-on-4.active{background-color: #006FFC;color: #5BB75B;}
\t.btn-default.btn-off-4.active{background-color: #DA4F49;color: #DA4F49;}
\t
\t
\t
";
    }

    // line 67
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 68
        echo "
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 77
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 78
        echo "\t\t\$('.input-daterange').datepicker({
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t

\t\tvar table = \$('#tb_work_order').DataTable({ 
\t\t\t\"scrollY\": \"800px\",
\t\t\t\"scrollCollapse\": true,
\t\t\t\"responsive\": true,
\t\t\t\"searching\": false,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'mr_emp_name'},
\t\t\t\t{ 'data':'mr_emp_lastname' },
\t\t\t\t{ 'data':'mr_emp_code' },
\t\t\t\t{ 'data':'mr_emp_email' },
\t\t\t\t{ 'data':'isLogin' },
\t\t\t\t{ 'data':'isFailed' },
\t\t\t\t{ 'data':'mr_user_username' },
\t\t\t\t{ 'data':'mr_user_role_name' },
\t\t\t\t{ 'data':'re_password' }
\t\t\t]
\t\t});
\t\t\$('#myInputTextField').keyup(function(){
\t\t\tvar data = table.search(\$(this).val());
\t\t\tconsole.log(data);
\t\t\ttable.search(\$(this).val()).draw() ;
\t  })

\t//\ttable = \$(\"#tb_work_order\").DataTable({
\t//\t\t\"ajax\": {
\t//\t\t\t\"url\": \"ajax/ajax_get_user.php\",
\t//\t\t\t\"type\": \"POST\",
\t//\t\t},
\t//\t
\t//\t\t'columns': [
\t//\t\t\t{ 'data':'no' },
\t//\t\t\t{ 'data':'mr_emp_name'},
\t//\t\t\t{ 'data':'mr_emp_lastname' },
\t//\t\t\t{ 'data':'mr_emp_code' },
\t//\t\t\t{ 'data':'mr_emp_email' },
\t//\t\t\t{ 'data':'isLogin' },
\t//\t\t\t{ 'data':'isFailed' },
\t//\t\t\t{ 'data':'mr_user_username' },
\t//\t\t\t{ 'data':'mr_user_role_name' },
\t//\t\t\t{ 'data':'re_password' }
\t//\t\t],
\t//\t\t'scrollCollapse': true
\t//\t});
\t\t
\t\t\$(\"#pass_emp_send\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_send\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\$(\"#name_sender\").val(res.data.mr_emp_name);
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(vmr_department_name);
\t\t\t\t\t\t\$(\"#emp_id_send\").val(vmr_emp_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t});\t\t\t\t\t
\t\t});\t\t\t
\t\t
\t\t
\t\t\$(\"#unlock_all\").click(function(){
\t\t\talertify.confirm(\"ปลดล็อคทุก User!\", function() {
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_unlock_user.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'status': 0,
\t\t\t\t\t\t'user_id': 0,
\t\t\t\t\t\t'page': 3,
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tif(res.status != 200){
\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t}\t\t
\t\t\t\t});
\t\t\t});\t\t\t
\t\t});
\t\t
\t\t
\t\t\$(\"#btn_excel\").click(function() {
\t\t\tvar data = new Object();
\t\t\tdata['barcode'] \t\t\t= \$(\"#barcode\").val();
\t\t\tdata['sender'] \t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tdata['receiver'] \t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tdata['start_date'] \t\t\t= \$(\"#start_date\").val();
\t\t\tdata['end_date'] \t\t\t= \$(\"#end_date\").val();
\t\t\tdata['status'] \t\t\t\t= \$(\"#status\").val();
\t\t\tvar param = JSON.stringify(data);
\t\t
\t\t\t// window.open('excel.report.php?params='+param);
\t\t\tlocation.href = 'excel.report.php?params='+param;
\t\t
\t\t});
\t\$('#success_msg').hide();\t
\t\$(\"#re_password\").click(function(){

\t\$('#success_msg').hide();
\t\$('.loading').hide();

\t
\t
\talertify.prompt( 'Reset Password', 'รหัสพนักงาน ที่ต้องการ reset', 'รหัสพนักงาน'
               , function(evt, value) { 
\t\t\t\t\t//alertify.success('You entered: ' + value) 
\t\t\t\t\tif(value != null || value != \"\") {
                                \$.ajax({
                                    url: \"./ajax/ajax_forget_password.php\",
\t\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\t\tdataType: 'json',
                                    data: {
                                        emp_code: value
                                    },
                                    success: function(res) {
\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\t\t\t\talertify.success(res.message);
\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').show();
\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').html(res.html);
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\talertify.error(res.message);
\t\t\t\t\t\t\t\t\t\t}
                                    },error: function (error) {
\t\t\t\t\t\t\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t},beforeSend: function() {
\t\t\t\t\t\t\t\t\t\t\$('#success_msg').hide();
\t\t\t\t\t\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t\t\t\t\t}
                                })
                        }
\t\t\t\t}, function() { 
\t\t\t\t\talertify.error('Cancel') 
\t\t\t\t});
\t\t});
\t\t
\t\t
\tgetData();




";
    }

    // line 242
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 243
        echo "    /* function getData(){
\t\t\ttable.rows('.selected').remove().draw( false );
\t\t\t\$(\"#barcode\").val(\"\");
\t\t\ttable.destroy();
\t\t\tvar table = \$(\"#tb_work_order\").DataTable({
\t\t\t\"ajax\": {
\t\t\t\t\"url\": \"ajax/ajax_load_work_mailroom.php\",
\t\t\t\t\"type\": \"POST\"
\t\t\t},
\t\t\t'columns': [
\t\t\t\t{ 'data':'mr_work_main_id' },
\t\t\t\t{ 'data':'time_test '},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_re' },
\t\t\t\t{ 'data':'lastname_re' },
\t\t\t\t{ 'data':'mr_emp_name' },
\t\t\t\t{ 'data':'mr_emp_lastname' }
\t\t\t],
\t\t\t'order': [[ 0, \"desc\" ]],
\t\t\t'scrollCollapse': true
\t\t});
\t\t\t
\t};
 */

 
   function getData(){
\t\t\tvar username \t\t\t\t\t\t= \$(\"#input-search\").val();

\t\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType: \"json\",
\t\t\turl: \"ajax/ajax_get_user.php\",
\t\t\tdata: {
\t\t\t\t'username': username
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกินไป!');
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw(); 
\t\t\t}else{
\t\t\t\talert(msg.message);
\t\t\t\tlocation.reload();
\t\t\t}
\t\t});
\t};




function re_password(mr_user_id){\t\t
\t\t\$('#success_msg').hide();
\t\t\$('.loading').hide();
\t
\t\talertify.prompt( 'Reset Password', 'ยืนยันรหัสพนักงาน ที่ต้องการ reset', 'รหัสพนักงาน'
\t\t\t\t   , function(evt, value) { 
\t\t\t\t\t\t//alertify.success('You entered: ' + value) 
\t\t\t\t\t\tif(value != null || value != \"\") {
\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\turl: \"./ajax/ajax_forget_password.php\",
\t\t\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t\t\t\t\temp_code\t: value,
\t\t\t\t\t\t\t\t\t\t\tmr_user_id\t\t: mr_user_id
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\t\t\t\t\talertify.success(res.message);
\t\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').show();
\t\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').html(res.html);
\t\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t\talertify.error(res.message);
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t},error: function (error) {
\t\t\t\t\t\t\t\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t\t},beforeSend: function() {
\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').hide();
\t\t\t\t\t\t\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t})
\t\t\t\t\t\t\t}
\t\t\t\t\t}, function() { 
\t\t\t\t\t\talertify.error('Cancel') 
\t\t\t\t\t});
\t\t\t};




 function isFailed( id , val ){
\t\$.ajax({
\t\turl: \"ajax/ajax_unlock_user.php\",
\t\ttype: \"post\",
\t\tdata: {
\t\t\t'status': val,
\t\t\t'user_id': id,
\t\t\t'page': 1,
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res){
\t\t\tif(res.status == 200){
\t\t\t\talertify.success(res.message);
\t\t\t}else{
\t\t\t\talertify.error(res.message);
\t\t\t}
\t\t}\t\t\t\t\t\t\t
\t});
\t
 };
 
 
 function isLogin( id , val ){
\t\$.ajax({
\t\turl: \"ajax/ajax_unlock_user.php\",
\t\ttype: \"post\",
\t\tdata: {
\t\t\t'status': val,
\t\t\t'user_id': id,
\t\t\t'page': 2,
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res){
\t\t\tif(res.status == 200){
\t\t\t\talertify.success(res.message);
\t\t\t}else{
\t\t\t\talertify.error(res.message);
\t\t\t}
\t\t}\t\t\t\t\t\t\t
\t});
\t
 };
 
 
 
";
    }

    // line 389
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 390
        echo "\t\t\t
\t<div class=\"row\">
        <div class=\"col-md-12\">
\t\t\t<div class='loading' style=\"display: none;\">
\t\t\t\t\t<img src=\"../themes/images/loading.gif\" class='img_loading'>
\t\t\t</div>
\t\t\t\t\t<h1><center>จัดการบัญชีผู้เข้าใช้งานระบบ</center></h1>
\t\t</div>
\t</div>
\t<div class=\"row justify-content-end\">
        <div class=\"col-md-3 col-sm-12\">
\t\t\t<div class=\"input-group input-group-sm mb-3\">
\t\t\t\t<input id=\"input-search\" type=\"text\" class=\"form-control\" aria-label=\"search\" aria-describedby=\"search\">
\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn-sm\" onclick=\"getData();\">search</button>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
    </div>\t
\t
\t
\t\t\t
\t\t\t\t
\t\t <div class=\"panel panel-default\">
                  <div class=\"panel-body\">
\t\t\t\t  <div id=\"success_msg\" class=\"alert alert-primary\" role=\"alert\">
\t\t\t\t\t 
\t\t\t\t\t</div>
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>ชื่อ</th>
                            <th>นามสกุล</th>
                            <th>รหัสพนักงาน</th>
                            <th>E-mail</th>
                            <th>ค้างอยู่ในระบบ</th>
                            <th>ล็อค ใส่รหัสผิด</th>
                            <th>username</th>
                            <th>สิทธิ์การเข้าใช้งาน ";
        // line 431
        echo "                            <th>re_password</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
        </div>
\t\t
\t\t



\t\t";
        // line 462
        echo "
";
    }

    // line 466
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 468
        if ((($context["debug"] ?? null) != "")) {
            // line 469
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 471
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/user.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  553 => 471,  547 => 469,  545 => 468,  538 => 466,  533 => 462,  516 => 431,  474 => 390,  470 => 389,  322 => 243,  318 => 242,  153 => 78,  149 => 77,  138 => 68,  134 => 67,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
    body {
      height:100%;
    }
    .table-hover tbody tr.hilight:hover {
        background-color: #555;
        color: #fff;
    }

    table.dataTable tbody tr.hilight {
\t\tbackground-color: #7fff7f;
    }

    table.dataTable tbody tr.selected {
\t\tbackground-color: #555;
\t\tcolor: #fff;
\t\tcursor:pointer;
\t}

    #tb_work_order {
\t\tfont-size: 13px;
    }

\t.panel {
\t\tmargin-bottom : 5px;
\t}
\t.loading {
\t\tposition: fixed;
\t\ttop: 40%;
\t\tleft: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\t-ms-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1000;
\t}
\t
\t.img_loading {
\t\twidth: 350px;
\t\theight: auto;
\t}
\t
\t
\t
\t.btn-default.btn-on.active{background-color: #5BB75B;color: white;}
\t.btn-default.btn-off.active{background-color: #DA4F49;color: white;}
\t
\t.btn-default.btn-on-1.active{background-color: #006FFC;color: white;}
\t.btn-default.btn-off-1.active{background-color: #DA4F49;color: white;}
\t
\t.btn-default.btn-on-2.active{background-color: #00D590;color: white;}
\t.btn-default.btn-off-2.active{background-color: #A7A7A7;color: white;}
\t
\t.btn-default.btn-on-3.active{color: #5BB75B;font-weight:bolder;}
\t.btn-default.btn-off-3.active{color: #DA4F49;font-weight:bolder;}
\t
\t.btn-default.btn-on-4.active{background-color: #006FFC;color: #5BB75B;}
\t.btn-default.btn-off-4.active{background-color: #DA4F49;color: #DA4F49;}
\t
\t
\t
{% endblock %}
{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}

{% block domReady %}
\t\t\$('.input-daterange').datepicker({
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t

\t\tvar table = \$('#tb_work_order').DataTable({ 
\t\t\t\"scrollY\": \"800px\",
\t\t\t\"scrollCollapse\": true,
\t\t\t\"responsive\": true,
\t\t\t\"searching\": false,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'mr_emp_name'},
\t\t\t\t{ 'data':'mr_emp_lastname' },
\t\t\t\t{ 'data':'mr_emp_code' },
\t\t\t\t{ 'data':'mr_emp_email' },
\t\t\t\t{ 'data':'isLogin' },
\t\t\t\t{ 'data':'isFailed' },
\t\t\t\t{ 'data':'mr_user_username' },
\t\t\t\t{ 'data':'mr_user_role_name' },
\t\t\t\t{ 'data':'re_password' }
\t\t\t]
\t\t});
\t\t\$('#myInputTextField').keyup(function(){
\t\t\tvar data = table.search(\$(this).val());
\t\t\tconsole.log(data);
\t\t\ttable.search(\$(this).val()).draw() ;
\t  })

\t//\ttable = \$(\"#tb_work_order\").DataTable({
\t//\t\t\"ajax\": {
\t//\t\t\t\"url\": \"ajax/ajax_get_user.php\",
\t//\t\t\t\"type\": \"POST\",
\t//\t\t},
\t//\t
\t//\t\t'columns': [
\t//\t\t\t{ 'data':'no' },
\t//\t\t\t{ 'data':'mr_emp_name'},
\t//\t\t\t{ 'data':'mr_emp_lastname' },
\t//\t\t\t{ 'data':'mr_emp_code' },
\t//\t\t\t{ 'data':'mr_emp_email' },
\t//\t\t\t{ 'data':'isLogin' },
\t//\t\t\t{ 'data':'isFailed' },
\t//\t\t\t{ 'data':'mr_user_username' },
\t//\t\t\t{ 'data':'mr_user_role_name' },
\t//\t\t\t{ 'data':'re_password' }
\t//\t\t],
\t//\t\t'scrollCollapse': true
\t//\t});
\t\t
\t\t\$(\"#pass_emp_send\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_send\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\$(\"#name_sender\").val(res.data.mr_emp_name);
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(vmr_department_name);
\t\t\t\t\t\t\$(\"#emp_id_send\").val(vmr_emp_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t});\t\t\t\t\t
\t\t});\t\t\t
\t\t
\t\t
\t\t\$(\"#unlock_all\").click(function(){
\t\t\talertify.confirm(\"ปลดล็อคทุก User!\", function() {
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_unlock_user.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'status': 0,
\t\t\t\t\t\t'user_id': 0,
\t\t\t\t\t\t'page': 3,
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tif(res.status != 200){
\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t}\t\t
\t\t\t\t});
\t\t\t});\t\t\t
\t\t});
\t\t
\t\t
\t\t\$(\"#btn_excel\").click(function() {
\t\t\tvar data = new Object();
\t\t\tdata['barcode'] \t\t\t= \$(\"#barcode\").val();
\t\t\tdata['sender'] \t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tdata['receiver'] \t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tdata['start_date'] \t\t\t= \$(\"#start_date\").val();
\t\t\tdata['end_date'] \t\t\t= \$(\"#end_date\").val();
\t\t\tdata['status'] \t\t\t\t= \$(\"#status\").val();
\t\t\tvar param = JSON.stringify(data);
\t\t
\t\t\t// window.open('excel.report.php?params='+param);
\t\t\tlocation.href = 'excel.report.php?params='+param;
\t\t
\t\t});
\t\$('#success_msg').hide();\t
\t\$(\"#re_password\").click(function(){

\t\$('#success_msg').hide();
\t\$('.loading').hide();

\t
\t
\talertify.prompt( 'Reset Password', 'รหัสพนักงาน ที่ต้องการ reset', 'รหัสพนักงาน'
               , function(evt, value) { 
\t\t\t\t\t//alertify.success('You entered: ' + value) 
\t\t\t\t\tif(value != null || value != \"\") {
                                \$.ajax({
                                    url: \"./ajax/ajax_forget_password.php\",
\t\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\t\tdataType: 'json',
                                    data: {
                                        emp_code: value
                                    },
                                    success: function(res) {
\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\t\t\t\talertify.success(res.message);
\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').show();
\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').html(res.html);
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\talertify.error(res.message);
\t\t\t\t\t\t\t\t\t\t}
                                    },error: function (error) {
\t\t\t\t\t\t\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t},beforeSend: function() {
\t\t\t\t\t\t\t\t\t\t\$('#success_msg').hide();
\t\t\t\t\t\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t\t\t\t\t}
                                })
                        }
\t\t\t\t}, function() { 
\t\t\t\t\talertify.error('Cancel') 
\t\t\t\t});
\t\t});
\t\t
\t\t
\tgetData();




{% endblock %}


{% block javaScript %}
    /* function getData(){
\t\t\ttable.rows('.selected').remove().draw( false );
\t\t\t\$(\"#barcode\").val(\"\");
\t\t\ttable.destroy();
\t\t\tvar table = \$(\"#tb_work_order\").DataTable({
\t\t\t\"ajax\": {
\t\t\t\t\"url\": \"ajax/ajax_load_work_mailroom.php\",
\t\t\t\t\"type\": \"POST\"
\t\t\t},
\t\t\t'columns': [
\t\t\t\t{ 'data':'mr_work_main_id' },
\t\t\t\t{ 'data':'time_test '},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_re' },
\t\t\t\t{ 'data':'lastname_re' },
\t\t\t\t{ 'data':'mr_emp_name' },
\t\t\t\t{ 'data':'mr_emp_lastname' }
\t\t\t],
\t\t\t'order': [[ 0, \"desc\" ]],
\t\t\t'scrollCollapse': true
\t\t});
\t\t\t
\t};
 */

 
   function getData(){
\t\t\tvar username \t\t\t\t\t\t= \$(\"#input-search\").val();

\t\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType: \"json\",
\t\t\turl: \"ajax/ajax_get_user.php\",
\t\t\tdata: {
\t\t\t\t'username': username
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกินไป!');
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw(); 
\t\t\t}else{
\t\t\t\talert(msg.message);
\t\t\t\tlocation.reload();
\t\t\t}
\t\t});
\t};




function re_password(mr_user_id){\t\t
\t\t\$('#success_msg').hide();
\t\t\$('.loading').hide();
\t
\t\talertify.prompt( 'Reset Password', 'ยืนยันรหัสพนักงาน ที่ต้องการ reset', 'รหัสพนักงาน'
\t\t\t\t   , function(evt, value) { 
\t\t\t\t\t\t//alertify.success('You entered: ' + value) 
\t\t\t\t\t\tif(value != null || value != \"\") {
\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\turl: \"./ajax/ajax_forget_password.php\",
\t\t\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t\t\t\t\temp_code\t: value,
\t\t\t\t\t\t\t\t\t\t\tmr_user_id\t\t: mr_user_id
\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\t\t\t\t\talertify.success(res.message);
\t\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').show();
\t\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').html(res.html);
\t\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t\talertify.error(res.message);
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t},error: function (error) {
\t\t\t\t\t\t\t\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\t\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\t\t\t\t},beforeSend: function() {
\t\t\t\t\t\t\t\t\t\t\t\$('#success_msg').hide();
\t\t\t\t\t\t\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t})
\t\t\t\t\t\t\t}
\t\t\t\t\t}, function() { 
\t\t\t\t\t\talertify.error('Cancel') 
\t\t\t\t\t});
\t\t\t};




 function isFailed( id , val ){
\t\$.ajax({
\t\turl: \"ajax/ajax_unlock_user.php\",
\t\ttype: \"post\",
\t\tdata: {
\t\t\t'status': val,
\t\t\t'user_id': id,
\t\t\t'page': 1,
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res){
\t\t\tif(res.status == 200){
\t\t\t\talertify.success(res.message);
\t\t\t}else{
\t\t\t\talertify.error(res.message);
\t\t\t}
\t\t}\t\t\t\t\t\t\t
\t});
\t
 };
 
 
 function isLogin( id , val ){
\t\$.ajax({
\t\turl: \"ajax/ajax_unlock_user.php\",
\t\ttype: \"post\",
\t\tdata: {
\t\t\t'status': val,
\t\t\t'user_id': id,
\t\t\t'page': 2,
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res){
\t\t\tif(res.status == 200){
\t\t\t\talertify.success(res.message);
\t\t\t}else{
\t\t\t\talertify.error(res.message);
\t\t\t}
\t\t}\t\t\t\t\t\t\t
\t});
\t
 };
 
 
 
{% endblock %}

{% block Content2 %}
\t\t\t
\t<div class=\"row\">
        <div class=\"col-md-12\">
\t\t\t<div class='loading' style=\"display: none;\">
\t\t\t\t\t<img src=\"../themes/images/loading.gif\" class='img_loading'>
\t\t\t</div>
\t\t\t\t\t<h1><center>จัดการบัญชีผู้เข้าใช้งานระบบ</center></h1>
\t\t</div>
\t</div>
\t<div class=\"row justify-content-end\">
        <div class=\"col-md-3 col-sm-12\">
\t\t\t<div class=\"input-group input-group-sm mb-3\">
\t\t\t\t<input id=\"input-search\" type=\"text\" class=\"form-control\" aria-label=\"search\" aria-describedby=\"search\">
\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn-sm\" onclick=\"getData();\">search</button>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
    </div>\t
\t
\t
\t\t\t
\t\t\t\t
\t\t <div class=\"panel panel-default\">
                  <div class=\"panel-body\">
\t\t\t\t  <div id=\"success_msg\" class=\"alert alert-primary\" role=\"alert\">
\t\t\t\t\t 
\t\t\t\t\t</div>
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>ชื่อ</th>
                            <th>นามสกุล</th>
                            <th>รหัสพนักงาน</th>
                            <th>E-mail</th>
                            <th>ค้างอยู่ในระบบ</th>
                            <th>ล็อค ใส่รหัสผิด</th>
                            <th>username</th>
                            <th>สิทธิ์การเข้าใช้งาน {# <button type=\"button\" class=\"btn btn-outline-warning\" id=\"re_password\">Reset Password </button></th>#}
                            <th>re_password</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
        </div>
\t\t
\t\t



\t\t{#
<div class=\"row\">
        <div class=\"col-md-1\" style=\"margin-top:30px;\">
\t\t\t<button type=\"button\" class=\"btn btn-outline-warning\" id=\"unlock_all\">Unlock User All </button>
           
        </div>
\t\t<div class=\"col-md-9\">
\t\t</div>
\t\t<div class=\"col-md-1\" style=\"margin-top:30px;\">
\t\t\t<button type=\"button\" class=\"btn btn-outline-warning\" id=\"re_password\">Reset Password </button>
           
        </div>
    </div>
\t\t\t
\t
\t#}

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/user.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\user.tpl");
    }
}

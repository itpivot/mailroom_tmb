<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/search_follow.tpl */
class __TwigTemplate_c95ec1e8baf7f50a20b107f5257cd34e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/search_follow.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "

.card {
       margin: 8px 0px;
    }

    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
    }
    
    .btn {
        border-radius: 0px;
    }
";
    }

    // line 34
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "\t\t\t\t
\t\t\t\t
\tloadFollowEmp();

    \$('#txt_search').on('keyup', function() {
        loadFollowEmp();
    });

";
    }

    // line 44
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "function loadFollowEmp() {
\tlet txt = \$('#txt_search').val();
        let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_follow_work_emp_curdate.php\",
            type: \"POST\",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                \$('#count_all').html(res.length);
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {\t
\t\t\t\t\t\t
\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\tstr += '<div class=\"card-body \">';
\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\tstr += '<b>ผู้รับ : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'<br>';
                                str += '<b>เบอร์ติดต่อ : </b>';
\t\t\t\t\t\t\t\tif( res[i]['mr_emp_mobile'] != \"\" && res[i]['mr_emp_mobile'] != 'NULL' && res[i]['mr_emp_mobile'] != null ){
\t\t\t\t\t\t\t\t\tstr += '<a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a>';
                                }
                                if( res[i]['mr_emp_tel'] != \"\" && res[i]['mr_emp_tel'] != 'NULL' && res[i]['mr_emp_tel'] != null ){
\t\t\t\t\t\t\t\t\tstr += '/ <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a>';
                                }
                                str += '<br>';

\t\t\t\t\t\t\t\tif(res[i]['mr_type_work_id']=='2'){
\t\t\t\t\t\t\t\t\tstr += '<b>ส่งที่สาขา : </b>';\t
                                    str += ''+res[i]['mr_branch_name']+'<br>';
                                    if(res[i]['mr_branch_floor'] != '' && res[i]['mr_branch_floor'] != null && res[i]['mr_branch_floor'] != \"null\" ){
                                        str += '<b>ชั้นที่ส่งเอกสาร : </b>ชั้น '+res[i]['mr_branch_floor']+'<br>';
                                    }
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\tstr += '<b>แผนก : </b>';\t
\t\t\t\t\t\t\t\t\tstr += ''+res[i]['mr_department_name']+'<br>';
\t\t\t\t\t\t\t\t\tstr += '<b>ชั้นที่ส่งเอกสาร : </b>'+res[i]['floor_name']+'<br>';
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tstr += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tstr += '<b><h4>สถานะ : </b>'+res[i]['mr_status_name']+'</h4><br>';
\t\t\t\t\t\t\t\tif( res[i]['mr_status_id'] == 1 ){
\t\t\t\t\t\t\t\t\t\tstr += '<button type=\"button\" class=\"btn btn-danger\" id=\"cancle_work\" onclick=\"cancle_work('+res[i]['mr_work_main_id']+');\" value=\"\">Cancel</button><br>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif ( res[i]['mr_status_id'] == 5 ){
\t\t\t\t\t\t\t\t\tstr += '<a href=\"detail_receive.php?barcode='+res[i]['mr_work_barcode']+'\" class=\"btn btn-info\"  >รายละเอียดการรับเอกสาร</a>';
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\t
                    \$('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    \$('#data_list').html(str);
                }
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
}
\t
\t
\tfunction cancle_work(work_id) {\t
\t\t//var work_id \t\t= \$(\"#cancle_work\").val();
            alertify.confirm('ยืนยัน','ยืนยันการยกเลิกการส่งเอกสาร!',function(){
            \$.ajax({
                url: './ajax/ajax_cancle_work.php',
                type: 'POST',
                data: {
                    work_id: work_id
                },
                success: function(res){
                    if(res == \"success\") {
                        location.reload();
                    }
                }
            });
      },function() {
       
    })
}\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
";
    }

    // line 150
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 151
        echo "<div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหาจาก Tracking number, ชื่อ-สกุล, เบอร์ติดต่อ, วันที่\">
            </div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<center>จำนวนเอกสาร : <span id=\"count_all\"> ";
        // line 159
        echo twig_escape_filter($this->env, ($context["count_curdate"] ?? null), "html", null, true);
        echo "</span>  ฉบับ </center>
            </div>
        </form>
    </div>
\t<div id=\"data_list\">
\t</div>
\t
\t
\t

";
    }

    // line 172
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 174
        if ((($context["debug"] ?? null) != "")) {
            // line 175
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 177
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/search_follow.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 177,  248 => 175,  246 => 174,  239 => 172,  224 => 159,  214 => 151,  210 => 150,  103 => 45,  99 => 44,  87 => 35,  83 => 34,  54 => 5,  50 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}


{% block styleReady %}


.card {
       margin: 8px 0px;
    }

    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
    }
    
    .btn {
        border-radius: 0px;
    }
{% endblock %}



{% block domReady %}
\t\t\t\t
\t\t\t\t
\tloadFollowEmp();

    \$('#txt_search').on('keyup', function() {
        loadFollowEmp();
    });

{% endblock %}
{% block javaScript %}
function loadFollowEmp() {
\tlet txt = \$('#txt_search').val();
        let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_follow_work_emp_curdate.php\",
            type: \"POST\",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                \$('#count_all').html(res.length);
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {\t
\t\t\t\t\t\t
\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\tstr += '<div class=\"card-body \">';
\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\tstr += '<b>ผู้รับ : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'<br>';
                                str += '<b>เบอร์ติดต่อ : </b>';
\t\t\t\t\t\t\t\tif( res[i]['mr_emp_mobile'] != \"\" && res[i]['mr_emp_mobile'] != 'NULL' && res[i]['mr_emp_mobile'] != null ){
\t\t\t\t\t\t\t\t\tstr += '<a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a>';
                                }
                                if( res[i]['mr_emp_tel'] != \"\" && res[i]['mr_emp_tel'] != 'NULL' && res[i]['mr_emp_tel'] != null ){
\t\t\t\t\t\t\t\t\tstr += '/ <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a>';
                                }
                                str += '<br>';

\t\t\t\t\t\t\t\tif(res[i]['mr_type_work_id']=='2'){
\t\t\t\t\t\t\t\t\tstr += '<b>ส่งที่สาขา : </b>';\t
                                    str += ''+res[i]['mr_branch_name']+'<br>';
                                    if(res[i]['mr_branch_floor'] != '' && res[i]['mr_branch_floor'] != null && res[i]['mr_branch_floor'] != \"null\" ){
                                        str += '<b>ชั้นที่ส่งเอกสาร : </b>ชั้น '+res[i]['mr_branch_floor']+'<br>';
                                    }
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\tstr += '<b>แผนก : </b>';\t
\t\t\t\t\t\t\t\t\tstr += ''+res[i]['mr_department_name']+'<br>';
\t\t\t\t\t\t\t\t\tstr += '<b>ชั้นที่ส่งเอกสาร : </b>'+res[i]['floor_name']+'<br>';
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tstr += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
\t\t\t\t\t\t\t\tstr += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tstr += '<b><h4>สถานะ : </b>'+res[i]['mr_status_name']+'</h4><br>';
\t\t\t\t\t\t\t\tif( res[i]['mr_status_id'] == 1 ){
\t\t\t\t\t\t\t\t\t\tstr += '<button type=\"button\" class=\"btn btn-danger\" id=\"cancle_work\" onclick=\"cancle_work('+res[i]['mr_work_main_id']+');\" value=\"\">Cancel</button><br>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tif ( res[i]['mr_status_id'] == 5 ){
\t\t\t\t\t\t\t\t\tstr += '<a href=\"detail_receive.php?barcode='+res[i]['mr_work_barcode']+'\" class=\"btn btn-info\"  >รายละเอียดการรับเอกสาร</a>';
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\t
                    \$('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    \$('#data_list').html(str);
                }
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
}
\t
\t
\tfunction cancle_work(work_id) {\t
\t\t//var work_id \t\t= \$(\"#cancle_work\").val();
            alertify.confirm('ยืนยัน','ยืนยันการยกเลิกการส่งเอกสาร!',function(){
            \$.ajax({
                url: './ajax/ajax_cancle_work.php',
                type: 'POST',
                data: {
                    work_id: work_id
                },
                success: function(res){
                    if(res == \"success\") {
                        location.reload();
                    }
                }
            });
      },function() {
       
    })
}\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
{% endblock %}

{% block Content %}
<div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหาจาก Tracking number, ชื่อ-สกุล, เบอร์ติดต่อ, วันที่\">
            </div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<center>จำนวนเอกสาร : <span id=\"count_all\"> {{ count_curdate }}</span>  ฉบับ </center>
            </div>
        </form>
    </div>
\t<div id=\"data_list\">
\t</div>
\t
\t
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/search_follow.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\search_follow.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/seting.floor.tpl */
class __TwigTemplate_736629f062e5be79d32efdd2466eea4f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/seting.floor.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 25
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'name'},
\t\t{'data': 'g_mr_user_username'},
\t\t{'data': 'sys_time'},
\t\t{'data': 'action'}
    ]
});
var tbl_floor = \$('#tb_floor').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_floor_mame'},
\t\t{'data': 'mr_building_name'},
\t\t{'data': 'mr_branch_name'},
\t\t{'data': 'sys_time'},
\t\t{'data': 'action'}
    ]
});
 \$('#tb_keyin').on('click', 'tr', function () {
        var data = tbl_data.row( this ).data();
        //alert( 'You clicked on '+data['mr_round_name']+'\\'s row' );
\t\t//\$('#round_name').val(data['mr_round_name']);
\t\t//\$('#type_work').val(data['mr_type_work_id']);
\t\t//\$('#mr_round_id').val(data['mr_round_id']);
    } );


\$('#myTab a').on('click', function (e) {
\te.preventDefault()
\tvar id = \$(this).attr('id');
\tif(id==\"profile-tab\"){
\t\tload_data_emp_floor();
\t}else{
\t\tload_data_floor();
\t}
\tconsole.log(id);
  })

  \$('#mr_floor_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
});
  \$('#mr_user_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
});
  \$('#mr_building_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
});
  \$('#mr_branch_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {
  set_option_building();
  var data = e.params.data;
\t\$('#mr_branch_name').val(data['text']);
});

load_data_floor();
";
    }

    // line 139
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 140
        echo "

function save_add_floor() {
\tvar data =  \$('#myform_data_floor').serialize()+\"&page=save_add_floor\";
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t   //alert('error; ' + eval(error));

\t\t  alertify.alert('error',eval(error)+'!');
\t\t  \$(\"#bg_loader\").hide();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\tset_option_building() 

\t\t}else{
\t\t\t alertify.alert('เกิดข้อผิดพลาด',res['message']+'!');
\t\t  \t\$(\"#bg_loader\").hide();
\t\t}
\t  });
}
function set_option_building() {
\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:{
\t\t\tpage\t\t\t\t\t\t:\t'getBuilding',
\t\t\tmr_branch_id\t\t\t\t:\tmr_branch_id
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#mr_building_id').html('');
\t\tvar newOption = new Option('กรุณาระบุอาคาร', '', false, false);
\t\t\$('#mr_building_id').append(newOption).trigger('change');

\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\tif(res['data'].length  >= 1 && res['data'] != undefined){
\t\t\t\t
\t\t\t\tres['data'].forEach(async function(rating) {
\t\t\t\t //console.log(rating.mr_building_id);
\t\t\t\t \tnewOption = new Option(rating.mr_building_name, rating.mr_building_id, false, false);
\t\t\t\t\t\$('#mr_building_id').append(newOption).trigger('change');
\t\t\t\t
\t\t\t\t})
\t\t\t}else{
\t\t\t\t\t\$('#mr_building_id').html('');
\t\t\t\t\tnewOption = new Option('ไม่มีข้อมูลตึก/อาคาร', 0, false, false);
\t\t\t\t\t\$('#mr_building_id').append(newOption).trigger('change');
\t\t\t}
\t\t}else{
 \t\t\talert('error; ' + res['message']);
\t\t  \t\$(\"#bg_loader\").hide();
\t\t}
\t  });
}

function load_data_emp_floor() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function load_data_floor() {
\tvar data =  \$('#myform_data_floor').serialize()+\"&page=loadFloor\";
\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_floor').DataTable().clear().draw();
\t\t\t\$('#tb_floor').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}
function remove_zone(id) {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:{
\t\t\tpage\t\t\t:\t'remove',
\t\t\tid\t\t\t\t:\tid
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();
\t\t\t\$('#round_name').val('');
\t\t\t\$('#type_work').val('');
\t\t\t\$('#mr_round_id').val('');
\t\t\talertify.alert('ลบข้อมูลสำเร็จ',res['message']+'!');
\t\t}else{
\t\t\talertify.alert('ตรวจสอบข้อมูล',res['message']+'!');
\t\t}
\t  });
}
function save_zone() {
var mr_floor_id = \$('#mr_floor_id').val();
var mr_user_id = \$('#mr_user_id').val();

\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:{
\t\t\tpage\t\t\t:\t'save',
\t\t\tmr_user_id\t\t:\tmr_user_id,
\t\t\tmr_floor_id\t\t:\tmr_floor_id
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();
\t\t\t\$('#round_name').val('');
\t\t\t\$('#type_work').val('');
\t\t\t\$('#mr_round_id').val('');
\t\t\talertify.alert('บันทึกสำเร็จ',res['message']+'!');
\t\t}else{
\t\t\talertify.alert('ตรวจสอบข้อมูล',res['message']+'!');
\t\t}
\t  });
}


";
    }

    // line 341
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 342
        echo "
<div  class=\"container-fluid\">




\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t<li class=\"nav-item\">
\t\t  <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">ข้อมูลชั้น</a>
\t\t</li>
\t\t<li class=\"nav-item\">
\t\t  <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">พนักงานประจำชั้น</a>
\t\t</li>
\t  </ul>
\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">


\t\t\t<form id=\"myform_data_floor\">
\t\t\t\t<br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t<label><h3><b>ข้อมูลชั้น</b></h3></label><br>
\t\t\t\t\t\t\t<label>ชั้น > จัดการชั้น</label>
\t\t\t\t\t   </div>\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการชั้น</h5>
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_floor\">
\t\t\t\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\" colspan=\"2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleInputEmail1\">ชื่อชั้น</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"floor_name\" name=\"floor_name\"  placeholder=\"ชื่อชั้น 1A,1B,วงศ์สว่างชั้น 1,ตึกรัชดาชั้น 2-B\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleInputEmail1\">ละดับชั้น</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"floor_level\" name=\"floor_level\" placeholder=\"1,2,3,4,5....\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <label for=\"mr_branch_id\">สาขา</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t  <input type=\"hidden\" value=\"\" id=\"mr_branch_name\" name=\"mr_branch_name\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <select class=\"form-control\" id=\"mr_branch_id\" name=\"mr_branch_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>สาขา</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
        // line 400
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branchdata"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 401
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 401), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_code", [], "any", false, false, false, 401), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 401), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 403
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t  </select>
\t\t\t\t\t\t\t\t\t\t\t  </th>
\t\t\t\t\t\t\t\t\t\t\t  <th width=\"10%\" scope=\"col\">\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"mr_building_id\">ตึก/อาคาร</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"mr_building_id\" name=\"mr_building_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" disabled selected>ตึก</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 415
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-info\" onclick=\"load_data_floor()\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" style=\"padding-right:5px;\">search</i>
\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" colspan=\"6\" scope=\"col\"><button type=\"button\" class=\"btn btn-success\" onclick=\"save_add_floor()\">บันทึก(เพิ่มข้อมูลสาขา)</button></th>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ชั้น</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ตึก/อาคาร</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">สาขา/สถานที่</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่นำเข้า</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t  </table>
\t\t\t
\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</form>
\t\t\t
\t\t</div>
\t\t<div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">
\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t<br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t<label><h3><b>ชั้นที่รับผิดชอบ ของพนักงาน</b></h3></label><br>
\t\t\t\t\t\t\t<label>ชั้น > จัดการชั้น</label>
\t\t\t\t\t   </div>\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการชั้น</h5>
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_keyin\">
\t\t\t\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\" colspan=\"2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control type_work\" id=\"mr_floor_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>ชั้น</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
        // line 480
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
            // line 481
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 481), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "name", [], "any", false, false, false, 481), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 483
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control type_work\" id=\"mr_user_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>พนักงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
        // line 488
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["messenger"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["emp"]) {
            // line 489
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
            if ((twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_code", [], "any", false, false, false, 489) != "Resign")) {
                // line 490
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_user_id", [], "any", false, false, false, 490), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_code", [], "any", false, false, false, 490), "html", null, true);
                echo " :";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_name", [], "any", false, false, false, 490), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_lastname", [], "any", false, false, false, 490), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
            }
            // line 492
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['emp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 493
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\"><button type=\"button\" class=\"btn btn-success\" onclick=\"save_zone()\">เพิม</button></th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\"></th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ชั้น</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">พนักงาน</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่ Update</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t  </table>
\t\t\t
\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</form>
\t\t</div>
\t  </div>




</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




";
    }

    // line 546
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 548
        if ((($context["debug"] ?? null) != "")) {
            // line 549
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 551
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/seting.floor.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  697 => 551,  691 => 549,  689 => 548,  682 => 546,  628 => 493,  622 => 492,  610 => 490,  607 => 489,  603 => 488,  596 => 483,  585 => 481,  581 => 480,  514 => 415,  505 => 403,  492 => 401,  488 => 400,  428 => 342,  424 => 341,  220 => 140,  216 => 139,  130 => 59,  95 => 26,  91 => 25,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}\t

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'name'},
\t\t{'data': 'g_mr_user_username'},
\t\t{'data': 'sys_time'},
\t\t{'data': 'action'}
    ]
});
var tbl_floor = \$('#tb_floor').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_floor_mame'},
\t\t{'data': 'mr_building_name'},
\t\t{'data': 'mr_branch_name'},
\t\t{'data': 'sys_time'},
\t\t{'data': 'action'}
    ]
});
 \$('#tb_keyin').on('click', 'tr', function () {
        var data = tbl_data.row( this ).data();
        //alert( 'You clicked on '+data['mr_round_name']+'\\'s row' );
\t\t//\$('#round_name').val(data['mr_round_name']);
\t\t//\$('#type_work').val(data['mr_type_work_id']);
\t\t//\$('#mr_round_id').val(data['mr_round_id']);
    } );


\$('#myTab a').on('click', function (e) {
\te.preventDefault()
\tvar id = \$(this).attr('id');
\tif(id==\"profile-tab\"){
\t\tload_data_emp_floor();
\t}else{
\t\tload_data_floor();
\t}
\tconsole.log(id);
  })

  \$('#mr_floor_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
});
  \$('#mr_user_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
});
  \$('#mr_building_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
});
  \$('#mr_branch_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {
  set_option_building();
  var data = e.params.data;
\t\$('#mr_branch_name').val(data['text']);
});

load_data_floor();
{% endblock %}
{% block javaScript %}


function save_add_floor() {
\tvar data =  \$('#myform_data_floor').serialize()+\"&page=save_add_floor\";
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t   //alert('error; ' + eval(error));

\t\t  alertify.alert('error',eval(error)+'!');
\t\t  \$(\"#bg_loader\").hide();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\tset_option_building() 

\t\t}else{
\t\t\t alertify.alert('เกิดข้อผิดพลาด',res['message']+'!');
\t\t  \t\$(\"#bg_loader\").hide();
\t\t}
\t  });
}
function set_option_building() {
\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:{
\t\t\tpage\t\t\t\t\t\t:\t'getBuilding',
\t\t\tmr_branch_id\t\t\t\t:\tmr_branch_id
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#mr_building_id').html('');
\t\tvar newOption = new Option('กรุณาระบุอาคาร', '', false, false);
\t\t\$('#mr_building_id').append(newOption).trigger('change');

\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\tif(res['data'].length  >= 1 && res['data'] != undefined){
\t\t\t\t
\t\t\t\tres['data'].forEach(async function(rating) {
\t\t\t\t //console.log(rating.mr_building_id);
\t\t\t\t \tnewOption = new Option(rating.mr_building_name, rating.mr_building_id, false, false);
\t\t\t\t\t\$('#mr_building_id').append(newOption).trigger('change');
\t\t\t\t
\t\t\t\t})
\t\t\t}else{
\t\t\t\t\t\$('#mr_building_id').html('');
\t\t\t\t\tnewOption = new Option('ไม่มีข้อมูลตึก/อาคาร', 0, false, false);
\t\t\t\t\t\$('#mr_building_id').append(newOption).trigger('change');
\t\t\t}
\t\t}else{
 \t\t\talert('error; ' + res['message']);
\t\t  \t\$(\"#bg_loader\").hide();
\t\t}
\t  });
}

function load_data_emp_floor() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function load_data_floor() {
\tvar data =  \$('#myform_data_floor').serialize()+\"&page=loadFloor\";
\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_floor').DataTable().clear().draw();
\t\t\t\$('#tb_floor').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}
function remove_zone(id) {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:{
\t\t\tpage\t\t\t:\t'remove',
\t\t\tid\t\t\t\t:\tid
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();
\t\t\t\$('#round_name').val('');
\t\t\t\$('#type_work').val('');
\t\t\t\$('#mr_round_id').val('');
\t\t\talertify.alert('ลบข้อมูลสำเร็จ',res['message']+'!');
\t\t}else{
\t\t\talertify.alert('ตรวจสอบข้อมูล',res['message']+'!');
\t\t}
\t  });
}
function save_zone() {
var mr_floor_id = \$('#mr_floor_id').val();
var mr_user_id = \$('#mr_user_id').val();

\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_floor.php\",
\t\tdata:{
\t\t\tpage\t\t\t:\t'save',
\t\t\tmr_user_id\t\t:\tmr_user_id,
\t\t\tmr_floor_id\t\t:\tmr_floor_id
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();
\t\t\t\$('#round_name').val('');
\t\t\t\$('#type_work').val('');
\t\t\t\$('#mr_round_id').val('');
\t\t\talertify.alert('บันทึกสำเร็จ',res['message']+'!');
\t\t}else{
\t\t\talertify.alert('ตรวจสอบข้อมูล',res['message']+'!');
\t\t}
\t  });
}


{% endblock %}
{% block Content2 %}

<div  class=\"container-fluid\">




\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t<li class=\"nav-item\">
\t\t  <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">ข้อมูลชั้น</a>
\t\t</li>
\t\t<li class=\"nav-item\">
\t\t  <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">พนักงานประจำชั้น</a>
\t\t</li>
\t  </ul>
\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">


\t\t\t<form id=\"myform_data_floor\">
\t\t\t\t<br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t<label><h3><b>ข้อมูลชั้น</b></h3></label><br>
\t\t\t\t\t\t\t<label>ชั้น > จัดการชั้น</label>
\t\t\t\t\t   </div>\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการชั้น</h5>
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_floor\">
\t\t\t\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\" colspan=\"2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleInputEmail1\">ชื่อชั้น</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"floor_name\" name=\"floor_name\"  placeholder=\"ชื่อชั้น 1A,1B,วงศ์สว่างชั้น 1,ตึกรัชดาชั้น 2-B\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"exampleInputEmail1\">ละดับชั้น</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"floor_level\" name=\"floor_level\" placeholder=\"1,2,3,4,5....\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <label for=\"mr_branch_id\">สาขา</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t  <input type=\"hidden\" value=\"\" id=\"mr_branch_name\" name=\"mr_branch_name\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <select class=\"form-control\" id=\"mr_branch_id\" name=\"mr_branch_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>สาขา</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  {% for b in branchdata %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"{{b.mr_branch_id}}\">{{b.mr_branch_code}} {{b.mr_branch_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  {% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t  </select>
\t\t\t\t\t\t\t\t\t\t\t  </th>
\t\t\t\t\t\t\t\t\t\t\t  <th width=\"10%\" scope=\"col\">\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"mr_building_id\">ตึก/อาคาร</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"mr_building_id\" name=\"mr_building_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" disabled selected>ตึก</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for f in floor_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{f.mr_floor_id}}\">{{f.name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t#}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-info\" onclick=\"load_data_floor()\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" style=\"padding-right:5px;\">search</i>
\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" colspan=\"6\" scope=\"col\"><button type=\"button\" class=\"btn btn-success\" onclick=\"save_add_floor()\">บันทึก(เพิ่มข้อมูลสาขา)</button></th>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ชั้น</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ตึก/อาคาร</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">สาขา/สถานที่</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่นำเข้า</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t  </table>
\t\t\t
\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</form>
\t\t\t
\t\t</div>
\t\t<div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">
\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t<br>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t<label><h3><b>ชั้นที่รับผิดชอบ ของพนักงาน</b></h3></label><br>
\t\t\t\t\t\t\t<label>ชั้น > จัดการชั้น</label>
\t\t\t\t\t   </div>\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการชั้น</h5>
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_keyin\">
\t\t\t\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\" colspan=\"2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control type_work\" id=\"mr_floor_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>ชั้น</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  {% for f in floor_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"{{f.mr_floor_id}}\">{{f.name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  {% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control type_work\" id=\"mr_user_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"\" disabled selected>พนักงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  {% for emp in messenger %}
\t\t\t\t\t\t\t\t\t\t\t\t\t  {% if emp.mr_emp_code != \"Resign\" %}
\t\t\t\t\t\t\t\t\t\t\t\t\t  <option value=\"{{emp.mr_user_id}}\">{{ emp.mr_emp_code }} :{{ emp.mr_emp_name }} {{ emp.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t  {% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t\t  {% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\"><button type=\"button\" class=\"btn btn-success\" onclick=\"save_zone()\">เพิม</button></th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\"></th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ชั้น</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">พนักงาน</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่ Update</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t  </table>
\t\t\t
\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</form>
\t\t</div>
\t  </div>




</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/seting.floor.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\seting.floor.tpl");
    }
}

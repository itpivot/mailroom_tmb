<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/news.tpl */
class __TwigTemplate_b44483b36e1e6bb0cdacc3c95e0956cb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/news.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 8
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "
\t\t<div class=\"row justify-content-center\">
\t\t\t<div class=\"col-xs-12\"><img src=\"../themes/images/news.png\" alt=\"news\" class=\"img-responsive\" width=\"100%\"/></div>
\t\t</div>
\t

";
    }

    // line 18
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 20
        if ((($context["debug"] ?? null) != "")) {
            // line 21
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 23
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/news.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 23,  86 => 21,  84 => 20,  77 => 18,  67 => 9,  63 => 8,  56 => 5,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block Content %}

\t\t<div class=\"row justify-content-center\">
\t\t\t<div class=\"col-xs-12\"><img src=\"../themes/images/news.png\" alt=\"news\" class=\"img-responsive\" width=\"100%\"/></div>
\t\t</div>
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/news.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/employee/news.tpl");
    }
}

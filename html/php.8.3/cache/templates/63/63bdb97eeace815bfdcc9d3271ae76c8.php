<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/mailroom_import_barcode_TNT.tpl */
class __TwigTemplate_6130ff70226d5fc0a6701fb2818f7122 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/mailroom_import_barcode_TNT.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}
      #tb_work_order {
        font-size: 13px;
      }
\t  .panel {
\t\tmargin-bottom : 5px;
\t  }
#loader{
\t  width:5%;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

";
    }

    // line 43
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>

   <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\" integrity=\"sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i\" crossorigin=\"anonymous\"></script>

";
    }

    // line 55
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "\$('#bg_loader').hide()
\t\tload_data();
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'searching': false,
\t\t\t'lengthChange': false,
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'sys_date'},
\t\t\t\t{ 'data':'date_send' },
\t\t\t\t{ 'data':'barcode_tnt' },
\t\t\t\t{ 'data':'mr_branch_code' },
\t\t\t\t{ 'data':'status' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
\t\t\t
\t\t
\t\t
";
    }

    // line 84
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 85
        echo "\t\t
\tfunction remove_barcode_TNT() {
\t\talertify.confirm(\"ลบข้อมูล\",\"ยืนยันการลบข้อมูล.\",
\t\tfunction(){
\t\t\tok_remove_barcode_TNT();
\t\t},
\t\tfunction(){
\t\t  alertify.error('Cancel');
\t\t});
\t}
\tfunction ok_remove_barcode_TNT() {
\t\tvar dataall \t\t= [];
\t\tvar tbl_data \t\t= \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});
\t\t\t
\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"เกิดข้อผิดพลาด\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t
\t\tvar newdataall = dataall.join(\",\");
\t
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_remove_barcode_tnt.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_tnt_barcode_import_id'\t: newdataall
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();\t
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#bg_loader').hide();
\t\t\tload_data();
\t\t});
\t}
\t
\tfunction load_data(){

\t\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\tvar hub_id = \$('#hub_id').val();
\t\tvar data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';
\t\tif(hub_id == 'all_branch'){
\t\t\t\$('#div_mr_user_id').hide();
\t\t\t\$('#div_tnt_tracking').show();
\t\t}else{
\t\t\t\$('#div_mr_user_id').show();
\t\t\t\$('#div_tnt_tracking').hide();
\t\t}\t

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_barcode_TNT.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_branch_id'\t: mr_branch_id,
\t\t\t\t'data_all'\t\t: data_all,
\t\t\t\t'hub_id'\t\t: hub_id
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert(msg.message);
\t\t\t}else{
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t}
\t\t});
\t}



\t\tfunction import_excel() {
\t\t\t\$('#div_error').hide();\t
\t\t\tvar formData = new FormData();
\t\t\tformData.append('file', \$('#file')[0].files[0]);
\t\t\tif(\$('#file').val() == ''){
\t\t\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t}else{
\t\t\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t\t\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t }
\t\t\t}
\t\t\t\$.ajax({
\t\t\t\t   url : 'ajax/ajax_readFile_barcode_tnt.php',
\t\t\t\t   dataType : 'json',
\t\t\t\t   type : 'POST',
\t\t\t\t   data : formData,
\t\t\t\t   processData: false,  // tell jQuery not to process the data
\t\t\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t\t\t   success : function(data) {

\t\t\t\t\tif(data.status == 200){
\t\t\t\t\t\tif(data.data.count_error > 0){
\t\t\t\t\t\t\t\$('#div_error').html(data.data.txt_error)
\t\t\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\t\t}

\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\t\t\$('#file').val('');
\t\t\t\t\t\tload_data();
\t\t\t\t\t}

\t\t\t\t\t//    if(data['count_error'] > 0){
\t\t\t\t\t// \t\$('#div_error').html(data['txt_error'])
\t\t\t\t\t// \t\$('#div_error').show();
\t\t\t\t\t//    }else{

\t\t\t\t\t//    }
\t\t\t\t\t//    \$('#bg_loader').hide();
\t\t\t\t\t//    \$('#file').val('');
\t\t\t\t\t//    load_data();
\t\t\t\t\t   
\t\t\t\t}, beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t}
\t\t\t});
\t\t
\t\t}


";
    }

    // line 220
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 221
        echo "<form id=\"print_1\" action=\"print_sort_branch_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data1\" name=\"data1\">
</form>
<form id=\"print_2\" action=\"print_hab_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data2\" name=\"data2\">
</form>
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">TNT Barcode Import</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t<form id=\"form_import_excel\">
\t\t\t\t\t\t\t\t<label for=\"file\">
\t\t\t\t\t\t\t\t\t</label>

\t\t\t\t\t\t\t<div class=\"col-md-3 col-lg-4\">\t  
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"></a>
\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-success\"> \t
\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\tvertical_align_top
\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t<br> 
\t\t\t\t\t\t\t\tUpload
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<a  href=\"template_tnt.xlsx\" type=\"button\" class=\"btn btn-light\" download> \t
\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\tvertical_align_bottom
\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t<br> 
\t\t\t\t\t\t\t\tdownload templates
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t\t<div id=\"div_error\" class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t  </div>




\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-sm-12\"><br>
\t\t\t\t\t\t\t\t";
        // line 278
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>วันที่/เวลา Import</th>
\t\t\t\t\t\t\t\t<th>วันที่จัดส่ง</th>
\t\t\t\t\t\t\t\t<th>TNT Barcode</th>
\t\t\t\t\t\t\t\t<th>สาขาปลายทาง</th>
\t\t\t\t\t\t\t\t<th class=\"text-center\">
\t\t\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือกทั้งหมด</span>
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" onclick=\"remove_barcode_TNT();\">ลบข้อมูลที่เลือก</button> 
\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
";
    }

    // line 328
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 330
        if ((($context["debug"] ?? null) != "")) {
            // line 331
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 333
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/mailroom_import_barcode_TNT.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  426 => 333,  420 => 331,  418 => 330,  411 => 328,  360 => 278,  305 => 221,  301 => 220,  164 => 85,  160 => 84,  131 => 56,  127 => 55,  114 => 44,  110 => 43,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}
      #tb_work_order {
        font-size: 13px;
      }
\t  .panel {
\t\tmargin-bottom : 5px;
\t  }
#loader{
\t  width:5%;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>

   <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\" integrity=\"sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i\" crossorigin=\"anonymous\"></script>

{% endblock %}

{% block domReady %}
\$('#bg_loader').hide()
\t\tload_data();
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'searching': false,
\t\t\t'lengthChange': false,
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'sys_date'},
\t\t\t\t{ 'data':'date_send' },
\t\t\t\t{ 'data':'barcode_tnt' },
\t\t\t\t{ 'data':'mr_branch_code' },
\t\t\t\t{ 'data':'status' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
\t\t\t
\t\t
\t\t
{% endblock %}


{% block javaScript %}
\t\t
\tfunction remove_barcode_TNT() {
\t\talertify.confirm(\"ลบข้อมูล\",\"ยืนยันการลบข้อมูล.\",
\t\tfunction(){
\t\t\tok_remove_barcode_TNT();
\t\t},
\t\tfunction(){
\t\t  alertify.error('Cancel');
\t\t});
\t}
\tfunction ok_remove_barcode_TNT() {
\t\tvar dataall \t\t= [];
\t\tvar tbl_data \t\t= \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});
\t\t\t
\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"เกิดข้อผิดพลาด\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t
\t\tvar newdataall = dataall.join(\",\");
\t
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_remove_barcode_tnt.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_tnt_barcode_import_id'\t: newdataall
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();\t
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\t\$('#bg_loader').hide();
\t\t\tload_data();
\t\t});
\t}
\t
\tfunction load_data(){

\t\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\tvar hub_id = \$('#hub_id').val();
\t\tvar data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';
\t\tif(hub_id == 'all_branch'){
\t\t\t\$('#div_mr_user_id').hide();
\t\t\t\$('#div_tnt_tracking').show();
\t\t}else{
\t\t\t\$('#div_mr_user_id').show();
\t\t\t\$('#div_tnt_tracking').hide();
\t\t}\t

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_barcode_TNT.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_branch_id'\t: mr_branch_id,
\t\t\t\t'data_all'\t\t: data_all,
\t\t\t\t'hub_id'\t\t: hub_id
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert(msg.message);
\t\t\t}else{
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t}
\t\t});
\t}



\t\tfunction import_excel() {
\t\t\t\$('#div_error').hide();\t
\t\t\tvar formData = new FormData();
\t\t\tformData.append('file', \$('#file')[0].files[0]);
\t\t\tif(\$('#file').val() == ''){
\t\t\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t}else{
\t\t\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t\t\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t }
\t\t\t}
\t\t\t\$.ajax({
\t\t\t\t   url : 'ajax/ajax_readFile_barcode_tnt.php',
\t\t\t\t   dataType : 'json',
\t\t\t\t   type : 'POST',
\t\t\t\t   data : formData,
\t\t\t\t   processData: false,  // tell jQuery not to process the data
\t\t\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t\t\t   success : function(data) {

\t\t\t\t\tif(data.status == 200){
\t\t\t\t\t\tif(data.data.count_error > 0){
\t\t\t\t\t\t\t\$('#div_error').html(data.data.txt_error)
\t\t\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\t\t}

\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\t\t\$('#file').val('');
\t\t\t\t\t\tload_data();
\t\t\t\t\t}

\t\t\t\t\t//    if(data['count_error'] > 0){
\t\t\t\t\t// \t\$('#div_error').html(data['txt_error'])
\t\t\t\t\t// \t\$('#div_error').show();
\t\t\t\t\t//    }else{

\t\t\t\t\t//    }
\t\t\t\t\t//    \$('#bg_loader').hide();
\t\t\t\t\t//    \$('#file').val('');
\t\t\t\t\t//    load_data();
\t\t\t\t\t   
\t\t\t\t}, beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t}
\t\t\t});
\t\t
\t\t}


{% endblock %}

{% block Content2 %}
<form id=\"print_1\" action=\"print_sort_branch_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data1\" name=\"data1\">
</form>
<form id=\"print_2\" action=\"print_hab_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data2\" name=\"data2\">
</form>
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">TNT Barcode Import</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t<form id=\"form_import_excel\">
\t\t\t\t\t\t\t\t<label for=\"file\">
\t\t\t\t\t\t\t\t\t</label>

\t\t\t\t\t\t\t<div class=\"col-md-3 col-lg-4\">\t  
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"></a>
\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-success\"> \t
\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\tvertical_align_top
\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t<br> 
\t\t\t\t\t\t\t\tUpload
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<a  href=\"template_tnt.xlsx\" type=\"button\" class=\"btn btn-light\" download> \t
\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\tvertical_align_bottom
\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t<br> 
\t\t\t\t\t\t\t\tdownload templates
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t\t<div id=\"div_error\" class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t  </div>




\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-sm-12\"><br>
\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t<button onclick=\"print_byHub();\"type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมใหญ่</button>
\t\t\t\t\t\t\t\t<button onclick=\"print_all();\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมย่อย</button>
\t\t\t\t\t\t\t\t#}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>วันที่/เวลา Import</th>
\t\t\t\t\t\t\t\t<th>วันที่จัดส่ง</th>
\t\t\t\t\t\t\t\t<th>TNT Barcode</th>
\t\t\t\t\t\t\t\t<th>สาขาปลายทาง</th>
\t\t\t\t\t\t\t\t<th class=\"text-center\">
\t\t\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือกทั้งหมด</span>
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" onclick=\"remove_barcode_TNT();\">ลบข้อมูลที่เลือก</button> 
\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/mailroom_import_barcode_TNT.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/mailroom_import_barcode_TNT.tpl");
    }
}

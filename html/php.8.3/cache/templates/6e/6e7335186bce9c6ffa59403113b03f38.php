<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/resive_work_post_out.tpl */
class __TwigTemplate_a7915ffa83e36e3346569e7aa3380bc3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/resive_work_post_out.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 29
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t

//console.log('55555');

\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_import').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: true
\t\t},
\t\t'name_re': {
\t\t\trequired: true
\t\t},
\t\t'cost_id': {
\t\t\trequired: true
\t\t},
\t\t'lname_re': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\t//required: true
\t\t},
\t\t'address_re': {
\t\t\trequired: true
\t\t},
\t\t'sub_district_re': {
\t\t\t//required: true
\t\t},
\t\t'district_re': {
\t\t\t//required: true
\t\t},
\t\t'province_re': {
\t\t\t//required: true
\t\t},
\t\t'post_code_re': {
\t\t\t//required: true
\t\t},
\t\t'topic': {
\t\t\t//required: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'weight': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'price': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'mr_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 1){
\t\t\t\t\t\treturn false;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn true;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t\t'sub_district_re': {
\t\t\trequired: true,
\t\t},
\t\t'district_re': {
\t\t\trequired: true,
\t\t},
\t\t'province_re': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 3 || mr_type_post_id == 5){
\t\t\t\t\t\treturn true;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn false;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'cost_id': {
\t\t\trequired: 'กรุณาระบุรหัสค่าใช้จ่าย'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'dep_id_send': {
\t\t  required: 'กรุณาระบุหน่วยงาน ผู้ส่ง'
\t\t},
\t\t'name_re': {
\t\t  required: 'กรุณาระบุ ชื่อผู้รับ'
\t\t},
\t\t'lname_re': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้รับ'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_re': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'sub_district_re': {
\t\t  //required: 'กรุณาระบุ แขวง/ตำบล'
\t\t},
\t\t'district_re': {
\t\t  //required: 'กรุณาระบุ เขต/อำเภอ'
\t\t},
\t\t'province_re': {
\t\t  //required: 'กรุณาระบุ จังหวัด'
\t\t},
\t\t'post_code_re': {
\t\t  //required: 'กรุณาระบุ รหัสไปรษณีย์'
\t\t},
\t\t'topic': {
\t\t  required: 'กรุณาระบุ หัวเรื่อง'
\t\t},
\t\t'mr_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'post_barcode': {
\t\t  required: 'กรุณาระบุ เลขที่เอกสาร'
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'price': {
\t\t  required: 'ไม่พบข้อมูลราคา',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'cost_id': {
\t\t  required: 'กรุณาระบุรหัสค่าใช้จ่าย'
\t\t},
\t\t'sub_district_re': {
\t\t\trequired:  'กรุณาระบุ แขวง/ตำบล:',
\t\t},
\t\t'district_re': {
\t\t\trequired:  'กรุณาระบุ เขต/อำเภอ:',
\t\t},
\t\t'province_re': {
\t\t\trequired:  'กรุณาระบุ จังหวัด:',
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  'กรุณาระบุ เลขตอบรับ :',
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });


\$('#btn-show-form-edit').click(function() {
\t\$('#div_detail_receiver_sender').hide();
\t\$('#acction_update').hide();
\t\$('#acction_edit').show();
\t\$('#div_sender').show();
\t\$('#div_receiver').show();
\t\$(\"#page\").val('edit');
});
\$('#btn_cancle_edit').click(function() {
\t\$('#div_detail_receiver_sender').show();
\t\$('#acction_update').show();
\t\$('#acction_edit').hide();
\t\$('#div_sender').hide();
\t\$('#div_receiver').hide();
\t\$(\"#page\").val('update_status');
});

\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t//load_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_price_form\").prop(\"checked\") == false){
\t\t\t\t\treset_price_form();
\t\t\t\t}
\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\$(\"#work_barcode\").val('');
\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});
\$('#btn_edit').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\talertify.alert('สำเร็จ',\"  แก้ไขข้อมูลเรียบร้อยแล้ว\"
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});

\$('#dep_id_send').select2();
\$('#cost_id').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'mr_type_post_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'num_doc'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_round_name'},
        {'data': 'name_resive'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'}
    ]
});
\$('#select-all').on('click', function(){
\t// Check/uncheck all checkboxes in the table
\tvar rows = tbl_data.rows({ 'search': 'applied' }).nodes();
\t\$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.name); // display the selected text
                \$('#lname_re').val(ui.item.lname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t\$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
\t\t\t\t\$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
\t\t\t\t\$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
\t\t\t\t\$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
\t\t\t\t\$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
\t\t\t\t\$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
\t\t\t\t\$('#post_code_re').val(ui.item.zipcode); // save selected id to input
\t\t\t\t\$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
\t\t
\t\t\$('#work_barcode').keypress(function(event){
\t\t\tvar keycode = (event.keyCode ? event.keyCode : event.which);
\t\t\tif(keycode == '13'){
\t\t\t\tvar barcode = \$(this).val();
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_get_data_bybacose_thaipost.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tbarcode\t: barcode,
\t\t\t\t\t\tpage\t: 'getdataBybarcode',
\t\t\t\t\t\tcsrf_token\t: \$('#csrf_token').val()
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);

\t\t\t\t\t\t//console.log(\"++++++++++++++\");
\t\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\tif(res['count']>0){
\t\t\t\t\t\t\t\tset_form_val_all(res['data'])
\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('');
\t\t\t\t\t\t\t\t\$('#work_barcode').addClass('is-invalid');
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\$('#work_barcode').removeClass('is-invalid');
\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('ไม่พบข้อมูล')
\t\t\t\t\t\t\t\t\$('#btn-show-form-edit').attr('disabled','disabled');
\t\t\t\t\t\t\t\t//reset_price_form();
\t\t\t\t\t\t\t\treset_resive_form();
\t\t\t\t\t\t\t\treset_send_form();
\t\t\t\t\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}); 
\t\t\t}
\t\t  });\t\t
\t\t
";
    }

    // line 608
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 609
        echo "
function load_data_bydate() {

\tvar form = \$('#form_print');
\tvar serializeData = form.serialize();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:serializeData,
\t\turl: \"ajax/ajax_load_Resive_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function reset_send_form() {
\t\$('input[name=\"type_send\"]').attr('checked', false);
\t\$('#round').val(\"\");
\t\$('#emp_id_send').val(\"\").trigger('change');
\t\$('#emp_send_data').val(\"\");
}
function reset_price_form() {
\t\$(\"#weight\").val('');
\t\$(\"#quty\").val(1);
\t\$(\"#price\").val('');
\t\$(\"#total_price\").val('');
\t\$(\"#post_barcode\").val('');
\t\$(\"#post_barcode_re\").val('');
\t\$(\"#mr_type_post_id\").val('');
}

function reset_resive_form() {
\t\$(\"#name_re\").val(\"\");
\t\$(\"#lname_re\").val(\"\");
\t\$(\"#tel_re\").val(\"\");
\t\$(\"#address_re\").val(\"\");
\t\$(\"#sub_districts_code_re\").val(\"\");
\t\$(\"#districts_code_re\").val(\"\");
\t\$(\"#provinces_code_re\").val(\"\");
\t\$(\"#sub_district_re\").val(\"\");
\t\$(\"#district_re\").val(\"\");
\t\$(\"#province_re\").val(\"\");
\t\$(\"#post_code_re\").val(\"\");
\t\$(\"#quty\").val(\"1\");
\t\$(\"#topic\").val(\"\");
\t\$(\"#work_remark\").val(\"\");
}

function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_byHand_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}

function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\tconsole.log(11);
\t\t\$(\"#form_print\").attr('action', 'print_peper_thai_post.php');
\t\t\$('#form_print').submit();
\t}else if(type == 2){
\t\tconsole.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_cover_byhand.php');
\t\t\$('#form_print').submit();
\t}else{
\t\talert('----');
\t}

}

function chang_dep_id() {
\tvar dep = \$('#dep_id_send').select2('data'); 
\t// var emp = \$('#emp_id_send').select2('data'); 
\t// 
\t// console.log(emp);
\t// if(emp[0].id!=''){
\t// \t\$(\"#emp_send_data\").val(emp[0].text+'\\\\n'+dep[0].text);
\t// }else{
\t// \t\$(\"#emp_send_data\").val(dep[0].text);
\t// }
\t
}

function changPost_Type(type_id){
\t//console.log(type_id);
\tif(type_id == 3 || type_id == 5){
\t\t\$('#tr-post-barcode-re').show();
\t\tconsole.log(\"if ::\"+type_id);
\t}else{
\t\t\$('#tr-post-barcode-re').hide();
\t\tconsole.log(\"else ::\"+type_id);
\t}
\tsetPost_Price();
}
function setPost_Price(){
\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\tvar quty \t\t\t= \$('#quty').val();
\tvar weight \t\t\t= \$('#weight').val();
\tvar price \t\t\t= \$('#price').val();
\tvar total_price \t= \$('#totsl_price').val();

\tif(mr_type_post_id != '' && mr_type_post_id != null && weight != '' && weight != null){
\t\t//console.log('เข้า');
\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_get_post_price.php\",
\t\t\tdata :{
\t\t\t\tmr_type_post_id : mr_type_post_id,
\t\t\t\tquty \t\t \t: quty \t\t,
\t\t\t\tweight \t\t \t: weight \t\t,
\t\t\t\tprice \t\t \t: price \t\t,
\t\t\t\ttotal_price \t: total_price
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t\$('#total_price').val('');
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t  \$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 200){
\t\t\t\t\t\$('#price').val(res['data']['post_price']);
\t\t\t\t\t\$('#total_price').val(res['data']['totalprice']);
\t\t\t\t}else{
\t\t\t\t\t\$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t\t}
\t\t  });
\t}else{
\t\t//console.log('ไม่เข้า');
\t\t
\t\t\$('#price').val('');
\t\t\$('#totsl_price').val('');
\t}
\t
}

function import_excel() {
\t\$('#div_error').hide();\t
\tvar formData = new FormData();
\t
\tformData.append('file', \$('#file')[0].files[0]);
\tformData.append('csrf_token', \$('#csrf_token').val());
\tformData.append('date_import', \$('#date_import').val());
\tformData.append('import_round', \$('#import_round').val());
\tformData.append('page', 'Uploadfile');

\tif(\$('#file').val() == ''){
\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\$('#div_error').show();
\t\treturn;
\t}else{
\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\$('#div_error').show();
\t\t// console.log(extension);
\t\treturn;
\t }
\t}
\t\$.ajax({
\t\t   url : 'ajax/ajax_save_resive_Work_post_out.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : formData,
\t\t   processData: false,  // tell jQuery not to process the data
\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t   success : function(res) {
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t}
\t\t\t   \$('#bg_loader').hide();
\t\t   }, beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t\t
\t\t\t},
\t\t\terror: function (error) {
\t\t\t\talert(\"เกิดข้อผิดหลาด\");
\t\t\t\t//location.reload();
\t\t\t}
\t\t});

}



function cancelwork() {
\tvar dataall = [];
\tvar tbl_data = \$('#tb_keyin').DataTable();
\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\tdataall.push(this.value);
\t});

\tif(dataall.length < 1){
\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกรายการ\"); 
\t\treturn;
\t}
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: newdataall,
\t\t\tpage\t:'cancle_multiple'
\t\t},
\t\turl: \"ajax/ajax_load_Resive_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
\t

}\t


function dowload_excel(){
\tvar r = confirm(\"ต้องการโหลดไฟล์!\");
\tif (r == true) {
\t\twindow.open(\"../themes/thai_post_template.xlsx\", \"_blank\");
\t} else {
\t\talertify.error('Cancel')
\t}
\t

}\t


function set_form_val_all(data) {
\tconsole.log(data);
\tvar new_emp_send = new Option(data.full_name_send, data.mr_send_emp_id, true, true);
\t\$('#emp_id_send').append(new_emp_send).trigger('change');
\t\$('#dep_id_send').val(data.mr_send_department_id).trigger('change');
\t\$('#cost_id').val(data.mr_cost_id).trigger('change');
\t\$(\"#emp_send_data\").val(data.mr_send_emp_detail);
\t//\$(\"#mr_type_post_id\").val(data.mr_type_post_id);
\t\$(\"#name_re\").val(data.name_resive);
\t\$(\"#lname_re\").val(data.lname_resive);
\t\$(\"#address_re\").val(data.mr_address);
\t\$(\"#sub_districts_code_re\").val(data.mr_sub_districts_code);
\t\$(\"#districts_code_re\").val(data.mr_districts_code);
\t\$(\"#provinces_code_re\").val(data.mr_provinces_code);
\t\$(\"#post_barcode\").val(data.num_doc);
\t\$(\"#post_barcode_re\").val(data.sp_num_doc);
\t\$(\"#tel_re\").val(data.mr_cus_tel);
\t\$(\"#sub_district_re\").val(data.mr_sub_districts_name);
\t\$(\"#district_re\").val(data.mr_districts_name);
\t\$(\"#province_re\").val(data.mr_provinces_name);
\t\$(\"#post_code_re\").val(data.mr_post_code);
\t\$(\"#topic\").val(data.mr_topic);
\t\$(\"#work_remark\").val(data.mr_work_remark);
\t\$(\"#mr_work_main_id\").val(data.mr_work_main_id);
\t\$(\"#mr_work_post_id\").val(data.mr_work_post_id);
\t//\$(\"#weight\").val(data.mr_post_weight);
\t//\$(\"#quty\").val(data.mr_post_amount);
\t//\$(\"#price\").val(data.mr_post_price);
\t//\$(\"#total_price\").val(data.mr_post_totalprice);
\t\$(\"#page\").val('update_status');
\t//\$(\"#round\").val(data.mr_round_id);
\t\$(\"#tel_re\").val(data.mr_cus_tel);
\t
\t\$(\"#detail_sender\").val(data.mr_send_emp_detail);
\tvar detail_receiver = '';
\tdetail_receiver += data.name_resive;
\tdetail_receiver += ' '+data.lname_resive;
\tdetail_receiver += ' '+data.mr_address;
\tdetail_receiver += ' '+data.mr_sub_districts_name;
\tdetail_receiver += ' '+data.mr_districts_name;
\tdetail_receiver += ' '+data.mr_provinces_name;
\tdetail_receiver += ' '+data.mr_post_code;
\tdetail_receiver += '  โทร: '+data.mr_cus_tel;
\t\$(\"#detail_receiver\").val(detail_receiver);
\t\$('#btn-show-form-edit').removeAttr('disabled');
}
";
    }

    // line 974
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 975
        echo "
<div  class=\"container-fluid\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ส่งออกไปรษณีย์ไทย</b></h3></label><br>
\t\t\t\t
\t\t   </div>\t
\t\t</div>
\t</div>
\t  
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t  
\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t<div class=\"tab-pane fade show active\" id=\"key\" role=\"tabpanel\" aria-labelledby=\"key-tab\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"date_send\"><span class=\"text-muted font_mini\" >วันที่นำส่ง:<span class=\"box_error\" id=\"err_date_send\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 1004
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"round\"><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 1011
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 1012
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 1012), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 1012), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1014
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"work_barcode\"><span class=\"text-muted font_mini\" >Barcode: <span class=\"box_error\" id=\"err_work_barcode\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_work_barcode\" name=\"work_barcode\" id=\"work_barcode\" class=\"form-control form-control-sm\" type=\"text\" value=\"\" placeholder=\"Enter Barcode\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลรารา</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสค่าใช้จ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_cost_id\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_cost_id\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"cost_id\" name=\"cost_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกรหัสค่าใช้จ่าย</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 1036
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cost"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 1037
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_id", [], "any", false, false, false, 1037), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_code", [], "any", false, false, false, 1037), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_name", [], "any", false, false, false, 1037), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1039
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการส่ง: </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"box_error\" id=\"err_mr_type_post_id\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select onchange=\"changPost_Type(\$(this).val());\"  id=\"mr_type_post_id\" name=\"mr_type_post_id\" data-error=\"#err_mr_type_post_id\" class=\"form-control form-control-sm\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกประเภทการส่ง</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 1049
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["type_post"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 1050
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["t"], "mr_type_post_id", [], "any", false, false, false, 1050), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["t"], "mr_type_post_name", [], "any", false, false, false, 1050), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1052
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >เลขที่เอกสาร :</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode\" name=\"post_barcode\" data-error=\"#err_post_barcode\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr id=\"tr-post-barcode-re\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >เลขตอบรับ : </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode_re\" name=\"post_barcode_re\" data-error=\"#err_post_barcode_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >น้ำหนัก/กรัม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_weight\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"weight\" name=\"weight\" data-error=\"#err_weight\"  value=\"0\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\"  class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคา:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"price\" name=\"price\" data-error=\"#err_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\" readonly placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคารวม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_total_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"total_price\" name=\"total_price\" data-error=\"#err_total_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\" readonly placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" id=\"div_sender\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_send\" class=\"form-control form-control-sm\" id=\"emp_id_send\" name=\"emp_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >หน่วยงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_id_send\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"dep_id_send\" name=\"dep_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 1117
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 1118
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_id", [], "any", false, false, false, 1118), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_code", [], "any", false, false, false, 1118), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_name", [], "any", false, false, false, 1118), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1120
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"emp_send_data\" name=\"emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"div_detail_receiver_sender\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ผู้ส่งและผู้รับ <button type=\"button\" id=\"btn-show-form-edit\" class=\"btn btn-link btn-sm\" disabled><i class=\"material-icons\">edit</i></button></h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-muted font_mini\" >ข้อมูลผู้สั่งงาน:</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"detail_sender\" class=\"form-control\"  rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-muted font_mini\" >ข้อมูลผู้รับ:</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"detail_receiver\" class=\"form-control\"  rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"display:none;\" id=\"div_receiver\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้รับ</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_re\" id=\"name_re\" data-error=\"#err_name_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"นามสกุล\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_lname_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"lname_re\" id=\"lname_re\" data-error=\"#err_lname_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"นามสกุล\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์มือถือ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t-->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์โทร:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_re\" name=\"address_re\" data-error=\"#err_address_re\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"";
        // line 1196
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"mr_work_main_id\" id=\"mr_work_main_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"mr_work_post_id\" id=\"mr_work_post_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"page\" id=\"page\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</div>";
        // line 1246
        echo "
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center\" id=\"acction_update\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_price_form\" value=\"option1\"> คงข้อมูลราคา
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center\" id=\"acction_edit\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_edit\">บันทึกการแก้ไขข้อมูล </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_cancle_edit\">ยกเลิก </button>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"tab-pane fade\" id=\"import\" role=\"tabpanel\" aria-labelledby=\"import-tab\">
\t\t\t\t\t<br>
\t\t\t\t\t\t<form id=\"form_import_excel\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"><i class=\"material-icons\">help</i></a>
\t\t\t\t\t\t\t\t<label for=\"file\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group\" id=\"detail_receiver_head\">
\t\t\t\t\t\t\t\t\t\t<h4>Import File <button class=\"btn btn-link\" onclick=\"dowload_excel();\">Doowload Template</button></h4>  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</label><br>
\t\t\t\t\t\t\t\t<label for=\"date_import\">วันที่นำส่ง</label>
\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_import\"></span>
\t\t\t\t\t\t\t\t<input data-error=\"#err_date_import\" name=\"date_import\" id=\"date_import\" class=\"form-control form-control-sm col-md-2 c0l-12\" type=\"text\" value=\"";
        // line 1289
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_import_round\"></span>
\t\t\t\t\t\t\t\t\t<select data-error=\"#err_import_round\" class=\"form-control form-control-sm col-md-2 c0l-12\" id=\"import_round\" name=\"import_round\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t";
        // line 1295
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 1296
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 1296), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 1296), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1298
        echo "\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t\t\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 1305
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
\t</div>
\t

<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t";
        // line 1331
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 1332
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 1332), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 1332), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1334
        echo "\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 1339
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-sm btn-outline-info\" id=\"\">พิมพ์ใบงาน</button>
\t\t\t\t<!-- \t\t\t\t
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t-->
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\"></span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t<button onclick=\"cancelwork();\" type=\"button\" class=\"btn btn-danger btn-sm\">
\t\t\t\t\t\tลบรายการ
\t\t\t\t\t</button>
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">ประเภท</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่ \tปณ.</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ผู้รับ</th>
\t\t\t\t<th width=\"15%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">เบอร์โทร</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">หมายเหตุ</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>





<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




";
    }

    // line 1416
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 1418
        if ((($context["debug"] ?? null) != "")) {
            // line 1419
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 1421
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/resive_work_post_out.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1625 => 1421,  1619 => 1419,  1617 => 1418,  1610 => 1416,  1529 => 1339,  1522 => 1334,  1511 => 1332,  1507 => 1331,  1478 => 1305,  1469 => 1298,  1458 => 1296,  1454 => 1295,  1443 => 1289,  1398 => 1246,  1349 => 1196,  1271 => 1120,  1258 => 1118,  1254 => 1117,  1187 => 1052,  1176 => 1050,  1172 => 1049,  1160 => 1039,  1147 => 1037,  1143 => 1036,  1119 => 1014,  1108 => 1012,  1104 => 1011,  1092 => 1004,  1061 => 975,  1057 => 974,  689 => 609,  685 => 608,  130 => 59,  99 => 30,  95 => 29,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

{% endblock %}

{% block domReady %}\t

//console.log('55555');

\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_import').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: true
\t\t},
\t\t'name_re': {
\t\t\trequired: true
\t\t},
\t\t'cost_id': {
\t\t\trequired: true
\t\t},
\t\t'lname_re': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\t//required: true
\t\t},
\t\t'address_re': {
\t\t\trequired: true
\t\t},
\t\t'sub_district_re': {
\t\t\t//required: true
\t\t},
\t\t'district_re': {
\t\t\t//required: true
\t\t},
\t\t'province_re': {
\t\t\t//required: true
\t\t},
\t\t'post_code_re': {
\t\t\t//required: true
\t\t},
\t\t'topic': {
\t\t\t//required: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'weight': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'price': {
\t\t\trequired: true,
\t\t\tmin: 1,
\t\t},
\t\t'mr_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 1){
\t\t\t\t\t\treturn false;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn true;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t\t'sub_district_re': {
\t\t\trequired: true,
\t\t},
\t\t'district_re': {
\t\t\trequired: true,
\t\t},
\t\t'province_re': {
\t\t\trequired: true,
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  {
\t\t\t\tdepends: 
\t\t\t\t  function(element){
\t\t\t\t\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\t\t\t\t\tif(mr_type_post_id == 3 || mr_type_post_id == 5){
\t\t\t\t\t\treturn true;
\t\t\t\t\t} else {
\t\t\t\t\t\treturn false;
\t\t\t\t\t}
\t\t\t\t  }
\t\t\t\t},
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'cost_id': {
\t\t\trequired: 'กรุณาระบุรหัสค่าใช้จ่าย'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'dep_id_send': {
\t\t  required: 'กรุณาระบุหน่วยงาน ผู้ส่ง'
\t\t},
\t\t'name_re': {
\t\t  required: 'กรุณาระบุ ชื่อผู้รับ'
\t\t},
\t\t'lname_re': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้รับ'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_re': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'sub_district_re': {
\t\t  //required: 'กรุณาระบุ แขวง/ตำบล'
\t\t},
\t\t'district_re': {
\t\t  //required: 'กรุณาระบุ เขต/อำเภอ'
\t\t},
\t\t'province_re': {
\t\t  //required: 'กรุณาระบุ จังหวัด'
\t\t},
\t\t'post_code_re': {
\t\t  //required: 'กรุณาระบุ รหัสไปรษณีย์'
\t\t},
\t\t'topic': {
\t\t  required: 'กรุณาระบุ หัวเรื่อง'
\t\t},
\t\t'mr_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'post_barcode': {
\t\t  required: 'กรุณาระบุ เลขที่เอกสาร'
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'price': {
\t\t  required: 'ไม่พบข้อมูลราคา',
\t\t  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
\t\t},
\t\t'cost_id': {
\t\t  required: 'กรุณาระบุรหัสค่าใช้จ่าย'
\t\t},
\t\t'sub_district_re': {
\t\t\trequired:  'กรุณาระบุ แขวง/ตำบล:',
\t\t},
\t\t'district_re': {
\t\t\trequired:  'กรุณาระบุ เขต/อำเภอ:',
\t\t},
\t\t'province_re': {
\t\t\trequired:  'กรุณาระบุ จังหวัด:',
\t\t},
\t\t'post_barcode_re': {
\t\t\trequired:  'กรุณาระบุ เลขตอบรับ :',
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });


\$('#btn-show-form-edit').click(function() {
\t\$('#div_detail_receiver_sender').hide();
\t\$('#acction_update').hide();
\t\$('#acction_edit').show();
\t\$('#div_sender').show();
\t\$('#div_receiver').show();
\t\$(\"#page\").val('edit');
});
\$('#btn_cancle_edit').click(function() {
\t\$('#div_detail_receiver_sender').show();
\t\$('#acction_update').show();
\t\$('#acction_edit').hide();
\t\$('#div_sender').hide();
\t\$('#div_receiver').hide();
\t\$(\"#page\").val('update_status');
});

\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t//load_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_price_form\").prop(\"checked\") == false){
\t\t\t\t\treset_price_form();
\t\t\t\t}
\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\$(\"#work_barcode\").val('');
\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});
\$('#btn_edit').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\talertify.alert('สำเร็จ',\"  แก้ไขข้อมูลเรียบร้อยแล้ว\"
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});

\$('#dep_id_send').select2();
\$('#cost_id').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'mr_type_post_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'num_doc'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_round_name'},
        {'data': 'name_resive'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'}
    ]
});
\$('#select-all').on('click', function(){
\t// Check/uncheck all checkboxes in the table
\tvar rows = tbl_data.rows({ 'search': 'applied' }).nodes();
\t\$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.name); // display the selected text
                \$('#lname_re').val(ui.item.lname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t\$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
\t\t\t\t\$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
\t\t\t\t\$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
\t\t\t\t\$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
\t\t\t\t\$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
\t\t\t\t\$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
\t\t\t\t\$('#post_code_re').val(ui.item.zipcode); // save selected id to input
\t\t\t\t\$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
\t\t
\t\t\$('#work_barcode').keypress(function(event){
\t\t\tvar keycode = (event.keyCode ? event.keyCode : event.which);
\t\t\tif(keycode == '13'){
\t\t\t\tvar barcode = \$(this).val();
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_get_data_bybacose_thaipost.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tbarcode\t: barcode,
\t\t\t\t\t\tpage\t: 'getdataBybarcode',
\t\t\t\t\t\tcsrf_token\t: \$('#csrf_token').val()
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);

\t\t\t\t\t\t//console.log(\"++++++++++++++\");
\t\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\tif(res['count']>0){
\t\t\t\t\t\t\t\tset_form_val_all(res['data'])
\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('');
\t\t\t\t\t\t\t\t\$('#work_barcode').addClass('is-invalid');
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\$('#work_barcode').removeClass('is-invalid');
\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('ไม่พบข้อมูล')
\t\t\t\t\t\t\t\t\$('#btn-show-form-edit').attr('disabled','disabled');
\t\t\t\t\t\t\t\t//reset_price_form();
\t\t\t\t\t\t\t\treset_resive_form();
\t\t\t\t\t\t\t\treset_send_form();
\t\t\t\t\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}); 
\t\t\t}
\t\t  });\t\t
\t\t
{% endblock %}
{% block javaScript %}

function load_data_bydate() {

\tvar form = \$('#form_print');
\tvar serializeData = form.serialize();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:serializeData,
\t\turl: \"ajax/ajax_load_Resive_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function reset_send_form() {
\t\$('input[name=\"type_send\"]').attr('checked', false);
\t\$('#round').val(\"\");
\t\$('#emp_id_send').val(\"\").trigger('change');
\t\$('#emp_send_data').val(\"\");
}
function reset_price_form() {
\t\$(\"#weight\").val('');
\t\$(\"#quty\").val(1);
\t\$(\"#price\").val('');
\t\$(\"#total_price\").val('');
\t\$(\"#post_barcode\").val('');
\t\$(\"#post_barcode_re\").val('');
\t\$(\"#mr_type_post_id\").val('');
}

function reset_resive_form() {
\t\$(\"#name_re\").val(\"\");
\t\$(\"#lname_re\").val(\"\");
\t\$(\"#tel_re\").val(\"\");
\t\$(\"#address_re\").val(\"\");
\t\$(\"#sub_districts_code_re\").val(\"\");
\t\$(\"#districts_code_re\").val(\"\");
\t\$(\"#provinces_code_re\").val(\"\");
\t\$(\"#sub_district_re\").val(\"\");
\t\$(\"#district_re\").val(\"\");
\t\$(\"#province_re\").val(\"\");
\t\$(\"#post_code_re\").val(\"\");
\t\$(\"#quty\").val(\"1\");
\t\$(\"#topic\").val(\"\");
\t\$(\"#work_remark\").val(\"\");
}

function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_byHand_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}

function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\tconsole.log(11);
\t\t\$(\"#form_print\").attr('action', 'print_peper_thai_post.php');
\t\t\$('#form_print').submit();
\t}else if(type == 2){
\t\tconsole.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_cover_byhand.php');
\t\t\$('#form_print').submit();
\t}else{
\t\talert('----');
\t}

}

function chang_dep_id() {
\tvar dep = \$('#dep_id_send').select2('data'); 
\t// var emp = \$('#emp_id_send').select2('data'); 
\t// 
\t// console.log(emp);
\t// if(emp[0].id!=''){
\t// \t\$(\"#emp_send_data\").val(emp[0].text+'\\\\n'+dep[0].text);
\t// }else{
\t// \t\$(\"#emp_send_data\").val(dep[0].text);
\t// }
\t
}

function changPost_Type(type_id){
\t//console.log(type_id);
\tif(type_id == 3 || type_id == 5){
\t\t\$('#tr-post-barcode-re').show();
\t\tconsole.log(\"if ::\"+type_id);
\t}else{
\t\t\$('#tr-post-barcode-re').hide();
\t\tconsole.log(\"else ::\"+type_id);
\t}
\tsetPost_Price();
}
function setPost_Price(){
\tvar mr_type_post_id = \$('#mr_type_post_id').val();
\tvar quty \t\t\t= \$('#quty').val();
\tvar weight \t\t\t= \$('#weight').val();
\tvar price \t\t\t= \$('#price').val();
\tvar total_price \t= \$('#totsl_price').val();

\tif(mr_type_post_id != '' && mr_type_post_id != null && weight != '' && weight != null){
\t\t//console.log('เข้า');
\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_get_post_price.php\",
\t\t\tdata :{
\t\t\t\tmr_type_post_id : mr_type_post_id,
\t\t\t\tquty \t\t \t: quty \t\t,
\t\t\t\tweight \t\t \t: weight \t\t,
\t\t\t\tprice \t\t \t: price \t\t,
\t\t\t\ttotal_price \t: total_price
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t\$('#total_price').val('');
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t  \$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 200){
\t\t\t\t\t\$('#price').val(res['data']['post_price']);
\t\t\t\t\t\$('#total_price').val(res['data']['totalprice']);
\t\t\t\t}else{
\t\t\t\t\t\$('#price').val('');
\t\t\t\t\t\$('#total_price').val('');
\t\t\t\t}
\t\t  });
\t}else{
\t\t//console.log('ไม่เข้า');
\t\t
\t\t\$('#price').val('');
\t\t\$('#totsl_price').val('');
\t}
\t
}

function import_excel() {
\t\$('#div_error').hide();\t
\tvar formData = new FormData();
\t
\tformData.append('file', \$('#file')[0].files[0]);
\tformData.append('csrf_token', \$('#csrf_token').val());
\tformData.append('date_import', \$('#date_import').val());
\tformData.append('import_round', \$('#import_round').val());
\tformData.append('page', 'Uploadfile');

\tif(\$('#file').val() == ''){
\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\$('#div_error').show();
\t\treturn;
\t}else{
\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\$('#div_error').show();
\t\t// console.log(extension);
\t\treturn;
\t }
\t}
\t\$.ajax({
\t\t   url : 'ajax/ajax_save_resive_Work_post_out.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : formData,
\t\t   processData: false,  // tell jQuery not to process the data
\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t   success : function(res) {
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t}
\t\t\t   \$('#bg_loader').hide();
\t\t   }, beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t\t
\t\t\t},
\t\t\terror: function (error) {
\t\t\t\talert(\"เกิดข้อผิดหลาด\");
\t\t\t\t//location.reload();
\t\t\t}
\t\t});

}



function cancelwork() {
\tvar dataall = [];
\tvar tbl_data = \$('#tb_keyin').DataTable();
\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\tdataall.push(this.value);
\t});

\tif(dataall.length < 1){
\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกรายการ\"); 
\t\treturn;
\t}
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: newdataall,
\t\t\tpage\t:'cancle_multiple'
\t\t},
\t\turl: \"ajax/ajax_load_Resive_Work_Post_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
\t

}\t


function dowload_excel(){
\tvar r = confirm(\"ต้องการโหลดไฟล์!\");
\tif (r == true) {
\t\twindow.open(\"../themes/thai_post_template.xlsx\", \"_blank\");
\t} else {
\t\talertify.error('Cancel')
\t}
\t

}\t


function set_form_val_all(data) {
\tconsole.log(data);
\tvar new_emp_send = new Option(data.full_name_send, data.mr_send_emp_id, true, true);
\t\$('#emp_id_send').append(new_emp_send).trigger('change');
\t\$('#dep_id_send').val(data.mr_send_department_id).trigger('change');
\t\$('#cost_id').val(data.mr_cost_id).trigger('change');
\t\$(\"#emp_send_data\").val(data.mr_send_emp_detail);
\t//\$(\"#mr_type_post_id\").val(data.mr_type_post_id);
\t\$(\"#name_re\").val(data.name_resive);
\t\$(\"#lname_re\").val(data.lname_resive);
\t\$(\"#address_re\").val(data.mr_address);
\t\$(\"#sub_districts_code_re\").val(data.mr_sub_districts_code);
\t\$(\"#districts_code_re\").val(data.mr_districts_code);
\t\$(\"#provinces_code_re\").val(data.mr_provinces_code);
\t\$(\"#post_barcode\").val(data.num_doc);
\t\$(\"#post_barcode_re\").val(data.sp_num_doc);
\t\$(\"#tel_re\").val(data.mr_cus_tel);
\t\$(\"#sub_district_re\").val(data.mr_sub_districts_name);
\t\$(\"#district_re\").val(data.mr_districts_name);
\t\$(\"#province_re\").val(data.mr_provinces_name);
\t\$(\"#post_code_re\").val(data.mr_post_code);
\t\$(\"#topic\").val(data.mr_topic);
\t\$(\"#work_remark\").val(data.mr_work_remark);
\t\$(\"#mr_work_main_id\").val(data.mr_work_main_id);
\t\$(\"#mr_work_post_id\").val(data.mr_work_post_id);
\t//\$(\"#weight\").val(data.mr_post_weight);
\t//\$(\"#quty\").val(data.mr_post_amount);
\t//\$(\"#price\").val(data.mr_post_price);
\t//\$(\"#total_price\").val(data.mr_post_totalprice);
\t\$(\"#page\").val('update_status');
\t//\$(\"#round\").val(data.mr_round_id);
\t\$(\"#tel_re\").val(data.mr_cus_tel);
\t
\t\$(\"#detail_sender\").val(data.mr_send_emp_detail);
\tvar detail_receiver = '';
\tdetail_receiver += data.name_resive;
\tdetail_receiver += ' '+data.lname_resive;
\tdetail_receiver += ' '+data.mr_address;
\tdetail_receiver += ' '+data.mr_sub_districts_name;
\tdetail_receiver += ' '+data.mr_districts_name;
\tdetail_receiver += ' '+data.mr_provinces_name;
\tdetail_receiver += ' '+data.mr_post_code;
\tdetail_receiver += '  โทร: '+data.mr_cus_tel;
\t\$(\"#detail_receiver\").val(detail_receiver);
\t\$('#btn-show-form-edit').removeAttr('disabled');
}
{% endblock %}
{% block Content2 %}

<div  class=\"container-fluid\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ส่งออกไปรษณีย์ไทย</b></h3></label><br>
\t\t\t\t
\t\t   </div>\t
\t\t</div>
\t</div>
\t  
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t  
\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t<div class=\"tab-pane fade show active\" id=\"key\" role=\"tabpanel\" aria-labelledby=\"key-tab\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"date_send\"><span class=\"text-muted font_mini\" >วันที่นำส่ง:<span class=\"box_error\" id=\"err_date_send\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"round\"><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"work_barcode\"><span class=\"text-muted font_mini\" >Barcode: <span class=\"box_error\" id=\"err_work_barcode\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_work_barcode\" name=\"work_barcode\" id=\"work_barcode\" class=\"form-control form-control-sm\" type=\"text\" value=\"\" placeholder=\"Enter Barcode\">
\t\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลรารา</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสค่าใช้จ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_cost_id\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_cost_id\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"cost_id\" name=\"cost_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกรหัสค่าใช้จ่าย</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for c in cost %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ c.mr_cost_id }}\">{{ c.mr_cost_code }} - {{ c.mr_cost_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการส่ง: </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"box_error\" id=\"err_mr_type_post_id\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select onchange=\"changPost_Type(\$(this).val());\"  id=\"mr_type_post_id\" name=\"mr_type_post_id\" data-error=\"#err_mr_type_post_id\" class=\"form-control form-control-sm\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกประเภทการส่ง</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for t in type_post %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{t.mr_type_post_id}}\">{{t.mr_type_post_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >เลขที่เอกสาร :</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode\" name=\"post_barcode\" data-error=\"#err_post_barcode\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr id=\"tr-post-barcode-re\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >เลขตอบรับ : </span></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode_re\" name=\"post_barcode_re\" data-error=\"#err_post_barcode_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >น้ำหนัก/กรัม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_weight\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"weight\" name=\"weight\" data-error=\"#err_weight\"  value=\"0\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\"  class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" onkeyup=\"setPost_Price();\" onchange=\"setPost_Price();\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคา:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"price\" name=\"price\" data-error=\"#err_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\" readonly placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >ราคารวม:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_total_price\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"total_price\" name=\"total_price\" data-error=\"#err_total_price\"  value=\"0\" class=\"form-control form-control-sm\" type=\"number\" readonly placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" id=\"div_sender\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_send\" class=\"form-control form-control-sm\" id=\"emp_id_send\" name=\"emp_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >หน่วยงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_id_send\" onchange=\"chang_dep_id();\" class=\"form-control form-control-sm\" id=\"dep_id_send\" name=\"dep_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for d in department %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ d.mr_department_id }}\">{{ d.mr_department_code }} - {{ d.mr_department_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"emp_send_data\" name=\"emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"div_detail_receiver_sender\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ผู้ส่งและผู้รับ <button type=\"button\" id=\"btn-show-form-edit\" class=\"btn btn-link btn-sm\" disabled><i class=\"material-icons\">edit</i></button></h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-muted font_mini\" >ข้อมูลผู้สั่งงาน:</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"detail_sender\" class=\"form-control\"  rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-muted font_mini\" >ข้อมูลผู้รับ:</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"detail_receiver\" class=\"form-control\"  rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"display:none;\" id=\"div_receiver\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้รับ</h5>
\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_re\" id=\"name_re\" data-error=\"#err_name_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"นามสกุล\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_lname_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input name=\"lname_re\" id=\"lname_re\" data-error=\"#err_lname_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"นามสกุล\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์มือถือ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t-->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์โทร:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_re\" name=\"address_re\" data-error=\"#err_address_re\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"{{csrf_token}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"mr_work_main_id\" id=\"mr_work_main_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"mr_work_post_id\" id=\"mr_work_post_id\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"page\" id=\"page\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</div>{#
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_send_form();\">ล้างข้อมูลผู้ส่ง </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_resive_form();\">ล้างข้อมูลผู้รับ </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t#}

\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center\" id=\"acction_update\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_price_form\" value=\"option1\"> คงข้อมูลราคา
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-center\" id=\"acction_edit\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_edit\">บันทึกการแก้ไขข้อมูล </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_cancle_edit\">ยกเลิก </button>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"tab-pane fade\" id=\"import\" role=\"tabpanel\" aria-labelledby=\"import-tab\">
\t\t\t\t\t<br>
\t\t\t\t\t\t<form id=\"form_import_excel\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"><i class=\"material-icons\">help</i></a>
\t\t\t\t\t\t\t\t<label for=\"file\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group\" id=\"detail_receiver_head\">
\t\t\t\t\t\t\t\t\t\t<h4>Import File <button class=\"btn btn-link\" onclick=\"dowload_excel();\">Doowload Template</button></h4>  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</label><br>
\t\t\t\t\t\t\t\t<label for=\"date_import\">วันที่นำส่ง</label>
\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_import\"></span>
\t\t\t\t\t\t\t\t<input data-error=\"#err_date_import\" name=\"date_import\" id=\"date_import\" class=\"form-control form-control-sm col-md-2 c0l-12\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_import_round\"></span>
\t\t\t\t\t\t\t\t\t<select data-error=\"#err_import_round\" class=\"form-control form-control-sm col-md-2 c0l-12\" id=\"import_round\" name=\"import_round\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t\t\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\"></input>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
\t</div>
\t

<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-sm btn-outline-info\" id=\"\">พิมพ์ใบงาน</button>
\t\t\t\t<!-- \t\t\t\t
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t-->
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\"></span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t<button onclick=\"cancelwork();\" type=\"button\" class=\"btn btn-danger btn-sm\">
\t\t\t\t\t\tลบรายการ
\t\t\t\t\t</button>
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">ประเภท</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่ \tปณ.</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ผู้รับ</th>
\t\t\t\t<th width=\"15%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">เบอร์โทร</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">หมายเหตุ</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>





<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/resive_work_post_out.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\resive_work_post_out.tpl");
    }
}

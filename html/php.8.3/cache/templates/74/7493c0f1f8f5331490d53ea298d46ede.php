<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/profile.tpl */
class __TwigTemplate_3ef2282b5a55b19a59ada87dca72f64f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/profile.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo ".myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}
";
    }

    // line 28
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "\t\tvar usrID = '";
        echo twig_escape_filter($this->env, ($context["userID"] ?? null), "html", null, true);
        echo "';

\t\t\$(\"#mr_role_id\").change(function(){
\t\t    var role_id = \$(this).val();
\t\t\t//alert(role_id);
\t\t\talertify.confirm('แก้ไขสถานที่ปฏิบัติงาน :','ยืนยันการเปลี่ยนสถานที่ปฏิบัติงาน', function(){ 
\t\t\t\t//alertify.alert(\"ok\");
\t\t\t\t\$.post('./ajax/ajax_UpdateUserRole.php',{role_id:role_id},
\t\t\t\t\tfunction(res) {\t
\t\t\t\t\t\tif ( res.status == 200 ){
\t\t\t\t\t\t\talertify.alert('',\"บันทึกสำเร็จ\",function(){window.location.reload();});
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('error',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t},'json');
\t\t\t},function(){ 
\t\t\t\talertify.error('Cancel')
\t\t\t});
\t\t});

\t\t\$(\"#btn_save\").click(function(){
\t\t\t\$('.myErrorClass').removeClass('myErrorClass');

\t\t\tvar branch_floor = \$('#mr_branch_floor').select2('data')
\t\t\tvar mr_position_id \t\t= \$(\"#mr_position_id\").val();
\t\t\tvar mr_branch_floor \t= branch_floor[0].text;
\t\t\tvar floor_id \t\t\t= branch_floor[0].id;
\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\tvar name \t\t\t\t= \$(\"#name\").val();
\t\t\tvar last_name \t\t\t= \$(\"#last_name\").val();
\t\t\tvar tel \t\t\t\t= \$(\"#tel\").val();
\t\t\tvar tel_mobile \t\t\t= \$(\"#tel_mobile\").val();
\t\t\tvar email \t\t\t\t= \$(\"#email\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar mr_branch_id \t\t= \$(\"#mr_branch_id\").val();
\t\t\tvar department \t\t= \$(\"#department\").val();
\t\t\tvar\tstatus \t\t\t\t= true;
\t\t\t
\t\t\tif( mr_branch_id == \"\" || mr_branch_id == null){
\t\t\t\t//\$('#mr_branch_error').css({'border-bottom':' 1px solid red'});
\t\t\t\t\$('#select2-mr_branch_id-container').addClass('myErrorClass');
\t\t\t\tstatus = false;
\t\t\t\talert('กรุณากรอกข้อมูล สาขา');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( pass_emp == \"\" || pass_emp == null){
\t\t\t\t//\$('#pass_emp').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#pass_emp').addClass('myErrorClass');
\t\t\t\tstatus = false;
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif(\tname == \"\" || name == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#name').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#name').addClass('myErrorClass');
\t\t\t\tstatus = false;
\t\t\t\talert('กรุณากรอกข้อมูล ชื่อ');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif( last_name == \"\" || last_name == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#last_name ').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#last_name ').addClass('myErrorClass');
\t\t\t\talert('กรุณากรอกข้อมูล นามสกุล');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif( email == \"\" || email == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#email ').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#email ').addClass('myErrorClass');
\t\t\t\talert('กรุณากรอกข้อมูล E-mail');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( tel == \"\" || tel == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#tel ').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#tel ').addClass('myErrorClass');
\t\t\t\talert('กรุณากรอกข้อมูล เบอร์โทรศัพท์');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif( status === true ){
\t\t\t\talertify.confirm('บันทึก','ยืนยันการแก้ไขข้อมูล', 
\t\t\t\tfunction(){
\t\t\t\t\$.post(
\t\t\t\t\t'./ajax/ajax_update_emp.php',
\t\t\t\t\t{
\t\t\t\t\t\tmr_position_id\t : mr_position_id,
\t\t\t\t\t\tfloor_id\t : floor_id,
\t\t\t\t\t\tmr_branch_floor\t : mr_branch_floor,
\t\t\t\t\t\tpass_emp\t : pass_emp,
\t\t\t\t\t\tuser_id\t\t : user_id,
\t\t\t\t\t\temp_id\t\t : emp_id,
\t\t\t\t\t\tname\t\t : name,\t\t
\t\t\t\t\t\tlast_name \t : last_name,
\t\t\t\t\t\ttel \t\t : tel,\t\t
\t\t\t\t\t\ttel_mobile \t : tel_mobile,
\t\t\t\t\t\temail \t\t : email,
\t\t\t\t\t\tmr_branch_id : mr_branch_id,
\t\t\t\t\t\tdepartment : department,
\t\t\t\t\t},
\t\t\t\t\tfunction(res) {
\t\t\t\t\t\tif ( res.status == 200 ){
\t\t\t\t\t\t\talertify.alert('บันทึก',res.error.message);
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('บันทึก',res.error.message);
\t\t\t\t\t\t}

\t\t\t\t\t},'json');
\t\t\t\t},function(){ });
\t\t\t}else{
\t\t\t\talert('กรุณากรอกข้อมูลให้ครบถ้วน');
\t\t\t}\t
\t\t});

\t\$('#mr_branch_id').select2({
\t\ttheme: \"bootstrap4\",
\t\tplaceholder: \"เลือกสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\tresults : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t}).on('select2:select', function (e) {
\t\t\tconsole.log(\$('#mr_branch_id').val());
\t\t\t
\t\t\t

\t\t\tvar branch_id = \$('#mr_branch_id').val();
\t\t\t\$.ajax({
\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tbranch_id: branch_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\$('#mr_branch_floor').html(res.data);
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t})

\t\t});

\t\t
";
    }

    // line 188
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 189
        echo "\t\t\t\t
";
    }

    // line 192
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 193
        echo "
\t<br>
\t<br>
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้สั่งงาน </h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 200
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 200), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"";
        // line 201
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_id", [], "any", false, false, false, 201), "html", null, true);
        echo "\">
\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"";
        // line 209
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_code", [], "any", false, false, false, 209), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"";
        // line 220
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_name", [], "any", false, false, false, 220), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"";
        // line 231
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_lastname", [], "any", false, false, false, 231), "html", null, true);
        echo "\" >
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input maxlength=\"10\" type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"";
        // line 241
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_tel", [], "any", false, false, false, 241), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input maxlength=\"10\" type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"";
        // line 251
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_mobile", [], "any", false, false, false, 251), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"";
        // line 261
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_email", [], "any", false, false, false, 261), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tตำแหน่ง\t\t\t\t\t:
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t<input type=\"text\" readonly class=\"form-control form-control-sm\" id=\"mr_position_id\" placeholder=\"\" value=\"";
        // line 271
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_position_name", [], "any", false, false, false, 271), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tสาขา :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div id=\"mr_branch_error\"class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_id\" style=\"width:100%;\">
\t\t\t\t\t\t<option selected value=\"";
        // line 282
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 282), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_name", [], "any", false, false, false, 282), "html", null, true);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\">
\t\t\t\t\t\t\t";
        // line 296
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 297
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 297), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 297) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_id", [], "any", false, false, false, 297))) {
                echo " selected=\"selected\" ";
            }
            echo " >";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_code", [], "any", false, false, false, 297), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_name", [], "any", false, false, false, 297), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 298
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_floor\" style=\"width:100%;\" >
\t\t\t\t\t\t\t<option value=\"\"> ไม่มีข้อมูล</option>
\t\t\t\t\t\t\t\t<option selected value=\"";
        // line 311
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 311), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 311), "html", null, true);
        echo "</option>\t\t\t
\t\t\t\t\t\t\t\t";
        // line 312
        echo twig_escape_filter($this->env, ($context["floor"] ?? null), "html", null, true);
        echo "
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<!-- <div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_depart\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" value=\"";
        // line 328
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_code", [], "any", false, false, false, 328), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor\" placeholder=\"ชั้น\" value=\"";
        // line 339
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_floor", [], "any", false, false, false, 339), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t\t -->
\t
\t\t\t";
        // line 346
        if (((($context["role_id"] ?? null) == 2) || (($context["role_id"] ?? null) == 5))) {
            // line 347
            echo "\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px;color:red;\">
\t\t\t\t\t***สถานที่ปฏิบัติงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_role_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"2\" ";
            // line 354
            if ((($context["role_id"] ?? null) == 2)) {
                echo "selected ";
            }
            echo "> สำนักงาน(พหลโยธิน)</option>\t\t\t\t
\t\t\t\t\t\t\t<option value=\"5\" ";
            // line 355
            if ((($context["role_id"] ?? null) == 5)) {
                echo "selected ";
            }
            echo "> สาขา/สาขาและอาคารอื่นๆ</option>\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t";
        }
        // line 361
        echo "\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึกการแก้ไข</button>
\t\t\t\t<br>
\t\t\t\t<center>
\t\t\t\t\t<a href = \"../user/change_password.php?usr=";
        // line 366
        echo twig_escape_filter($this->env, ($context["userID"] ?? null), "html", null, true);
        echo "\"><b> เปลี่ยนรหัสผ่าน คลิกที่นี่</b></a>
\t\t\t\t</center>
\t\t\t</div>
\t</div>
\t

";
    }

    // line 375
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 377
        if ((($context["debug"] ?? null) != "")) {
            // line 378
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 380
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/profile.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  556 => 380,  550 => 378,  548 => 377,  541 => 375,  530 => 366,  523 => 361,  512 => 355,  506 => 354,  497 => 347,  495 => 346,  485 => 339,  471 => 328,  452 => 312,  446 => 311,  431 => 298,  414 => 297,  410 => 296,  391 => 282,  377 => 271,  364 => 261,  351 => 251,  338 => 241,  325 => 231,  311 => 220,  297 => 209,  286 => 201,  282 => 200,  273 => 193,  269 => 192,  264 => 189,  260 => 188,  96 => 29,  92 => 28,  70 => 8,  66 => 7,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block styleReady %}
.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}
{% endblock %}

{% block domReady %}
\t\tvar usrID = '{{ userID }}';

\t\t\$(\"#mr_role_id\").change(function(){
\t\t    var role_id = \$(this).val();
\t\t\t//alert(role_id);
\t\t\talertify.confirm('แก้ไขสถานที่ปฏิบัติงาน :','ยืนยันการเปลี่ยนสถานที่ปฏิบัติงาน', function(){ 
\t\t\t\t//alertify.alert(\"ok\");
\t\t\t\t\$.post('./ajax/ajax_UpdateUserRole.php',{role_id:role_id},
\t\t\t\t\tfunction(res) {\t
\t\t\t\t\t\tif ( res.status == 200 ){
\t\t\t\t\t\t\talertify.alert('',\"บันทึกสำเร็จ\",function(){window.location.reload();});
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('error',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t},'json');
\t\t\t},function(){ 
\t\t\t\talertify.error('Cancel')
\t\t\t});
\t\t});

\t\t\$(\"#btn_save\").click(function(){
\t\t\t\$('.myErrorClass').removeClass('myErrorClass');

\t\t\tvar branch_floor = \$('#mr_branch_floor').select2('data')
\t\t\tvar mr_position_id \t\t= \$(\"#mr_position_id\").val();
\t\t\tvar mr_branch_floor \t= branch_floor[0].text;
\t\t\tvar floor_id \t\t\t= branch_floor[0].id;
\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\tvar name \t\t\t\t= \$(\"#name\").val();
\t\t\tvar last_name \t\t\t= \$(\"#last_name\").val();
\t\t\tvar tel \t\t\t\t= \$(\"#tel\").val();
\t\t\tvar tel_mobile \t\t\t= \$(\"#tel_mobile\").val();
\t\t\tvar email \t\t\t\t= \$(\"#email\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar mr_branch_id \t\t= \$(\"#mr_branch_id\").val();
\t\t\tvar department \t\t= \$(\"#department\").val();
\t\t\tvar\tstatus \t\t\t\t= true;
\t\t\t
\t\t\tif( mr_branch_id == \"\" || mr_branch_id == null){
\t\t\t\t//\$('#mr_branch_error').css({'border-bottom':' 1px solid red'});
\t\t\t\t\$('#select2-mr_branch_id-container').addClass('myErrorClass');
\t\t\t\tstatus = false;
\t\t\t\talert('กรุณากรอกข้อมูล สาขา');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( pass_emp == \"\" || pass_emp == null){
\t\t\t\t//\$('#pass_emp').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#pass_emp').addClass('myErrorClass');
\t\t\t\tstatus = false;
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif(\tname == \"\" || name == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#name').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#name').addClass('myErrorClass');
\t\t\t\tstatus = false;
\t\t\t\talert('กรุณากรอกข้อมูล ชื่อ');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif( last_name == \"\" || last_name == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#last_name ').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#last_name ').addClass('myErrorClass');
\t\t\t\talert('กรุณากรอกข้อมูล นามสกุล');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif( email == \"\" || email == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#email ').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#email ').addClass('myErrorClass');
\t\t\t\talert('กรุณากรอกข้อมูล E-mail');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( tel == \"\" || tel == null){
\t\t\t\tstatus = false;
\t\t\t\t//\$('#tel ').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\$('#tel ').addClass('myErrorClass');
\t\t\t\talert('กรุณากรอกข้อมูล เบอร์โทรศัพท์');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tif( status === true ){
\t\t\t\talertify.confirm('บันทึก','ยืนยันการแก้ไขข้อมูล', 
\t\t\t\tfunction(){
\t\t\t\t\$.post(
\t\t\t\t\t'./ajax/ajax_update_emp.php',
\t\t\t\t\t{
\t\t\t\t\t\tmr_position_id\t : mr_position_id,
\t\t\t\t\t\tfloor_id\t : floor_id,
\t\t\t\t\t\tmr_branch_floor\t : mr_branch_floor,
\t\t\t\t\t\tpass_emp\t : pass_emp,
\t\t\t\t\t\tuser_id\t\t : user_id,
\t\t\t\t\t\temp_id\t\t : emp_id,
\t\t\t\t\t\tname\t\t : name,\t\t
\t\t\t\t\t\tlast_name \t : last_name,
\t\t\t\t\t\ttel \t\t : tel,\t\t
\t\t\t\t\t\ttel_mobile \t : tel_mobile,
\t\t\t\t\t\temail \t\t : email,
\t\t\t\t\t\tmr_branch_id : mr_branch_id,
\t\t\t\t\t\tdepartment : department,
\t\t\t\t\t},
\t\t\t\t\tfunction(res) {
\t\t\t\t\t\tif ( res.status == 200 ){
\t\t\t\t\t\t\talertify.alert('บันทึก',res.error.message);
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('บันทึก',res.error.message);
\t\t\t\t\t\t}

\t\t\t\t\t},'json');
\t\t\t\t},function(){ });
\t\t\t}else{
\t\t\t\talert('กรุณากรอกข้อมูลให้ครบถ้วน');
\t\t\t}\t
\t\t});

\t\$('#mr_branch_id').select2({
\t\ttheme: \"bootstrap4\",
\t\tplaceholder: \"เลือกสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\tresults : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t}).on('select2:select', function (e) {
\t\t\tconsole.log(\$('#mr_branch_id').val());
\t\t\t
\t\t\t

\t\t\tvar branch_id = \$('#mr_branch_id').val();
\t\t\t\$.ajax({
\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tbranch_id: branch_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\$('#mr_branch_floor').html(res.data);
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t})

\t\t});

\t\t
{% endblock %}\t\t\t\t
{% block javaScript %}
\t\t\t\t
{% endblock %}\t\t\t\t\t
\t\t\t\t
{% block Content %}

\t<br>
\t<br>
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้สั่งงาน </h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"{{ user_data.mr_emp_id }}\">
\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"{{ user_data.mr_emp_code }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"{{ user_data.mr_emp_name }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"{{ user_data.mr_emp_lastname }}\" >
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input maxlength=\"10\" type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"{{ user_data.mr_emp_tel }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input maxlength=\"10\" type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"{{ user_data.mr_emp_mobile }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"{{ user_data.mr_emp_email }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tตำแหน่ง\t\t\t\t\t:
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t<input type=\"text\" readonly class=\"form-control form-control-sm\" id=\"mr_position_id\" placeholder=\"\" value=\"{{ user_data.mr_position_name }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tสาขา :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div id=\"mr_branch_error\"class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_id\" style=\"width:100%;\">
\t\t\t\t\t\t<option selected value=\"{{ user_data.mr_branch_id }}\">{{ user_data.mr_branch_name }}</option>
\t\t\t\t\t</select>
\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\">
\t\t\t\t\t\t\t{% for s in department_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_department_id }}\" {% if s.mr_department_id == user_data.mr_department_id %} selected=\"selected\" {% endif %} >{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_floor\" style=\"width:100%;\" >
\t\t\t\t\t\t\t<option value=\"\"> ไม่มีข้อมูล</option>
\t\t\t\t\t\t\t\t<option selected value=\"{{ user_data.mr_branch_floor }}\">{{ user_data.mr_branch_floor }}</option>\t\t\t
\t\t\t\t\t\t\t\t{{floor}}
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<!-- <div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_depart\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" value=\"{{ user_data.mr_department_code }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor\" placeholder=\"ชั้น\" value=\"{{ user_data.mr_department_floor }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t\t -->
\t
\t\t\t{% if role_id == 2 or role_id == 5%}
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px;color:red;\">
\t\t\t\t\t***สถานที่ปฏิบัติงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_role_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"2\" {% if role_id == 2 %}selected {% endif %}> สำนักงาน(พหลโยธิน)</option>\t\t\t\t
\t\t\t\t\t\t\t<option value=\"5\" {% if role_id == 5 %}selected {% endif %}> สาขา/สาขาและอาคารอื่นๆ</option>\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t{% endif %}
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึกการแก้ไข</button>
\t\t\t\t<br>
\t\t\t\t<center>
\t\t\t\t\t<a href = \"../user/change_password.php?usr={{ userID }}\"><b> เปลี่ยนรหัสผ่าน คลิกที่นี่</b></a>
\t\t\t\t</center>
\t\t\t</div>
\t</div>
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "branch/profile.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\profile.tpl");
    }
}

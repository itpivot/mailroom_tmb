<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/create_work.tpl */
class __TwigTemplate_6f3a53c2d60c6adbbd1bfd38a3f9ad06 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/create_work.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "  ";
        // line 11
        echo " 
\t <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t
\t
";
    }

    // line 17
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "\t#btn_save:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#fff;
\t\t}
\t
\t#btn_save{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tpadding:20px;
\t\t\t//margin: 0rem auto;
\t\t}
 
.valit_error{
\tborder:2px solid red;
\tborder-color: red;
\tborder-radius: 5px;
} 
 
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t\tz-index:10000;
\t}
\t
\t
.disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}
.dataTables_filter {
display: none; 
}
select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}
.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
\tborder-width: 1px !important;
\tborder-style: solid !important;
\tborder-color: #cc0000 !important;
\tbackground-color: #f3d8d8 !important;
\tbackground-image: url(http://goo.gl/GXVcmC) !important;
\tbackground-position: 50% 50% !important;
\tbackground-repeat: repeat !important;
}
ul.myErrorClass input {
\tcolor: #666 !important;
}
label.myErrorClass {
\tcolor: red;
\tfont-size: 11px;
\t/*    font-style: italic;*/
\tdisplay: block;
}

";
    }

    // line 142
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 143
        echo twig_escape_filter($this->env, ($context["alertt"] ?? null), "html", null, true);
        echo "
var from_obj = [];
var from_ch = 0;
\$('[data-toggle=\"tooltip\"]').tooltip();

var tbl_data = \$('#tb_addata').DataTable({ 
    \"responsive\": true,
\t//\"scrollX\": true,
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'type'},
        {'data': 'name'},
        {'data': 'tel'},
        {'data': 'strdep'},
        {'data': 'floor'},
        {'data': 'doc_title'},
        {'data': 'remark'}
    ]
});


var tb_con = \$('#tb_con').DataTable({ 
    \"responsive\": true,
\t
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
         {'data': 'no2'},
         {'data': 'department'},
         {'data': 'floor'},
         {'data': 'mr_contact_name'},
         {'data': 'emp_code'},
         {'data': 'emp_tel'},
         {'data': 'mr_position_name'},
         {'data': 'remark'}
    ]
});


\$('#ClearFilter').on( 'click', function () {
\t\$('#search1').val('');
\t\$('#search2').val('');
\t\$('#search3').val('');
\t\$('#search4').val('');
\t\$('#search5').val('');
\t\$('#search6').val('');
\t\$('#search7').val('');
\ttb_con.search( '' ).columns().search( '' ).draw();
});

\$('#search1').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(1).search(this.value, true, false).draw();
});
\$('#search2').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(2).search(this.value, true, false).draw();
});
\$('#search3').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(3).search(this.value, true, false).draw();
});
\$('#search4').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(4).search(this.value, true, false).draw();
});
\$('#search5').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(5).search(this.value, true, false).draw();
});
\$('#search6').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(6).search(this.value, true, false).draw();
});
\$('#search7').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(7).search(this.value, true, false).draw();
});



 \$(\".alert\").alert();

\t\t\$(\"#modal_click\").click(function(){
\t\t\t\$('#modal_showdata').modal({ backdrop: false});
\t\t\tload_contact(4);
\t\t})

\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar branch_floor = '';
\t\t\tvar floor_id = '';
\t\t\t\t\t
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar mr_contact_id \t\t= \$(\"#mr_contact_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar floor_name \t\t\t= \$(\"#floor_name\").val();
\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\tif( \$(\"#type_send_1\").is(':checked')){
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_head\").val();
\t\t\t\t\t\t\tvar remark\t\t\t\t= \$(\"#remark_head\").val();
\t\t\t\t\t\t\tvar destination \t\t= \$(\"#floor_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 1;
\t\t\t\t\t\t\t//alert(floor_send);
\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_branch\").val();
\t\t\t\t\t\t\tvar remark  \t\t\t= \$(\"#remark_branch\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 2;
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t
\t\t\tif( type_send == 2 ){

\t\t\t\tvar branch_floor \t\t= \$('#branch_floor').select2('data');
\t\t\t\tif( branch_floor == \"\" || branch_floor == null ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูล \"ชั้น\" ผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t\t
\t\t\t\t   

\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\tvar branch_send\t\t\t\t= \$(\"#branch_send\").val();
\t\t\t\t\tfloor_id \t\t\t\t= branch_floor[0].id;
\t\t\t\t    branch_floor\t\t\t= branch_floor[0].text;
\t\t\t\tvar quty \t\t\t\t\t= \$(\"#quty_branch\").val();
\t\t\t\t
\t\t\t\t//console.log(branch_receiver);
\t\t\t\t//console.log(branch_send);
\t\t\t\tif( branch_receiver == \"-\" && branch_send == 0 ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t\tif( branch_floor == \"\" || branch_floor == null ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชั้นผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\tvar quty \t\t\t\t\t= \$(\"#quty_head\").val();
\t\t\t\tif( (destination == \"-\" || destination == \"\" || destination == 0) && (\$('#floor_name').val() == 0 || \$('#floor_name').val() == '') ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชั้นผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}
\t\t\t\t\tif( type_send == 2 ){
\t\t\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\t\t\tvar destination \t\t\t= \$(\"#branch_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(destination);
\t\t\t\t\t\t//console.log(branch_send);
\t\t\t\t\t\tif( branch_receiver == \"-\" && destination == 0 ){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(emp_id == \"\" || emp_id == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tquty: quty,
\t\t\t\t\t\t\t\tmr_contact_id: mr_contact_id,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\ttopic: topic,
\t\t\t\t\t\t\t\tcsrf_token \t\t: \$('#csrf_token').val(),
\t\t\t\t\t\t\t\tdestination\t\t: destination ,
\t\t\t\t\t\t\t\ttype_send\t\t: type_send, \t\t\t
\t\t\t\t\t\t\t\tbranch_floor\t: branch_floor, \t\t\t
\t\t\t\t\t\t\t\tfloor_id\t\t: floor_id, \t\t\t
\t\t\t\t\t\t\t}

\t\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล',\"โปรดตรวจสอบข้อมูลผู้รับให้ถูกต้อง\",
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\tsaveBranch(obj);   
                                },
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\talertify.error('ยกเลิก');
\t\t\t\t\t\t\t\t}).set('labels', {ok:'บันทึก', cancel:'ยกเลิก'});
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t                                                                                  
\t\t\t\t\t\t\t                 
\t\t\t\t\t\t\t//\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});                              
\t\t\t\t\t\t\t                                                                                          
\t\t\t\t\t\t}                                                                                             
\t\t\t
\t\t});
\t\t
\t\t
\t\t\$('#branch_floor').select2(\t{theme: 'bootstrap4'});
\t\t\$('#floor_send').select2(\t{theme: 'bootstrap4'});
\t\t\$('#branch_send').select2({
\t\t\ttheme: 'bootstrap4',
\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\tajax: {
\t\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\t\tdataType: \"json\",
\t\t\t\tdelay: 250,
\t\t\t\tprocessResults: function (data) {
\t\t\t\t\treturn {
\t\t\t\t\t\t results : data
\t\t\t\t\t};
\t\t\t\t},
\t\t\t\tcache: true
\t\t\t}
\t\t}).on('select2:select', function (e) {
\t\t\tconsole.log(\$('#branch_send').val());
\t\t\t
\t\t\tvar branch_id = \$('#branch_send').val();
\t\t\tif(branch_id == 1){
\t\t\t\t\$('#type_send_1').click();
\t\t\t\tcheck_type_send('1');
\t\t\t}
\t\t\t
\t\t\t\$.ajax({
\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tbranch_id: branch_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\$('#branch_floor').html(res.data);
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t})

\t\t});
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\ttheme: 'bootstrap4',
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\ttype:\"post\",
\t\t\t\t\tdata: function (params) {
\t\t\t\t\t  var query = {
\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t  }

\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t  return query;
\t\t\t\t\t},
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t};
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});
\t\t
\t\t\$('.select_name').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\ttype:\"post\",
\t\t\t\t\tdata: function (params) {
\t\t\t\t\t  var query = {
\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t  }

\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t  return query;
\t\t\t\t\t},
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t};
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});

//load_contact(3);
\$('#bg_loader').hide();\t\t
\$('#div_error').hide();\t
\t
\$(\"#dataType_contact\").on('select2:select', function(e) {
\t\$('#tb_con').DataTable().clear().draw();
\tload_contact(\$(this).val());
\t\$('#bg_loader').show();
});

\$('#type_send_1').prop('checked',true);\t\t
\$(\"#dataType_contact\").select2({ width: '100%' });

\$('#remark_head').keydown(function(e) {
        
\tvar newLines = \$(this).val().split(\"\\\\n\").length;
\t  //</link>linesUsed.text(newLines);
\t
\tif(e.keyCode == 13 &&  newLines >= 3) {
\t\t\t//linesUsed.css('color', 'red');
\t\t\treturn  false;
\t}
\telse {
\t\t\t//linesUsed.css('color', '');
\t}
});
\$('#remark_branch').keydown(function(e) {
        
\tvar newLines = \$(this).val().split(\"\\\\n\").length;
\t  //</link>linesUsed.text(newLines);
\t
\tif(e.keyCode == 13 &&  newLines >= 3) {
\t\t\t//linesUsed.css('color', 'red');
\t\t\treturn  false;
\t}
\telse {
\t\t\t//linesUsed.css('color', '');
\t}
});


";
    }

    // line 531
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 532
        echo "\t\t
\t\tfunction load_contact(type){\t\t
\t\t\t\$.ajax({
\t\t\t   url : '../employee/ajax/ajax_load_dataContact.php',
\t\t\t   dataType : 'json',
\t\t\t   type : 'POST',
\t\t\t   data : {
\t\t\t\t\t'type': type ,
\t\t\t   },
\t\t\t   success : function(data) {
\t\t\t\t   if(data.status == 200 && data.data != \"\"){
\t\t\t\t\t\t\$('#tb_con').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_con').DataTable().rows.add(data.data).draw();
\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t   }
\t\t\t\t}, beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();\t
\t\t\t\t}
\t\t\t});
\t\t}
\t\tfunction check_type_send( type_send ){
\t\t\t if( type_send == 1 ){ 
\t\t\t\t\$(\"#type_1\").show();
\t\t\t\t\$(\"#type_2\").hide();
\t\t\t }else{
\t\t\t\t\$(\"#type_1\").hide();
\t\t\t\t\$(\"#type_2\").show();
\t\t\t }
\t\t}
\t\t
\t\tfunction getemp(emp_code) {
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_get_empID_bycode.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\temp_code: emp_code
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#modal_showdata').modal('hide');
\t\t\t\t\t\t\$('#modal_showdata').modal('hide');
\t\t\t\t\t\tif(res.data.length > 0){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tid: \$.trim(res['data'][0]['mr_emp_id'])
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\tsetForm(obj);
\t\t\t\t\t\t\t\$('#name_receiver_select').html('');
\t\t\t\t\t\t\t\$('#name_receiver_select').html('<option value=\"'+\$.trim(res['data'][0]['mr_emp_id'])+'\">'+res['data'][0]['mr_emp_code']+':'+res['data'][0]['mr_emp_name']+'  '+res['data'][0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t\$('#name_receiver_select').html('<option value=\"'+\$.trim(res['data'][0]['mr_emp_id'])+'\">'+res['data'][0]['mr_emp_code']+':'+res['data'][0]['mr_emp_name']+'  '+res['data'][0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t\$('#name_receiver_select').html('<option value=\"'+\$.trim(res['data'][0]['mr_emp_id'])+'\">'+res['data'][0]['mr_emp_code']+':'+res['data'][0]['mr_emp_name']+'  '+res['data'][0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\$(\"#depart_receiver\").val(res['department']);
\t\t\t\t\t\t\$(\"#emp_id\").val(res['mr_emp_id']);
\t\t\t\t\t\t\$(\"#tel_receiver\").val(res['mr_emp_tel']);
\t\t\t\t\t\t\$(\"#place_receiver\").val(res['mr_workarea']);
\t\t\t\t\t\t\$(\"#floor_name\").val(res['mr_department_floor']);
\t\t\t\t\t\t\$(\"#branch_receiver\").val(res['branch']);
\t\t\t\t\t\t\$('#branch_send').html('');
\t\t\t\t\t\t\$('#branch_send').append('<option value=\"'+\$.trim(res['mr_branch_id'])+'\">'+res['mr_branch_code']+':'+res['mr_branch_name']+'</option>');
\t\t\t\t\t\tvar branch_id = res['mr_branch_id'];
\t\t\t\t\t\tvar mr_branch_floor = res['mr_branch_floor'];
\t\t\t\t\t\tconsole.log(mr_branch_floor);
\t\t\t\t\t\tif(branch_id == 1){
\t\t\t\t\t\t\t\$('#type_send_1').click();
\t\t\t\t\t\t\tcheck_type_send('1');
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$('#type_send_2').click();
\t\t\t\t\t\t\tcheck_type_send('2');
\t\t\t\t\t\t}
\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t\tbranch_id: branch_id
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\t\t\t\$('#branch_floor').html(res.floor_setName);
\t\t\t\t\t\t\t\t\tif(mr_branch_floor !='' && mr_branch_floor != null ){
\t\t\t\t\t\t\t\t\t\t\$('#branch_floor').val(mr_branch_floor).trigger('change');
\t\t\t\t\t\t\t\t\t\tvar floor = \$('#branch_floor').val()
\t\t\t\t\t\t\t\t\t\t// if(floor == '' || floor == null ){
\t\t\t\t\t\t\t\t\t\t// \t\$('#branch_floor').html(res.floor_setName);
\t\t\t\t\t\t\t\t\t\t// }
\t\t\t\t\t\t\t\t\t\t// console.log(floor);
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t})

\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\tfunction setrowdata(id,eli_name) {
\t\t\tvar emp_id = \$('#emp_'+eli_name).val();
\t\t\t//console.log('>>>>>>>>'+eli_name+'>>>>>>'+emp_id)
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log('vallllllll>>'+\$('#val_type_'+eli_name).val());
\t\t\t\t\t\tif(\$('#val_type_'+eli_name).val() == 3){
\t\t\t\t\t\t\t\$(\"#floor_\"+eli_name).val(res['mr_floor_id']).trigger(\"change\")
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['mr_department_id'])+'\">'+res['department']+'</option>');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).attr({'readonly': 'readonly'}).trigger('change');
\t\t\t\t\t\t\t//console.log('555');
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['mr_branch_id'])+'\">'+res['branch']+'</option>');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).removeAttr('readonly').trigger('change');
\t\t\t\t\t\t}
\t\t\t\t\t\t\$(\"#tel_\"+eli_name).val(res['mr_emp_tel']);
\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\tvar saveBranch = function(obj) {
\t\t\t//return \$.post('ajax/ajax_save_work_branch.php', obj);
\t\t\t\$.ajax({
\t\t\t\turl: 'ajax/ajax_save_work_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: obj,
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\$('#csrf_token').val(res.token);
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' + res.barcodeok + '<b></font></h1>';
\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg, 
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\talertify.success('print');
\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\twindow.location.href=\"../branch/create_work.php\";
\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\tvar url=\"../branch/printcoverpage.php?maim_id=\"+res.work_main_id;
\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val('');
\t\t\t\t\t\t\t\t\$(\"#topic_head\").val('');
\t\t\t\t\t\t\t\t\$(\"#topic_branch\").val('');
\t\t\t\t\t\t\t\t\$(\"#remark_head\").val('');
\t\t\t\t\t\t\t\t\$(\"#remark_branch\").val('');
\t\t\t\t\t\t\t\t\$(\"#tel_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#floor_name\").val('');
\t\t\t\t\t\t\t\t\$(\"#depart_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\$(\"#branch_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\talertify.success('save');
\t\t\t\t\t\t}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'บันทึก'});    
\t\t\t\t\t\t
\t\t\t\t\t}else if(res.status == 200){
\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด!', function(){ 
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',res.message);
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}
\t\t
\t\tfunction import_excel() {
\t\t\t\$('#div_error').hide();\t
\t\t\tvar formData = new FormData();
\t\t\tformData.append('file', \$('#file')[0].files[0]);
\t\t\tif(\$('#file').val() == ''){
\t\t\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t}else{
\t\t\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t\t\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\t// console.log(extension);
\t\t\t\treturn;
\t\t\t }
\t\t\t}
\t\t\t//console.log(formData);
\t\t\t\$.ajax({
\t\t\t\t   url : 'ajax/ajax_readFile_excel_work.php',
\t\t\t\t   dataType : 'json',
\t\t\t\t   type : 'POST',
\t\t\t\t   data : formData,
\t\t\t\t   processData: false,  // tell jQuery not to process the data
\t\t\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t\t\t   success : function(data) {
\t\t\t\t\t
\t\t\t\t\t\tif(data.status != 200 ){
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',data.messagee);
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$('#tb_addata').DataTable().clear().draw();
\t\t\t\t\t\t\t\$('#tb_addata').DataTable().rows.add(data.data).draw();
\t\t\t\t\t\t\t\$('.select_floor').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t\tch_fromdata()

\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\$('.select_type').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t\tvar name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t\tvar v_al = \$(this).val();

\t\t\t\t\t\t\t\tvar myarr = name.split(\"_\");
\t\t\t\t\t\t\t\tvar extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tsetrowdata(v_al,extension)
\t\t\t\t\t\t\t\tif(v_al == 2){
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html(\$('#branch_floor').html());
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html(\$('#floor_send').html());
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t\t// console.log('floor_'+extension)
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$('.select_name').select2({
\t\t\t\t\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\t\ttype:\"post\",
\t\t\t\t\t\t\t\t\tdata: function (params) {
\t\t\t\t\t\t\t\t\t  var query = {
\t\t\t\t\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t\t\t\t\t  }

\t\t\t\t\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t\t\t\t\t  return query;
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\t\tvar name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t\tvar v_al = \$(this).val();
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tvar myarr = name.split(\"_\");
\t\t\t\t\t\t\t\tvar extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\t\tsetrowdata(v_al,extension)
\t\t\t\t\t\t\t\t//console.log(extension)
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\$('.strdep').select2({
\t\t\t\t\t\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\tresults : data
\t\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\t\t//var name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t\t//var v_al = \$(this).val();
\t\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\t\t//setrowdata(v_al,extension)
\t\t\t\t\t\t\t\t// //console.log(extension)
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t});
\t\t\t\t\t\t// console.log(data);
\t\t\t\t\t\tfrom_obj = data.data;
\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t// alert(data);
\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\t\t}
\t\t\t\t   \t}, beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t});
\t\t
\t\t}
\t\t
\t\tfunction ch_fromdata() {
\t\tfrom_ch=0;
\t\t\$('#div_error').hide();
\t\t\$('#div_error').html('กรุณาตรวจสอบข้อมูล !!');
\t\t\t\$.each( from_obj, function( key, value ) {
\t\t\t\tif(\$('#val_type_'+value['in']).val() == \"\" || \$('#val_type_'+value['in']).val() == null){
\t\t\t\t\t\$('#type_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#type_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#emp_'+value['in']).val() == \"\" || \$('#emp_'+value['in']).val() == null){
\t\t\t\t\t\$('#name_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#name_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#floor_'+value['in']).val() == \"\" || \$('#floor_'+value['in']).val() == null || \$('#floor_'+value['in']).val() == 0){
\t\t\t\t\t\$('#floor_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#floor_error_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\t
\t\t\t\tif(\$('#title_'+value['in']).val() == \"\" || \$('#title_'+value['in']).val() == null || \$('#title_'+value['in']).val() == 0){
\t\t\t\t\t\$('#title_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#title_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#val_type_'+value['in']).val() != 3){
\t\t\t\t\tif(\$('#strdep_'+value['in']).val() == \"\" || \$('#strdep_'+value['in']).val() == null || \$('#strdep_'+value['in']).val() == 0){
\t\t\t\t\t\t\$('#dep_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\t\tfrom_ch+=1;
\t\t\t\t\t}else{
\t\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t}else{
\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t}
\t\t\t\t
\t\t\t
\t\t\t\t//console.log('<<<<<<<<<<<<<>>>>>>>>>>>>>>>>');
\t\t\t});
\t\t}
\t\tfunction start_save() {
\t\t\t\$('#div_error').hide();
\t\t\tif( from_ch > 0 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( from_obj.length < 1 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล  !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tvar adll_data = \$('#form_import_excel').serialize()
\t\t\tvar count_data = from_obj.length;
\t\t\t//console.log(adll_data);
\t\t\t//console.log(from_obj.length);
\t\t\t\$.ajax({
\t\t\t\turl: 'ajax/ajax_save_work_import.php?count='+count_data,
\t\t\t\ttype: 'POST',
\t\t\t\tdata: adll_data,
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == \"\"){
\t\t\t\t\t\talertify.confirm('บันทึกข้อมูลสำเร็จ','ท่านต้องการปริ้นใบปะหน้าซองหรือไม่', 
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['arr_id'];  
\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\twindow.location.href=\"../branch/create_work.php\";
\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['arr_id'];  
\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\twindow.location.href=\"../branch/send_work_all.php\";
\t\t\t\t\t\t}).set('labels', {ok:'ปริ้นใบปะหน้า', cancel:'ไม่'});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',res.message);
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}
\t\t\t

function dowload_excel(){
\t\talertify.confirm('Download Excel','คุณต้องการ Download Excel file', 
\t\t\t\tfunction(){
\t\t\t\t\t window.open(\"../themes/TMBexcelimport.xlsx\", \"_blank\");
\t\t\t\t\t //window.location.href='../themes/TMBexcelimport.xlsx';
\t\t\t\t},function() {
\t\t\t}).set('labels', {ok:'ตกลง', cancel:'ยกเลิก'});

}

\t\t\t
";
    }

    // line 972
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 973
        echo "<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\">
<div  class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>บันทึกรายการนำส่ง</b></h3></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 978
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 978), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"";
        // line 980
        echo twig_escape_filter($this->env, ($context["barcode"] ?? null), "html", null, true);
        echo "\">
\t\t\t
\t\t<!-- \t<div class=\"\" style=\"text-align: left;\">
\t\t\t\t <label>เลขที่เอกสาร :  -</label>
\t\t\t</div>\t -->
\t\t\t
\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t\t <h4>รายละเอียดผู้ส่ง </h4>
\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"row row justify-content-sm-center\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">สาขาผู้ส่ง :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อสาขา\" value=\"";
        // line 993
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_branch_code", [], "any", false, false, false, 993), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_branch_name", [], "any", false, false, false, 993), "html", null, true);
        echo "\" disabled>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"mr_emp_tel\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_emp_tel\" placeholder=\"เบอร์โทร\" value=\"";
        // line 1001
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_tel", [], "any", false, false, false, 1001), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"div_re_select\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">ชื่อผู้ส่ง  :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\"  value=\"";
        // line 1015
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_code", [], "any", false, false, false, 1015), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_name", [], "any", false, false, false, 1015), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_lastname", [], "any", false, false, false, 1015), "html", null, true);
        echo "\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-6\" style=\"padding-left:0px\">
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>
\t\t
\t\t<!-- ----------------------------------------------------------------------- \tรายละเอียดผู้รับ -->
\t<hr>\t
\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#page_create_work\" role=\"tab\" aria-controls=\"tab_1\" aria-selected=\"true\">สร้างรายการนำส่ง</a>
\t\t  </li>
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#page_inport_excel\" role=\"tab\" aria-controls=\"tab_2\" aria-selected=\"false\">Import Excel</a>
\t\t  </li>
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" onclick=\"dowload_excel();\">Download Templates Excel</a>
\t\t  </li>
\t\t</ul>
\t<div class=\"tab-content\" id=\"myTabContent\">
\t<div id=\"page_inport_excel\" class=\"tab-pane fade\" aria-labelledby=\"tab2-tab\" role=\"tabpanel\">
\t\t<form id=\"form_import_excel\">
\t\t\t<div class=\"form-group\">
\t\t\t<a onclick=\"\$('#modal_click').click();\"data-toggle=\"tooltip\" data-placement=\"top\" title=\"ค้นหารายชื่อผู้รับ\"><i class=\"material-icons\">help</i></a>
\t\t\t\t<label for=\"file\">
\t\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t\t\t<h4>Import File </h4>  
\t\t\t\t\t</div>
\t\t\t\t</label>
\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t</div>
\t<br>
\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t<button onclick=\"start_save()\" type=\"button\" class=\"btn btn-success\">&nbsp;Save&nbsp;</button>
\t\t\t<br>
\t\t\t<hr>
\t\t\t<br>
\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\">
\t\t\t  A simple danger alert—check it out!
\t\t\t</div>
\t
\t\t
\t\t\t<div class=\"table table-responsive\">
\t\t\t<table id=\"tb_addata\" width=\"100%\">
\t\t\t\t  <thead>
\t\t\t\t\t<tr>
\t\t\t\t\t  <td>No</td>
\t\t\t\t\t  <td>ประเภทการส่ง</td>
\t\t\t\t\t  <td>ผู้รับงาน</td>
\t\t\t\t\t  <td>ตำแหน่ง</td>
\t\t\t\t\t  <td>แผนก</td>
\t\t\t\t\t  <td>ชั้น</td>
\t\t\t\t\t  <td>ชื่อเอกสาร</td>
\t\t\t\t\t  <td>หมายเหตุ</td>
\t\t\t\t\t</tr>
\t\t\t\t  </thead>
\t\t\t\t  <tbody id=\"t_data\">
\t\t\t\t\t
\t\t\t\t  </tbody>
\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t
\t\t</form>
\t<br>
\t<br>
\t<br>
\t<br>
\t</div>\t\t

\t<div id=\"page_create_work\" class=\"tab-pane fade show active tab-pane fade\" aria-labelledby=\"tab1-tab\" role=\"tabpanel\">
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t <h4>รายละเอียดผู้รับ</h4>
\t\t\t</div>
\t\t\t<input maxlength=\"150\" type=\"hidden\" class=\"form-control form-control-sm\" id=\"mr_contact_id\" placeholder=\"\">
\t\t\t<div class=\"\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"name_receiver\">ประเภทการส่ง  :</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_1\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_1\" value=\"1\" onclick=\"check_type_send('1');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สำนักงานใหญ่(TMB)
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_2\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_2\" value=\"2\" onclick=\"check_type_send('2');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สาขา
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t
\t\t\t\t\t<!-- <div class=\"col-1\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t -->
\t\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t\t\t
\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t";
        // line 1126
        echo "

\t\t\t\t\t\t\t<label for=\"name_receiver_select\">ค้นหาผู้รับ  :<span style=\"color:red;\">*</span></label><br>
\t\t\t\t\t\t\t<div class=\"input-group btn-warning bg-info\">
\t\t\t\t\t\t\t\t\t<select class=\"\" id=\"name_receiver_select\" style=\"width:100%;\" ></select>
\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t<button id=\"modal_click\" style=\"height: 28px;\" class=\"btn btn-outline-info btn-sm\" type=\"button\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"รายชื่อผู้ที่เกี่ยวข้องของแต่ละหน่วยงาน\"><i class=\"material-icons\">
\t\t\t\t\t\t\t\t\t\tmore_horiz
\t\t\t\t\t\t\t\t\t\t</i></button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>






\t\t\t\t\t\t\t 
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"tel_receiver\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_receiver\" placeholder=\"เบอร์โทร\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div id=\"type_1\" style=\"display:;\">
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"depart_receiver\">แผนก : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart_receiver\" placeholder=\"ชื่อแผนก\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_name\">ชั้น : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor_name\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_send\">ชั้นผู้รับ\t: </label>
\t\t\t\t\t\t\t\t<select class=\"form-control form-control-lg\" id=\"floor_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t<option value=\"0\" > เลือกชั้นปลายทาง</option>
\t\t\t\t\t\t\t\t\t";
        // line 1182
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 1183
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_floor_id", [], "any", false, false, false, 1183), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "name", [], "any", false, false, false, 1183), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1184
        echo "\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label  for=\"topic_head\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"text\" class=\"form-control\" id=\"topic_head\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-6\" style=\"\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label  for=\"quty_head\">จำนวน :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"number\" value=\"1\" class=\"form-control\" id=\"quty_head\" placeholder=\"จำนวน\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_head\">รายละเอียดของเอกสาร :  </label>
\t\t\t\t\t\t\t\t<textarea  maxlength=\"150\" class=\"form-control\" id=\"remark_head\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t

\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div id=\"type_2\" style=\"display:none;\">
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_receiver\">สาขา :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"branch_receiver\" placeholder=\"ชื่อสาขา\" disabled>
\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"place_receiver\">สถานที่อยู่ :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"place_receiver\" placeholder=\"สถานที่อยู่\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_send\">สาขาผู้รับ : </label>
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"topic_branch\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"text\" class=\"form-control form-control-sm\" id=\"topic_branch\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-3\" style=\"\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label  for=\"quty_branch\">จำนวน :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"number\" value=\"1\" class=\"form-control\" id=\"quty_branch\" placeholder=\"จำนวน\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_floor\">ชั้นผู้รับ : </label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"G\" selected>ชั้น G</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
\t\t\t\t\t\t\t\t</select>\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_branch\">รายละเอียดของเอกสาร  : </label>
\t\t\t\t\t\t\t\t<textarea maxlength=\"150\" class=\"form-control form-control-sm\" id=\"remark_branch\" placeholder=\"รายละเอียดของเอกสาร :\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" style=\"margin-top:50px;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t</div>\t
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn_save\"  >บันทึกรายการ</button>
\t\t\t\t</div>\t\t
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t
\t\t
\t</div>
</div>
\t
\t


<div class=\"modal fade\" id=\"modal_showdata\">
  <div class=\"modal-dialog modal-dialog modal-xl\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\">ค้นหาผู้รับ</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<select id=\"dataType_contact\" class=\"\" style=\"width:30%;\">
\t\t\t\t\t\t\t\t<option value=\"4\" selected>  หน่วยงานที่สาขาส่งประจำ  </option>
\t\t\t\t\t\t\t\t<option value=\"3\"> รายชื่อตามหน่วยงาน  </option>
\t\t\t\t\t\t\t\t<option value=\"2\"> รายชื่อตามสาขา </option>
\t\t\t\t\t\t\t\t<option value=\"1\"> รายชื่อติดต่อ อื่นๆ  </option>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
      <div class=\"modal-body\">
\t  <div style=\"overflow-x:auto;\">
        <table class=\"table\" id=\"tb_con\">
\t\t\t  <thead>
\t\t\t  
\t\t\t\t<tr>
\t\t\t\t  <td><button type=\"button\" id=\"ClearFilter\" class=\"btn btn-secondary\">ClearFilter</button></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search1\"  placeholder=\"ชื่อหน่วยงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search2\"  placeholder=\"ชั้น\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search3\"  placeholder=\"ผู้รับงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search4\"  placeholder=\"รหัสพนักงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search5\"  placeholder=\"เบอร์โทร\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search6\"  placeholder=\"ตำแหน่ง\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search7\"  placeholder=\"หมายเหตุ\"></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <td>#</td>
\t\t\t\t  <td>ชื่อหน่วยงาน</td>
\t\t\t\t  <td>ชั้น</td>
\t\t\t\t  <td>ผู้รับงาน</td>
\t\t\t\t  <td>รหัสพนักงาน</td>
\t\t\t\t  <td>เบอร์โทร</td>
\t\t\t\t  <td>ตำแหน่ง</td>
\t\t\t\t  <td>หมายเหตุ</td>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t  ";
        // line 1373
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contactdata"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 1374
            echo "\t\t\t\t<tr>
\t\t\t\t  <td scope=\"row\"><button type=\"button\" class=\"btn btn-link\" onclick=\"getemp('";
            // line 1375
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_code", [], "any", false, false, false, 1375), "html", null, true);
            echo "')\">เลือก</button></td>
\t\t\t\t  <td>";
            // line 1376
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "department_code", [], "any", false, false, false, 1376), "html", null, true);
            echo ":";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "department_name", [], "any", false, false, false, 1376), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1377
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "floor", [], "any", false, false, false, 1377), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1378
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_contact_name", [], "any", false, false, false, 1378), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1379
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_code", [], "any", false, false, false, 1379), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1380
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_tel", [], "any", false, false, false, 1380), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1381
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_tel", [], "any", false, false, false, 1381), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1382
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "remark", [], "any", false, false, false, 1382), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1385
        echo "\t\t\t  </tbody>
\t\t\t</table>
\t\t</div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>


<div id=\"bg_loader\" class=\"card\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>

";
    }

    // line 1404
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 1406
        if ((($context["debug"] ?? null) != "")) {
            // line 1407
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 1409
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/create_work.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1574 => 1409,  1568 => 1407,  1566 => 1406,  1559 => 1404,  1539 => 1385,  1530 => 1382,  1526 => 1381,  1522 => 1380,  1518 => 1379,  1514 => 1378,  1510 => 1377,  1504 => 1376,  1500 => 1375,  1497 => 1374,  1493 => 1373,  1302 => 1184,  1291 => 1183,  1287 => 1182,  1229 => 1126,  1116 => 1015,  1099 => 1001,  1086 => 993,  1070 => 980,  1065 => 978,  1056 => 973,  1052 => 972,  609 => 532,  605 => 531,  216 => 143,  212 => 142,  86 => 18,  82 => 17,  73 => 11,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
  {#   
\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css\">
\t\t<script type='text/javascript' src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js\"></script>
#} 
\t <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t
\t
{% endblock %}
{% block styleReady %}
\t#btn_save:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#fff;
\t\t}
\t
\t#btn_save{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tpadding:20px;
\t\t\t//margin: 0rem auto;
\t\t}
 
.valit_error{
\tborder:2px solid red;
\tborder-color: red;
\tborder-radius: 5px;
} 
 
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t\tz-index:10000;
\t}
\t
\t
.disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}
.dataTables_filter {
display: none; 
}
select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}
.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
\tborder-width: 1px !important;
\tborder-style: solid !important;
\tborder-color: #cc0000 !important;
\tbackground-color: #f3d8d8 !important;
\tbackground-image: url(http://goo.gl/GXVcmC) !important;
\tbackground-position: 50% 50% !important;
\tbackground-repeat: repeat !important;
}
ul.myErrorClass input {
\tcolor: #666 !important;
}
label.myErrorClass {
\tcolor: red;
\tfont-size: 11px;
\t/*    font-style: italic;*/
\tdisplay: block;
}

{% endblock %}

{% block domReady %}
{{alertt}}
var from_obj = [];
var from_ch = 0;
\$('[data-toggle=\"tooltip\"]').tooltip();

var tbl_data = \$('#tb_addata').DataTable({ 
    \"responsive\": true,
\t//\"scrollX\": true,
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'type'},
        {'data': 'name'},
        {'data': 'tel'},
        {'data': 'strdep'},
        {'data': 'floor'},
        {'data': 'doc_title'},
        {'data': 'remark'}
    ]
});


var tb_con = \$('#tb_con').DataTable({ 
    \"responsive\": true,
\t
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
         {'data': 'no2'},
         {'data': 'department'},
         {'data': 'floor'},
         {'data': 'mr_contact_name'},
         {'data': 'emp_code'},
         {'data': 'emp_tel'},
         {'data': 'mr_position_name'},
         {'data': 'remark'}
    ]
});


\$('#ClearFilter').on( 'click', function () {
\t\$('#search1').val('');
\t\$('#search2').val('');
\t\$('#search3').val('');
\t\$('#search4').val('');
\t\$('#search5').val('');
\t\$('#search6').val('');
\t\$('#search7').val('');
\ttb_con.search( '' ).columns().search( '' ).draw();
});

\$('#search1').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(1).search(this.value, true, false).draw();
});
\$('#search2').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(2).search(this.value, true, false).draw();
});
\$('#search3').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(3).search(this.value, true, false).draw();
});
\$('#search4').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(4).search(this.value, true, false).draw();
});
\$('#search5').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(5).search(this.value, true, false).draw();
});
\$('#search6').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(6).search(this.value, true, false).draw();
});
\$('#search7').on( 'keyup', function () {
\t\t//table.search( this.value ).draw();
\t\ttb_con.column(7).search(this.value, true, false).draw();
});



 \$(\".alert\").alert();

\t\t\$(\"#modal_click\").click(function(){
\t\t\t\$('#modal_showdata').modal({ backdrop: false});
\t\t\tload_contact(4);
\t\t})

\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar branch_floor = '';
\t\t\tvar floor_id = '';
\t\t\t\t\t
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar mr_contact_id \t\t= \$(\"#mr_contact_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar floor_name \t\t\t= \$(\"#floor_name\").val();
\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\tif( \$(\"#type_send_1\").is(':checked')){
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_head\").val();
\t\t\t\t\t\t\tvar remark\t\t\t\t= \$(\"#remark_head\").val();
\t\t\t\t\t\t\tvar destination \t\t= \$(\"#floor_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 1;
\t\t\t\t\t\t\t//alert(floor_send);
\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_branch\").val();
\t\t\t\t\t\t\tvar remark  \t\t\t= \$(\"#remark_branch\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 2;
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t
\t\t\tif( type_send == 2 ){

\t\t\t\tvar branch_floor \t\t= \$('#branch_floor').select2('data');
\t\t\t\tif( branch_floor == \"\" || branch_floor == null ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูล \"ชั้น\" ผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t\t
\t\t\t\t   

\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\tvar branch_send\t\t\t\t= \$(\"#branch_send\").val();
\t\t\t\t\tfloor_id \t\t\t\t= branch_floor[0].id;
\t\t\t\t    branch_floor\t\t\t= branch_floor[0].text;
\t\t\t\tvar quty \t\t\t\t\t= \$(\"#quty_branch\").val();
\t\t\t\t
\t\t\t\t//console.log(branch_receiver);
\t\t\t\t//console.log(branch_send);
\t\t\t\tif( branch_receiver == \"-\" && branch_send == 0 ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t\tif( branch_floor == \"\" || branch_floor == null ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชั้นผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\tvar quty \t\t\t\t\t= \$(\"#quty_head\").val();
\t\t\t\tif( (destination == \"-\" || destination == \"\" || destination == 0) && (\$('#floor_name').val() == 0 || \$('#floor_name').val() == '') ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชั้นผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}
\t\t\t\t\tif( type_send == 2 ){
\t\t\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\t\t\tvar destination \t\t\t= \$(\"#branch_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(destination);
\t\t\t\t\t\t//console.log(branch_send);
\t\t\t\t\t\tif( branch_receiver == \"-\" && destination == 0 ){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(emp_id == \"\" || emp_id == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tquty: quty,
\t\t\t\t\t\t\t\tmr_contact_id: mr_contact_id,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\ttopic: topic,
\t\t\t\t\t\t\t\tcsrf_token \t\t: \$('#csrf_token').val(),
\t\t\t\t\t\t\t\tdestination\t\t: destination ,
\t\t\t\t\t\t\t\ttype_send\t\t: type_send, \t\t\t
\t\t\t\t\t\t\t\tbranch_floor\t: branch_floor, \t\t\t
\t\t\t\t\t\t\t\tfloor_id\t\t: floor_id, \t\t\t
\t\t\t\t\t\t\t}

\t\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล',\"โปรดตรวจสอบข้อมูลผู้รับให้ถูกต้อง\",
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\tsaveBranch(obj);   
                                },
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\talertify.error('ยกเลิก');
\t\t\t\t\t\t\t\t}).set('labels', {ok:'บันทึก', cancel:'ยกเลิก'});
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t                                                                                  
\t\t\t\t\t\t\t                 
\t\t\t\t\t\t\t//\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});                              
\t\t\t\t\t\t\t                                                                                          
\t\t\t\t\t\t}                                                                                             
\t\t\t
\t\t});
\t\t
\t\t
\t\t\$('#branch_floor').select2(\t{theme: 'bootstrap4'});
\t\t\$('#floor_send').select2(\t{theme: 'bootstrap4'});
\t\t\$('#branch_send').select2({
\t\t\ttheme: 'bootstrap4',
\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\tajax: {
\t\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\t\tdataType: \"json\",
\t\t\t\tdelay: 250,
\t\t\t\tprocessResults: function (data) {
\t\t\t\t\treturn {
\t\t\t\t\t\t results : data
\t\t\t\t\t};
\t\t\t\t},
\t\t\t\tcache: true
\t\t\t}
\t\t}).on('select2:select', function (e) {
\t\t\tconsole.log(\$('#branch_send').val());
\t\t\t
\t\t\tvar branch_id = \$('#branch_send').val();
\t\t\tif(branch_id == 1){
\t\t\t\t\$('#type_send_1').click();
\t\t\t\tcheck_type_send('1');
\t\t\t}
\t\t\t
\t\t\t\$.ajax({
\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tbranch_id: branch_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\$('#branch_floor').html(res.data);
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t})

\t\t});
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\ttheme: 'bootstrap4',
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\ttype:\"post\",
\t\t\t\t\tdata: function (params) {
\t\t\t\t\t  var query = {
\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t  }

\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t  return query;
\t\t\t\t\t},
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t};
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});
\t\t
\t\t\$('.select_name').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\ttype:\"post\",
\t\t\t\t\tdata: function (params) {
\t\t\t\t\t  var query = {
\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t  }

\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t  return query;
\t\t\t\t\t},
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t};
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});

//load_contact(3);
\$('#bg_loader').hide();\t\t
\$('#div_error').hide();\t
\t
\$(\"#dataType_contact\").on('select2:select', function(e) {
\t\$('#tb_con').DataTable().clear().draw();
\tload_contact(\$(this).val());
\t\$('#bg_loader').show();
});

\$('#type_send_1').prop('checked',true);\t\t
\$(\"#dataType_contact\").select2({ width: '100%' });

\$('#remark_head').keydown(function(e) {
        
\tvar newLines = \$(this).val().split(\"\\\\n\").length;
\t  //</link>linesUsed.text(newLines);
\t
\tif(e.keyCode == 13 &&  newLines >= 3) {
\t\t\t//linesUsed.css('color', 'red');
\t\t\treturn  false;
\t}
\telse {
\t\t\t//linesUsed.css('color', '');
\t}
});
\$('#remark_branch').keydown(function(e) {
        
\tvar newLines = \$(this).val().split(\"\\\\n\").length;
\t  //</link>linesUsed.text(newLines);
\t
\tif(e.keyCode == 13 &&  newLines >= 3) {
\t\t\t//linesUsed.css('color', 'red');
\t\t\treturn  false;
\t}
\telse {
\t\t\t//linesUsed.css('color', '');
\t}
});


{% endblock %}\t



{% block javaScript %}
\t\t
\t\tfunction load_contact(type){\t\t
\t\t\t\$.ajax({
\t\t\t   url : '../employee/ajax/ajax_load_dataContact.php',
\t\t\t   dataType : 'json',
\t\t\t   type : 'POST',
\t\t\t   data : {
\t\t\t\t\t'type': type ,
\t\t\t   },
\t\t\t   success : function(data) {
\t\t\t\t   if(data.status == 200 && data.data != \"\"){
\t\t\t\t\t\t\$('#tb_con').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_con').DataTable().rows.add(data.data).draw();
\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t   }
\t\t\t\t}, beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();\t
\t\t\t\t}
\t\t\t});
\t\t}
\t\tfunction check_type_send( type_send ){
\t\t\t if( type_send == 1 ){ 
\t\t\t\t\$(\"#type_1\").show();
\t\t\t\t\$(\"#type_2\").hide();
\t\t\t }else{
\t\t\t\t\$(\"#type_1\").hide();
\t\t\t\t\$(\"#type_2\").show();
\t\t\t }
\t\t}
\t\t
\t\tfunction getemp(emp_code) {
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_get_empID_bycode.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\temp_code: emp_code
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#modal_showdata').modal('hide');
\t\t\t\t\t\t\$('#modal_showdata').modal('hide');
\t\t\t\t\t\tif(res.data.length > 0){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tid: \$.trim(res['data'][0]['mr_emp_id'])
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\tsetForm(obj);
\t\t\t\t\t\t\t\$('#name_receiver_select').html('');
\t\t\t\t\t\t\t\$('#name_receiver_select').html('<option value=\"'+\$.trim(res['data'][0]['mr_emp_id'])+'\">'+res['data'][0]['mr_emp_code']+':'+res['data'][0]['mr_emp_name']+'  '+res['data'][0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t\$('#name_receiver_select').html('<option value=\"'+\$.trim(res['data'][0]['mr_emp_id'])+'\">'+res['data'][0]['mr_emp_code']+':'+res['data'][0]['mr_emp_name']+'  '+res['data'][0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t\$('#name_receiver_select').html('<option value=\"'+\$.trim(res['data'][0]['mr_emp_id'])+'\">'+res['data'][0]['mr_emp_code']+':'+res['data'][0]['mr_emp_name']+'  '+res['data'][0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\$(\"#depart_receiver\").val(res['department']);
\t\t\t\t\t\t\$(\"#emp_id\").val(res['mr_emp_id']);
\t\t\t\t\t\t\$(\"#tel_receiver\").val(res['mr_emp_tel']);
\t\t\t\t\t\t\$(\"#place_receiver\").val(res['mr_workarea']);
\t\t\t\t\t\t\$(\"#floor_name\").val(res['mr_department_floor']);
\t\t\t\t\t\t\$(\"#branch_receiver\").val(res['branch']);
\t\t\t\t\t\t\$('#branch_send').html('');
\t\t\t\t\t\t\$('#branch_send').append('<option value=\"'+\$.trim(res['mr_branch_id'])+'\">'+res['mr_branch_code']+':'+res['mr_branch_name']+'</option>');
\t\t\t\t\t\tvar branch_id = res['mr_branch_id'];
\t\t\t\t\t\tvar mr_branch_floor = res['mr_branch_floor'];
\t\t\t\t\t\tconsole.log(mr_branch_floor);
\t\t\t\t\t\tif(branch_id == 1){
\t\t\t\t\t\t\t\$('#type_send_1').click();
\t\t\t\t\t\t\tcheck_type_send('1');
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$('#type_send_2').click();
\t\t\t\t\t\t\tcheck_type_send('2');
\t\t\t\t\t\t}
\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t\tbranch_id: branch_id
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\t\t\t\$('#branch_floor').html(res.floor_setName);
\t\t\t\t\t\t\t\t\tif(mr_branch_floor !='' && mr_branch_floor != null ){
\t\t\t\t\t\t\t\t\t\t\$('#branch_floor').val(mr_branch_floor).trigger('change');
\t\t\t\t\t\t\t\t\t\tvar floor = \$('#branch_floor').val()
\t\t\t\t\t\t\t\t\t\t// if(floor == '' || floor == null ){
\t\t\t\t\t\t\t\t\t\t// \t\$('#branch_floor').html(res.floor_setName);
\t\t\t\t\t\t\t\t\t\t// }
\t\t\t\t\t\t\t\t\t\t// console.log(floor);
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t})

\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\tfunction setrowdata(id,eli_name) {
\t\t\tvar emp_id = \$('#emp_'+eli_name).val();
\t\t\t//console.log('>>>>>>>>'+eli_name+'>>>>>>'+emp_id)
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t//console.log('vallllllll>>'+\$('#val_type_'+eli_name).val());
\t\t\t\t\t\tif(\$('#val_type_'+eli_name).val() == 3){
\t\t\t\t\t\t\t\$(\"#floor_\"+eli_name).val(res['mr_floor_id']).trigger(\"change\")
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['mr_department_id'])+'\">'+res['department']+'</option>');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).attr({'readonly': 'readonly'}).trigger('change');
\t\t\t\t\t\t\t//console.log('555');
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['mr_branch_id'])+'\">'+res['branch']+'</option>');
\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).removeAttr('readonly').trigger('change');
\t\t\t\t\t\t}
\t\t\t\t\t\t\$(\"#tel_\"+eli_name).val(res['mr_emp_tel']);
\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\tvar saveBranch = function(obj) {
\t\t\t//return \$.post('ajax/ajax_save_work_branch.php', obj);
\t\t\t\$.ajax({
\t\t\t\turl: 'ajax/ajax_save_work_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: obj,
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\$('#csrf_token').val(res.token);
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' + res.barcodeok + '<b></font></h1>';
\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg, 
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\talertify.success('print');
\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\twindow.location.href=\"../branch/create_work.php\";
\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\tvar url=\"../branch/printcoverpage.php?maim_id=\"+res.work_main_id;
\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val('');
\t\t\t\t\t\t\t\t\$(\"#topic_head\").val('');
\t\t\t\t\t\t\t\t\$(\"#topic_branch\").val('');
\t\t\t\t\t\t\t\t\$(\"#remark_head\").val('');
\t\t\t\t\t\t\t\t\$(\"#remark_branch\").val('');
\t\t\t\t\t\t\t\t\$(\"#tel_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#floor_name\").val('');
\t\t\t\t\t\t\t\t\$(\"#depart_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\$(\"#branch_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\talertify.success('save');
\t\t\t\t\t\t}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'บันทึก'});    
\t\t\t\t\t\t
\t\t\t\t\t}else if(res.status == 200){
\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด!', function(){ 
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',res.message);
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}
\t\t
\t\tfunction import_excel() {
\t\t\t\$('#div_error').hide();\t
\t\t\tvar formData = new FormData();
\t\t\tformData.append('file', \$('#file')[0].files[0]);
\t\t\tif(\$('#file').val() == ''){
\t\t\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t}else{
\t\t\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t\t\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\t// console.log(extension);
\t\t\t\treturn;
\t\t\t }
\t\t\t}
\t\t\t//console.log(formData);
\t\t\t\$.ajax({
\t\t\t\t   url : 'ajax/ajax_readFile_excel_work.php',
\t\t\t\t   dataType : 'json',
\t\t\t\t   type : 'POST',
\t\t\t\t   data : formData,
\t\t\t\t   processData: false,  // tell jQuery not to process the data
\t\t\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t\t\t   success : function(data) {
\t\t\t\t\t
\t\t\t\t\t\tif(data.status != 200 ){
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',data.messagee);
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$('#tb_addata').DataTable().clear().draw();
\t\t\t\t\t\t\t\$('#tb_addata').DataTable().rows.add(data.data).draw();
\t\t\t\t\t\t\t\$('.select_floor').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t\tch_fromdata()

\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\$('.select_type').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t\tvar name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t\tvar v_al = \$(this).val();

\t\t\t\t\t\t\t\tvar myarr = name.split(\"_\");
\t\t\t\t\t\t\t\tvar extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tsetrowdata(v_al,extension)
\t\t\t\t\t\t\t\tif(v_al == 2){
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html(\$('#branch_floor').html());
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t\t\$('#floor_'+extension).html(\$('#floor_send').html());
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t\t// console.log('floor_'+extension)
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$('.select_name').select2({
\t\t\t\t\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\t\ttype:\"post\",
\t\t\t\t\t\t\t\t\tdata: function (params) {
\t\t\t\t\t\t\t\t\t  var query = {
\t\t\t\t\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t\t\t\t\t  }

\t\t\t\t\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t\t\t\t\t  return query;
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\t\tvar name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t\tvar v_al = \$(this).val();
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tvar myarr = name.split(\"_\");
\t\t\t\t\t\t\t\tvar extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\t\tsetrowdata(v_al,extension)
\t\t\t\t\t\t\t\t//console.log(extension)
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\$('.strdep').select2({
\t\t\t\t\t\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\tresults : data
\t\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\t\t//var name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t\t//var v_al = \$(this).val();
\t\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\t\t//setrowdata(v_al,extension)
\t\t\t\t\t\t\t\t// //console.log(extension)
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t});
\t\t\t\t\t\t// console.log(data);
\t\t\t\t\t\tfrom_obj = data.data;
\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t// alert(data);
\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\t\t}
\t\t\t\t   \t}, beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t});
\t\t
\t\t}
\t\t
\t\tfunction ch_fromdata() {
\t\tfrom_ch=0;
\t\t\$('#div_error').hide();
\t\t\$('#div_error').html('กรุณาตรวจสอบข้อมูล !!');
\t\t\t\$.each( from_obj, function( key, value ) {
\t\t\t\tif(\$('#val_type_'+value['in']).val() == \"\" || \$('#val_type_'+value['in']).val() == null){
\t\t\t\t\t\$('#type_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#type_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#emp_'+value['in']).val() == \"\" || \$('#emp_'+value['in']).val() == null){
\t\t\t\t\t\$('#name_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#name_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#floor_'+value['in']).val() == \"\" || \$('#floor_'+value['in']).val() == null || \$('#floor_'+value['in']).val() == 0){
\t\t\t\t\t\$('#floor_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#floor_error_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\t
\t\t\t\tif(\$('#title_'+value['in']).val() == \"\" || \$('#title_'+value['in']).val() == null || \$('#title_'+value['in']).val() == 0){
\t\t\t\t\t\$('#title_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#title_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#val_type_'+value['in']).val() != 3){
\t\t\t\t\tif(\$('#strdep_'+value['in']).val() == \"\" || \$('#strdep_'+value['in']).val() == null || \$('#strdep_'+value['in']).val() == 0){
\t\t\t\t\t\t\$('#dep_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\t\tfrom_ch+=1;
\t\t\t\t\t}else{
\t\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t}else{
\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t}
\t\t\t\t
\t\t\t
\t\t\t\t//console.log('<<<<<<<<<<<<<>>>>>>>>>>>>>>>>');
\t\t\t});
\t\t}
\t\tfunction start_save() {
\t\t\t\$('#div_error').hide();
\t\t\tif( from_ch > 0 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( from_obj.length < 1 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล  !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\t
\t\t\tvar adll_data = \$('#form_import_excel').serialize()
\t\t\tvar count_data = from_obj.length;
\t\t\t//console.log(adll_data);
\t\t\t//console.log(from_obj.length);
\t\t\t\$.ajax({
\t\t\t\turl: 'ajax/ajax_save_work_import.php?count='+count_data,
\t\t\t\ttype: 'POST',
\t\t\t\tdata: adll_data,
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == \"\"){
\t\t\t\t\t\talertify.confirm('บันทึกข้อมูลสำเร็จ','ท่านต้องการปริ้นใบปะหน้าซองหรือไม่', 
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['arr_id'];  
\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\twindow.location.href=\"../branch/create_work.php\";
\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['arr_id'];  
\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\twindow.location.href=\"../branch/send_work_all.php\";
\t\t\t\t\t\t}).set('labels', {ok:'ปริ้นใบปะหน้า', cancel:'ไม่'});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',res.message);
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}
\t\t\t

function dowload_excel(){
\t\talertify.confirm('Download Excel','คุณต้องการ Download Excel file', 
\t\t\t\tfunction(){
\t\t\t\t\t window.open(\"../themes/TMBexcelimport.xlsx\", \"_blank\");
\t\t\t\t\t //window.location.href='../themes/TMBexcelimport.xlsx';
\t\t\t\t},function() {
\t\t\t}).set('labels', {ok:'ตกลง', cancel:'ยกเลิก'});

}

\t\t\t
{% endblock %}
{% block Content %}
<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\">
<div  class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>บันทึกรายการนำส่ง</b></h3></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"{{ barcode }}\">
\t\t\t
\t\t<!-- \t<div class=\"\" style=\"text-align: left;\">
\t\t\t\t <label>เลขที่เอกสาร :  -</label>
\t\t\t</div>\t -->
\t\t\t
\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t\t <h4>รายละเอียดผู้ส่ง </h4>
\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"row row justify-content-sm-center\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">สาขาผู้ส่ง :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อสาขา\" value=\"{{ user_data_show.mr_branch_code }} - {{ user_data_show.mr_branch_name }}\" disabled>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"mr_emp_tel\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_emp_tel\" placeholder=\"เบอร์โทร\" value=\"{{  user_data_show.mr_emp_tel  }}\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"div_re_select\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">ชื่อผู้ส่ง  :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\"  value=\"{{  user_data_show.mr_emp_code  }} - {{  user_data_show.mr_emp_name  }} {{  user_data_show.mr_emp_lastname  }}\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-6\" style=\"padding-left:0px\">
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>
\t\t
\t\t<!-- ----------------------------------------------------------------------- \tรายละเอียดผู้รับ -->
\t<hr>\t
\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#page_create_work\" role=\"tab\" aria-controls=\"tab_1\" aria-selected=\"true\">สร้างรายการนำส่ง</a>
\t\t  </li>
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#page_inport_excel\" role=\"tab\" aria-controls=\"tab_2\" aria-selected=\"false\">Import Excel</a>
\t\t  </li>
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" onclick=\"dowload_excel();\">Download Templates Excel</a>
\t\t  </li>
\t\t</ul>
\t<div class=\"tab-content\" id=\"myTabContent\">
\t<div id=\"page_inport_excel\" class=\"tab-pane fade\" aria-labelledby=\"tab2-tab\" role=\"tabpanel\">
\t\t<form id=\"form_import_excel\">
\t\t\t<div class=\"form-group\">
\t\t\t<a onclick=\"\$('#modal_click').click();\"data-toggle=\"tooltip\" data-placement=\"top\" title=\"ค้นหารายชื่อผู้รับ\"><i class=\"material-icons\">help</i></a>
\t\t\t\t<label for=\"file\">
\t\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t\t\t<h4>Import File </h4>  
\t\t\t\t\t</div>
\t\t\t\t</label>
\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t</div>
\t<br>
\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t<button onclick=\"start_save()\" type=\"button\" class=\"btn btn-success\">&nbsp;Save&nbsp;</button>
\t\t\t<br>
\t\t\t<hr>
\t\t\t<br>
\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\">
\t\t\t  A simple danger alert—check it out!
\t\t\t</div>
\t
\t\t
\t\t\t<div class=\"table table-responsive\">
\t\t\t<table id=\"tb_addata\" width=\"100%\">
\t\t\t\t  <thead>
\t\t\t\t\t<tr>
\t\t\t\t\t  <td>No</td>
\t\t\t\t\t  <td>ประเภทการส่ง</td>
\t\t\t\t\t  <td>ผู้รับงาน</td>
\t\t\t\t\t  <td>ตำแหน่ง</td>
\t\t\t\t\t  <td>แผนก</td>
\t\t\t\t\t  <td>ชั้น</td>
\t\t\t\t\t  <td>ชื่อเอกสาร</td>
\t\t\t\t\t  <td>หมายเหตุ</td>
\t\t\t\t\t</tr>
\t\t\t\t  </thead>
\t\t\t\t  <tbody id=\"t_data\">
\t\t\t\t\t
\t\t\t\t  </tbody>
\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t
\t\t</form>
\t<br>
\t<br>
\t<br>
\t<br>
\t</div>\t\t

\t<div id=\"page_create_work\" class=\"tab-pane fade show active tab-pane fade\" aria-labelledby=\"tab1-tab\" role=\"tabpanel\">
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t <h4>รายละเอียดผู้รับ</h4>
\t\t\t</div>
\t\t\t<input maxlength=\"150\" type=\"hidden\" class=\"form-control form-control-sm\" id=\"mr_contact_id\" placeholder=\"\">
\t\t\t<div class=\"\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"name_receiver\">ประเภทการส่ง  :</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_1\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_1\" value=\"1\" onclick=\"check_type_send('1');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สำนักงานใหญ่(TMB)
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_2\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_2\" value=\"2\" onclick=\"check_type_send('2');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สาขา
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t
\t\t\t\t\t<!-- <div class=\"col-1\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t -->
\t\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t\t\t
\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t{# <div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver_select\">ค้นหาผู้รับ  :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" ></select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t#}


\t\t\t\t\t\t\t<label for=\"name_receiver_select\">ค้นหาผู้รับ  :<span style=\"color:red;\">*</span></label><br>
\t\t\t\t\t\t\t<div class=\"input-group btn-warning bg-info\">
\t\t\t\t\t\t\t\t\t<select class=\"\" id=\"name_receiver_select\" style=\"width:100%;\" ></select>
\t\t\t\t\t\t\t\t<div class=\"input-group-append\">
\t\t\t\t\t\t\t\t\t<button id=\"modal_click\" style=\"height: 28px;\" class=\"btn btn-outline-info btn-sm\" type=\"button\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"รายชื่อผู้ที่เกี่ยวข้องของแต่ละหน่วยงาน\"><i class=\"material-icons\">
\t\t\t\t\t\t\t\t\t\tmore_horiz
\t\t\t\t\t\t\t\t\t\t</i></button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>






\t\t\t\t\t\t\t 
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"tel_receiver\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_receiver\" placeholder=\"เบอร์โทร\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div id=\"type_1\" style=\"display:;\">
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"depart_receiver\">แผนก : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart_receiver\" placeholder=\"ชื่อแผนก\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_name\">ชั้น : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor_name\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_send\">ชั้นผู้รับ\t: </label>
\t\t\t\t\t\t\t\t<select class=\"form-control form-control-lg\" id=\"floor_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t<option value=\"0\" > เลือกชั้นปลายทาง</option>
\t\t\t\t\t\t\t\t\t{% for s in floor_data %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_floor_id }}\" > {{ s.name }}</option>
\t\t\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label  for=\"topic_head\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"text\" class=\"form-control\" id=\"topic_head\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-6\" style=\"\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label  for=\"quty_head\">จำนวน :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"number\" value=\"1\" class=\"form-control\" id=\"quty_head\" placeholder=\"จำนวน\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_head\">รายละเอียดของเอกสาร :  </label>
\t\t\t\t\t\t\t\t<textarea  maxlength=\"150\" class=\"form-control\" id=\"remark_head\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t

\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div id=\"type_2\" style=\"display:none;\">
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_receiver\">สาขา :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"branch_receiver\" placeholder=\"ชื่อสาขา\" disabled>
\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"place_receiver\">สถานที่อยู่ :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"place_receiver\" placeholder=\"สถานที่อยู่\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_send\">สาขาผู้รับ : </label>
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"topic_branch\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"text\" class=\"form-control form-control-sm\" id=\"topic_branch\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-3\" style=\"\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label  for=\"quty_branch\">จำนวน :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input maxlength=\"150\" type=\"number\" value=\"1\" class=\"form-control\" id=\"quty_branch\" placeholder=\"จำนวน\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_floor\">ชั้นผู้รับ : </label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"G\" selected>ชั้น G</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
\t\t\t\t\t\t\t\t</select>\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_branch\">รายละเอียดของเอกสาร  : </label>
\t\t\t\t\t\t\t\t<textarea maxlength=\"150\" class=\"form-control form-control-sm\" id=\"remark_branch\" placeholder=\"รายละเอียดของเอกสาร :\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" style=\"margin-top:50px;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t</div>\t
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn_save\"  >บันทึกรายการ</button>
\t\t\t\t</div>\t\t
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t
\t\t
\t</div>
</div>
\t
\t


<div class=\"modal fade\" id=\"modal_showdata\">
  <div class=\"modal-dialog modal-dialog modal-xl\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\">ค้นหาผู้รับ</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<select id=\"dataType_contact\" class=\"\" style=\"width:30%;\">
\t\t\t\t\t\t\t\t<option value=\"4\" selected>  หน่วยงานที่สาขาส่งประจำ  </option>
\t\t\t\t\t\t\t\t<option value=\"3\"> รายชื่อตามหน่วยงาน  </option>
\t\t\t\t\t\t\t\t<option value=\"2\"> รายชื่อตามสาขา </option>
\t\t\t\t\t\t\t\t<option value=\"1\"> รายชื่อติดต่อ อื่นๆ  </option>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
      <div class=\"modal-body\">
\t  <div style=\"overflow-x:auto;\">
        <table class=\"table\" id=\"tb_con\">
\t\t\t  <thead>
\t\t\t  
\t\t\t\t<tr>
\t\t\t\t  <td><button type=\"button\" id=\"ClearFilter\" class=\"btn btn-secondary\">ClearFilter</button></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search1\"  placeholder=\"ชื่อหน่วยงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search2\"  placeholder=\"ชั้น\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search3\"  placeholder=\"ผู้รับงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search4\"  placeholder=\"รหัสพนักงาน\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search5\"  placeholder=\"เบอร์โทร\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search6\"  placeholder=\"ตำแหน่ง\"></td>
\t\t\t\t  <td><input type=\"text\" class=\"form-control\" id=\"search7\"  placeholder=\"หมายเหตุ\"></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <td>#</td>
\t\t\t\t  <td>ชื่อหน่วยงาน</td>
\t\t\t\t  <td>ชั้น</td>
\t\t\t\t  <td>ผู้รับงาน</td>
\t\t\t\t  <td>รหัสพนักงาน</td>
\t\t\t\t  <td>เบอร์โทร</td>
\t\t\t\t  <td>ตำแหน่ง</td>
\t\t\t\t  <td>หมายเหตุ</td>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t  {% for d in contactdata %}
\t\t\t\t<tr>
\t\t\t\t  <td scope=\"row\"><button type=\"button\" class=\"btn btn-link\" onclick=\"getemp('{{d.emp_code}}')\">เลือก</button></td>
\t\t\t\t  <td>{{d.department_code}}:{{d.department_name}}</td>
\t\t\t\t  <td>{{d.floor}}</td>
\t\t\t\t  <td>{{d.mr_contact_name}}</td>
\t\t\t\t  <td>{{d.emp_code}}</td>
\t\t\t\t  <td>{{d.emp_tel}}</td>
\t\t\t\t  <td>{{d.emp_tel}}</td>
\t\t\t\t  <td>{{d.remark }}</td>
\t\t\t\t</tr>
\t\t\t\t{% endfor %}
\t\t\t  </tbody>
\t\t\t</table>
\t\t</div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>


<div id=\"bg_loader\" class=\"card\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "branch/create_work.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\create_work.tpl");
    }
}

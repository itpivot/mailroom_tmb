<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/send_list.tpl */
class __TwigTemplate_4ac78413746684e3e67c8f3bd953ad17 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg2' => [$this, 'block_menu_msg2'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/send_list.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PivotSend List ";
    }

    // line 5
    public function block_menu_msg2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 9
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "\t .card {
       margin: 8px 0px;
\t   
    }

\t
    .space-height {
        padding: 10px 15px;
        line-height: 100%;
\t\t
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: -webkit-sticky;
\t\tposition: sticky;
\t\tbottom: 0;
\t\tz-index: 1075;

\t}
\t
\t.wraps {
\t\tdisplay:block;
\t\tword-break: break-all;
\t\twidth: 30px;
\t}

\t
";
    }

    // line 76
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 77
        echo "    loadSendMailroom();

    \$('#txt_search').on('keyup', function() {
\t\t\$('#img_loading').show();
\t\tloadSendMailroom();
\t\t
    });
\t
\t
\t


";
    }

    // line 91
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        echo "
\t function goPageSend()
    {
\t\twindow.location.href = 'receive_list.php'; 
\t}
\t
\t
    function loadSendMailroom() {
\t\tvar txt_search = \$('#txt_search').val();
        var str = '';
        \$.ajax({
            url: '../messenger/ajax/ajax_get_sendMailroom.php',
            type: 'POST',
\t\t\tdataType: 'json',
\t\t\tdata: {
                txt: txt_search
            },
            cache: false,
            beforeSend: function() {
                \$('#img_loading').show();
            },
            success: function(res) {
\t\t\t\tdocument.getElementById(\"badge-show\").textContent=res.length;
\t\t\t\tif( res != \"\" ){
\t\t\t\t\tfor(var i = 0; i < res.length; i++) {\t
\t\t\t\t\t\tif( res[i]['mr_status_send'] == 2 ){
\t\t\t\t\t\t\tstr += '<div class=\"card bg-warning\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<label class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<input onclick=\"ch_change();\" name=\"ch_bog[]\" value=\"'+res[i]['mr_work_main_id']+'\" type=\"checkbox\" class=\"custom-control-input\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-indicator\"></span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-description\">เลือก</span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t</label>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
\t\t\t\t\t\t\t\t\tif( res[i]['tel_re'] ){
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}else{\t
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'      <a href=\"edit_floor_send.php?id='+res[i]['mr_work_main_id']+'\" ><b>แก้ไขชั้น</b></a></p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-5 text-left btn-zone\"></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-4 text-center btn-zone\"><a href=\"agent_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง - รับแทน</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-3 text-right btn-zone\"><a href=\"confirm_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else if( res[i]['mr_status_id'] == 5 ){
\t\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<label class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<input onclick=\"ch_change();\" name=\"ch_bog[]\" value=\"'+res[i]['mr_work_main_id']+'\" type=\"checkbox\" class=\"custom-control-input\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-indicator\"></span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-description\">เลือก</span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t</label>';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
\t\t\t\t\t\t\t\t\tif( res[i]['tel_re'] ){
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}else{\t
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>สถานะ : Success </b></p>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<label class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<input onclick=\"ch_change();\" name=\"ch_bog[]\" value=\"'+res[i]['mr_work_main_id']+'\" type=\"checkbox\" class=\"custom-control-input\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-indicator\"></span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-description\">เลือก</span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t</label>';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
\t\t\t\t\t\t\t\t\tif( res[i]['tel_re'] ){
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}else{\t
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'     <a href=\"edit_floor_send.php?id='+res[i]['mr_work_main_id']+'\" ><b>แก้ไขชั้น</b></a></p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-5 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoSendById('+res[i]['work_main_id']+');\"><b>ไม่เจอผู้รับ</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-4 text-right btn-zone\"><a href=\"agent_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง - รับแทน</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-3 text-right btn-zone\"><a href=\"confirm_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}else{
\t\t\t\t\t\$('#btn_save_all').hide();
\t\t\t\t\tstr = '<center>ไม่มีเอกสารที่ต้องส่ง !</center>';
\t\t\t\t\t 
\t\t\t\t}
\t\t\t\t\$('#data_list').html(str);
                
\t\t\t\t
                
            },
            complete: function() {
                \$('#img_loading').hide();
            }
        });
    }


\tfunction updateNoSendById(id) {
        \$.ajax({
\t\t\turl: '../messenger/ajax/ajax_updateNoSendById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == \"success\") {
                    location.reload();
                }
            }
        })
    }

function save_click_all(type) {
\t\t\t\tvar dataall = [];
\t\t\t\t \$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t dataall.push(this.value);
\t\t\t\t\t 
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\tif(type == 1 ){
\t\t\t\t\twindow.location.href ='confirm_receive.php?id='+newdataall+'';
\t\t\t\t}else{
\t\t\t\t\twindow.location.href ='agent_receive.php?id='+newdataall+'';
\t\t\t\t}
\t\t\t\t//console.log(newdataall)
\t\t\t}

function ch_change() {
\t\t\t\tvar dataall = [];
\t\t\t\t\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t dataall.push(this.value);
\t\t\t\t\t 
\t\t\t\t  });
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t\$('#btn_all').hide()  ;
\t\t\t\t  }else{
\t\t\t\t\t\$('#btn_all').show()  ;
\t\t\t\t  }
}
";
    }

    // line 273
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 274
        echo "    <div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหา\">
            </div>
        </form>
    </div>
    <div class=\"text-center\">
        <label>จำนวนเอกสาร</label>
        
            <span id=\"badge-show\" class=\"badge badge-secondary badge-pill badge-dark\"></span>

         <label>ฉบับ</label>
    </div>
    <div id=\"btn_all\" style=\"display:none;\">
\t<button onclick=\"save_click_all(1);\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">ส่ง</button>
\t<button onclick=\"save_click_all(2);\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">ส่ง-รับแทน</button>
\t</div>
\t<div id=\"data_list\">
\t</div>
\t<br>
\t<br>
\t<div class=\"fixed-bottom text-right\">
\t\t<button onclick=\"goPageSend();\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">รับเอกสาร</button>
\t</div>
\t
";
    }

    // line 305
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 307
        if ((($context["debug"] ?? null) != "")) {
            // line 308
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 310
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/send_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  396 => 310,  390 => 308,  388 => 307,  381 => 305,  349 => 274,  345 => 273,  162 => 92,  158 => 91,  142 => 77,  138 => 76,  70 => 10,  66 => 9,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg2 %} active {% endblock %}



{% block styleReady %}
\t .card {
       margin: 8px 0px;
\t   
    }

\t
    .space-height {
        padding: 10px 15px;
        line-height: 100%;
\t\t
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: -webkit-sticky;
\t\tposition: sticky;
\t\tbottom: 0;
\t\tz-index: 1075;

\t}
\t
\t.wraps {
\t\tdisplay:block;
\t\tword-break: break-all;
\t\twidth: 30px;
\t}

\t
{% endblock %}

{% block domReady %}
    loadSendMailroom();

    \$('#txt_search').on('keyup', function() {
\t\t\$('#img_loading').show();
\t\tloadSendMailroom();
\t\t
    });
\t
\t
\t


{% endblock %}

{% block javaScript %}

\t function goPageSend()
    {
\t\twindow.location.href = 'receive_list.php'; 
\t}
\t
\t
    function loadSendMailroom() {
\t\tvar txt_search = \$('#txt_search').val();
        var str = '';
        \$.ajax({
            url: '../messenger/ajax/ajax_get_sendMailroom.php',
            type: 'POST',
\t\t\tdataType: 'json',
\t\t\tdata: {
                txt: txt_search
            },
            cache: false,
            beforeSend: function() {
                \$('#img_loading').show();
            },
            success: function(res) {
\t\t\t\tdocument.getElementById(\"badge-show\").textContent=res.length;
\t\t\t\tif( res != \"\" ){
\t\t\t\t\tfor(var i = 0; i < res.length; i++) {\t
\t\t\t\t\t\tif( res[i]['mr_status_send'] == 2 ){
\t\t\t\t\t\t\tstr += '<div class=\"card bg-warning\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<label class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<input onclick=\"ch_change();\" name=\"ch_bog[]\" value=\"'+res[i]['mr_work_main_id']+'\" type=\"checkbox\" class=\"custom-control-input\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-indicator\"></span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-description\">เลือก</span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t</label>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
\t\t\t\t\t\t\t\t\tif( res[i]['tel_re'] ){
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}else{\t
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'      <a href=\"edit_floor_send.php?id='+res[i]['mr_work_main_id']+'\" ><b>แก้ไขชั้น</b></a></p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-5 text-left btn-zone\"></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-4 text-center btn-zone\"><a href=\"agent_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง - รับแทน</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-3 text-right btn-zone\"><a href=\"confirm_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else if( res[i]['mr_status_id'] == 5 ){
\t\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<label class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<input onclick=\"ch_change();\" name=\"ch_bog[]\" value=\"'+res[i]['mr_work_main_id']+'\" type=\"checkbox\" class=\"custom-control-input\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-indicator\"></span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-description\">เลือก</span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t</label>';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
\t\t\t\t\t\t\t\t\tif( res[i]['tel_re'] ){
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}else{\t
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>สถานะ : Success </b></p>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\tstr += '<div class=\"card\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<label class=\"custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<input onclick=\"ch_change();\" name=\"ch_bog[]\" value=\"'+res[i]['mr_work_main_id']+'\" type=\"checkbox\" class=\"custom-control-input\">';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-indicator\"></span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t\t<span class=\"custom-control-description\">เลือก</span>';
\t\t\t\t\t\t\t\t\tstr += '\t\t</label>';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
\t\t\t\t\t\t\t\t\tif( res[i]['tel_re'] ){
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}else{\t
\t\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mobile_re']+'\">'+res[i]['mobile_re']+' </a>/ <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'     <a href=\"edit_floor_send.php?id='+res[i]['mr_work_main_id']+'\" ><b>แก้ไขชั้น</b></a></p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-5 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoSendById('+res[i]['work_main_id']+');\"><b>ไม่เจอผู้รับ</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-4 text-right btn-zone\"><a href=\"agent_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง - รับแทน</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-3 text-right btn-zone\"><a href=\"confirm_receive.php?id='+res[i]['mr_work_main_id']+'\" class=\"btn btn-success\"><b>ส่ง</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}else{
\t\t\t\t\t\$('#btn_save_all').hide();
\t\t\t\t\tstr = '<center>ไม่มีเอกสารที่ต้องส่ง !</center>';
\t\t\t\t\t 
\t\t\t\t}
\t\t\t\t\$('#data_list').html(str);
                
\t\t\t\t
                
            },
            complete: function() {
                \$('#img_loading').hide();
            }
        });
    }


\tfunction updateNoSendById(id) {
        \$.ajax({
\t\t\turl: '../messenger/ajax/ajax_updateNoSendById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == \"success\") {
                    location.reload();
                }
            }
        })
    }

function save_click_all(type) {
\t\t\t\tvar dataall = [];
\t\t\t\t \$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t dataall.push(this.value);
\t\t\t\t\t 
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\tif(type == 1 ){
\t\t\t\t\twindow.location.href ='confirm_receive.php?id='+newdataall+'';
\t\t\t\t}else{
\t\t\t\t\twindow.location.href ='agent_receive.php?id='+newdataall+'';
\t\t\t\t}
\t\t\t\t//console.log(newdataall)
\t\t\t}

function ch_change() {
\t\t\t\tvar dataall = [];
\t\t\t\t\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t dataall.push(this.value);
\t\t\t\t\t 
\t\t\t\t  });
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t\$('#btn_all').hide()  ;
\t\t\t\t  }else{
\t\t\t\t\t\$('#btn_all').show()  ;
\t\t\t\t  }
}
{% endblock %}

{% block Content %}
    <div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหา\">
            </div>
        </form>
    </div>
    <div class=\"text-center\">
        <label>จำนวนเอกสาร</label>
        
            <span id=\"badge-show\" class=\"badge badge-secondary badge-pill badge-dark\"></span>

         <label>ฉบับ</label>
    </div>
    <div id=\"btn_all\" style=\"display:none;\">
\t<button onclick=\"save_click_all(1);\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">ส่ง</button>
\t<button onclick=\"save_click_all(2);\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">ส่ง-รับแทน</button>
\t</div>
\t<div id=\"data_list\">
\t</div>
\t<br>
\t<br>
\t<div class=\"fixed-bottom text-right\">
\t\t<button onclick=\"goPageSend();\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">รับเอกสาร</button>
\t</div>
\t
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "messenger/send_list.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\messenger\\send_list.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/send_work_all.tpl */
class __TwigTemplate_3e19fcb407a6dc60cb2952a71832629d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_4' => [$this, 'block_menu_4'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/send_work_all.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 3
    public function block_menu_4($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 5
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 15
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "#btn_save:hover{
\tcolor: #FFFFFF;
\tbackground-color: #055d97;

}

#btn_save{
\tborder-color: #0074c0;
\tcolor: #FFFFFF;
\tbackground-color: #0074c0;
}

#detail_sender_head h4{
\ttext-align:left;
\tcolor: #006cb7;
\tborder-bottom: 3px solid #006cb7;
\tdisplay: inline;
}

#detail_sender_head {
\tborder-bottom: 3px solid #eee;
\tmargin-bottom: 20px;
\tmargin-top: 20px;
}
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
";
    }

    // line 56
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 57
        echo twig_escape_filter($this->env, ($context["alertt"] ?? null), "html", null, true);
        echo "
var tbl_data = \$('#tableData').DataTable({ 
    \"responsive\": true,
\t\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'send_work_no'},
        {'data': 'send_name'},
        {'data': 'res_depart'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
\t\t{'data': 'mr_status_name'},
        {'data': 'action'}
    ]
});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
loaddata();
\$('#history_print').hide();
\$('#toggle-sliding').click(function(){
    \$('#history_print').slideToggle('slow');
\t\$.ajax({
\t  method: 'POST',
\t  dataType: 'json',
\t  url: \"ajax/ajax_load_history_print.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#history_print').html(msg.data);
\t\t\t\$('#bg_loader').hide();
\t\t}else{
\t\t\talertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\treturn;
\t\t}
\t});
});
\t\t
";
    }

    // line 107
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 108
        echo "function save_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t dataall.push(this.value);
\t\t 
      });
\t  //console.log(tel_receiver);
\t  if(tel_receiver == '' || tel_receiver == null){
\t\t\t setTimeout(function(){ 
\t\t\t\t\$('#tel_receiver').focus();
\t\t\t}, 3000);
\t\t alertify.alert(\"ข้อผิดพลาด\",\"กรุณาระบุเบอร์โทร\",function() {
\t\t\t setTimeout(function(){ 
\t\t\t\t\t\$('#tel_receiver').focus();
\t\t\t\t}, 2000);
\t\t }); 
\t\treturn;
\t  }else if(dataall.length < 1){
\t\t alertify.alert(\"ข้อผิดพลาด\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\tvar newdataall = dataall.join(\",\");
\t//console.log(newdataall);
\t//console.log(tel_receiver);
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_send_work_all.php\",
\t  data: { \t
\t\t\tall_id: newdataall, 
\t\t\ttel: tel_receiver 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\tif(msg['st'] == 'success'){
\t\t\tloaddata();
\t\t\twindow.open('print_send_work_all.php?id='+msg['re']+'');
\t\t\t//\$('#hid_print').html('<a href=\"print_send_work_all.php?id='+msg['re']+'\" target=\"_blank\">พิมพ์รายการล่าสุด</a>');
\t\t\t
\t\t}else{
\t\t\talertify.alert(\"ข้อผิดพลาด\",\"เกิดข้อผิดพลาด  \"+msg['re']+\"  กรุณาลองใหม่\" ); 
\t\t}
\t\t\t\$('#bg_loader').hide();
\t});
}
function print_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t dataall.push(this.value);
\t\t 
      });
\t  //console.log(tel_receiver);
\t  if(dataall.length < 1){
\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\tvar newdataall = dataall.join(\",\");
\t\twindow.open('printcoverpage.php?maim_id='+newdataall+'');
}\t\t
\t
function loaddata() {
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_load_send_work_all.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tableData').DataTable().clear().draw();
\t\t\t\$('#tableData').DataTable().rows.add(msg.data).draw();
\t\t}else{
\t\t\talertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\treturn;
\t\t}
\t});
}
";
    }

    // line 201
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 202
        echo "
\t
\t\t\t<div class=\"container-fluid\">
\t\t\t
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>รวมรายการเอกสารนำส่ง</b></h3></label>
\t\t\t</div>\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4>รายการเอกสารนำส่งทั้งหมด </H4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group row\">
\t\t\t\t<label for=\"tel_receiver\" class=\"col-sm-4 col-md-3 col-lg-2 col-form-label\">ระบุเบอร์โทรติดต่อผู้ส่ง :</label>
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t   <input value=\"";
        // line 216
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_mobile", [], "any", false, false, false, 216) == "")) {
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_tel", [], "any", false, false, false, 216), "html", null, true);
            echo " ";
        } else {
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_mobile", [], "any", false, false, false, 216), "html", null, true);
        }
        echo "\" type=\"text\" class=\"form-control\" id=\"tel_receiver\" placeholder=\"ระบุเบอร์โทรติดต่อ\">
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t\t\t\t
\t\t\t
\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tableData\">
\t\t\t\t\t<thead class=\"\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>ลำดับ</th>
\t\t\t\t\t\t<th>เลขที่เอกสาร</th>
\t\t\t\t\t\t<th>ชื่อผู้ส่ง</th>
\t\t\t\t\t\t<th>หน่วยงานรผู้รับ</th>
\t\t\t\t\t\t<th>ชื่อผู้รับ</th>
\t\t\t\t\t\t<th>วันที่สร้างรายการ</th>
\t\t\t\t\t\t<th>Status ล่าสุด</th>
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือก</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
<div id=\"hid_print\">\t
";
        // line 248
        echo "\t<button id=\"toggle-sliding\" target=\"_blank\"  class=\"btn btn-link\">ประวัติใบคุม</button>
</div>\t\t
\t<div id=\"history_print\" class=\"card\">
\t  
\t</div>\t
\t\t<div class=\"form-group\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 text-center\" style=\"padding-left:0px\">
\t\t\t\t\t<button onclick=\"save_click();\" type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_save\"  >บันทึกรายการและพิมพ์ใบคุม</button>
\t\t\t\t\t<button onclick=\"print_click();\" type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_print\"  >พิมพ์ใบปะหน้าซอง</button>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>
\t<br/>
\t<br/>
\t<br/>
\t<br/>
\t<br/>
\t</div>
\t\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
<div id=\"bog_link\">
</div>\t

";
    }

    // line 277
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
\t";
        // line 278
        if ((($context["debug"] ?? null) != "")) {
            // line 279
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 281
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/send_work_all.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  387 => 281,  381 => 279,  379 => 278,  373 => 277,  343 => 248,  304 => 216,  288 => 202,  284 => 201,  188 => 108,  184 => 107,  130 => 57,  126 => 56,  85 => 16,  81 => 15,  71 => 6,  67 => 5,  60 => 3,  53 => 2,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}
{% block title %}Pivot- List{% endblock %}
{% block menu_4 %} active {% endblock %}

{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}


{% block styleReady %}
#btn_save:hover{
\tcolor: #FFFFFF;
\tbackground-color: #055d97;

}

#btn_save{
\tborder-color: #0074c0;
\tcolor: #FFFFFF;
\tbackground-color: #0074c0;
}

#detail_sender_head h4{
\ttext-align:left;
\tcolor: #006cb7;
\tborder-bottom: 3px solid #006cb7;
\tdisplay: inline;
}

#detail_sender_head {
\tborder-bottom: 3px solid #eee;
\tmargin-bottom: 20px;
\tmargin-top: 20px;
}
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
{% endblock %}


{% block domReady %}
{{alertt}}
var tbl_data = \$('#tableData').DataTable({ 
    \"responsive\": true,
\t\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'send_work_no'},
        {'data': 'send_name'},
        {'data': 'res_depart'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
\t\t{'data': 'mr_status_name'},
        {'data': 'action'}
    ]
});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
loaddata();
\$('#history_print').hide();
\$('#toggle-sliding').click(function(){
    \$('#history_print').slideToggle('slow');
\t\$.ajax({
\t  method: 'POST',
\t  dataType: 'json',
\t  url: \"ajax/ajax_load_history_print.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#history_print').html(msg.data);
\t\t\t\$('#bg_loader').hide();
\t\t}else{
\t\t\talertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\treturn;
\t\t}
\t});
});
\t\t
{% endblock %}
{% block javaScript %}
function save_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t dataall.push(this.value);
\t\t 
      });
\t  //console.log(tel_receiver);
\t  if(tel_receiver == '' || tel_receiver == null){
\t\t\t setTimeout(function(){ 
\t\t\t\t\$('#tel_receiver').focus();
\t\t\t}, 3000);
\t\t alertify.alert(\"ข้อผิดพลาด\",\"กรุณาระบุเบอร์โทร\",function() {
\t\t\t setTimeout(function(){ 
\t\t\t\t\t\$('#tel_receiver').focus();
\t\t\t\t}, 2000);
\t\t }); 
\t\treturn;
\t  }else if(dataall.length < 1){
\t\t alertify.alert(\"ข้อผิดพลาด\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\tvar newdataall = dataall.join(\",\");
\t//console.log(newdataall);
\t//console.log(tel_receiver);
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_send_work_all.php\",
\t  data: { \t
\t\t\tall_id: newdataall, 
\t\t\ttel: tel_receiver 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\tif(msg['st'] == 'success'){
\t\t\tloaddata();
\t\t\twindow.open('print_send_work_all.php?id='+msg['re']+'');
\t\t\t//\$('#hid_print').html('<a href=\"print_send_work_all.php?id='+msg['re']+'\" target=\"_blank\">พิมพ์รายการล่าสุด</a>');
\t\t\t
\t\t}else{
\t\t\talertify.alert(\"ข้อผิดพลาด\",\"เกิดข้อผิดพลาด  \"+msg['re']+\"  กรุณาลองใหม่\" ); 
\t\t}
\t\t\t\$('#bg_loader').hide();
\t});
}
function print_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t dataall.push(this.value);
\t\t 
      });
\t  //console.log(tel_receiver);
\t  if(dataall.length < 1){
\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\tvar newdataall = dataall.join(\",\");
\t\twindow.open('printcoverpage.php?maim_id='+newdataall+'');
}\t\t
\t
function loaddata() {
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_load_send_work_all.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t}).done(function( msg ) {
\t\tif(msg.status == 200){
\t\t\t\$('#bg_loader').hide();
\t\t\t\$('#tableData').DataTable().clear().draw();
\t\t\t\$('#tableData').DataTable().rows.add(msg.data).draw();
\t\t}else{
\t\t\talertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\treturn;
\t\t}
\t});
}
{% endblock %}\t
{% block Content %}

\t
\t\t\t<div class=\"container-fluid\">
\t\t\t
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>รวมรายการเอกสารนำส่ง</b></h3></label>
\t\t\t</div>\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4>รายการเอกสารนำส่งทั้งหมด </H4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group row\">
\t\t\t\t<label for=\"tel_receiver\" class=\"col-sm-4 col-md-3 col-lg-2 col-form-label\">ระบุเบอร์โทรติดต่อผู้ส่ง :</label>
\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t   <input value=\"{% if user_data.mr_emp_mobile == '' %}{{user_data.mr_emp_tel}} {% else %}{{user_data.mr_emp_mobile}}{% endif %}\" type=\"text\" class=\"form-control\" id=\"tel_receiver\" placeholder=\"ระบุเบอร์โทรติดต่อ\">
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t\t\t\t
\t\t\t
\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tableData\">
\t\t\t\t\t<thead class=\"\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>ลำดับ</th>
\t\t\t\t\t\t<th>เลขที่เอกสาร</th>
\t\t\t\t\t\t<th>ชื่อผู้ส่ง</th>
\t\t\t\t\t\t<th>หน่วยงานรผู้รับ</th>
\t\t\t\t\t\t<th>ชื่อผู้รับ</th>
\t\t\t\t\t\t<th>วันที่สร้างรายการ</th>
\t\t\t\t\t\t<th>Status ล่าสุด</th>
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือก</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
<div id=\"hid_print\">\t
{# <a href=\"print_send_work_all.php?id={{id}}\" target=\"_blank\">พิมพ์รายการล่าสุด</a> #}
\t<button id=\"toggle-sliding\" target=\"_blank\"  class=\"btn btn-link\">ประวัติใบคุม</button>
</div>\t\t
\t<div id=\"history_print\" class=\"card\">
\t  
\t</div>\t
\t\t<div class=\"form-group\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 text-center\" style=\"padding-left:0px\">
\t\t\t\t\t<button onclick=\"save_click();\" type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_save\"  >บันทึกรายการและพิมพ์ใบคุม</button>
\t\t\t\t\t<button onclick=\"print_click();\" type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_print\"  >พิมพ์ใบปะหน้าซอง</button>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</div>
\t<br/>
\t<br/>
\t<br/>
\t<br/>
\t<br/>
\t</div>
\t\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
<div id=\"bog_link\">
</div>\t

{% endblock %}


{% block debug %} 
\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "branch/send_work_all.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\send_work_all.tpl");
    }
}

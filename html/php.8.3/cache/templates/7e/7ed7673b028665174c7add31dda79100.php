<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/receive_work.tpl */
class __TwigTemplate_2f1b89f9eda92506a58a228c786ac185 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_6' => [$this, 'block_menu_6'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/receive_work.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 3
    public function block_menu_6($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 5
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 15
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "th, td{
\twhite-space: nowrap;
}
#btn_save:hover{
\tcolor: #FFFFFF;
\tbackground-color: #055d97;

}

#btn_save{
\tborder-color: #0074c0;
\tcolor: #FFFFFF;
\tbackground-color: #0074c0;
}

#detail_sender_head h4{
\ttext-align:left;
\tcolor: #006cb7;
\tborder-bottom: 3px solid #006cb7;
\tdisplay: inline;
}

#detail_sender_head {
\tborder-bottom: 3px solid #eee;
\tmargin-bottom: 20px;
\tmargin-top: 20px;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tleft:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
";
    }

    // line 60
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 61
        echo twig_escape_filter($this->env, ($context["alertt"] ?? null), "html", null, true);
        echo "
\$('#barcode').keydown(function (e){
    if(e.keyCode == 13){
        save_click();
    }
})
var tbl_data = \$('#tableData').DataTable({ 
    \"responsive\": true,
\t//\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'btn'},
        {'data': 'send_work_no'},
        {'data': 'tnt_tracking_no'},
        {'data': 'send_name'},
        {'data': 'mr_status_name'},
        {'data': 'mr_type_work_name'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
\t\t{'data': 'update_date'},
        {'data': 'action'}
    ]
});


\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});

\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
loaddata();
";
    }

    // line 103
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 104
        echo "function print_click() {
\tvar dataall = [];
\t
\tvar tbl_data = \$('#tableData').DataTable();
\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t dataall.push(this.value);
\t\t 
\t  });
\t  //console.log(tel_receiver);
\t  if(dataall.length < 1){
\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\tvar newdataall = dataall.join(\",\");
\talertify.confirm(\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง \"+(dataall.length)+\"  รายการ \", function() {
\t\t\t//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t//return;
\t\t\t\$.ajax({
\t\t\t\tdataType: \"json\",
\t\t\t\tmethod: \"POST\",
\t\t\t\turl: \"ajax/ajax_save_receive_multiple.php\",
\t\t\t\tdata: { 
\t\t\t\t\t\tmaim_id: newdataall
\t\t\t\t  },
\t\t\t\t beforeSend: function( xhr ) {
\t\t\t\t\t\t\t  \$('#bg_loader').show();
\t\t\t\t\t\t\t  
\t\t\t\t  },
\t\t\t\t  error: function(xhr, error){
\t\t\t\t\t  alert('เกิดข้อผิดพลาด !');
\t\t\t\t\t  location.reload();
\t\t\t   }
\t\t\t  }).done(function( msg ) {
\t\t\t\t  if(msg['st'] == 'success'){
\t\t\t\t\tlocation.href = \"rate_send.php?id=\"+newdataall;
\t\t\t\t  }else{
\t\t\t\t\t  alertify.alert(msg['st'],msg['msg']); 
\t\t\t\t  }
\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t});

\t}).setHeader('<h5> ยืนยันการรับเอกสาร  </h5> ');
}\t



function click_resave(barcode) {
\t//console.log(bc);
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\t//var barcode = \$('#barcode').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t// tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t// dataall.push(this.value);
\t\t 
    //  });
\t  //console.log(tel_receiver);
\t  if(barcode == '' ){
\t\t alertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\t//var newdataall = dataall.join(\",\");
\t//console.log(newdataall);
\t//console.log(tel_receiver);
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_receive.php\",
\t  data: { 
\t\t\t\tbarcode: barcode, 
\t\t\t\ttel: tel_receiver 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\tlocation.reload();
\t }
\t}).done(function( msg ) {
\t\tif(msg['st'] == 'success'){
\t\t\t//loaddata();
\t\t\t//alertify.alert(msg['st'],msg['msg']);
\t\t\tlocation.href = \"rate_send.php?id=\"+msg['mr_work_main_id'];
\t\t}else{
\t\t\talertify.alert(msg['st'],msg['msg']); 
\t\t}
\t\t  \$('#bg_loader').hide();
\t  });
\t
}
function save_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar barcode = \$('#barcode').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t// tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t// dataall.push(this.value);
\t\t 
    //  });
\t  //console.log(tel_receiver);
\t  if(barcode == '' ){
\t\t alertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\t//var newdataall = dataall.join(\",\");
\t//console.log(newdataall);
\t//console.log(tel_receiver);
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_receive.php\",
\t  data: { 
\t\t\t\tbarcode: barcode, 
\t\t\t\ttel: tel_receiver 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\tlocation.reload();
\t }
\t}).done(function( msg ) {
\t\tif(msg['st'] == 'success'){
\t\t\t\$('#barcode').val('');
\t\t\tloaddata();

\t\t\t\$( \"#succ\" ).show();
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#succ\" ).hide();
\t\t}, 300);
\t\t\t
\t\t\t//lertify.alert(msg['st'],msg['msg']);
\t\t}else{
\t\t\t\$( \"#err\" ).show();
\t\t\tsetTimeout(function(){ 
\t\t\t\t\$( \"#err\" ).hide();
\t\t\t}, 3000);
\t\t}
\t\t  \$('#bg_loader').hide();
\t  });
}
function onseart() {
 var barcode = \$('#barcode').val();
 var ch_b = \$('#b_'+barcode).val();
 if(ch_b){
\t\$('#b_'+barcode).prop(\"checked\", true);
\t\t\$( \"#succ\" ).show();
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#succ\" ).hide();
\t\t}, 3000);
 }else{
\t\t\$( \"#err\" ).show();
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#err\" ).hide();
\t\t}, 3000);
 }

}
function loaddata() {
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_load_receive_work.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\t//location.reload();
\t }
\t}).done(function( msg ) {
\t\t  \$('#bg_loader').hide();
\t\t  \$('#tableData').DataTable().clear().draw();
\t\t  \$('#tableData').DataTable().rows.add(msg).draw();
\t  });
}
";
    }

    // line 295
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 296
        echo "
\t
\t\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>รายการเอกสาร</b></h3></label>
\t\t\t</div>\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4> รับเอกสาร </H4>
\t\t\t</div>
\t\t\t<div class=\"form-row align-items-center\">
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t  <input type=\"text\" class=\"form-control mb-2\" id=\"barcode\" placeholder=\"Barcode\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t";
        // line 312
        echo "<button onclick=\"save_click();\" type=\"button\" class=\"btn btn-primary mb-2\">บันทึกรับ</button>
\t\t\t\t  
\t\t\t\t</div>
\t\t\t\t<div id=\"err\" class=\"col-md-12 alert alert-danger\" style=\"display:none;\"role=\"alert\">
\t\t\t\t ไม่พบข้อมูลงานที่กำลังนำส่ง
\t\t\t\t</div>
\t\t\t\t<div id=\"succ\"class=\"col-md-12 alert alert-success \" style=\"display:none;\" role=\"alert\">
\t\t\t\t ok!!
\t\t\t\t</div>
\t\t\t </div>

\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tableData\">
\t\t\t\t\t<thead class=\"\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>ลำดับ</th>
\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t<th>เลขที่เอกสาร</th>
\t\t\t\t\t\t<th>Tracking No.</th>
\t\t\t\t\t\t<th>ชื่อผู้ส่ง</th>
\t\t\t\t\t\t<th>สถานะงาน</th>
\t\t\t\t\t\t<th>ประเภทงาน</th>
\t\t\t\t\t\t<th>ชื่อผู้รับ</th>
\t\t\t\t\t\t<th>วันที่สร้างรายการ</th>
\t\t\t\t\t\t<th>วันที่แก้ไขล่าสุด</th>
\t\t\t\t\t\t<th>
\t\t\t\t\t\t<div class=\"btn-group mr-2\" role=\"group\" aria-label=\"First group\">
\t\t\t\t\t\t\t<button onclick=\"\$('#select-all').click();\" type=\"button\" class=\"btn btn-secondary\">
\t\t\t\t\t\t\t<input id=\"select-all\" type=\"checkbox\">
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" onclick=\"print_click();\">รับเอกสารที่เลือก</button>
\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t</div>
\t\t
\t\t<div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
\t\t<div id=\"bog_link\">
\t\t</div>\t\t
";
    }

    // line 386
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 388
        if ((($context["debug"] ?? null) != "")) {
            // line 389
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 391
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/receive_work.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  488 => 391,  482 => 389,  480 => 388,  473 => 386,  398 => 312,  382 => 296,  378 => 295,  184 => 104,  180 => 103,  134 => 61,  130 => 60,  85 => 16,  81 => 15,  71 => 6,  67 => 5,  60 => 3,  53 => 2,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}
{% block title %}Pivot- List{% endblock %}
{% block menu_6 %} active {% endblock %}

{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}


{% block styleReady %}
th, td{
\twhite-space: nowrap;
}
#btn_save:hover{
\tcolor: #FFFFFF;
\tbackground-color: #055d97;

}

#btn_save{
\tborder-color: #0074c0;
\tcolor: #FFFFFF;
\tbackground-color: #0074c0;
}

#detail_sender_head h4{
\ttext-align:left;
\tcolor: #006cb7;
\tborder-bottom: 3px solid #006cb7;
\tdisplay: inline;
}

#detail_sender_head {
\tborder-bottom: 3px solid #eee;
\tmargin-bottom: 20px;
\tmargin-top: 20px;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tleft:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
{% endblock %}


{% block domReady %}
{{alertt}}
\$('#barcode').keydown(function (e){
    if(e.keyCode == 13){
        save_click();
    }
})
var tbl_data = \$('#tableData').DataTable({ 
    \"responsive\": true,
\t//\"searching\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'btn'},
        {'data': 'send_work_no'},
        {'data': 'tnt_tracking_no'},
        {'data': 'send_name'},
        {'data': 'mr_status_name'},
        {'data': 'mr_type_work_name'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
\t\t{'data': 'update_date'},
        {'data': 'action'}
    ]
});


\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});

\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
loaddata();
{% endblock %}
{% block javaScript %}
function print_click() {
\tvar dataall = [];
\t
\tvar tbl_data = \$('#tableData').DataTable();
\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t dataall.push(this.value);
\t\t 
\t  });
\t  //console.log(tel_receiver);
\t  if(dataall.length < 1){
\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\tvar newdataall = dataall.join(\",\");
\talertify.confirm(\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง \"+(dataall.length)+\"  รายการ \", function() {
\t\t\t//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t//return;
\t\t\t\$.ajax({
\t\t\t\tdataType: \"json\",
\t\t\t\tmethod: \"POST\",
\t\t\t\turl: \"ajax/ajax_save_receive_multiple.php\",
\t\t\t\tdata: { 
\t\t\t\t\t\tmaim_id: newdataall
\t\t\t\t  },
\t\t\t\t beforeSend: function( xhr ) {
\t\t\t\t\t\t\t  \$('#bg_loader').show();
\t\t\t\t\t\t\t  
\t\t\t\t  },
\t\t\t\t  error: function(xhr, error){
\t\t\t\t\t  alert('เกิดข้อผิดพลาด !');
\t\t\t\t\t  location.reload();
\t\t\t   }
\t\t\t  }).done(function( msg ) {
\t\t\t\t  if(msg['st'] == 'success'){
\t\t\t\t\tlocation.href = \"rate_send.php?id=\"+newdataall;
\t\t\t\t  }else{
\t\t\t\t\t  alertify.alert(msg['st'],msg['msg']); 
\t\t\t\t  }
\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t});

\t}).setHeader('<h5> ยืนยันการรับเอกสาร  </h5> ');
}\t



function click_resave(barcode) {
\t//console.log(bc);
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\t//var barcode = \$('#barcode').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t// tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t// dataall.push(this.value);
\t\t 
    //  });
\t  //console.log(tel_receiver);
\t  if(barcode == '' ){
\t\t alertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\t//var newdataall = dataall.join(\",\");
\t//console.log(newdataall);
\t//console.log(tel_receiver);
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_receive.php\",
\t  data: { 
\t\t\t\tbarcode: barcode, 
\t\t\t\ttel: tel_receiver 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\tlocation.reload();
\t }
\t}).done(function( msg ) {
\t\tif(msg['st'] == 'success'){
\t\t\t//loaddata();
\t\t\t//alertify.alert(msg['st'],msg['msg']);
\t\t\tlocation.href = \"rate_send.php?id=\"+msg['mr_work_main_id'];
\t\t}else{
\t\t\talertify.alert(msg['st'],msg['msg']); 
\t\t}
\t\t  \$('#bg_loader').hide();
\t  });
\t
}
function save_click() {
\tvar dataall = [];
\tvar tel_receiver = \$('#tel_receiver').val();
\tvar barcode = \$('#barcode').val();
\tvar tbl_data = \$('#tableData').DataTable();
\t// tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\t// dataall.push(this.value);
\t\t 
    //  });
\t  //console.log(tel_receiver);
\t  if(barcode == '' ){
\t\t alertify.alert(\"ข้อมูลไม่ครบถ้วน\",\"ท่านยังไม่เลือกงาน\"); 
\t\t return;
\t  }
\t // return;
\t//var newdataall = dataall.join(\",\");
\t//console.log(newdataall);
\t//console.log(tel_receiver);
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_receive.php\",
\t  data: { 
\t\t\t\tbarcode: barcode, 
\t\t\t\ttel: tel_receiver 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\tlocation.reload();
\t }
\t}).done(function( msg ) {
\t\tif(msg['st'] == 'success'){
\t\t\t\$('#barcode').val('');
\t\t\tloaddata();

\t\t\t\$( \"#succ\" ).show();
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#succ\" ).hide();
\t\t}, 300);
\t\t\t
\t\t\t//lertify.alert(msg['st'],msg['msg']);
\t\t}else{
\t\t\t\$( \"#err\" ).show();
\t\t\tsetTimeout(function(){ 
\t\t\t\t\$( \"#err\" ).hide();
\t\t\t}, 3000);
\t\t}
\t\t  \$('#bg_loader').hide();
\t  });
}
function onseart() {
 var barcode = \$('#barcode').val();
 var ch_b = \$('#b_'+barcode).val();
 if(ch_b){
\t\$('#b_'+barcode).prop(\"checked\", true);
\t\t\$( \"#succ\" ).show();
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#succ\" ).hide();
\t\t}, 3000);
 }else{
\t\t\$( \"#err\" ).show();
\t\tsetTimeout(function(){ 
\t\t\t\$( \"#err\" ).hide();
\t\t}, 3000);
 }

}
function loaddata() {
\t\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_load_receive_work.php\",
\t  data: { \tid: '',
\t\t\t\tstatus: '' 
\t\t},
\t   beforeSend: function( xhr ) {
\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t
\t\t},
\t\terror: function(xhr, error){
\t\t\talert('เกิดข้อผิดพลาด !');
\t\t\t//location.reload();
\t }
\t}).done(function( msg ) {
\t\t  \$('#bg_loader').hide();
\t\t  \$('#tableData').DataTable().clear().draw();
\t\t  \$('#tableData').DataTable().rows.add(msg).draw();
\t  });
}
{% endblock %}\t
{% block Content2 %}

\t
\t\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>รายการเอกสาร</b></h3></label>
\t\t\t</div>\t
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t <H4> รับเอกสาร </H4>
\t\t\t</div>
\t\t\t<div class=\"form-row align-items-center\">
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t  <input type=\"text\" class=\"form-control mb-2\" id=\"barcode\" placeholder=\"Barcode\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-auto\">
\t\t\t\t{#
\t\t\t\t  <button onclick=\"save_click();\" type=\"button\" class=\"btn btn-primary mb-2\">ค้นหา</button>
\t\t\t\t  #}<button onclick=\"save_click();\" type=\"button\" class=\"btn btn-primary mb-2\">บันทึกรับ</button>
\t\t\t\t  
\t\t\t\t</div>
\t\t\t\t<div id=\"err\" class=\"col-md-12 alert alert-danger\" style=\"display:none;\"role=\"alert\">
\t\t\t\t ไม่พบข้อมูลงานที่กำลังนำส่ง
\t\t\t\t</div>
\t\t\t\t<div id=\"succ\"class=\"col-md-12 alert alert-success \" style=\"display:none;\" role=\"alert\">
\t\t\t\t ok!!
\t\t\t\t</div>
\t\t\t </div>

\t\t\t<div class=\"table-responsive\">
\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tableData\">
\t\t\t\t\t<thead class=\"\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>ลำดับ</th>
\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t<th>เลขที่เอกสาร</th>
\t\t\t\t\t\t<th>Tracking No.</th>
\t\t\t\t\t\t<th>ชื่อผู้ส่ง</th>
\t\t\t\t\t\t<th>สถานะงาน</th>
\t\t\t\t\t\t<th>ประเภทงาน</th>
\t\t\t\t\t\t<th>ชื่อผู้รับ</th>
\t\t\t\t\t\t<th>วันที่สร้างรายการ</th>
\t\t\t\t\t\t<th>วันที่แก้ไขล่าสุด</th>
\t\t\t\t\t\t<th>
\t\t\t\t\t\t<div class=\"btn-group mr-2\" role=\"group\" aria-label=\"First group\">
\t\t\t\t\t\t\t<button onclick=\"\$('#select-all').click();\" type=\"button\" class=\"btn btn-secondary\">
\t\t\t\t\t\t\t<input id=\"select-all\" type=\"checkbox\">
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" onclick=\"print_click();\">รับเอกสารที่เลือก</button>
\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t\t<br/>
\t\t</div>
\t\t
\t\t<div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
\t\t<div id=\"bog_link\">
\t\t</div>\t\t
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/receive_work.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\receive_work.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/work_in.tpl */
class __TwigTemplate_e23007f132e9d3ffd844d5e5eafce326 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/work_in.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 8
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "

";
    }

    // line 14
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "
\t\tcheck_name_re();
\t\t

\t\t\$(\"#btn_save\").click(function(){
\t\t\t
\t\t\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\t\t\tvar name_receiver \t\t= \$(\"#name_receiver\").val();
\t\t\t\t\tvar pass_depart \t\t= \$(\"#pass_depart\").val();
\t\t\t\t\tvar floor \t\t\t\t= \$(\"#floor\").val();
\t\t\t\t\tvar depart \t\t\t\t= \$(\"#depart\").val();
\t\t\t\t\tvar remark \t\t\t\t= \$(\"#remark\").val();
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar floor_send \t\t\t= \$(\"#floor_send\").val();
\t\t\t\t\tvar floor_id \t\t\t= \$(\"#floor_id\").val();
\t\t\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic\").val();
\t\t\t\t\tvar qty \t\t\t\t= \$(\"#qty\").val();
\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(pass_emp == \"\" || pass_emp == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\tif(floor_id == \"\" || floor_id == null || floor_id == 0 ){
\t\t\t\t\t\t\tif(floor_send == 0 || floor_send == null){
\t\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชั้นของผู้รับ!');
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(floor_id);
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tpass_emp: pass_emp,
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tfloor_send: floor_send,
\t\t\t\t\t\t\t\tcsrf_token\t : \$('#csrf_token').val(),
\t\t\t\t\t\t\t\tfloor_id: floor_id,
\t\t\t\t\t\t\t\tqty: qty,
\t\t\t\t\t\t\t\ttopic: topic
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\talertify.confirm('ยืนยันการบันทึกข้อมูล',\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง\", function() {
\t\t\t\t\t\t\t\t\$.when(saveInOut(obj)).done(function(res) {
\t\t\t\t\t\t\t\t\tobj_d = JSON.parse(res);
\t\t\t\t\t\t\t\t\t//console.log(obj_d);
\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(obj_d['token']);
\t\t\t\t\t\t\t\t\tif(obj_d.status == 200) {
\t\t\t\t\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' +obj_d.barcodeok + '<b></font></h1>';
\t\t\t\t\t\t\t\t\t\talertify.alert('บันทึกสำเร็จ','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg, function() {
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t});


\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',obj_d['message'],
\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t);
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t});\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$('#name_receiver ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_depart ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#floor ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_emp ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#depart ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t
\t\t});
\t\t
\t\t\$(\"#pass_emp\").keyup(function(){
\t\t\tcheck_name_re();
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress2.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\tvar fullname = res['data']['mr_emp_name'] + \" \" + res['data']['mr_emp_lastname'];
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(fullname);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(\"\");
\t\t\t\t\t\t\t\$(\"#floor\").val(\"\");
\t\t\t\t\t\t\t\$(\"#floor_id\").val(\"\");
\t\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t}  else {
\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\$(\"#pass_depart\").val(\"\");
\t\t\t\t\t\t\$(\"#floor\").val(\"\");
\t\t\t\t\t\t\$(\"#floor_id\").val(\"\");
\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t}\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t\$(\"#name_receiver_select\").click(function(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#mr_emp_code\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t} 
\t\t\t\t\t} 
\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});

\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\turl: url+\"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});


\t\t
";
    }

    // line 204
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 205
        echo "var url = \"https://www.pivot-services.com/mailroom_tmb/employee/\";
url = \"\";
\t\tfunction check_name_re(){
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t if(pass_emp == \"\" || pass_emp == null){ 
\t\t\t\t\$(\"#div_re_text\").hide();
\t\t\t\t\$(\"#div_re_select\").show();
\t\t\t }else{
\t\t\t\t\$(\"#div_re_text\").show();
\t\t\t\t\$(\"#div_re_select\").hide();
\t\t\t }
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\t\turl: url+'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t\tfunction name_re_chang(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t}


\t\tvar saveInOut = function(obj) {
\t\t\t//return \$.post('https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_save_inout.php', obj);
\t\t\treturn \$.post(url+'ajax/ajax_save_inout.php', obj);
\t\t}
\t\t\t\t
";
    }

    // line 282
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 283
        echo "
\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4 >ข้อมูลผู้รับเอกสาร</h4></label>
\t\t\t</div>\t
\t\t\t
\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 290
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 291
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 291), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"floor_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"";
        // line 294
        echo twig_escape_filter($this->env, ($context["barcode"] ?? null), "html", null, true);
        echo "\">
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"div_re_select\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อผู้รับ <span style='color:red; font-size: 15px'>&#42;</span>:
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t ";
        // line 320
        echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน <span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_depart\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor\" placeholder=\"ชั้น\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart\" placeholder=\"ชื่อหน่วยงาน\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อเอกสาร<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"topic\" placeholder=\"ชื่อเรื่อง, หัวข้อเอกสาร\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tจำนวน<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"number\" value=\"1\" class=\"form-control form-control-sm\" id=\"qty\" placeholder=\"จำนวนเอกสาร 1,2,3,...\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้นของผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"0\" > เลือกชั้นปลายทาง</option>
\t\t\t\t\t\t\t";
        // line 401
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 402
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_floor_id", [], "any", false, false, false, 402), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "name", [], "any", false, false, false, 402), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 403
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<textarea class=\"form-control form-control-sm\" id=\"remark\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t</div>
\t\t\t";
        // line 415
        if ((($context["tomorrow"] ?? null) == 1)) {
            // line 416
            echo "\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-12\" style=\"color:red;\">
\t\t\t\t\t\t\t<center>เอกสารที่ส่งหลัง 16.00 น. จะทำการส่งในวันถัดไป </center>
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>\t
\t\t\t";
        }
        // line 424
        echo "\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\"  >Save</button>
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

";
    }

    // line 436
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 438
        if ((($context["debug"] ?? null) != "")) {
            // line 439
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 441
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/work_in.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  555 => 441,  549 => 439,  547 => 438,  540 => 436,  527 => 424,  517 => 416,  515 => 415,  501 => 403,  490 => 402,  486 => 401,  403 => 320,  378 => 294,  372 => 291,  368 => 290,  359 => 283,  355 => 282,  276 => 205,  272 => 204,  80 => 15,  76 => 14,  70 => 9,  66 => 8,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block scriptImport %}


{% endblock %}


{% block domReady %}

\t\tcheck_name_re();
\t\t

\t\t\$(\"#btn_save\").click(function(){
\t\t\t
\t\t\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\t\t\tvar name_receiver \t\t= \$(\"#name_receiver\").val();
\t\t\t\t\tvar pass_depart \t\t= \$(\"#pass_depart\").val();
\t\t\t\t\tvar floor \t\t\t\t= \$(\"#floor\").val();
\t\t\t\t\tvar depart \t\t\t\t= \$(\"#depart\").val();
\t\t\t\t\tvar remark \t\t\t\t= \$(\"#remark\").val();
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar floor_send \t\t\t= \$(\"#floor_send\").val();
\t\t\t\t\tvar floor_id \t\t\t= \$(\"#floor_id\").val();
\t\t\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic\").val();
\t\t\t\t\tvar qty \t\t\t\t= \$(\"#qty\").val();
\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(pass_emp == \"\" || pass_emp == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\tif(floor_id == \"\" || floor_id == null || floor_id == 0 ){
\t\t\t\t\t\t\tif(floor_send == 0 || floor_send == null){
\t\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชั้นของผู้รับ!');
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(floor_id);
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tpass_emp: pass_emp,
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tfloor_send: floor_send,
\t\t\t\t\t\t\t\tcsrf_token\t : \$('#csrf_token').val(),
\t\t\t\t\t\t\t\tfloor_id: floor_id,
\t\t\t\t\t\t\t\tqty: qty,
\t\t\t\t\t\t\t\ttopic: topic
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\talertify.confirm('ยืนยันการบันทึกข้อมูล',\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง\", function() {
\t\t\t\t\t\t\t\t\$.when(saveInOut(obj)).done(function(res) {
\t\t\t\t\t\t\t\t\tobj_d = JSON.parse(res);
\t\t\t\t\t\t\t\t\t//console.log(obj_d);
\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(obj_d['token']);
\t\t\t\t\t\t\t\t\tif(obj_d.status == 200) {
\t\t\t\t\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' +obj_d.barcodeok + '<b></font></h1>';
\t\t\t\t\t\t\t\t\t\talertify.alert('บันทึกสำเร็จ','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg, function() {
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t});


\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',obj_d['message'],
\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t);
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t});\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\$('#name_receiver ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_depart ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#floor ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#pass_emp ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#depart ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t
\t\t});
\t\t
\t\t\$(\"#pass_emp\").keyup(function(){
\t\t\tcheck_name_re();
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress2.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\tvar fullname = res['data']['mr_emp_name'] + \" \" + res['data']['mr_emp_lastname'];
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(fullname);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(\"\");
\t\t\t\t\t\t\t\$(\"#floor\").val(\"\");
\t\t\t\t\t\t\t\$(\"#floor_id\").val(\"\");
\t\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t}  else {
\t\t\t\t\t\t\$(\"#name_receiver\").val(\"\");
\t\t\t\t\t\t\$(\"#pass_depart\").val(\"\");
\t\t\t\t\t\t\$(\"#floor\").val(\"\");
\t\t\t\t\t\t\$(\"#floor_id\").val(\"\");
\t\t\t\t\t\t\$(\"#depart\").val(\"\");
\t\t\t\t\t\t\$(\"#emp_id\").val(\"\");
\t\t\t\t\t}\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});
\t\t
\t\t\$(\"#name_receiver_select\").click(function(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#mr_emp_code\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t} 
\t\t\t\t\t} 
\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t});

\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\turl: url+\"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});


\t\t
{% endblock %}\t\t\t\t
{% block javaScript %}
var url = \"https://www.pivot-services.com/mailroom_tmb/employee/\";
url = \"\";
\t\tfunction check_name_re(){
\t\t\tvar pass_emp = \$(\"#pass_emp\").val();
\t\t\t if(pass_emp == \"\" || pass_emp == null){ 
\t\t\t\t\$(\"#div_re_text\").hide();
\t\t\t\t\$(\"#div_re_select\").show();
\t\t\t }else{
\t\t\t\t\$(\"#div_re_text\").show();
\t\t\t\t\$(\"#div_re_select\").hide();
\t\t\t }
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\t\t\t\turl: url+'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t\tfunction name_re_chang(){
\t\t\tvar name_receiver_select = \$(\"#name_receiver_select\").val();
\t\t\t\$.ajax({
\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php\",
\t\t\t\turl: url+\"ajax/ajax_autocompress_name.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'name_receiver_select': name_receiver_select,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\$(\"#pass_emp\").val(res['data']['mr_emp_code']);
\t\t\t\t\t\t\t\$(\"#pass_depart\").val(res['data']['mr_department_code']);
\t\t\t\t\t\t\t\$(\"#floor\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\$(\"#floor_id\").val(res['data']['mr_floor_id']);
\t\t\t\t\t\t\t\$(\"#depart\").val(res['data']['mr_department_name']);
\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t});
\t\t}


\t\tvar saveInOut = function(obj) {
\t\t\t//return \$.post('https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_save_inout.php', obj);
\t\t\treturn \$.post(url+'ajax/ajax_save_inout.php', obj);
\t\t}
\t\t\t\t
{% endblock %}\t\t\t\t\t
\t\t\t\t
{% block Content %}

\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4 >ข้อมูลผู้รับเอกสาร</h4></label>
\t\t\t</div>\t
\t\t\t
\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\">
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"floor_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"{{ barcode }}\">
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"div_re_select\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อผู้รับ <span style='color:red; font-size: 15px'>&#42;</span>:
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t {# < option value = \"0\" > ค้นหาผู้รับ < /option>
\t\t\t\t\t\t\t {% for e in emp_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ e.mr_emp_id }}\" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t {% endfor %} #}
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน <span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_depart\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor\" placeholder=\"ชั้น\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart\" placeholder=\"ชื่อหน่วยงาน\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อเอกสาร<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"topic\" placeholder=\"ชื่อเรื่อง, หัวข้อเอกสาร\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tจำนวน<span style='color:red; font-size: 15px'>&#42;</span> :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"number\" value=\"1\" class=\"form-control form-control-sm\" id=\"qty\" placeholder=\"จำนวนเอกสาร 1,2,3,...\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้นของผู้รับ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"0\" > เลือกชั้นปลายทาง</option>
\t\t\t\t\t\t\t{% for s in floor_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_floor_id }}\" > {{ s.name }}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<textarea class=\"form-control form-control-sm\" id=\"remark\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t</div>
\t\t\t{% if tomorrow == 1 %}
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-12\" style=\"color:red;\">
\t\t\t\t\t\t\t<center>เอกสารที่ส่งหลัง 16.00 น. จะทำการส่งในวันถัดไป </center>
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>\t
\t\t\t{% endif %}
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\"  >Save</button>
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/work_in.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\work_in.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/create_work_post_in.tpl */
class __TwigTemplate_a1845018d54c2c3afb5aa1adcc7ca14f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/create_work_post_in.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
<script src=\"../themes/jquery/jquery-ui.js\"></script>

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
<!-- dependencies for zip mode -->
\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t<!-- / dependencies for zip mode -->

\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t
\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 29
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t



\$(document).keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          \$('#btn_save').click();
        }
});




load_data_bydate();
\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t


var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'}, 
\t\t{'data': 'action'},
        {'data': 'd_send'}, 
        {'data': 'mr_work_barcode'},
        {'data': 'num_doc'},
        {'data': 'mr_status_name'},
        {'data': 'mr_round_name'},
        {'data': 'sendder_name'},
        {'data': 'sendder_address'},
        {'data': 'mr_work_remark'},
        {'data': 'name_resive'}
    ]
});



  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'emp_id_re': {
\t\t\t//required: true
\t\t},
\t\t'dep_re': {
\t\t\trequired: true
\t\t},
\t\t'building': {
\t\t\trequired: true
\t\t},
\t\t'name_send': {
\t\t\trequired: true
\t\t},
\t\t'lname_send': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\trequired: true
\t\t},
\t\t'post_barcode': {
\t\t\trequired: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'emp_id_re': {
\t\t  required: 'กรุณาระบุ ผู้รับ'
\t\t},
\t\t'dep_re': {
\t\t\trequired: 'กรุณาระบุ หน่วยงานผู้รับ'
\t\t  },
\t\t  'building': {
\t\t\trequired: 'กรุณาระบุ ช้นผู้รับ'
\t\t  },
\t\t'name_send': {
\t\t  required: 'กรุณาระบุ ชื่อผู้ส่ง'
\t\t},
\t\t'lname_send': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้ส่ง'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_send': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'post_barcode': {
\t\t  required: 'กรุณาระบุ เลขที่เอกสาร '
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });


\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_Work_post_in.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_detail_form\").prop(\"checked\") == false){
\t\t\t\t\treset_detail_form();
\t\t\t\t}
\t\t\t\t
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t

\$('#dep_re').select2();
\$('#building').select2();
//\$('#cost_id').select2();
\$('#cost_id').select2({
\tplaceholder: \"รหัสค่าใช้จ่าย:\",
\tajax: {
\t\turl: \"./ajax/ajax_get_data_cost_center_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
});
\$('#emp_id_re').select2({
\tplaceholder: \"ค้นหาผู้รับ\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.data.mr_floor_id);
\t//console.log(e.params.data.data.mr_department_id);
\t\$('#dep_re').val(e.params.data.data.mr_department_id).trigger('change');
\t\$('#building').val(e.params.data.data.mr_floor_id).trigger('change');
\t\$('#cost_id').val(e.params.data.data.mr_cost_id).trigger('change');
\t//setForm(e.params.data.id);
});



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_sendceiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}


\t\t\$(\"#name_send\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_send').val(ui.item.name); // display the selected text
                \$('#lname_send').val(ui.item.lname); // display the selected text
                \$('#address_send').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
\t\t\t\t\$('#name_send').val(ui.item.name); // display the selected text
                \$('#lname_send').val(ui.item.lname); // display the selected text
                \$('#address_send').val(ui.item.mr_address); // save selected id to input
                return false;
                return false;
            },
        });


\t\t  \$(\"#post_barcode\").keypress(function(event){
\t\t\tvar ew = event.which;
\t\t\tif(ew == 32)
\t\t\t\treturn true;
\t\t\tif(48 <= ew && ew <= 57)
\t\t\t\treturn true;
\t\t\tif(65 <= ew && ew <= 90)
\t\t\t\treturn true;
\t\t\tif(97 <= ew && ew <= 122)
\t\t\t\treturn true;
\t\t\treturn false;
\t\t});
";
    }

    // line 383
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 384
        echo "
function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_post_in.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}


function reset_detail_form() {
\t\$('#round').val(\"\");
\t\$('#post_barcode').val(\"\");
\t\$('#quty').val(\"1\");
\t\$('#work_remark').val(\"\");
}

function reset_send_form() {
\t\$('#name_send').val(\"\");
\t\$('#lname_send').val(\"\");
\t\$('#address_send').val(\"\");
}

function reset_resive_form() {
\t\$('#emp_id_re').val(\"\").trigger('change');
\t\$('#dep_re').val(\"\").trigger('change');
\t\$('#building').val(\"\").trigger('change');
}

function load_data_bydate() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_load_Work_post_in.php\",
\t\tdata:{
\t\t\tdate:\$('#date_report').val(),
\t\t\tround:\$('#round_printreper').val()
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res['data']).draw();

\t\t}
\t  });
}



function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\t//console.log(11);
\t\t\$(\"#form_print_post_in\").attr('action', 'print_peper_post_in.php');
\t\t\$('#form_print_post_in').submit();
\t}else if(type == 2 ){
\t\t\$(\"#form_print_post_in\").attr('action', 'excel.PostIn.php');
\t\t\$('#form_print_post_in').submit();
\t}else if(type == 3 ){
\t\t\$(\"#form_print_post_in\").attr('action', 'print_peper_post_in_sort_resive.php');
\t\t\$('#form_print_post_in').submit();
\t}else{
\t\talert('ไม่มีข้อมูล');
\t}

}


function reset_emp_id_re(){
\t\$(\"#emp_id_re\").empty().trigger('change')
\t//console.log('reset_emp_id_re');
}

";
    }

    // line 487
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 488
        echo "
<div  class=\"container-fluid\">

\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>สั่งงาน ปณ. ขาเข้า</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > ปณ. ขาเข้า</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"card\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลเอกสาร</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table style=\"width: 100%;\" class=\"\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 514
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\"></td>
\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 522
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 523
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 523), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 523), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 525
        echo "\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t



\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">ผู้ส่ง</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t<table style=\"width: 100%;\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้ส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_send\" id=\"name_send\" data-error=\"#err_name_send\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"ชื่อ นามสกุลผู้ส่ง\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >เลขที่เอกสาร :</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode\" name=\"post_barcode\" data-error=\"#err_post_barcode\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"2\"></textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"";
        // line 577
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">

\t\t\t\t\t\t\t\t<!-- <hr> 
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_send\" name=\"address_send\" data-error=\"#err_address_send\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>-->
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  <hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ผู้รับ</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group mb-3\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-link btn-sm text-danger\" type=\"button\" onclick=\"reset_emp_id_re();\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tล้างข้อมูลผู้รับ
\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_re\" class=\"form-control form-control-sm\" id=\"emp_id_re\" name=\"emp_id_re\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t</select>\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >แผนก/ผ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_re\" class=\"form-control form-control-sm\" id=\"dep_re\" name=\"dep_re\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option selected disabled value=\"\">ระบุหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 614
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["departmentdata"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 615
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_id", [], "any", false, false, false, 615), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_code", [], "any", false, false, false, 615), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_name", [], "any", false, false, false, 615), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 617
        echo "\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสค่าใช้จ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_cost\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_cost\" class=\"form-control form-control-sm\" id=\"cost_id\" name=\"cost_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 628
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cost"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 629
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_id", [], "any", false, false, false, 629), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_code", [], "any", false, false, false, 629), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_cost_name", [], "any", false, false, false, 629), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 631
        echo "\t\t\t\t\t\t\t\t\t\t\t\t</select><br>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >ชัน/อาคาร:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_building\"></span>
\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_building\" class=\"form-control form-control-sm\" id=\"building\" name=\"building\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ระบุอาคาร/ชั้น</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 642
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floordata"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
            // line 643
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 643), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "name", [], "any", false, false, false, 643), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 645
        echo "\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t  <div class=\"form-group text-center\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_detail_form\" value=\"option1\">คงข้อมูลเอกสาร
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t";
        // line 666
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>
\t\t\t\t</form>



<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print_post_in\" action=\"#\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t \t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 682
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t";
        // line 688
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 689
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 689), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 689), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 691
        echo "\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<div class=\"btn-group\" role=\"group\">
\t\t\t\t\t<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-sm btn-secondary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t   พิมพ์ใบงาน
\t\t\t\t\t</button>
\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\">
\t\t\t\t\t\t<a onclick=\"print_option(2);\"  class=\"dropdown-item\" href=\"#\">Excel</a>
\t\t\t\t\t\t<a onclick=\"print_option(1);\"  class=\"dropdown-item\" href=\"#\">แยกหน่วยงาน</a>
\t\t\t\t\t\t<a onclick=\"print_option(3);\"  class=\"dropdown-item\" href=\"#\">แยกผู้รับ</a>
\t\t\t\t\t</div>
\t\t\t\t</div>


\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>





\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการรับเข้่า</h5>
\t\t\t\t\t\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">Update</th>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">วันที่</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่ ปณ.</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">รอบ</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">หมายเหตุ</th>
\t\t\t\t\t\t\t\t\t<th width=\"30%\"scope=\"col\">ผู้รับ</th>
\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t  </table>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t

\t\t\t\t</div>
\t\t\t  </div>
\t\t
\t\t</div>
\t</div>

</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




";
    }

    // line 772
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 774
        if ((($context["debug"] ?? null) != "")) {
            // line 775
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 777
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/create_work_post_in.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  959 => 777,  953 => 775,  951 => 774,  944 => 772,  862 => 691,  851 => 689,  847 => 688,  836 => 682,  818 => 666,  799 => 645,  788 => 643,  784 => 642,  771 => 631,  758 => 629,  754 => 628,  741 => 617,  728 => 615,  724 => 614,  684 => 577,  630 => 525,  619 => 523,  615 => 522,  602 => 514,  574 => 488,  570 => 487,  464 => 384,  460 => 383,  130 => 59,  99 => 30,  95 => 29,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
<script src=\"../themes/jquery/jquery-ui.js\"></script>

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
<!-- dependencies for zip mode -->
\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t<!-- / dependencies for zip mode -->

\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t
\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

{% endblock %}

{% block domReady %}\t



\$(document).keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          \$('#btn_save').click();
        }
});




load_data_bydate();
\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t


var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'}, 
\t\t{'data': 'action'},
        {'data': 'd_send'}, 
        {'data': 'mr_work_barcode'},
        {'data': 'num_doc'},
        {'data': 'mr_status_name'},
        {'data': 'mr_round_name'},
        {'data': 'sendder_name'},
        {'data': 'sendder_address'},
        {'data': 'mr_work_remark'},
        {'data': 'name_resive'}
    ]
});



  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'emp_id_re': {
\t\t\t//required: true
\t\t},
\t\t'dep_re': {
\t\t\trequired: true
\t\t},
\t\t'building': {
\t\t\trequired: true
\t\t},
\t\t'name_send': {
\t\t\trequired: true
\t\t},
\t\t'lname_send': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\trequired: true
\t\t},
\t\t'post_barcode': {
\t\t\trequired: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'emp_id_re': {
\t\t  required: 'กรุณาระบุ ผู้รับ'
\t\t},
\t\t'dep_re': {
\t\t\trequired: 'กรุณาระบุ หน่วยงานผู้รับ'
\t\t  },
\t\t  'building': {
\t\t\trequired: 'กรุณาระบุ ช้นผู้รับ'
\t\t  },
\t\t'name_send': {
\t\t  required: 'กรุณาระบุ ชื่อผู้ส่ง'
\t\t},
\t\t'lname_send': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้ส่ง'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_send': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'post_barcode': {
\t\t  required: 'กรุณาระบุ เลขที่เอกสาร '
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });


\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_Work_post_in.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_detail_form\").prop(\"checked\") == false){
\t\t\t\t\treset_detail_form();
\t\t\t\t}
\t\t\t\t
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t

\$('#dep_re').select2();
\$('#building').select2();
//\$('#cost_id').select2();
\$('#cost_id').select2({
\tplaceholder: \"รหัสค่าใช้จ่าย:\",
\tajax: {
\t\turl: \"./ajax/ajax_get_data_cost_center_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
});
\$('#emp_id_re').select2({
\tplaceholder: \"ค้นหาผู้รับ\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.data.mr_floor_id);
\t//console.log(e.params.data.data.mr_department_id);
\t\$('#dep_re').val(e.params.data.data.mr_department_id).trigger('change');
\t\$('#building').val(e.params.data.data.mr_floor_id).trigger('change');
\t\$('#cost_id').val(e.params.data.data.mr_cost_id).trigger('change');
\t//setForm(e.params.data.id);
});



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_sendceiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}


\t\t\$(\"#name_send\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_send').val(ui.item.name); // display the selected text
                \$('#lname_send').val(ui.item.lname); // display the selected text
                \$('#address_send').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
\t\t\t\t\$('#name_send').val(ui.item.name); // display the selected text
                \$('#lname_send').val(ui.item.lname); // display the selected text
                \$('#address_send').val(ui.item.mr_address); // save selected id to input
                return false;
                return false;
            },
        });


\t\t  \$(\"#post_barcode\").keypress(function(event){
\t\t\tvar ew = event.which;
\t\t\tif(ew == 32)
\t\t\t\treturn true;
\t\t\tif(48 <= ew && ew <= 57)
\t\t\t\treturn true;
\t\t\tif(65 <= ew && ew <= 90)
\t\t\t\treturn true;
\t\t\tif(97 <= ew && ew <= 122)
\t\t\t\treturn true;
\t\t\treturn false;
\t\t});
{% endblock %}
{% block javaScript %}

function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_post_in.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}


function reset_detail_form() {
\t\$('#round').val(\"\");
\t\$('#post_barcode').val(\"\");
\t\$('#quty').val(\"1\");
\t\$('#work_remark').val(\"\");
}

function reset_send_form() {
\t\$('#name_send').val(\"\");
\t\$('#lname_send').val(\"\");
\t\$('#address_send').val(\"\");
}

function reset_resive_form() {
\t\$('#emp_id_re').val(\"\").trigger('change');
\t\$('#dep_re').val(\"\").trigger('change');
\t\$('#building').val(\"\").trigger('change');
}

function load_data_bydate() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_load_Work_post_in.php\",
\t\tdata:{
\t\t\tdate:\$('#date_report').val(),
\t\t\tround:\$('#round_printreper').val()
\t\t},
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res['data']).draw();

\t\t}
\t  });
}



function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\t//console.log(11);
\t\t\$(\"#form_print_post_in\").attr('action', 'print_peper_post_in.php');
\t\t\$('#form_print_post_in').submit();
\t}else if(type == 2 ){
\t\t\$(\"#form_print_post_in\").attr('action', 'excel.PostIn.php');
\t\t\$('#form_print_post_in').submit();
\t}else if(type == 3 ){
\t\t\$(\"#form_print_post_in\").attr('action', 'print_peper_post_in_sort_resive.php');
\t\t\$('#form_print_post_in').submit();
\t}else{
\t\talert('ไม่มีข้อมูล');
\t}

}


function reset_emp_id_re(){
\t\$(\"#emp_id_re\").empty().trigger('change')
\t//console.log('reset_emp_id_re');
}

{% endblock %}
{% block Content2 %}

<div  class=\"container-fluid\">

\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>สั่งงาน ปณ. ขาเข้า</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > ปณ. ขาเข้า</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"card\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลเอกสาร</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table style=\"width: 100%;\" class=\"\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\"></td>
\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t



\t\t\t\t\t\t<div class=\"col-md-9\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">ผู้ส่ง</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t<table style=\"width: 100%;\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้ส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_send\" id=\"name_send\" data-error=\"#err_name_send\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"ชื่อ นามสกุลผู้ส่ง\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >เลขที่เอกสาร :</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_barcode\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_barcode\" name=\"post_barcode\" data-error=\"#err_post_barcode\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"2\"></textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"{{csrf_token}}\">

\t\t\t\t\t\t\t\t<!-- <hr> 
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_send\" name=\"address_send\" data-error=\"#err_address_send\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>-->
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  <hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ผู้รับ</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group mb-3\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-link btn-sm text-danger\" type=\"button\" onclick=\"reset_emp_id_re();\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tล้างข้อมูลผู้รับ
\t\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_re\" class=\"form-control form-control-sm\" id=\"emp_id_re\" name=\"emp_id_re\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t</select>\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >แผนก/ผ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_re\" class=\"form-control form-control-sm\" id=\"dep_re\" name=\"dep_re\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option selected disabled value=\"\">ระบุหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% for d in departmentdata %}
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{d.mr_department_id}}\">{{d.mr_department_code}} {{d.mr_department_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสค่าใช้จ่าย:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_cost\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_cost\" class=\"form-control form-control-sm\" id=\"cost_id\" name=\"cost_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for c in cost %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ c.mr_cost_id }}\">{{ c.mr_cost_code }} - {{ c.mr_cost_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t</select><br>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >ชัน/อาคาร:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_building\"></span>
\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_building\" class=\"form-control form-control-sm\" id=\"building\" name=\"building\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ระบุอาคาร/ชั้น</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% for f in floordata %}
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{f.mr_floor_id}}\">{{f.name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t  <div class=\"form-group text-center\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_detail_form\" value=\"option1\">คงข้อมูลเอกสาร
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_send_form();\">ล้างข้อมูลผู้ส่ง </button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_resive_form();\">ล้างข้อมูลผู้รับ </button>
\t\t\t\t\t\t\t\t#}

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>
\t\t\t\t</form>



<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print_post_in\" action=\"#\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t \t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<div class=\"btn-group\" role=\"group\">
\t\t\t\t\t<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-sm btn-secondary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t   พิมพ์ใบงาน
\t\t\t\t\t</button>
\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\">
\t\t\t\t\t\t<a onclick=\"print_option(2);\"  class=\"dropdown-item\" href=\"#\">Excel</a>
\t\t\t\t\t\t<a onclick=\"print_option(1);\"  class=\"dropdown-item\" href=\"#\">แยกหน่วยงาน</a>
\t\t\t\t\t\t<a onclick=\"print_option(3);\"  class=\"dropdown-item\" href=\"#\">แยกผู้รับ</a>
\t\t\t\t\t</div>
\t\t\t\t</div>


\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>





\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">รายการรับเข้่า</h5>
\t\t\t\t\t\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">Update</th>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">วันที่</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่ ปณ.</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">รอบ</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">หมายเหตุ</th>
\t\t\t\t\t\t\t\t\t<th width=\"30%\"scope=\"col\">ผู้รับ</th>
\t\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t  </table>


\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t

\t\t\t\t</div>
\t\t\t  </div>
\t\t
\t\t</div>
\t</div>

</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/create_work_post_in.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/create_work_post_in.tpl");
    }
}

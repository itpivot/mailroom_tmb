<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/create_work_import_excel.tpl */
class __TwigTemplate_78d55444f33a06e29584cde93141c362 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/create_work_import_excel.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
";
    }

    // line 11
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "\t#btn_save:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#46A6FB;
\t\t}
\t
\t#btn_save{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tmargin: 0rem auto;
\t\t}
 
.valit_error{
\tborder:2px solid red;
\tborder-color: red;
\tborder-radius: 5px;
} 
 
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
\t
\t
.disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}

select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}



\t
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#46A6FB;
\t\t}

\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tmargin: 0rem auto;
\t\t}
 

";
    }

    // line 159
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 160
        echo "/* \$('#bg_loader').show(); */
var from_obj = [];
var from_ch = 0;
 \$('[data-toggle=\"tooltip\"]').tooltip()
 {/* 
 \$('#tb_con').DataTable();
 var tbl_data = \$('#tb_addata').DataTable({ 
    \"responsive\": true,
\t\"scrollX\": true,
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'type'},
        {'data': 'name'},
        {'data': 'tel'},
        {'data': 'strdep'},
        {'data': 'floor'},
        {'data': 'doc_title'},
        {'data': 'remark'}
    ]
}); 

*/}


 \$(\".alert\").alert();

\t\t\$(\"#modal_click\").click(function(){
\t\t\t\$('#modal_showdata').modal({ backdrop: false})
\t\t})
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar branch_floor = '';
\t\t\t\t\t
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\tif( \$(\"#type_send_1\").is(':checked')){
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_head\").val();
\t\t\t\t\t\t\tvar remark\t\t\t\t= \$(\"#remark_head\").val();
\t\t\t\t\t\t\tvar destination \t\t= \$(\"#floor_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 1;
\t\t\t\t\t\t\t//alert(floor_send);
\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_branch\").val();
\t\t\t\t\t\t\tvar remark  \t\t\t= \$(\"#remark_branch\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 2;
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t
\t\t\tif( type_send == 2 ){
\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\tvar branch_send\t\t\t\t= \$(\"#branch_send\").val();
\t\t\t\t    branch_floor\t\t\t\t= \$(\"#branch_floor\").val();
\t\t\t\t
\t\t\t\t//console.log(branch_receiver);
\t\t\t\t//console.log(branch_send);
\t\t\t\tif( branch_receiver == \"-\" && branch_send == 0 ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\tif( (destination == \"-\" || destination == \"\") && \$('#floor_name').val() == 0 ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}
\t\t\t\t\tif( type_send == 2 ){
\t\t\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\t\t\tvar destination \t\t\t= \$(\"#branch_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(destination);
\t\t\t\t\t\t//console.log(branch_send);
\t\t\t\t\t\tif( branch_receiver == \"-\" && destination == 0 ){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(emp_id == \"\" || emp_id == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\ttopic: topic,
\t\t\t\t\t\t\t\tdestination: destination ,
\t\t\t\t\t\t\t\ttype_send: \ttype_send, \t\t\t
\t\t\t\t\t\t\t\tbranch_floor: \tbranch_floor \t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t//console.log(obj);
\t\t\t\t\t\t//return;
\t\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล',\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง\",
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\tsaveBranch(obj);   
                                },
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\talertify.error('ยกเลิก');
\t\t\t\t\t\t\t\t}).set('labels', {ok:'บันทึก', cancel:'ยกเลิก'});
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t                                                                                  
\t\t\t\t\t\t\t                 
\t\t\t\t\t\t\t//\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});                              
\t\t\t\t\t\t\t                                                                                          
\t\t\t\t\t\t}                                                                                             
\t\t\t
\t\t});
\t\t
\t\t
\t\t\$('#branch_send').select2({
\t\t\t\t\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\turl: \"../branch/ajax/ajax_getdata_branch_select.php\",
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"../branch/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});
\t\t
\t\t\$('.select_name').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"../branch/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});


\$('#bg_loader').hide(); 
\$('#div_error').hide();\t\t
";
    }

    // line 357
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 358
        echo "
\t\t\$('#type_send_1').prop('checked',true);
\t\t
\t\tfunction check_type_send( type_send ){
\t\t\t if( type_send == 1 ){ 
\t\t\t\t\$(\"#type_1\").show();
\t\t\t\t\$(\"#type_2\").hide();
\t\t\t }else{
\t\t\t\t\$(\"#type_1\").hide();
\t\t\t\t\$(\"#type_2\").show();
\t\t\t }
\t\t}
\t\t
\t\tfunction getemp(emp_code) {
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_get_empID_bycode.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\temp_code: emp_code
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#modal_showdata').modal('hide');
\t\t\t\t\tif(res.length > 0){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tid: \$.trim(res[0]['mr_emp_id'])
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\tsetForm(obj);
\t\t\t\t\t\t\t\$('#name_receiver_select').html('');
\t\t\t\t\t\t\t\$('#name_receiver_select').append('<option value=\"'+\$.trim(res[0]['mr_emp_id'])+'\">'+res[0]['mr_emp_code']+':'+res[0]['mr_emp_name']+'  '+res[0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#depart_receiver\").val(res['data']['department']);
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#tel_receiver\").val(res['data']['mr_emp_tel']);
\t\t\t\t\t\t\t\t\$(\"#place_receiver\").val(res['data']['mr_workarea']);
\t\t\t\t\t\t\t\t\$(\"#floor_name\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t\$(\"#branch_receiver\").val(res['data']['branch']);
\t\t\t\t\t\t\t\t\$('#branch_send').html('');
\t\t\t\t\t\t\t\t\$('#branch_send').append('<option value=\"'+\$.trim(res['data']['mr_branch_id'])+'\">'+res['data']['mr_branch_code']+':'+res['data']['mr_branch_name']+'</option>');\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\tfunction setrowdata(id,eli_name) {
\t\t\tvar emp_id = \$('#emp_'+eli_name).val();
\t\t\t//</link>console.log('>>>>>>>>'+eli_name+'>>>>>>'+emp_id)
\t\t\t\t\$.ajax({
\t\t\t\t\turl: '../branch/ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\tif(\$('#val_type_'+eli_name).val() == 1){
\t\t\t\t\t\t\t\t\t\$(\"#floor_\"+eli_name).val(res['data']['mr_floor_id']).trigger(\"change\")
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['data']['mr_department_id'])+'\">'+res['data']['department']+'</option>');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).attr({'readonly': 'readonly'}).trigger('change');
\t\t\t\t\t\t\t\t\t//console.log('555');
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['data']['mr_branch_id'])+'\">'+res['data']['branch']+'</option>');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).removeAttr('readonly').trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\$(\"#tel_\"+eli_name).val(res['data']['department']);
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t}\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t

\t\tvar saveBranch = function(obj) {
\t\t\t//return \$.post('ajax/ajax_save_work_branch.php', obj);
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_save_work_branch.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: obj,
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res) {
\t\t\t\t\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' + res['barcodeok'] + '<b></font></h1>';
\t\t\t\t\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg, 
\t\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t    alertify.success('print');
\t\t\t\t\t\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\t\t\t\t\t\twindow.open(url);
\t\t\t\t\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#emp_id\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic_head\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic_branch\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark_head\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark_branch\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#tel_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_name\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#branch_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\talertify.success('save');
\t\t\t\t\t\t\t\t\t\t\t\t//alertify.confirm('ตรวจสอบข้อมูล',\"This is a confirm dialog.\",
\t\t\t\t\t\t\t\t\t\t\t\t//  function(){
\t\t\t\t\t\t\t\t\t\t\t\t//\t//alertify.success('print');
\t\t\t\t\t\t\t\t\t\t\t\t//  },
\t\t\t\t\t\t\t\t\t\t\t\t//  function(){
\t\t\t\t\t\t\t\t\t\t\t\t//\t//alertify.error('OK');
\t\t\t\t\t\t\t\t\t\t\t\t//  }).set('labels', {ok:'Submit', cancel:'Cancel'});
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t}).set('labels', {ok:'ปริ้นใบปะหน้า', cancel:'ตกลง'});

\t\t\t
\t\t\t\t\t\t\t\t\t\t                                                                              
\t\t\t\t\t\t\t\t\t} 
\t\t\t\t\t}
\t\t\t\t})
\t\t\t
\t\t}
\t\t
\t\tfunction import_excel() {
\t\t\t\$('#div_error').hide();\t
\t\t\tvar formData = new FormData();
\t\t\tformData.append('file', \$('#file')[0].files[0]);
\t\t\tif(\$('#file').val() == ''){
\t\t\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t}else{
\t\t\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t\t\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\t// console.log(extension);
\t\t\t\treturn;
\t\t\t }
\t\t\t}
\t\t\t//console.log(formData);
\t\t\t\$.ajax({
\t\t\t\t   url : 'ajax/ajax_readFile_excel_work.php',
\t\t\t\t   dataType : 'json',
\t\t\t\t   type : 'POST',
\t\t\t\t   data : formData,
\t\t\t\t   processData: false,  // tell jQuery not to process the data
\t\t\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t\t\t   success : function(data) {
\t\t\t\t\t    // \$('#tb_addata').DataTable().clear().draw();
\t\t\t\t\t\t// \$('#tb_addata').DataTable().rows.add(data).draw();
\t\t\t\t\t\tvar txt_html ='';
\t\t\t\t\t\t\$.each( data, function( key, value ) {
\t\t\t\t\t\t\t txt_html += '<tr>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['no']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['type']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['name']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['tel']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['strdep']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['floor']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['doc_title']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['remark']+'</td>';
\t\t\t\t\t\t\t txt_html += '</tr>';
\t\t\t\t\t\t  });

\t\t\t\t\t\t\$('#t_data').html(txt_html);
\t\t\t\t\t\t\$('.select_floor').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t ch_fromdata()

\t\t\t\t\t\t});
\t\t\t\t\t\t\$('.select_type').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t var name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t var v_al = \$(this).val();

\t\t\t\t\t\t\t var myarr = name.split(\"_\");
\t\t\t\t\t\t\t var extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t setrowdata(v_al,extension)
\t\t\t\t\t\t\t if(v_al == 2){
\t\t\t\t\t\t\t\t  \$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t  \$('#floor_'+extension).html(\$('#branch_floor').html());
\t\t\t\t\t\t\t }else{
\t\t\t\t\t\t\t\t \$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t \$('#floor_'+extension).html(\$('#floor_send').html());
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t }
\t\t\t\t\t\t\t ch_fromdata()
\t\t\t\t\t\t\t// console.log('floor_'+extension)
\t\t\t\t\t\t});
\t\t\t\t\t\t
\t\t\t\t\t\t\$('.select_name').select2({
\t\t\t\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select_file_import.php\",
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\tvar name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\tvar v_al = \$(this).val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar myarr = name.split(\"_\");
\t\t\t\t\t\t\tvar extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\tsetrowdata(v_al,extension)
\t\t\t\t\t\t\t //console.log(extension)
\t\t\t\t\t\t\t ch_fromdata()
\t\t\t\t\t\t});
\t\t\t\t\t\t\$('.strdep').select2({
\t\t\t\t\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\turl: \"../branch/ajax/ajax_getdata_branch_select.php\",
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\t//var name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t//var v_al = \$(this).val();
\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\t//setrowdata(v_al,extension)
\t\t\t\t\t\t\t// //console.log(extension)
\t\t\t\t\t\t\t ch_fromdata()
\t\t\t\t\t\t});
\t\t\t\t\t  // console.log(data);
\t\t\t\t\t   from_obj = data;
\t\t\t\t\t   ch_fromdata()
\t\t\t\t\t  // alert(data);
\t\t\t\t\t   \$('#bg_loader').hide();
\t\t\t\t   }, beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\terror: function (error) {
\t\t\t\t\t\talert(\"sesstion หมดอายุ  กรุณา Login\");
\t\t\t\t\t\t//location.reload();
\t\t\t\t\t}
\t\t\t\t});
\t\t
\t\t}
\t\t
\t\tfunction ch_fromdata() {
\t\tfrom_ch=0;
\t\t\$('#div_error').hide();
\t\t\$('#div_error').html('กรุณาตรวจสอบข้อมูล !!');
\t\t\t\$.each( from_obj, function( key, value ) {
\t\t\t\tif(\$('#val_type_'+value['in']).val() == \"\" || \$('#val_type_'+value['in']).val() == null){
\t\t\t\t\t\$('#type_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#type_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#emp_'+value['in']).val() == \"\" || \$('#emp_'+value['in']).val() == null){
\t\t\t\t\t\$('#name_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#name_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#floor_'+value['in']).val() == \"\" || \$('#floor_'+value['in']).val() == null || \$('#floor_'+value['in']).val() == 0){
\t\t\t\t\t\$('#floor_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#floor_error_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\t
\t\t\t\tif(\$('#title_'+value['in']).val() == \"\" || \$('#title_'+value['in']).val() == null || \$('#title_'+value['in']).val() == 0){
\t\t\t\t\t\$('#title_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#title_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#val_type_'+value['in']).val() != 1){
\t\t\t\t\tif(\$('#strdep_'+value['in']).val() == \"\" || \$('#strdep_'+value['in']).val() == null || \$('#strdep_'+value['in']).val() == 0){
\t\t\t\t\t\t\$('#dep_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\t\tfrom_ch+=1;
\t\t\t\t\t}else{
\t\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t}else{
\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t}
\t\t\t\t
\t\t\t
\t\t\t\t//console.log('<<<<<<<<<<<<<>>>>>>>>>>>>>>>>');
\t\t\t});
\t\t}
\t\tfunction start_save() {
\t\t\t\$('#div_error').hide();
\t\t\tif( from_ch > 0 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( from_obj.length < 1 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล  !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\tvar adll_data = \$('#form_import_excel').serializeArray()
\t\t\tvar count_data = from_obj.length;
\t\t\tdata = JSON.stringify(adll_data);
\t\t\t// console.log(adll_data);
\t\t\t// console.log(from_obj.length);
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_save_work_import.php?count='+count_data,
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'data_all' : data,
\t\t\t\t\t\t'csrf_token' : \$('#csrf_token').val(),
\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 401){
\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t\talert(res['message']);
\t\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t\t\t
\t\t\t\t\t\t}else{
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\t\t\talertify.confirm('บันทึกข้อมูลสำเร็จ','ท่านต้องการปริ้นใบปะหน้าซองหรือไม่', 
\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['arr_id']; 
\t\t\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t}, 3000);
\t\t\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['arr_id']; 
\t\t\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'ไม่'});
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\terror: function (error) {
\t\t\t\t\t alert('เกิดข้อผิดพลาด !!!');
\t\t\t\t\t window.location.reload();
\t\t\t\t\t}
\t\t\t\t})
\t\t\t
\t\t}
\t\t\t

function dowload_excel(){
\t\talertify.confirm('Download Excel','คุณต้องการ Download Excel file', 
\t\t\t\tfunction(){
\t\t\t\t\t//window.location.href='../themes/TMBexcelimport.xlsx';
\t\t\t\t\twindow.open(\"../themes/TMBexcelimport.xlsx\", \"_blank\");
\t\t\t\t},function() {
\t\t\t}).set('labels', {ok:'ตกลง', cancel:'ยกเลิก'});

}\t\t\t
";
    }

    // line 774
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 775
        echo "
<div  class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>บันทึกรายการนำส่ง</b></h3></label>
\t\t\t</div>\t
\t\t\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 781
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 781), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"";
        // line 783
        echo twig_escape_filter($this->env, ($context["barcode"] ?? null), "html", null, true);
        echo "\">
\t\t\t
\t\t<!-- \t<div class=\"\" style=\"text-align: left;\">
\t\t\t\t <label>เลขที่เอกสาร :  -</label>
\t\t\t</div>\t -->
\t\t\t
\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t\t <h4>รายละเอียดผู้ส่ง </h4>
\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"row row justify-content-sm-center\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">สาขาผู้ส่ง :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อสาขา\" value=\"";
        // line 796
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_branch_code", [], "any", false, false, false, 796), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_branch_name", [], "any", false, false, false, 796), "html", null, true);
        echo "\" disabled>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"mr_emp_tel\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_emp_tel\" placeholder=\"เบอร์โทร\" value=\"";
        // line 804
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_tel", [], "any", false, false, false, 804), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"div_re_select\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">ชื่อผู้ส่ง  :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\"  value=\"";
        // line 818
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_code", [], "any", false, false, false, 818), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_name", [], "any", false, false, false, 818), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_lastname", [], "any", false, false, false, 818), "html", null, true);
        echo "\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-6\" style=\"padding-left:0px\">
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>
\t\t
\t\t<!-- ----------------------------------------------------------------------- \tรายละเอียดผู้รับ -->

\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t  ";
        // line 834
        echo "\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link active\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#page_inport_excel\" role=\"tab\" aria-controls=\"tab_2\" aria-selected=\"false\">Import Excel</a>
\t\t  </li>
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" onclick=\"dowload_excel();\">Download Templates Excel</a>
\t\t  </li>
\t\t</ul>
\t<div class=\"tab-content\" id=\"myTabContent\">
\t<div id=\"page_inport_excel\" class=\"tab-pane fade show active tab-pane fade\" aria-labelledby=\"tab2-tab\" role=\"tabpanel\">
\t\t<form id=\"form_import_excel\">
\t\t\t<div class=\"form-group\">
\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"><i class=\"material-icons\">help</i></a>
\t\t\t\t<label for=\"file\">
\t\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t\t\t<h4>Import File </h4>  
\t\t\t\t\t</div>
\t\t\t\t</label>
\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t</div>
\t<br>
\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t<button onclick=\"start_save()\" type=\"button\" class=\"btn btn-success\">&nbsp;Save&nbsp;</button>
\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 856
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\"></input>

\t\t\t<br>
\t\t\t<hr>
\t\t\t<br>
\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\">
\t\t\t  A simple danger alert—check it out!
\t\t\t</div>
\t
\t\t
\t\t\t<div class=\"table table-responsive\">
\t\t\t<table id=\"tb_addata\" class=\"table display nowrap table-striped table-bordered\">
\t\t\t\t  <thead>
\t\t\t\t\t<tr>
\t\t\t\t\t  <th scope=\"col\">No</th>
\t\t\t\t\t  <th scope=\"col\">ประเภทการส่ง</th>
\t\t\t\t\t  <th scope=\"col\">ผู้รับงาน</th>
\t\t\t\t\t  <th scope=\"col\">ตำแหน่ง</th>
\t\t\t\t\t  <th scope=\"col\">แผนก</th>
\t\t\t\t\t  <th scope=\"col\">ชั้น</th>
\t\t\t\t\t  <th scope=\"col\">ชื่อเอกสาร</th>
\t\t\t\t\t  <th scope=\"col\">หมายเหตุ</th>
\t\t\t\t\t</tr>
\t\t\t\t  </thead>
\t\t\t\t  <tbody id=\"t_data\">
\t\t\t\t\t
\t\t\t\t  </tbody>
\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t
\t\t</form>
\t<br>
\t<br>
\t<br>
\t<br>
\t</div>\t\t

\t<div id=\"page_create_work\" class=\"tab-pane fade\" aria-labelledby=\"tab1-tab\" role=\"tabpanel\">
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t <h4>รายละเอียดผู้รับ</h4>  <a id=\"modal_click\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ตรวจสอบชื่อผู้รับตามหน่วยงาน\"><i class=\"material-icons\">help</i></a>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"name_receiver\">ประเภทการส่ง  :</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_1\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_1\" value=\"1\" onclick=\"check_type_send('1');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สำนักงานใหญ่
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_2\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_2\" value=\"2\" onclick=\"check_type_send('2');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สาขา
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t
\t\t\t\t\t<!-- <div class=\"col-1\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t -->
\t\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t\t\t
\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver_select\">ค้นหาผู้รับ  :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t 
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"tel_receiver\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_receiver\" placeholder=\"เบอร์โทร\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div id=\"type_1\" style=\"display:;\">
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"depart_receiver\">แผนก : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart_receiver\" placeholder=\"ชื่อแผนก\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_name\">ชั้น : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor_name\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_send\">ชั้นผู้รับ\t: </label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t<option value=\"0\" > เลือกชั้นปลายทาง</option>
\t\t\t\t\t\t\t\t\t";
        // line 970
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 971
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_floor_id", [], "any", false, false, false, 971), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "name", [], "any", false, false, false, 971), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 972
        echo "\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"topic_head\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"topic_head\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_head\">หมายเหตุ :  </label>
\t\t\t\t\t\t\t\t<textarea class=\"form-control form-control-sm\" id=\"remark_head\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t

\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div id=\"type_2\" style=\"display:none;\">
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_receiver\">สาขา :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"branch_receiver\" placeholder=\"ชื่อสาขา\" disabled>
\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"place_receiver\">สถานที่อยู่ :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"place_receiver\" placeholder=\"สถานที่อยู่\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_send\">สาขาผู้รับ : </label>
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"topic_branch\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"topic_branch\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_floor\">ชั้นผู้รับ : </label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
\t\t\t\t\t\t\t\t</select>\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_branch\">หมายเหตุ : </label>
\t\t\t\t\t\t\t\t<textarea class=\"form-control form-control-sm\" id=\"remark_branch\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" style=\"margin-top:50px;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-2\" style=\"padding-left:0px\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn_save\"  >บันทึกรายการ</button>

\t\t\t\t\t</div>\t\t
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t
\t\t
\t</div>
\t</div>
</div>
\t
\t


<div class=\"modal fade\" id=\"modal_showdata\">
  <div class=\"modal-dialog modal-dialog modal-xl\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\">รายชื่อติดต่อในการจัดส่ง</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t  <div style=\"overflow-x:auto;\">
        <table class=\"table\" id=\"tb_con\">
\t\t\t  <thead>
\t\t\t  
\t\t\t\t<tr>
\t\t\t\t  <th>ลำดับ</th>
\t\t\t\t  <th>ชื่อหน่วยงาน</th>
\t\t\t\t  <th>ชั้น</th>
\t\t\t\t  <th>ผู้รับงาน</th>
\t\t\t\t  <th>รหัสพนักงาน</th>
\t\t\t\t  <th>เบอร์โทร</th>
\t\t\t\t  <th>หมายเหตุ</th>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t  ";
        // line 1127
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contactdata"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 1128
            echo "\t\t\t\t<tr>
\t\t\t\t  <th scope=\"row\">";
            // line 1129
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 1129), "html", null, true);
            echo "</th>
\t\t\t\t  <td>";
            // line 1130
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "department_code", [], "any", false, false, false, 1130), "html", null, true);
            echo ":";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "department_name", [], "any", false, false, false, 1130), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1131
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "floor", [], "any", false, false, false, 1131), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1132
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_contact_name", [], "any", false, false, false, 1132), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1133
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_code", [], "any", false, false, false, 1133), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1134
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "emp_tel", [], "any", false, false, false, 1134), "html", null, true);
            echo "</td>
\t\t\t\t  <td>";
            // line 1135
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "remark", [], "any", false, false, false, 1135), "html", null, true);
            echo "</td>
\t\t\t\t</tr>
\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1138
        echo "\t\t\t  </tbody>
\t\t\t</table>
\t\t</div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>


<div id=\"click_new_tab\">
</div>
<div id=\"bg_loader\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>

";
    }

    // line 1159
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 1161
        if ((($context["debug"] ?? null) != "")) {
            // line 1162
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 1164
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/create_work_import_excel.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1350 => 1164,  1344 => 1162,  1342 => 1161,  1335 => 1159,  1313 => 1138,  1296 => 1135,  1292 => 1134,  1288 => 1133,  1284 => 1132,  1280 => 1131,  1274 => 1130,  1270 => 1129,  1267 => 1128,  1250 => 1127,  1093 => 972,  1082 => 971,  1078 => 970,  961 => 856,  937 => 834,  917 => 818,  900 => 804,  887 => 796,  871 => 783,  866 => 781,  858 => 775,  854 => 774,  435 => 358,  431 => 357,  234 => 160,  230 => 159,  81 => 12,  77 => 11,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
{% endblock %}
{% block styleReady %}
\t#btn_save:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#46A6FB;
\t\t}
\t
\t#btn_save{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tmargin: 0rem auto;
\t\t}
 
.valit_error{
\tborder:2px solid red;
\tborder-color: red;
\tborder-radius: 5px;
} 
 
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}
\t
\t
.disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}

select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}



\t
\t#modal_click {
\t\tcursor: help;
\t\tcolor:#46A6FB;
\t\t}

\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t

\t\t.modal-dialog {
\t\t\tmax-width: 2000px; 
\t\t\tmargin: 0rem auto;
\t\t}
 

{% endblock %}

{% block domReady %}
/* \$('#bg_loader').show(); */
var from_obj = [];
var from_ch = 0;
 \$('[data-toggle=\"tooltip\"]').tooltip()
 {/* 
 \$('#tb_con').DataTable();
 var tbl_data = \$('#tb_addata').DataTable({ 
    \"responsive\": true,
\t\"scrollX\": true,
\t\"searching\": true,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
\t\"pageLength\": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'type'},
        {'data': 'name'},
        {'data': 'tel'},
        {'data': 'strdep'},
        {'data': 'floor'},
        {'data': 'doc_title'},
        {'data': 'remark'}
    ]
}); 

*/}


 \$(\".alert\").alert();

\t\t\$(\"#modal_click\").click(function(){
\t\t\t\$('#modal_showdata').modal({ backdrop: false})
\t\t})
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar branch_floor = '';
\t\t\t\t\t
\t\t\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\t\t\tvar status \t\t\t\t= true;
\t\t\t\t\tif( \$(\"#type_send_1\").is(':checked')){
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_head\").val();
\t\t\t\t\t\t\tvar remark\t\t\t\t= \$(\"#remark_head\").val();
\t\t\t\t\t\t\tvar destination \t\t= \$(\"#floor_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 1;
\t\t\t\t\t\t\t//alert(floor_send);
\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar topic \t\t\t\t= \$(\"#topic_branch\").val();
\t\t\t\t\t\t\tvar remark  \t\t\t= \$(\"#remark_branch\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\tvar type_send\t\t\t= 2;
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t
\t\t\tif( type_send == 2 ){
\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\tvar branch_send\t\t\t\t= \$(\"#branch_send\").val();
\t\t\t\t    branch_floor\t\t\t\t= \$(\"#branch_floor\").val();
\t\t\t\t
\t\t\t\t//console.log(branch_receiver);
\t\t\t\t//console.log(branch_send);
\t\t\t\tif( branch_receiver == \"-\" && branch_send == 0 ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\tif( (destination == \"-\" || destination == \"\") && \$('#floor_name').val() == 0 ){
\t\t\t\t\tstatus = false;
\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\treturn;
\t\t\t\t
\t\t\t\t}
\t\t\t}
\t\t\t\t\tif( type_send == 2 ){
\t\t\t\t\t\tvar branch_receiver \t\t= \$.trim(\$(\"#branch_receiver\").val());
\t\t\t\t\t\tvar destination \t\t\t= \$(\"#branch_send\").val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t//console.log(destination);
\t\t\t\t\t\t//console.log(branch_send);
\t\t\t\t\t\tif( branch_receiver == \"-\" && destination == 0 ){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif(emp_id == \"\" || emp_id == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\tif(topic == \"\" || topic == null){
\t\t\t\t\t\t\tstatus = false;
\t\t\t\t\t\t\talertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชื่อเอกสาร !');
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\tif( status === true ){
\t\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\temp_id: emp_id,
\t\t\t\t\t\t\t\tuser_id: user_id,
\t\t\t\t\t\t\t\tremark: remark,
\t\t\t\t\t\t\t\ttopic: topic,
\t\t\t\t\t\t\t\tdestination: destination ,
\t\t\t\t\t\t\t\ttype_send: \ttype_send, \t\t\t
\t\t\t\t\t\t\t\tbranch_floor: \tbranch_floor \t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t//console.log(obj);
\t\t\t\t\t\t//return;
\t\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล',\"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง\",
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\tsaveBranch(obj);   
                                },
\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\talertify.error('ยกเลิก');
\t\t\t\t\t\t\t\t}).set('labels', {ok:'บันทึก', cancel:'ยกเลิก'});
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t                                                                                  
\t\t\t\t\t\t\t                 
\t\t\t\t\t\t\t//\$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});                              
\t\t\t\t\t\t\t                                                                                          
\t\t\t\t\t\t}                                                                                             
\t\t\t
\t\t});
\t\t
\t\t
\t\t\$('#branch_send').select2({
\t\t\t\t\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\turl: \"../branch/ajax/ajax_getdata_branch_select.php\",
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"../branch/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});
\t\t
\t\t\$('.select_name').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"../branch/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t});


\$('#bg_loader').hide(); 
\$('#div_error').hide();\t\t
{% endblock %}\t



{% block javaScript %}

\t\t\$('#type_send_1').prop('checked',true);
\t\t
\t\tfunction check_type_send( type_send ){
\t\t\t if( type_send == 1 ){ 
\t\t\t\t\$(\"#type_1\").show();
\t\t\t\t\$(\"#type_2\").hide();
\t\t\t }else{
\t\t\t\t\$(\"#type_1\").hide();
\t\t\t\t\$(\"#type_2\").show();
\t\t\t }
\t\t}
\t\t
\t\tfunction getemp(emp_code) {
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_get_empID_bycode.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\temp_code: emp_code
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#modal_showdata').modal('hide');
\t\t\t\t\tif(res.length > 0){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\tvar obj = {};
\t\t\t\t\t\t\tobj = {
\t\t\t\t\t\t\t\tid: \$.trim(res[0]['mr_emp_id'])
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\tsetForm(obj);
\t\t\t\t\t\t\t\$('#name_receiver_select').html('');
\t\t\t\t\t\t\t\$('#name_receiver_select').append('<option value=\"'+\$.trim(res[0]['mr_emp_id'])+'\">'+res[0]['mr_emp_code']+':'+res[0]['mr_emp_name']+'  '+res[0]['mr_emp_lastname']+'</option>');
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\tfunction setForm(data) {
\t\t\t\tvar emp_id = parseInt(data.id);
\t\t\t\tvar fullname = data.text;
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\t\$(\"#depart_receiver\").val(res['data']['department']);
\t\t\t\t\t\t\t\t\$(\"#emp_id\").val(res['data']['mr_emp_id']);
\t\t\t\t\t\t\t\t\$(\"#tel_receiver\").val(res['data']['mr_emp_tel']);
\t\t\t\t\t\t\t\t\$(\"#place_receiver\").val(res['data']['mr_workarea']);
\t\t\t\t\t\t\t\t\$(\"#floor_name\").val(res['data']['mr_department_floor']);
\t\t\t\t\t\t\t\t\$(\"#branch_receiver\").val(res['data']['branch']);
\t\t\t\t\t\t\t\t\$('#branch_send').html('');
\t\t\t\t\t\t\t\t\$('#branch_send').append('<option value=\"'+\$.trim(res['data']['mr_branch_id'])+'\">'+res['data']['mr_branch_code']+':'+res['data']['mr_branch_name']+'</option>');\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\tfunction setrowdata(id,eli_name) {
\t\t\tvar emp_id = \$('#emp_'+eli_name).val();
\t\t\t//</link>console.log('>>>>>>>>'+eli_name+'>>>>>>'+emp_id)
\t\t\t\t\$.ajax({
\t\t\t\t\turl: '../branch/ajax/ajax_autocompress_name.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\tif(res.data != false) {
\t\t\t\t\t\t\t\tif(\$('#val_type_'+eli_name).val() == 1){
\t\t\t\t\t\t\t\t\t\$(\"#floor_\"+eli_name).val(res['data']['mr_floor_id']).trigger(\"change\")
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['data']['mr_department_id'])+'\">'+res['data']['department']+'</option>');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).attr({'readonly': 'readonly'}).trigger('change');
\t\t\t\t\t\t\t\t\t//console.log('555');
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).html('');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).append('<option value=\"'+\$.trim(res['data']['mr_branch_id'])+'\">'+res['data']['branch']+'</option>');
\t\t\t\t\t\t\t\t\t\$(\"#strdep_\"+eli_name).removeAttr('readonly').trigger('change');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\$(\"#tel_\"+eli_name).val(res['data']['department']);
\t\t\t\t\t\t\t\tch_fromdata()
\t\t\t\t\t\t\t}\t
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t}
\t\t
\t\t
\t\t
\t

\t\tvar saveBranch = function(obj) {
\t\t\t//return \$.post('ajax/ajax_save_work_branch.php', obj);
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_save_work_branch.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: obj,
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res) {
\t\t\t\t\t\t\t\t\t\tvar msg = '<h1><font color=\"red\" ><b>' + res['barcodeok'] + '<b></font></h1>';
\t\t\t\t\t\t\t\t\t\talertify.confirm('ตรวจสอบข้อมูล','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\\\n'+ msg, 
\t\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#pass_emp\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t}, 500);
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t    alertify.success('print');
\t\t\t\t\t\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['work_main_id'];
\t\t\t\t\t\t\t\t\t\t\t\t\twindow.open(url);
\t\t\t\t\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#emp_id\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic_head\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#topic_branch\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark_head\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#remark_branch\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#tel_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_name\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#depart_receiver\").val('');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#name_receiver_select\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#branch_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#floor_send\").val(0).trigger('change');
\t\t\t\t\t\t\t\t\t\t\t\talertify.success('save');
\t\t\t\t\t\t\t\t\t\t\t\t//alertify.confirm('ตรวจสอบข้อมูล',\"This is a confirm dialog.\",
\t\t\t\t\t\t\t\t\t\t\t\t//  function(){
\t\t\t\t\t\t\t\t\t\t\t\t//\t//alertify.success('print');
\t\t\t\t\t\t\t\t\t\t\t\t//  },
\t\t\t\t\t\t\t\t\t\t\t\t//  function(){
\t\t\t\t\t\t\t\t\t\t\t\t//\t//alertify.error('OK');
\t\t\t\t\t\t\t\t\t\t\t\t//  }).set('labels', {ok:'Submit', cancel:'Cancel'});
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t}).set('labels', {ok:'ปริ้นใบปะหน้า', cancel:'ตกลง'});

\t\t\t
\t\t\t\t\t\t\t\t\t\t                                                                              
\t\t\t\t\t\t\t\t\t} 
\t\t\t\t\t}
\t\t\t\t})
\t\t\t
\t\t}
\t\t
\t\tfunction import_excel() {
\t\t\t\$('#div_error').hide();\t
\t\t\tvar formData = new FormData();
\t\t\tformData.append('file', \$('#file')[0].files[0]);
\t\t\tif(\$('#file').val() == ''){
\t\t\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\treturn;
\t\t\t}else{
\t\t\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t\t\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\t\t\$('#div_error').show();
\t\t\t\t// console.log(extension);
\t\t\t\treturn;
\t\t\t }
\t\t\t}
\t\t\t//console.log(formData);
\t\t\t\$.ajax({
\t\t\t\t   url : 'ajax/ajax_readFile_excel_work.php',
\t\t\t\t   dataType : 'json',
\t\t\t\t   type : 'POST',
\t\t\t\t   data : formData,
\t\t\t\t   processData: false,  // tell jQuery not to process the data
\t\t\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t\t\t   success : function(data) {
\t\t\t\t\t    // \$('#tb_addata').DataTable().clear().draw();
\t\t\t\t\t\t// \$('#tb_addata').DataTable().rows.add(data).draw();
\t\t\t\t\t\tvar txt_html ='';
\t\t\t\t\t\t\$.each( data, function( key, value ) {
\t\t\t\t\t\t\t txt_html += '<tr>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['no']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['type']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['name']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['tel']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['strdep']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['floor']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['doc_title']+'</td>';
\t\t\t\t\t\t\t txt_html += '<td>'+value['remark']+'</td>';
\t\t\t\t\t\t\t txt_html += '</tr>';
\t\t\t\t\t\t  });

\t\t\t\t\t\t\$('#t_data').html(txt_html);
\t\t\t\t\t\t\$('.select_floor').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t ch_fromdata()

\t\t\t\t\t\t});
\t\t\t\t\t\t\$('.select_type').select2().on('select2:select', function(e) {
\t\t\t\t\t\t\t var name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t var v_al = \$(this).val();

\t\t\t\t\t\t\t var myarr = name.split(\"_\");
\t\t\t\t\t\t\t var extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t setrowdata(v_al,extension)
\t\t\t\t\t\t\t if(v_al == 2){
\t\t\t\t\t\t\t\t  \$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t  \$('#floor_'+extension).html(\$('#branch_floor').html());
\t\t\t\t\t\t\t }else{
\t\t\t\t\t\t\t\t \$('#floor_'+extension).html('');
\t\t\t\t\t\t\t\t \$('#floor_'+extension).html(\$('#floor_send').html());
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t }
\t\t\t\t\t\t\t ch_fromdata()
\t\t\t\t\t\t\t// console.log('floor_'+extension)
\t\t\t\t\t\t});
\t\t\t\t\t\t
\t\t\t\t\t\t\$('.select_name').select2({
\t\t\t\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select_file_import.php\",
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\tvar name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\tvar v_al = \$(this).val();
\t\t\t\t\t\t\t
\t\t\t\t\t\t\tvar myarr = name.split(\"_\");
\t\t\t\t\t\t\tvar extension = myarr[(myarr.length)-1]
\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\tsetrowdata(v_al,extension)
\t\t\t\t\t\t\t //console.log(extension)
\t\t\t\t\t\t\t ch_fromdata()
\t\t\t\t\t\t});
\t\t\t\t\t\t\$('.strdep').select2({
\t\t\t\t\t\t\tplaceholder: \"ค้นหาสาขา\",
\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\turl: \"../branch/ajax/ajax_getdata_branch_select.php\",
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcache: true
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}).on('select2:select', function(e) {
\t\t\t\t\t\t\t//var name = \$(this).attr( \"name\" );
\t\t\t\t\t\t\t//var v_al = \$(this).val();
\t\t\t\t\t\t\t//var extension = name.replace(/^.*\\./, '');
\t\t\t\t\t\t\t//setrowdata(v_al,extension)
\t\t\t\t\t\t\t// //console.log(extension)
\t\t\t\t\t\t\t ch_fromdata()
\t\t\t\t\t\t});
\t\t\t\t\t  // console.log(data);
\t\t\t\t\t   from_obj = data;
\t\t\t\t\t   ch_fromdata()
\t\t\t\t\t  // alert(data);
\t\t\t\t\t   \$('#bg_loader').hide();
\t\t\t\t   }, beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\terror: function (error) {
\t\t\t\t\t\talert(\"sesstion หมดอายุ  กรุณา Login\");
\t\t\t\t\t\t//location.reload();
\t\t\t\t\t}
\t\t\t\t});
\t\t
\t\t}
\t\t
\t\tfunction ch_fromdata() {
\t\tfrom_ch=0;
\t\t\$('#div_error').hide();
\t\t\$('#div_error').html('กรุณาตรวจสอบข้อมูล !!');
\t\t\t\$.each( from_obj, function( key, value ) {
\t\t\t\tif(\$('#val_type_'+value['in']).val() == \"\" || \$('#val_type_'+value['in']).val() == null){
\t\t\t\t\t\$('#type_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#type_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#emp_'+value['in']).val() == \"\" || \$('#emp_'+value['in']).val() == null){
\t\t\t\t\t\$('#name_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#name_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#floor_'+value['in']).val() == \"\" || \$('#floor_'+value['in']).val() == null || \$('#floor_'+value['in']).val() == 0){
\t\t\t\t\t\$('#floor_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#floor_error_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\t
\t\t\t\tif(\$('#title_'+value['in']).val() == \"\" || \$('#title_'+value['in']).val() == null || \$('#title_'+value['in']).val() == 0){
\t\t\t\t\t\$('#title_'+value['in']).addClass('valit_error');
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\tfrom_ch+=1;
\t\t\t\t}else{
\t\t\t\t\t\$('#title_'+value['in']).removeClass('valit_error');
\t\t\t\t
\t\t\t\t}
\t\t\t\tif(\$('#val_type_'+value['in']).val() != 1){
\t\t\t\t\tif(\$('#strdep_'+value['in']).val() == \"\" || \$('#strdep_'+value['in']).val() == null || \$('#strdep_'+value['in']).val() == 0){
\t\t\t\t\t\t\$('#dep_error_'+value['in']).addClass('valit_error');
\t\t\t\t\t\t\$('#div_error').show();
\t\t\t\t\t\tfrom_ch+=1;
\t\t\t\t\t}else{
\t\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t}else{
\t\t\t\t\t\$('#dep_error_'+value['in']).removeClass('valit_error');
\t\t\t\t}
\t\t\t\t
\t\t\t
\t\t\t\t//console.log('<<<<<<<<<<<<<>>>>>>>>>>>>>>>>');
\t\t\t});
\t\t}
\t\tfunction start_save() {
\t\t\t\$('#div_error').hide();
\t\t\tif( from_ch > 0 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\tif( from_obj.length < 1 ){
\t\t\t\t\$('#div_error').show();
\t\t\t\t\$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล  !! ');
\t\t\t\treturn;
\t\t\t}
\t\t\tvar adll_data = \$('#form_import_excel').serializeArray()
\t\t\tvar count_data = from_obj.length;
\t\t\tdata = JSON.stringify(adll_data);
\t\t\t// console.log(adll_data);
\t\t\t// console.log(from_obj.length);
\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax/ajax_save_work_import.php?count='+count_data,
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'data_all' : data,
\t\t\t\t\t\t'csrf_token' : \$('#csrf_token').val(),
\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\tif(res.status == 401){
\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t\talert(res['message']);
\t\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t\t\t
\t\t\t\t\t\t}else{
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\t\t\talertify.confirm('บันทึกข้อมูลสำเร็จ','ท่านต้องการปริ้นใบปะหน้าซองหรือไม่', 
\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t//window.location.href=\"../branch/printcoverpage.php?maim_id=\"+res['arr_id']; 
\t\t\t\t\t\t\t\t\t\tsetTimeout(function(){ 
\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t}, 3000);
\t\t\t\t\t\t\t\t\t\tvar url =\"../branch/printcoverpage.php?maim_id=\"+res['arr_id']; 
\t\t\t\t\t\t\t\t\t\twindow.open(url,'_blank');
\t\t\t\t\t\t\t\t\t},function() {
\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'ไม่'});
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\terror: function (error) {
\t\t\t\t\t alert('เกิดข้อผิดพลาด !!!');
\t\t\t\t\t window.location.reload();
\t\t\t\t\t}
\t\t\t\t})
\t\t\t
\t\t}
\t\t\t

function dowload_excel(){
\t\talertify.confirm('Download Excel','คุณต้องการ Download Excel file', 
\t\t\t\tfunction(){
\t\t\t\t\t//window.location.href='../themes/TMBexcelimport.xlsx';
\t\t\t\t\twindow.open(\"../themes/TMBexcelimport.xlsx\", \"_blank\");
\t\t\t\t},function() {
\t\t\t}).set('labels', {ok:'ตกลง', cancel:'ยกเลิก'});

}\t\t\t
{% endblock %}
{% block Content2 %}

<div  class=\"container-fluid\">
\t\t\t<div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
\t\t\t\t <label><h3><b>บันทึกรายการนำส่ง</b></h3></label>
\t\t\t</div>\t
\t\t\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"\">
\t\t\t <input type=\"hidden\" id=\"barcode\" value=\"{{ barcode }}\">
\t\t\t
\t\t<!-- \t<div class=\"\" style=\"text-align: left;\">
\t\t\t\t <label>เลขที่เอกสาร :  -</label>
\t\t\t</div>\t -->
\t\t\t
\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
\t\t\t\t\t <h4>รายละเอียดผู้ส่ง </h4>
\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"row row justify-content-sm-center\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">สาขาผู้ส่ง :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"ชื่อสาขา\" value=\"{{ user_data_show.mr_branch_code }} - {{ user_data_show.mr_branch_name }}\" disabled>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"mr_emp_tel\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"mr_emp_tel\" placeholder=\"เบอร์โทร\" value=\"{{  user_data_show.mr_emp_tel  }}\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"div_re_select\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-right:0px\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver\">ชื่อผู้ส่ง  :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\"  value=\"{{  user_data_show.mr_emp_code  }} - {{  user_data_show.mr_emp_name  }} {{  user_data_show.mr_emp_lastname  }}\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-6\" style=\"padding-left:0px\">
\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>
\t\t
\t\t<!-- ----------------------------------------------------------------------- \tรายละเอียดผู้รับ -->

\t\t<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">
\t\t  {#<li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" id=\"home-tab\" data-toggle=\"tab\" href=\"#page_create_work\" role=\"tab\" aria-controls=\"tab_1\" aria-selected=\"true\">สร้างรายการนำส่ง</a>
\t\t  </li>#}
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link active\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#page_inport_excel\" role=\"tab\" aria-controls=\"tab_2\" aria-selected=\"false\">Import Excel</a>
\t\t  </li>
\t\t  <li class=\"nav-item\">
\t\t\t<a class=\"nav-link\" onclick=\"dowload_excel();\">Download Templates Excel</a>
\t\t  </li>
\t\t</ul>
\t<div class=\"tab-content\" id=\"myTabContent\">
\t<div id=\"page_inport_excel\" class=\"tab-pane fade show active tab-pane fade\" aria-labelledby=\"tab2-tab\" role=\"tabpanel\">
\t\t<form id=\"form_import_excel\">
\t\t\t<div class=\"form-group\">
\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"><i class=\"material-icons\">help</i></a>
\t\t\t\t<label for=\"file\">
\t\t\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t\t\t<h4>Import File </h4>  
\t\t\t\t\t</div>
\t\t\t\t</label>
\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t</div>
\t<br>
\t\t\t<button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-warning\">Upload</button>
\t\t\t<button onclick=\"start_save()\" type=\"button\" class=\"btn btn-success\">&nbsp;Save&nbsp;</button>
\t\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\"></input>

\t\t\t<br>
\t\t\t<hr>
\t\t\t<br>
\t\t\t<div id=\"div_error\"class=\"alert alert-danger\" role=\"alert\">
\t\t\t  A simple danger alert—check it out!
\t\t\t</div>
\t
\t\t
\t\t\t<div class=\"table table-responsive\">
\t\t\t<table id=\"tb_addata\" class=\"table display nowrap table-striped table-bordered\">
\t\t\t\t  <thead>
\t\t\t\t\t<tr>
\t\t\t\t\t  <th scope=\"col\">No</th>
\t\t\t\t\t  <th scope=\"col\">ประเภทการส่ง</th>
\t\t\t\t\t  <th scope=\"col\">ผู้รับงาน</th>
\t\t\t\t\t  <th scope=\"col\">ตำแหน่ง</th>
\t\t\t\t\t  <th scope=\"col\">แผนก</th>
\t\t\t\t\t  <th scope=\"col\">ชั้น</th>
\t\t\t\t\t  <th scope=\"col\">ชื่อเอกสาร</th>
\t\t\t\t\t  <th scope=\"col\">หมายเหตุ</th>
\t\t\t\t\t</tr>
\t\t\t\t  </thead>
\t\t\t\t  <tbody id=\"t_data\">
\t\t\t\t\t
\t\t\t\t  </tbody>
\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t
\t\t</form>
\t<br>
\t<br>
\t<br>
\t<br>
\t</div>\t\t

\t<div id=\"page_create_work\" class=\"tab-pane fade\" aria-labelledby=\"tab1-tab\" role=\"tabpanel\">
\t\t\t<div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
\t\t\t\t <h4>รายละเอียดผู้รับ</h4>  <a id=\"modal_click\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ตรวจสอบชื่อผู้รับตามหน่วยงาน\"><i class=\"material-icons\">help</i></a>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"\" id=\"div_re_text\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"name_receiver\">ประเภทการส่ง  :</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_1\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_1\" value=\"1\" onclick=\"check_type_send('1');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สำนักงานใหญ่
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"btn btn-primary\"for=\"type_send_2\">
\t\t\t\t\t\t\t\t<input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_2\" value=\"2\" onclick=\"check_type_send('2');\"> &nbsp;
\t\t\t\t\t\t\t\tส่งที่สาขา
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t
\t\t\t\t\t<!-- <div class=\"col-1\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name_receiver\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t</div>\t -->
\t\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t\t\t
\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"name_receiver_select\">ค้นหาผู้รับ  :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t 
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"tel_receiver\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_receiver\" placeholder=\"เบอร์โทร\" readonly>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div id=\"type_1\" style=\"display:;\">
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"depart_receiver\">แผนก : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart_receiver\" placeholder=\"ชื่อแผนก\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_name\">ชั้น : </label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor_name\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"floor_send\">ชั้นผู้รับ\t: </label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t<option value=\"0\" > เลือกชั้นปลายทาง</option>
\t\t\t\t\t\t\t\t\t{% for s in floor_data %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_floor_id }}\" > {{ s.name }}</option>
\t\t\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"topic_head\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"topic_head\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_head\">หมายเหตุ :  </label>
\t\t\t\t\t\t\t\t<textarea class=\"form-control form-control-sm\" id=\"remark_head\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t

\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div id=\"type_2\" style=\"display:none;\">
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\" >
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_receiver\">สาขา :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"branch_receiver\" placeholder=\"ชื่อสาขา\" disabled>
\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"place_receiver\">สถานที่อยู่ :</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"place_receiver\" placeholder=\"สถานที่อยู่\" disabled>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t<div class=\"\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_send\">สาขาผู้รับ : </label>
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"topic_branch\">ชื่อเอกสาร :<span style=\"color:red;\">*</span></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"topic_branch\" placeholder=\"ชื่อเอกสาร\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"branch_floor\">ชั้นผู้รับ : </label>
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
\t\t\t\t\t\t\t\t</select>\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label for=\"remark_branch\">หมายเหตุ : </label>
\t\t\t\t\t\t\t\t<textarea class=\"form-control form-control-sm\" id=\"remark_branch\" placeholder=\"หมายเหตุ\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" style=\"margin-top:50px;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-2\" style=\"padding-left:0px\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn_save\"  >บันทึกรายการ</button>

\t\t\t\t\t</div>\t\t
\t\t\t\t\t<div class=\"col-5\" style=\"padding-right:0px\">
\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t
\t\t
\t</div>
\t</div>
</div>
\t
\t


<div class=\"modal fade\" id=\"modal_showdata\">
  <div class=\"modal-dialog modal-dialog modal-xl\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\">รายชื่อติดต่อในการจัดส่ง</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t  <div style=\"overflow-x:auto;\">
        <table class=\"table\" id=\"tb_con\">
\t\t\t  <thead>
\t\t\t  
\t\t\t\t<tr>
\t\t\t\t  <th>ลำดับ</th>
\t\t\t\t  <th>ชื่อหน่วยงาน</th>
\t\t\t\t  <th>ชั้น</th>
\t\t\t\t  <th>ผู้รับงาน</th>
\t\t\t\t  <th>รหัสพนักงาน</th>
\t\t\t\t  <th>เบอร์โทร</th>
\t\t\t\t  <th>หมายเหตุ</th>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t  </thead>
\t\t\t  <tbody>
\t\t\t  {% for d in contactdata %}
\t\t\t\t<tr>
\t\t\t\t  <th scope=\"row\">{{ loop.index }}</th>
\t\t\t\t  <td>{{d.department_code}}:{{d.department_name}}</td>
\t\t\t\t  <td>{{d.floor}}</td>
\t\t\t\t  <td>{{d.mr_contact_name}}</td>
\t\t\t\t  <td>{{d.emp_code}}</td>
\t\t\t\t  <td>{{d.emp_tel}}</td>
\t\t\t\t  <td>{{d.remark }}</td>
\t\t\t\t</tr>
\t\t\t\t{% endfor %}
\t\t\t  </tbody>
\t\t\t</table>
\t\t</div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>


<div id=\"click_new_tab\">
</div>
<div id=\"bg_loader\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/create_work_import_excel.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\create_work_import_excel.tpl");
    }
}

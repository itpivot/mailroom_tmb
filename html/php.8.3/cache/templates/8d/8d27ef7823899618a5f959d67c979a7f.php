<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/index.tpl */
class __TwigTemplate_5cd40624c0fc3050611c086dcfc40328 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_1' => [$this, 'block_menu_1'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/index.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "
.card_ {
   // padding-top: 20px;
  //  margin: 10px 0 20px 0;
   // background-color: rgba(214, 224, 226, 0.2);
    border-top-width: 0;
    border-bottom-width: 2px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
\tmin-height:100%;
}

.card_ .card_-heading {
    padding: 0 20px;
    margin: 0;
}

.card_ .card_-heading.simple {
    font-size: 20px;
    font-weight: 300;
    color: #777;
    //border-bottom: 1px solid #e5e5e5;
}

.card_ .card_-heading.image img {
    display: inline-block;
    width: 46px;
    height: 46px;
    margin-right: 15px;
    vertical-align: top;
    border: 0;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}

.card_ .card_-heading.image .card_-heading-header {
    display: inline-block;
    vertical-align: top;
}

.card_ .card_-heading.image .card_-heading-header h3 {
    margin: 0;
    font-size: 14px;
    line-height: 16px;
    color: #262626;
}

.card_ .card_-heading.image .card_-heading-header span {
    font-size: 12px;
    color: #999999;
}

.card_ .card_-body {
    padding: 0 20px;
    margin-top: 20px;
}

.card_ .card_-media {
    padding: 0 20px;
    margin: 0 -14px;
}

.card_ .card_-media img {
    max-width: 100%;
    max-height: 100%;
}

.card_ .card_-actions {
    min-height: 30px;
    padding: 0 20px 20px 20px;
    margin: 20px 0 0 0;
}

.card_ .card_-comments {
    padding: 20px;
    margin: 0;
   // background-color: #f8f8f8;
}

.card_ .card_-comments .comments-collapse-toggle {
    padding: 0;
    margin: 0 20px 12px 20px;
}

.card_ .card_-comments .comments-collapse-toggle a,
.card_ .card_-comments .comments-collapse-toggle span {
    padding-right: 5px;
    overflow: hidden;
    font-size: 12px;
    color: #999;
    text-overflow: ellipsis;
    white-space: nowrap;
}

.card_-comments .media-heading {
    font-size: 13px;
    font-weight: bold;
}

.card_.people {
    position: relative;
    display: inline-block;
    width: 170px;
    height: 300px;
    padding-top: 0;
    margin-left: 20px;
    overflow: hidden;
    vertical-align: top;
}

.card_.people:first-child {
    margin-left: 0;
}

.card_.people .card_-top {
    position: absolute;
    top: 0;
    left: 0;
    display: inline-block;
    width: 170px;
    height: 150px;
    background-color: #ffffff;
}

.card_.people .card_-top.green {
    background-color: #53a93f;
}

.card_.people .card_-top.blue {
    background-color: #427fed;
}

.card_.people .card_-info {
    position: absolute;
    top: 150px;
    display: inline-block;
    width: 100%;
    height: 101px;
    overflow: hidden;
    background: #ffffff;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card_.people .card_-info .title {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 16px;
    font-weight: bold;
    line-height: 18px;
    color: #404040;
}

.card_.people .card_-info .desc {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 12px;
    line-height: 16px;
    color: #737373;
    text-overflow: ellipsis;
}

.card_.people .card_-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    display: inline-block;
    width: 100%;
    padding: 10px 20px;
    line-height: 29px;
    text-align: center;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card_.hovercard {
    position: relative;
    padding-top: 0;
    overflow: hidden;
    text-align: center;
   // background-color: rgba(214, 224, 226, 0.2);
}

.card_.hovercard .cardheader {
    //background: url(\"../themes/images/Artboard4.png\");
    background-size: cover;
\tbackground-color:#0074c0;
    height: 60px;
\tborder-bottom-left-radius: 90px 50px;
\tborder-bottom-right-radius: 90px 50px;
\tborder-top-right-radius: 0px 0px;
\tborder-top-left-radius: 0px 0px;
}

.card_.hovercard .avatar {
    position: relative;
    top: -50px;
    margin-bottom: -50px;
\t
}

.card_.hovercard .avatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255,255,255,0.5);
}

.card_.hovercard .info {
    padding: 4px 8px 10px;
}

.card_.hovercard .info .title {
    margin-bottom: 4px;
    font-size: 24px;
    line-height: 1;
    color: #262626;
    vertical-align: middle;
}

.card_.hovercard .info .desc {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}

.card_.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
}
.btn_login{
 background-color:#0074c0;
}.btn_login:hover{
 background-color:#055d97;
}
.btn_logouts{
\tbackground-color:#6d6d6d;
}.btn_logouts:hover{
\tbackground-color:#4d4d4d;
}
";
    }

    // line 266
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 267
        echo "\$('#name_emp').select2({
\t\t\t\tdropdownParent: \$('#editCon'),
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\t\$.ajax({
\t\t\t  dataType: \"json\",
\t\t\t  method: \"POST\",
\t\t\t  url: \"ajax/ajax_getdataemployee.php\",
\t\t\t  data: { employee_id : \$('#name_emp').val() }
\t\t\t}).done(function(data1){
\t\t\t\t//alert('55555');
\t\t\t\t\$('#con_emp_tel').val(data1['mr_emp_tel']);
\t\t\t\t\$('#mr_emp_code').val(data1['mr_emp_code']);
\t\t\t\t\$('#con_emp_name').val(data1['mr_emp_name']+'  '+data1['mr_emp_lastname']);
\t\t\t\t//console.log(data1);
\t\t\t\t//console.log(\$('#name_emp').val());
\t\t\t});
\t\t\t
\t\t});
\t\t
\$( \"#btn_save\" ).click(function() {
\t\t\t\t\tvar con_remark \t\t\t= \$('#con_remark').val();
\t\t\t\t\tvar con_floor \t\t\t= \$('#con_floor').val();
\t\t\t\t\tvar con_department_name \t\t\t= \$('#con_department_name').val();
\t\t\t\t\tvar mr_emp_tel \t\t\t= \$('#con_emp_tel').val();
\t\t\t\t\tvar con_emp_name \t\t= \$('#con_emp_name').val();
\t\t\t\t\tvar mr_emp_code \t\t= \$('#mr_emp_code').val();
\t\t\t\t\tvar mr_emp_id \t\t\t= \$('#name_emp').val();
\t\t\t\t\tvar department_code \t\t= \$('#department_code').val();
\t\t\t\t\tvar mr_contact_id \t\t= \$('#mr_contact_id').val();
\t\t\t\t\tvar type_add \t\t= \$('#type_add').val();
\t\t\t\t\t
\t\t\t\t\t\$( \"#mr_emp_tel\" ).removeClass( \"is-invalid\" );
\t\t\t\t\t\$( \"#mr_emp_code\" ).removeClass( \"is-invalid\" );
\t\t\t\t\t\$( \"#name_emp\" ).removeClass( \"is-invalid\" );
\t\t\t\t\t
\t\t\t\t\tif(mr_emp_id == ''){
\t\t\t\t\t\t\$( \"#name_emp\" ).addClass( \"is-invalid\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_code == ''){
\t\t\t\t\t\t\$( \"#mr_emp_code\" ).addClass( \"is-invalid\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_tel == ''){
\t\t\t\t\t\t\$( \"#mr_emp_tel\" ).addClass( \"is-invalid\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t\t
\talertify.confirm(\"ตรวจสอบข้อมูล\", \"กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก\",
\t\t\tfunction(){
\t\t\t\t\$.ajax({
\t\t\t\t\t\t  dataType: \"json\",
\t\t\t\t\t\t  method: \"POST\",
\t\t\t\t\t\t  url: \"ajax/ajax_save_data_contact.php\",
\t\t\t\t\t\t  data: { 
\t\t\t\t\t\t\t\tdepartment_code \t\t\t \t : department_code \t\t\t \t,
\t\t\t\t\t\t\t\tcon_emp_name \t\t\t \t : con_emp_name \t\t\t \t,
\t\t\t\t\t\t\t\tcon_remark \t\t\t \t : con_remark \t\t\t \t,
\t\t\t\t\t\t\t\tcon_floor \t\t\t \t : con_floor \t\t\t \t,
\t\t\t\t\t\t\t\tcon_department_name \t : con_department_name \t,
\t\t\t\t\t\t\t\ttype_add \t : type_add \t,
\t\t\t\t\t\t\t\tmr_emp_tel \t : mr_emp_tel \t,
\t\t\t\t\t\t\t\tmr_emp_code  : mr_emp_code ,
\t\t\t\t\t\t\t\tmr_emp_id \t : mr_emp_id \t,
\t\t\t\t\t\t\t\tmr_contact_id: mr_contact_id
\t\t\t\t\t\t  }
\t\t\t\t\t\t}).done(function(data) {
\t\t\t\t\t\t\t
\t\t\t\t\t\t\talertify.alert(data['st'],data['msg'],
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t  },function(){
\t\t\t\t\talertify.error('Cancel');
\t  });
\t
\t
\t//alert( \"Handler for .click() called.\" );
\t//is-invalid
});
";
    }

    // line 363
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 364
        echo "
function load_modal(mr_contact_id,type) {\t

 \$('#editCon').modal('show');

\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_data_contact.php\",
\t  data: { \t
\t\t\t\tmr_contact_id : mr_contact_id,
\t\t\t\tpage : 'getdata'
\t  }
\t}).done(function(data) {
\t\t//alert('55555');
\t\t\$('#con_emp_tel').val(data['emp_tel']);
\t\tif(type!= \"add\"){
\t\t\t\$('#mr_contact_id').val(data['mr_contact_id']);
\t\t\t
\t\t}
\t\t\$('#type_add').val(type);
\t\t\$('#mr_emp_code').val(data['emp_code']);
\t\t\$('#con_department_name').val(data['department_name']);
\t\t\$('#con_floor').val(data['floor']);
\t\t\$('#con_remark').val(data['remark']);
\t\t\$('#department_code').val(data['department_code']);
\t});
\t
}


";
    }

    // line 400
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t<div class=\"row\">
\t\t<div class=\"col-lg-12 col-sm-12\">

            <div class=\"card_ hovercard\">
                <div class=\"cardheader\">

                </div>
                <div class=\"avatar\">
                    <img alt=\"\" src=\"../themes/images/Asset1.png\">
                </div>
                <div class=\"info\">
\t\t\t\t<br>
\t\t\t\t<br>
                    <div class=\"title\">
                        <h3 style=\"color:#0074c0;\">ยินดีต้อนรับ</h3>
                    </div>
                    <div class=\"desc\"><h5>รหัส ";
        // line 418
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 418), "html", null, true);
        echo " :  ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 418), "mr_emp_name", [], "any", false, false, false, 418), "html", null, true);
        echo "  ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 418), "mr_emp_lastname", [], "any", false, false, false, 418), "html", null, true);
        echo "</h5></div>
\t\t\t\t\t<div class=\"desc\" style=\"color:red;\"><b><h5><p>สถานที่ปฎิบัติงาน  : 
\t\t\t\t\t ";
        // line 420
        if ((($context["role_id"] ?? null) == 5)) {
            echo " 
\t\t\t\t\t \t\" สาขา/สาขาและอาคารอื่นๆ\"  
\t\t\t\t\t ";
        } else {
            // line 422
            echo " 
\t\t\t\t\t \t\"สำนักงาน(พหลโยธิน)\"  
\t\t\t\t\t ";
        }
        // line 424
        echo "</p></h5></b></div>
\t\t\t\t\t
\t\t\t\t  <!--  
\t\t\t\t\t<div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div> 
\t\t\t\t\t-->
                    <div class=\"desc\"><h5>สาขา :  ";
        // line 436
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 436), "mr_branch_code", [], "any", false, false, false, 436), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 436), "mr_branch_name", [], "any", false, false, false, 436), "html", null, true);
        echo "</b></h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>ชั้น : ";
        // line 437
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 437), "mr_branch_floor", [], "any", false, false, false, 437), "html", null, true);
        echo "</h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>เบอร์โทรศัพท์ : ";
        // line 438
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 438), "mr_emp_tel", [], "any", false, false, false, 438), "html", null, true);
        echo "</h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>เบอร์มือถือ :  ";
        // line 439
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 439), "mr_emp_mobile", [], "any", false, false, false, 439), "html", null, true);
        echo "</h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>Email :  ";
        // line 440
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 440), "mr_emp_email", [], "any", false, false, false, 440), "html", null, true);
        echo "</h5></div>
\t\t\t\t\t\t
\t\t\t\t\t\t <div class=\"desc\"><h5><b>
\t\t\t\t\t\t\t\t\t<a class=\"btn btn-link\" style=\"text-decoration: underline;\" href=\"profile.php\">
\t\t\t\t\t\t\t\t\t\tแก้ไขข้อมูลส่วนตัว
\t\t\t\t\t\t\t\t\t</a></b></h5></div>
\t\t\t\t\t\t";
        // line 446
        if ((twig_length_filter($this->env, ($context["con_data"] ?? null)) > 0)) {
            // line 447
            echo "\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<h4>ข้อมูลหน่วยงานที่อยู่ในความรับผิดชอบรับเอกสาร</h4><br>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<table class=\"table table-striped\" align=\"center\" border=\"1\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">#</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">พนักงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">รหัสหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">ชื่อหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">จัดการ</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 459
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["con_data"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 460
                echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 461
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 461), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 462
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "emp_code", [], "any", false, false, false, 462), "html", null, true);
                echo "  ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_contact_name", [], "any", false, false, false, 462), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 463
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "department_code", [], "any", false, false, false, 463), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t   ";
                // line 465
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "department_name", [], "any", false, false, false, 465), "html", null, true);
                echo " [";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "remark", [], "any", false, false, false, 465), "html", null, true);
                echo "]   
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<button ";
                // line 468
                if ((twig_get_attribute($this->env, $this->source, $context["c"], "emp_code", [], "any", false, false, false, 468) != twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["emp_data"] ?? null), 0, [], "any", false, false, false, 468), "mr_emp_code", [], "any", false, false, false, 468))) {
                    echo " disabled ";
                }
                // line 469
                echo "\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_contact_id", [], "any", false, false, false, 469), "html", null, true);
                echo "',null);\" class=\"btn btn-link\" style=\"text-decoration: underline;\">
\t\t\t\t\t\t\t\t\t\t\t\tแก้ไขข้อมูล <i class=\"material-icons\">create</i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-link\"
\t\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('";
                // line 473
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_contact_id", [], "any", false, false, false, 473), "html", null, true);
                echo "','add');\">
\t\t\t\t\t\t\t\t\t\t\t\tเพิ่ม <i class=\"material-icons\">add_circle</i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 480
            echo "\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t";
        }
        // line 482
        echo "                </div>
               
            </div>

        </div>
\t\t

\t</div>
\t<div class=\"row justify-content-center justify-content-sm-center justify-content-md-center\tjustify-content-lg-center justify-content-xl-center\">
\t\t<!-- .col-\t.col-sm-\t.col-md-\t.col-lg-\t.col-xl- -->
\t\t<div class=\"col-9 col-sm-6 col-md-5 col-lg-5 col-xl-4\">
\t\t\t\t 
\t\t\t\t\t\t
\t\t\t\t\t\t

\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-lg-12 col-sm-12\">
\t\t\t\t<div class=\"bottom\">
\t\t\t\t\t";
        // line 505
        echo "\t\t\t\t </div>
\t\t\t</div>
\t</div>
\t


<!-- Modal -->
<div class=\"modal fade\" id=\"editCon\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อ - นามสกุล</label><br>
\t\t\t\t <select class=\"form-control\" id=\"name_emp\" style=\"width:100%;\">
\t\t\t\t  <option value=\"\">";
        // line 525
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["con_data"] ?? null), 0, [], "any", false, false, false, 525), "emp_code", [], "any", false, false, false, 525), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["con_data"] ?? null), 0, [], "any", false, false, false, 525), "mr_contact_name", [], "any", false, false, false, 525), "html", null, true);
        echo "</option>
\t\t\t\t  ";
        // line 526
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["all_emp"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["emp"]) {
            // line 527
            echo "\t\t\t\t  <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_id", [], "any", false, false, false, 527), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_code", [], "any", false, false, false, 527), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_name", [], "any", false, false, false, 527), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_lastname", [], "any", false, false, false, 527), "html", null, true);
            echo "</option>
\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['emp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 529
        echo "\t\t\t\t</select>
\t\t\t  </div>
\t\t\t 
\t\t\t  
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">เบอร์โทร</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_emp_tel\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"type_add\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_contact_id\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_emp_code\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"con_emp_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">รหัสหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"department_code\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_department_name\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชั้น</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_floor\" value=\"\"  readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">หมายเหตุ</label>
\t\t\t\t <textarea class=\"form-control\" id=\"con_remark\" rows=\"3\"></textarea >
\t\t\t  </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
        <button type=\"button\" id=\"btn_save\" class=\"btn btn-primary\">บันทึก</button>
      </div>
    </div>
  </div>
</div>
\t
";
    }

    // line 567
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 569
        if ((($context["debug"] ?? null) != "")) {
            // line 570
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 572
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  777 => 572,  771 => 570,  769 => 569,  762 => 567,  721 => 529,  707 => 527,  703 => 526,  697 => 525,  675 => 505,  653 => 482,  649 => 480,  628 => 473,  620 => 469,  616 => 468,  608 => 465,  603 => 463,  597 => 462,  593 => 461,  590 => 460,  573 => 459,  559 => 447,  557 => 446,  548 => 440,  544 => 439,  540 => 438,  536 => 437,  530 => 436,  516 => 424,  511 => 422,  505 => 420,  496 => 418,  473 => 400,  438 => 364,  434 => 363,  336 => 267,  332 => 266,  70 => 7,  66 => 6,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_1 %} active {% endblock %}
{% block styleReady %}

.card_ {
   // padding-top: 20px;
  //  margin: 10px 0 20px 0;
   // background-color: rgba(214, 224, 226, 0.2);
    border-top-width: 0;
    border-bottom-width: 2px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
\tmin-height:100%;
}

.card_ .card_-heading {
    padding: 0 20px;
    margin: 0;
}

.card_ .card_-heading.simple {
    font-size: 20px;
    font-weight: 300;
    color: #777;
    //border-bottom: 1px solid #e5e5e5;
}

.card_ .card_-heading.image img {
    display: inline-block;
    width: 46px;
    height: 46px;
    margin-right: 15px;
    vertical-align: top;
    border: 0;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}

.card_ .card_-heading.image .card_-heading-header {
    display: inline-block;
    vertical-align: top;
}

.card_ .card_-heading.image .card_-heading-header h3 {
    margin: 0;
    font-size: 14px;
    line-height: 16px;
    color: #262626;
}

.card_ .card_-heading.image .card_-heading-header span {
    font-size: 12px;
    color: #999999;
}

.card_ .card_-body {
    padding: 0 20px;
    margin-top: 20px;
}

.card_ .card_-media {
    padding: 0 20px;
    margin: 0 -14px;
}

.card_ .card_-media img {
    max-width: 100%;
    max-height: 100%;
}

.card_ .card_-actions {
    min-height: 30px;
    padding: 0 20px 20px 20px;
    margin: 20px 0 0 0;
}

.card_ .card_-comments {
    padding: 20px;
    margin: 0;
   // background-color: #f8f8f8;
}

.card_ .card_-comments .comments-collapse-toggle {
    padding: 0;
    margin: 0 20px 12px 20px;
}

.card_ .card_-comments .comments-collapse-toggle a,
.card_ .card_-comments .comments-collapse-toggle span {
    padding-right: 5px;
    overflow: hidden;
    font-size: 12px;
    color: #999;
    text-overflow: ellipsis;
    white-space: nowrap;
}

.card_-comments .media-heading {
    font-size: 13px;
    font-weight: bold;
}

.card_.people {
    position: relative;
    display: inline-block;
    width: 170px;
    height: 300px;
    padding-top: 0;
    margin-left: 20px;
    overflow: hidden;
    vertical-align: top;
}

.card_.people:first-child {
    margin-left: 0;
}

.card_.people .card_-top {
    position: absolute;
    top: 0;
    left: 0;
    display: inline-block;
    width: 170px;
    height: 150px;
    background-color: #ffffff;
}

.card_.people .card_-top.green {
    background-color: #53a93f;
}

.card_.people .card_-top.blue {
    background-color: #427fed;
}

.card_.people .card_-info {
    position: absolute;
    top: 150px;
    display: inline-block;
    width: 100%;
    height: 101px;
    overflow: hidden;
    background: #ffffff;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card_.people .card_-info .title {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 16px;
    font-weight: bold;
    line-height: 18px;
    color: #404040;
}

.card_.people .card_-info .desc {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 12px;
    line-height: 16px;
    color: #737373;
    text-overflow: ellipsis;
}

.card_.people .card_-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    display: inline-block;
    width: 100%;
    padding: 10px 20px;
    line-height: 29px;
    text-align: center;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card_.hovercard {
    position: relative;
    padding-top: 0;
    overflow: hidden;
    text-align: center;
   // background-color: rgba(214, 224, 226, 0.2);
}

.card_.hovercard .cardheader {
    //background: url(\"../themes/images/Artboard4.png\");
    background-size: cover;
\tbackground-color:#0074c0;
    height: 60px;
\tborder-bottom-left-radius: 90px 50px;
\tborder-bottom-right-radius: 90px 50px;
\tborder-top-right-radius: 0px 0px;
\tborder-top-left-radius: 0px 0px;
}

.card_.hovercard .avatar {
    position: relative;
    top: -50px;
    margin-bottom: -50px;
\t
}

.card_.hovercard .avatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255,255,255,0.5);
}

.card_.hovercard .info {
    padding: 4px 8px 10px;
}

.card_.hovercard .info .title {
    margin-bottom: 4px;
    font-size: 24px;
    line-height: 1;
    color: #262626;
    vertical-align: middle;
}

.card_.hovercard .info .desc {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}

.card_.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
}
.btn_login{
 background-color:#0074c0;
}.btn_login:hover{
 background-color:#055d97;
}
.btn_logouts{
\tbackground-color:#6d6d6d;
}.btn_logouts:hover{
\tbackground-color:#4d4d4d;
}
{% endblock %}
{% block domReady %}
\$('#name_emp').select2({
\t\t\t\tdropdownParent: \$('#editCon'),
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\t\$.ajax({
\t\t\t  dataType: \"json\",
\t\t\t  method: \"POST\",
\t\t\t  url: \"ajax/ajax_getdataemployee.php\",
\t\t\t  data: { employee_id : \$('#name_emp').val() }
\t\t\t}).done(function(data1){
\t\t\t\t//alert('55555');
\t\t\t\t\$('#con_emp_tel').val(data1['mr_emp_tel']);
\t\t\t\t\$('#mr_emp_code').val(data1['mr_emp_code']);
\t\t\t\t\$('#con_emp_name').val(data1['mr_emp_name']+'  '+data1['mr_emp_lastname']);
\t\t\t\t//console.log(data1);
\t\t\t\t//console.log(\$('#name_emp').val());
\t\t\t});
\t\t\t
\t\t});
\t\t
\$( \"#btn_save\" ).click(function() {
\t\t\t\t\tvar con_remark \t\t\t= \$('#con_remark').val();
\t\t\t\t\tvar con_floor \t\t\t= \$('#con_floor').val();
\t\t\t\t\tvar con_department_name \t\t\t= \$('#con_department_name').val();
\t\t\t\t\tvar mr_emp_tel \t\t\t= \$('#con_emp_tel').val();
\t\t\t\t\tvar con_emp_name \t\t= \$('#con_emp_name').val();
\t\t\t\t\tvar mr_emp_code \t\t= \$('#mr_emp_code').val();
\t\t\t\t\tvar mr_emp_id \t\t\t= \$('#name_emp').val();
\t\t\t\t\tvar department_code \t\t= \$('#department_code').val();
\t\t\t\t\tvar mr_contact_id \t\t= \$('#mr_contact_id').val();
\t\t\t\t\tvar type_add \t\t= \$('#type_add').val();
\t\t\t\t\t
\t\t\t\t\t\$( \"#mr_emp_tel\" ).removeClass( \"is-invalid\" );
\t\t\t\t\t\$( \"#mr_emp_code\" ).removeClass( \"is-invalid\" );
\t\t\t\t\t\$( \"#name_emp\" ).removeClass( \"is-invalid\" );
\t\t\t\t\t
\t\t\t\t\tif(mr_emp_id == ''){
\t\t\t\t\t\t\$( \"#name_emp\" ).addClass( \"is-invalid\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_code == ''){
\t\t\t\t\t\t\$( \"#mr_emp_code\" ).addClass( \"is-invalid\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_tel == ''){
\t\t\t\t\t\t\$( \"#mr_emp_tel\" ).addClass( \"is-invalid\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t\t
\talertify.confirm(\"ตรวจสอบข้อมูล\", \"กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก\",
\t\t\tfunction(){
\t\t\t\t\$.ajax({
\t\t\t\t\t\t  dataType: \"json\",
\t\t\t\t\t\t  method: \"POST\",
\t\t\t\t\t\t  url: \"ajax/ajax_save_data_contact.php\",
\t\t\t\t\t\t  data: { 
\t\t\t\t\t\t\t\tdepartment_code \t\t\t \t : department_code \t\t\t \t,
\t\t\t\t\t\t\t\tcon_emp_name \t\t\t \t : con_emp_name \t\t\t \t,
\t\t\t\t\t\t\t\tcon_remark \t\t\t \t : con_remark \t\t\t \t,
\t\t\t\t\t\t\t\tcon_floor \t\t\t \t : con_floor \t\t\t \t,
\t\t\t\t\t\t\t\tcon_department_name \t : con_department_name \t,
\t\t\t\t\t\t\t\ttype_add \t : type_add \t,
\t\t\t\t\t\t\t\tmr_emp_tel \t : mr_emp_tel \t,
\t\t\t\t\t\t\t\tmr_emp_code  : mr_emp_code ,
\t\t\t\t\t\t\t\tmr_emp_id \t : mr_emp_id \t,
\t\t\t\t\t\t\t\tmr_contact_id: mr_contact_id
\t\t\t\t\t\t  }
\t\t\t\t\t\t}).done(function(data) {
\t\t\t\t\t\t\t
\t\t\t\t\t\t\talertify.alert(data['st'],data['msg'],
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t  },function(){
\t\t\t\t\talertify.error('Cancel');
\t  });
\t
\t
\t//alert( \"Handler for .click() called.\" );
\t//is-invalid
});
{% endblock %}

{% block javaScript %}

function load_modal(mr_contact_id,type) {\t

 \$('#editCon').modal('show');

\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_data_contact.php\",
\t  data: { \t
\t\t\t\tmr_contact_id : mr_contact_id,
\t\t\t\tpage : 'getdata'
\t  }
\t}).done(function(data) {
\t\t//alert('55555');
\t\t\$('#con_emp_tel').val(data['emp_tel']);
\t\tif(type!= \"add\"){
\t\t\t\$('#mr_contact_id').val(data['mr_contact_id']);
\t\t\t
\t\t}
\t\t\$('#type_add').val(type);
\t\t\$('#mr_emp_code').val(data['emp_code']);
\t\t\$('#con_department_name').val(data['department_name']);
\t\t\$('#con_floor').val(data['floor']);
\t\t\$('#con_remark').val(data['remark']);
\t\t\$('#department_code').val(data['department_code']);
\t});
\t
}


{% endblock %}


\t\t
\t\t
{% block Content %} 

\t<div class=\"row\">
\t\t<div class=\"col-lg-12 col-sm-12\">

            <div class=\"card_ hovercard\">
                <div class=\"cardheader\">

                </div>
                <div class=\"avatar\">
                    <img alt=\"\" src=\"../themes/images/Asset1.png\">
                </div>
                <div class=\"info\">
\t\t\t\t<br>
\t\t\t\t<br>
                    <div class=\"title\">
                        <h3 style=\"color:#0074c0;\">ยินดีต้อนรับ</h3>
                    </div>
                    <div class=\"desc\"><h5>รหัส {{ user_data.mr_user_username }} :  {{emp_data.0.mr_emp_name}}  {{emp_data.0.mr_emp_lastname}}</h5></div>
\t\t\t\t\t<div class=\"desc\" style=\"color:red;\"><b><h5><p>สถานที่ปฎิบัติงาน  : 
\t\t\t\t\t {% if role_id == 5 %} 
\t\t\t\t\t \t\" สาขา/สาขาและอาคารอื่นๆ\"  
\t\t\t\t\t {% else %} 
\t\t\t\t\t \t\"สำนักงาน(พหลโยธิน)\"  
\t\t\t\t\t {% endif %}</p></h5></b></div>
\t\t\t\t\t
\t\t\t\t  <!--  
\t\t\t\t\t<div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class=\"desc\"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div> 
\t\t\t\t\t-->
                    <div class=\"desc\"><h5>สาขา :  {{emp_data.0.mr_branch_code}}-{{emp_data.0.mr_branch_name}}</b></h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>ชั้น : {{emp_data.0.mr_branch_floor}}</h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>เบอร์โทรศัพท์ : {{emp_data.0.mr_emp_tel}}</h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>เบอร์มือถือ :  {{emp_data.0.mr_emp_mobile}}</h5></div>
\t\t\t\t\t\t <div class=\"desc\"><h5>Email :  {{emp_data.0.mr_emp_email }}</h5></div>
\t\t\t\t\t\t
\t\t\t\t\t\t <div class=\"desc\"><h5><b>
\t\t\t\t\t\t\t\t\t<a class=\"btn btn-link\" style=\"text-decoration: underline;\" href=\"profile.php\">
\t\t\t\t\t\t\t\t\t\tแก้ไขข้อมูลส่วนตัว
\t\t\t\t\t\t\t\t\t</a></b></h5></div>
\t\t\t\t\t\t{% if con_data|length > 0 %}
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<h4>ข้อมูลหน่วยงานที่อยู่ในความรับผิดชอบรับเอกสาร</h4><br>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<table class=\"table table-striped\" align=\"center\" border=\"1\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">#</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">พนักงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">รหัสหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">ชื่อหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">จัดการ</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t{% for c in con_data %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>{{ loop.index }}</td>
\t\t\t\t\t\t\t\t\t\t<td>{{ c.emp_code }}  {{ c.mr_contact_name }}</td>
\t\t\t\t\t\t\t\t\t\t<td>{{ c.department_code }}</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t   {{         c.department_name  }} [{{         c.remark  }}]   
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<button {% if c.emp_code  != emp_data.0.mr_emp_code %} disabled {% endif %}
\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('{{c.mr_contact_id}}',null);\" class=\"btn btn-link\" style=\"text-decoration: underline;\">
\t\t\t\t\t\t\t\t\t\t\t\tแก้ไขข้อมูล <i class=\"material-icons\">create</i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-link\"
\t\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('{{c.mr_contact_id}}','add');\">
\t\t\t\t\t\t\t\t\t\t\t\tเพิ่ม <i class=\"material-icons\">add_circle</i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t{% endif %}
                </div>
               
            </div>

        </div>
\t\t

\t</div>
\t<div class=\"row justify-content-center justify-content-sm-center justify-content-md-center\tjustify-content-lg-center justify-content-xl-center\">
\t\t<!-- .col-\t.col-sm-\t.col-md-\t.col-lg-\t.col-xl- -->
\t\t<div class=\"col-9 col-sm-6 col-md-5 col-lg-5 col-xl-4\">
\t\t\t\t 
\t\t\t\t\t\t
\t\t\t\t\t\t

\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-lg-12 col-sm-12\">
\t\t\t\t<div class=\"bottom\">
\t\t\t\t\t{# <a class=\"btn_logouts btn btn-primary\" rel=\"\" href=\"https://plus.google.com/shahnuralam\">
\t\t\t\t\t\t\tออกจากระบบ
\t\t\t\t\t</a>#}
\t\t\t\t </div>
\t\t\t</div>
\t</div>
\t


<!-- Modal -->
<div class=\"modal fade\" id=\"editCon\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อ - นามสกุล</label><br>
\t\t\t\t <select class=\"form-control\" id=\"name_emp\" style=\"width:100%;\">
\t\t\t\t  <option value=\"\">{{con_data.0.emp_code}} : {{con_data.0.mr_contact_name}}</option>
\t\t\t\t  {% for emp in all_emp %}
\t\t\t\t  <option value=\"{{ emp.mr_emp_id }}\">{{emp.mr_emp_code}} : {{emp.mr_emp_name}}{{emp.mr_emp_lastname}}</option>
\t\t\t\t  {% endfor %}
\t\t\t\t</select>
\t\t\t  </div>
\t\t\t 
\t\t\t  
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">เบอร์โทร</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_emp_tel\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"type_add\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_contact_id\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_emp_code\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"con_emp_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">รหัสหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"department_code\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_department_name\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชั้น</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_floor\" value=\"\"  readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">หมายเหตุ</label>
\t\t\t\t <textarea class=\"form-control\" id=\"con_remark\" rows=\"3\"></textarea >
\t\t\t  </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
        <button type=\"button\" id=\"btn_save\" class=\"btn btn-primary\">บันทึก</button>
      </div>
    </div>
  </div>
</div>
\t
{% endblock %}
{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "branch/index.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\index.tpl");
    }
}

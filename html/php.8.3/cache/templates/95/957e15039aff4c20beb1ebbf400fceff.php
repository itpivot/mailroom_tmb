<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* employee/profile_check.tpl */
class __TwigTemplate_c133dd23fb13a4c2032561e814490e7a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "employee/profile_check.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "\t";
        if ((($context["type_work"] ?? null) == "")) {
            // line 9
            echo "\t\talertify.alert('เกิดข้อผิดพลาดไม่พลข้อมูลประเภทการส่ง!!',function(){
\t\t\tlocation.href = \"../data/profile.php\";
\t\t});
\t\t
\t";
        }
        // line 13
        echo "\t
\t
";
    }

    // line 16
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "\t\$(\"#next\").click(function(){
\t\tvar floor \t\t\t= \$(\"#floor\").val();
\t\tif( floor == \"\" || floor == null){
\t\t\t\t\talertify.alert('กรุณาใส่ชั้นของคุณให้ถูกต้อง!!');
\t\t\t\t\tlocation.href = \"../data/profile.php\";
\t\t}else{
\t\t\t";
        // line 23
        if ((($context["type_work"] ?? null) == 1)) {
            // line 24
            echo "\t\t\t\tlocation.href = \"../employee/work_in.php\";
\t\t\t";
        } elseif ((        // line 25
($context["type_work"] ?? null) == 2)) {
            // line 26
            echo "\t\t\t    location.href = \"../employee/work_out.php\";
\t\t\t";
        }
        // line 28
        echo "\t\t}
\t
\t})
\t
\t
\t\t\t\t
";
    }

    // line 36
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "
\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้สั่งงาน ";
        // line 41
        if ((($context["type_work"] ?? null) == 1)) {
            echo " (ส่งที่สำนักงานใหญ่) ";
        } else {
            echo " (ส่งที่สาขา)  ";
        }
        echo "</h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 43), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_id", [], "any", false, false, false, 44), "html", null, true);
        echo "\">
\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_code", [], "any", false, false, false, 52), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_name", [], "any", false, false, false, 63), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"";
        // line 74
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_lastname", [], "any", false, false, false, 74), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"";
        // line 84
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_tel", [], "any", false, false, false, 84), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"";
        // line 94
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_mobile", [], "any", false, false, false, 94), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"";
        // line 104
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_email", [], "any", false, false, false, 104), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tแผนก :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\" disabled>
\t\t\t\t\t\t\t";
        // line 115
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 116
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 116), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 116) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_id", [], "any", false, false, false, 116))) {
                echo " selected=\"selected\" ";
            }
            echo " >";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_code", [], "any", false, false, false, 116), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_name", [], "any", false, false, false, 116), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor\" style=\"width:100%;\" disabled>
\t\t\t\t\t\t\t<option value=\"\"> ไม่มีข้อมูลชั้น</option>
\t\t\t\t\t\t\t";
        // line 130
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
            // line 131
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 131), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 131) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "floor_emp", [], "any", false, false, false, 131))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "name", [], "any", false, false, false, 131), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"next\">Next </button>
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

";
    }

    // line 151
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 153
        if ((($context["debug"] ?? null) != "")) {
            // line 154
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 156
        echo "
";
    }

    public function getTemplateName()
    {
        return "employee/profile_check.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  323 => 156,  317 => 154,  315 => 153,  308 => 151,  288 => 132,  273 => 131,  269 => 130,  254 => 117,  237 => 116,  233 => 115,  219 => 104,  206 => 94,  193 => 84,  180 => 74,  166 => 63,  152 => 52,  141 => 44,  137 => 43,  128 => 41,  122 => 37,  118 => 36,  108 => 28,  104 => 26,  102 => 25,  99 => 24,  97 => 23,  89 => 17,  85 => 16,  79 => 13,  72 => 9,  69 => 8,  65 => 7,  58 => 5,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
\t{% if type_work == ''%}
\t\talertify.alert('เกิดข้อผิดพลาดไม่พลข้อมูลประเภทการส่ง!!',function(){
\t\t\tlocation.href = \"../data/profile.php\";
\t\t});
\t\t
\t{% endif %}\t
\t
{% endblock %}\t\t\t\t
{% block javaScript %}
\t\$(\"#next\").click(function(){
\t\tvar floor \t\t\t= \$(\"#floor\").val();
\t\tif( floor == \"\" || floor == null){
\t\t\t\t\talertify.alert('กรุณาใส่ชั้นของคุณให้ถูกต้อง!!');
\t\t\t\t\tlocation.href = \"../data/profile.php\";
\t\t}else{
\t\t\t{% if type_work == 1 %}
\t\t\t\tlocation.href = \"../employee/work_in.php\";
\t\t\t{% elseif type_work == 2 %}
\t\t\t    location.href = \"../employee/work_out.php\";
\t\t\t{% endif %}
\t\t}
\t
\t})
\t
\t
\t\t\t\t
{% endblock %}\t\t\t\t\t
\t\t\t\t
{% block Content %}

\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้สั่งงาน {% if type_work == 1 %} (ส่งที่สำนักงานใหญ่) {% else %} (ส่งที่สาขา)  {% endif %}</h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"{{ user_data.mr_emp_id }}\">
\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"{{ user_data.mr_emp_code }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"{{ user_data.mr_emp_name }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"{{ user_data.mr_emp_lastname }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"{{ user_data.mr_emp_tel }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"{{ user_data.mr_emp_mobile }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"{{ user_data.mr_emp_email }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tแผนก :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\" disabled>
\t\t\t\t\t\t\t{% for s in department_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_department_id }}\" {% if s.mr_department_id == user_data.mr_department_id %} selected=\"selected\" {% endif %} >{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor\" style=\"width:100%;\" disabled>
\t\t\t\t\t\t\t<option value=\"\"> ไม่มีข้อมูลชั้น</option>
\t\t\t\t\t\t\t{% for f in floor_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ f.mr_floor_id }}\" {% if f.mr_floor_id == user_data.floor_emp %} selected=\"selected\" {% endif %}>{{ f.name }}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"next\">Next </button>
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "employee/profile_check.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\employee\\profile_check.tpl");
    }
}

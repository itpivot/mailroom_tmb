<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/sender.tpl */
class __TwigTemplate_762c9e53dabc0b6d4c66af0862399f49 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/sender.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }


";
    }

    // line 35
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
";
    }

    // line 44
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "\t\t
\t\t\$(\"#barcode\").focus();
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\tstatus = false;
\t\t\t\t}else{
\t\t\t\t\tstatus = true;
\t\t\t\t}

\t\t\t\tif( status === true ){
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: \"ajax/ajax_update_work_by_barcode_sender.php\",
\t\t\t\t\t\ttype: \"post\",
\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t},
\t\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\t\tif(res.status != 200){
\t\t\t\t\t\t\t\talertify.alert(res.message);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t\t\t});\t
\t\t\t\t\t
\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\$('#tb_work_order').DataTable().ajax.reload();
\t\t\t\t}
\t\t});
\t\t
\t\t
\t\t\$('#btn_print').click(function() {
\t\t\tvar mess_id \t\t\t= \$(\"#mess_id\").val();
\t\t\t\$('#fmess_id').val(mess_id);
\t\t\t\$('#frome_print').submit();
\t\t\t//console.log(mess_id);
\t\t\t//window.open('print_report_work_order_by_select_2.php?mess_id='+mess_id);
\t\t\t
\t\t});
\t\t
\t\t
\t\t
";
    }

    // line 94
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 95
        echo "    function select_mess(){
\t\tvar mess_id = \$('#mess_id').val();
\t\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'destroy': true,
\t\t\t \"ajax\": {
\t\t\t\t\"url\": \"./ajax/ajax_load_send_work_mailroom.php\",
\t\t\t\t\"type\": \"POST\",
\t\t\t\t\"dataType\": 'json',
\t\t\t\t\"data\": {
\t\t\t\t\t\t'mess_id': mess_id,
\t\t\t\t\t},
\t\t\t},
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'date_send'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});

\t\t
\t\t
\t}
\t
";
    }

    // line 127
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 128
        echo "
<form id=\"frome_print\" method=\"POST\" action=\"print_report_work_order_by_select_2.php\">
\t<input type=\"hidden\" value=\"0\" id='fmess_id' name=\"mess_id\">
</form>

\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-9\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ส่งออกเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<!-- <div class=\"form-row justify-content-center\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"barcode\" placeholder=\"Barcode\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-auto\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึก</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-left:20px;\">
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mess_id\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\">เลือกทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t";
        // line 156
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["mess_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 157
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_user_id", [], "any", false, false, false, 157), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_emp_name", [], "any", false, false, false, 157), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_emp_lastname", [], "any", false, false, false, 157), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 159
        echo "\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\" style=\"padding-left:20px;\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_search\" onclick=\"select_mess();\">ค้นหา</button>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\" style=\"padding-left:20px;\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-success btn-block\" id=\"btn_print\">พิมพ์ใบคุม</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t<br>
\t\t <div class=\"panel panel-default\">
                  <div class=\"panel-body\">
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Sent : Date/Time</th>
                            <th>Bacode</th>
                            <th>Sender</th>
                            <th>Receiver</th>
                            <th>Type Work</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
\t
\t
\t

";
    }

    // line 214
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 216
        if ((($context["debug"] ?? null) != "")) {
            // line 217
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 219
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/sender.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 219,  324 => 217,  322 => 216,  315 => 214,  259 => 159,  246 => 157,  242 => 156,  212 => 128,  208 => 127,  174 => 95,  170 => 94,  120 => 45,  116 => 44,  106 => 36,  102 => 35,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }


{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
{% endblock %}

{% block domReady %}
\t\t
\t\t\$(\"#barcode\").focus();
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\t\tstatus = false;
\t\t\t\t}else{
\t\t\t\t\tstatus = true;
\t\t\t\t}

\t\t\t\tif( status === true ){
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: \"ajax/ajax_update_work_by_barcode_sender.php\",
\t\t\t\t\t\ttype: \"post\",
\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t},
\t\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\t\tif(res.status != 200){
\t\t\t\t\t\t\t\talertify.alert(res.message);
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t\t\t});\t
\t\t\t\t\t
\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\$('#tb_work_order').DataTable().ajax.reload();
\t\t\t\t}
\t\t});
\t\t
\t\t
\t\t\$('#btn_print').click(function() {
\t\t\tvar mess_id \t\t\t= \$(\"#mess_id\").val();
\t\t\t\$('#fmess_id').val(mess_id);
\t\t\t\$('#frome_print').submit();
\t\t\t//console.log(mess_id);
\t\t\t//window.open('print_report_work_order_by_select_2.php?mess_id='+mess_id);
\t\t\t
\t\t});
\t\t
\t\t
\t\t
{% endblock %}


{% block javaScript %}
    function select_mess(){
\t\tvar mess_id = \$('#mess_id').val();
\t\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'destroy': true,
\t\t\t \"ajax\": {
\t\t\t\t\"url\": \"./ajax/ajax_load_send_work_mailroom.php\",
\t\t\t\t\"type\": \"POST\",
\t\t\t\t\"dataType\": 'json',
\t\t\t\t\"data\": {
\t\t\t\t\t\t'mess_id': mess_id,
\t\t\t\t\t},
\t\t\t},
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'date_send'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});

\t\t
\t\t
\t}
\t
{% endblock %}

{% block Content %}

<form id=\"frome_print\" method=\"POST\" action=\"print_report_work_order_by_select_2.php\">
\t<input type=\"hidden\" value=\"0\" id='fmess_id' name=\"mess_id\">
</form>

\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-9\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ส่งออกเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<!-- <div class=\"form-row justify-content-center\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"barcode\" placeholder=\"Barcode\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-auto\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึก</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"padding-left:20px;\">
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mess_id\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\">เลือกทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t{% for s in mess_data %}
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_user_id }}\">{{ s.mr_emp_name }} {{ s.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\" style=\"padding-left:20px;\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_search\" onclick=\"select_mess();\">ค้นหา</button>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\" style=\"padding-left:20px;\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-success btn-block\" id=\"btn_print\">พิมพ์ใบคุม</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t<br>
\t\t <div class=\"panel panel-default\">
                  <div class=\"panel-body\">
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Sent : Date/Time</th>
                            <th>Bacode</th>
                            <th>Sender</th>
                            <th>Receiver</th>
                            <th>Type Work</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
\t
\t
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/sender.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/sender.tpl");
    }
}

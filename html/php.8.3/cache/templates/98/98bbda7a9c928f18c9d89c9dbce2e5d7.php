<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/reciever.tpl */
class __TwigTemplate_183b01f802eaebabb3c0becb799a19ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/reciever.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }


";
    }

    // line 35
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
";
    }

    // line 44
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "\t\t
\t\t
\t\t
\t\t\$(\"#barcode\").focus();
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar mr_round_id \t\t\t= \$(\"#mr_round_id\").val();
\t\t\tvar full_barcode \t\t= \$('#check_full_barcode') .prop( 'checked' );
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\tstatus = false;
\t\t\t}else{
\t\t\t\tstatus = true;
\t\t\t}
\t\t
\t\t\t//console.log(status);
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_update_work_by_barcode.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'mr_round_id': mr_round_id,
\t\t\t\t\t\t'full_barcode': full_barcode,
\t\t\t\t\t},
\t\t\t\t\t
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t\t\$('#tb_work_order').DataTable().ajax.reload();\t
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert(res.message);
\t\t\t\t\t\t}
\t\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t\t});\t
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t}
\t\t});
\t\t\t

\t\t\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'pageLength': '100',
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'time_send'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});

\t//\tvar table = \$('#tb_work_order').DataTable({
\t//\t\t'responsive': true,
\t//\t\t \"ajax\": {
\t//\t\t\t\"url\": \"./ajax/ajax_load_work_mailroom.php\",
\t//\t\t\t\"type\": \"POST\",
\t//\t\t\t\"dataType\": 'json'
\t//\t\t},
\t//\t\t'columns': [
\t//\t\t\t{ 'data':'no' },
\t//\t\t\t{ 'data':'time_send'},
\t//\t\t\t{ 'data':'mr_work_barcode' },
\t//\t\t\t{ 'data':'name_send' },
\t//\t\t\t{ 'data':'name_receive' },
\t//\t\t\t{ 'data':'mr_type_work_name' },
\t//\t\t\t{ 'data':'mr_status_name' }
\t//\t\t],
\t//\t\t
\t//\t\t'scrollCollapse': true 
\t//\t});

load_all_data();
";
    }

    // line 128
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 129
        echo "
function load_all_data(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_load_work_mailroom.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType:'json',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t},
\t\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t}).done(function( msg ) {
\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t//\$('#bg_loader').hide();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert(msg.message);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}


\t\tfunction check_barcode() {
\t\t\tvar full_barcode \t= \$('#check_full_barcode') .prop( 'checked' );
\t\t\tif ( full_barcode == true ) {
\t\t\t\t\$(\"#barcode_full\").hide();
\t\t\t}else{
\t\t\t\t\$(\"#barcode_full\").show();
\t\t\t}
\t\t}


\t\tfunction update_barcode() {
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar full_barcode \t\t= \$('#check_full_barcode') .prop( 'checked' );
\t\t\tvar mr_round_id \t\t= \$(\"#mr_round_id\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\tstatus = false;
\t\t\t}else{
\t\t\t\tstatus = true;
\t\t\t}
\t\t
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_update_work_by_barcode.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode,\t\t\t\t\t\t
\t\t\t\t\t\t'mr_round_id': mr_round_id,
\t\t\t\t\t\t'full_barcode': full_barcode,
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t\t load_all_data();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert(res.message);
\t\t\t\t\t\t}
\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t\t});\t
\t\t\t\t
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t}
\t\t}

\t\tfunction keyEvent( evt, barcode )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tupdate_barcode();
\t\t\t}
\t\t}


";
    }

    // line 221
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 222
        echo "
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-9\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">รับเข้าเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"barcode_full\" value=\"";
        // line 234
        echo twig_escape_filter($this->env, ($context["barcode_full"] ?? null), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"barcode\" placeholder=\"Barcode\" onkeyup=\"keyEvent(event,this.value);\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-auto\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึก</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"form-check\">
\t\t\t\t\t\t\t\t\t<label class=\"form-check-label\">
\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" class=\"form-check-input\" id=\"check_full_barcode\" onclick=\"check_barcode();\">
\t\t\t\t\t\t\t\t\t\tยิงบาร์โค้ดย้อนหลัง
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t <select class=\"form-control\" id=\"mr_round_id\">
\t\t\t\t\t\t\t\t\t\t ";
        // line 259
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 260
            echo "\t\t\t\t\t\t\t\t\t\t <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 260), "html", null, true);
            echo "\">รับ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_type_work_name", [], "any", false, false, false, 260), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 260), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 262
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t<br>
\t\t <div class=\"panel panel-default\">
                  <div class=\"panel-body\">
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date/Time</th>
                            <th>Bacode</th>
\t\t\t\t\t\t\t<th>Sender</th>
                            <th>Receiver</th>
                            <th>Type Work</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
\t
\t
\t

";
    }

    // line 304
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 306
        if ((($context["debug"] ?? null) != "")) {
            // line 307
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 309
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/reciever.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  423 => 309,  417 => 307,  415 => 306,  408 => 304,  365 => 262,  352 => 260,  348 => 259,  320 => 234,  306 => 222,  302 => 221,  208 => 129,  204 => 128,  120 => 45,  116 => 44,  106 => 36,  102 => 35,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

      #tb_work_order {
        font-size: 13px;
      }

\t  .panel {
\t\tmargin-bottom : 5px;
\t  }


{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
{% endblock %}

{% block domReady %}
\t\t
\t\t
\t\t
\t\t\$(\"#barcode\").focus();
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar mr_round_id \t\t\t= \$(\"#mr_round_id\").val();
\t\t\tvar full_barcode \t\t= \$('#check_full_barcode') .prop( 'checked' );
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\tstatus = false;
\t\t\t}else{
\t\t\t\tstatus = true;
\t\t\t}
\t\t
\t\t\t//console.log(status);
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_update_work_by_barcode.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'mr_round_id': mr_round_id,
\t\t\t\t\t\t'full_barcode': full_barcode,
\t\t\t\t\t},
\t\t\t\t\t
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t\t\$('#tb_work_order').DataTable().ajax.reload();\t
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert(res.message);
\t\t\t\t\t\t}
\t\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t\t});\t
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t}
\t\t});
\t\t\t

\t\t\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'pageLength': '100',
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'time_send'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});

\t//\tvar table = \$('#tb_work_order').DataTable({
\t//\t\t'responsive': true,
\t//\t\t \"ajax\": {
\t//\t\t\t\"url\": \"./ajax/ajax_load_work_mailroom.php\",
\t//\t\t\t\"type\": \"POST\",
\t//\t\t\t\"dataType\": 'json'
\t//\t\t},
\t//\t\t'columns': [
\t//\t\t\t{ 'data':'no' },
\t//\t\t\t{ 'data':'time_send'},
\t//\t\t\t{ 'data':'mr_work_barcode' },
\t//\t\t\t{ 'data':'name_send' },
\t//\t\t\t{ 'data':'name_receive' },
\t//\t\t\t{ 'data':'mr_type_work_name' },
\t//\t\t\t{ 'data':'mr_status_name' }
\t//\t\t],
\t//\t\t
\t//\t\t'scrollCollapse': true 
\t//\t});

load_all_data();
{% endblock %}


{% block javaScript %}

function load_all_data(){
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_load_work_mailroom.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType:'json',
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode
\t\t\t\t\t},
\t\t\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t\t\$('#bg_loader').show();
\t\t\t\t\t}
\t\t\t\t}).done(function( msg ) {
\t\t\t\t\tif(msg.status == 200){
\t\t\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\t\t//\$('#bg_loader').hide();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert(msg.message);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}


\t\tfunction check_barcode() {
\t\t\tvar full_barcode \t= \$('#check_full_barcode') .prop( 'checked' );
\t\t\tif ( full_barcode == true ) {
\t\t\t\t\$(\"#barcode_full\").hide();
\t\t\t}else{
\t\t\t\t\$(\"#barcode_full\").show();
\t\t\t}
\t\t}


\t\tfunction update_barcode() {
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar full_barcode \t\t= \$('#check_full_barcode') .prop( 'checked' );
\t\t\tvar mr_round_id \t\t= \$(\"#mr_round_id\").val();
\t\t\tvar status \t\t\t\t= true;
\t\t\t
\t\t\tif(barcode == \"\" || barcode == null){
\t\t\t\t\$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
\t\t\t\tstatus = false;
\t\t\t}else{
\t\t\t\tstatus = true;
\t\t\t}
\t\t
\t\t\tif( status === true ){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_update_work_by_barcode.php\",
\t\t\t\t\ttype: \"post\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: {
\t\t\t\t\t\t'barcode': barcode,\t\t\t\t\t\t
\t\t\t\t\t\t'mr_round_id': mr_round_id,
\t\t\t\t\t\t'full_barcode': full_barcode,
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t\t\t load_all_data();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert(res.message);
\t\t\t\t\t\t}
\t\t\t\t\t\t\$(\"#barcode\").val('');
\t\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t\t});\t
\t\t\t\t
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t}
\t\t}

\t\tfunction keyEvent( evt, barcode )
\t\t{
\t\t\tif(window.event)
\t\t\t\tvar key = evt.keyCode;
\t\t\telse if(evt.which)
\t\t\t\tvar key = evt.which;
\t\t\t\t
\t\t\tif( key == 13 ){
\t\t\t\tupdate_barcode();
\t\t\t}
\t\t}


{% endblock %}

{% block Content2 %}

\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-9\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">รับเข้าเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"barcode_full\" value=\"{{ barcode_full }}\" readonly>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"barcode\" placeholder=\"Barcode\" onkeyup=\"keyEvent(event,this.value);\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-auto\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึก</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"form-check\">
\t\t\t\t\t\t\t\t\t<label class=\"form-check-label\">
\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" class=\"form-check-input\" id=\"check_full_barcode\" onclick=\"check_barcode();\">
\t\t\t\t\t\t\t\t\t\tยิงบาร์โค้ดย้อนหลัง
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t <select class=\"form-control\" id=\"mr_round_id\">
\t\t\t\t\t\t\t\t\t\t {% for r in round %}
\t\t\t\t\t\t\t\t\t\t <option value=\"{{r.mr_round_id}}\">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t {% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t<br>
\t\t <div class=\"panel panel-default\">
                  <div class=\"panel-body\">
                    <div class=\"table-responsive\">
                      <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date/Time</th>
                            <th>Bacode</th>
\t\t\t\t\t\t\t<th>Sender</th>
                            <th>Receiver</th>
                            <th>Type Work</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
\t
\t
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/reciever.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/reciever.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/confirm_receive.tpl */
class __TwigTemplate_09e7eeee55c2408d80bf90cc479aa9d5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/confirm_receive.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 4
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    
\t#img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1050;
    }
\t#pic_loading {
        width: 600px;
        height: auto;
    }
@font-face {
  font-family: 'password';
  font-style: normal;
  font-weight: 400;
  src: url(../themes/password.ttf);
}

.loginpass {
  font-family: 'password';
}
";
    }

    // line 30
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo " 


\$('#img_loading').hide();
    \$('#btn_save').on('click', function() {
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar work_id \t\t\t= \$(\"#work_id\").val();
\t\t\tvar work_id2 \t\t\t= \$(\"#work_id2\").val();
\t\t\tvar user_pass \t\t\t= \$(\"#user_pass\").val();
\t\t\tvar obj = { emp_id: emp_id, work_id: work_id, user_pass: user_pass };

\t\t\t\$.when(confirmReceive(obj)).then(function(res) {
\t\t\t\tif(res == 1) {
\t\t\t\t\t\$('#img_loading').show();
\t\t\t\t\tif(confirm(\"ยืนยันการส่งเอกสาร!\")) {
\t\t\t\t\t\treturn updateWorkOrders(work_id, emp_id);
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\talert('ข้อมูลไม่ถูกต้องถูกต้อง');
\t\t\t\t\t\$(\"#emp_id\").val('');
\t\t\t\t\t\$(\"#user_pass\").val('');
\t\t\t\t}
\t\t\t}).done(function(data) {
\t\t\t\tif(data == 'success') {
\t\t\t\t\t\$('#img_loading').hide();
\t\t\t\t\twindow.location.href = \"../messenger/rate_send.php?id=\" + work_id2;
\t\t\t\t\t
\t\t\t\t} 
\t\t\t});

\t\t\t// confirmReceive(obj).then(function(res) {
\t\t\t// \tif(res == 1) {
\t\t\t\t\t
\t\t\t// \t\tupdateWorkOrders(work_id).then(function(res) {
\t\t\t// \t\t\tif(res == \"success\") {
\t\t\t// \t\t\t\t\$('#img_loading').hide();
\t\t\t// \t\t\t\tlocation.href = \"../messenger/rate_send.php?id=\" + work_id;
\t\t\t// \t\t\t} else {
\t\t\t\t\t\t\t
\t\t\t// \t\t\t}
\t\t\t// \t\t})
\t\t\t// \t}\t
\t\t\t// })

            // \$.ajax({
            //     url: './ajax/ajax_confirm_receive.php',
            //     type: 'POST',
            //     data: {
\t\t\t// \t\t'emp_id': emp_id,
\t\t\t// \t\t'work_id': work_id,
\t\t\t// \t\t'user_pass': user_pass
            //     },
\t\t\t\t
            //     success: function(res){
            //         if( res == 1 ) {
\t\t\t// \t\t\tif(confirm(\"ยืนยันการส่งเอกสาร!\")) {
\t\t\t// \t\t\t\t\t \$.ajax({
\t\t\t// \t\t\t\t\t\turl: './ajax/ajax_updateSendMailroom.php',
\t\t\t// \t\t\t\t\t\ttype: 'POST',
\t\t\t// \t\t\t\t\t\tdata: {
\t\t\t// \t\t\t\t\t\t\tid: work_id
\t\t\t// \t\t\t\t\t\t},
\t\t\t// \t\t\t\t\t\tdataType: 'html',
\t\t\t// \t\t\t\t\t\tcache: false,
\t\t\t// \t\t\t\t\t\tbeforeSend: function() {
\t\t\t// \t\t\t\t\t\t\t\t\$('#img_loading').show();
\t\t\t// \t\t\t\t\t\t\t},
\t\t\t// \t\t\t\t\t\tsuccess: function(res){
\t\t\t// \t\t\t\t\t\t\t\tif(res == \"success\") {
\t\t\t// \t\t\t\t\t\t\t\t\tlocation.href = \"../messenger/rate_send.php?id=\"+work_id;
\t\t\t// \t\t\t\t\t\t\t\t}
\t\t\t// \t\t\t\t\t\t},
\t\t\t// \t\t\t\t\t\tcomplete: function() {
\t\t\t// \t\t\t\t\t\t\t\$('#img_loading').hide();
\t\t\t// \t\t\t\t\t\t}
\t\t\t// \t\t\t\t\t})
\t\t\t\t\t\t
            //            }
            //         }else{
\t\t\t// \t\t\talert('ข้อมูลไม่ถูกต้องถูกต้อง');
\t\t\t// \t\t\t\$(\"#emp_id\").val('');\t
\t\t\t// \t\t\t\$(\"#user_pass\").val('');\t
\t\t\t// \t\t}
            //     }
            // });
        
    });

";
    }

    // line 121
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 122
        echo "\tvar confirmReceive = function(obj) {
\t\treturn \$.post('../messenger/ajax/ajax_confirm_receive.php', obj);
\t}

\tvar updateWorkOrders = function(id, emp_id) {
\t\treturn \$.post('../messenger/ajax/ajax_updateSendMailroom.php', { id: id, username: emp_id });
\t} 

";
    }

    // line 133
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 134
        echo "<form autocomplete=\"new-password\" method=\"post\" action=\"\">
<div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
\t\t\t\t<label><h3>ยืนยันการรับเอกสาร</h3></label>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label>รหัสพนักงาน</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"emp_id\" placeholder=\"รหัสพนักงาน\"  value=\"\"  autocomplete=\"off\">
\t\t\t\t\t<input type=\"hidden\" id=\"work_id\" value=\"";
        // line 140
        echo twig_escape_filter($this->env, ($context["work_id"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t<input type=\"hidden\" id=\"work_id2\" value=\"";
        // line 141
        echo twig_escape_filter($this->env, ($context["work_id2"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label>รหัสผ่านการเข้าสู่ระบบ</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control loginpass\" id=\"user_pass\" placeholder=\"Password\" value=\"\" autocomplete=\"new-password\">
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-3\"></div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"width:100%;\" id=\"btn_save\">Submit</button>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-3\"></div>
\t\t\t\t</div>
\t\t\t
</form>
";
    }

    // line 161
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 163
        if ((($context["debug"] ?? null) != "")) {
            // line 164
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 166
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/confirm_receive.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 166,  248 => 164,  246 => 163,  239 => 161,  217 => 141,  213 => 140,  205 => 134,  201 => 133,  189 => 122,  185 => 121,  93 => 31,  89 => 30,  62 => 5,  58 => 4,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}
{% block styleReady %}
    
\t#img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1050;
    }
\t#pic_loading {
        width: 600px;
        height: auto;
    }
@font-face {
  font-family: 'password';
  font-style: normal;
  font-weight: 400;
  src: url(../themes/password.ttf);
}

.loginpass {
  font-family: 'password';
}
{% endblock %}

{% block domReady %}
 


\$('#img_loading').hide();
    \$('#btn_save').on('click', function() {
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar work_id \t\t\t= \$(\"#work_id\").val();
\t\t\tvar work_id2 \t\t\t= \$(\"#work_id2\").val();
\t\t\tvar user_pass \t\t\t= \$(\"#user_pass\").val();
\t\t\tvar obj = { emp_id: emp_id, work_id: work_id, user_pass: user_pass };

\t\t\t\$.when(confirmReceive(obj)).then(function(res) {
\t\t\t\tif(res == 1) {
\t\t\t\t\t\$('#img_loading').show();
\t\t\t\t\tif(confirm(\"ยืนยันการส่งเอกสาร!\")) {
\t\t\t\t\t\treturn updateWorkOrders(work_id, emp_id);
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\talert('ข้อมูลไม่ถูกต้องถูกต้อง');
\t\t\t\t\t\$(\"#emp_id\").val('');
\t\t\t\t\t\$(\"#user_pass\").val('');
\t\t\t\t}
\t\t\t}).done(function(data) {
\t\t\t\tif(data == 'success') {
\t\t\t\t\t\$('#img_loading').hide();
\t\t\t\t\twindow.location.href = \"../messenger/rate_send.php?id=\" + work_id2;
\t\t\t\t\t
\t\t\t\t} 
\t\t\t});

\t\t\t// confirmReceive(obj).then(function(res) {
\t\t\t// \tif(res == 1) {
\t\t\t\t\t
\t\t\t// \t\tupdateWorkOrders(work_id).then(function(res) {
\t\t\t// \t\t\tif(res == \"success\") {
\t\t\t// \t\t\t\t\$('#img_loading').hide();
\t\t\t// \t\t\t\tlocation.href = \"../messenger/rate_send.php?id=\" + work_id;
\t\t\t// \t\t\t} else {
\t\t\t\t\t\t\t
\t\t\t// \t\t\t}
\t\t\t// \t\t})
\t\t\t// \t}\t
\t\t\t// })

            // \$.ajax({
            //     url: './ajax/ajax_confirm_receive.php',
            //     type: 'POST',
            //     data: {
\t\t\t// \t\t'emp_id': emp_id,
\t\t\t// \t\t'work_id': work_id,
\t\t\t// \t\t'user_pass': user_pass
            //     },
\t\t\t\t
            //     success: function(res){
            //         if( res == 1 ) {
\t\t\t// \t\t\tif(confirm(\"ยืนยันการส่งเอกสาร!\")) {
\t\t\t// \t\t\t\t\t \$.ajax({
\t\t\t// \t\t\t\t\t\turl: './ajax/ajax_updateSendMailroom.php',
\t\t\t// \t\t\t\t\t\ttype: 'POST',
\t\t\t// \t\t\t\t\t\tdata: {
\t\t\t// \t\t\t\t\t\t\tid: work_id
\t\t\t// \t\t\t\t\t\t},
\t\t\t// \t\t\t\t\t\tdataType: 'html',
\t\t\t// \t\t\t\t\t\tcache: false,
\t\t\t// \t\t\t\t\t\tbeforeSend: function() {
\t\t\t// \t\t\t\t\t\t\t\t\$('#img_loading').show();
\t\t\t// \t\t\t\t\t\t\t},
\t\t\t// \t\t\t\t\t\tsuccess: function(res){
\t\t\t// \t\t\t\t\t\t\t\tif(res == \"success\") {
\t\t\t// \t\t\t\t\t\t\t\t\tlocation.href = \"../messenger/rate_send.php?id=\"+work_id;
\t\t\t// \t\t\t\t\t\t\t\t}
\t\t\t// \t\t\t\t\t\t},
\t\t\t// \t\t\t\t\t\tcomplete: function() {
\t\t\t// \t\t\t\t\t\t\t\$('#img_loading').hide();
\t\t\t// \t\t\t\t\t\t}
\t\t\t// \t\t\t\t\t})
\t\t\t\t\t\t
            //            }
            //         }else{
\t\t\t// \t\t\talert('ข้อมูลไม่ถูกต้องถูกต้อง');
\t\t\t// \t\t\t\$(\"#emp_id\").val('');\t
\t\t\t// \t\t\t\$(\"#user_pass\").val('');\t
\t\t\t// \t\t}
            //     }
            // });
        
    });

{% endblock %}

{% block javaScript %}
\tvar confirmReceive = function(obj) {
\t\treturn \$.post('../messenger/ajax/ajax_confirm_receive.php', obj);
\t}

\tvar updateWorkOrders = function(id, emp_id) {
\t\treturn \$.post('../messenger/ajax/ajax_updateSendMailroom.php', { id: id, username: emp_id });
\t} 

{% endblock %}


{% block Content %}
<form autocomplete=\"new-password\" method=\"post\" action=\"\">
<div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
\t\t\t\t<label><h3>ยืนยันการรับเอกสาร</h3></label>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label>รหัสพนักงาน</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"emp_id\" placeholder=\"รหัสพนักงาน\"  value=\"\"  autocomplete=\"off\">
\t\t\t\t\t<input type=\"hidden\" id=\"work_id\" value=\"{{ work_id }}\">
\t\t\t\t\t<input type=\"hidden\" id=\"work_id2\" value=\"{{ work_id2 }}\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label>รหัสผ่านการเข้าสู่ระบบ</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control loginpass\" id=\"user_pass\" placeholder=\"Password\" value=\"\" autocomplete=\"new-password\">
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-3\"></div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" style=\"width:100%;\" id=\"btn_save\">Submit</button>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-3\"></div>
\t\t\t\t</div>
\t\t\t
</form>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "messenger/confirm_receive.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\messenger\\confirm_receive.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/search.tpl */
class __TwigTemplate_1b896984eae84edceebde00d7516abce extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/search.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    body {
        height:100%;
    }
    .table-hover tbody tr.hilight:hover {
        background-color: #555;
        color: #fff;
    }

    table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
    }

    table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

    #tb_work_order {
    \tfont-size: 13px;
    }

\t.panel {
\t\tmargin-bottom : 5px;
\t}
\t.loading {
\t\tposition: fixed;
\t\ttop: 40%;
\t\tleft: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\t-ms-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1000;
\t}
\t
\t.img_loading {
\t\twidth: 350px;
\t\theight: auto;
\t}

";
    }

    // line 48
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 58
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "\$('[data-toggle=\"tooltip\"]').tooltip()
\t\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
\t\t
\t\tvar table = \$('#tb_work_order').DataTable({ 
\t\t\t\"scrollY\": \"800px\",
\t\t\t\"scrollCollapse\": true,
\t\t\t\"responsive\": true,
\t\t\t//\"searching\": false,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t\t\t{ 'data':'cancle' },
\t\t\t\t\t\t{ 'data':'ch_data'},
\t\t\t\t\t\t{ 'data':'time_re'},
\t\t\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t\t\t{ 'data':'con_log' },
\t\t\t\t\t\t{ 'data':'depart_floor_receive' },
\t\t\t\t\t\t{ 'data':'name_send' },
\t\t\t\t\t\t{ 'data':'depart_floor_send' },
\t\t\t\t\t\t{ 'data':'mr_status_name' },
\t\t\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t\t\t{ 'data':'mr_work_remark' },
\t\t\t\t\t\t{ 'data':'mr_topic' },
\t\t\t\t\t\t{ 'data':'con_name' }
\t\t\t]
\t\t});
\t\t\$(\"#btn_search\").click(function(){
\t\t\tgetData();
\t\t});\t
\t\t
\t\t\$(\"#pass_emp_send\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_send\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200 ){
\t\t\t\t\t\t\$(\"#name_sender\").val(res.data.mr_emp_name);
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#emp_id_send\").val(res.data.mr_emp_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t});
\t\t});
\t\t
\t\t
\t\t\$('#select-all').on('click', function(){
\t\t   // Check/uncheck all checkboxes in the table
\t\t   var rows = table.rows({ 'search': 'applied' }).nodes();
\t\t   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
\t\t});
\t\t
\t\t
\t\t
\t\t\$(\"#pass_emp_re\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_re\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200 ){
\t\t\t\t\t\t\$(\"#name_sender\").val(res.data.mr_emp_name);
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#emp_id_send\").val(res.data.mr_emp_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t});\t\t\t\t
\t\t});
\t\t
\t\t
\t\t\$(\"#btn_excel\").click(function() {
\t\t\tvar data = new Object();
\t\t\tdata['barcode'] \t\t\t= \$(\"#barcode\").val();
\t\t\tdata['sender'] \t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tdata['receiver'] \t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tdata['start_date'] \t\t\t= \$(\"#start_date\").val();
\t\t\tdata['end_date'] \t\t\t= \$(\"#end_date\").val();
\t\t\tdata['status'] \t\t\t\t= \$(\"#status\").val();
\t\t\tvar param = JSON.stringify(data);
\t\t
\t\t\tlocation.href='excel.report.php?params='+param;
\t\t\t// window.open('excel.report.php?params='+param);
\t\t
\t\t});
\t\t
\t\t
\t\t
\t\t\$(\"#btn_clear\").click(function() {
\t\t\tlocation.reload();
\t\t\t
\t\t});
\t\t
\t\t
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tresults : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t\t
\t\t});

\t\tfunction setForm(data) {
\t\t\tvar emp_id = parseInt(data.id);
\t\t\tvar fullname = data.text;
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\$(\"#pass_depart_re\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_re\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_re\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#pass_emp_re\").val(res.data.mr_emp_code);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}
\t\t
\t\t\$('#name_send_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้ส่ง\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tresults : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetFormSend(e.params.data);
\t\t});

\t\tfunction setFormSend(data) {
\t\t\tvar emp_id = parseInt(data.id);
\t\t\tvar fullname = data.text;
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\t
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#pass_emp_send\").val(res.data.mr_emp_code);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

";
    }

    // line 262
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 263
        echo "
function successAll_click() {
\t\t\t\tvar dataall = [];
\t

\t\t\t\tvar tel_receiver = \$('#tel_receiver').val();
\t\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t//  dataall.push(this.value);
\t\t\t\t
\t\t\t\t\tvar token = encodeURIComponent(window.btoa(this.value));
\t\t\t\t\t
\t\t\t\t\tdataall.push(token);
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }

\t\t\t\t
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\t\$('#wo_status_id').val('5');
\t\t\t\t\$('#work_all_id').val(newdataall);
\t\t\t\tif(\$('#work_all_id').val()!= ''){
\t\t\t\t\t\$('#form_success_work').submit();
\t\t\t\t}
\t\t\t\t//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t}
function cancleAll_click() {
\t\t\t\tvar dataall = [];
\t\t\t\tvar tel_receiver = \$('#tel_receiver').val();
\t\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t//  dataall.push(this.value);
\t\t\t\t
\t\t\t\t\tvar token = encodeURIComponent(window.btoa(this.value));
\t\t\t\t\t
\t\t\t\t\tdataall.push(token);
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }

\t\t\t\t
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\t\$('#work_all_id').val(newdataall);
\t\t\t\t\$('#wo_status_id').val('6');
\t\t\t\tif(\$('#work_all_id').val()!= ''){
\t\t\t\t\t\$('#form_success_work').submit();
\t\t\t\t}
\t\t\t\t//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t}
   function getData(){
\t\t\tvar barcode \t\t\t\t\t\t= \$(\"#barcode\").val();
\t\t\tvar pass_emp_send \t\t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tvar pass_emp_re \t\t\t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tvar start_date \t\t\t\t\t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t\t\t\t\t= \$(\"#end_date\").val();
\t\t\tvar status \t\t\t\t\t\t\t= \$(\"#status\").val();
\t\t\tvar select_mr_contact_id \t\t\t= \$(\"#select_mr_contact_id\").val();

\t\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType: \"json\",
\t\t\turl: \"ajax/ajax_search_work_mailroom.php\",
\t\t\tdata: {
\t\t\t\t'barcode': barcode,
\t\t\t\t'receiver': pass_emp_re,
\t\t\t\t'sender': pass_emp_send,
\t\t\t\t'start_date': start_date,
\t\t\t\t'end_date': end_date,
\t\t\t\t'select_mr_contact_id': select_mr_contact_id,
\t\t\t\t'status': status
\t\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกินไป!');
\t\t\t\t//alertify.alert(\"กิดข้อผิดผลาด !\");
\t\t\t\t//location.reload();

\t\t\t}
\t\t})
\t\t.done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data.data).draw(); 
\t\t\t}else{
\t\t\t\talert(msg.message);
\t\t\t\tlocation.reload();
\t\t\t}
\t\t});
\t};

";
    }

    // line 368
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 369
        echo "
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-10\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ค้นหา</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form id=\"form_success_work\" action=\"success_work_all_mailroom.php\" method=\"post\">
\t\t\t\t\t\t\t<input id=\"wo_status_id\" name=\"wo_status_id\" type=\"hidden\" value=\"\">
\t\t\t\t\t\t\t<input id=\"work_all_id\" name=\"work_all_id\" type=\"hidden\" value=\"\">
\t\t\t\t\t\t</form>
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<div class=\"row justify-content-between\">
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-left:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้ส่ง</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_send_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 396
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_send\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_send\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_sender\" placeholder=\"ชื่อผู้ส่ง\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_send\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_send\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_send\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-right:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้รับ</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 456
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_re\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_re\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_re\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_re\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_re\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"select_mr_contact_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <option value = \"\" selected> รายชื่อติดต่อ </option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 493
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contactdata"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["con"]) {
            // line 494
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["con"], "mr_contact_id", [], "any", false, false, false, 494), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["con"], "department_name", [], "any", false, false, false, 494), "html", null, true);
            echo " ** ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["con"], "remark", [], "any", false, false, false, 494), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["con"], "QuantityText", [], "any", false, false, false, 494), "html", null, true);
            echo " **</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['con'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 496
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:40px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t\t\t\t\t\t\t\t<input  autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\"/>
\t\t\t\t\t\t\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">to</label>
\t\t\t\t\t\t\t\t\t\t<input autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\"/>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"barcode\" placeholder=\"Barcode\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"status\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">สถานะงานทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งภายไนสำนักงานใหญ่\">
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 532
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["status_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 533
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_id", [], "any", false, false, false, 533), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_name", [], "any", false, false, false, 533), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 535
        echo "\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งที่สาขา\">
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 537
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["status_data2"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 538
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_id", [], "any", false, false, false, 538), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_name", [], "any", false, false, false, 538), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 539
        echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"Pending\">เอกสารคงค้าง</option>
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-center\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_search\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\tfind_in_page
\t\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t\tค้นหา
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-dark\" id=\"btn_excel\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\t\tplay_for_work
\t\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t\t\tExport Excel
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-warning\" id=\"btn_clear\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\t\tundo
\t\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t\t\tClear</button>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t
\t\t <div class=\"panel panel-default\" style=\"margin-top:50px;\">
            <div class=\"panel-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>#</th><div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">
                      <th width=\"250\"><label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">ทั้งหมด</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<button type=\"button\" onclick=\"successAll_click();\" class=\"btn btn-outline-success btn-sm\"
\t\t\t\t\t\t\tdata-toggle=\"tooltip\" data-placement=\"top\" title=\"ปิดงานสำเร็จเอกสารถึงผู้รับเรียบร้อยแล้ว\"
\t\t\t\t\t\t\t>
\t\t\t\t\t\t\t\t<span class=\"material-icons\">
\t\t\t\t\t\t\t\t\tdone
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<button type=\"button\" onclick=\"cancleAll_click();\" class=\"btn btn-outline-danger btn-sm\"
\t\t\t\t\t\t\tdata-toggle=\"tooltip\" data-placement=\"top\" title=\"ยกเลิกรายการ\"
\t\t\t\t\t\t\t>
\t\t\t\t\t\t\t\t<span class=\"material-icons\">
\t\t\t\t\t\t\t\t\tclear
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</th>
                      <th>วันที่/เวลาสั่งงาน</th>
                      <th>Bacode</th>
                      <th>ผู้รับ</th>
                      <th>วันที่/เวลา</th>
                      <th>ชั้นของผู้รับ</th>
                      <th>ผู้ส่ง</th>
                      <th>ชั้นของผู้ส่ง</th>
                      <th>สถานะเอกสาร</th>
                      <th>type work</th>
                      <th>หมายเหตุ</th>
                      <th>ชื่อเอกสาร</th>
                      <th>contact</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
\t
<div class='loading' style=\"display: none;\">
                <img src=\"../themes/images/loading.gif\" class='img_loading'>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
\t

";
    }

    // line 664
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 666
        if ((($context["debug"] ?? null) != "")) {
            // line 667
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 669
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/search.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  807 => 669,  801 => 667,  799 => 666,  792 => 664,  666 => 539,  655 => 538,  651 => 537,  647 => 535,  636 => 533,  632 => 532,  594 => 496,  579 => 494,  575 => 493,  536 => 456,  478 => 396,  453 => 369,  449 => 368,  342 => 263,  338 => 262,  134 => 59,  130 => 58,  119 => 49,  115 => 48,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
    body {
        height:100%;
    }
    .table-hover tbody tr.hilight:hover {
        background-color: #555;
        color: #fff;
    }

    table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
    }

    table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}

    #tb_work_order {
    \tfont-size: 13px;
    }

\t.panel {
\t\tmargin-bottom : 5px;
\t}
\t.loading {
\t\tposition: fixed;
\t\ttop: 40%;
\t\tleft: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\t-ms-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1000;
\t}
\t
\t.img_loading {
\t\twidth: 350px;
\t\theight: auto;
\t}

{% endblock %}
{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}

{% block domReady %}
\$('[data-toggle=\"tooltip\"]').tooltip()
\t\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
\t\t
\t\tvar table = \$('#tb_work_order').DataTable({ 
\t\t\t\"scrollY\": \"800px\",
\t\t\t\"scrollCollapse\": true,
\t\t\t\"responsive\": true,
\t\t\t//\"searching\": false,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t\t\t{ 'data':'cancle' },
\t\t\t\t\t\t{ 'data':'ch_data'},
\t\t\t\t\t\t{ 'data':'time_re'},
\t\t\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t\t\t{ 'data':'con_log' },
\t\t\t\t\t\t{ 'data':'depart_floor_receive' },
\t\t\t\t\t\t{ 'data':'name_send' },
\t\t\t\t\t\t{ 'data':'depart_floor_send' },
\t\t\t\t\t\t{ 'data':'mr_status_name' },
\t\t\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t\t\t{ 'data':'mr_work_remark' },
\t\t\t\t\t\t{ 'data':'mr_topic' },
\t\t\t\t\t\t{ 'data':'con_name' }
\t\t\t]
\t\t});
\t\t\$(\"#btn_search\").click(function(){
\t\t\tgetData();
\t\t});\t
\t\t
\t\t\$(\"#pass_emp_send\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_send\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200 ){
\t\t\t\t\t\t\$(\"#name_sender\").val(res.data.mr_emp_name);
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#emp_id_send\").val(res.data.mr_emp_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t});
\t\t});
\t\t
\t\t
\t\t\$('#select-all').on('click', function(){
\t\t   // Check/uncheck all checkboxes in the table
\t\t   var rows = table.rows({ 'search': 'applied' }).nodes();
\t\t   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
\t\t});
\t\t
\t\t
\t\t
\t\t\$(\"#pass_emp_re\").keyup(function(){
\t\t\tvar pass_emp = \$(\"#pass_emp_re\").val();
\t\t\t\$.ajax({
\t\t\t\turl: \"ajax/ajax_autocompress2.php\",
\t\t\t\ttype: \"post\",
\t\t\t\tdata: {
\t\t\t\t\t'pass_emp': pass_emp,
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res){
\t\t\t\t\tif(res.status == 200 ){
\t\t\t\t\t\t\$(\"#name_sender\").val(res.data.mr_emp_name);
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#emp_id_send\").val(res.data.mr_emp_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}\t\t\t\t\t\t\t
\t\t\t});\t\t\t\t
\t\t});
\t\t
\t\t
\t\t\$(\"#btn_excel\").click(function() {
\t\t\tvar data = new Object();
\t\t\tdata['barcode'] \t\t\t= \$(\"#barcode\").val();
\t\t\tdata['sender'] \t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tdata['receiver'] \t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tdata['start_date'] \t\t\t= \$(\"#start_date\").val();
\t\t\tdata['end_date'] \t\t\t= \$(\"#end_date\").val();
\t\t\tdata['status'] \t\t\t\t= \$(\"#status\").val();
\t\t\tvar param = JSON.stringify(data);
\t\t
\t\t\tlocation.href='excel.report.php?params='+param;
\t\t\t// window.open('excel.report.php?params='+param);
\t\t
\t\t});
\t\t
\t\t
\t\t
\t\t\$(\"#btn_clear\").click(function() {
\t\t\tlocation.reload();
\t\t\t
\t\t});
\t\t
\t\t
\t\t
\t\t\$('#name_receiver_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tresults : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetForm(e.params.data);
\t\t\t
\t\t});

\t\tfunction setForm(data) {
\t\t\tvar emp_id = parseInt(data.id);
\t\t\tvar fullname = data.text;
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\$(\"#pass_depart_re\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_re\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_re\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#pass_emp_re\").val(res.data.mr_emp_code);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}
\t\t
\t\t\$('#name_send_select').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้ส่ง\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tresults : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\tsetFormSend(e.params.data);
\t\t});

\t\tfunction setFormSend(data) {
\t\t\tvar emp_id = parseInt(data.id);
\t\t\tvar fullname = data.text;
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\t
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\t\$(\"#pass_depart_send\").val(res.data.mr_department_code);
\t\t\t\t\t\t\$(\"#floor_send\").val(res.data.mr_department_floor);
\t\t\t\t\t\t\$(\"#depart_send\").val(res.data.mr_department_name);
\t\t\t\t\t\t\$(\"#pass_emp_send\").val(res.data.mr_emp_code);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

{% endblock %}


{% block javaScript %}

function successAll_click() {
\t\t\t\tvar dataall = [];
\t

\t\t\t\tvar tel_receiver = \$('#tel_receiver').val();
\t\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t//  dataall.push(this.value);
\t\t\t\t
\t\t\t\t\tvar token = encodeURIComponent(window.btoa(this.value));
\t\t\t\t\t
\t\t\t\t\tdataall.push(token);
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }

\t\t\t\t
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\t\$('#wo_status_id').val('5');
\t\t\t\t\$('#work_all_id').val(newdataall);
\t\t\t\tif(\$('#work_all_id').val()!= ''){
\t\t\t\t\t\$('#form_success_work').submit();
\t\t\t\t}
\t\t\t\t//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t}
function cancleAll_click() {
\t\t\t\tvar dataall = [];
\t\t\t\tvar tel_receiver = \$('#tel_receiver').val();
\t\t\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t\t\t //console.log(this.value);
\t\t\t\t\t//  dataall.push(this.value);
\t\t\t\t
\t\t\t\t\tvar token = encodeURIComponent(window.btoa(this.value));
\t\t\t\t\t
\t\t\t\t\tdataall.push(token);
\t\t\t\t  });
\t\t\t\t  //console.log(tel_receiver);
\t\t\t\t  if(dataall.length < 1){
\t\t\t\t\t alertify.alert(\"ตรวจสอบข้อมูล\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t\t\t return;
\t\t\t\t  }

\t\t\t\t
\t\t\t\t // return;
\t\t\t\tvar newdataall = dataall.join(\",\");
\t\t\t\t\$('#work_all_id').val(newdataall);
\t\t\t\t\$('#wo_status_id').val('6');
\t\t\t\tif(\$('#work_all_id').val()!= ''){
\t\t\t\t\t\$('#form_success_work').submit();
\t\t\t\t}
\t\t\t\t//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
\t\t\t}
   function getData(){
\t\t\tvar barcode \t\t\t\t\t\t= \$(\"#barcode\").val();
\t\t\tvar pass_emp_send \t\t\t\t\t= \$(\"#pass_emp_send\").val();
\t\t\tvar pass_emp_re \t\t\t\t\t= \$(\"#pass_emp_re\").val();
\t\t\tvar start_date \t\t\t\t\t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t\t\t\t\t= \$(\"#end_date\").val();
\t\t\tvar status \t\t\t\t\t\t\t= \$(\"#status\").val();
\t\t\tvar select_mr_contact_id \t\t\t= \$(\"#select_mr_contact_id\").val();

\t\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType: \"json\",
\t\t\turl: \"ajax/ajax_search_work_mailroom.php\",
\t\t\tdata: {
\t\t\t\t'barcode': barcode,
\t\t\t\t'receiver': pass_emp_re,
\t\t\t\t'sender': pass_emp_send,
\t\t\t\t'start_date': start_date,
\t\t\t\t'end_date': end_date,
\t\t\t\t'select_mr_contact_id': select_mr_contact_id,
\t\t\t\t'status': status
\t\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกินไป!');
\t\t\t\t//alertify.alert(\"กิดข้อผิดผลาด !\");
\t\t\t\t//location.reload();

\t\t\t}
\t\t})
\t\t.done(function( msg ) {
\t\t\tif(msg.status == 200){
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data.data).draw(); 
\t\t\t}else{
\t\t\t\talert(msg.message);
\t\t\t\tlocation.reload();
\t\t\t}
\t\t});
\t};

{% endblock %}

{% block Content2 %}

\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"col-10\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ค้นหา</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<form id=\"form_success_work\" action=\"success_work_all_mailroom.php\" method=\"post\">
\t\t\t\t\t\t\t<input id=\"wo_status_id\" name=\"wo_status_id\" type=\"hidden\" value=\"\">
\t\t\t\t\t\t\t<input id=\"work_all_id\" name=\"work_all_id\" type=\"hidden\" value=\"\">
\t\t\t\t\t\t</form>
\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<div class=\"row justify-content-between\">
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-left:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้ส่ง</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_send_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{# < option value = \"0\" > ค้นหาผู้ส่ง < /option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for e in emp_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ e.mr_emp_id }}\" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %} #}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_send\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_send\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_sender\" placeholder=\"ชื่อผู้ส่ง\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<br>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_send\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_send\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_send\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"card col-5\" style=\"padding:0px;margin-right:20px\">
\t\t\t\t\t\t\t\t\t<h5 style=\"padding:5px 10px;\">ผู้รับ</h5>
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"name_receiver_select\" style=\"width:100%;\" >
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{# < option value = \"0\" > ค้นหาผู้รับ< /option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for e in emp_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ e.mr_emp_id }}\" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %} #}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"pass_emp_re\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_emp_re\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"name_receiver\" placeholder=\"ชื่อผู้รับ\">
\t\t\t\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-8\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"pass_depart_re\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-0\" id=\"floor_re\" placeholder=\"ชั้น\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"depart_re\" placeholder=\"ชื่อหน่วยงาน\" disabled>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row \">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"select_mr_contact_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <option value = \"\" selected> รายชื่อติดต่อ </option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for con in contactdata %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ con.mr_contact_id }}\" > {{ con.department_name }} ** {{ con.remark }} : {{ con.QuantityText }} **</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:40px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t\t\t\t\t\t\t\t<input  autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\"/>
\t\t\t\t\t\t\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">to</label>
\t\t\t\t\t\t\t\t\t\t<input autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\"/>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-1\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-10\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control mb-5 mb-sm-2\" id=\"barcode\" placeholder=\"Barcode\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"status\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">สถานะงานทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งภายไนสำนักงานใหญ่\">
\t\t\t\t\t\t\t\t\t\t\t\t{% for s in status_data %}
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_status_id }}\">{{ s.mr_status_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งที่สาขา\">
\t\t\t\t\t\t\t\t\t\t\t\t{% for s in status_data2 %}
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_status_id }}\">{{ s.mr_status_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"Pending\">เอกสารคงค้าง</option>
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-center\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_search\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\tfind_in_page
\t\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t\tค้นหา
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-dark\" id=\"btn_excel\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\t\tplay_for_work
\t\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t\t\tExport Excel
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-warning\" id=\"btn_clear\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">
\t\t\t\t\t\t\t\t\t\tundo
\t\t\t\t\t\t\t\t\t</i>
\t\t\t\t\t\t\t\t\t\tClear</button>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\" style=\"margin-top:10px;\">
\t\t\t\t\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col\">
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t
\t\t <div class=\"panel panel-default\" style=\"margin-top:50px;\">
            <div class=\"panel-body\">
              <div class=\"table-responsive\">
                <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>#</th><div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">
                      <th width=\"250\"><label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">ทั้งหมด</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<button type=\"button\" onclick=\"successAll_click();\" class=\"btn btn-outline-success btn-sm\"
\t\t\t\t\t\t\tdata-toggle=\"tooltip\" data-placement=\"top\" title=\"ปิดงานสำเร็จเอกสารถึงผู้รับเรียบร้อยแล้ว\"
\t\t\t\t\t\t\t>
\t\t\t\t\t\t\t\t<span class=\"material-icons\">
\t\t\t\t\t\t\t\t\tdone
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t<button type=\"button\" onclick=\"cancleAll_click();\" class=\"btn btn-outline-danger btn-sm\"
\t\t\t\t\t\t\tdata-toggle=\"tooltip\" data-placement=\"top\" title=\"ยกเลิกรายการ\"
\t\t\t\t\t\t\t>
\t\t\t\t\t\t\t\t<span class=\"material-icons\">
\t\t\t\t\t\t\t\t\tclear
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</th>
                      <th>วันที่/เวลาสั่งงาน</th>
                      <th>Bacode</th>
                      <th>ผู้รับ</th>
                      <th>วันที่/เวลา</th>
                      <th>ชั้นของผู้รับ</th>
                      <th>ผู้ส่ง</th>
                      <th>ชั้นของผู้ส่ง</th>
                      <th>สถานะเอกสาร</th>
                      <th>type work</th>
                      <th>หมายเหตุ</th>
                      <th>ชื่อเอกสาร</th>
                      <th>contact</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
\t
<div class='loading' style=\"display: none;\">
                <img src=\"../themes/images/loading.gif\" class='img_loading'>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/search.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\search.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/reports.tpl */
class __TwigTemplate_d32004a56256023b64480a633faa80aa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_5' => [$this, 'block_menu_5'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/reports.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- รายงาน ";
    }

    // line 5
    public function block_menu_5($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    #btn_exports:hover{
        color: #FFFFFF;
        background-color: #055d97;
    }

    #btn_exports{
        border-color: #0074c0;
        color: #FFFFFF;
        background-color: #0074c0;
    }

    #detail_sender_head h4{
        text-align:left;
        color: #006cb7;
        border-bottom: 3px solid #006cb7;
        display: inline;
    }

    #detail_sender_head {
        border-bottom: 3px solid #eee;
        margin-bottom: 20px;
        margin-top: 20px;
    }

    #detail_receiver_head h4{
        text-align:left;
        color: #006cb7;
        border-bottom: 3px solid #006cb7;
        display: inline;
    }

    #detail_receiver_head {
        border-bottom: 3px solid #eee;
        margin-bottom: 20px;
        margin-top: 40px;
    }

    .input-group-addon{
        background-color: #FFFFFF;
        border: 1px solid #055d97;
    }

";
    }

    // line 52
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    // \$('#month_success').select2({
    //     data: [
    //         { id: 1, text: 'text' }
    //     ]
    // });

    \$('#btn_exports').click(function() {
        var months = \$('#month_success').val();
        var years = \$('#year_success').val();
        var status = true;

        if(months == \"\" || months == null) {
            status = false;
        }

        if (years == \"\" || years == null) {
            status = false;
        }

        if(!status) {
             alertify.alert('ยังไม่เลือกเงื่อนไข');
        } else {
            window.open('export_dateSuccess.php?month='+months+\"&year=\"+years, \"_blank\")
        }
        
    });


    \$('#year_success').on('select2:select', function (e) {
        \$('#month_success').empty().trigger('change')
        \$.ajax({
            url: \"./ajax/ajax_getMonthSuccess.php\",
            type: \"POST\",
            data: {
                year: e.params.data.text
            },
            dataType: 'json',
            success: function(res) {
                \$('#month_success').select2(res).trigger('change');
            }
        })
    });

";
    }

    // line 98
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 99
        echo "
";
    }

    // line 102
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 103
        echo "<div class=\"container\">
    <div style=\"text-align:center; color:#0074c0; margin-top:20px;\">
        <label>
            <h3><b>รายงานรอบนำส่งเอกสาร</b></h3>
        </label>
    </div>

    <div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
        <h4>รายงานรอบนำส่งเอกสาร (ประวัติการนำส่ง) </h4>
    </div>

    <div class=\"row\">
        <div class=\"col-md-6\">
            <div class=\"form-group row\">
                <label for=\"date_success\" class=\"col-sm-2 font-weight-bold\">ปี:</label>
                <div class=\"col-sm-10\">
                    <select class=\"form-control form-control-sm\" name=\"yearSuccess\" id=\"year_success\">
                        <option value=\"\">--- เลือกปี ---</option>
                        ";
        // line 121
        if ((twig_length_filter($this->env, ($context["years"] ?? null)) > 0)) {
            // line 122
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["years"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["year"]) {
                // line 123
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $context["year"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["year"], "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['year'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 125
            echo "                        ";
        }
        // line 126
        echo "                    </select>
                </div>
            </div>
        </div>
        <div class=\"col-md-6\">
            <div class=\"form-group row\">
                <label for=\"date_success\" class=\"col-sm-2 font-weight-bold\">เดือน:</label>
                <div class=\"col-sm-10\">
                    <select class=\"form-control form-control-sm\" name=\"monthSuccess\" id=\"month_success\">
                        <option value=\"\">--- เลือกเดือน ---</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
     <div class=\"float-right\">
            <button type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_exports\">ออกรายงาน</button>
    </div>
    </div>
    


";
    }

    // line 151
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 152
        echo "
";
        // line 153
        if ((($context["debug"] ?? null) != "")) {
            // line 154
            echo "<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
";
        }
        // line 156
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/reports.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  262 => 156,  256 => 154,  254 => 153,  251 => 152,  247 => 151,  221 => 126,  218 => 125,  207 => 123,  202 => 122,  200 => 121,  180 => 103,  176 => 102,  171 => 99,  167 => 98,  120 => 53,  116 => 52,  70 => 8,  66 => 7,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}

{% block title %}Pivot- รายงาน {% endblock %}

{% block menu_5 %} active {% endblock %}

{% block styleReady %}
    #btn_exports:hover{
        color: #FFFFFF;
        background-color: #055d97;
    }

    #btn_exports{
        border-color: #0074c0;
        color: #FFFFFF;
        background-color: #0074c0;
    }

    #detail_sender_head h4{
        text-align:left;
        color: #006cb7;
        border-bottom: 3px solid #006cb7;
        display: inline;
    }

    #detail_sender_head {
        border-bottom: 3px solid #eee;
        margin-bottom: 20px;
        margin-top: 20px;
    }

    #detail_receiver_head h4{
        text-align:left;
        color: #006cb7;
        border-bottom: 3px solid #006cb7;
        display: inline;
    }

    #detail_receiver_head {
        border-bottom: 3px solid #eee;
        margin-bottom: 20px;
        margin-top: 40px;
    }

    .input-group-addon{
        background-color: #FFFFFF;
        border: 1px solid #055d97;
    }

{% endblock %}

{% block domReady %}
    // \$('#month_success').select2({
    //     data: [
    //         { id: 1, text: 'text' }
    //     ]
    // });

    \$('#btn_exports').click(function() {
        var months = \$('#month_success').val();
        var years = \$('#year_success').val();
        var status = true;

        if(months == \"\" || months == null) {
            status = false;
        }

        if (years == \"\" || years == null) {
            status = false;
        }

        if(!status) {
             alertify.alert('ยังไม่เลือกเงื่อนไข');
        } else {
            window.open('export_dateSuccess.php?month='+months+\"&year=\"+years, \"_blank\")
        }
        
    });


    \$('#year_success').on('select2:select', function (e) {
        \$('#month_success').empty().trigger('change')
        \$.ajax({
            url: \"./ajax/ajax_getMonthSuccess.php\",
            type: \"POST\",
            data: {
                year: e.params.data.text
            },
            dataType: 'json',
            success: function(res) {
                \$('#month_success').select2(res).trigger('change');
            }
        })
    });

{% endblock %}

{% block javaScript %}

{% endblock %}

{% block Content %}
<div class=\"container\">
    <div style=\"text-align:center; color:#0074c0; margin-top:20px;\">
        <label>
            <h3><b>รายงานรอบนำส่งเอกสาร</b></h3>
        </label>
    </div>

    <div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
        <h4>รายงานรอบนำส่งเอกสาร (ประวัติการนำส่ง) </h4>
    </div>

    <div class=\"row\">
        <div class=\"col-md-6\">
            <div class=\"form-group row\">
                <label for=\"date_success\" class=\"col-sm-2 font-weight-bold\">ปี:</label>
                <div class=\"col-sm-10\">
                    <select class=\"form-control form-control-sm\" name=\"yearSuccess\" id=\"year_success\">
                        <option value=\"\">--- เลือกปี ---</option>
                        {% if years|length > 0 %}
                            {% for year in years %}
                                <option value=\"{{ year }}\">{{ year }}</option>
                            {% endfor %}
                        {% endif %}
                    </select>
                </div>
            </div>
        </div>
        <div class=\"col-md-6\">
            <div class=\"form-group row\">
                <label for=\"date_success\" class=\"col-sm-2 font-weight-bold\">เดือน:</label>
                <div class=\"col-sm-10\">
                    <select class=\"form-control form-control-sm\" name=\"monthSuccess\" id=\"month_success\">
                        <option value=\"\">--- เลือกเดือน ---</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
     <div class=\"float-right\">
            <button type=\"button\" class=\"btn btn-outline-primary\" id=\"btn_exports\">ออกรายงาน</button>
    </div>
    </div>
    


{% endblock %}


{% block debug %}

{% if debug != '' %}
<pre>{{ debug }}</pre>
{% endif %}

{% endblock %}
", "branch/reports.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\reports.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/send_branch_list.tpl */
class __TwigTemplate_85f904dd67cbb4500b32c7b6ab20d272 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg3' => [$this, 'block_menu_msg3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/send_branch_list.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PivotSend List ";
    }

    // line 5
    public function block_menu_msg3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
\t#loading{
\t\t\tdisplay: block;
\t\t\tmargin-left: auto;
\t\t\tmargin-right: auto;
\t\t\twidth: 50%;
\t\t
\t}
";
    }

    // line 105
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 106
        echo "    \$('.block_btn').hide();

    var receive_list = {};
    getSendData();

    

";
    }

    // line 115
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 116
        echo "    var selectChoice = [];

    function getSendData(branch_id)
    {
        if(!branch_id){ 
            \$('.block_btn').hide();
        }

        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: \"GET\",
            cache: false,
            data: {
                type: 'send',
                branch_id: branch_id
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            },
            complete: function() {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }


    function confirmApprove(wId)
    {
        var data = {
            type: 'one', 
            action: 1,
            main_id: wId
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function confirmCancel(wId) 
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel',
                wId: wId
            },
            dataType: 'json',
            success: function (resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })

    }

    function checkChoice() {

        \$('.check_all').each(function (i, elm) {
            var wId = parseInt(\$('#' + elm.id).attr('data-value'));
            var foundArr = selectChoice.indexOf(wId);

  
            \$(elm).prop('checked', !elm.checked);
    
            if (elm.checked) {
                if (foundArr == -1) {
                    selectChoice.push(wId)
                } 
            } else {
                if (foundArr != -1) {
                    selectChoice.splice(foundArr, 1);
                }
            }
        });
       
        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }

    }

    function selectedCard(wId, elm)
    {
        var foundArr = selectChoice.indexOf(wId);
        if(\$('#'+elm.id).is(':checked')) {
            if (foundArr == -1) {
                selectChoice.push(wId)
            }
        } else {
            if (foundArr != -1) {
                selectChoice.splice(foundArr, 1);
            }
        }

        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }
        console.log(selectChoice)
    }

    function sendAll()
    {
        var data = {
            type: 'all', 
            action: 1,
            wId: selectChoice
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function cancelAll()
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel_all',
                wId: JSON.stringify(selectChoice)
            },
            dataType: 'json',
            success: function(resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })
    }

";
    }

    // line 276
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 277
        echo "<div class=\"content_bch\">
<h4 class=\"text-center text-muted\">ส่งเอกสารที่สาขา </h4>
    <div class=\"header_bch\">
        <div class=\"form-group\">
            <p class=\"font-weight-bold text-muted\">ทั้งหมด <span id=\"counter_works\">0</span> งาน </p>
            <select name=\"lst_branch\" id=\"lst_branch\" class=\"form-control\" onchange=\"getSendData(this.value);\">
                <option value=\"\">-- เลือกสาขา --</option>
                ";
        // line 284
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 285
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 285), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 285), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 287
        echo "            </select>
        </div>
    </div>
    <button type=\"button\" class=\"btn btn-secondary btn-block my-2\" id=\"btn_checked\" onclick=\"checkChoice();\">เลือกทั้งหมด</button>
    <div class=\"block_btn my-2\">
        <button type=\"button\" id=\"btn_receive\" class=\"btn btn-success btn-block\" onclick=\"sendAll();\">ส่งเอกสาร</button>
        <button type=\"button\" id=\"btn_not_found\" class=\"btn btn-warning btn-block\" onclick=\"cancelAll();\">ไม่พบผู้รับ</button>
    </div>
\t<img id=\"loading\" src=\"../themes/images/loading.gif\">
    <div class=\"result_bch\">
        
    </div>
</div>
<div class=\"footer_bch\">
    
</div>
";
    }

    // line 306
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 308
        if ((($context["debug"] ?? null) != "")) {
            // line 309
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 311
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/send_branch_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  418 => 311,  412 => 309,  410 => 308,  403 => 306,  383 => 287,  372 => 285,  368 => 284,  359 => 277,  355 => 276,  193 => 116,  189 => 115,  178 => 106,  174 => 105,  77 => 10,  73 => 9,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
\t#loading{
\t\t\tdisplay: block;
\t\t\tmargin-left: auto;
\t\t\tmargin-right: auto;
\t\t\twidth: 50%;
\t\t
\t}
{% endblock %}

{% block domReady %}
    \$('.block_btn').hide();

    var receive_list = {};
    getSendData();

    

{% endblock %}

{% block javaScript %}
    var selectChoice = [];

    function getSendData(branch_id)
    {
        if(!branch_id){ 
            \$('.block_btn').hide();
        }

        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: \"GET\",
            cache: false,
            data: {
                type: 'send',
                branch_id: branch_id
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            },
            complete: function() {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }


    function confirmApprove(wId)
    {
        var data = {
            type: 'one', 
            action: 1,
            main_id: wId
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function confirmCancel(wId) 
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel',
                wId: wId
            },
            dataType: 'json',
            success: function (resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })

    }

    function checkChoice() {

        \$('.check_all').each(function (i, elm) {
            var wId = parseInt(\$('#' + elm.id).attr('data-value'));
            var foundArr = selectChoice.indexOf(wId);

  
            \$(elm).prop('checked', !elm.checked);
    
            if (elm.checked) {
                if (foundArr == -1) {
                    selectChoice.push(wId)
                } 
            } else {
                if (foundArr != -1) {
                    selectChoice.splice(foundArr, 1);
                }
            }
        });
       
        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }

    }

    function selectedCard(wId, elm)
    {
        var foundArr = selectChoice.indexOf(wId);
        if(\$('#'+elm.id).is(':checked')) {
            if (foundArr == -1) {
                selectChoice.push(wId)
            }
        } else {
            if (foundArr != -1) {
                selectChoice.splice(foundArr, 1);
            }
        }

        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }
        console.log(selectChoice)
    }

    function sendAll()
    {
        var data = {
            type: 'all', 
            action: 1,
            wId: selectChoice
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function cancelAll()
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel_all',
                wId: JSON.stringify(selectChoice)
            },
            dataType: 'json',
            success: function(resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })
    }

{% endblock %}

{% block Content %}
<div class=\"content_bch\">
<h4 class=\"text-center text-muted\">ส่งเอกสารที่สาขา </h4>
    <div class=\"header_bch\">
        <div class=\"form-group\">
            <p class=\"font-weight-bold text-muted\">ทั้งหมด <span id=\"counter_works\">0</span> งาน </p>
            <select name=\"lst_branch\" id=\"lst_branch\" class=\"form-control\" onchange=\"getSendData(this.value);\">
                <option value=\"\">-- เลือกสาขา --</option>
                {% for b in branch %}
                    <option value=\"{{ b.mr_branch_id }}\">{{ b.mr_branch_name }}</option>
                {% endfor %}
            </select>
        </div>
    </div>
    <button type=\"button\" class=\"btn btn-secondary btn-block my-2\" id=\"btn_checked\" onclick=\"checkChoice();\">เลือกทั้งหมด</button>
    <div class=\"block_btn my-2\">
        <button type=\"button\" id=\"btn_receive\" class=\"btn btn-success btn-block\" onclick=\"sendAll();\">ส่งเอกสาร</button>
        <button type=\"button\" id=\"btn_not_found\" class=\"btn btn-warning btn-block\" onclick=\"cancelAll();\">ไม่พบผู้รับ</button>
    </div>
\t<img id=\"loading\" src=\"../themes/images/loading.gif\">
    <div class=\"result_bch\">
        
    </div>
</div>
<div class=\"footer_bch\">
    
</div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}

", "messenger/send_branch_list.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\messenger\\send_branch_list.tpl");
    }
}

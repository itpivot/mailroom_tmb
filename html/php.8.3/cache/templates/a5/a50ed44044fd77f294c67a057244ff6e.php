<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/seting.postPrice.tpl */
class __TwigTemplate_c784e1b4c5aef673c940a4e23376c78a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/seting.postPrice.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 25
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t

var tbl_floor = \$('#tb_floor').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_type_post_name'},
\t\t{'data': 'min_weight'},
\t\t{'data': 'max_weight'},
\t\t{'data': 'description'},
\t\t{'data': 'post_price'},
\t\t{'data': 'action'}
    ]
});



  \$('#mr_type_post_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});

  \$('#add_type_post_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});


\$( \"#min_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight;
\t//console.log(description);
\t\$( \"#description_weight\" ).val(description);
  });
\$( \"#max_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight
\t\$( \"#description_weight\" ).val(description);
  });


  
  \$('#myform_data_price').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'add_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'min_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'max_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'description_weight': {
\t\t\trequired: true,
\t\t},
\t\t'post_price': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t  
\t},
\tmessages: {
\t\t'add_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภท',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'min_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'max_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'description_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'post_price': {
\t\t  required: 'กรุณาระบุ ราคาไปรษณีย์ไทย',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });

  \$('#btn-edit-price').click(function() {
\tif(\$('#myform_data_price').valid()) {
\t \tvar form = \$('#myform_data_price');
\t \tvar csrf_token = \$('#csrf_token').val();
      \tvar serializeData = form.serialize()+\"&page=addPrice&addType=edit&csrf_token=\"+csrf_token;
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t\$('#modaiAddPrice').modal('hide')
\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\$('#myform_data_price')[0].reset();
\t\t\t\t\tload_dataPrice();
\t\t\t\t\t
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});




\$('#btn-add-price').click(function() {
\tif(\$('#myform_data_price').valid()) {
\t \tvar form = \$('#myform_data_price');
\t \tvar csrf_token = \$('#csrf_token').val();
      \tvar serializeData = form.serialize()+\"&page=addPrice&csrf_token=\"+csrf_token;
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t\$('#modaiAddPrice').modal('hide')
\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\$('#myform_data_price')[0].reset();
\t\t\t\t\tload_dataPrice();
\t\t\t\t\t
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$('#btn-modaiAddPrice').click(function() {
\t\$('#add_type_post_id').val('').trigger('change');
\t\$('#modaiAddPrice').modal({
\t\tkeyboard: false,backdrop:'static'
\t});
\t\$('#myform_data_price')[0].reset();
\t\$('.div-btn-edit').hide();
\t\$('.div-btn-add').show();
\t
});



load_dataPrice();
";
    }

    // line 298
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 299
        echo "


function load_dataPrice() {
var data =  \$('#myform_data_floor').serialize()+\"&page=loadPrice\";
//console.log(data);
\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token']);
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_floor').DataTable().clear().draw();
\t\t\t\$('#tb_floor').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function remove_price(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อลบ',
\tfunction(){ 
\t\tvar csrf_token = \$('#csrf_token').val();
\t\t\$.ajax({
\t\t\t\tmethod: \"POST\",
\t\t\t\tdataType:'json',
\t\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\t\tdata:{
\t\t\t\t\tid \t\t\t: id,
\t\t\t\t\tpage \t\t: 'remove',
\t\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t\t},
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t// setting a timeout
\t\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t},
\t\t\t\terror: function (error) {
\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t// location.reload();
\t\t\t\t}
\t\t\t})
\t\t\t.done(function( res ) {
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\t\tload_dataPrice();
\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t});
\t\t}, function(){ 
\t\t\talertify.error('Cancel')
\t\t});
}



function edit_price(id) {
\t\$('#mr_post_price_id').val(id);
\tvar csrf_token = \$('#csrf_token').val();
\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata:{
\t\t\t\tid \t\t\t: id,
\t\t\t\tpage \t\t: 'loadPrice',
\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\talert('error; ' + eval(error));
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t})
\t\t.done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t//console.log(res);
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t});
\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\tif(res['is_empty'] == 1){
\t\t\t\t\t\tvar data = res['data'][0];
\t\t\t\t\t\tconsole.log(data);
\t\t\t\t\t\t\$('.div-btn-add').hide();
\t\t\t\t\t\t\$('.div-btn-edit').show();
\t\t\t\t\t\t\$('#modaiAddPrice').modal({
\t\t\t\t\t\t\tkeyboard: false,
\t\t\t\t\t\t\tbackdrop:'static'
\t\t\t\t\t\t});
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t\$('#add_type_post_id').val(data['mr_type_post_id']).trigger('change');
\t\t\t\t\t\t\$('#min_weight').val(data['min_weight']);
\t\t\t\t\t\t\$('#max_weight').val(data['max_weight']);
\t\t\t\t\t\t\$('#description_weight').val(data['description']);
\t\t\t\t\t\t\$('#post_price').val(data['post_price']);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\" ไม่พบข้อมูล \"
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t

\t\t\t\t}else{
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t});
\t\t\t\t}
\t\t});
}

function setpriceonly(id) {
\tvar price = \$(\"#txt-set-price-\"+id).val();
\tif(price<=0){
\t\talertify.alert('ผิดพลาด',\"  กรุณาระบุ ราคามากกว่า  0 บาท\"
\t\t\t\t\t,function(){
\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t});
\t\treturn;
\t}
\tvar csrf_token = \$('#csrf_token').val();
\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata:{
\t\t\t\tid \t\t\t: id,
\t\t\t\tpage \t\t: 'setpriceonly',
\t\t\t\tprice \t\t: price,
\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\talert('error; ' + eval(error));
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t})
\t\t.done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t//console.log(res);
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t});
\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\tload_dataPrice();
\t\t\t\t}else{
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t});
\t\t\t\t}
\t\t});
}

function check_click(id) {
\t\$('#btn-set-save-'+id).show();
\t\$('#btn-reset-save-'+id).show();
\t\$('#btn-set-change-'+id).hide();
\t\$(\"#txt-set-price-\"+id).prop('readonly', false);
}
function resetfrome(id) {
\tload_dataPrice();
}


";
    }

    // line 510
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 511
        echo "<div  class=\"container-fluid\">
\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"";
        // line 512
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">
<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">



\t<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class=\"modal fade\" id=\"modaiAddPrice\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modaiAddPriceTitle\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">เพิ่มราคาใหม่</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t  </button>
\t\t</div>
\t\t<div class=\"modal-body\">
\t\t\t<form id=\"myform_data_price\">
\t\t\t\t<input type=\"hidden\" name=\"mr_post_price_id\" id=\"mr_post_price_id\" value=\"\">
\t\t\t<div class=\"row\">
\t\t\t<div class=\"form-group col-12\">
\t\t\t\t<h5 class=\"card-title\">ประเภท <span class=\"box_error\" id=\"err_mr_type_post_id\"></span></h5>
\t\t\t\t<select  name=\"add_type_post_id\" class=\"form-control \" id=\"add_type_post_id\"  data-error=\"#err_mr_type_post_id\">
\t\t\t\t<option value=\"\" selected>ประเภทการส่ง</option>
\t\t\t\t\t";
        // line 538
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["type_postData"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 539
            echo "\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_type_post_id", [], "any", false, false, false, 539), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_type_post_name", [], "any", false, false, false, 539), "html", null, true);
            echo "</option>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 541
        echo "\t\t\t\t</select>
\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 col-md-6\">
\t\t\t\t\t<label for=\"min_weight\">น้ำหนักตั้งแต่/กรัม<span class=\"box_error\" id=\"err_min_weight\"></span></label>
\t\t\t\t\t<input id=\"min_weight\" name=\"min_weight\" data-error=\"#err_min_weight\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12 col-md-6\">
\t\t\t\t\t
\t\t\t\t\t<label for=\"max_weight\">น้ำหนักสูงสุด/กรัม<span class=\"box_error\" id=\"err_max_weight\"></span></label>
\t\t\t\t\t<input id=\"max_weight\" name=\"max_weight\" data-error=\"#err_max_weight\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 col-md-12\">
\t\t\t\t\t
\t\t\t\t\t<label for=\"description\">รายละเอียด<span class=\"box_error\" id=\"err_description\"></span></label>
\t\t\t\t\t<input id=\"description_weight\" name=\"description_weight\" data-error=\"#err_description\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\" readonly>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12 col-md-12\">
\t\t\t\t\t<label for=\"post_price\">ราคา<span class=\"box_error\" id=\"err_post_price\"></span></label>
\t\t\t\t\t<input id=\"post_price\" name=\"post_price\" data-error=\"#err_post_price\"  class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t</form>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-add\">
\t\t  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t  <button id=\"btn-add-price\" type=\"button\" class=\"btn btn-primary\">บันทึก</button>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-edit\">
\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t\t<button id=\"btn-edit-price\" type=\"button\" class=\"btn btn-primary\">แก้ไข</button>
\t\t</div>
\t  </div>
\t</div>
  </div>



\t<br>
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ราคา ไปรษณีย์ไทย.</b></h3></label><br>
\t\t\t\t<label>ราคาไปรษณีย์ไทย > อัปเดทราคา </label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"card\">
\t\t<div class=\"card-body\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<hr>
\t\t\t\t\t<form id=\"myform_data_floor\">
\t\t\t\t\t\t<div class=\"form-group col-12 col-md-3\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">ประเภท</h5>
\t\t\t\t\t\t\t<select  name=\"mr_type_post_id\" class=\"form-control \" id=\"mr_type_post_id\" >
\t\t\t\t\t\t\t<option value=\"\" selected>ประเภทการส่ง</option>
\t\t\t\t\t\t\t\t";
        // line 604
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["type_postData"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 605
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_type_post_id", [], "any", false, false, false, 605), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["type"], "mr_type_post_name", [], "any", false, false, false, 605), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 607
        echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-info\" onclick=\"load_dataPrice()\">
\t\t\t\t\t\t\t<i class=\"material-icons\" style=\"padding-right:5px;\">search</i>
\t\t\t\t\t\t</button>
\t\t\t\t\t
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn-modaiAddPrice\" data-backdrop=\"static\">
\t\t\t\t\t\t\tต้องการเพิ่มราคาใหม่
\t\t\t\t\t\t  </button>
\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_floor\">
\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ประเภท</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">น้ำหนักตั้งแต่/กรัม</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">น้ำหนักสูงสุด/กรัม</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">รายละเอียด</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ราคา/บาท</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t  </table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t </div>

</div>
</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




";
    }

    // line 659
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 661
        if ((($context["debug"] ?? null) != "")) {
            // line 662
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 664
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/seting.postPrice.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  792 => 664,  786 => 662,  784 => 661,  777 => 659,  724 => 607,  713 => 605,  709 => 604,  644 => 541,  633 => 539,  629 => 538,  600 => 512,  597 => 511,  593 => 510,  379 => 299,  375 => 298,  130 => 59,  95 => 26,  91 => 25,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>
\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}\t

var tbl_floor = \$('#tb_floor').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_type_post_name'},
\t\t{'data': 'min_weight'},
\t\t{'data': 'max_weight'},
\t\t{'data': 'description'},
\t\t{'data': 'post_price'},
\t\t{'data': 'action'}
    ]
});



  \$('#mr_type_post_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});

  \$('#add_type_post_id').select2({ 
\ttheme: 'bootstrap4',
\twidth: '100%' 
}).on('select2:select', function (e) {

});


\$( \"#min_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight;
\t//console.log(description);
\t\$( \"#description_weight\" ).val(description);
  });
\$( \"#max_weight\" ).keyup(function() {
\tvar min_weight = \$( \"#min_weight\" ).val();
\tvar max_weight = \$( \"#max_weight\" ).val();
\tvar description = min_weight+\" - \"+max_weight
\t\$( \"#description_weight\" ).val(description);
  });


  
  \$('#myform_data_price').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'add_type_post_id': {
\t\t\trequired: true,
\t\t},
\t\t'min_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'max_weight': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t\t'description_weight': {
\t\t\trequired: true,
\t\t},
\t\t'post_price': {
\t\t\trequired: true,
\t\t\tnumber: true
\t\t},
\t  
\t},
\tmessages: {
\t\t'add_type_post_id': {
\t\t  required: 'กรุณาระบุ ประเภท',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'min_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'max_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'description_weight': {
\t\t  required: 'กรุณาระบุ น้ำหนัก',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t'post_price': {
\t\t  required: 'กรุณาระบุ ราคาไปรษณีย์ไทย',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });

  \$('#btn-edit-price').click(function() {
\tif(\$('#myform_data_price').valid()) {
\t \tvar form = \$('#myform_data_price');
\t \tvar csrf_token = \$('#csrf_token').val();
      \tvar serializeData = form.serialize()+\"&page=addPrice&addType=edit&csrf_token=\"+csrf_token;
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t\$('#modaiAddPrice').modal('hide')
\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\$('#myform_data_price')[0].reset();
\t\t\t\t\tload_dataPrice();
\t\t\t\t\t
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});




\$('#btn-add-price').click(function() {
\tif(\$('#myform_data_price').valid()) {
\t \tvar form = \$('#myform_data_price');
\t \tvar csrf_token = \$('#csrf_token').val();
      \tvar serializeData = form.serialize()+\"&page=addPrice&csrf_token=\"+csrf_token;
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t\$('#modaiAddPrice').modal('hide')
\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\$('#myform_data_price')[0].reset();
\t\t\t\t\tload_dataPrice();
\t\t\t\t\t
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$('#btn-modaiAddPrice').click(function() {
\t\$('#add_type_post_id').val('').trigger('change');
\t\$('#modaiAddPrice').modal({
\t\tkeyboard: false,backdrop:'static'
\t});
\t\$('#myform_data_price')[0].reset();
\t\$('.div-btn-edit').hide();
\t\$('.div-btn-add').show();
\t
});



load_dataPrice();
{% endblock %}
{% block javaScript %}



function load_dataPrice() {
var data =  \$('#myform_data_floor').serialize()+\"&page=loadPrice\";
//console.log(data);
\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\tdata:data,
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token']);
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_floor').DataTable().clear().draw();
\t\t\t\$('#tb_floor').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function remove_price(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อลบ',
\tfunction(){ 
\t\tvar csrf_token = \$('#csrf_token').val();
\t\t\$.ajax({
\t\t\t\tmethod: \"POST\",
\t\t\t\tdataType:'json',
\t\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\t\tdata:{
\t\t\t\t\tid \t\t\t: id,
\t\t\t\t\tpage \t\t: 'remove',
\t\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t\t},
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t// setting a timeout
\t\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t\t},
\t\t\t\terror: function (error) {
\t\t\t\talert('error; ' + eval(error));
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t// location.reload();
\t\t\t\t}
\t\t\t})
\t\t\t.done(function( res ) {
\t\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t\t//console.log(res);
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\talertify.alert('Success',\"  \"+res.message
\t\t\t\t\t\t,function(){\t\t\t\t
\t\t\t\t\t\t\tload_dataPrice();
\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t});
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t});
\t\t}, function(){ 
\t\t\talertify.error('Cancel')
\t\t});
}



function edit_price(id) {
\t\$('#mr_post_price_id').val(id);
\tvar csrf_token = \$('#csrf_token').val();
\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata:{
\t\t\t\tid \t\t\t: id,
\t\t\t\tpage \t\t: 'loadPrice',
\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\talert('error; ' + eval(error));
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t})
\t\t.done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t//console.log(res);
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t});
\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\tif(res['is_empty'] == 1){
\t\t\t\t\t\tvar data = res['data'][0];
\t\t\t\t\t\tconsole.log(data);
\t\t\t\t\t\t\$('.div-btn-add').hide();
\t\t\t\t\t\t\$('.div-btn-edit').show();
\t\t\t\t\t\t\$('#modaiAddPrice').modal({
\t\t\t\t\t\t\tkeyboard: false,
\t\t\t\t\t\t\tbackdrop:'static'
\t\t\t\t\t\t});
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\t\$('#add_type_post_id').val(data['mr_type_post_id']).trigger('change');
\t\t\t\t\t\t\$('#min_weight').val(data['min_weight']);
\t\t\t\t\t\t\$('#max_weight').val(data['max_weight']);
\t\t\t\t\t\t\$('#description_weight').val(data['description']);
\t\t\t\t\t\t\$('#post_price').val(data['post_price']);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\" ไม่พบข้อมูล \"
\t\t\t\t\t\t,function(){
\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t

\t\t\t\t}else{
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t});
\t\t\t\t}
\t\t});
}

function setpriceonly(id) {
\tvar price = \$(\"#txt-set-price-\"+id).val();
\tif(price<=0){
\t\talertify.alert('ผิดพลาด',\"  กรุณาระบุ ราคามากกว่า  0 บาท\"
\t\t\t\t\t,function(){
\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t});
\t\treturn;
\t}
\tvar csrf_token = \$('#csrf_token').val();
\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_seting.postPrice.php\",
\t\t\tdata:{
\t\t\t\tid \t\t\t: id,
\t\t\t\tpage \t\t: 'setpriceonly',
\t\t\t\tprice \t\t: price,
\t\t\t\tcsrf_token \t: csrf_token,
\t\t\t},
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\talert('error; ' + eval(error));
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t})
\t\t.done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\t\tif(res['status'] == 505){
\t\t\t\t\t//console.log(res);
\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\t//window.location.reload();
\t\t\t\t\t});
\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\t\t\tload_dataPrice();
\t\t\t\t}else{
\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t\t,function(){
\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t});
\t\t\t\t}
\t\t});
}

function check_click(id) {
\t\$('#btn-set-save-'+id).show();
\t\$('#btn-reset-save-'+id).show();
\t\$('#btn-set-change-'+id).hide();
\t\$(\"#txt-set-price-\"+id).prop('readonly', false);
}
function resetfrome(id) {
\tload_dataPrice();
}


{% endblock %}
{% block Content2 %}
<div  class=\"container-fluid\">
\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"{{csrf_token}}\">
<div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">



\t<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class=\"modal fade\" id=\"modaiAddPrice\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modaiAddPriceTitle\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">เพิ่มราคาใหม่</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t  </button>
\t\t</div>
\t\t<div class=\"modal-body\">
\t\t\t<form id=\"myform_data_price\">
\t\t\t\t<input type=\"hidden\" name=\"mr_post_price_id\" id=\"mr_post_price_id\" value=\"\">
\t\t\t<div class=\"row\">
\t\t\t<div class=\"form-group col-12\">
\t\t\t\t<h5 class=\"card-title\">ประเภท <span class=\"box_error\" id=\"err_mr_type_post_id\"></span></h5>
\t\t\t\t<select  name=\"add_type_post_id\" class=\"form-control \" id=\"add_type_post_id\"  data-error=\"#err_mr_type_post_id\">
\t\t\t\t<option value=\"\" selected>ประเภทการส่ง</option>
\t\t\t\t\t{% for type in type_postData %}
\t\t\t\t\t<option value=\"{{type.mr_type_post_id}}\"> {{type.mr_type_post_name}}</option>
\t\t\t\t\t{% endfor %}
\t\t\t\t</select>
\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 col-md-6\">
\t\t\t\t\t<label for=\"min_weight\">น้ำหนักตั้งแต่/กรัม<span class=\"box_error\" id=\"err_min_weight\"></span></label>
\t\t\t\t\t<input id=\"min_weight\" name=\"min_weight\" data-error=\"#err_min_weight\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12 col-md-6\">
\t\t\t\t\t
\t\t\t\t\t<label for=\"max_weight\">น้ำหนักสูงสุด/กรัม<span class=\"box_error\" id=\"err_max_weight\"></span></label>
\t\t\t\t\t<input id=\"max_weight\" name=\"max_weight\" data-error=\"#err_max_weight\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 col-md-12\">
\t\t\t\t\t
\t\t\t\t\t<label for=\"description\">รายละเอียด<span class=\"box_error\" id=\"err_description\"></span></label>
\t\t\t\t\t<input id=\"description_weight\" name=\"description_weight\" data-error=\"#err_description\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\" readonly>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12 col-md-12\">
\t\t\t\t\t<label for=\"post_price\">ราคา<span class=\"box_error\" id=\"err_post_price\"></span></label>
\t\t\t\t\t<input id=\"post_price\" name=\"post_price\" data-error=\"#err_post_price\"  class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t</form>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-add\">
\t\t  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t  <button id=\"btn-add-price\" type=\"button\" class=\"btn btn-primary\">บันทึก</button>
\t\t</div>
\t\t<div class=\"modal-footer div-btn-edit\">
\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
\t\t\t<button id=\"btn-edit-price\" type=\"button\" class=\"btn btn-primary\">แก้ไข</button>
\t\t</div>
\t  </div>
\t</div>
  </div>



\t<br>
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ราคา ไปรษณีย์ไทย.</b></h3></label><br>
\t\t\t\t<label>ราคาไปรษณีย์ไทย > อัปเดทราคา </label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"card\">
\t\t<div class=\"card-body\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<hr>
\t\t\t\t\t<form id=\"myform_data_floor\">
\t\t\t\t\t\t<div class=\"form-group col-12 col-md-3\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">ประเภท</h5>
\t\t\t\t\t\t\t<select  name=\"mr_type_post_id\" class=\"form-control \" id=\"mr_type_post_id\" >
\t\t\t\t\t\t\t<option value=\"\" selected>ประเภทการส่ง</option>
\t\t\t\t\t\t\t\t{% for type in type_postData %}
\t\t\t\t\t\t\t\t<option value=\"{{type.mr_type_post_id}}\"> {{type.mr_type_post_name}}</option>
\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-info\" onclick=\"load_dataPrice()\">
\t\t\t\t\t\t\t<i class=\"material-icons\" style=\"padding-right:5px;\">search</i>
\t\t\t\t\t\t</button>
\t\t\t\t\t
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"btn-modaiAddPrice\" data-backdrop=\"static\">
\t\t\t\t\t\t\tต้องการเพิ่มราคาใหม่
\t\t\t\t\t\t  </button>
\t\t\t\t\t</form>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t<table class=\"table table-hover\" id=\"tb_floor\">
\t\t\t\t\t\t<thead class=\"thead-light\">
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ประเภท</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">น้ำหนักตั้งแต่/กรัม</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">น้ำหนักสูงสุด/กรัม</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">รายละเอียด</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">ราคา/บาท</th>
\t\t\t\t\t\t\t<th width=\"10%\" scope=\"col\">จัดการ</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t  </table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t </div>

</div>
</div>







<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/seting.postPrice.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\seting.postPrice.tpl");
    }
}

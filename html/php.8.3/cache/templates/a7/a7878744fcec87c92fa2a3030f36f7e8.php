<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/work_info.tpl */
class __TwigTemplate_caf94cfb91b3367f9955e82555af3a9e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_2' => [$this, 'block_menu_2'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/work_info.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "#btn_update:hover,
#btn_cancel:hover,
#btn_back:hover {
    color: #FFFFFF;
    background-color: #055d97;
}


#btn_update,
#btn_cancel,
#btn_back {
    border-color: #0074c0;
    color: #FFFFFF;
    background-color: #0074c0;
}

#detail_sender_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_sender_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 20px;
}

#workNumber {
    color: #006cb7;
}

#detail_receiver_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_receiver_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 40px;
}
.divTimes{
\tposition: absolute;
    top: -30px;
\twidth: 100%;
    //right: -20px;
\t//transform: translateX(-5px) rotateZ(-45deg);
}

.coloe_compress:before{
\tcolor:#fff;
\tbackground-color:#2196F3 !important ;
\t
}
.multi-steps > li.is-active:before, .multi-steps > li.is-active ~ li:before {
  content: counter(stepNum);
  font-family: inherit;
  font-weight: 700;
}
.multi-steps > li.is-active:after, .multi-steps > li.is-active ~ li:after {
  background-color: #ededed;
}

.multi-steps {
  display: table;
  table-layout: fixed;
  width: 100%;
}
.multi-steps > li {
  counter-increment: stepNum;
  text-align: center;
  display: table-cell;
  position: relative;
  color: #2196F3;
}
.multi-steps > li:before {
  content: counter(stepNum);
  display: block;
  margin: 0 auto 4px;
  background-color: #fff;
  width: 36px;
  height: 36px;
  line-height: 32px;
  text-align: center;
  font-weight: bold;
  border-width: 2px;
  border-style: solid;
  border-color: #2196F3;
  border-radius: 50%;
}
.multi-steps > li:after {
  content: '';
  height: 2px;
  width: 100%;
  background-color: #2196F3 ;
  position: absolute;
  top: 16px;
  left: 50%;
  z-index: -1;
}
.multi-steps > li:last-child:after {
  display: none;
}
.multi-steps > li.is-active:before {
  background-color: #fff;
  border-color: #2196F3 ;
}
.multi-steps > li.is-active ~ li {
  color: #808080;
}
.multi-steps > li.is-active ~ li:before {
  background-color: #ededed;
  border-color: #ededed;
}

";
    }

    // line 129
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 130
        echo "moment.locale('th');

var init = { id: '";
        // line 132
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_id", [], "any", false, false, false, 132), "html", null, true);
        echo "', text: '";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_name", [], "any", false, false, false, 132), "html", null, true);
        echo "' };
var init_replace = { id: '";
        // line 133
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_id", [], "any", false, false, false, 133), "html", null, true);
        echo "', text: '";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_name", [], "any", false, false, false, 133), "html", null, true);
        echo "' };
";
        // line 134
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 134) == 2)) {
            // line 135
            echo "var type_work = '2';
";
        } else {
            // line 137
            echo "var type_work = '1';
";
        }
        // line 139
        echo "
var mr_branch_floor = '";
        // line 140
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 140), "html", null, true);
        echo "';
var real_branch_floor = '";
        // line 141
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 141), "html", null, true);
        echo "';

var branch_id = '";
        // line 143
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_branch_id", [], "any", false, false, false, 143), "html", null, true);
        echo "';
var real_branch_id = '";
        // line 144
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_branch_id3", [], "any", false, false, false, 144), "html", null, true);
        echo "';
";
        // line 145
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_floor_id", [], "any", false, false, false, 145) == "")) {
            // line 146
            echo "\tvar floor_id  = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_floor_id", [], "any", false, false, false, 146), "html", null, true);
            echo "';
";
        } else {
            // line 148
            echo "\tvar floor_id  = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_floor_id", [], "any", false, false, false, 148), "html", null, true);
            echo "';
";
        }
        // line 150
        echo "var receiver_id = '";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_id", [], "any", false, false, false, 150), "html", null, true);
        echo "';

var real_receive_id = '";
        // line 152
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_id", [], "any", false, false, false, 152), "html", null, true);
        echo "'
var real_receiver_name = '";
        // line 153
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_name", [], "any", false, false, false, 153), "html", null, true);
        echo "';

var receiver_name = '";
        // line 155
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_name", [], "any", false, false, false, 155), "html", null, true);
        echo "';
var status = '";
        // line 156
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_status_id", [], "any", false, false, false, 156), "html", null, true);
        echo "';

// console.log(d.days()+\" วัน \"+ d.hours()+\" ชม. \"+d.minutes()+\" นาที \"+ d.seconds() + \" วินาที\");

// initial Page
initialPage(status);
setForm(init);
setFormReplace(init_replace);
check_type_send(parseInt(type_work));


//console.log(hoursArr);
//calculateDuration(timeArr,hoursArr);


if (!!branch_id) {
    \$('#branch_send').val(parseInt(branch_id)).trigger('change');
}

if (!!real_branch_id) {
    \$('#real_branch_send').val(parseInt(real_branch_id)).trigger('change');
}

if (!!floor_id) {
    \$('#floor_send').val(parseInt(branch_id)).trigger('change');
}
if (!!mr_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}

if (!!real_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}


\$(\".alert\").alert();

\$('#name_receiver_select').select2({
    placeholder: \"ค้นหาผู้รับ\",
   
    ajax: {
        url: \"ajax/ajax_getdataemployee_select.php\",
        dataType: \"json\",
        delay: 250,
        processResults: function (data) {
            return {
                results : data
            };
        },
        cache: true
    },
    
}).on('select2:select', function(e) {
    //console.log(e.params.data)
    setForm(e.params.data);
});

var frmValid = \$('#frmBranch').validate({
    onsubmit: false,
    onkeyup: false,
    errorClass: \"is-invalid\",
    highlight: function (element) {
        if (element.type == \"radio\" || element.type == \"checkbox\") {
            \$(element).removeClass('is-invalid')
        } else {
            \$(element).addClass('is-invalid')
        }
    },
    rules: {
        telSender: {
            required: false
        },
        type_send: {
            required: true
        },
        nameReceiver: {
            required: true
        },
        telReceiver: {
            required: false
        },
        floorRealReceiver: {
            required: {
                depends: function (elm) {
                    var f = \$('#floor_name').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if( (f == \" - \" || f == \"\") && type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        branchRealReceiver: {
            required: {
                depends: function (elm) {
                    var b = \$('#branch_receiver').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if((b == \" - \" || b == \"\") && type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topic: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topicBranch: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
    },
    messages: {
        telSender: {
            required: 'ระบุเบอร์ติดต่อ'
        },
        type_send: {
            required: 'ระบุประเภทผู้รับ'
        },
        nameReceiver: {
            required: 'กรุณาเลือกผู้รับ'
        },
        telReceiver: {
            required: 'ระบุเบอร์ติดต่อผู้รับ'
        },
        branchRealReceiver: {
            required: 'ระบุสาขาผู้รับ'
        },
        topic: {
            required: 'ระบุชื่อเอกสาร'
        },
        topicBranch: {
            required: 'ระบุชื่อเอกสาร'
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        var placement = \$(element).data('error');
        if (placement) {
            \$(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});

\$('#btn_update').click(function() {
     var _frm = \$('#frmBranch');
    if(\$('#frmBranch').valid()) {
       
        \$.ajax({
            url: _frm.attr('action'),
            type: 'POST',
            data: {
                form: _frm.serializeArray(),
                actions: 'update',
                wid: '";
        // line 332
        echo twig_escape_filter($this->env, ($context["wid"] ?? null), "html", null, true);
        echo "'
            },
            dataType: 'json',
            success: function(res) {
                if(res.status == \"success\") {
                    alertify.alert(res['messeges'], function() {
                        location.reload();
                    });
                }
            }
        })
    }
});


\$(\"#btn_cancel\").click(function(){
    alertify.confirm('ยืนยันจะยกเลิกรายการนำส่งนี้', function() {
        \$.post('./ajax/ajax_updateWorkInfo.php', { actions: 'cancel', wid: '";
        // line 349
        echo twig_escape_filter($this->env, ($context["wid"] ?? null), "html", null, true);
        echo "'}, function(res){
            if(res.status == \"success\") {
                        alertify.alert(res['messeges'], function() {
                            location.reload();
                        });
                    }
        }, 'json');
    })
   
});


";
    }

    // line 365
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 366
        echo "

// \$('#type_send_1').prop('checked',true);

function check_type_send( type_send ){
    if( type_send == 1 ){
        \$(\"#type_1\").show();
        \$(\"#type_2\").hide();

        \$('#type_send_1').prop('checked', true);
        \$('#type_send_2').prop('checked', false);
    }else{
        \$(\"#type_1\").hide();
        \$(\"#type_2\").show();
        
        \$('#type_send_2').prop('checked', true);
        \$('#type_send_1').prop('checked', false);
    }
}

function check_name_re() {
    var pass_emp = \$(\"#pass_emp\").val();
    if(pass_emp == \"\" || pass_emp == null){
        \$(\"#div_re_text\").hide();
        \$(\"#div_re_select\").show();
    }else{
        \$(\"#div_re_text\").show();
        \$(\"#div_re_select\").hide();
    }
}

function setForm(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            \$(\"#depart_receiver\").val(res['department']);
            \$(\"#emp_id\").val(res['mr_emp_id']);
            \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#place_receiver\").val(res['mr_workarea']);
            \$(\"#floor_name\").val(res['mr_department_floor']);
            \$(\"#branch_receiver\").val(res['branch']);
        }
    })
}

function setFormReplace(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            console.log(res);
            // \$(\"#depart_receiver\").val(res['department']);
            // \$(\"#emp_id\").val(res['mr_emp_id']);
            // \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#real_place_receiver\").val(res['mr_workarea']);
            \$(\"#real_floor_name\").val(res['mr_department_floor']);
            \$(\"#real_branch_receiver\").val(res['branch']);
        }
    })
}

var saveBranch = function (obj) {
    return \$.post('ajax/ajax_save_work_branch.php', obj);
}

function initialPage(status) {
    //console.log(status)
    if(parseInt(status) != 7) {
        \$('input').prop('disabled', true);
        \$('select').prop('disabled', true);
        \$('textarea').prop('disabled', true);
        \$('#btn_update').prop('disabled', true);
        \$('#btn_cancel').prop('disabled', true);
    }
}

";
    }

    // line 456
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 457
        echo "
<div class=\"container\">



<br>
<br>
<br>

  
  
  
  





    <div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
        <label>
            <h3><b>รายละเอียดรายการนำส่ง</b></h3>
        </label>
    </div>
    <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 480
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 480), "html", null, true);
        echo "\">
    <input type=\"hidden\" id=\"emp_id\" value=\"\">
    <input type=\"hidden\" id=\"barcode\" name=\"barcode\" value=\"";
        // line 482
        echo twig_escape_filter($this->env, ($context["barcode"] ?? null), "html", null, true);
        echo "\">
    
    <div>
        <p class=\"font-weight-bold\"><span class=\"text-muted\">เลขที่เอกสาร: </span> <span id=\"workNumber\">";
        // line 485
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_work_barcode", [], "any", false, false, false, 485), "html", null, true);
        echo "</span> <span class=\"text-muted\">( ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_status_name", [], "any", false, false, false, 485), "html", null, true);
        echo " )</span></p>
    </div>
    

    <div class=\"form-group\"  id=\"detail_sender_head\">
        <h4>สถานะเอกสาร </h4>
    </div>

    ";
        // line 493
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_status_id", [], "any", false, false, false, 493) != 6)) {
            // line 494
            echo "    <div class=\"demo\"><br><br><br><br>
        <ul class=\"list-unstyled multi-steps\">
\t\t\t ";
            // line 496
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["status"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["st"]) {
                // line 497
                echo "\t\t\t\t\t\t\t
\t\t\t\t ";
                // line 499
                echo "\t\t\t\t\t<li class=\"";
                if ((($context["end_step"] ?? null) == twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 499))) {
                    echo " is-active ";
                }
                if ((twig_get_attribute($this->env, $this->source, $context["st"], "active", [], "any", false, false, false, 499) != "")) {
                    echo " coloe_compress ";
                }
                echo "\">
\t\t\t\t\t\t<strong>";
                // line 500
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["st"], "emp_code", [], "any", false, false, false, 500), "html", null, true);
                echo "</strong><br />
\t\t\t\t\t\t<small>";
                // line 501
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["st"], "name", [], "any", false, false, false, 501), "html", null, true);
                echo "</small>
\t\t\t\t\t\t<div class=\"divTimes\">
\t\t\t\t\t\t\t\t<small>";
                // line 503
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["st"], "date", [], "any", false, false, false, 503), "html", null, true);
                echo "</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['st'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 508
            echo "\t\t\t </ul>
    </div>
    ";
        } else {
            // line 511
            echo "    <div style=\"text-align:center;\">
        <p class=\"text-muted\">
            <i class=\"material-icons\">block</i>
            <br/>
            <strong>ยกเลิกรายการนำส่ง</strong>
        </p>
    </div>
\t<br>
\t<br>
    ";
        }
        // line 521
        echo "        
     <!-- <div class='clearfix'></div> -->
    
     <form action=\"./ajax/ajax_updateWorkInfo.php\" method=\"POST\" id=\"frmBranch\">
        <div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
            <h4>รายละเอียดผู้ส่ง </h4>
        </div>
        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    สาขาผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"branchSender\" id=\"brach_sender\" placeholder=\"ชื่อสาขา\" value=\"";
        // line 534
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_branch_code", [], "any", false, false, false, 534), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_branch_name", [], "any", false, false, false, 534), "html", null, true);
        echo "\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"telSender\" id=\"name_sender_tel\" placeholder=\"เบอร์โทร\" value=\"";
        // line 541
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_tel", [], "any", false, false, false, 541), "html", null, true);
        echo "\" readonly>
                </div>
            </div>
        </div>

        <div class=\"form-group\" id=\"div_re_select\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ชื่อผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"nameSender\" id=\"name_sender\" value=\"";
        // line 552
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_code", [], "any", false, false, false, 552), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_name", [], "any", false, false, false, 552), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data_show"] ?? null), "mr_emp_lastname", [], "any", false, false, false, 552), "html", null, true);
        echo "\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">

                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">

                </div>
            </div>
        </div>

        <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
            <h4>รายละเอียดผู้รับ</h4>
        </div>
        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ค้นหาผู้รับ :
                </div>

                <div class=\"col-2\" style=\"padding-left:0px\">
                    <div class=\"input-group\">
                        <label class=\"btn btn-primary\">
                            <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_1\"
                                value=\"1\" onclick=\"check_type_send(this.value);\"> &nbsp;
                            ส่งสำนักงานใหญ่
                        </label>
                    </div>
                </div>
                <div class=\"col-2\" style=\"padding-left:0px\">
                    <div class=\"input-group\">
                        <label class=\"btn btn-primary\">
                            <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_2\"
                                value=\"2\" onclick=\"check_type_send(this.value);\"> &nbsp;
                            ส่งสาขา
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ค้นหาผู้รับ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <select class=\"form-control-lg\" name=\"nameReceiver\" id=\"name_receiver_select\" style=\"width:100%;\" data-error=\"#err_receiver_select\">
                        ";
        // line 601
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_id", [], "any", false, false, false, 601) != "")) {
            // line 602
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_id", [], "any", false, false, false, 602), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_code", [], "any", false, false, false, 602), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_name", [], "any", false, false, false, 602), "html", null, true);
            echo "</option>
                        ";
        }
        // line 604
        echo "                    </select>
                    <span class=\"box_error\" id=\"err_receiver_select\"></span>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"telReceiver\" id=\"tel_receiver\" value=\"";
        // line 611
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_tel", [], "any", false, false, false, 611), "html", null, true);
        echo "\"
                        placeholder=\"เบอร์โทร\" readonly>
                </div>
            </div>
        </div>

        <div id=\"type_1\" style=\"display:;\">
            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        แผนก :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"departReceiver\" id=\"depart_receiver\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"floorReceiverName\" id=\"floor_name\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>

            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชั้นผู้รับ :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <select class=\"form-control-lg\" name=\"floorRealReceiver\" id=\"floor_send\" style=\"width:100%;\" data-error=\"#err_floor_send\">
                            <option value=\"\"> เลือกชั้นปลายทาง</option>
                            ";
        // line 647
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 648
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_floor_id", [], "any", false, false, false, 648), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "name", [], "any", false, false, false, 648), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 650
        echo "                        </select>
                        <span class=\"box_error\" id=\"err_floor_send\"></span>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"topic\" id=\"topic_head\" placeholder=\"ชื่อเอกสาร\" value=\"";
        // line 657
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_topic", [], "any", false, false, false, 657), "html", null, true);
        echo "\">
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        หมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" name=\"remark\" id=\"remark_head\" placeholder=\"หมายเหตุ\">";
        // line 670
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_work_remark", [], "any", false, false, false, 670), "html", null, true);
        echo "</textarea>
                    </div>
                    <div class=\"col-1\" style=\"padding-left:0px\">

                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                    </div>
                </div>
            </div>
        </div> <!-- end type 1-->

        <div id=\"type_2\" style=\"display:none;\">

            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขา :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"branchReceiver\" id=\"branch_receiver\" placeholder=\"ชื่อสาขา\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สถานที่อยู่ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"placeReceiver\" id=\"place_receiver\" placeholder=\"สถานที่อยู่\"
                            readonly>
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขาผู้รับ :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <select class=\"form-control-lg\" name=\"branchRealReceiver\" id=\"branch_send\" style=\"width:100%;\" data-error=\"#err_branch_send\">
                            <option value=\"\"> เลือกสาขาปลายทาง</option>
                            ";
        // line 715
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 716
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 716), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "branch", [], "any", false, false, false, 716), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 718
        echo "                        </select>
                        <span class=\"box_error\" id=\"err_branch_send\"></span>
                    </div>
\t\t\t\t\t
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"topicBranch\" id=\"topic_branch\" value=\"";
        // line 726
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_topic", [], "any", false, false, false, 726), "html", null, true);
        echo "\"
                            placeholder=\"ชื่อเอกสาร\">
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
\t\t\t\t<div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้นผู้รับ :
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_floor\" style=\"width:100%;\" name=\"mr_branch_floor\">
\t\t\t\t\t\t\t\t\t";
        // line 741
        if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 741) == 1)) {
            // line 742
            echo "                                        <option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t                           
                                    ";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 745
($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 745) == 2)) {
            // line 746
            echo "                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected>ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
                                    ";
        } else {
            // line 750
            echo "                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected>ชั้น 3</option>\t
                                    ";
        }
        // line 753
        echo "\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        หมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" name=\"remarkBranch\" id=\"remark_branch\" placeholder=\"หมายเหตุ\">";
        // line 760
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_work_remark", [], "any", false, false, false, 760), "html", null, true);
        echo "</textarea>
                    </div>
                    <div class=\"col-1\" style=\"padding-left:0px\">

                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                    </div>
                </div>
            </div>
        </div> 
\t\t<!-- end type 2-->

        ";
        // line 773
        if ((($context["received"] ?? null) == "replace")) {
            // line 774
            echo "        <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
            <h4>รายละเอียดผู้รับ (แทน)</h4>
        </div>
         <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ค้นหาผู้รับ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <select class=\"form-control-lg\" name=\"nameReceiver\" id=\"name_receiver_select\" style=\"width:100%;\" data-error=\"#err_receiver_select\">
                        ";
            // line 784
            if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "receive_id", [], "any", false, false, false, 784) != "")) {
                // line 785
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_id", [], "any", false, false, false, 785), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_code", [], "any", false, false, false, 785), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_name", [], "any", false, false, false, 785), "html", null, true);
                echo "</option>
                        ";
            }
            // line 787
            echo "                    </select>
                    <span class=\"box_error\" id=\"err_receiver_select\"></span>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"telReceiver\" id=\"tel_receiver\" value=\"";
            // line 794
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "real_receive_tel", [], "any", false, false, false, 794), "html", null, true);
            echo "\"
                        placeholder=\"เบอร์โทร\" readonly>
                </div>
            </div>
        </div>
         <div id=\"type_2\" >

            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขา :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"branchReceiver\" id=\"real_branch_receiver\" placeholder=\"ชื่อสาขา\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สถานที่อยู่ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"placeReceiver\" id=\"real_place_receiver\" placeholder=\"สถานที่อยู่\"
                            readonly>
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขาผู้รับ :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <select class=\"form-control-lg\" name=\"real_branch_send\" id=\"real_branch_send\" style=\"width:100%;\" data-error=\"#err_branch_send\">
                            <option value=\"\"> เลือกสาขาปลายทาง</option>
                            ";
            // line 832
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["branch_data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
                // line 833
                echo "                            <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 833), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "branch", [], "any", false, false, false, 833), "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 835
            echo "                        </select>
                        <span class=\"box_error\" id=\"err_branch_send\"></span>
                    </div>
\t\t\t\t\t
                    <!--<div class=\"col-1\" style=\"padding-right:0px\">
                        ชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"topicBranch\" id=\"topic_branch\" value=\"";
            // line 843
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_topic", [], "any", false, false, false, 843), "html", null, true);
            echo "\"
                            placeholder=\"ชื่อเอกสาร\">
                    </div> -->
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
\t\t\t\t<div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้นผู้รับ :
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"real_branch_floor\" style=\"width:100%;\" name=\"mr_branch_floor\">
                                    ";
            // line 858
            if ((twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 858) == 1)) {
                // line 859
                echo "                                        <option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t                           
                                    ";
            } elseif ((twig_get_attribute($this->env, $this->source,             // line 862
($context["data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 862) == 2)) {
                // line 863
                echo "                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected>ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
                                    ";
            } else {
                // line 867
                echo "                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected>ชั้น 3</option>\t
                                    ";
            }
            // line 871
            echo "\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
                    <!-- <div class=\"col-1\" style=\"padding-right:0px\">
                        หมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" name=\"remarkBranch\" id=\"remark_branch\" placeholder=\"หมายเหตุ\">";
            // line 878
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "mr_work_remark", [], "any", false, false, false, 878), "html", null, true);
            echo "</textarea> 
                    </div>-->
                    <div class=\"col-1\" style=\"padding-left:0px\">

                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                    </div>
                </div>
            </div>
        </div>
        ";
        }
        // line 890
        echo "
        <hr/>
        <div class=\"form-group\" style=\"text-align:center;\">
            <button type=\"button\" class=\"btn btn-primary\" id=\"btn_update\">บันทึกรายการ</button>
            <button type=\"button\" class=\"btn btn-primary\" id=\"btn_cancel\">ยกเลิกรายการนำส่ง</button>
            <a href=\"search_work.php\" class=\"btn btn-primary\" id=\"btn_back\">กลับ</a>
        </div>
     </form>
</div>
";
    }

    // line 902
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 903
        echo "
";
        // line 904
        if ((($context["debug"] ?? null) != "")) {
            // line 905
            echo "<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
";
        }
        // line 907
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/work_info.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1249 => 907,  1243 => 905,  1241 => 904,  1238 => 903,  1234 => 902,  1221 => 890,  1206 => 878,  1197 => 871,  1191 => 867,  1185 => 863,  1183 => 862,  1178 => 859,  1176 => 858,  1158 => 843,  1148 => 835,  1137 => 833,  1133 => 832,  1092 => 794,  1083 => 787,  1073 => 785,  1071 => 784,  1059 => 774,  1057 => 773,  1041 => 760,  1032 => 753,  1026 => 750,  1020 => 746,  1018 => 745,  1013 => 742,  1011 => 741,  993 => 726,  983 => 718,  972 => 716,  968 => 715,  920 => 670,  904 => 657,  895 => 650,  884 => 648,  880 => 647,  841 => 611,  832 => 604,  822 => 602,  820 => 601,  764 => 552,  750 => 541,  738 => 534,  723 => 521,  711 => 511,  706 => 508,  687 => 503,  682 => 501,  678 => 500,  668 => 499,  665 => 497,  648 => 496,  644 => 494,  642 => 493,  629 => 485,  623 => 482,  618 => 480,  593 => 457,  589 => 456,  496 => 366,  492 => 365,  475 => 349,  455 => 332,  276 => 156,  272 => 155,  267 => 153,  263 => 152,  257 => 150,  251 => 148,  245 => 146,  243 => 145,  239 => 144,  235 => 143,  230 => 141,  226 => 140,  223 => 139,  219 => 137,  215 => 135,  213 => 134,  207 => 133,  201 => 132,  197 => 130,  193 => 129,  70 => 8,  66 => 7,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_2 %} active {% endblock %}

{% block styleReady %}
#btn_update:hover,
#btn_cancel:hover,
#btn_back:hover {
    color: #FFFFFF;
    background-color: #055d97;
}


#btn_update,
#btn_cancel,
#btn_back {
    border-color: #0074c0;
    color: #FFFFFF;
    background-color: #0074c0;
}

#detail_sender_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_sender_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 20px;
}

#workNumber {
    color: #006cb7;
}

#detail_receiver_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_receiver_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 40px;
}
.divTimes{
\tposition: absolute;
    top: -30px;
\twidth: 100%;
    //right: -20px;
\t//transform: translateX(-5px) rotateZ(-45deg);
}

.coloe_compress:before{
\tcolor:#fff;
\tbackground-color:#2196F3 !important ;
\t
}
.multi-steps > li.is-active:before, .multi-steps > li.is-active ~ li:before {
  content: counter(stepNum);
  font-family: inherit;
  font-weight: 700;
}
.multi-steps > li.is-active:after, .multi-steps > li.is-active ~ li:after {
  background-color: #ededed;
}

.multi-steps {
  display: table;
  table-layout: fixed;
  width: 100%;
}
.multi-steps > li {
  counter-increment: stepNum;
  text-align: center;
  display: table-cell;
  position: relative;
  color: #2196F3;
}
.multi-steps > li:before {
  content: counter(stepNum);
  display: block;
  margin: 0 auto 4px;
  background-color: #fff;
  width: 36px;
  height: 36px;
  line-height: 32px;
  text-align: center;
  font-weight: bold;
  border-width: 2px;
  border-style: solid;
  border-color: #2196F3;
  border-radius: 50%;
}
.multi-steps > li:after {
  content: '';
  height: 2px;
  width: 100%;
  background-color: #2196F3 ;
  position: absolute;
  top: 16px;
  left: 50%;
  z-index: -1;
}
.multi-steps > li:last-child:after {
  display: none;
}
.multi-steps > li.is-active:before {
  background-color: #fff;
  border-color: #2196F3 ;
}
.multi-steps > li.is-active ~ li {
  color: #808080;
}
.multi-steps > li.is-active ~ li:before {
  background-color: #ededed;
  border-color: #ededed;
}

{% endblock %}

{% block domReady %}
moment.locale('th');

var init = { id: '{{ data.receive_id }}', text: '{{ data.receive_name }}' };
var init_replace = { id: '{{ data.real_receive_id }}', text: '{{ data.real_receive_name }}' };
{% if data.mr_type_work_id == 2 %}
var type_work = '2';
{% else %}
var type_work = '1';
{% endif %}

var mr_branch_floor = '{{ data.mr_branch_floor }}';
var real_branch_floor = '{{ data.mr_branch_floor }}';

var branch_id = '{{ data.receive_branch_id }}';
var real_branch_id = '{{ data.receive_branch_id3 }}';
{% if data.mr_floor_id=='' %}
\tvar floor_id  = '{{ data.receive_floor_id }}';
{% else %}
\tvar floor_id  = '{{ data.mr_floor_id }}';
{% endif %}
var receiver_id = '{{ data.receive_id }}';

var real_receive_id = '{{ data.real_receive_id }}'
var real_receiver_name = '{{ data.real_receive_name }}';

var receiver_name = '{{ data.receive_name }}';
var status = '{{ data.mr_status_id }}';

// console.log(d.days()+\" วัน \"+ d.hours()+\" ชม. \"+d.minutes()+\" นาที \"+ d.seconds() + \" วินาที\");

// initial Page
initialPage(status);
setForm(init);
setFormReplace(init_replace);
check_type_send(parseInt(type_work));


//console.log(hoursArr);
//calculateDuration(timeArr,hoursArr);


if (!!branch_id) {
    \$('#branch_send').val(parseInt(branch_id)).trigger('change');
}

if (!!real_branch_id) {
    \$('#real_branch_send').val(parseInt(real_branch_id)).trigger('change');
}

if (!!floor_id) {
    \$('#floor_send').val(parseInt(branch_id)).trigger('change');
}
if (!!mr_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}

if (!!real_branch_floor) {
    \$('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}


\$(\".alert\").alert();

\$('#name_receiver_select').select2({
    placeholder: \"ค้นหาผู้รับ\",
   
    ajax: {
        url: \"ajax/ajax_getdataemployee_select.php\",
        dataType: \"json\",
        delay: 250,
        processResults: function (data) {
            return {
                results : data
            };
        },
        cache: true
    },
    
}).on('select2:select', function(e) {
    //console.log(e.params.data)
    setForm(e.params.data);
});

var frmValid = \$('#frmBranch').validate({
    onsubmit: false,
    onkeyup: false,
    errorClass: \"is-invalid\",
    highlight: function (element) {
        if (element.type == \"radio\" || element.type == \"checkbox\") {
            \$(element).removeClass('is-invalid')
        } else {
            \$(element).addClass('is-invalid')
        }
    },
    rules: {
        telSender: {
            required: false
        },
        type_send: {
            required: true
        },
        nameReceiver: {
            required: true
        },
        telReceiver: {
            required: false
        },
        floorRealReceiver: {
            required: {
                depends: function (elm) {
                    var f = \$('#floor_name').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if( (f == \" - \" || f == \"\") && type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        branchRealReceiver: {
            required: {
                depends: function (elm) {
                    var b = \$('#branch_receiver').val();
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if((b == \" - \" || b == \"\") && type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topic: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topicBranch: {
            required: {
                depends: function (elm) {
                    var type = \$('input[name=\"type_send\"]:checked').val();
                    if(type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
    },
    messages: {
        telSender: {
            required: 'ระบุเบอร์ติดต่อ'
        },
        type_send: {
            required: 'ระบุประเภทผู้รับ'
        },
        nameReceiver: {
            required: 'กรุณาเลือกผู้รับ'
        },
        telReceiver: {
            required: 'ระบุเบอร์ติดต่อผู้รับ'
        },
        branchRealReceiver: {
            required: 'ระบุสาขาผู้รับ'
        },
        topic: {
            required: 'ระบุชื่อเอกสาร'
        },
        topicBranch: {
            required: 'ระบุชื่อเอกสาร'
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        var placement = \$(element).data('error');
        if (placement) {
            \$(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});

\$('#btn_update').click(function() {
     var _frm = \$('#frmBranch');
    if(\$('#frmBranch').valid()) {
       
        \$.ajax({
            url: _frm.attr('action'),
            type: 'POST',
            data: {
                form: _frm.serializeArray(),
                actions: 'update',
                wid: '{{ wid }}'
            },
            dataType: 'json',
            success: function(res) {
                if(res.status == \"success\") {
                    alertify.alert(res['messeges'], function() {
                        location.reload();
                    });
                }
            }
        })
    }
});


\$(\"#btn_cancel\").click(function(){
    alertify.confirm('ยืนยันจะยกเลิกรายการนำส่งนี้', function() {
        \$.post('./ajax/ajax_updateWorkInfo.php', { actions: 'cancel', wid: '{{ wid }}'}, function(res){
            if(res.status == \"success\") {
                        alertify.alert(res['messeges'], function() {
                            location.reload();
                        });
                    }
        }, 'json');
    })
   
});


{% endblock %}



{% block javaScript %}


// \$('#type_send_1').prop('checked',true);

function check_type_send( type_send ){
    if( type_send == 1 ){
        \$(\"#type_1\").show();
        \$(\"#type_2\").hide();

        \$('#type_send_1').prop('checked', true);
        \$('#type_send_2').prop('checked', false);
    }else{
        \$(\"#type_1\").hide();
        \$(\"#type_2\").show();
        
        \$('#type_send_2').prop('checked', true);
        \$('#type_send_1').prop('checked', false);
    }
}

function check_name_re() {
    var pass_emp = \$(\"#pass_emp\").val();
    if(pass_emp == \"\" || pass_emp == null){
        \$(\"#div_re_text\").hide();
        \$(\"#div_re_select\").show();
    }else{
        \$(\"#div_re_text\").show();
        \$(\"#div_re_select\").hide();
    }
}

function setForm(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            \$(\"#depart_receiver\").val(res['department']);
            \$(\"#emp_id\").val(res['mr_emp_id']);
            \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#place_receiver\").val(res['mr_workarea']);
            \$(\"#floor_name\").val(res['mr_department_floor']);
            \$(\"#branch_receiver\").val(res['branch']);
        }
    })
}

function setFormReplace(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    \$.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            console.log(res);
            // \$(\"#depart_receiver\").val(res['department']);
            // \$(\"#emp_id\").val(res['mr_emp_id']);
            // \$(\"#tel_receiver\").val(res['mr_emp_tel']);
            \$(\"#real_place_receiver\").val(res['mr_workarea']);
            \$(\"#real_floor_name\").val(res['mr_department_floor']);
            \$(\"#real_branch_receiver\").val(res['branch']);
        }
    })
}

var saveBranch = function (obj) {
    return \$.post('ajax/ajax_save_work_branch.php', obj);
}

function initialPage(status) {
    //console.log(status)
    if(parseInt(status) != 7) {
        \$('input').prop('disabled', true);
        \$('select').prop('disabled', true);
        \$('textarea').prop('disabled', true);
        \$('#btn_update').prop('disabled', true);
        \$('#btn_cancel').prop('disabled', true);
    }
}

{% endblock %}
{% block Content %}

<div class=\"container\">



<br>
<br>
<br>

  
  
  
  





    <div class=\"\" style=\"text-align: center;color:#0074c0;margin-top:20px;\">
        <label>
            <h3><b>รายละเอียดรายการนำส่ง</b></h3>
        </label>
    </div>
    <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
    <input type=\"hidden\" id=\"emp_id\" value=\"\">
    <input type=\"hidden\" id=\"barcode\" name=\"barcode\" value=\"{{ barcode }}\">
    
    <div>
        <p class=\"font-weight-bold\"><span class=\"text-muted\">เลขที่เอกสาร: </span> <span id=\"workNumber\">{{ data.mr_work_barcode }}</span> <span class=\"text-muted\">( {{ data.mr_status_name }} )</span></p>
    </div>
    

    <div class=\"form-group\"  id=\"detail_sender_head\">
        <h4>สถานะเอกสาร </h4>
    </div>

    {% if data.mr_status_id != 6 %}
    <div class=\"demo\"><br><br><br><br>
        <ul class=\"list-unstyled multi-steps\">
\t\t\t {% for st in status %}
\t\t\t\t\t\t\t
\t\t\t\t {# {% if st.active != \"\" %} #}
\t\t\t\t\t<li class=\"{% if end_step == loop.index %} is-active {% endif %}{% if st.active != \"\" %} coloe_compress {% endif %}\">
\t\t\t\t\t\t<strong>{{ st.emp_code }}</strong><br />
\t\t\t\t\t\t<small>{{ st.name }}</small>
\t\t\t\t\t\t<div class=\"divTimes\">
\t\t\t\t\t\t\t\t<small>{{st.date}}</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t{% endfor %}
\t\t\t </ul>
    </div>
    {% else %}
    <div style=\"text-align:center;\">
        <p class=\"text-muted\">
            <i class=\"material-icons\">block</i>
            <br/>
            <strong>ยกเลิกรายการนำส่ง</strong>
        </p>
    </div>
\t<br>
\t<br>
    {% endif %}
        
     <!-- <div class='clearfix'></div> -->
    
     <form action=\"./ajax/ajax_updateWorkInfo.php\" method=\"POST\" id=\"frmBranch\">
        <div class=\"form-group\" style=\"\" id=\"detail_sender_head\">
            <h4>รายละเอียดผู้ส่ง </h4>
        </div>
        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    สาขาผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"branchSender\" id=\"brach_sender\" placeholder=\"ชื่อสาขา\" value=\"{{ user_data_show.mr_branch_code }} - {{ user_data_show.mr_branch_name }}\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"telSender\" id=\"name_sender_tel\" placeholder=\"เบอร์โทร\" value=\"{{  user_data_show.mr_emp_tel  }}\" readonly>
                </div>
            </div>
        </div>

        <div class=\"form-group\" id=\"div_re_select\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ชื่อผู้ส่ง :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"nameSender\" id=\"name_sender\" value=\"{{  user_data_show.mr_emp_code  }} - {{  user_data_show.mr_emp_name  }} {{  user_data_show.mr_emp_lastname  }}\"
                        readonly>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">

                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">

                </div>
            </div>
        </div>

        <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
            <h4>รายละเอียดผู้รับ</h4>
        </div>
        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ค้นหาผู้รับ :
                </div>

                <div class=\"col-2\" style=\"padding-left:0px\">
                    <div class=\"input-group\">
                        <label class=\"btn btn-primary\">
                            <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_1\"
                                value=\"1\" onclick=\"check_type_send(this.value);\"> &nbsp;
                            ส่งสำนักงานใหญ่
                        </label>
                    </div>
                </div>
                <div class=\"col-2\" style=\"padding-left:0px\">
                    <div class=\"input-group\">
                        <label class=\"btn btn-primary\">
                            <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"type_send\" id=\"type_send_2\"
                                value=\"2\" onclick=\"check_type_send(this.value);\"> &nbsp;
                            ส่งสาขา
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ค้นหาผู้รับ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <select class=\"form-control-lg\" name=\"nameReceiver\" id=\"name_receiver_select\" style=\"width:100%;\" data-error=\"#err_receiver_select\">
                        {% if data.receive_id != \"\" %}
                        <option value=\"{{ data.receive_id }}\">{{ data.receive_code }} - {{ data.receive_name }}</option>
                        {% endif %}
                    </select>
                    <span class=\"box_error\" id=\"err_receiver_select\"></span>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"telReceiver\" id=\"tel_receiver\" value=\"{{ data.receive_tel }}\"
                        placeholder=\"เบอร์โทร\" readonly>
                </div>
            </div>
        </div>

        <div id=\"type_1\" style=\"display:;\">
            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        แผนก :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"departReceiver\" id=\"depart_receiver\" placeholder=\"ชื่อแผนก\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชั้น :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"floorReceiverName\" id=\"floor_name\" placeholder=\"ชั้น\" readonly>
                    </div>
                </div>

            </div>

            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชั้นผู้รับ :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <select class=\"form-control-lg\" name=\"floorRealReceiver\" id=\"floor_send\" style=\"width:100%;\" data-error=\"#err_floor_send\">
                            <option value=\"\"> เลือกชั้นปลายทาง</option>
                            {% for s in floor_data %}
                            <option value=\"{{ s.mr_floor_id }}\"> {{ s.name }}</option>
                            {% endfor %}
                        </select>
                        <span class=\"box_error\" id=\"err_floor_send\"></span>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"topic\" id=\"topic_head\" placeholder=\"ชื่อเอกสาร\" value=\"{{ data.mr_topic }}\">
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        หมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" name=\"remark\" id=\"remark_head\" placeholder=\"หมายเหตุ\">{{ data.mr_work_remark }}</textarea>
                    </div>
                    <div class=\"col-1\" style=\"padding-left:0px\">

                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                    </div>
                </div>
            </div>
        </div> <!-- end type 1-->

        <div id=\"type_2\" style=\"display:none;\">

            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขา :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"branchReceiver\" id=\"branch_receiver\" placeholder=\"ชื่อสาขา\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สถานที่อยู่ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"placeReceiver\" id=\"place_receiver\" placeholder=\"สถานที่อยู่\"
                            readonly>
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขาผู้รับ :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <select class=\"form-control-lg\" name=\"branchRealReceiver\" id=\"branch_send\" style=\"width:100%;\" data-error=\"#err_branch_send\">
                            <option value=\"\"> เลือกสาขาปลายทาง</option>
                            {% for b in branch_data %}
                            <option value=\"{{ b.mr_branch_id }}\"> {{ b.branch }}</option>
                            {% endfor %}
                        </select>
                        <span class=\"box_error\" id=\"err_branch_send\"></span>
                    </div>
\t\t\t\t\t
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        ชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"topicBranch\" id=\"topic_branch\" value=\"{{ data.mr_topic }}\"
                            placeholder=\"ชื่อเอกสาร\">
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
\t\t\t\t<div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้นผู้รับ :
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_floor\" style=\"width:100%;\" name=\"mr_branch_floor\">
\t\t\t\t\t\t\t\t\t{% if data.mr_branch_floor == 1 %}
                                        <option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t                           
                                    {% elseif data.mr_branch_floor == 2 %}
                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected>ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
                                    {% else %}
                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected>ชั้น 3</option>\t
                                    {% endif %}\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        หมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" name=\"remarkBranch\" id=\"remark_branch\" placeholder=\"หมายเหตุ\">{{ data.mr_work_remark }}</textarea>
                    </div>
                    <div class=\"col-1\" style=\"padding-left:0px\">

                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                    </div>
                </div>
            </div>
        </div> 
\t\t<!-- end type 2-->

        {% if received == \"replace\" %}
        <div class=\"form-group\" style=\"\" id=\"detail_receiver_head\">
            <h4>รายละเอียดผู้รับ (แทน)</h4>
        </div>
         <div class=\"form-group\" id=\"div_re_text\">
            <div class=\"row\">
                <div class=\"col-1\" style=\"padding-right:0px\">
                    ค้นหาผู้รับ :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <select class=\"form-control-lg\" name=\"nameReceiver\" id=\"name_receiver_select\" style=\"width:100%;\" data-error=\"#err_receiver_select\">
                        {% if data.receive_id != \"\" %}
                        <option value=\"{{ data.real_receive_id }}\">{{ data.real_receive_code }} - {{ data.real_receive_name }}</option>
                        {% endif %}
                    </select>
                    <span class=\"box_error\" id=\"err_receiver_select\"></span>
                </div>
                <div class=\"col-1\" style=\"padding-right:0px\">
                    เบอร์โทร :
                </div>
                <div class=\"col-5\" style=\"padding-left:0px\">
                    <input type=\"text\" class=\"form-control form-control-sm\" name=\"telReceiver\" id=\"tel_receiver\" value=\"{{ data.real_receive_tel }}\"
                        placeholder=\"เบอร์โทร\" readonly>
                </div>
            </div>
        </div>
         <div id=\"type_2\" >

            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขา :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"branchReceiver\" id=\"real_branch_receiver\" placeholder=\"ชื่อสาขา\"
                            readonly>
                    </div>
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สถานที่อยู่ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"placeReceiver\" id=\"real_place_receiver\" placeholder=\"สถานที่อยู่\"
                            readonly>
                    </div>
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
                    <div class=\"col-1\" style=\"padding-right:0px\">
                        สาขาผู้รับ :
                    </div>

                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <select class=\"form-control-lg\" name=\"real_branch_send\" id=\"real_branch_send\" style=\"width:100%;\" data-error=\"#err_branch_send\">
                            <option value=\"\"> เลือกสาขาปลายทาง</option>
                            {% for b in branch_data %}
                            <option value=\"{{ b.mr_branch_id }}\"> {{ b.branch }}</option>
                            {% endfor %}
                        </select>
                        <span class=\"box_error\" id=\"err_branch_send\"></span>
                    </div>
\t\t\t\t\t
                    <!--<div class=\"col-1\" style=\"padding-right:0px\">
                        ชื่อเอกสาร :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <input type=\"text\" class=\"form-control form-control-sm\" name=\"topicBranch\" id=\"topic_branch\" value=\"{{ data.mr_topic }}\"
                            placeholder=\"ชื่อเอกสาร\">
                    </div> -->
                </div>

            </div>


            <div class=\"form-group\" id=\"div_re_select\">
                <div class=\"row\">
\t\t\t\t<div class=\"col-1\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tชั้นผู้รับ :
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t<div class=\"col-5\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"real_branch_floor\" style=\"width:100%;\" name=\"mr_branch_floor\">
                                    {% if data.mr_branch_floor == 1 %}
                                        <option value=\"1\" selected>ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t                           
                                    {% elseif data.mr_branch_floor == 2 %}
                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" selected>ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" >ชั้น 3</option>\t\t\t
                                    {% else %}
                                        <option value=\"1\" >ชั้น 1</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"2\" >ชั้น 2</option>\t\t\t
\t\t\t\t\t\t\t\t\t\t<option value=\"3\" selected>ชั้น 3</option>\t
                                    {% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
                    <!-- <div class=\"col-1\" style=\"padding-right:0px\">
                        หมายเหตุ :
                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">
                        <textarea class=\"form-control form-control-sm\" name=\"remarkBranch\" id=\"remark_branch\" placeholder=\"หมายเหตุ\">{{ data.mr_work_remark }}</textarea> 
                    </div>-->
                    <div class=\"col-1\" style=\"padding-left:0px\">

                    </div>
                    <div class=\"col-5\" style=\"padding-left:0px\">

                    </div>
                </div>
            </div>
        </div>
        {% endif %}

        <hr/>
        <div class=\"form-group\" style=\"text-align:center;\">
            <button type=\"button\" class=\"btn btn-primary\" id=\"btn_update\">บันทึกรายการ</button>
            <button type=\"button\" class=\"btn btn-primary\" id=\"btn_cancel\">ยกเลิกรายการนำส่ง</button>
            <a href=\"search_work.php\" class=\"btn btn-primary\" id=\"btn_back\">กลับ</a>
        </div>
     </form>
</div>
{% endblock %}


{% block debug %}

{% if debug != '' %}
<pre>{{ debug }}</pre>
{% endif %}

{% endblock %}
", "branch/work_info.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\work_info.tpl");
    }
}

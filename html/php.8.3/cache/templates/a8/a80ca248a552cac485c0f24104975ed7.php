<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/resive_work_post_in.tpl */
class __TwigTemplate_7f41a1fefdd1800f68bd69a64c5bc531 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/resive_work_post_in.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 29
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo ".alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

";
    }

    // line 59
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t

//console.log('55555');

\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_import').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t

  \$('#myform_data_thaipost').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'work_barcode': {
\t\t\trequired: true
\t\t},
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t}\t  
\t},
\tmessages: {
\t\t'work_barcode': {
\t\t  required: 'กรุณาระบุ barcode.'
\t\t},
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });


\$('#btn-show-form-edit').click(function() {
\t\$('#div_detail_receiver_sender').hide();
\t\$('#acction_update').hide();
\t\$('#acction_edit').show();
\t\$('#div_sender').show();
\t\$('#div_receiver').show();
\t\$(\"#page\").val('edit');
});
\$('#btn_cancle_edit').click(function() {
\t\$('#div_detail_receiver_sender').show();
\t\$('#acction_update').show();
\t\$('#acction_edit').hide();
\t\$('#div_sender').hide();
\t\$('#div_receiver').hide();
\t\$(\"#page\").val('update_status');
});

\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t//load_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_price_form\").prop(\"checked\") == false){
\t\t\t\t\treset_price_form();
\t\t\t\t}
\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\$(\"#work_barcode\").val('');
\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});
\$('#btn_edit').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\talertify.alert('สำเร็จ',\"  แก้ไขข้อมูลเรียบร้อยแล้ว\"
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});

\$('#dep_id_send').select2();
\$('#cost_id').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'barcode'},
        {'data': 'date_send'},
        {'data': 'mr_round_name'},
    ]
});
\$('#select-all').on('click', function(){
\t// Check/uncheck all checkboxes in the table
\tvar rows = tbl_data.rows({ 'search': 'applied' }).nodes();
\t\$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.name); // display the selected text
                \$('#lname_re').val(ui.item.lname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t\$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
\t\t\t\t\$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
\t\t\t\t\$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
\t\t\t\t\$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
\t\t\t\t\$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
\t\t\t\t\$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
\t\t\t\t\$('#post_code_re').val(ui.item.zipcode); // save selected id to input
\t\t\t\t\$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
\t\t
\t\t\$('#work_barcode').keypress(function(event){
\t\t\tvar keycode = (event.keyCode ? event.keyCode : event.which);
\t\t\tif(keycode == '13'){
\t\t\t\tif(\$('#myform_data_thaipost').valid()) {
\t\t\t\t\tvar barcode = \$(this).val();
\t\t\t\t\t
\t\t\t\t\talertify.confirm('ยืนยันการบันทึก', 'กด \"OK\" ',
\t\t\t\t\tfunction(){ 
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl: './ajax/ajax_save_bacose_thaipost_in.php',
\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t\t\tbarcode\t: barcode,
\t\t\t\t\t\t\t\t\tpage\t: 'saveBarcode',
\t\t\t\t\t\t\t\t\tcsrf_token\t: \$('#csrf_token').val(),
\t\t\t\t\t\t\t\t\tdate_send\t: \$('#date_send').val(),
\t\t\t\t\t\t\t\t\tround\t: \$('#round').val()
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);

\t\t\t\t\t\t\t\t\t//console.log(\"++++++++++++++\");
\t\t\t\t\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\t\t\t\tif(res['count']==0){
\t\t\t\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('');
\t\t\t\t\t\t\t\t\t\t\t\$('#work_barcode').val('');
\t\t\t\t\t\t\t\t\t\t\t\$('#work_barcode').removeClass('is-invalid');
\t\t\t\t\t\t\t\t\t\t\tload_data_bydate();
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t\$('#work_barcode').addClass('is-invalid');
\t\t\t\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('Barcode ซ้ำ')
\t\t\t\t\t\t\t\t\t\t\t\$('#btn-show-form-edit').attr('disabled','disabled');
\t\t\t\t\t\t\t\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}); 
\t\t\t\t\t\t}, function(){ 
\t\t\t\t\t\t\talertify.error('Cancel')
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t});
\t\t\t\t\t
\t\t\t\t}
\t\t\t}
\t\t  });\t\t


\t\t  \$(\"#work_barcode\").keypress(function(event){
\t\t\tvar ew = event.which;
\t\t\tif(ew == 32)
\t\t\t\treturn true;
\t\t\tif(48 <= ew && ew <= 57)
\t\t\t\treturn true;
\t\t\tif(65 <= ew && ew <= 90)
\t\t\t\treturn true;
\t\t\tif(97 <= ew && ew <= 122)
\t\t\t\treturn true;
\t\t\treturn false;
\t\t});

\t\t
";
    }

    // line 472
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 473
        echo "
function load_data_bydate() {
\t\$('#tb_keyin').DataTable().clear().draw();
\tvar form = \$('#form_print');
\tvar serializeData = form.serialize();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:serializeData,
\t\turl: \"ajax/ajax_save_bacose_thaipost_in.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token'])
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}


function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" ',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle',
\t\t\tcsrf_token\t: \$('#csrf_token').val(),
\t\t},
\t\turl: \"ajax/ajax_save_bacose_thaipost_in.php\",
\t\tbeforeSend: function() {
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  \talert('error; ' + eval(error));
\t\t  \t\$(\"#bg_loader\").hide();
\t\t \tlocation.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token'])
\t\t\$(\"#bg_loader\").hide();
\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}




function cancelwork() {
\tvar dataall = [];
\tvar tbl_data = \$('#tb_keyin').DataTable();
\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\tdataall.push(this.value);
\t});

\tif(dataall.length < 1){
\t\talertify.alert(\"เกิดข้อผิดพลาด\",\"ท่านยังไม่เลือกรายการ\"); 
\t\treturn;
\t}
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" ',
\tfunction(){ 
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: newdataall,
\t\t\tpage\t:'cancle_multiple',
\t\t\tcsrf_token\t: \$('#csrf_token').val(),
\t\t},
\t\turl: \"ajax/ajax_save_bacose_thaipost_in.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token'])
\t\t\$(\"#bg_loader\").hide();
\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
\t

}


function import_excel() {
\t\$('#div_error').hide();\t
\tvar formData = new FormData();
\tformData.append('file', \$('#file')[0].files[0]);
\tif(\$('#file').val() == ''){
\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\$('#div_error').show();
\t\treturn;
\t}else{
\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\$('#div_error').show();
\t\treturn;
\t }
\t}
\t\$.ajax({
\t\t   url : 'ajax/ajax_readFile_barcode_Thai_post.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : formData,
\t\t   processData: false,  // tell jQuery not to process the data
\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t   success : function(data) {

\t\t\tif(data.status == 200){
\t\t\t\tif(data.data.count_error > 0){
\t\t\t\t\t\$('#div_error').html(data.data.txt_error)
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t}

\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#file').val('');
\t\t\t\tload_data();
\t\t\t}

\t\t\t//    if(data['count_error'] > 0){
\t\t\t// \t\$('#div_error').html(data['txt_error'])
\t\t\t// \t\$('#div_error').show();
\t\t\t//    }else{

\t\t\t//    }
\t\t\t//    \$('#bg_loader').hide();
\t\t\t//    \$('#file').val('');
\t\t\t//    load_data();
\t\t\t   
\t\t}, beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t});

}


";
    }

    // line 643
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 644
        echo "
<div  class=\"container-fluid\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ยิงรับเอกสารจาก ไปรษณีย์ไทย</b></h3></label><br>
\t\t\t\t
\t\t   </div>\t
\t\t</div>
\t</div>
\t  
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t<div class=\"tab-pane fade show active\" id=\"key\" role=\"tabpanel\" aria-labelledby=\"key-tab\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form id=\"myform_data_thaipost\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"date_send\"><span class=\"text-muted font_mini\" >วันที่นำส่ง:<span class=\"box_error\" id=\"err_date_send\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 672
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-3\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"round\"><span class=\"text-muted font_mini\" >รอบการนำส่ง:</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 679
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 680
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 680), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 680), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 682
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\"  data-backdrop=\"static\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\tUpload Excel ป.ณ.
\t\t\t\t\t\t\t\t\t\t\t\t\t  </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"work_barcode\"><span class=\"text-muted font_mini\" >Barcode: <span class=\"box_error\" id=\"err_work_barcode\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_work_barcode\" name=\"work_barcode\" id=\"work_barcode\" class=\"form-control form-control-sm\" type=\"text\" value=\"\" placeholder=\"Enter Barcode\">
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
\t</div>
\t

<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 711
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\"></input>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t";
        // line 718
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 719
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 719), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 719), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 721
        echo "\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 726
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<!-- <button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-sm btn-outline-info\" id=\"\">พิมพ์ใบงาน</button> -->
\t\t\t\t<!-- \t\t\t\t
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t-->
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\"></span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t<button onclick=\"cancelwork();\" type=\"button\" class=\"btn btn-danger btn-sm\">
\t\t\t\t\t\tลบรายการ
\t\t\t\t\t</button>
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่จะส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>





<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




  
  <!-- Modal -->
  <div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog\" role=\"document\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t  </button>
\t\t</div>
\t\t<div class=\"modal-body\">
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"form-group col-md-6\">
\t\t\t\t\t<label for=\"date_send\"><span class=\"text-muted font_mini\" >วันที่นำส่ง:<span class=\"box_error\" id=\"err_date_send\"></span></span></label>
\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send2\" id=\"date_send2\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 804
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t  </div>
\t\t\t\t <div class=\"form-group col-md-6\">
\t\t\t\t\t<label for=\"round\"><span class=\"text-muted font_mini\" >รอบการนำส่ง:</span></label>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round2\"></span>
\t\t\t\t\t<select data-error=\"#err_round2\" class=\"form-control form-control-sm\" id=\"round2\" name=\"round2\">
\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t";
        // line 811
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 812
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 812), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 812), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 814
        echo "\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"card w-100\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<div class=\"col-md-12 col-lg-12\">\t  
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"></a>
\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<a  href=\"template_tnt.xlsx\" type=\"button\" class=\"btn btn-dark\" download> \t
\t\t\t\t\t\t\t\t<i class=\"material-icons\">vertical_align_bottom</i>template
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"modal-footer\">
\t\t  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
\t\t  <button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-success\"> \t
\t\t\t<i class=\"material-icons\">vertical_align_top</i> upload
\t\t</button>
\t\t</div>
\t  </div>
\t</div>
  </div>
";
    }

    // line 846
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 848
        if ((($context["debug"] ?? null) != "")) {
            // line 849
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 851
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/resive_work_post_in.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1008 => 851,  1002 => 849,  1000 => 848,  993 => 846,  960 => 814,  949 => 812,  945 => 811,  933 => 804,  850 => 726,  843 => 721,  832 => 719,  828 => 718,  818 => 711,  787 => 682,  776 => 680,  772 => 679,  760 => 672,  730 => 644,  726 => 643,  553 => 473,  549 => 472,  130 => 59,  99 => 30,  95 => 29,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

{% endblock %}

{% block domReady %}\t

//console.log('55555');

\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_import').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t

  \$('#myform_data_thaipost').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'work_barcode': {
\t\t\trequired: true
\t\t},
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t}\t  
\t},
\tmessages: {
\t\t'work_barcode': {
\t\t  required: 'กรุณาระบุ barcode.'
\t\t},
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });


\$('#btn-show-form-edit').click(function() {
\t\$('#div_detail_receiver_sender').hide();
\t\$('#acction_update').hide();
\t\$('#acction_edit').show();
\t\$('#div_sender').show();
\t\$('#div_receiver').show();
\t\$(\"#page\").val('edit');
});
\$('#btn_cancle_edit').click(function() {
\t\$('#div_detail_receiver_sender').show();
\t\$('#acction_update').show();
\t\$('#acction_edit').hide();
\t\$('#div_sender').hide();
\t\$('#div_receiver').hide();
\t\$(\"#page\").val('update_status');
});

\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\t//load_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_price_form\").prop(\"checked\") == false){
\t\t\t\t\treset_price_form();
\t\t\t\t}
\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\$(\"#work_barcode\").val('');
\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});
\$('#btn_edit').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_post_out.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 401){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\talertify.alert('สำเร็จ',\"  แก้ไขข้อมูลเรียบร้อยแล้ว\"
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});

\$('#dep_id_send').select2();
\$('#cost_id').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'barcode'},
        {'data': 'date_send'},
        {'data': 'mr_round_name'},
    ]
});
\$('#select-all').on('click', function(){
\t// Check/uncheck all checkboxes in the table
\tvar rows = tbl_data.rows({ 'search': 'applied' }).nodes();
\t\$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_WorkPost.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.name); // display the selected text
                \$('#lname_re').val(ui.item.lname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t\$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
\t\t\t\t\$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
\t\t\t\t\$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
\t\t\t\t\$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
\t\t\t\t\$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
\t\t\t\t\$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
\t\t\t\t\$('#post_code_re').val(ui.item.zipcode); // save selected id to input
\t\t\t\t\$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
\t\t
\t\t\$('#work_barcode').keypress(function(event){
\t\t\tvar keycode = (event.keyCode ? event.keyCode : event.which);
\t\t\tif(keycode == '13'){
\t\t\t\tif(\$('#myform_data_thaipost').valid()) {
\t\t\t\t\tvar barcode = \$(this).val();
\t\t\t\t\t
\t\t\t\t\talertify.confirm('ยืนยันการบันทึก', 'กด \"OK\" ',
\t\t\t\t\tfunction(){ 
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl: './ajax/ajax_save_bacose_thaipost_in.php',
\t\t\t\t\t\t\t\ttype: 'POST',
\t\t\t\t\t\t\t\tdata: {
\t\t\t\t\t\t\t\t\tbarcode\t: barcode,
\t\t\t\t\t\t\t\t\tpage\t: 'saveBarcode',
\t\t\t\t\t\t\t\t\tcsrf_token\t: \$('#csrf_token').val(),
\t\t\t\t\t\t\t\t\tdate_send\t: \$('#date_send').val(),
\t\t\t\t\t\t\t\t\tround\t: \$('#round').val()
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\t\t\$('#csrf_token').val(res['token']);

\t\t\t\t\t\t\t\t\t//console.log(\"++++++++++++++\");
\t\t\t\t\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\t\t\t\tif(res['count']==0){
\t\t\t\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('');
\t\t\t\t\t\t\t\t\t\t\t\$('#work_barcode').val('');
\t\t\t\t\t\t\t\t\t\t\t\$('#work_barcode').removeClass('is-invalid');
\t\t\t\t\t\t\t\t\t\t\tload_data_bydate();
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t\$('#work_barcode').addClass('is-invalid');
\t\t\t\t\t\t\t\t\t\t\t\$('#err_work_barcode').html('Barcode ซ้ำ')
\t\t\t\t\t\t\t\t\t\t\t\$('#btn-show-form-edit').attr('disabled','disabled');
\t\t\t\t\t\t\t\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}); 
\t\t\t\t\t\t}, function(){ 
\t\t\t\t\t\t\talertify.error('Cancel')
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t});
\t\t\t\t\t
\t\t\t\t}
\t\t\t}
\t\t  });\t\t


\t\t  \$(\"#work_barcode\").keypress(function(event){
\t\t\tvar ew = event.which;
\t\t\tif(ew == 32)
\t\t\t\treturn true;
\t\t\tif(48 <= ew && ew <= 57)
\t\t\t\treturn true;
\t\t\tif(65 <= ew && ew <= 90)
\t\t\t\treturn true;
\t\t\tif(97 <= ew && ew <= 122)
\t\t\t\treturn true;
\t\t\treturn false;
\t\t});

\t\t
{% endblock %}
{% block javaScript %}

function load_data_bydate() {
\t\$('#tb_keyin').DataTable().clear().draw();
\tvar form = \$('#form_print');
\tvar serializeData = form.serialize();
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:serializeData,
\t\turl: \"ajax/ajax_save_bacose_thaipost_in.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token'])
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}


function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" ',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle',
\t\t\tcsrf_token\t: \$('#csrf_token').val(),
\t\t},
\t\turl: \"ajax/ajax_save_bacose_thaipost_in.php\",
\t\tbeforeSend: function() {
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  \talert('error; ' + eval(error));
\t\t  \t\$(\"#bg_loader\").hide();
\t\t \tlocation.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token'])
\t\t\$(\"#bg_loader\").hide();
\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}




function cancelwork() {
\tvar dataall = [];
\tvar tbl_data = \$('#tb_keyin').DataTable();
\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t //console.log(this.value);
\t\tdataall.push(this.value);
\t});

\tif(dataall.length < 1){
\t\talertify.alert(\"เกิดข้อผิดพลาด\",\"ท่านยังไม่เลือกรายการ\"); 
\t\treturn;
\t}
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" ',
\tfunction(){ 
\t\tvar newdataall = dataall.join(\",\");
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: newdataall,
\t\t\tpage\t:'cancle_multiple',
\t\t\tcsrf_token\t: \$('#csrf_token').val(),
\t\t},
\t\turl: \"ajax/ajax_save_bacose_thaipost_in.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$('#csrf_token').val(res['token'])
\t\t\$(\"#bg_loader\").hide();
\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
\t

}


function import_excel() {
\t\$('#div_error').hide();\t
\tvar formData = new FormData();
\tformData.append('file', \$('#file')[0].files[0]);
\tif(\$('#file').val() == ''){
\t\t\$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
\t\t\$('#div_error').show();
\t\treturn;
\t}else{
\t var extension = \$('#file').val().replace(/^.*\\./, '');
\t if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
\t\t \$('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
\t\t\$('#div_error').show();
\t\treturn;
\t }
\t}
\t\$.ajax({
\t\t   url : 'ajax/ajax_readFile_barcode_Thai_post.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : formData,
\t\t   processData: false,  // tell jQuery not to process the data
\t\t   contentType: false,  // tell jQuery not to set contentType
\t\t   success : function(data) {

\t\t\tif(data.status == 200){
\t\t\t\tif(data.data.count_error > 0){
\t\t\t\t\t\$('#div_error').html(data.data.txt_error)
\t\t\t\t\t\$('#div_error').show();
\t\t\t\t}

\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#file').val('');
\t\t\t\tload_data();
\t\t\t}

\t\t\t//    if(data['count_error'] > 0){
\t\t\t// \t\$('#div_error').html(data['txt_error'])
\t\t\t// \t\$('#div_error').show();
\t\t\t//    }else{

\t\t\t//    }
\t\t\t//    \$('#bg_loader').hide();
\t\t\t//    \$('#file').val('');
\t\t\t//    load_data();
\t\t\t   
\t\t}, beforeSend: function( xhr ) {
\t\t\t\$('#bg_loader').show();
\t\t}
\t});

}


{% endblock %}
{% block Content2 %}

<div  class=\"container-fluid\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>ยิงรับเอกสารจาก ไปรษณีย์ไทย</b></h3></label><br>
\t\t\t\t
\t\t   </div>\t
\t\t</div>
\t</div>
\t  
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t  <div class=\"tab-content\" id=\"myTabContent\">
\t\t\t\t<div class=\"tab-pane fade show active\" id=\"key\" role=\"tabpanel\" aria-labelledby=\"key-tab\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t\t<form id=\"myform_data_thaipost\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"date_send\"><span class=\"text-muted font_mini\" >วันที่นำส่ง:<span class=\"box_error\" id=\"err_date_send\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-3\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"round\"><span class=\"text-muted font_mini\" >รอบการนำส่ง:</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModal\"  data-backdrop=\"static\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\tUpload Excel ป.ณ.
\t\t\t\t\t\t\t\t\t\t\t\t\t  </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"work_barcode\"><span class=\"text-muted font_mini\" >Barcode: <span class=\"box_error\" id=\"err_work_barcode\"></span></span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_work_barcode\" name=\"work_barcode\" id=\"work_barcode\" class=\"form-control form-control-sm\" type=\"text\" value=\"\" placeholder=\"Enter Barcode\">
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t</div>
\t</div>
\t

<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf_token}}\"></input>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"load_data_bydate();\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">ค้นหา</button>
\t\t\t\t\t<!-- <button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-sm btn-outline-info\" id=\"\">พิมพ์ใบงาน</button> -->
\t\t\t\t<!-- \t\t\t\t
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t-->
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t<span class=\"custom-control-description\"></span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">
\t\t\t\t\t<button onclick=\"cancelwork();\" type=\"button\" class=\"btn btn-danger btn-sm\">
\t\t\t\t\t\tลบรายการ
\t\t\t\t\t</button>
\t\t\t\t</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่จะส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>





<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>




  
  <!-- Modal -->
  <div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog\" role=\"document\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>
\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t  </button>
\t\t</div>
\t\t<div class=\"modal-body\">
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"form-group col-md-6\">
\t\t\t\t\t<label for=\"date_send\"><span class=\"text-muted font_mini\" >วันที่นำส่ง:<span class=\"box_error\" id=\"err_date_send\"></span></span></label>
\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send2\" id=\"date_send2\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t  </div>
\t\t\t\t <div class=\"form-group col-md-6\">
\t\t\t\t\t<label for=\"round\"><span class=\"text-muted font_mini\" >รอบการนำส่ง:</span></label>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round2\"></span>
\t\t\t\t\t<select data-error=\"#err_round2\" class=\"form-control form-control-sm\" id=\"round2\" name=\"round2\">
\t\t\t\t\t\t<option value=\"\" selected disabled>กรุณาเลือกรอบ</option>
\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"card w-100\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<div class=\"col-md-12 col-lg-12\">\t  
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<a onclick=\"\$('#modal_showdata').modal({ backdrop: false});\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เลือกไฟล์ Excel ของท่าน\"></a>
\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"file\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<a  href=\"template_tnt.xlsx\" type=\"button\" class=\"btn btn-dark\" download> \t
\t\t\t\t\t\t\t\t<i class=\"material-icons\">vertical_align_bottom</i>template
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"modal-footer\">
\t\t  <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
\t\t  <button onclick=\"import_excel();\" id=\"btn_fileUpload\" type=\"button\" class=\"btn btn-success\"> \t
\t\t\t<i class=\"material-icons\">vertical_align_top</i> upload
\t\t</button>
\t\t</div>
\t  </div>
\t</div>
  </div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/resive_work_post_in.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/resive_work_post_in.tpl");
    }
}

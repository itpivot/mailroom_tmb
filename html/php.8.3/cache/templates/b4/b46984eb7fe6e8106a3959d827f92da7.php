<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base_emp_branch.tpl */
class __TwigTemplate_86147f2ee5edf32ac0f3bd9938981ec4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'title' => [$this, 'block_title'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'menu_1' => [$this, 'block_menu_1'],
            'menu_3_maim' => [$this, 'block_menu_3_maim'],
            'menu_3_1' => [$this, 'block_menu_3_1'],
            'menu_3_2' => [$this, 'block_menu_3_2'],
            'menu_3_3' => [$this, 'block_menu_3_3'],
            'menu_4' => [$this, 'block_menu_4'],
            'menu_2' => [$this, 'block_menu_2'],
            'menu_6' => [$this, 'block_menu_6'],
            'menu_7_maim' => [$this, 'block_menu_7_maim'],
            'menu_7_1' => [$this, 'block_menu_7_1'],
            'menu_7_2' => [$this, 'block_menu_7_2'],
            'menu_5' => [$this, 'block_menu_5'],
            'Content' => [$this, 'block_Content'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
            'domReady' => [$this, 'block_domReady'],
            'domReady2' => [$this, 'block_domReady2'],
            'javaScript' => [$this, 'block_javaScript'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>

<!-- <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> -->
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" >
<head>
  ";
        // line 6
        $this->displayBlock('head', $context, $blocks);
        // line 62
        echo "  <style type=\"text/css\">
  ";
        // line 63
        $this->displayBlock('styleReady', $context, $blocks);
        // line 64
        echo "\tbody{
\tpadding:0px !important;
\tmargin:0px !important;
\tbackground-position: center;
\t\tbackground-repeat: no-repeat;
\t\tbackground-size: cover;
\t\tbackground-repeat: no-repeat;
\t\tbackground-attachment: fixed;
\t}
\t.footer_ {
\t\t\tbottom: 0;
\t\t\twidth:100%;
\t\t\tbackground-color: #0074c0 ;
\t\t\tcolor:#fff;
\t\t\tvertical-align: middle;
\t\t\tpadding:1%;
\t\t\tfont-size:13;
\t}

\t\t.img{
\t\twidth:100%;
\t\tmargin-bottom:-3%;
\t\t}
\t\t.all{
\t\tpadding-right:0px;
\t\tpadding-left:0px;
\t\theight:100%;
\t\t-webkit-box-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
\t\t-moz-box-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
\t\tbox-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
\t\t
\t\t}
\t\t.m_nav{background: #ece9e6; /* fallback for old browsers */
\t\tbackground: -webkit-linear-gradient(to right, #ece9e6, #ece9e6); /* Chrome 10-25, Safari 5.1-6 */
\t\twidth:100%;
\t\t//opacity: 0.9;
\t\t//filter: alpha(opacity=90);
\t\t}
\t\t
\t\t.nav-item>.active {
\t\t\tcolor: #fff !important;
\t\t\tbackground-color: #0074c0 !important;
\t\t\tborder-radius: 5px;
\t\t\tpadding-left:10px;
\t\t}
\t\t.navbar-nav .nav-link, 
\t\t.show>.navbar-nav .nav-link {
\t\t\tfont-weight: bold !important;
\t\t\tfont-size:14px !important;
\t\t}
\t\t.nav-link:hover{
\t\t\tcolor: #0077c1 !important;
\t\t}
\t\t.nav-item>.active:hover{
\t\t\tcolor: #fff !important;
\t\t}
\t\t.m_nav a {
\t\t\tcolor: #777777;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: transparent;
\t\t\t-webkit-text-decoration-skip: objects;
\t\t}
\t.btn_logout{
\t\tborder: none;
\t\toutline: 0;
\t\tdisplay: inline-block;
\t\tpadding: 8px;
\t\tcolor: #000;
\t\ttext-align: center;
\t\tcursor: pointer;
\t\twidth: 100%;
\t\tfont-size: 18px;
\t}

  </style>
</head>
<body onload=\"resize()\" onresize=\"resize()\">
<!--
<body background=\"../themes/images/bg.png\">
body>Start Header-->




 


<!--End Header-->
<!--Start Container-->
<div class=\"container-fluid\">
<div class=\"row justify-content-center\">
    <div class=\"all col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xl-offset-1 col-lg-offset-1\">\t
\t\t\t\t
\t\t<header class=\"panel-header\" >
\t\t\t\t<img class=\"img\"  src=\"../themes/images/Artboard4.png\" alt=\"Smiley face\" class=\"img-fluid mx-auto \" alt=\"Responsive image\">
\t\t\t\t<nav class=\"m_nav navbar navbar-expand-lg navbar-light bg-light\">
\t\t\t\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
\t\t\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t\t\t  </button>
\t\t\t\t\t   <div class=\"collapse navbar-collapse\" id=\"navbarNavDropdown\">
\t\t\t\t\t<ul class=\"navbar-nav mr-auto\">
\t\t\t\t\t
\t\t\t\t\t\t<li class=\"nav-item\" id=\"menu_1\">
\t\t\t\t\t\t\t<a class=\"nav-link ";
        // line 167
        $this->displayBlock('menu_1', $context, $blocks);
        echo "\" href=\"./\">หน้าหลัก</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t  <li class=\"nav-item dropdown\">
\t\t\t\t\t\t\t\t<a class=\"nav-link dropdown-toggle ";
        // line 170
        $this->displayBlock('menu_3_maim', $context, $blocks);
        echo "\" href=\"#\" id=\"navbarDropdown2\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\tสร้างรายการนำส่ง
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown2\">
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item ";
        // line 174
        $this->displayBlock('menu_3_1', $context, $blocks);
        echo "\" href=\"create_work.php\">ส่งสาขาและอาคารต่างๆ</a>
\t\t\t\t\t\t\t\t\t";
        // line 175
        if (((((((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 175) == 1386) || (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 175) == 1383)) || (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 175) == 1384)) || (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 175) == 1385)) || (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 175) == 1419)) || (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 175) == 88))) {
            // line 176
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item ";
            // line 177
            $this->displayBlock('menu_3_2', $context, $blocks);
            echo "\" href=\"work_byhand.php\">By Hand</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 179
        echo "\t\t\t\t\t\t\t\t\t";
        if (((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 179) == 1383) || (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 179) == 88))) {
            // line 180
            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item ";
            // line 181
            $this->displayBlock('menu_3_3', $context, $blocks);
            echo "\" href=\"work_post.php\">ส่งไปรษณีย์</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 183
        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t<a class=\"nav-link ";
        // line 186
        $this->displayBlock('menu_4', $context, $blocks);
        echo "\" href=\"send_work_all.php\">รวมเอกสารนำส่ง</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t<a class=\"nav-link ";
        // line 189
        $this->displayBlock('menu_2', $context, $blocks);
        echo "\" href=\"search_work.php\">รายการเอกสารนำส่ง</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        // line 191
        if ((((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 191) != "TL0001") && (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 191) != "88415")) && (twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 191) != "CL0193/63/013"))) {
            // line 192
            echo "\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t<a class=\"nav-link ";
            // line 193
            $this->displayBlock('menu_6', $context, $blocks);
            echo "\" href=\"receive_work.php\">รับเอกสาร</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        } else {
            // line 196
            echo "\t\t\t\t\t\t  <li class=\"nav-item dropdown\">
\t\t\t\t\t\t\t\t<a class=\"nav-link dropdown-toggle ";
            // line 197
            $this->displayBlock('menu_7_maim', $context, $blocks);
            echo "\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\tรับเอกสาร
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item ";
            // line 201
            $this->displayBlock('menu_7_1', $context, $blocks);
            echo "\" href=\"receive_work.php\">รับเอกสารส่วนตัว</a>
\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item ";
            // line 203
            $this->displayBlock('menu_7_2', $context, $blocks);
            echo "\" href=\"reciever_branch.php\">รับเอกสาร Mailroom</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        }
        // line 207
        echo "\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t\t<a class=\"nav-link ";
        // line 208
        $this->displayBlock('menu_5', $context, $blocks);
        echo "\" href=\"reports.php\">รายงาน</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t\t<ul class=\"nav nav-pills\">\t\t\t\t\t\t\t
\t\t\t\t\t\t<li class=\"nav-item dropdown\">
\t\t\t\t\t\t\t<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown2\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t";
        // line 215
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 215), "html", null, true);
        echo "
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown2\">
\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t<a href=\"profile.php\" class=\"dropdown-item preview-item text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t\t\t\t\t\t<i style=\"font-size: 50px !important;\"class=\"material-icons md-18\">perm_identity</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"preview-item-content\">
\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"preview-subject font-weight-medium text-dark\">Profile</h6>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"font-weight-light small-text\">
\t\t\t\t\t\t\t\t\t\t\t\tSettings
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item preview-item\">
\t\t\t\t\t\t\t\t\t\t\t<a class=\"btn_logout bg-primary text-white\" href=\"profile.php\">แก้ไขข้อมูลส่วนตัว</a>
\t\t\t\t\t\t\t\t\t\t<div class=\"preview-thumbnail\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"preview-icon bg-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"mdi mdi-email-outline mx-0\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"preview-item-content\">
\t\t\t\t\t\t\t\t\t\t\t<a class=\"btn_logout bg-primary text-white\" href=\"../user/logout.php\">ออกจากระบบ</a>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</a>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</nav>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t


\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t</header> 
\t\t\t
\t\t
\t

\t<!--  <header class=\"panel-header\" style=\"background-color: #DCD9D4; 
\tbackground-image: linear-gradient(to bottom, rgba(255,255,255,0.50) 0%, rgba(0,0,0,0.50) 100%), radial-gradient(at 50% 0%, rgba(255,255,255,0.10) 0%, rgba(0,0,0,0.50) 50%); 
\tbackground-blend-mode: soft-light,screen;\">
\t\t\t<span class=\"js-slideout-toggle\"><i class=\"material-icons\">menu</i> <b>MENU</b> </span>
\t\t\t<span class=\"header-nav\">
\t\t\t\t
\t\t\t\t\t<label class=\"header-text\" >The Digital PEON Book System</label>
\t\t\t\t<!-- <img class=\"img-responsive\" style=\"position:absolute;right:0px;padding:10px;\" src=\"../themes/images/logo-tmb_mini.png\" title=\"TMB\" />  \t
\t\t\t</span>
\t</header>  -->
\t\t\t
\t
\t\t
\t\t<div  class=\"con\">
\t\t\t<div id=\"main\">";
        // line 282
        $this->displayBlock('Content', $context, $blocks);
        echo "</div>
\t\t\t<div>";
        // line 283
        $this->displayBlock('Content2', $context, $blocks);
        echo "</div>
\t\t</div>
\t
\t<!--End Container-->
\t
<!--Start Footer-->
\t\t<footer class=\"footer_ align-middle text-center\">
\t\t\t<div class=\"align-middle\"> 
\t\t\t\t<p class=\"align-middle\">
\t\t\t\t\t<span class=\"\">
\t\t\t\t\t\t<strong>ติดต่อห้อง Mailroom:</strong>
\t\t\t\t\t\t02-299-1111 ต่อ 5694
\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p class=\"\"><b>ปัญหาการใช้งานระบบติดต่อที่:</b></p>
\t\t\t\t<p class=\"\"><strong></strong>คุณจุฑารัตน์ เนียม ศรี เบอร์โทร 061-823-4346 อีเมล์ jutarat_ni@pivot.co.th</p>
\t\t\t\t<p class=\"\"><strong></strong>คุณณิธิวัชรา อยู่ถิ่น เบอร์โทร 02-299-5694 (ห้องสารบรรณกลาง) อีเมล์ Mailroom@pivot.co.th</p>
\t\t\t\t<p class=\"align-middle\">Copyright © 2018 Pivot Co., Ltd.</p>
\t\t\t</div>
\t\t</footer>
\t<!--End Footer-->
\t</div>
</div>







</div>


<!--Start Debug-->

\t";
        // line 318
        $this->displayBlock('debug', $context, $blocks);
        // line 319
        echo "<!--End Debug-->

\t<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src=\"../themes/bootstrap/js/jquery.min.js\"></script> -->
    <script>window.jQuery || document.write('<script src=\"../themes/bootstrap/js/jquery.vendor.min.js\"><\\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=\"../themes/bootstrap/js/ie10-viewport-bug-workaround.js\"></script>
\t
<!-- \t<link href=\"../themes/jquery/jquery-ui.css\" rel=\"stylesheet\"> -->
\t  
\t
<!-- \t<script src=\"../themes/bootstrap_emp/dist/sweetalert2.min.js\"></script> -->
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js\"></script>
\t
    <script src=\"../themes/bootstrap_emp/popper/popper.min.js\"></script>
    <script src=\"../themes/bootstrap_emp/bootstrap/js/bootstrap.min.js\"></script>
\t
\t
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js\"></script>
\t<script src=\"../themes/fancybox/jquery.fancybox.min.js\"></script>
\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/select2-bootstrap4.min.css\">

\t<script src=\"../themes/progress-tracker/scripts/site.js\"></script>
\t<script src=\"../themes/moment/moment.min.js\"></script>
\t<script src=\"../themes/moment/moment-local.min.js\"></script>

\t<script src=\"../themes/jquery-validate/jquery.validate.min.js\"></script>
\t<script src=\"../themes/alertifyjs/alertify.js\"></script> 
\t<!-- <script src=\"../themes/jquery/jquery-ui.js\"></script> -->
\t<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
\t<!-- <script src=\"../themes/bootstrap/js/accounting.min.js\"></script> -->
\t<script type=\"text/javascript\">

\tfunction resize() {
            var frame = document.getElementById(\"main\");
            var windowheight = window.innerHeight;
\t\t\t\twindowheight -= 290;
            //document.body.style.height = windowheight + \"px\";
           // frame.style.mi-height = windowheight - 290 + \"px\";
\t\t\t\$('#main').css('min-height', windowheight);
        }
\t\t\$(document).ready(function(){
\t\t
\t\t 
\t\t
\t\t
\t\t
\t\t
\t\t
\t\t\$('.dropdown-toggle').dropdown()
\t";
        // line 371
        if ((($context["select"] ?? null) == 0)) {
            // line 372
            echo "\t";
        } else {
            // line 373
            echo "\t\t\$('select').select2(
\t\t\t{
\t\t\t\ttheme: 'bootstrap4',
\t\t\t}
\t\t);
\t";
        }
        // line 379
        echo "\t\t
\t\tvar usrID = '";
        // line 380
        echo twig_escape_filter($this->env, ($context["userID"] ?? null), "html", null, true);
        echo "';
\t\t\$.ajax({
\t\t\turl: '../data/ajax/ajax_check_password_Date.php',
\t\t\tmethod: 'GET',
\t\t\tdata: { userID: usrID },
\t\t\tdataType: 'json',
\t\t\tsuccess: function (res) {
\t\t\t//console.log(res);
\t\t\t\tif(parseInt(res['diffdate']) >= 45 ) {
\t\t\t\t\t//alert('');
\t\t\t\t\talertify.alert('Alert!!', 'กรุณาเปลี่ยนรหัสผ่าน เนื่องจากรหัสผ่านของคุณมีอายุการใช้งานเกิน 45 วันแล้ว!', function(){ 
\t\t\t\t\t\t//alertify.success('Ok'); 
\t\t\t\t\t\twindow.location.href='../user/change_password.php?usr='+res['usrID'];
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}
\t\t});
\t\t
\t\t\t";
        // line 398
        $this->displayBlock('domReady', $context, $blocks);
        // line 399
        echo "     \t    ";
        $this->displayBlock('domReady2', $context, $blocks);
        echo " 
\t\t});
\t   ";
        // line 401
        $this->displayBlock('javaScript', $context, $blocks);
        // line 403
        echo "
\t    //window.onbeforeunload = function (e) {
        //            e = e || window.event;
        //            // For IE and Firefox prior to version 4
        //            if (e) {
        //              // e.returnValue = 'ท่านต้องการปิดหน้านี้หรือไม่';
        //              if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //                 location.href = '../user/logout.php';
        //              }
        //            }
        //            // For Safari
        //        //return 'ท่านต้องการปิดหน้านี้หรือไม่';
        //       // if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //              // location.href = '../user/logout.php';
        //       // }
        //};
\t</script>
\t<!-- Global site tag (gtag.js) - Google Analytics -->
\t<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-116752839-1\"></script>
\t<script>
\t  window.dataLayer = window.dataLayer || [];
\t  function gtag(){dataLayer.push(arguments);}
\t  gtag('js', new Date());

\t  gtag('config', 'UA-116752839-1');
\t</script>

\t


\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
</body>
</html>
";
    }

    // line 6
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "\t\t<title>The Digital PEON Book System";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"description\" content=\"Pivot Mailroom Services\">
\t\t<meta name=\"author\" content=\"Pivot\">
\t\t<link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" />
\t\t <meta charset=\"utf-8\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
\t\t
\t\t   <!-- Bootstrap core CSS 
   <link href=\"../themes/bootstrap-4.1.3/css/bootstrap.min.css\" rel=\"stylesheet\">
     --><link href=\"../themes/bootstrap_emp/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">
  <!--
   <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css\" integrity=\"sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ\" crossorigin=\"anonymous\">
\t
\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
\t-->
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/alertify.css\" id=\"alertifyCSS\">
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/themes/default.css\" >
\t
\t  <!-- Datepicker -->
\t
    <!-- Custom styles for this template -->
  
\t\t
\t<link href=\"../themes/bootstrap/css/sticky-footer-navbar.css\" rel=\"stylesheet\">
\t\t\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/material_icon/material-icons.min.css\">
\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/fancybox/jquery.fancybox.min.css\">

\t<!-- <link rel=\"stylesheet\" href=\"../themes/progress-tracker/styles/site.css\"> -->
    <link rel=\"stylesheet\" href=\"../themes/progress-tracker/styles/progress-tracker.css\">
\t 
\t<!-- <script src=\"../themes/bootstrap_emp/jquery/jquery.min.js\"></script> -->
\t<script src=\"../scripts/jquery-1.12.4.js\"></script>
\t
\t";
        // line 44
        $this->displayBlock('scriptImport', $context, $blocks);
        // line 45
        echo "\t<script src=\"../themes/bootstrap/js/ie-emulation-modes-warning.js\"></script>
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js\"></script>

\t<!-- <link rel=\"stylesheet\" href=\"../themes/bootstrap_emp/dist/sweetalert2.min.css\"> -->
\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css\" rel=\"stylesheet\" />
\t
\t
\t
\t<script src=\"../themes/bootstrap_emp/dist/slideout.min.js\"></script>
\t
\t
\t\t<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
\t\t<!--[if lt IE 9]>
\t\t  <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
\t\t  <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
\t\t<![endif]-->
  ";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 44
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 63
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 167
    public function block_menu_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 170
    public function block_menu_3_maim($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 174
    public function block_menu_3_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 177
    public function block_menu_3_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 181
    public function block_menu_3_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 186
    public function block_menu_4($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 189
    public function block_menu_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 193
    public function block_menu_6($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 197
    public function block_menu_7_maim($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 201
    public function block_menu_7_1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 203
    public function block_menu_7_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 208
    public function block_menu_5($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 282
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 283
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 318
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 398
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 399
    public function block_domReady2($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 401
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 402
        echo "\t   ";
    }

    public function getTemplateName()
    {
        return "base_emp_branch.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  746 => 402,  742 => 401,  736 => 399,  730 => 398,  724 => 318,  718 => 283,  712 => 282,  705 => 208,  698 => 203,  691 => 201,  684 => 197,  677 => 193,  670 => 189,  663 => 186,  656 => 181,  649 => 177,  642 => 174,  635 => 170,  628 => 167,  622 => 63,  616 => 44,  610 => 7,  590 => 45,  588 => 44,  547 => 7,  543 => 6,  493 => 403,  491 => 401,  485 => 399,  483 => 398,  462 => 380,  459 => 379,  451 => 373,  448 => 372,  446 => 371,  392 => 319,  390 => 318,  352 => 283,  348 => 282,  278 => 215,  268 => 208,  265 => 207,  258 => 203,  253 => 201,  246 => 197,  243 => 196,  237 => 193,  234 => 192,  232 => 191,  227 => 189,  221 => 186,  216 => 183,  211 => 181,  208 => 180,  205 => 179,  200 => 177,  197 => 176,  195 => 175,  191 => 174,  184 => 170,  178 => 167,  73 => 64,  71 => 63,  68 => 62,  66 => 6,  59 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>

<!-- <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> -->
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" >
<head>
  {% block head %}
\t\t<title>The Digital PEON Book System{% block title %}{% endblock %}</title>
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"description\" content=\"Pivot Mailroom Services\">
\t\t<meta name=\"author\" content=\"Pivot\">
\t\t<link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" />
\t\t <meta charset=\"utf-8\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
\t\t
\t\t   <!-- Bootstrap core CSS 
   <link href=\"../themes/bootstrap-4.1.3/css/bootstrap.min.css\" rel=\"stylesheet\">
     --><link href=\"../themes/bootstrap_emp/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">
  <!--
   <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css\" integrity=\"sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ\" crossorigin=\"anonymous\">
\t
\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
\t-->
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/alertify.css\" id=\"alertifyCSS\">
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/themes/default.css\" >
\t
\t  <!-- Datepicker -->
\t
    <!-- Custom styles for this template -->
  
\t\t
\t<link href=\"../themes/bootstrap/css/sticky-footer-navbar.css\" rel=\"stylesheet\">
\t\t\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/material_icon/material-icons.min.css\">
\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/fancybox/jquery.fancybox.min.css\">

\t<!-- <link rel=\"stylesheet\" href=\"../themes/progress-tracker/styles/site.css\"> -->
    <link rel=\"stylesheet\" href=\"../themes/progress-tracker/styles/progress-tracker.css\">
\t 
\t<!-- <script src=\"../themes/bootstrap_emp/jquery/jquery.min.js\"></script> -->
\t<script src=\"../scripts/jquery-1.12.4.js\"></script>
\t
\t{% block scriptImport %}{% endblock %}
\t<script src=\"../themes/bootstrap/js/ie-emulation-modes-warning.js\"></script>
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js\"></script>

\t<!-- <link rel=\"stylesheet\" href=\"../themes/bootstrap_emp/dist/sweetalert2.min.css\"> -->
\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css\" rel=\"stylesheet\" />
\t
\t
\t
\t<script src=\"../themes/bootstrap_emp/dist/slideout.min.js\"></script>
\t
\t
\t\t<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
\t\t<!--[if lt IE 9]>
\t\t  <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
\t\t  <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
\t\t<![endif]-->
  {% endblock %}
  <style type=\"text/css\">
  {% block styleReady %}{% endblock %}
\tbody{
\tpadding:0px !important;
\tmargin:0px !important;
\tbackground-position: center;
\t\tbackground-repeat: no-repeat;
\t\tbackground-size: cover;
\t\tbackground-repeat: no-repeat;
\t\tbackground-attachment: fixed;
\t}
\t.footer_ {
\t\t\tbottom: 0;
\t\t\twidth:100%;
\t\t\tbackground-color: #0074c0 ;
\t\t\tcolor:#fff;
\t\t\tvertical-align: middle;
\t\t\tpadding:1%;
\t\t\tfont-size:13;
\t}

\t\t.img{
\t\twidth:100%;
\t\tmargin-bottom:-3%;
\t\t}
\t\t.all{
\t\tpadding-right:0px;
\t\tpadding-left:0px;
\t\theight:100%;
\t\t-webkit-box-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
\t\t-moz-box-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
\t\tbox-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
\t\t
\t\t}
\t\t.m_nav{background: #ece9e6; /* fallback for old browsers */
\t\tbackground: -webkit-linear-gradient(to right, #ece9e6, #ece9e6); /* Chrome 10-25, Safari 5.1-6 */
\t\twidth:100%;
\t\t//opacity: 0.9;
\t\t//filter: alpha(opacity=90);
\t\t}
\t\t
\t\t.nav-item>.active {
\t\t\tcolor: #fff !important;
\t\t\tbackground-color: #0074c0 !important;
\t\t\tborder-radius: 5px;
\t\t\tpadding-left:10px;
\t\t}
\t\t.navbar-nav .nav-link, 
\t\t.show>.navbar-nav .nav-link {
\t\t\tfont-weight: bold !important;
\t\t\tfont-size:14px !important;
\t\t}
\t\t.nav-link:hover{
\t\t\tcolor: #0077c1 !important;
\t\t}
\t\t.nav-item>.active:hover{
\t\t\tcolor: #fff !important;
\t\t}
\t\t.m_nav a {
\t\t\tcolor: #777777;
\t\t\ttext-decoration: none;
\t\t\tbackground-color: transparent;
\t\t\t-webkit-text-decoration-skip: objects;
\t\t}
\t.btn_logout{
\t\tborder: none;
\t\toutline: 0;
\t\tdisplay: inline-block;
\t\tpadding: 8px;
\t\tcolor: #000;
\t\ttext-align: center;
\t\tcursor: pointer;
\t\twidth: 100%;
\t\tfont-size: 18px;
\t}

  </style>
</head>
<body onload=\"resize()\" onresize=\"resize()\">
<!--
<body background=\"../themes/images/bg.png\">
body>Start Header-->




 


<!--End Header-->
<!--Start Container-->
<div class=\"container-fluid\">
<div class=\"row justify-content-center\">
    <div class=\"all col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xl-offset-1 col-lg-offset-1\">\t
\t\t\t\t
\t\t<header class=\"panel-header\" >
\t\t\t\t<img class=\"img\"  src=\"../themes/images/Artboard4.png\" alt=\"Smiley face\" class=\"img-fluid mx-auto \" alt=\"Responsive image\">
\t\t\t\t<nav class=\"m_nav navbar navbar-expand-lg navbar-light bg-light\">
\t\t\t\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
\t\t\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t\t\t  </button>
\t\t\t\t\t   <div class=\"collapse navbar-collapse\" id=\"navbarNavDropdown\">
\t\t\t\t\t<ul class=\"navbar-nav mr-auto\">
\t\t\t\t\t
\t\t\t\t\t\t<li class=\"nav-item\" id=\"menu_1\">
\t\t\t\t\t\t\t<a class=\"nav-link {% block menu_1 %} {% endblock %}\" href=\"./\">หน้าหลัก</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t  <li class=\"nav-item dropdown\">
\t\t\t\t\t\t\t\t<a class=\"nav-link dropdown-toggle {% block menu_3_maim %} {% endblock %}\" href=\"#\" id=\"navbarDropdown2\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\tสร้างรายการนำส่ง
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown2\">
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item {% block menu_3_1 %} {% endblock %}\" href=\"create_work.php\">ส่งสาขาและอาคารต่างๆ</a>
\t\t\t\t\t\t\t\t\t{% if user_data.mr_branch_id == 1386  or user_data.mr_branch_id == 1383 or user_data.mr_branch_id == 1384 or user_data.mr_branch_id == 1385 or user_data.mr_branch_id == 1419 or user_data.mr_branch_id == 88%}
\t\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item {% block menu_3_2 %} {% endblock %}\" href=\"work_byhand.php\">By Hand</a>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% if user_data.mr_branch_id == 1383 or user_data.mr_branch_id == 88 %}
\t\t\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item {% block menu_3_3 %} {% endblock %}\" href=\"work_post.php\">ส่งไปรษณีย์</a>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t<a class=\"nav-link {% block menu_4 %} {% endblock %}\" href=\"send_work_all.php\">รวมเอกสารนำส่ง</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t<a class=\"nav-link {% block menu_2 %} {% endblock %}\" href=\"search_work.php\">รายการเอกสารนำส่ง</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t{% if user_data.mr_user_username != \"TL0001\" and  user_data.mr_user_username != \"88415\" and  user_data.mr_user_username != \"CL0193/63/013\" %}
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t<a class=\"nav-link {% block menu_6 %} {% endblock %}\" href=\"receive_work.php\">รับเอกสาร</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t  <li class=\"nav-item dropdown\">
\t\t\t\t\t\t\t\t<a class=\"nav-link dropdown-toggle {% block menu_7_maim %} {% endblock %}\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\tรับเอกสาร
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item {% block menu_7_1 %} {% endblock %}\" href=\"receive_work.php\">รับเอกสารส่วนตัว</a>
\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item {% block menu_7_2 %} {% endblock %}\" href=\"reciever_branch.php\">รับเอกสาร Mailroom</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t\t\t<a class=\"nav-link {% block menu_5 %} {% endblock %}\" href=\"reports.php\">รายงาน</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t\t<ul class=\"nav nav-pills\">\t\t\t\t\t\t\t
\t\t\t\t\t\t<li class=\"nav-item dropdown\">
\t\t\t\t\t\t\t<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown2\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t{{ user_data.mr_user_username }}
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown2\">
\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t<a href=\"profile.php\" class=\"dropdown-item preview-item text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t\t\t\t\t\t<i style=\"font-size: 50px !important;\"class=\"material-icons md-18\">perm_identity</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"preview-item-content\">
\t\t\t\t\t\t\t\t\t\t\t<h6 class=\"preview-subject font-weight-medium text-dark\">Profile</h6>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"font-weight-light small-text\">
\t\t\t\t\t\t\t\t\t\t\t\tSettings
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>
\t\t\t\t\t\t\t\t\t<a class=\"dropdown-item preview-item\">
\t\t\t\t\t\t\t\t\t\t\t<a class=\"btn_logout bg-primary text-white\" href=\"profile.php\">แก้ไขข้อมูลส่วนตัว</a>
\t\t\t\t\t\t\t\t\t\t<div class=\"preview-thumbnail\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"preview-icon bg-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"mdi mdi-email-outline mx-0\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"preview-item-content\">
\t\t\t\t\t\t\t\t\t\t\t<a class=\"btn_logout bg-primary text-white\" href=\"../user/logout.php\">ออกจากระบบ</a>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</a>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</nav>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t


\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t</header> 
\t\t\t
\t\t
\t

\t<!--  <header class=\"panel-header\" style=\"background-color: #DCD9D4; 
\tbackground-image: linear-gradient(to bottom, rgba(255,255,255,0.50) 0%, rgba(0,0,0,0.50) 100%), radial-gradient(at 50% 0%, rgba(255,255,255,0.10) 0%, rgba(0,0,0,0.50) 50%); 
\tbackground-blend-mode: soft-light,screen;\">
\t\t\t<span class=\"js-slideout-toggle\"><i class=\"material-icons\">menu</i> <b>MENU</b> </span>
\t\t\t<span class=\"header-nav\">
\t\t\t\t
\t\t\t\t\t<label class=\"header-text\" >The Digital PEON Book System</label>
\t\t\t\t<!-- <img class=\"img-responsive\" style=\"position:absolute;right:0px;padding:10px;\" src=\"../themes/images/logo-tmb_mini.png\" title=\"TMB\" />  \t
\t\t\t</span>
\t</header>  -->
\t\t\t
\t
\t\t
\t\t<div  class=\"con\">
\t\t\t<div id=\"main\">{% block Content %}{% endblock %}</div>
\t\t\t<div>{% block Content2 %}{% endblock %}</div>
\t\t</div>
\t
\t<!--End Container-->
\t
<!--Start Footer-->
\t\t<footer class=\"footer_ align-middle text-center\">
\t\t\t<div class=\"align-middle\"> 
\t\t\t\t<p class=\"align-middle\">
\t\t\t\t\t<span class=\"\">
\t\t\t\t\t\t<strong>ติดต่อห้อง Mailroom:</strong>
\t\t\t\t\t\t02-299-1111 ต่อ 5694
\t\t\t\t\t</span>
\t\t\t\t</p>
\t\t\t\t<p class=\"\"><b>ปัญหาการใช้งานระบบติดต่อที่:</b></p>
\t\t\t\t<p class=\"\"><strong></strong>คุณจุฑารัตน์ เนียม ศรี เบอร์โทร 061-823-4346 อีเมล์ jutarat_ni@pivot.co.th</p>
\t\t\t\t<p class=\"\"><strong></strong>คุณณิธิวัชรา อยู่ถิ่น เบอร์โทร 02-299-5694 (ห้องสารบรรณกลาง) อีเมล์ Mailroom@pivot.co.th</p>
\t\t\t\t<p class=\"align-middle\">Copyright © 2018 Pivot Co., Ltd.</p>
\t\t\t</div>
\t\t</footer>
\t<!--End Footer-->
\t</div>
</div>







</div>


<!--Start Debug-->

\t{% block debug %}{% endblock %}
<!--End Debug-->

\t<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src=\"../themes/bootstrap/js/jquery.min.js\"></script> -->
    <script>window.jQuery || document.write('<script src=\"../themes/bootstrap/js/jquery.vendor.min.js\"><\\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=\"../themes/bootstrap/js/ie10-viewport-bug-workaround.js\"></script>
\t
<!-- \t<link href=\"../themes/jquery/jquery-ui.css\" rel=\"stylesheet\"> -->
\t  
\t
<!-- \t<script src=\"../themes/bootstrap_emp/dist/sweetalert2.min.js\"></script> -->
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js\"></script>
\t
    <script src=\"../themes/bootstrap_emp/popper/popper.min.js\"></script>
    <script src=\"../themes/bootstrap_emp/bootstrap/js/bootstrap.min.js\"></script>
\t
\t
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js\"></script>
\t<script src=\"../themes/fancybox/jquery.fancybox.min.js\"></script>
\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/select2-bootstrap4.min.css\">

\t<script src=\"../themes/progress-tracker/scripts/site.js\"></script>
\t<script src=\"../themes/moment/moment.min.js\"></script>
\t<script src=\"../themes/moment/moment-local.min.js\"></script>

\t<script src=\"../themes/jquery-validate/jquery.validate.min.js\"></script>
\t<script src=\"../themes/alertifyjs/alertify.js\"></script> 
\t<!-- <script src=\"../themes/jquery/jquery-ui.js\"></script> -->
\t<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
\t<!-- <script src=\"../themes/bootstrap/js/accounting.min.js\"></script> -->
\t<script type=\"text/javascript\">

\tfunction resize() {
            var frame = document.getElementById(\"main\");
            var windowheight = window.innerHeight;
\t\t\t\twindowheight -= 290;
            //document.body.style.height = windowheight + \"px\";
           // frame.style.mi-height = windowheight - 290 + \"px\";
\t\t\t\$('#main').css('min-height', windowheight);
        }
\t\t\$(document).ready(function(){
\t\t
\t\t 
\t\t
\t\t
\t\t
\t\t
\t\t
\t\t\$('.dropdown-toggle').dropdown()
\t{% if select == 0 %}
\t{% else %}
\t\t\$('select').select2(
\t\t\t{
\t\t\t\ttheme: 'bootstrap4',
\t\t\t}
\t\t);
\t{% endif %}
\t\t
\t\tvar usrID = '{{ userID }}';
\t\t\$.ajax({
\t\t\turl: '../data/ajax/ajax_check_password_Date.php',
\t\t\tmethod: 'GET',
\t\t\tdata: { userID: usrID },
\t\t\tdataType: 'json',
\t\t\tsuccess: function (res) {
\t\t\t//console.log(res);
\t\t\t\tif(parseInt(res['diffdate']) >= 45 ) {
\t\t\t\t\t//alert('');
\t\t\t\t\talertify.alert('Alert!!', 'กรุณาเปลี่ยนรหัสผ่าน เนื่องจากรหัสผ่านของคุณมีอายุการใช้งานเกิน 45 วันแล้ว!', function(){ 
\t\t\t\t\t\t//alertify.success('Ok'); 
\t\t\t\t\t\twindow.location.href='../user/change_password.php?usr='+res['usrID'];
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}
\t\t});
\t\t
\t\t\t{% block domReady %}{% endblock %}
     \t    {% block domReady2 %}{% endblock %} 
\t\t});
\t   {% block javaScript %}
\t   {% endblock %}

\t    //window.onbeforeunload = function (e) {
        //            e = e || window.event;
        //            // For IE and Firefox prior to version 4
        //            if (e) {
        //              // e.returnValue = 'ท่านต้องการปิดหน้านี้หรือไม่';
        //              if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //                 location.href = '../user/logout.php';
        //              }
        //            }
        //            // For Safari
        //        //return 'ท่านต้องการปิดหน้านี้หรือไม่';
        //       // if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //              // location.href = '../user/logout.php';
        //       // }
        //};
\t</script>
\t<!-- Global site tag (gtag.js) - Google Analytics -->
\t<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-116752839-1\"></script>
\t<script>
\t  window.dataLayer = window.dataLayer || [];
\t  function gtag(){dataLayer.push(arguments);}
\t  gtag('js', new Date());

\t  gtag('config', 'UA-116752839-1');
\t</script>

\t


\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
\t
</body>
</html>
", "base_emp_branch.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\base_emp_branch.tpl");
    }
}

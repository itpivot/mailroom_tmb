<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* branch/search_work.tpl */
class __TwigTemplate_b7675b190e617dc17f9aed33a5c85443 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_2' => [$this, 'block_menu_2'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp_branch.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp_branch.tpl", "branch/search_work.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_2($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 16
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "\t#btn_search:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t
\t#btn_search{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#btn_clear:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t.input-group-addon{
\t\tbackground-color: #FFFFFF; 
\t\tborder: 1px solid #055d97;
\t
\t}

\t.pt-primary {
\t\tcolor: #03a9fa;
\t}

\t.pt-cyan {
\t\tcolor: #00bcd4;
\t}

\t.pt-indigo {
\t\tcolor: #5677fc;
\t}

\t.pt-teal {
\t\tcolor: #009688;
\t}

\t.pt-cancel {
\t\tcolor: #9e9e9e;
\t}

\t.pt-lemon {
\t\tcolor: #8bc34a;
\t}
\t.pt-success {
\t\tcolor: #259b24;
\t}
\t
\t.pt-dark {
\t\tcolor: #212121;
\t}

\t.pt-amber {
\t\tcolor: #ffc107;
\t}

\t#tb_work_order tr th,
\t#tb_work_order tr td {
\t\tfont-size: 14px;
\t}
\t
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t\tdisplay:none;
\t}
 
";
    }

    // line 126
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 127
        echo "
";
        // line 128
        echo twig_escape_filter($this->env, ($context["alertt"] ?? null), "html", null, true);
        echo "

\t\t\$('.input-daterange').datepicker({
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\ttodayHighlight: true
\t\t});\t
\t\t
\t\t
\t\tvar table = \$(\"#tb_work_order\").DataTable();
\t\tvar mr_type_work_id \t\t= \$(\"#mr_type_work_id\").val();
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status\t\t \t\t= \$(\"#status_id\").val();
\t\t\tvar start_date \t\t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t\t= \$(\"#end_date\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar department_id \t\t= \$(\"#department_id\").val();
\t\t\tvar branch_id \t\t\t= \$(\"#branch_id\").val();
\t\t\t
\t\t\tif( \$(\"#emp_data_1\").is(':checked')){
\t\t\t\tvar emp_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar emp_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#department_data_1\").is(':checked')){
\t\t\t\tvar department_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar department_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#branch_data_1\").is(':checked')){
\t\t\t\tvar branch_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar branch_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\t//alert(emp_id);
\t\t\t//alert(user_id);
\t\t\t//return;
\t\t\t
\t\t\t
\t\t\t\ttable.destroy();
\t\t\t\ttable = \$(\"#tb_work_order\").DataTable({
\t\t\t\t\t\"ajax\": {
\t\t\t\t\t\t\"url\": \"ajax/ajax_search_work_mailroom.php\",
\t\t\t\t\t\t\"type\": \"POST\",
\t\t\t\t\t\t\"data\": {
\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'status' :status,\t\t \t
\t\t\t\t\t\t'mr_type_work_id' : mr_type_work_id,\t
\t\t\t\t\t\t'start_date' : start_date,\t
\t\t\t\t\t\t'end_date' : end_date,\t\t
\t\t\t\t\t\t'user_id' : user_id,\t\t
\t\t\t\t\t\t'emp_id' : emp_id,\t\t\t
\t\t\t\t\t\t'department_id' : department_id,\t
\t\t\t\t\t\t'branch_id' : branch_id,\t\t
\t\t\t\t\t\t'emp_type' : emp_type,
\t\t\t\t\t\t'department_type' : department_type,
\t\t\t\t\t\t'branch_type' : branch_type
\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t
\t\t\t\t\t'columns': [
\t\t\t\t\t
\t\t\t\t\t\t{ 'data':'action' },
\t\t\t\t\t\t{ 'data':'status' },
\t\t\t\t\t\t{ 'data':'barcode_show' },
\t\t\t\t\t\t{ 'data':'time_test'},
\t\t\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t\t\t{ 'data':'add_receiver' },
\t\t\t\t\t\t{ 'data':'name_receive2' },
\t\t\t\t\t\t{ 'data':'add_receiver2' },
\t\t\t\t\t\t{ 'data':'name_send' },
\t\t\t\t\t\t{ 'data':'add_sender' },
\t\t\t\t\t\t{ 'data':'mr_topic' },
\t\t\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t\t\t{ 'data':'mr_work_remark' }
\t\t\t\t\t],
\t\t\t\t\t/* \"fnRowCallback\": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
\t\t\t\t\t\tif (aData['scg_status_name'] == \"Newly\") {
\t\t\t\t\t\t\t\t\$(nRow).addClass('hilight');
\t\t\t\t\t\t\t}
\t\t\t\t\t\telse{
\t\t\t\t\t\t\t\$(nRow).removeClass('hilight');
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}, */
\t\t\t\t\t'scrollCollapse': true
\t\t\t\t});
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t<!-- Search -->
\t\t\t\$(\"#btn_search\").click(function(){
\t\t\tvar mr_type_work_id \t= \$(\"#mr_type_work_id\").val();
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status\t\t \t\t= \$(\"#status_id\").val();
\t\t\tvar start_date \t\t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t\t= \$(\"#end_date\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar department_id \t\t= \$(\"#department_id\").val();
\t\t\tvar branch_id \t\t\t= \$(\"#branch_id\").val();
\t\t\t
\t\t\tif( \$(\"#emp_data_1\").is(':checked')){
\t\t\t\tvar emp_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar emp_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#department_data_1\").is(':checked')){
\t\t\t\tvar department_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar department_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#branch_data_1\").is(':checked')){
\t\t\t\tvar branch_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar branch_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\t//alert(emp_id);
\t\t\t//alert(user_id);
\t\t\t//return;
\t\t\t
\t\t\t\$.ajax({
\t\t\t\tdataType: \"json\",
\t\t\t\tmethod: \"POST\",
\t\t\t\turl: \"ajax/ajax_search_work_mailroom.php\",
\t\t\t\tdata: { \t
\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'status' :status,\t\t \t
\t\t\t\t\t\t'mr_type_work_id' : mr_type_work_id,\t
\t\t\t\t\t\t'start_date' : start_date,\t
\t\t\t\t\t\t'end_date' : end_date,\t\t
\t\t\t\t\t\t'user_id' : user_id,\t\t
\t\t\t\t\t\t'emp_id' : emp_id,\t\t\t
\t\t\t\t\t\t'department_id' : department_id,\t
\t\t\t\t\t\t'branch_id' : branch_id,\t\t
\t\t\t\t\t\t'emp_type' : emp_type,
\t\t\t\t\t\t'department_type' : department_type,
\t\t\t\t\t\t'branch_type' : branch_type
\t\t\t\t  },
\t\t\t\t beforeSend: function( xhr ) {
\t\t\t\t\t  \$('#bg_loader').show();
\t\t\t\t  }
\t\t\t  }).done(function( msg ) {
\t\t\t\t  if(msg.status == 200){
\t\t\t\t\t  \$('#bg_loader').hide();
\t\t\t\t\t  \$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t  \$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t  }else{
\t\t\t\t\t  alertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\t\t\t  return;
\t\t\t\t  }
\t\t\t  });
\t\t\t});
\t\t\t<!-- end Search -->
\t\t\t
\t\t\t
\t\t\$(\"#btn_clear\").click(function() {
\t\t\tlocation.reload();
\t\t\t
\t\t});
\t\t
\t\t\$('#department_id').select2({
\t\t\tplaceholder: \"เลือกหน่วยงาน\",
\t\t\ttheme: 'bootstrap4',
\t\t\tajax: {
\t\t\t\turl: \"ajax/ajax_getdata_department_select.php\",
\t\t\t\tdataType: \"json\",
\t\t\t\tdelay: 250,
\t\t\t\tprocessResults: function (data) {
\t\t\t\t\treturn {
\t\t\t\t\t\t results : data
\t\t\t\t\t};
\t\t\t\t},
\t\t\t\tcache: true
\t\t\t}
\t\t});
\t\t
\t\t
\t\t\$('#branch_id').select2({
\t\t\ttheme: 'bootstrap4',
\t\t\tplaceholder: \"เลือกสาขา\",
\t\t\tajax: {
\t\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\t\tdataType: \"json\",
\t\t\t\tdelay: 250,
\t\t\t\tprocessResults: function (data) {
\t\t\t\t\treturn {
\t\t\t\t\t\t results : data
\t\t\t\t\t};
\t\t\t\t},
\t\t\t\tcache: true
\t\t\t}
\t\t});
\t\t\t
\t\t\t
\t\t\$('#emp_id').select2({
\t\t\tplaceholder: \"เลือกพนักงาน\",
\t\t\ttheme: 'bootstrap4',
\t\t\t
\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\ttype:\"post\",
\t\t\t\t\tdata: function (params) {
\t\t\t\t\t  var query = {
\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t  }

\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t  return query;
\t\t\t\t\t},
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t};
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t});
\t\t\t
\t\t\t
\t\t\t
";
    }

    // line 372
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 373
        echo "function cancel_order(id){
\talertify.confirm('ยกเลิกการจัดส่ง', 'ท่านต้องการยกเลิกการจัดส่ง', 
\tfunction(){ 
\t\t\$.ajax({
\t\t   url : 'ajax/ajax_save_cancel_work.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : {
\t\t\t\t\t'id': id,
\t\t   },
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t
\t\t\t}
\t\t\t
\t\t}).done(function(data) {\t
\t\t\tif(data.status == 200){
\t\t\t\talertify.alert('แจ้งเตือน',data.message,function(){
\t\t\t\t\t\$(\"#btn_search\").click();
\t\t\t\t});
\t\t\t}
\t\t   
\t\t});
\t}, 
\tfunction(){ 
\t\talertify.error('Cancel')
\t});
\t
\t\t
\t
}
";
    }

    // line 405
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 406
        echo "<input id=\"csrf_token\" type=\"hidden\" value=\"";
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\">
\t<div  class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t<div class=\"\" style=\"text-align: center; color:#0074c0;margin-top:20px;\">
\t\t\t\t\t\t\t<label><h3><b>รายการเอกสารนำส่ง</b></h3></label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\" id=\"detail_sender_head\">
\t\t\t\t\t\t\t<h4>ค้นหารายการ</h4>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4\" style=\"\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"mr_type_work_id\">ประเภทการส่ง  : </label>
\t\t\t\t\t\t\t\t<select id= \"mr_type_work_id\" name=\"mr_type_work_id\" style=\"width:100%\"> 
\t\t\t\t\t\t\t\t  <option value=\"\">เลือกประเภทการส่ง</option>
\t\t\t\t\t\t\t\t  <option value=\"2\">ส่งที่สาขา</option>
\t\t\t\t\t\t\t\t  <option value=\"3\">ส่งที่สำนักงานใหญ่</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"barcode\">เลขที่เอกสาร :</label>
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"barcode\" placeholder=\"เลขที่เอกสาร\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"status_id\">สถานะเอกสาร  :</label>
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"status_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t<option value=\"\" > เลือกสถานะ</option>
\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งที่สาขา\">
\t\t\t\t\t\t\t\t\t";
        // line 435
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["status_data2"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 436
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_id", [], "any", false, false, false, 436), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_name", [], "any", false, false, false, 436), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 438
        echo "\t\t\t\t\t\t\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["status_data3"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 439
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_id", [], "any", false, false, false, 439), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_name", [], "any", false, false, false, 439), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 441
        echo "\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งสำนักงานใหญ่\">
\t\t\t\t\t\t\t\t\t";
        // line 443
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["status_data1"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 444
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_id", [], "any", false, false, false, 444), "html", null, true);
            echo "\" > ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_status_name", [], "any", false, false, false, 444), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 446
        echo "\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"barcode\">วันที่สร้างรายการ ::</label>
\t\t\t\t\t\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\" autocomplete=\"off\"/>
\t\t\t\t\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">ถึง</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\" autocomplete=\"off\"/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4\" style=\"\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t\tพนักงาน  :
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<label class=\"\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"emp_data\" id=\"emp_data_1\" value=\"1\" checked>ผู้ส่ง
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t<label class=\"\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"emp_data\" value=\"2\" >ผู้รับ
\t\t\t\t\t\t\t\t</label>:
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"emp_id\" style=\"width:100%;\">\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t<div class=\"col-md-12 text-left\" style=\"\">
\t\t\t\t\t\t\t\tหน่วยงาน
\t\t\t\t\t\t\t<label style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"department_data\" Id=\"department_data_1\" value=\"1\" checked>ผู้ส่ง
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"department_data\" value=\"2\">ผู้รับ
\t\t\t\t\t\t\t</label>:
\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department_id\" style=\"width:100%;\">\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"form-group\">\t
\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t\t<div class=\"col-m-12 text-left\" style=\"\">
\t\t\t\t\t\t\t\t\tสาขา
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<label style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"branch_data\" value=\"1\" id=\"branch_data_1\" checked>ผู้ส่ง
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t<label  style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"branch_data\" value=\"2\" >ผู้รับ
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t</div>
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 518
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 518), "html", null, true);
        echo "\">
\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-2\" style=\"padding-left:20px\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_search\"  >ค้นหา</button>

\t\t\t\t\t</div>\t\t
\t\t\t\t\t<div class=\"col-2\" style=\"padding-left:0px\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_clear\"  >ล้าง</button>

\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"detail_sender_head\">
\t\t\t\t <h4>รายละเอียดผู้รับ</h4>
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"panel panel-default\" style=\"margin-top:50px;\">
            <div class=\"panel-body\">
              <div class=\"table-responsive\" style=\"overflow-x:auto;\">
                <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                  <thead>
                    <tr>
\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t<th>สถานะ</th>
\t\t\t\t\t\t<th>เลขที่</th>
\t\t\t\t\t\t<th>วันที่</th>
\t\t\t\t\t\t<th>ผู้รับ</th>
\t\t\t\t\t\t<th>หน่วยงาน</th>
\t\t\t\t\t\t<th>ผู้รับแทน</th>
\t\t\t\t\t\t<th>หน่วยงานผู้รับแทน</th>
\t\t\t\t\t\t<th>ผู้ส่ง</th>
\t\t\t\t\t\t<th>ที่อยู่ผู้ส่ง</th>
\t\t\t\t\t\t<th>สถานะ</th>
\t\t\t\t\t\t<th>ชื่อเอกสาร</th>
\t\t\t\t\t\t<th>ประเภทการส่ง</th>
\t\t\t\t\t\t<th>หมายเหตุ</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
\t\t\t
\t\t\t
\t\t\t\t
\t\t
\t</div>\t
\t<div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
";
    }

    // line 586
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 588
        if ((($context["debug"] ?? null) != "")) {
            // line 589
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 591
        echo "
";
    }

    public function getTemplateName()
    {
        return "branch/search_work.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  735 => 591,  729 => 589,  727 => 588,  720 => 586,  650 => 518,  576 => 446,  565 => 444,  561 => 443,  557 => 441,  546 => 439,  541 => 438,  530 => 436,  526 => 435,  493 => 406,  489 => 405,  455 => 373,  451 => 372,  205 => 128,  202 => 127,  198 => 126,  86 => 17,  82 => 16,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp_branch.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_2 %} active {% endblock %}

{% block scriptImport %}

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}
{% block styleReady %}
\t#btn_search:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t
\t#btn_search{
\t\tborder-color: #0074c0;
\t\tcolor: #FFFFFF;
\t\tbackground-color: #0074c0;
\t}
 
\t#btn_clear:hover{
\t\tcolor: #FFFFFF;
\t\tbackground-color: #055d97;
\t
\t}
\t
\t#detail_sender_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t}
\t
\t#detail_sender_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 20px;
\t}
\t
\t#detail_receiver_head h4{
\t\ttext-align:left;
\t\tcolor: #006cb7;
\t\tborder-bottom: 3px solid #006cb7;
\t\tdisplay: inline;
\t\t
\t\t
\t}
\t
\t#detail_receiver_head {
\t\tborder-bottom: 3px solid #eee;
\t\tmargin-bottom: 20px;
\t\tmargin-top: 40px;
\t\t
\t}\t
 
\t.input-group-addon{
\t\tbackground-color: #FFFFFF; 
\t\tborder: 1px solid #055d97;
\t
\t}

\t.pt-primary {
\t\tcolor: #03a9fa;
\t}

\t.pt-cyan {
\t\tcolor: #00bcd4;
\t}

\t.pt-indigo {
\t\tcolor: #5677fc;
\t}

\t.pt-teal {
\t\tcolor: #009688;
\t}

\t.pt-cancel {
\t\tcolor: #9e9e9e;
\t}

\t.pt-lemon {
\t\tcolor: #8bc34a;
\t}
\t.pt-success {
\t\tcolor: #259b24;
\t}
\t
\t.pt-dark {
\t\tcolor: #212121;
\t}

\t.pt-amber {
\t\tcolor: #ffc107;
\t}

\t#tb_work_order tr th,
\t#tb_work_order tr td {
\t\tfont-size: 14px;
\t}
\t
#loader{
\t  width:100px;
\t  height:100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t\tdisplay:none;
\t}
 
{% endblock %}
{% block domReady %}

{{alertt}}

\t\t\$('.input-daterange').datepicker({
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\ttodayHighlight: true
\t\t});\t
\t\t
\t\t
\t\tvar table = \$(\"#tb_work_order\").DataTable();
\t\tvar mr_type_work_id \t\t= \$(\"#mr_type_work_id\").val();
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status\t\t \t\t= \$(\"#status_id\").val();
\t\t\tvar start_date \t\t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t\t= \$(\"#end_date\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar department_id \t\t= \$(\"#department_id\").val();
\t\t\tvar branch_id \t\t\t= \$(\"#branch_id\").val();
\t\t\t
\t\t\tif( \$(\"#emp_data_1\").is(':checked')){
\t\t\t\tvar emp_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar emp_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#department_data_1\").is(':checked')){
\t\t\t\tvar department_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar department_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#branch_data_1\").is(':checked')){
\t\t\t\tvar branch_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar branch_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\t//alert(emp_id);
\t\t\t//alert(user_id);
\t\t\t//return;
\t\t\t
\t\t\t
\t\t\t\ttable.destroy();
\t\t\t\ttable = \$(\"#tb_work_order\").DataTable({
\t\t\t\t\t\"ajax\": {
\t\t\t\t\t\t\"url\": \"ajax/ajax_search_work_mailroom.php\",
\t\t\t\t\t\t\"type\": \"POST\",
\t\t\t\t\t\t\"data\": {
\t\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'status' :status,\t\t \t
\t\t\t\t\t\t'mr_type_work_id' : mr_type_work_id,\t
\t\t\t\t\t\t'start_date' : start_date,\t
\t\t\t\t\t\t'end_date' : end_date,\t\t
\t\t\t\t\t\t'user_id' : user_id,\t\t
\t\t\t\t\t\t'emp_id' : emp_id,\t\t\t
\t\t\t\t\t\t'department_id' : department_id,\t
\t\t\t\t\t\t'branch_id' : branch_id,\t\t
\t\t\t\t\t\t'emp_type' : emp_type,
\t\t\t\t\t\t'department_type' : department_type,
\t\t\t\t\t\t'branch_type' : branch_type
\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t
\t\t\t\t\t'columns': [
\t\t\t\t\t
\t\t\t\t\t\t{ 'data':'action' },
\t\t\t\t\t\t{ 'data':'status' },
\t\t\t\t\t\t{ 'data':'barcode_show' },
\t\t\t\t\t\t{ 'data':'time_test'},
\t\t\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t\t\t{ 'data':'add_receiver' },
\t\t\t\t\t\t{ 'data':'name_receive2' },
\t\t\t\t\t\t{ 'data':'add_receiver2' },
\t\t\t\t\t\t{ 'data':'name_send' },
\t\t\t\t\t\t{ 'data':'add_sender' },
\t\t\t\t\t\t{ 'data':'mr_topic' },
\t\t\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t\t\t{ 'data':'mr_work_remark' }
\t\t\t\t\t],
\t\t\t\t\t/* \"fnRowCallback\": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
\t\t\t\t\t\tif (aData['scg_status_name'] == \"Newly\") {
\t\t\t\t\t\t\t\t\$(nRow).addClass('hilight');
\t\t\t\t\t\t\t}
\t\t\t\t\t\telse{
\t\t\t\t\t\t\t\$(nRow).removeClass('hilight');
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}, */
\t\t\t\t\t'scrollCollapse': true
\t\t\t\t});
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t<!-- Search -->
\t\t\t\$(\"#btn_search\").click(function(){
\t\t\tvar mr_type_work_id \t= \$(\"#mr_type_work_id\").val();
\t\t\tvar barcode \t\t\t= \$(\"#barcode\").val();
\t\t\tvar status\t\t \t\t= \$(\"#status_id\").val();
\t\t\tvar start_date \t\t\t= \$(\"#start_date\").val();
\t\t\tvar end_date \t\t\t= \$(\"#end_date\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar department_id \t\t= \$(\"#department_id\").val();
\t\t\tvar branch_id \t\t\t= \$(\"#branch_id\").val();
\t\t\t
\t\t\tif( \$(\"#emp_data_1\").is(':checked')){
\t\t\t\tvar emp_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar emp_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#department_data_1\").is(':checked')){
\t\t\t\tvar department_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar department_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\tif( \$(\"#branch_data_1\").is(':checked')){
\t\t\t\tvar branch_type = \"sender\";
\t\t\t}else{
\t\t\t\tvar branch_type = \"receiver\";
\t\t\t}
\t\t\t
\t\t\t//alert(emp_id);
\t\t\t//alert(user_id);
\t\t\t//return;
\t\t\t
\t\t\t\$.ajax({
\t\t\t\tdataType: \"json\",
\t\t\t\tmethod: \"POST\",
\t\t\t\turl: \"ajax/ajax_search_work_mailroom.php\",
\t\t\t\tdata: { \t
\t\t\t\t\t'barcode': barcode,
\t\t\t\t\t\t'status' :status,\t\t \t
\t\t\t\t\t\t'mr_type_work_id' : mr_type_work_id,\t
\t\t\t\t\t\t'start_date' : start_date,\t
\t\t\t\t\t\t'end_date' : end_date,\t\t
\t\t\t\t\t\t'user_id' : user_id,\t\t
\t\t\t\t\t\t'emp_id' : emp_id,\t\t\t
\t\t\t\t\t\t'department_id' : department_id,\t
\t\t\t\t\t\t'branch_id' : branch_id,\t\t
\t\t\t\t\t\t'emp_type' : emp_type,
\t\t\t\t\t\t'department_type' : department_type,
\t\t\t\t\t\t'branch_type' : branch_type
\t\t\t\t  },
\t\t\t\t beforeSend: function( xhr ) {
\t\t\t\t\t  \$('#bg_loader').show();
\t\t\t\t  }
\t\t\t  }).done(function( msg ) {
\t\t\t\t  if(msg.status == 200){
\t\t\t\t\t  \$('#bg_loader').hide();
\t\t\t\t\t  \$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\t  \$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t\t  }else{
\t\t\t\t\t  alertify.alert('เกิดข้อผิดพลาด',msg.message);
\t\t\t\t\t  return;
\t\t\t\t  }
\t\t\t  });
\t\t\t});
\t\t\t<!-- end Search -->
\t\t\t
\t\t\t
\t\t\$(\"#btn_clear\").click(function() {
\t\t\tlocation.reload();
\t\t\t
\t\t});
\t\t
\t\t\$('#department_id').select2({
\t\t\tplaceholder: \"เลือกหน่วยงาน\",
\t\t\ttheme: 'bootstrap4',
\t\t\tajax: {
\t\t\t\turl: \"ajax/ajax_getdata_department_select.php\",
\t\t\t\tdataType: \"json\",
\t\t\t\tdelay: 250,
\t\t\t\tprocessResults: function (data) {
\t\t\t\t\treturn {
\t\t\t\t\t\t results : data
\t\t\t\t\t};
\t\t\t\t},
\t\t\t\tcache: true
\t\t\t}
\t\t});
\t\t
\t\t
\t\t\$('#branch_id').select2({
\t\t\ttheme: 'bootstrap4',
\t\t\tplaceholder: \"เลือกสาขา\",
\t\t\tajax: {
\t\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\t\tdataType: \"json\",
\t\t\t\tdelay: 250,
\t\t\t\tprocessResults: function (data) {
\t\t\t\t\treturn {
\t\t\t\t\t\t results : data
\t\t\t\t\t};
\t\t\t\t},
\t\t\t\tcache: true
\t\t\t}
\t\t});
\t\t\t
\t\t\t
\t\t\$('#emp_id').select2({
\t\t\tplaceholder: \"เลือกพนักงาน\",
\t\t\ttheme: 'bootstrap4',
\t\t\t
\t\t\tajax: {
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\ttype:\"post\",
\t\t\t\t\tdata: function (params) {
\t\t\t\t\t  var query = {
\t\t\t\t\t\tsearch: params.term,
\t\t\t\t\t\tcsrf_token: \$('#csrf_token').val(),
\t\t\t\t\t\tq: params.term,
\t\t\t\t\t\ttype: 'public'
\t\t\t\t\t  }

\t\t\t\t\t  // Query parameters will be ?search=[term]&type=public
\t\t\t\t\t  return query;
\t\t\t\t\t},
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\t\$('#csrf_token').val(data.token);
\t\t\t\t\t\tif(data.status == 401){
\t\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t\t}else if(data.status == 200){
\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t results : data.data
\t\t\t\t\t\t\t};
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t});
\t\t\t
\t\t\t
\t\t\t
{% endblock %}


{% block javaScript %}
function cancel_order(id){
\talertify.confirm('ยกเลิกการจัดส่ง', 'ท่านต้องการยกเลิกการจัดส่ง', 
\tfunction(){ 
\t\t\$.ajax({
\t\t   url : 'ajax/ajax_save_cancel_work.php',
\t\t   dataType : 'json',
\t\t   type : 'POST',
\t\t   data : {
\t\t\t\t\t'id': id,
\t\t   },
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\t
\t\t\t}
\t\t\t
\t\t}).done(function(data) {\t
\t\t\tif(data.status == 200){
\t\t\t\talertify.alert('แจ้งเตือน',data.message,function(){
\t\t\t\t\t\$(\"#btn_search\").click();
\t\t\t\t});
\t\t\t}
\t\t   
\t\t});
\t}, 
\tfunction(){ 
\t\talertify.error('Cancel')
\t});
\t
\t\t
\t
}
{% endblock %}

{% block Content %}
<input id=\"csrf_token\" type=\"hidden\" value=\"{{csrf}}\">
\t<div  class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t<div class=\"\" style=\"text-align: center; color:#0074c0;margin-top:20px;\">
\t\t\t\t\t\t\t<label><h3><b>รายการเอกสารนำส่ง</b></h3></label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\" id=\"detail_sender_head\">
\t\t\t\t\t\t\t<h4>ค้นหารายการ</h4>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4\" style=\"\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"mr_type_work_id\">ประเภทการส่ง  : </label>
\t\t\t\t\t\t\t\t<select id= \"mr_type_work_id\" name=\"mr_type_work_id\" style=\"width:100%\"> 
\t\t\t\t\t\t\t\t  <option value=\"\">เลือกประเภทการส่ง</option>
\t\t\t\t\t\t\t\t  <option value=\"2\">ส่งที่สาขา</option>
\t\t\t\t\t\t\t\t  <option value=\"3\">ส่งที่สำนักงานใหญ่</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"barcode\">เลขที่เอกสาร :</label>
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"barcode\" placeholder=\"เลขที่เอกสาร\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"status_id\">สถานะเอกสาร  :</label>
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"status_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t<option value=\"\" > เลือกสถานะ</option>
\t\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งที่สาขา\">
\t\t\t\t\t\t\t\t\t{% for s in status_data2 %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_status_id }}\" > {{ s.mr_status_name }}</option>
\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t{% for s in status_data3 %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_status_id }}\" > {{ s.mr_status_name }}</option>
\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t<optgroup label=\"รับส่งสำนักงานใหญ่\">
\t\t\t\t\t\t\t\t\t{% for s in status_data1 %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_status_id }}\" > {{ s.mr_status_name }}</option>
\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"barcode\">วันที่สร้างรายการ ::</label>
\t\t\t\t\t\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\" autocomplete=\"off\"/>
\t\t\t\t\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">ถึง</label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\" autocomplete=\"off\"/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4\" style=\"\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t\tพนักงาน  :
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<label class=\"\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"emp_data\" id=\"emp_data_1\" value=\"1\" checked>ผู้ส่ง
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t<label class=\"\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"emp_data\" value=\"2\" >ผู้รับ
\t\t\t\t\t\t\t\t</label>:
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"emp_id\" style=\"width:100%;\">\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t<div class=\"col-md-12 text-left\" style=\"\">
\t\t\t\t\t\t\t\tหน่วยงาน
\t\t\t\t\t\t\t<label style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"department_data\" Id=\"department_data_1\" value=\"1\" checked>ผู้ส่ง
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"department_data\" value=\"2\">ผู้รับ
\t\t\t\t\t\t\t</label>:
\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department_id\" style=\"width:100%;\">\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"form-group\">\t
\t\t\t\t\t\t\t<div class=\"col-md-12\" style=\"\">
\t\t\t\t\t\t\t\t<div class=\"col-m-12 text-left\" style=\"\">
\t\t\t\t\t\t\t\t\tสาขา
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<label style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"branch_data\" value=\"1\" id=\"branch_data_1\" checked>ผู้ส่ง
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t<label  style=\"padding-left:0px\">
\t\t\t\t\t\t\t\t\t  <input type=\"radio\" aria-label=\"Radio button for following text input\" name=\"branch_data\" value=\"2\" >ผู้รับ
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t</div>
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-2\" style=\"padding-left:20px\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_search\"  >ค้นหา</button>

\t\t\t\t\t</div>\t\t
\t\t\t\t\t<div class=\"col-2\" style=\"padding-left:0px\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_clear\"  >ล้าง</button>

\t\t\t\t\t</div>\t
\t\t\t\t\t\t
\t\t\t\t</div>\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\" id=\"detail_sender_head\">
\t\t\t\t <h4>รายละเอียดผู้รับ</h4>
\t\t\t</div>\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"panel panel-default\" style=\"margin-top:50px;\">
            <div class=\"panel-body\">
              <div class=\"table-responsive\" style=\"overflow-x:auto;\">
                <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
                  <thead>
                    <tr>
\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t<th>สถานะ</th>
\t\t\t\t\t\t<th>เลขที่</th>
\t\t\t\t\t\t<th>วันที่</th>
\t\t\t\t\t\t<th>ผู้รับ</th>
\t\t\t\t\t\t<th>หน่วยงาน</th>
\t\t\t\t\t\t<th>ผู้รับแทน</th>
\t\t\t\t\t\t<th>หน่วยงานผู้รับแทน</th>
\t\t\t\t\t\t<th>ผู้ส่ง</th>
\t\t\t\t\t\t<th>ที่อยู่ผู้ส่ง</th>
\t\t\t\t\t\t<th>สถานะ</th>
\t\t\t\t\t\t<th>ชื่อเอกสาร</th>
\t\t\t\t\t\t<th>ประเภทการส่ง</th>
\t\t\t\t\t\t<th>หมายเหตุ</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
\t\t\t
\t\t\t
\t\t\t\t
\t\t
\t</div>\t
\t<div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "branch/search_work.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\branch\\search_work.tpl");
    }
}

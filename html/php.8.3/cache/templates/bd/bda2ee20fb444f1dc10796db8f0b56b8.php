<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/import_excel.tpl */
class __TwigTemplate_9f2402f4e26fd8aa61024f8efa32e1a3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/import_excel.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>


";
    }

    // line 14
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "\t.widthText {
\t\twidth: 300px;
\t}

\t
\t#lst_warning {
\t\tcolor: red;
\t}
\t.loading {
\t\tposition: fixed;
\t\ttop: 40%;
\t\tleft: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\t-ms-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1000;
\t}
\t
\t.img_loading {
\t\t  width: 350px;
\t\t  height: auto;
\t\t  
\t}
";
    }

    // line 40
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "//var url = \"https://www.pivot-services.com/mailroom_tmb/employee/\";
var url = \"\";



\$('#name_emp').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\ttheme: 'bootstrap4',
\t\t\t\tajax: {
\t\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t},
\t\t\t\tdropdownParent: \$('#editCon')

\t\t}).on('select2:select', function(e) {
\t\t\t//console.log(e.params.data);
\t\t\tsetForm(e.params.data);
\t\t});

\t\tvar table = \$('#tb_work_order').DataTable({ 
\t\t\t\"responsive\": true,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'edit' },
\t\t\t\t{ 'data':'mr_emp_name'},
\t\t\t\t{ 'data':'mr_emp_lastname' },
\t\t\t\t{ 'data':'mr_emp_code' },
\t\t\t\t{ 'data':'mr_emp_email' },
\t\t\t\t{ 'data':'mr_date_import' },
\t\t\t\t{ 'data':'update_date' },
\t\t\t\t{ 'data':'position_name' },
\t\t\t\t{ 'data':'department_name' },
\t\t\t\t{ 'data':'floor_name' },
\t\t\t\t{ 'data':'branchname' }
\t\t\t]
\t\t});

\t\tvar table = \$('#tb_data_contact').DataTable({ 
\t\t\t\"responsive\": true,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'edit' },
\t\t\t\t{ 'data':'shows' },
\t\t\t\t{ 'data':'department_code'},
\t\t\t\t{ 'data':'department_name' },
\t\t\t\t{ 'data':'emp_code' },
\t\t\t\t{ 'data':'mr_contact_name' },
\t\t\t\t{ 'data':'emp_tel' },
\t\t\t\t{ 'data':'remark' },
\t\t\t\t{ 'data':'con_sysdate' },
\t\t\t]
\t\t});
\t\tvar table = \$('#tb_cost_center').DataTable({ 
\t\t\t\"responsive\": true,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'edit' },
\t\t\t\t{ 'data':'mr_emp_name' },
\t\t\t\t{ 'data':'mr_emp_lastname' },
\t\t\t\t{ 'data':'mr_emp_code' },
\t\t\t\t{ 'data':'update_date' },
\t\t\t\t{ 'data':'mr_position_name' },
\t\t\t\t{ 'data':'mr_department_name' },
\t\t\t\t{ 'data':'mr_cost_code' },
\t\t\t]
\t\t});

\t
\t\$('#btn_upload').on('click',function() {
\t\tvar data = \$('#txt_upload').prop(\"files\");
\t\tvar form_val = \$('#txt_upload').prop(\"files\")[0];
\t
\t\t// console.log(data);
\t\t
\t\t\t\$('#btn_upload').text('Uploading...').attr('disabled', 'disabled');
\t\t\tvar form_data = new FormData();
\t\t\tform_data.append('file', form_val);
\t\t\t//if(data[0]['type'] == \"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" || data[0]['type'] == \"application/vnd.ms-excel\"){
\t\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax_readFile_Emp_csv.php',
               \t\tcache: false,
                \tcontentType: false,
                \tprocessData: false,
                \tdata: form_data,                         
\t\t\t\t\ttype: 'post',
\t\t\t\t\tdataType: 'html',
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t
\t\t\t\t\t\tif( res <= 0 ) {
\t\t\t\t\t\t\talert('ไม่มีข้อมูลนำเข้า');
\t\t\t\t\t\t\tclearForm();
\t\t\t\t\t\t\t\$('#btn_upload').text('Upload').removeAttr('disabled');
\t\t\t\t\t\t\tgetData();
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\$('#btn_upload').text('Upload').removeAttr('disabled');
\t\t\t\t\t\t\tclearForm();
\t\t\t\t\t\t\talert('การอัพโหลดรายชื่อพนักงานจำนวน '+ res +' รายชื่อ');
\t\t\t\t\t\t\tgetData();
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t});
\t\t\t
\t\t//}
\t});

\t\$('#btn_clear').click(function() {
        window.history.back();
\t}); 
\t
\t\$('#branch').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'branch'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  }\t,
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});

\t\$('#edit_branch').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'branch'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  }\t,
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});
\t\$('#department').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาแผนก/หน่วยงาน\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'department'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});
\t\$('#position').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาตำแหน่ง\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'position'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});
\t\$('#floor').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาชั้น\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'floor'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});

\t\$('#emptype').select2({
\t\ttheme: 'bootstrap4',
\t\twidth: '100%' 
\t});
\t\$('#group').select2({ 
\t\ttheme: 'bootstrap4',
\t\ttheme: 'bootstrap4',
\t\twidth: '100%' 
\t});

\t\$('#emp_hub').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'hub'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});



\t\t\t\t
\$( \"#btnCon_save\" ).click(function() {
\t\t\t\t\t\t\t\t\t\t
\tvar emp_hub \t\t\t= \$('#emp_hub').val();
\tvar con_type \t\t\t= \$('#con_type').val();
\tvar con_remark \t\t\t= \$('#con_remark').val();
\tvar con_floor \t\t\t= \$('#con_floor').val();
\tvar con_department_name = \$('#con_department_name').val();
\tvar mr_emp_tel \t\t\t= \$('#con_emp_tel').val();
\tvar con_emp_name \t\t= \$('#con_emp_name').val();
\tvar mr_emp_code \t\t= \$('#mr_emp_code').val();
\tvar mr_emp_id \t\t\t= \$('#name_emp').val();
\tvar department_code \t= \$('#department_code').val();
\tvar mr_contact_id \t\t= \$('#mr_contact_id').val();
\tvar type_add \t\t\t= \$('#type_add').val();
\t
\tvar dataselect2 = \$('#name_emp').select2('data');
\tcon_emp_name\t= dataselect2[0].text;
\t
\t
\t\$( \".myErrorClass\" ).removeClass( \"myErrorClass\" );
\t
\tif(mr_emp_id == ''){
\t\t\$( \"#name_emp\" ).addClass( \"myErrorClass\" );
\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
\t\treturn;
\t}if(mr_emp_code == ''){
\t\t\$( \"#mr_emp_code\" ).addClass( \"myErrorClass\" );
\t\talertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
\t\treturn;
\t}if(mr_emp_tel == ''){
\t\t\$( \"#mr_emp_tel\" ).addClass( \"myErrorClass\" );
\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
\t\treturn;
\t}
\t
alertify.confirm(\"ตรวจสอบข้อมูล\", \"กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก\",
function(){
\$.ajax({
\t\t  dataType: \"json\",
\t\t  method: \"POST\",
\t\t  url: \"ajax/ajax_save_data_contact.php\",
\t\t  data: { 
\t\t\t    emp_hub \t\t\t \t \t: con_type \t\t\t \t,
\t\t\t\tcon_type \t\t\t \t \t: con_type \t\t\t \t,
\t\t\t\tdepartment_code \t\t \t: department_code \t\t\t \t,
\t\t\t\tcon_emp_name \t\t\t \t: con_emp_name \t\t\t \t,
\t\t\t\tcon_remark \t\t\t \t \t: con_remark \t\t\t \t,
\t\t\t\tcon_floor \t\t\t \t \t: con_floor \t\t\t \t,
\t\t\t\tcon_department_name \t \t: con_department_name \t,
\t\t\t\ttype_add \t \t\t\t\t: type_add \t,
\t\t\t\tmr_emp_tel \t \t\t\t\t: mr_emp_tel \t,
\t\t\t\tmr_emp_code  \t\t\t\t: mr_emp_code ,
\t\t\t\tmr_emp_id \t \t\t\t\t: mr_emp_id \t,
\t\t\t\tmr_contact_id\t\t\t\t: mr_contact_id
\t\t  }
\t\t}).done(function(data) {
\t\t\t\$('#csrf_token').val(data.token);

\t\t\talertify.alert(data['st'],data['msg'],
\t\t\tfunction(){
\t\t\t\tgetData2();
\t\t\t\t\$('#editCon').modal('hide');
\t\t\t});
\t\t});
},function(){
\talertify.error('Cancel');
});


//alert( \"Handler for .click() called.\" );
//is-invalid
});



getData();
";
    }

    // line 402
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 403
        echo "
function edit_empdata(id,email,name,lname,mr_branch_id,branchname){
\t\$('#div_edit_email').slideDown('slow');
\t\$('#edit_email').val(email)
\t\$('#edit_name').val(name)
\t\$('#edit_lname').val(lname)
\t\$('#emp_id').val(id)
\t
\tvar newOption = new Option(branchname, mr_branch_id, false, false);
\t\$('#edit_branch').append(newOption).trigger('change');
\t\$('#edit_branch').append(newOption).trigger('change');
\t\$('#edit_branch').val(mr_branch_id); // Select the option with a value of '1'
\t\$('#edit_branch').trigger('change'); // Notify any JS components that the value changed

\t//\$(\"#edit_branch\").select2(mr_branch_id, branchname);
 console.log(newOption);
 console.log(mr_branch_id);
 console.log(branchname);

}


function getData2(){\t
\t\$.ajax({
\tmethod: \"POST\",
\tdataType: \"json\",
\turl: \"ajax/ajax_get_contact.php\",
\tbeforeSend: function( xhr ) {
\t\t\$('.loading').show();
\t},
\terror: function (xhr, ajaxOptions, thrownError) {
\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
\t\tlocation.reload();
\t}
\t})
\t.done(function( msg ) {
\t\t\$('.loading').hide();
\t\t\$('#tb_data_contact').DataTable().clear().draw();
\t\t\$('#tb_data_contact').DataTable().rows.add(msg.data).draw(); 
\t\t
\t});
}

function getCost_center(){\t
\t\$.ajax({
\tmethod: \"POST\",
\tdataType: \"json\",
\turl: \"ajax/ajax_get_Cost_center.php\",
\tbeforeSend: function( xhr ) {
\t\t\$('.loading').show();
\t\t\$('#msg-alert').html('');
\t},
\terror: function (xhr, ajaxOptions, thrownError) {
\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
\t\t//location.reload();
\t}
\t})
\t.done(function( msg ) {
\t\t\$('.loading').hide();
\t\t\$('#tb_cost_center').DataTable().clear().draw();
\t\t\$('#tb_cost_center').DataTable().rows.add(msg.data).draw(); 
\t\t
\t});
}


function getData(){
\t\$.ajax({
\tmethod: \"POST\",
\tdataType: \"json\",
\turl: \"ajax/ajax_get_emp.php\",
\tbeforeSend: function( xhr ) {
\t\t\$('.loading').show();
\t},
\terror: function (xhr, ajaxOptions, thrownError) {
\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
\t\t//location.reload();
\t}
\t})
\t.done(function( msg ) {
\t\t\$('.loading').hide();
\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw(); 
\t\t
\t});
};


\tvar clearForm = function() {
\t\t\$('#txt_upload').val(\"\");
\t}
\tfunction chang_emptype(){
\t\t\$('#group').val(\"\");
\t\tvar emptype = \$('#emptype').val();
\t\tif(emptype == 2){
\t\t\t\$('#div_floor').hide();
\t\t}else{
\t\t\t\$('#div_floor').show();
\t\t}
\t\tchang_group();
\t}

\tfunction chang_group(){
\t\tvar emptype = \$('#emptype').val();
\t\tvar group = \$('#group').val();
\t\tif(group == \"pivot\"){
\t\t\t\$('.not_pivot').hide();
\t\t\tif(emptype == 2){
\t\t\t\t\$('#div_hub').show();
\t\t\t}else{
\t\t\t\t\$('#div_hub').hide();
\t\t\t}
\t\t}else{
\t\t\t\$('.not_pivot').show();
\t\t\t\$('#div_hub').hide();
\t\t}
\t}



\tfunction resign_empdata(code){
\t\talertify.confirm('ลบพนักงานออกจากระบบ','ท่านต้องการลบนพนักงาน '+code+' ออกจากระบบ หรือไม่', 
\t\t\tfunction(){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_save_employee.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: { 
\t\t\t\t\t\tmr_emp_code \t\t: code \t\t, 
\t\t\t\t\t\tpage \t\t\t\t: 'resign' \t
\t\t\t\t\t},
\t\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t},error: function (error) {
\t\t\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\t\t\tlocation.reload();
\t\t
\t\t\t\t\t},
\t\t\t\t  })
\t\t\t\t\t.done(function( data ) {
\t\t\t\t\t\t//getData()
\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\$('#btn_save').text('Save').removeAttr('disabled');
\t\t\t\t\t\tif(data['error'] == 'success'){
\t\t\t\t\t\t\t\$('#alert_success').show(); 
\t\t\t\t\t\t\t\$('#alert_error').hide(); 
\t\t\t\t\t\t\tgetData();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$('#alert_success').hide(); 
\t\t\t\t\t\t\t\$('#alert_error').show(); 
\t\t\t\t\t\t\t\$('#text_error').text(data['msg']); 
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t});\t
\t\t\t},function() {
\t\t}).set('labels', {ok:'ลบพนักงาน', cancel:'ไม่'});
\t}
\tfunction update_email(){
\t\tvar edit_email \t= \$('#edit_email').val();
\t\tvar edit_name \t= \$('#edit_name').val();
\t\tvar edit_lname \t= \$('#edit_lname').val();
\t\tvar edit_branch \t= \$('#edit_branch').val();
\t\tvar emp_id\t \t= \$('#emp_id').val();
\t\tif(edit_email == ''){
\t\t\talert('กรุณาระบบุ E-mail');
\t\t\treturn 
\t\t}
\t\t
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_save_employee.php\",
\t\t\tdataType: \"json\",
\t\t\tdata: { 
\t\t\t\temp_id\t \t\t: emp_id \t, 
\t\t\t\tedit_email \t\t: edit_email \t, 
\t\t\t\tedit_name \t\t: edit_name \t, 
\t\t\t\tedit_lname \t\t: edit_lname \t, 
\t\t\t\tedit_branch \t: edit_branch \t, 
\t\t\t\tpage \t\t\t: 'edit' \t
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t},error: function (error) {
\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\tlocation.reload();

\t\t\t},
\t\t  })
\t\t\t.done(function( data ) {
\t\t\t\t\$('.loading').hide();
\t\t\t\tif(data['error'] == 'success'){
\t\t\t\t\talert('บันทึกสำเร็จ');
\t\t\t\t\tlocation.reload();
\t\t\t\t}else{
\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\tlocation.reload();
\t\t\t\t}
\t\t\t\t
\t\t\t});


\t}
\tfunction save_emp(){
\t\tvar emp_hub \t\t\t= \$('#emp_hub').val();
\t\tvar emptype \t= \$('#emptype').val();
\t\tvar code \t\t= \$('#code').val();
\t\tvar name \t\t= \$('#name').val();
\t\tvar lname \t\t= \$('#lname').val();
\t\tvar email \t\t= \$('#email').val();
\t\tvar branch \t\t= \$('#branch').val();
\t\tvar department \t= \$('#department').val();
\t\tvar position \t= \$('#position').val();
\t\tvar tel \t\t= \$('#tel').val();
\t\tvar floor \t\t= \$('#floor').val();
\t\tvar group \t\t= \$('#group').val();




\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_save_employee.php\",
\t\t\tdataType: \"json\",
\t\t\tdata: { 
\t\t\t\temp_hub \t: emp_hub \t, 
\t\t\t\temptype \t: emptype \t, 
\t\t\t\tgroup \t\t: group \t, 
\t\t\t\tcode \t\t: code \t\t, 
\t\t\t\tname \t\t: name \t\t, 
\t\t\t\tfloor \t\t: floor \t\t, 
\t\t\t\tlname \t\t: lname \t\t, 
\t\t\t\temail \t\t: email \t\t, 
\t\t\t\tbranch \t\t: branch \t\t, 
\t\t\t\tdepartment \t: department \t, 
\t\t\t\ttel \t\t: tel \t, 
\t\t\t\tposition \t: position \t
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#btn_save').text('saving...').attr('disabled', 'disabled');
\t\t\t\t\$('.loading').show();
\t\t\t},error: function (error) {
\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\t//location.reload();

\t\t\t},
\t\t  })
\t\t\t.done(function( data ) {
\t\t\t\t//getData()
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#btn_save').text('Save').removeAttr('disabled');
\t\t\t\tif(data['error'] == 'success'){
\t\t\t\t\t\$('#alert_success').show(); 
\t\t\t\t\t\$('#alert_success').text(data['msg']); 
\t\t\t\t\t\$('#alert_error').hide(); 
\t\t\t\t\t//location.reload();
\t\t\t\t}else{
\t\t\t\t\t\$('#alert_success').hide(); 
\t\t\t\t\t\$('#alert_error').show(); 
\t\t\t\t\t\$('#text_error').text(data['msg']); 
\t\t\t\t}
\t\t\t\t
\t\t\t});


\t}

\t\t\t
function load_modal(mr_contact_id,type) {\t
window.scrollTo(0, 0);
 \$('#editCon').modal({
\t backdrop: false,
\t keyboard : true
\t});

\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_data_contact.php\",
\t  data: { \t
\t\t\t\tmr_contact_id : mr_contact_id,
\t\t\t\tpage : 'getdata'
\t  }
\t}).done(function(data) {
\t\t//alert('55555');
\t\tif(type!= \"add\"){
\t\t\t\$('#mr_contact_id').val(data['mr_contact_id']);
\t\t\t
\t\t}
\t\t
\t\t\$('#type_add').val(type);
\t\t\$('#mr_emp_code').val(data['emp_code']);
\t\t\$('#con_emp_tel').val(data['emp_tel']);
\t\t\$('#con_type').val(data['type']);

\t\t\$('#con_department_name').val(data['department_name']);
\t\t\$('#con_floor').val(data['floor']);
\t\t\$('#con_remark').val(data['remark']);
\t\t\$('#department_code').val(data['department_code']);
\t});
\t
}



function setForm(data) {
\tvar emp_id = parseInt(data.id);
\tvar fullname = data.text;
\t\$.ajax({
\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\turl: 'ajax/ajax_autocompress_name.php',
\t\ttype: 'POST',
\t\tdata: {
\t\t\tname_receiver_select: emp_id
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res) {
\t\t\tif(res.status == 200) {
\t\t\t\tif(res.data != false) {
\t\t\t\t\t\$(\"#con_emp_tel\").val(res['data']['mr_emp_tel']);
\t\t\t\t\t\$(\"#mr_emp_code\").val(res['data']['mr_emp_code']);
\t\t\t\t}
\t\t\t}
\t\t\t
\t\t}
\t})
}

function upload_cost_center() {
\t\$('#msg-alert').html('');
\t\tvar data = \$('#file_upload_cost').prop(\"files\");
\t\tvar form_val = \$('#file_upload_cost').prop(\"files\")[0];
\t
\t\t// console.log(data);
\t\t
\t\t\t\$('#btn_upload_cost').text('Uploading...').attr('disabled', 'disabled');
\t\t\tvar form_data = new FormData();
\t\t\tvar token = \$('#token').val();
\t\t\tform_data.append('file', form_val);
\t\t\tform_data.append('csrf_token', token);
\t\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_readFile_Cost_center.php',
               \t\tcache: false,
                \tcontentType: false,
                \tprocessData: false,
                \tdata: form_data,                         
\t\t\t\t\ttype: 'post',
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\t\t\$('#token').val(res['token']);
\t\t\t\t\t\t\tclearForm();
\t\t\t\t\t\t\t\$('#btn_upload_cost').text('Upload').removeAttr('disabled');
\t\t\t\t\t\t\t\$('#msg-alert').html(res['message']);
\t\t\t\t\t\t\t//getData();
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t});
}

";
    }

    // line 760
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 761
        echo "
<ul class=\"nav nav-pills mb-3\" id=\"pills-tab\" role=\"tablist\">
  <li class=\"nav-item\">
    <a class=\"nav-link active\" onclick=\"getData();\" id=\"pills-home-tab\" data-toggle=\"pill\" href=\"#pills-home\" role=\"tab\" aria-controls=\"pills-home\" aria-selected=\"true\">Import Files</a>
  </li>
  <li class=\"nav-item\">
    <a class=\"nav-link\" onclick=\"getData2();\" id=\"pills-profile-tab\" data-toggle=\"pill\" href=\"#pills-profile\" role=\"tab\" aria-controls=\"pills-profile\" aria-selected=\"false\">รายชื่อติดต่อ</a>
  </li>
  <li class=\"nav-item\">
    <a class=\"nav-link\" onclick=\"getCost_center();\" id=\"pills-cost-center-tab\" data-toggle=\"pill\" href=\"#pills-cost-center\" role=\"tab\" aria-controls=\"pills-cost-center\" aria-selected=\"false\">cost-center</a>
  </li>
</ul>
<div class=\"tab-content\" id=\"pills-tabContent\">
\t<div class=\"tab-pane fade\" id=\"pills-cost-center\" role=\"tabpanel\" aria-labelledby=\"pills-home-tab\">
\t\t<form action=\"#\" method=\"POST\" id=\"formUpload\" enctype=\"multipart/form-data\" accept-charset=\"utf-8\" class=\"form-horizontal\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 border border-danger\">\t
\t\t\t\t\t<img src=\"../themes/images/excel_costCenter.jpg\" class=\"w-100\" style=\"display: none;\" id=\"img-excel\">
\t\t\t\t</div> 
\t\t\t\t<div class=\"col-md-12\">\t 
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"left:50px;\">
\t\t\t\t\t\t\t<input type=\"file\" name=\"file_upload_cost\" id=\"file_upload_cost\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"token\" id=\"token\" value=\"";
        // line 786
        echo twig_escape_filter($this->env, ($context["token"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"btn_upload\" class=\"label-control col-sm-2\"></label>
\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t<button onclick=\"upload_cost_center();\" type=\"button\" class=\"btn btn-primary\" name=\"btn_upload\" id=\"btn_upload_cost\"><i class=\"material-icons\">publish</i>Update Cost-Center</button>
\t\t\t\t\t\t<button class=\"btn btn-link text-danger\" type=\"button\" onclick=\"\$('#img-excel').toggle('show');(\$(this).text()=='แสดงตัวอยา่าง ไฟล์ import'?\$(this).text('ซ่อน'):\$(this).text('แสดงตัวอยา่าง ไฟล์ import'));\">แสดงตัวอยา่าง ไฟล์ import</button>
\t
\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"msg-alert\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</form>
\t\t<div class=\"card\" style=\"margin-top:10px;\" >
\t\t\t<div class=\"card-body\">\t\t
\t\t\t\t\t<h3><strong>รายชื่อพนักงาน</strong></h3>
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_cost_center\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>No</th>
\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t<th>ชื่อ</th>
\t\t\t\t\t\t\t<th>นามสกุล</th>
\t\t\t\t\t\t\t<th>รหัสพนักงาน</th>
\t\t\t\t\t\t\t<th>วันที่อัปเดทข้อมูลล่าสุด</th>
\t\t\t\t\t\t\t<th>ตำแหน่ง</th>
\t\t\t\t\t\t\t<th>แผนก</th>
\t\t\t\t\t\t\t<th>Cost-Center</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t</div>
<div class=\"tab-pane fade show active\" id=\"pills-home\" role=\"tabpanel\" aria-labelledby=\"pills-home-tab\">

\t<div class=\"page-header\">
\t\t<h1><strong>Import Files</strong></h1>
\t</div>\t
\t<div class=\"card\" >
\t<div class=\"card-body\">
\t\t<div class=\"row\">
\t\t<br>
\t\t\t<div class=\"col-md-5\" style=\"\">\t
\t\t\t\t\t<form action=\"#\" method=\"POST\" id=\"formUpload\" enctype=\"multipart/form-data\" accept-charset=\"utf-8\" class=\"form-horizontal\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">\t 
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"left:50px;\">
\t\t\t\t\t\t\t\t\t\t<input type=\"file\" name=\"txt_upload\" id=\"txt_upload\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label for=\"btn_upload\" class=\"label-control col-sm-2\"></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" id=\"btn_clear\"><i class=\"material-icons\">reply_all</i> Back</button>
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" id=\"btn_add\" onclick=\"\$('#div_add').slideToggle('slow');\"><i class=\"material-icons\">person_add</i>  Add</button>
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" name=\"btn_upload\" id=\"btn_upload\"><i class=\"material-icons\">publish</i>Upload</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"\$('#formExport').first().submit();\"type=\"button\" class=\"btn btn-primary\" name=\"btn_export\" id=\"btn_export\"><i class=\"material-icons\">archive</i>Export</button>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</form>
\t\t\t\t\t<form id=\"formExport\" action=\"export_excel_emp_data.php\" method=\"POST\" enctype=\"multipart/form-data\" accept-charset=\"utf-8\"></form>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<div id=\"lst_warning\">
\t\t\t\t\t<label for=\"warning_text\"><strong>ข้อแนะนำ !</strong></label>
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li>สามารถ upload ได้เฉพาะไฟล์นามสกุล .csv เท่านั้น</li>
\t\t\t\t\t\t<li>โปรดตรวจสอบและจัดการไฟล์เอกสารก่อนการ upload ไฟล์ทุกครั้ง มิฉะนั้นอาจทำให้เกิดข้อผิดพลาดขึ้นได้</li>
\t\t\t\t\t\t<li>หากข้อมูลไม่ครบ ระบบจะไม่ทำการเพิ่มรายชื่อพนักงานผู้นั้นเข้าสู่ระบบ </li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t<!-- \t\t<a href='./template_emp_data.xlsx' target=\"_blank\" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลด template emp</a><br><br> -->
\t\t\t</div>
\t\t\t
\t\t\t<br>
\t\t</div>
\t\t
\t\t<div id=\"div_edit_email\" style=\"display: none;\">
\t\t\t<div class=\"row\">
\t\t\t\t
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_name\">name :</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"edit_name\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_lname\">last name :</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"edit_lname\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_email\">Email :</label>
\t\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"emp_id\">
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"edit_email\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_branch\">ที่อยู่สาขา : </label>
\t\t\t\t\t<select class=\"form-control-lg\" id=\"edit_branch\" style=\"width:100%;\"></select>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"form-group col-md-12\">
\t\t\t\t\t<br>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" onclick=\"update_email();\">
\t\t\t\t\t\t<i class=\"material-icons\">unarchive\t</i>save
\t\t\t\t\t</button>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger \" onclick=\"\$('#div_edit_email').slideToggle('slow');\">
\t\t\t\t\t\t<i class=\"material-icons\">reply_all</i>cancle
\t\t\t\t\t</button>
\t\t\t\t
\t\t\t\t\t<br>\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>\t
\t\t\t<div id=\"div_add\" style=\"display: none;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">\t 
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<u><h4>เพิ่มข้อมูลพนักงาน</h4></u>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<form>\t
\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t\t<label for=\"emptype\">สสถานที่ปฏิบัติงาน :</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"emptype\" class=\"form-control\" onchange=\"chang_emptype();\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>สำนักงานใหญ่</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\">สาขา</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t\t<label for=\"group\">สังกัดพนักงาน :</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"group\" class=\"form-control\" onchange=\"chang_group();\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected>เลือกสังกัด :</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"tmb\">TMB</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"outsource\">Outsource</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"pivot\">Pivot</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-1\">
\t\t\t\t\t\t\t\t\t\t<label for=\"code\">รหัสพนักงาน :</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"code\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t<label for=\"name\">ชื่อ</label>
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"name\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t<label for=\"lname\">นามสกุล :</label>
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"lname\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"email\">อีเมล :</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"email\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"tel\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"mr_emp_tel\" class=\"form-control\" id=\"tel\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"branch\">ที่อยู่สาขา : </label>
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch\" style=\"width:100%;\"></select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t<label for=\"department\">แผนก/หน่วยงาน : </label>
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\"></select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-1\" id=\"div_floor\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"floor\">ชั้น :</label>
\t\t\t\t\t\t\t\t\t\t\t<select id=\"floor\" class=\"form-control-lg\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-1\" id=\"div_hub\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t<label for=\"emp_hub\">ศูน(ฮับ) :</label style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t<select id=\"emp_hub\" class=\"form-control-lg\" style=\"width:200px;\">
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"position\">ตำแหน่ง :</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"position\" class=\"form-control-lg\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" onclick=\"save_emp();\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">unarchive\t</i>save
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger \" onclick=\"\$('#div_add').slideToggle('slow');\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">reply_all</i>cancle
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div id=\"alert_success\"class=\"alert alert-success alert-dismissible fade show\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t<strong>Success : </strong> บันทึกข้อมูลสำเร็จ


\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div id=\"alert_error\" class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t<strong>Error!:  </strong><span id=\"text_error\"></span>.


\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t</form>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>\t\t

\t<div class=\"card\" style=\"margin-top:10px;\" >
\t<div class=\"card-body\">\t\t
\t\t\t
\t\t\t<h3><strong>รายชื่อพนักงาน</strong></h3>
\t\t\t<div class=\"table-responsive\">
\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>No</th>
\t\t\t\t\t<th>#</th>
\t\t\t\t\t<th>ชื่อ</th>
\t\t\t\t\t<th>นามสกุล</th>
\t\t\t\t\t<th>รหัสพนักงาน</th>
\t\t\t\t\t<th>Email</th>
\t\t\t\t\t<th>วันที่นำเข้า</th>
\t\t\t\t\t<th>วันที่อัปเดทข้อมูลล่าสุด</th>
\t\t\t\t\t<th>ตำแหน่ง</th>
\t\t\t\t\t<th>แผนก</th>
\t\t\t\t\t<th>ชั้น</th>
\t\t\t\t\t<th>สาขา</th>
\t\t\t\t\t
\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t</tbody>
\t\t\t</table>


\t\t\t


\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t</div>
\t</div>
</div>
<div class=\"tab-pane fade\" id=\"pills-profile\" role=\"tabpanel\" aria-labelledby=\"pills-profile-tab\">
<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_data_contact\">
            <thead>
              <tr>
                <th>No</th>
\t\t\t\t<th>#</th>
\t\t\t\t<th>แสดง</th>
                <th>รหัสหน่วยงาน</th>
                <th>ชื่อหน่วยงาน</th>
                <th>รหัสพนักงาน</th>
                <th>ชื่อผู้ติดต่อ</th>
                <th>โทร</th>
                <th>หมายเหตุ</th>
                <th>sysdate</th>
                
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
</div>
</div>

<div class='loading' style=\"display: none;\">
\t<img src=\"../themes/images/loading.gif\" class='img_loading'>
</div>


\t
<!-- Modal -->
<div class=\"modal fade\" id=\"editCon\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">แสดงข้อมูลที่</label><br>
\t\t\t\t<select id=\"con_type\" class=\"form-control\">
\t\t\t\t\t<option value=\"1\" selected>สำนักงานใหญ่</option>
\t\t\t\t\t<option value=\"2\">สาขา</option>
\t\t\t  </select>
\t\t\t\t
\t\t\t  </div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อ - นามสกุล</label><br>
\t\t\t\t<select class=\"form-control\" id=\"name_emp\" style=\"width:100%;\">
\t\t\t\t  <option value=\"\">";
        // line 1118
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["con_data"] ?? null), 0, [], "any", false, false, false, 1118), "emp_code", [], "any", false, false, false, 1118), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["con_data"] ?? null), 0, [], "any", false, false, false, 1118), "mr_contact_name", [], "any", false, false, false, 1118), "html", null, true);
        echo "</option>
\t\t\t\t  ";
        // line 1119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["all_emp"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["emp"]) {
            // line 1120
            echo "\t\t\t\t  <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_id", [], "any", false, false, false, 1120), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_code", [], "any", false, false, false, 1120), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_name", [], "any", false, false, false, 1120), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_lastname", [], "any", false, false, false, 1120), "html", null, true);
            echo "</option>
\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['emp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1122
        echo "\t\t\t\t</select>
\t\t\t\t
\t\t\t  </div>
\t\t\t 
\t\t\t  
\t\t\t <div class=\"form-group\">
\t\t\t\t<label for=\"\">เบอร์โทร</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_emp_tel\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"type_add\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_contact_id\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_emp_code\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"con_emp_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">รหัสหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"department_code\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_department_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชั้น</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_floor\" value=\"\" >
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">หมายเหตุ</label>
\t\t\t\t <textarea class=\"form-control\" id=\"con_remark\" rows=\"3\"></textarea >
\t\t\t  </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"button\" id=\"btnCon_save\" class=\"btn btn-primary\">Save changes</button>
      </div>
    </div>
  </div>
</div>
\t
\t\t\t\t
\t\t\t
";
    }

    public function getTemplateName()
    {
        return "mailroom/import_excel.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1216 => 1122,  1202 => 1120,  1198 => 1119,  1192 => 1118,  857 => 786,  830 => 761,  826 => 760,  467 => 403,  463 => 402,  100 => 41,  96 => 40,  69 => 15,  65 => 14,  54 => 5,  50 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}


{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">

<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>


{% endblock %}

{% block styleReady %}
\t.widthText {
\t\twidth: 300px;
\t}

\t
\t#lst_warning {
\t\tcolor: red;
\t}
\t.loading {
\t\tposition: fixed;
\t\ttop: 40%;
\t\tleft: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\t-ms-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
\t\tz-index: 1000;
\t}
\t
\t.img_loading {
\t\t  width: 350px;
\t\t  height: auto;
\t\t  
\t}
{% endblock %}

{% block domReady %}
//var url = \"https://www.pivot-services.com/mailroom_tmb/employee/\";
var url = \"\";



\$('#name_emp').select2({
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\ttheme: 'bootstrap4',
\t\t\t\tajax: {
\t\t\t\t\t//url: \"https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\turl: \"ajax/ajax_getdataemployee_select_search.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t},
\t\t\t\tdropdownParent: \$('#editCon')

\t\t}).on('select2:select', function(e) {
\t\t\t//console.log(e.params.data);
\t\t\tsetForm(e.params.data);
\t\t});

\t\tvar table = \$('#tb_work_order').DataTable({ 
\t\t\t\"responsive\": true,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'edit' },
\t\t\t\t{ 'data':'mr_emp_name'},
\t\t\t\t{ 'data':'mr_emp_lastname' },
\t\t\t\t{ 'data':'mr_emp_code' },
\t\t\t\t{ 'data':'mr_emp_email' },
\t\t\t\t{ 'data':'mr_date_import' },
\t\t\t\t{ 'data':'update_date' },
\t\t\t\t{ 'data':'position_name' },
\t\t\t\t{ 'data':'department_name' },
\t\t\t\t{ 'data':'floor_name' },
\t\t\t\t{ 'data':'branchname' }
\t\t\t]
\t\t});

\t\tvar table = \$('#tb_data_contact').DataTable({ 
\t\t\t\"responsive\": true,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'edit' },
\t\t\t\t{ 'data':'shows' },
\t\t\t\t{ 'data':'department_code'},
\t\t\t\t{ 'data':'department_name' },
\t\t\t\t{ 'data':'emp_code' },
\t\t\t\t{ 'data':'mr_contact_name' },
\t\t\t\t{ 'data':'emp_tel' },
\t\t\t\t{ 'data':'remark' },
\t\t\t\t{ 'data':'con_sysdate' },
\t\t\t]
\t\t});
\t\tvar table = \$('#tb_cost_center').DataTable({ 
\t\t\t\"responsive\": true,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'edit' },
\t\t\t\t{ 'data':'mr_emp_name' },
\t\t\t\t{ 'data':'mr_emp_lastname' },
\t\t\t\t{ 'data':'mr_emp_code' },
\t\t\t\t{ 'data':'update_date' },
\t\t\t\t{ 'data':'mr_position_name' },
\t\t\t\t{ 'data':'mr_department_name' },
\t\t\t\t{ 'data':'mr_cost_code' },
\t\t\t]
\t\t});

\t
\t\$('#btn_upload').on('click',function() {
\t\tvar data = \$('#txt_upload').prop(\"files\");
\t\tvar form_val = \$('#txt_upload').prop(\"files\")[0];
\t
\t\t// console.log(data);
\t\t
\t\t\t\$('#btn_upload').text('Uploading...').attr('disabled', 'disabled');
\t\t\tvar form_data = new FormData();
\t\t\tform_data.append('file', form_val);
\t\t\t//if(data[0]['type'] == \"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" || data[0]['type'] == \"application/vnd.ms-excel\"){
\t\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'ajax_readFile_Emp_csv.php',
               \t\tcache: false,
                \tcontentType: false,
                \tprocessData: false,
                \tdata: form_data,                         
\t\t\t\t\ttype: 'post',
\t\t\t\t\tdataType: 'html',
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t
\t\t\t\t\t\tif( res <= 0 ) {
\t\t\t\t\t\t\talert('ไม่มีข้อมูลนำเข้า');
\t\t\t\t\t\t\tclearForm();
\t\t\t\t\t\t\t\$('#btn_upload').text('Upload').removeAttr('disabled');
\t\t\t\t\t\t\tgetData();
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\$('#btn_upload').text('Upload').removeAttr('disabled');
\t\t\t\t\t\t\tclearForm();
\t\t\t\t\t\t\talert('การอัพโหลดรายชื่อพนักงานจำนวน '+ res +' รายชื่อ');
\t\t\t\t\t\t\tgetData();
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t});
\t\t\t
\t\t//}
\t});

\t\$('#btn_clear').click(function() {
        window.history.back();
\t}); 
\t
\t\$('#branch').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'branch'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  }\t,
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});

\t\$('#edit_branch').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'branch'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  }\t,
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});
\t\$('#department').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาแผนก/หน่วยงาน\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'department'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});
\t\$('#position').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาตำแหน่ง\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'position'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});
\t\$('#floor').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาชั้น\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'floor'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});

\t\$('#emptype').select2({
\t\ttheme: 'bootstrap4',
\t\twidth: '100%' 
\t});
\t\$('#group').select2({ 
\t\ttheme: 'bootstrap4',
\t\ttheme: 'bootstrap4',
\t\twidth: '100%' 
\t});

\t\$('#emp_hub').select2({
\t\ttheme: 'bootstrap4',
\t\tplaceholder: \"ค้นหาสาขา\",
\t\tajax: {
\t\t\turl: \"ajax/ajax_getdata_branch_select.php\",
\t\t\tdataType: \"json\",
\t\t\tdelay: 250,
\t\t\tdata: function (params) {
\t\t\t\tvar query = {
\t\t\t\t  search: params.term,
\t\t\t\t  type: 'public',
\t\t\t\t  page: 'hub'
\t\t\t\t}
\t\t\t\treturn query;
\t\t\t  },
\t\t\tprocessResults: function (data) {
\t\t\t\treturn {
\t\t\t\t\t results : data
\t\t\t\t};
\t\t\t},
\t\t\tcache: true
\t\t}
\t});



\t\t\t\t
\$( \"#btnCon_save\" ).click(function() {
\t\t\t\t\t\t\t\t\t\t
\tvar emp_hub \t\t\t= \$('#emp_hub').val();
\tvar con_type \t\t\t= \$('#con_type').val();
\tvar con_remark \t\t\t= \$('#con_remark').val();
\tvar con_floor \t\t\t= \$('#con_floor').val();
\tvar con_department_name = \$('#con_department_name').val();
\tvar mr_emp_tel \t\t\t= \$('#con_emp_tel').val();
\tvar con_emp_name \t\t= \$('#con_emp_name').val();
\tvar mr_emp_code \t\t= \$('#mr_emp_code').val();
\tvar mr_emp_id \t\t\t= \$('#name_emp').val();
\tvar department_code \t= \$('#department_code').val();
\tvar mr_contact_id \t\t= \$('#mr_contact_id').val();
\tvar type_add \t\t\t= \$('#type_add').val();
\t
\tvar dataselect2 = \$('#name_emp').select2('data');
\tcon_emp_name\t= dataselect2[0].text;
\t
\t
\t\$( \".myErrorClass\" ).removeClass( \"myErrorClass\" );
\t
\tif(mr_emp_id == ''){
\t\t\$( \"#name_emp\" ).addClass( \"myErrorClass\" );
\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
\t\treturn;
\t}if(mr_emp_code == ''){
\t\t\$( \"#mr_emp_code\" ).addClass( \"myErrorClass\" );
\t\talertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
\t\treturn;
\t}if(mr_emp_tel == ''){
\t\t\$( \"#mr_emp_tel\" ).addClass( \"myErrorClass\" );
\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
\t\treturn;
\t}
\t
alertify.confirm(\"ตรวจสอบข้อมูล\", \"กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก\",
function(){
\$.ajax({
\t\t  dataType: \"json\",
\t\t  method: \"POST\",
\t\t  url: \"ajax/ajax_save_data_contact.php\",
\t\t  data: { 
\t\t\t    emp_hub \t\t\t \t \t: con_type \t\t\t \t,
\t\t\t\tcon_type \t\t\t \t \t: con_type \t\t\t \t,
\t\t\t\tdepartment_code \t\t \t: department_code \t\t\t \t,
\t\t\t\tcon_emp_name \t\t\t \t: con_emp_name \t\t\t \t,
\t\t\t\tcon_remark \t\t\t \t \t: con_remark \t\t\t \t,
\t\t\t\tcon_floor \t\t\t \t \t: con_floor \t\t\t \t,
\t\t\t\tcon_department_name \t \t: con_department_name \t,
\t\t\t\ttype_add \t \t\t\t\t: type_add \t,
\t\t\t\tmr_emp_tel \t \t\t\t\t: mr_emp_tel \t,
\t\t\t\tmr_emp_code  \t\t\t\t: mr_emp_code ,
\t\t\t\tmr_emp_id \t \t\t\t\t: mr_emp_id \t,
\t\t\t\tmr_contact_id\t\t\t\t: mr_contact_id
\t\t  }
\t\t}).done(function(data) {
\t\t\t\$('#csrf_token').val(data.token);

\t\t\talertify.alert(data['st'],data['msg'],
\t\t\tfunction(){
\t\t\t\tgetData2();
\t\t\t\t\$('#editCon').modal('hide');
\t\t\t});
\t\t});
},function(){
\talertify.error('Cancel');
});


//alert( \"Handler for .click() called.\" );
//is-invalid
});



getData();
{% endblock %}

{% block javaScript %}

function edit_empdata(id,email,name,lname,mr_branch_id,branchname){
\t\$('#div_edit_email').slideDown('slow');
\t\$('#edit_email').val(email)
\t\$('#edit_name').val(name)
\t\$('#edit_lname').val(lname)
\t\$('#emp_id').val(id)
\t
\tvar newOption = new Option(branchname, mr_branch_id, false, false);
\t\$('#edit_branch').append(newOption).trigger('change');
\t\$('#edit_branch').append(newOption).trigger('change');
\t\$('#edit_branch').val(mr_branch_id); // Select the option with a value of '1'
\t\$('#edit_branch').trigger('change'); // Notify any JS components that the value changed

\t//\$(\"#edit_branch\").select2(mr_branch_id, branchname);
 console.log(newOption);
 console.log(mr_branch_id);
 console.log(branchname);

}


function getData2(){\t
\t\$.ajax({
\tmethod: \"POST\",
\tdataType: \"json\",
\turl: \"ajax/ajax_get_contact.php\",
\tbeforeSend: function( xhr ) {
\t\t\$('.loading').show();
\t},
\terror: function (xhr, ajaxOptions, thrownError) {
\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
\t\tlocation.reload();
\t}
\t})
\t.done(function( msg ) {
\t\t\$('.loading').hide();
\t\t\$('#tb_data_contact').DataTable().clear().draw();
\t\t\$('#tb_data_contact').DataTable().rows.add(msg.data).draw(); 
\t\t
\t});
}

function getCost_center(){\t
\t\$.ajax({
\tmethod: \"POST\",
\tdataType: \"json\",
\turl: \"ajax/ajax_get_Cost_center.php\",
\tbeforeSend: function( xhr ) {
\t\t\$('.loading').show();
\t\t\$('#msg-alert').html('');
\t},
\terror: function (xhr, ajaxOptions, thrownError) {
\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
\t\t//location.reload();
\t}
\t})
\t.done(function( msg ) {
\t\t\$('.loading').hide();
\t\t\$('#tb_cost_center').DataTable().clear().draw();
\t\t\$('#tb_cost_center').DataTable().rows.add(msg.data).draw(); 
\t\t
\t});
}


function getData(){
\t\$.ajax({
\tmethod: \"POST\",
\tdataType: \"json\",
\turl: \"ajax/ajax_get_emp.php\",
\tbeforeSend: function( xhr ) {
\t\t\$('.loading').show();
\t},
\terror: function (xhr, ajaxOptions, thrownError) {
\t\talert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
\t\t//location.reload();
\t}
\t})
\t.done(function( msg ) {
\t\t\$('.loading').hide();
\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw(); 
\t\t
\t});
};


\tvar clearForm = function() {
\t\t\$('#txt_upload').val(\"\");
\t}
\tfunction chang_emptype(){
\t\t\$('#group').val(\"\");
\t\tvar emptype = \$('#emptype').val();
\t\tif(emptype == 2){
\t\t\t\$('#div_floor').hide();
\t\t}else{
\t\t\t\$('#div_floor').show();
\t\t}
\t\tchang_group();
\t}

\tfunction chang_group(){
\t\tvar emptype = \$('#emptype').val();
\t\tvar group = \$('#group').val();
\t\tif(group == \"pivot\"){
\t\t\t\$('.not_pivot').hide();
\t\t\tif(emptype == 2){
\t\t\t\t\$('#div_hub').show();
\t\t\t}else{
\t\t\t\t\$('#div_hub').hide();
\t\t\t}
\t\t}else{
\t\t\t\$('.not_pivot').show();
\t\t\t\$('#div_hub').hide();
\t\t}
\t}



\tfunction resign_empdata(code){
\t\talertify.confirm('ลบพนักงานออกจากระบบ','ท่านต้องการลบนพนักงาน '+code+' ออกจากระบบ หรือไม่', 
\t\t\tfunction(){
\t\t\t\t\$.ajax({
\t\t\t\t\turl: \"ajax/ajax_save_employee.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: { 
\t\t\t\t\t\tmr_emp_code \t\t: code \t\t, 
\t\t\t\t\t\tpage \t\t\t\t: 'resign' \t
\t\t\t\t\t},
\t\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t},error: function (error) {
\t\t\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\t\t\tlocation.reload();
\t\t
\t\t\t\t\t},
\t\t\t\t  })
\t\t\t\t\t.done(function( data ) {
\t\t\t\t\t\t//getData()
\t\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\t\$('#btn_save').text('Save').removeAttr('disabled');
\t\t\t\t\t\tif(data['error'] == 'success'){
\t\t\t\t\t\t\t\$('#alert_success').show(); 
\t\t\t\t\t\t\t\$('#alert_error').hide(); 
\t\t\t\t\t\t\tgetData();
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\$('#alert_success').hide(); 
\t\t\t\t\t\t\t\$('#alert_error').show(); 
\t\t\t\t\t\t\t\$('#text_error').text(data['msg']); 
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t});\t
\t\t\t},function() {
\t\t}).set('labels', {ok:'ลบพนักงาน', cancel:'ไม่'});
\t}
\tfunction update_email(){
\t\tvar edit_email \t= \$('#edit_email').val();
\t\tvar edit_name \t= \$('#edit_name').val();
\t\tvar edit_lname \t= \$('#edit_lname').val();
\t\tvar edit_branch \t= \$('#edit_branch').val();
\t\tvar emp_id\t \t= \$('#emp_id').val();
\t\tif(edit_email == ''){
\t\t\talert('กรุณาระบบุ E-mail');
\t\t\treturn 
\t\t}
\t\t
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_save_employee.php\",
\t\t\tdataType: \"json\",
\t\t\tdata: { 
\t\t\t\temp_id\t \t\t: emp_id \t, 
\t\t\t\tedit_email \t\t: edit_email \t, 
\t\t\t\tedit_name \t\t: edit_name \t, 
\t\t\t\tedit_lname \t\t: edit_lname \t, 
\t\t\t\tedit_branch \t: edit_branch \t, 
\t\t\t\tpage \t\t\t: 'edit' \t
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t},error: function (error) {
\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\tlocation.reload();

\t\t\t},
\t\t  })
\t\t\t.done(function( data ) {
\t\t\t\t\$('.loading').hide();
\t\t\t\tif(data['error'] == 'success'){
\t\t\t\t\talert('บันทึกสำเร็จ');
\t\t\t\t\tlocation.reload();
\t\t\t\t}else{
\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\tlocation.reload();
\t\t\t\t}
\t\t\t\t
\t\t\t});


\t}
\tfunction save_emp(){
\t\tvar emp_hub \t\t\t= \$('#emp_hub').val();
\t\tvar emptype \t= \$('#emptype').val();
\t\tvar code \t\t= \$('#code').val();
\t\tvar name \t\t= \$('#name').val();
\t\tvar lname \t\t= \$('#lname').val();
\t\tvar email \t\t= \$('#email').val();
\t\tvar branch \t\t= \$('#branch').val();
\t\tvar department \t= \$('#department').val();
\t\tvar position \t= \$('#position').val();
\t\tvar tel \t\t= \$('#tel').val();
\t\tvar floor \t\t= \$('#floor').val();
\t\tvar group \t\t= \$('#group').val();




\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_save_employee.php\",
\t\t\tdataType: \"json\",
\t\t\tdata: { 
\t\t\t\temp_hub \t: emp_hub \t, 
\t\t\t\temptype \t: emptype \t, 
\t\t\t\tgroup \t\t: group \t, 
\t\t\t\tcode \t\t: code \t\t, 
\t\t\t\tname \t\t: name \t\t, 
\t\t\t\tfloor \t\t: floor \t\t, 
\t\t\t\tlname \t\t: lname \t\t, 
\t\t\t\temail \t\t: email \t\t, 
\t\t\t\tbranch \t\t: branch \t\t, 
\t\t\t\tdepartment \t: department \t, 
\t\t\t\ttel \t\t: tel \t, 
\t\t\t\tposition \t: position \t
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('#btn_save').text('saving...').attr('disabled', 'disabled');
\t\t\t\t\$('.loading').show();
\t\t\t},error: function (error) {
\t\t\t\t\talert('เกิดข้อผิดพลาดกรุณาลองใหม่');
\t\t\t\t\t//location.reload();

\t\t\t},
\t\t  })
\t\t\t.done(function( data ) {
\t\t\t\t//getData()
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#btn_save').text('Save').removeAttr('disabled');
\t\t\t\tif(data['error'] == 'success'){
\t\t\t\t\t\$('#alert_success').show(); 
\t\t\t\t\t\$('#alert_success').text(data['msg']); 
\t\t\t\t\t\$('#alert_error').hide(); 
\t\t\t\t\t//location.reload();
\t\t\t\t}else{
\t\t\t\t\t\$('#alert_success').hide(); 
\t\t\t\t\t\$('#alert_error').show(); 
\t\t\t\t\t\$('#text_error').text(data['msg']); 
\t\t\t\t}
\t\t\t\t
\t\t\t});


\t}

\t\t\t
function load_modal(mr_contact_id,type) {\t
window.scrollTo(0, 0);
 \$('#editCon').modal({
\t backdrop: false,
\t keyboard : true
\t});

\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_data_contact.php\",
\t  data: { \t
\t\t\t\tmr_contact_id : mr_contact_id,
\t\t\t\tpage : 'getdata'
\t  }
\t}).done(function(data) {
\t\t//alert('55555');
\t\tif(type!= \"add\"){
\t\t\t\$('#mr_contact_id').val(data['mr_contact_id']);
\t\t\t
\t\t}
\t\t
\t\t\$('#type_add').val(type);
\t\t\$('#mr_emp_code').val(data['emp_code']);
\t\t\$('#con_emp_tel').val(data['emp_tel']);
\t\t\$('#con_type').val(data['type']);

\t\t\$('#con_department_name').val(data['department_name']);
\t\t\$('#con_floor').val(data['floor']);
\t\t\$('#con_remark').val(data['remark']);
\t\t\$('#department_code').val(data['department_code']);
\t});
\t
}



function setForm(data) {
\tvar emp_id = parseInt(data.id);
\tvar fullname = data.text;
\t\$.ajax({
\t\t//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
\t\turl: 'ajax/ajax_autocompress_name.php',
\t\ttype: 'POST',
\t\tdata: {
\t\t\tname_receiver_select: emp_id
\t\t},
\t\tdataType: 'json',
\t\tsuccess: function(res) {
\t\t\tif(res.status == 200) {
\t\t\t\tif(res.data != false) {
\t\t\t\t\t\$(\"#con_emp_tel\").val(res['data']['mr_emp_tel']);
\t\t\t\t\t\$(\"#mr_emp_code\").val(res['data']['mr_emp_code']);
\t\t\t\t}
\t\t\t}
\t\t\t
\t\t}
\t})
}

function upload_cost_center() {
\t\$('#msg-alert').html('');
\t\tvar data = \$('#file_upload_cost').prop(\"files\");
\t\tvar form_val = \$('#file_upload_cost').prop(\"files\")[0];
\t
\t\t// console.log(data);
\t\t
\t\t\t\$('#btn_upload_cost').text('Uploading...').attr('disabled', 'disabled');
\t\t\tvar form_data = new FormData();
\t\t\tvar token = \$('#token').val();
\t\t\tform_data.append('file', form_val);
\t\t\tform_data.append('csrf_token', token);
\t\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_readFile_Cost_center.php',
               \t\tcache: false,
                \tcontentType: false,
                \tprocessData: false,
                \tdata: form_data,                         
\t\t\t\t\ttype: 'post',
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res){
\t\t\t\t\t\t\t\$('#token').val(res['token']);
\t\t\t\t\t\t\tclearForm();
\t\t\t\t\t\t\t\$('#btn_upload_cost').text('Upload').removeAttr('disabled');
\t\t\t\t\t\t\t\$('#msg-alert').html(res['message']);
\t\t\t\t\t\t\t//getData();
\t\t\t\t\t\t
\t\t\t\t\t}
\t\t\t});
}

{% endblock %}
\t
{% block Content2 %}

<ul class=\"nav nav-pills mb-3\" id=\"pills-tab\" role=\"tablist\">
  <li class=\"nav-item\">
    <a class=\"nav-link active\" onclick=\"getData();\" id=\"pills-home-tab\" data-toggle=\"pill\" href=\"#pills-home\" role=\"tab\" aria-controls=\"pills-home\" aria-selected=\"true\">Import Files</a>
  </li>
  <li class=\"nav-item\">
    <a class=\"nav-link\" onclick=\"getData2();\" id=\"pills-profile-tab\" data-toggle=\"pill\" href=\"#pills-profile\" role=\"tab\" aria-controls=\"pills-profile\" aria-selected=\"false\">รายชื่อติดต่อ</a>
  </li>
  <li class=\"nav-item\">
    <a class=\"nav-link\" onclick=\"getCost_center();\" id=\"pills-cost-center-tab\" data-toggle=\"pill\" href=\"#pills-cost-center\" role=\"tab\" aria-controls=\"pills-cost-center\" aria-selected=\"false\">cost-center</a>
  </li>
</ul>
<div class=\"tab-content\" id=\"pills-tabContent\">
\t<div class=\"tab-pane fade\" id=\"pills-cost-center\" role=\"tabpanel\" aria-labelledby=\"pills-home-tab\">
\t\t<form action=\"#\" method=\"POST\" id=\"formUpload\" enctype=\"multipart/form-data\" accept-charset=\"utf-8\" class=\"form-horizontal\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 border border-danger\">\t
\t\t\t\t\t<img src=\"../themes/images/excel_costCenter.jpg\" class=\"w-100\" style=\"display: none;\" id=\"img-excel\">
\t\t\t\t</div> 
\t\t\t\t<div class=\"col-md-12\">\t 
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"left:50px;\">
\t\t\t\t\t\t\t<input type=\"file\" name=\"file_upload_cost\" id=\"file_upload_cost\">
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"token\" id=\"token\" value=\"{{token}}\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"btn_upload\" class=\"label-control col-sm-2\"></label>
\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t<button onclick=\"upload_cost_center();\" type=\"button\" class=\"btn btn-primary\" name=\"btn_upload\" id=\"btn_upload_cost\"><i class=\"material-icons\">publish</i>Update Cost-Center</button>
\t\t\t\t\t\t<button class=\"btn btn-link text-danger\" type=\"button\" onclick=\"\$('#img-excel').toggle('show');(\$(this).text()=='แสดงตัวอยา่าง ไฟล์ import'?\$(this).text('ซ่อน'):\$(this).text('แสดงตัวอยา่าง ไฟล์ import'));\">แสดงตัวอยา่าง ไฟล์ import</button>
\t
\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"msg-alert\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t
\t\t</form>
\t\t<div class=\"card\" style=\"margin-top:10px;\" >
\t\t\t<div class=\"card-body\">\t\t
\t\t\t\t\t<h3><strong>รายชื่อพนักงาน</strong></h3>
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_cost_center\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>No</th>
\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t<th>ชื่อ</th>
\t\t\t\t\t\t\t<th>นามสกุล</th>
\t\t\t\t\t\t\t<th>รหัสพนักงาน</th>
\t\t\t\t\t\t\t<th>วันที่อัปเดทข้อมูลล่าสุด</th>
\t\t\t\t\t\t\t<th>ตำแหน่ง</th>
\t\t\t\t\t\t\t<th>แผนก</th>
\t\t\t\t\t\t\t<th>Cost-Center</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t</div>
<div class=\"tab-pane fade show active\" id=\"pills-home\" role=\"tabpanel\" aria-labelledby=\"pills-home-tab\">

\t<div class=\"page-header\">
\t\t<h1><strong>Import Files</strong></h1>
\t</div>\t
\t<div class=\"card\" >
\t<div class=\"card-body\">
\t\t<div class=\"row\">
\t\t<br>
\t\t\t<div class=\"col-md-5\" style=\"\">\t
\t\t\t\t\t<form action=\"#\" method=\"POST\" id=\"formUpload\" enctype=\"multipart/form-data\" accept-charset=\"utf-8\" class=\"form-horizontal\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12\">\t 
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-6\" style=\"left:50px;\">
\t\t\t\t\t\t\t\t\t\t<input type=\"file\" name=\"txt_upload\" id=\"txt_upload\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label for=\"btn_upload\" class=\"label-control col-sm-2\"></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" id=\"btn_clear\"><i class=\"material-icons\">reply_all</i> Back</button>
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" id=\"btn_add\" onclick=\"\$('#div_add').slideToggle('slow');\"><i class=\"material-icons\">person_add</i>  Add</button>
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" name=\"btn_upload\" id=\"btn_upload\"><i class=\"material-icons\">publish</i>Upload</button>
\t\t\t\t\t\t\t\t\t\t<button onclick=\"\$('#formExport').first().submit();\"type=\"button\" class=\"btn btn-primary\" name=\"btn_export\" id=\"btn_export\"><i class=\"material-icons\">archive</i>Export</button>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</form>
\t\t\t\t\t<form id=\"formExport\" action=\"export_excel_emp_data.php\" method=\"POST\" enctype=\"multipart/form-data\" accept-charset=\"utf-8\"></form>
\t\t\t</div>
\t\t\t<div class=\"col-md-7\">
\t\t\t\t<div id=\"lst_warning\">
\t\t\t\t\t<label for=\"warning_text\"><strong>ข้อแนะนำ !</strong></label>
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li>สามารถ upload ได้เฉพาะไฟล์นามสกุล .csv เท่านั้น</li>
\t\t\t\t\t\t<li>โปรดตรวจสอบและจัดการไฟล์เอกสารก่อนการ upload ไฟล์ทุกครั้ง มิฉะนั้นอาจทำให้เกิดข้อผิดพลาดขึ้นได้</li>
\t\t\t\t\t\t<li>หากข้อมูลไม่ครบ ระบบจะไม่ทำการเพิ่มรายชื่อพนักงานผู้นั้นเข้าสู่ระบบ </li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t<!-- \t\t<a href='./template_emp_data.xlsx' target=\"_blank\" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลด template emp</a><br><br> -->
\t\t\t</div>
\t\t\t
\t\t\t<br>
\t\t</div>
\t\t
\t\t<div id=\"div_edit_email\" style=\"display: none;\">
\t\t\t<div class=\"row\">
\t\t\t\t
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_name\">name :</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"edit_name\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_lname\">last name :</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"edit_lname\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_email\">Email :</label>
\t\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"emp_id\">
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"edit_email\">
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-md-3\">
\t\t\t\t\t<label for=\"edit_branch\">ที่อยู่สาขา : </label>
\t\t\t\t\t<select class=\"form-control-lg\" id=\"edit_branch\" style=\"width:100%;\"></select>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"form-group col-md-12\">
\t\t\t\t\t<br>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" onclick=\"update_email();\">
\t\t\t\t\t\t<i class=\"material-icons\">unarchive\t</i>save
\t\t\t\t\t</button>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger \" onclick=\"\$('#div_edit_email').slideToggle('slow');\">
\t\t\t\t\t\t<i class=\"material-icons\">reply_all</i>cancle
\t\t\t\t\t</button>
\t\t\t\t
\t\t\t\t\t<br>\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>\t
\t\t\t<div id=\"div_add\" style=\"display: none;\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">\t 
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<u><h4>เพิ่มข้อมูลพนักงาน</h4></u>
\t\t\t\t\t\t<br>
\t\t\t\t\t\t<form>\t
\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t\t<label for=\"emptype\">สสถานที่ปฏิบัติงาน :</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"emptype\" class=\"form-control\" onchange=\"chang_emptype();\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected>สำนักงานใหญ่</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\">สาขา</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t\t<label for=\"group\">สังกัดพนักงาน :</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"group\" class=\"form-control\" onchange=\"chang_group();\">
\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected>เลือกสังกัด :</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"tmb\">TMB</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"outsource\">Outsource</option>
\t\t\t\t\t\t\t\t\t\t\t<option value=\"pivot\">Pivot</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-1\">
\t\t\t\t\t\t\t\t\t\t<label for=\"code\">รหัสพนักงาน :</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"code\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t<label for=\"name\">ชื่อ</label>
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"name\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-4\">
\t\t\t\t\t\t\t\t\t<label for=\"lname\">นามสกุล :</label>
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"lname\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"email\">อีเมล :</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"email\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"tel\">เบอร์โทร :</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"mr_emp_tel\" class=\"form-control\" id=\"tel\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"branch\">ที่อยู่สาขา : </label>
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"branch\" style=\"width:100%;\"></select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t<label for=\"department\">แผนก/หน่วยงาน : </label>
\t\t\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\"></select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-1\" id=\"div_floor\">
\t\t\t\t\t\t\t\t\t\t\t<label for=\"floor\">ชั้น :</label>
\t\t\t\t\t\t\t\t\t\t\t<select id=\"floor\" class=\"form-control-lg\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group col-md-1\" id=\"div_hub\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t<label for=\"emp_hub\">ศูน(ฮับ) :</label style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t<select id=\"emp_hub\" class=\"form-control-lg\" style=\"width:200px;\">
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group col-md-3 not_pivot\">
\t\t\t\t\t\t\t\t\t\t<label for=\"position\">ตำแหน่ง :</label>
\t\t\t\t\t\t\t\t\t\t<select id=\"position\" class=\"form-control-lg\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" onclick=\"save_emp();\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">unarchive\t</i>save
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger \" onclick=\"\$('#div_add').slideToggle('slow');\">
\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">reply_all</i>cancle
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<div id=\"alert_success\"class=\"alert alert-success alert-dismissible fade show\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t<strong>Success : </strong> บันทึกข้อมูลสำเร็จ


\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div id=\"alert_error\" class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t<strong>Error!:  </strong><span id=\"text_error\"></span>.


\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t</form>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>\t\t

\t<div class=\"card\" style=\"margin-top:10px;\" >
\t<div class=\"card-body\">\t\t
\t\t\t
\t\t\t<h3><strong>รายชื่อพนักงาน</strong></h3>
\t\t\t<div class=\"table-responsive\">
\t\t\t<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>No</th>
\t\t\t\t\t<th>#</th>
\t\t\t\t\t<th>ชื่อ</th>
\t\t\t\t\t<th>นามสกุล</th>
\t\t\t\t\t<th>รหัสพนักงาน</th>
\t\t\t\t\t<th>Email</th>
\t\t\t\t\t<th>วันที่นำเข้า</th>
\t\t\t\t\t<th>วันที่อัปเดทข้อมูลล่าสุด</th>
\t\t\t\t\t<th>ตำแหน่ง</th>
\t\t\t\t\t<th>แผนก</th>
\t\t\t\t\t<th>ชั้น</th>
\t\t\t\t\t<th>สาขา</th>
\t\t\t\t\t
\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t</tbody>
\t\t\t</table>


\t\t\t


\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t</div>
\t</div>
</div>
<div class=\"tab-pane fade\" id=\"pills-profile\" role=\"tabpanel\" aria-labelledby=\"pills-profile-tab\">
<table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_data_contact\">
            <thead>
              <tr>
                <th>No</th>
\t\t\t\t<th>#</th>
\t\t\t\t<th>แสดง</th>
                <th>รหัสหน่วยงาน</th>
                <th>ชื่อหน่วยงาน</th>
                <th>รหัสพนักงาน</th>
                <th>ชื่อผู้ติดต่อ</th>
                <th>โทร</th>
                <th>หมายเหตุ</th>
                <th>sysdate</th>
                
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
</div>
</div>

<div class='loading' style=\"display: none;\">
\t<img src=\"../themes/images/loading.gif\" class='img_loading'>
</div>


\t
<!-- Modal -->
<div class=\"modal fade\" id=\"editCon\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">แสดงข้อมูลที่</label><br>
\t\t\t\t<select id=\"con_type\" class=\"form-control\">
\t\t\t\t\t<option value=\"1\" selected>สำนักงานใหญ่</option>
\t\t\t\t\t<option value=\"2\">สาขา</option>
\t\t\t  </select>
\t\t\t\t
\t\t\t  </div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อ - นามสกุล</label><br>
\t\t\t\t<select class=\"form-control\" id=\"name_emp\" style=\"width:100%;\">
\t\t\t\t  <option value=\"\">{{con_data.0.emp_code}} : {{con_data.0.mr_contact_name}}</option>
\t\t\t\t  {% for emp in all_emp %}
\t\t\t\t  <option value=\"{{ emp.mr_emp_id }}\">{{emp.mr_emp_code}} : {{emp.mr_emp_name}}{{emp.mr_emp_lastname}}</option>
\t\t\t\t  {% endfor %}
\t\t\t\t</select>
\t\t\t\t
\t\t\t  </div>
\t\t\t 
\t\t\t  
\t\t\t <div class=\"form-group\">
\t\t\t\t<label for=\"\">เบอร์โทร</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_emp_tel\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"type_add\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_contact_id\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_emp_code\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"con_emp_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">รหัสหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"department_code\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_department_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชั้น</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_floor\" value=\"\" >
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">หมายเหตุ</label>
\t\t\t\t <textarea class=\"form-control\" id=\"con_remark\" rows=\"3\"></textarea >
\t\t\t  </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"button\" id=\"btnCon_save\" class=\"btn btn-primary\">Save changes</button>
      </div>
    </div>
  </div>
</div>
\t
\t\t\t\t
\t\t\t
{% endblock %}

", "mailroom/import_excel.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\import_excel.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/report.byhand.tpl */
class __TwigTemplate_fb963cca6b561148acb4a0beafa30ebc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/report.byhand.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "
.loading {
\tposition: fixed;
\ttop: 40%;
\tleft: 50%;
\t-webkit-transform: translate(-50%, -50%);
\t-ms-transform: translate(-50%, -50%);
\ttransform: translate(-50%, -50%);
\tz-index: 1000;
}

.img_loading {
\t  width: 350px;
\t  height: auto;
\t  
}

";
    }

    // line 25
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

";
    }

    // line 33
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "\t\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
\t\tvar table = \$('#tb_level3').DataTable({ 
\t\t\t\"scrollY\": \"800px\",
\t\t\t\"scrollCollapse\": true,
\t\t\t\"responsive\": true,
\t\t\t//\"searching\": false,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'link_click' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_topic'},
\t\t\t\t{ 'data':'mr_work_remark'},
\t\t\t\t{ 'data':'main_sys_time'},
\t\t\t\t{ 'data':'time_mail_re' },
\t\t\t\t{ 'data':'sla_date_end' },
\t\t\t\t{ 'data':'time_log' },
\t\t\t\t{ 'data':'name_get' },
\t\t\t\t{ 'data':'mr_status_name' },
\t\t\t\t{ 'data':'mr_type_work_name' }
\t\t\t]
\t\t});
";
    }

    // line 67
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 68
        echo "\t function getDetail_level1(){
\t\t\tvar date_1 = \$('#date_1').val();
\t\t\tvar date_2 = \$('#date_2').val();
\t\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();
\t\t\tif(date_1 == '' || date_2 == ''){
\t\t\t\talertify.alert(\"กิดข้อผิดผลาด !\",\"กรุณาเลือกช่วงวันที่\");
\t\t\t\treturn;\t
\t\t\t}
\t\t\tif(type == 'ho'){
\t\t\t\t\$('#box_branch_type').hide();
\t\t\t\t//console.log('hode');
\t\t\t}else{
\t\t\t\t\$('#box_branch_type').show();
\t\t\t}
\t\t\t//console.log(type);
\t\t\t\$.ajax({
\t\t\t\tmethod: \"POST\",
\t\t\t\tdataType: \"json\",
\t\t\t\turl: \"ajax/ajax_getReport_level1.php\",
\t\t\t\tdata: { 
\t\t\t\t\tdate_1\t\t: date_1, 
\t\t\t\t\tdate_2\t\t: date_2,
\t\t\t\t\ttype\t\t: type,
\t\t\t\t\tbranch_type\t: branch_type
\t\t\t\t},
\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t\$('#Detail_level1').hide();
\t\t\t\t\t\$('#Detail_level2').hide();
\t\t\t\t},
\t\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert('เกิดข้อผิดผลาด !');
\t\t\t\t\t//location.reload();

\t\t\t\t  }
\t\t\t})
\t\t\t.done(function( msg ) {
\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\$('#Detail_level2').fadeIn('slow');
\t\t\t\t\t\$('#content_level2').html(msg['html']);
\t\t\t\t\t\$('#date_1_2').val(msg['date_1'])
\t\t\t\t\t\$('#date_2_2').val(msg['date_2'])
\t\t\t\t  //alert( \"Data Saved: \" + msg );
\t\t\t});
\t}


\tfunction getDetail_level3(status,branch_type_re){
\t\tvar date_1 = \$('#date_1').val();
\t\tvar date_2 = \$('#date_2').val();
\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();

\t\tif(type == 'ho'){
\t\t\t\$('#box_branch_type').hide();
\t\t\t//console.log('hode');
\t\t}else{
\t\t\t\$('#box_branch_type').show();
\t\t}
\t\t//console.log(type);
\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType: \"json\",
\t\t\turl: \"ajax/ajax_getReport_level3.php\",
\t\t\tdata: { 
\t\t\t\tdate_1\t\t\t\t: date_1, 
\t\t\t\tdate_2\t\t\t\t: date_2,
\t\t\t\tstatus\t\t\t\t: status,
\t\t\t\ttype\t\t\t\t: type,
\t\t\t\tbranch_type\t\t\t: branch_type,
\t\t\t\tbranch_type_re\t\t: branch_type_re,
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t\t\$('#Detail_level1').hide();
\t\t\t\t\$('#Detail_level2').hide();
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert('เกิดข้อผิดผลาด !');
\t\t\t\t//alertify.alert(\"กิดข้อผิดผลาด !\");
\t\t\t\t//location.reload();

\t\t\t  }
\t\t})
\t\t.done(function( msg ) {
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#Detail_level3').fadeIn('slow');
\t\t\t\t\$('#head_level3').html(msg['html_head']);
\t\t\t\t\$('#tb_level3').DataTable().clear().draw();
\t\t\t\t\$('#tb_level3').DataTable().rows.add(msg['data']).draw();

\t\t\t\tvar input1 = \$(\"<input>\", { type: \"hidden\", name: \"status\", \t\tvalue: status }); 
\t\t\t\tvar input2 = \$(\"<input>\", { type: \"hidden\", name: \"branch_type_re\",value: branch_type_re }); 
\t\t\t\t\$('#form_level_3').append(input1); 
\t\t\t\t\$('#form_level_3').append(input2); 


\t\t\t//alert( \"Data Saved: \" + msg );
\t\t});
}

\tfunction click_back(){
\t\t\$('#Detail_level1').show();
\t\t\$('#Detail_level2').hide();
\t}
\tfunction click_backlevel3(){
\t\t\$('#Detail_level2').show();
\t\t\$('#Detail_level3').hide();
\t}
\tfunction click_branch_type_chang(){
\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();
\t\t//console.log(branch_type);
\t\tif(branch_type == 3){
\t\t\t\$('#tb_branch_1').fadeIn(1000);
\t\t\t\$('#tb_branch_2').hide();
\t\t}else{
\t\t\t\$('#tb_branch_2').fadeIn(1000);
\t\t\t\$('#tb_branch_1').hide();
\t\t}

\t}

\tfunction click_export_level_1(){
\t\t\tvar date_1 = \$('#date_1').val();
\t\t\tvar date_2 = \$('#date_2').val();
\t\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();


\t\tvar input1 = \$(\"<input>\", { type: \"hidden\", name: \"date_1\", \tvalue: date_1 }); 
\t\tvar input2 = \$(\"<input>\", { type: \"hidden\", name: \"date_2\", \tvalue: date_2 }); 
\t\tvar input3 = \$(\"<input>\", { type: \"hidden\", name: \"type\", \t\tvalue: type }); 
\t\tvar input4 = \$(\"<input>\", { type: \"hidden\", name: \"branch_type\",value: branch_type }); 

\t\t\$('#form_level_1').append(input1); 
\t\t\$('#form_level_1').append(input2); 
\t\t\$('#form_level_1').append(input3); 
\t\t\$('#form_level_1').append(input4); 
\t\t\$('#form_level_1').submit();
\t}

function click_export_level_3(){
\t
\t\t\tvar date_1 = \$('#date_1').val();
\t\t\tvar date_2 = \$('#date_2').val();
\t\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();


\t\tvar input1 = \$(\"<input>\", { type: \"hidden\", name: \"date_1\", \tvalue: date_1 }); 
\t\tvar input2 = \$(\"<input>\", { type: \"hidden\", name: \"date_2\", \tvalue: date_2 }); 
\t\tvar input3 = \$(\"<input>\", { type: \"hidden\", name: \"type\", \t\tvalue: type }); 
\t\tvar input4 = \$(\"<input>\", { type: \"hidden\", name: \"branch_type\",value: branch_type }); 

\t\t\$('#form_level_3').append(input1); 
\t\t\$('#form_level_3').append(input2); 
\t\t\$('#form_level_3').append(input3); 
\t\t\$('#form_level_3').append(input4); 
\t\t\$('#form_level_3').submit();
\t}
";
    }

    // line 235
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 236
        echo "<br>
<div id =\"Detail_level1\">
<div class=\"row justify-content-center\">
\t<div class=\"col-lg-4 col-12\">
\t\t<h2 class=\"text-center\">รายงาน By Hand(ขาออก)</h2>
\t</div>
</div>
<br>
<form id=\"form_submit_report\" action=\"excel.byhand.php\" method=\"post\" target=\"_blank\">
<div class=\"row justify-content-center\">
\t<div class=\"col-lg-4 col-12\">
\t\t<div class=\"input-daterange\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t<div class=\" input-group\">
\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">วันที่เริ่มต้น</label>
\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"date_1\" name=\"date_1\"  placeholder=\"From date\" autocomplete=\"off\" required/>
\t\t\t</div>
\t\t\t<br>
\t\t\t<div class=\" input-group\">
\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">วันที่สิ้นสุด</label>
\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"date_2\" name=\"date_2\" placeholder=\"To date\" autocomplete=\"off\" required/>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<br>
<br>
<hr>
<div class=\"row justify-content-center\">
\t\t<button type=\"submit\" class=\"btn btn-primary btn-lg\">EXPORT EXCEL</button>
</div>
</form>
</div>

<div class='loading' style=\"display: none;\">
    <img src=\"../themes/images/loading.gif\" class='img_loading'>
</div>
";
    }

    // line 275
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 277
        if ((($context["debug"] ?? null) != "")) {
            // line 278
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 280
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/report.byhand.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  371 => 280,  365 => 278,  363 => 277,  356 => 275,  316 => 236,  312 => 235,  147 => 68,  143 => 67,  109 => 34,  105 => 33,  96 => 26,  92 => 25,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}

.loading {
\tposition: fixed;
\ttop: 40%;
\tleft: 50%;
\t-webkit-transform: translate(-50%, -50%);
\t-ms-transform: translate(-50%, -50%);
\ttransform: translate(-50%, -50%);
\tz-index: 1000;
}

.img_loading {
\t  width: 350px;
\t  height: auto;
\t  
}

{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>

{% endblock %}

{% block domReady %}
\t\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
\t\tvar table = \$('#tb_level3').DataTable({ 
\t\t\t\"scrollY\": \"800px\",
\t\t\t\"scrollCollapse\": true,
\t\t\t\"responsive\": true,
\t\t\t//\"searching\": false,
\t\t\t\"language\": {
\t\t\t\t\"emptyTable\": \"ไม่มีข้อมูล!\"
\t\t\t},
\t\t\t\"pageLength\": 10, 
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'link_click' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_topic'},
\t\t\t\t{ 'data':'mr_work_remark'},
\t\t\t\t{ 'data':'main_sys_time'},
\t\t\t\t{ 'data':'time_mail_re' },
\t\t\t\t{ 'data':'sla_date_end' },
\t\t\t\t{ 'data':'time_log' },
\t\t\t\t{ 'data':'name_get' },
\t\t\t\t{ 'data':'mr_status_name' },
\t\t\t\t{ 'data':'mr_type_work_name' }
\t\t\t]
\t\t});
{% endblock %}


{% block javaScript %}
\t function getDetail_level1(){
\t\t\tvar date_1 = \$('#date_1').val();
\t\t\tvar date_2 = \$('#date_2').val();
\t\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();
\t\t\tif(date_1 == '' || date_2 == ''){
\t\t\t\talertify.alert(\"กิดข้อผิดผลาด !\",\"กรุณาเลือกช่วงวันที่\");
\t\t\t\treturn;\t
\t\t\t}
\t\t\tif(type == 'ho'){
\t\t\t\t\$('#box_branch_type').hide();
\t\t\t\t//console.log('hode');
\t\t\t}else{
\t\t\t\t\$('#box_branch_type').show();
\t\t\t}
\t\t\t//console.log(type);
\t\t\t\$.ajax({
\t\t\t\tmethod: \"POST\",
\t\t\t\tdataType: \"json\",
\t\t\t\turl: \"ajax/ajax_getReport_level1.php\",
\t\t\t\tdata: { 
\t\t\t\t\tdate_1\t\t: date_1, 
\t\t\t\t\tdate_2\t\t: date_2,
\t\t\t\t\ttype\t\t: type,
\t\t\t\t\tbranch_type\t: branch_type
\t\t\t\t},
\t\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\t\$('.loading').show();
\t\t\t\t\t\$('#Detail_level1').hide();
\t\t\t\t\t\$('#Detail_level2').hide();
\t\t\t\t},
\t\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert('เกิดข้อผิดผลาด !');
\t\t\t\t\t//location.reload();

\t\t\t\t  }
\t\t\t})
\t\t\t.done(function( msg ) {
\t\t\t\t\t\$('.loading').hide();
\t\t\t\t\t\$('#Detail_level2').fadeIn('slow');
\t\t\t\t\t\$('#content_level2').html(msg['html']);
\t\t\t\t\t\$('#date_1_2').val(msg['date_1'])
\t\t\t\t\t\$('#date_2_2').val(msg['date_2'])
\t\t\t\t  //alert( \"Data Saved: \" + msg );
\t\t\t});
\t}


\tfunction getDetail_level3(status,branch_type_re){
\t\tvar date_1 = \$('#date_1').val();
\t\tvar date_2 = \$('#date_2').val();
\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();

\t\tif(type == 'ho'){
\t\t\t\$('#box_branch_type').hide();
\t\t\t//console.log('hode');
\t\t}else{
\t\t\t\$('#box_branch_type').show();
\t\t}
\t\t//console.log(type);
\t\t\$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType: \"json\",
\t\t\turl: \"ajax/ajax_getReport_level3.php\",
\t\t\tdata: { 
\t\t\t\tdate_1\t\t\t\t: date_1, 
\t\t\t\tdate_2\t\t\t\t: date_2,
\t\t\t\tstatus\t\t\t\t: status,
\t\t\t\ttype\t\t\t\t: type,
\t\t\t\tbranch_type\t\t\t: branch_type,
\t\t\t\tbranch_type_re\t\t: branch_type_re,
\t\t\t},
\t\t\tbeforeSend: function( xhr ) {
\t\t\t\t\$('.loading').show();
\t\t\t\t\$('#Detail_level1').hide();
\t\t\t\t\$('#Detail_level2').hide();
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert('เกิดข้อผิดผลาด !');
\t\t\t\t//alertify.alert(\"กิดข้อผิดผลาด !\");
\t\t\t\t//location.reload();

\t\t\t  }
\t\t})
\t\t.done(function( msg ) {
\t\t\t\t\$('.loading').hide();
\t\t\t\t\$('#Detail_level3').fadeIn('slow');
\t\t\t\t\$('#head_level3').html(msg['html_head']);
\t\t\t\t\$('#tb_level3').DataTable().clear().draw();
\t\t\t\t\$('#tb_level3').DataTable().rows.add(msg['data']).draw();

\t\t\t\tvar input1 = \$(\"<input>\", { type: \"hidden\", name: \"status\", \t\tvalue: status }); 
\t\t\t\tvar input2 = \$(\"<input>\", { type: \"hidden\", name: \"branch_type_re\",value: branch_type_re }); 
\t\t\t\t\$('#form_level_3').append(input1); 
\t\t\t\t\$('#form_level_3').append(input2); 


\t\t\t//alert( \"Data Saved: \" + msg );
\t\t});
}

\tfunction click_back(){
\t\t\$('#Detail_level1').show();
\t\t\$('#Detail_level2').hide();
\t}
\tfunction click_backlevel3(){
\t\t\$('#Detail_level2').show();
\t\t\$('#Detail_level3').hide();
\t}
\tfunction click_branch_type_chang(){
\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();
\t\t//console.log(branch_type);
\t\tif(branch_type == 3){
\t\t\t\$('#tb_branch_1').fadeIn(1000);
\t\t\t\$('#tb_branch_2').hide();
\t\t}else{
\t\t\t\$('#tb_branch_2').fadeIn(1000);
\t\t\t\$('#tb_branch_1').hide();
\t\t}

\t}

\tfunction click_export_level_1(){
\t\t\tvar date_1 = \$('#date_1').val();
\t\t\tvar date_2 = \$('#date_2').val();
\t\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();


\t\tvar input1 = \$(\"<input>\", { type: \"hidden\", name: \"date_1\", \tvalue: date_1 }); 
\t\tvar input2 = \$(\"<input>\", { type: \"hidden\", name: \"date_2\", \tvalue: date_2 }); 
\t\tvar input3 = \$(\"<input>\", { type: \"hidden\", name: \"type\", \t\tvalue: type }); 
\t\tvar input4 = \$(\"<input>\", { type: \"hidden\", name: \"branch_type\",value: branch_type }); 

\t\t\$('#form_level_1').append(input1); 
\t\t\$('#form_level_1').append(input2); 
\t\t\$('#form_level_1').append(input3); 
\t\t\$('#form_level_1').append(input4); 
\t\t\$('#form_level_1').submit();
\t}

function click_export_level_3(){
\t
\t\t\tvar date_1 = \$('#date_1').val();
\t\t\tvar date_2 = \$('#date_2').val();
\t\t\tvar type = \$(\"input[name=type1]:checked\").val();
\t\t\tvar branch_type = \$(\"input[name=branch_type]:checked\").val();


\t\tvar input1 = \$(\"<input>\", { type: \"hidden\", name: \"date_1\", \tvalue: date_1 }); 
\t\tvar input2 = \$(\"<input>\", { type: \"hidden\", name: \"date_2\", \tvalue: date_2 }); 
\t\tvar input3 = \$(\"<input>\", { type: \"hidden\", name: \"type\", \t\tvalue: type }); 
\t\tvar input4 = \$(\"<input>\", { type: \"hidden\", name: \"branch_type\",value: branch_type }); 

\t\t\$('#form_level_3').append(input1); 
\t\t\$('#form_level_3').append(input2); 
\t\t\$('#form_level_3').append(input3); 
\t\t\$('#form_level_3').append(input4); 
\t\t\$('#form_level_3').submit();
\t}
{% endblock %}





{% block Content2 %}
<br>
<div id =\"Detail_level1\">
<div class=\"row justify-content-center\">
\t<div class=\"col-lg-4 col-12\">
\t\t<h2 class=\"text-center\">รายงาน By Hand(ขาออก)</h2>
\t</div>
</div>
<br>
<form id=\"form_submit_report\" action=\"excel.byhand.php\" method=\"post\" target=\"_blank\">
<div class=\"row justify-content-center\">
\t<div class=\"col-lg-4 col-12\">
\t\t<div class=\"input-daterange\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t<div class=\" input-group\">
\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">วันที่เริ่มต้น</label>
\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"date_1\" name=\"date_1\"  placeholder=\"From date\" autocomplete=\"off\" required/>
\t\t\t</div>
\t\t\t<br>
\t\t\t<div class=\" input-group\">
\t\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">วันที่สิ้นสุด</label>
\t\t\t\t<input type=\"text\" class=\"input-sm form-control\" id=\"date_2\" name=\"date_2\" placeholder=\"To date\" autocomplete=\"off\" required/>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<br>
<br>
<hr>
<div class=\"row justify-content-center\">
\t\t<button type=\"submit\" class=\"btn btn-primary btn-lg\">EXPORT EXCEL</button>
</div>
</form>
</div>

<div class='loading' style=\"display: none;\">
    <img src=\"../themes/images/loading.gif\" class='img_loading'>
</div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/report.byhand.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/report.byhand.tpl");
    }
}

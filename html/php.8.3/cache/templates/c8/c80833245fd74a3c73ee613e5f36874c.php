<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/edit_floor_send.tpl */
class __TwigTemplate_d4fd60f8598dd68b5a58da0e39a1a7a7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/edit_floor_send.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "\t\t
\t\t";
        // line 9
        echo twig_escape_filter($this->env, ($context["alert"] ?? null), "html", null, true);
        echo "
\t\t
\t\t\$(\"#mr_type_work_id\").change(function(){
\t\t\tvar type  = \$(this).val();
\t\t\tif(type == 2){
\t\t\t\t\$('#div_branch_id').show();
\t\t\t\t\$('#floor_branch').show();
\t\t\t\t\$('#floor_ho').hide();
\t\t\t}else{
\t\t\t\t\$('#div_branch_id').hide();
\t\t\t\t\$('#floor_branch').hide();
\t\t\t\t\$('#floor_ho').show();
\t\t\t}
\t\t});

\t\t\$(\"#mr_branch_id\").change(function(){
\t\t\tvar type  = \$(\"#mr_type_work_id\").val()
\t\t\tvar branch_id  = \$(this).val();
\t\t\t\$.ajax({
\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tbranch_id: branch_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\$('#floor2').html(res.data);
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t})
\t\t\tconsole.log(type);
\t\t});


\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar floor \t\t\t\t\t\t\t= \$(\"#floor\").val();
\t\t\tvar floor2 \t\t\t\t\t\t\t= \$(\"#floor2\").val();
\t\t\tvar mr_branch_id \t\t\t\t= \$(\"#mr_branch_id\").val();
\t\t\tvar mr_work_inout_id \t\t\t\t= \$(\"#mr_work_inout_id\").val();
\t\t\tvar mr_type_work_id \t\t\t\t= \$(\"#mr_type_work_id\").val();
\t\t\tvar mr_work_main_id \t\t\t\t= \$(\"#mr_work_main_id\").val();
\t\t\tvar mr_status_id \t\t\t\t\t= \$(\"#mr_status_id\").val();
\t\t\tvar data \t\t\t\t\t\t\t= \$('#floor2').select2('data');
\t\t\t
\t\t\tif(\$('#floor2').val()){
\t\t\t\tvar floor2_text \t\t\t\t\t= data[0].text;
\t\t\t}

\t\t\tif(mr_type_work_id == 2 && mr_branch_id == 0){
\t\t\t\talert('กรุณาระบุสาขาปลายทาง');
\t\t\t\treturn;
\t\t\t}else{
\t\t\t\t\$.post(
\t\t\t\t'./ajax/ajax_editFloorSend.php',
\t\t\t\t{
\t\t\t\t\tmr_branch_id \t\t : mr_branch_id,
\t\t\t\t\tmr_work_main_id \t\t : mr_work_main_id,
\t\t\t\t\tmr_type_work_id \t\t : mr_type_work_id,
\t\t\t\t\tmr_work_inout_id \t\t : mr_work_inout_id,
\t\t\t\t\tmr_status_id \t\t \t : mr_status_id,
\t\t\t\t\tfloor \t\t\t\t\t : floor,
\t\t\t\t\tfloor2 \t\t\t\t\t : floor2,
\t\t\t\t\tfloor2_text \t\t\t: floor2_text,
\t\t\t\t\ttype \t\t\t\t\t : \"mailrommedit\"
\t\t\t\t},
\t\t\t\tfunction(res) {
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t}
\t\t\t\t\talert(res.message);
\t\t\t\t\t
\t\t\t\t},'json');
\t\t\t}
\t\t});
\t\t
";
    }

    // line 87
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 88
        echo "\t\t\t\t
";
    }

    // line 91
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        echo "
\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้รับเอกสาร </h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"mr_work_inout_id\" value=\"";
        // line 98
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_work_inout_id", [], "any", false, false, false, 98), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"mr_work_main_id\" value=\"";
        // line 99
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_work_main_id", [], "any", false, false, false, 99), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"mr_status_id\" value=\"";
        // line 100
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_status_id", [], "any", false, false, false, 100), "html", null, true);
        echo "\">
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tประเภทการส่ง :
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_type_work_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t<option value=\"1\" ";
        // line 108
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 108) == 1)) {
            echo " selected=\"selected\" ";
        }
        echo ">รับส่งภายใน</option>\t\t\t\t
\t\t\t\t\t\t\t\t\t<option value=\"2\" ";
        // line 109
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 109) == 2)) {
            echo " selected=\"selected\" ";
        }
        echo ">ส่งที่สาขา</option>\t\t\t\t
\t\t\t\t\t\t\t\t\t<option value=\"3\" ";
        // line 110
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 110) == 3)) {
            echo " selected=\"selected\" ";
        }
        echo ">ส่งที่สำนักงานใหญ่</option>\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"div_branch_id\" ";
        // line 116
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 116) == 2)) {
            echo " ";
        } else {
            echo "style=\"display:none;\" ";
        }
        echo ">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tสาขาปลายทาง :
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
        // line 124
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 124) == "")) {
            // line 125
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">ไม่มี</option>
\t\t\t\t\t\t\t\t";
        }
        // line 126
        echo "\t\t
\t\t\t\t\t\t\t\t";
        // line 127
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 128
            echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<option value=\"";
            // line 129
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 129), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 129) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_id", [], "any", false, false, false, 129))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_code", [], "any", false, false, false, 129), "html", null, true);
            echo ":";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 129), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "\t\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t
\t
\t
\t
\t
\t
\t
\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"";
        // line 149
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "emp_code", [], "any", false, false, false, 149), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"";
        // line 160
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "name_re", [], "any", false, false, false, 160), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"";
        // line 171
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "lastname_re", [], "any", false, false, false, 171), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"";
        // line 181
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "tel_re", [], "any", false, false, false, 181), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"";
        // line 191
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mobile_re", [], "any", false, false, false, 191), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"";
        // line 201
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_email", [], "any", false, false, false, 201), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tแผนก :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\" disabled>
\t\t\t\t\t\t\t";
        // line 213
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 214
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 214), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 214) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_id", [], "any", false, false, false, 214))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_code", [], "any", false, false, false, 214), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_name", [], "any", false, false, false, 214), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 215
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px ;\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div id=\"floor_ho\" class=\"col-8\" style=\"padding-left:0px;  ";
        // line 226
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 226) != 2)) {
            echo " ";
        } else {
            echo "display:none; ";
        }
        echo " \">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t";
        // line 228
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_floor_id", [], "any", false, false, false, 228) == "")) {
            // line 229
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">ไม่มี</option>
\t\t\t\t\t\t\t";
        }
        // line 230
        echo "\t\t
\t\t\t\t\t\t\t";
        // line 231
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
            // line 232
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 232), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 232) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_floor_id", [], "any", false, false, false, 232))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "name", [], "any", false, false, false, 232), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 233
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div id=\"floor_branch\" class=\"col-8\" style=\"padding-left:0px;   ";
        // line 236
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_type_work_id", [], "any", false, false, false, 236) == 2)) {
            echo " ";
        } else {
            echo "display:none; ";
        }
        echo "\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor2\" style=\"width:100%;\">
\t\t\t\t\t\t\t";
        // line 238
        if ((twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_floor_id", [], "any", false, false, false, 238) == "")) {
            // line 239
            echo "\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_branch_floor", [], "any", false, false, false, 239), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        // line 241
        echo "\t\t\t\t\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data2"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
            // line 242
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 242), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 242) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_floor_id", [], "any", false, false, false, 242))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_mame", [], "any", false, false, false, 242), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 243
        echo "\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึกการแก้ไขชั้น</button>
\t\t\t\t<br>
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

";
    }

    // line 266
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 268
        if ((($context["debug"] ?? null) != "")) {
            // line 269
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 271
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/edit_floor_send.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  519 => 271,  513 => 269,  511 => 268,  504 => 266,  480 => 243,  465 => 242,  460 => 241,  454 => 239,  452 => 238,  443 => 236,  438 => 233,  423 => 232,  419 => 231,  416 => 230,  412 => 229,  410 => 228,  401 => 226,  388 => 215,  371 => 214,  367 => 213,  352 => 201,  339 => 191,  326 => 181,  313 => 171,  299 => 160,  285 => 149,  264 => 130,  248 => 129,  245 => 128,  241 => 127,  238 => 126,  234 => 125,  232 => 124,  217 => 116,  206 => 110,  200 => 109,  194 => 108,  183 => 100,  179 => 99,  175 => 98,  167 => 92,  163 => 91,  158 => 88,  154 => 87,  72 => 9,  69 => 8,  65 => 7,  58 => 5,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
\t\t
\t\t{{alert}}
\t\t
\t\t\$(\"#mr_type_work_id\").change(function(){
\t\t\tvar type  = \$(this).val();
\t\t\tif(type == 2){
\t\t\t\t\$('#div_branch_id').show();
\t\t\t\t\$('#floor_branch').show();
\t\t\t\t\$('#floor_ho').hide();
\t\t\t}else{
\t\t\t\t\$('#div_branch_id').hide();
\t\t\t\t\$('#floor_branch').hide();
\t\t\t\t\$('#floor_ho').show();
\t\t\t}
\t\t});

\t\t\$(\"#mr_branch_id\").change(function(){
\t\t\tvar type  = \$(\"#mr_type_work_id\").val()
\t\t\tvar branch_id  = \$(this).val();
\t\t\t\$.ajax({
\t\t\t\turl: 'ajax/ajax_autocompress_chang_branch.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tbranch_id: branch_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tif(res.status == 200) {
\t\t\t\t\t\t\$('#floor2').html(res.data);
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t}
\t\t\t})
\t\t\tconsole.log(type);
\t\t});


\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar floor \t\t\t\t\t\t\t= \$(\"#floor\").val();
\t\t\tvar floor2 \t\t\t\t\t\t\t= \$(\"#floor2\").val();
\t\t\tvar mr_branch_id \t\t\t\t= \$(\"#mr_branch_id\").val();
\t\t\tvar mr_work_inout_id \t\t\t\t= \$(\"#mr_work_inout_id\").val();
\t\t\tvar mr_type_work_id \t\t\t\t= \$(\"#mr_type_work_id\").val();
\t\t\tvar mr_work_main_id \t\t\t\t= \$(\"#mr_work_main_id\").val();
\t\t\tvar mr_status_id \t\t\t\t\t= \$(\"#mr_status_id\").val();
\t\t\tvar data \t\t\t\t\t\t\t= \$('#floor2').select2('data');
\t\t\t
\t\t\tif(\$('#floor2').val()){
\t\t\t\tvar floor2_text \t\t\t\t\t= data[0].text;
\t\t\t}

\t\t\tif(mr_type_work_id == 2 && mr_branch_id == 0){
\t\t\t\talert('กรุณาระบุสาขาปลายทาง');
\t\t\t\treturn;
\t\t\t}else{
\t\t\t\t\$.post(
\t\t\t\t'./ajax/ajax_editFloorSend.php',
\t\t\t\t{
\t\t\t\t\tmr_branch_id \t\t : mr_branch_id,
\t\t\t\t\tmr_work_main_id \t\t : mr_work_main_id,
\t\t\t\t\tmr_type_work_id \t\t : mr_type_work_id,
\t\t\t\t\tmr_work_inout_id \t\t : mr_work_inout_id,
\t\t\t\t\tmr_status_id \t\t \t : mr_status_id,
\t\t\t\t\tfloor \t\t\t\t\t : floor,
\t\t\t\t\tfloor2 \t\t\t\t\t : floor2,
\t\t\t\t\tfloor2_text \t\t\t: floor2_text,
\t\t\t\t\ttype \t\t\t\t\t : \"mailrommedit\"
\t\t\t\t},
\t\t\t\tfunction(res) {
\t\t\t\t\tif(res.status == 200){
\t\t\t\t\t\tlocation.reload();
\t\t\t\t\t}
\t\t\t\t\talert(res.message);
\t\t\t\t\t
\t\t\t\t},'json');
\t\t\t}
\t\t});
\t\t
{% endblock %}\t\t\t\t
{% block javaScript %}
\t\t\t\t
{% endblock %}\t\t\t\t\t
\t\t\t\t
{% block Content %}

\t
\t<div class=\"container\">
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้รับเอกสาร </h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"mr_work_inout_id\" value=\"{{ user_data.mr_work_inout_id }}\">
\t\t\t <input type=\"hidden\" id=\"mr_work_main_id\" value=\"{{ user_data.mr_work_main_id }}\">
\t\t\t <input type=\"hidden\" id=\"mr_status_id\" value=\"{{ user_data.mr_status_id }}\">
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tประเภทการส่ง :
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_type_work_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t<option value=\"1\" {% if user_data.mr_type_work_id == 1 %} selected=\"selected\" {% endif %}>รับส่งภายใน</option>\t\t\t\t
\t\t\t\t\t\t\t\t\t<option value=\"2\" {% if user_data.mr_type_work_id == 2 %} selected=\"selected\" {% endif %}>ส่งที่สาขา</option>\t\t\t\t
\t\t\t\t\t\t\t\t\t<option value=\"3\" {% if user_data.mr_type_work_id == 3 %} selected=\"selected\" {% endif %}>ส่งที่สำนักงานใหญ่</option>\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"form-group\" id=\"div_branch_id\" {% if user_data.mr_type_work_id == 2 %} {% else %}style=\"display:none;\" {% endif %}>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\t\tสาขาปลายทาง :
\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_branch_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t{% if user_data.mr_branch_id == '' %}
\t\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">ไม่มี</option>
\t\t\t\t\t\t\t\t{% endif %}\t\t
\t\t\t\t\t\t\t\t{% for b in branch %}
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<option value=\"{{ b.mr_branch_id }}\" {% if b.mr_branch_id == user_data.mr_branch_id %} selected=\"selected\" {% endif %}>{{ b.mr_branch_code }}:{{ b.mr_branch_name }}</option>
\t\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t
\t
\t
\t
\t
\t
\t
\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"{{ user_data.emp_code }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"{{ user_data.name_re }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"{{ user_data.lastname_re }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"{{ user_data.tel_re }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"{{ user_data.mobile_re }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"{{ user_data.mr_emp_email }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tแผนก :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\" disabled>
\t\t\t\t\t\t\t{% for s in department_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_department_id }}\" {% if s.mr_department_id == user_data.mr_department_id %} selected=\"selected\" {% endif %}>{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px ;\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div id=\"floor_ho\" class=\"col-8\" style=\"padding-left:0px;  {% if user_data.mr_type_work_id != 2 %} {% else %}display:none; {% endif %} \">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t{% if user_data.mr_floor_id == '' %}
\t\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">ไม่มี</option>
\t\t\t\t\t\t\t{% endif %}\t\t
\t\t\t\t\t\t\t{% for f in floor_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ f.mr_floor_id }}\" {% if f.mr_floor_id == user_data.mr_floor_id %} selected=\"selected\" {% endif %}>{{ f.name }}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div id=\"floor_branch\" class=\"col-8\" style=\"padding-left:0px;   {% if user_data.mr_type_work_id == 2 %} {% else %}display:none; {% endif %}\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor2\" style=\"width:100%;\">
\t\t\t\t\t\t\t{% if user_data.mr_floor_id == '' %}
\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">{{user_data.mr_branch_floor}}</option>
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t{% for f in floor_data2 %}
\t\t\t\t\t\t\t\t<option value=\"{{ f.mr_floor_id }}\" {% if f.mr_floor_id == user_data.mr_floor_id %} selected=\"selected\" {% endif %}>{{ f.mr_floor_mame }}</option>
\t\t\t\t\t\t\t{% endfor %}\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึกการแก้ไขชั้น</button>
\t\t\t\t<br>
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t
\t
\t</div>
\t

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/edit_floor_send.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\mailroom\\edit_floor_send.tpl");
    }
}

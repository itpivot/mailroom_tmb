<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/send_branch_list.tpl */
class __TwigTemplate_c9e120bc80b49472f5a7055dda7a47a1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg3' => [$this, 'block_menu_msg3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/send_branch_list.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PivotSend List ";
    }

    // line 5
    public function block_menu_msg3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
";
    }

    // line 29
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo ".modal-dialog {
      display: flex;
      align-items: center;
      justify-content: center;
      min-height: calc(100vh - 1rem); /* Adjust if needed */
    }
    .modal-body {
   
      flex-direction: column;
      align-items: center;
      justify-content: center;
      text-align: center;
    }
   
    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
\t#loading{
\t\t\tdisplay: block;
\t\t\tmargin-left: auto;
\t\t\tmargin-right: auto;
\t\t\twidth: 50%;
\t\t
\t}
    
";
    }

    // line 139
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 140
        echo "    \$('.block_btn').hide();

    var receive_list = {};
    getSendData();

    

";
    }

    // line 149
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 150
        echo "    var selectChoice = [];

    function getSendData(branch_id)
    {
        if(!branch_id){ 
            \$('.block_btn').hide();
        }

        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: \"GET\",
            cache: false,
            data: {
                type: 'send',
                branch_id: branch_id
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            },
            complete: function() {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }


    function confirmApprove()
    {
        var wId = \$('#hidden-id').val();
        var data = {
            type: 'one', 
            action: 1,
            main_id: wId
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function confirmCancel(wId) 
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel',
                wId: wId
            },
            dataType: 'json',
            success: function (resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })

    }

    function checkChoice() {

        \$('.check_all').each(function (i, elm) {
            var wId = parseInt(\$('#' + elm.id).attr('data-value'));
            var foundArr = selectChoice.indexOf(wId);

  
            \$(elm).prop('checked', !elm.checked);
    
            if (elm.checked) {
                if (foundArr == -1) {
                    selectChoice.push(wId)
                } 
            } else {
                if (foundArr != -1) {
                    selectChoice.splice(foundArr, 1);
                }
            }
        });
       
        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }

    }

    function selectedCard(wId, elm)
    {
        var foundArr = selectChoice.indexOf(wId);
        if(\$('#'+elm.id).is(':checked')) {
            if (foundArr == -1) {
                selectChoice.push(wId)
            }
        } else {
            if (foundArr != -1) {
                selectChoice.splice(foundArr, 1);
            }
        }

        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }
        console.log(selectChoice)
        \$('#hidden-id').val(selectChoice);
    }

    function sendAll()
    {
        var data = {
            type: 'all', 
            action: 1,
            wId: selectChoice
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function cancelAll()
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel_all',
                wId: JSON.stringify(selectChoice)
            },
            dataType: 'json',
            success: function(resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })
    }
    function showdatatosend(id) {
        \$('#hidden-id').val(id);
        \$('#modal-update').modal({
          keyboard: false, 
          backdrop: 'static' 
        });
    }
    function showdatatosendall() {
        \$('#modal-update-all').modal({
          keyboard: false, 
          backdrop: 'static' 
        });
    }
    function uploadimage(){
        var wId = \$('#hidden-id').val();
        var data = {
            type: 'one', 
            action: 1,
            main_id: wId
        };
       location.href = \"confirmSendBranch_no.php?\" + \$.param(data);
    }
    function uploadimageall(){
        var data = {
            type: 'all', 
            action: 1,
            main_id: selectChoice
        };
       location.href = \"confirmSendBranch_no.php?\" + \$.param(data);
    }
";
    }

    // line 341
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 342
        echo "<div class=\"content_bch\">
<h4 class=\"text-center text-muted\">ส่งเอกสารที่สาขา </h4>
    <div class=\"header_bch\">
        <div class=\"form-group\">
            <p class=\"font-weight-bold text-muted\">ทั้งหมด <span id=\"counter_works\">0</span> งาน </p>
            <select name=\"lst_branch\" id=\"lst_branch\" class=\"form-control\" onchange=\"getSendData(this.value);\">
                <option value=\"\">-- เลือกสาขา --</option>
                ";
        // line 349
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 350
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 350), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 350), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 352
        echo "            </select>
        </div>
    </div>
    <button type=\"button\" class=\"btn btn-secondary btn-block my-2\" id=\"btn_checked\" onclick=\"checkChoice();\">เลือกทั้งหมด</button>
    <div class=\"block_btn my-2\">
        ";
        // line 358
        echo "        <button type=\"button\" id=\"btn_receive\" class=\"btn btn-success btn-block\" onclick=\"showdatatosendall();\">ส่งเอกสาร</button> 
        <button type=\"button\" id=\"btn_not_found\" class=\"btn btn-warning btn-block\" onclick=\"cancelAll();\">ไม่พบผู้รับ</button>
    </div>
\t<img id=\"loading\" src=\"../themes/images/loading.gif\">
    <div class=\"result_bch\">
        
    </div>
</div>
<div class=\"footer_bch\">
    
</div>

";
        // line 371
        echo "<div class=\"modal fade\" id=\"modal-update\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLabel\">เลือกวิธีส่งงาน</h5>
\t\t</div>
\t\t<input type=\"hidden\" id=\"hidden-id\" value=\"\">
\t\t<div class=\"modal-body\">
        <button type=\"button\" class=\"btn btn-primary\" data-bs-dismiss=\"modal\" onclick=\"confirmApprove();\">ลงชื่อเข้ารับ</button>
        <button type=\"button\" class=\"btn btn-secondary\" onclick=\"uploadimage();\">
\t\t\tอัพโหลดใบคุม
\t\t</button>
\t\t</div>
\t  </div>
\t</div>
  </div>
";
        // line 388
        echo "<div class=\"modal fade\" id=\"modal-update-all\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLabel\">เลือกวิธีส่งงาน</h5>
\t\t</div>
\t\t<input type=\"hidden\" id=\"hidden-id-all\" value=\"\">
\t\t<div class=\"modal-body\">
        <button type=\"button\" class=\"btn btn-primary\" data-bs-dismiss=\"modal\" onclick=\"sendAll();\">ลงชื่อเข้ารับ</button>
        <button type=\"button\" class=\"btn btn-secondary\" onclick=\"uploadimageall();\">
\t\t\tอัพโหลดใบคุม
\t\t</button>
\t\t</div>
\t  </div>
\t</div>
  </div>

";
    }

    // line 408
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 410
        if ((($context["debug"] ?? null) != "")) {
            // line 411
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 413
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/send_branch_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  524 => 413,  518 => 411,  516 => 410,  509 => 408,  488 => 388,  470 => 371,  456 => 358,  449 => 352,  438 => 350,  434 => 349,  425 => 342,  421 => 341,  228 => 150,  224 => 149,  213 => 140,  209 => 139,  98 => 30,  94 => 29,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>
{% endblock %}

{% block styleReady %}
.modal-dialog {
      display: flex;
      align-items: center;
      justify-content: center;
      min-height: calc(100vh - 1rem); /* Adjust if needed */
    }
    .modal-body {
   
      flex-direction: column;
      align-items: center;
      justify-content: center;
      text-align: center;
    }
   
    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
\t#loading{
\t\t\tdisplay: block;
\t\t\tmargin-left: auto;
\t\t\tmargin-right: auto;
\t\t\twidth: 50%;
\t\t
\t}
    
{% endblock %}

{% block domReady %}
    \$('.block_btn').hide();

    var receive_list = {};
    getSendData();

    

{% endblock %}

{% block javaScript %}
    var selectChoice = [];

    function getSendData(branch_id)
    {
        if(!branch_id){ 
            \$('.block_btn').hide();
        }

        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: \"GET\",
            cache: false,
            data: {
                type: 'send',
                branch_id: branch_id
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            },
            complete: function() {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }


    function confirmApprove()
    {
        var wId = \$('#hidden-id').val();
        var data = {
            type: 'one', 
            action: 1,
            main_id: wId
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function confirmCancel(wId) 
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel',
                wId: wId
            },
            dataType: 'json',
            success: function (resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })

    }

    function checkChoice() {

        \$('.check_all').each(function (i, elm) {
            var wId = parseInt(\$('#' + elm.id).attr('data-value'));
            var foundArr = selectChoice.indexOf(wId);

  
            \$(elm).prop('checked', !elm.checked);
    
            if (elm.checked) {
                if (foundArr == -1) {
                    selectChoice.push(wId)
                } 
            } else {
                if (foundArr != -1) {
                    selectChoice.splice(foundArr, 1);
                }
            }
        });
       
        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }

    }

    function selectedCard(wId, elm)
    {
        var foundArr = selectChoice.indexOf(wId);
        if(\$('#'+elm.id).is(':checked')) {
            if (foundArr == -1) {
                selectChoice.push(wId)
            }
        } else {
            if (foundArr != -1) {
                selectChoice.splice(foundArr, 1);
            }
        }

        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }
        console.log(selectChoice)
        \$('#hidden-id').val(selectChoice);
    }

    function sendAll()
    {
        var data = {
            type: 'all', 
            action: 1,
            wId: selectChoice
        };
       location.href = \"confirmSendBranch.php?\" + \$.param(data);
    }

    function cancelAll()
    {
        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: 'POST',
            data: {
                type: 'cancel_all',
                wId: JSON.stringify(selectChoice)
            },
            dataType: 'json',
            success: function(resp) {
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            }
        })
    }
    function showdatatosend(id) {
        \$('#hidden-id').val(id);
        \$('#modal-update').modal({
          keyboard: false, 
          backdrop: 'static' 
        });
    }
    function showdatatosendall() {
        \$('#modal-update-all').modal({
          keyboard: false, 
          backdrop: 'static' 
        });
    }
    function uploadimage(){
        var wId = \$('#hidden-id').val();
        var data = {
            type: 'one', 
            action: 1,
            main_id: wId
        };
       location.href = \"confirmSendBranch_no.php?\" + \$.param(data);
    }
    function uploadimageall(){
        var data = {
            type: 'all', 
            action: 1,
            main_id: selectChoice
        };
       location.href = \"confirmSendBranch_no.php?\" + \$.param(data);
    }
{% endblock %}

{% block Content %}
<div class=\"content_bch\">
<h4 class=\"text-center text-muted\">ส่งเอกสารที่สาขา </h4>
    <div class=\"header_bch\">
        <div class=\"form-group\">
            <p class=\"font-weight-bold text-muted\">ทั้งหมด <span id=\"counter_works\">0</span> งาน </p>
            <select name=\"lst_branch\" id=\"lst_branch\" class=\"form-control\" onchange=\"getSendData(this.value);\">
                <option value=\"\">-- เลือกสาขา --</option>
                {% for b in branch %}
                    <option value=\"{{ b.mr_branch_id }}\">{{ b.mr_branch_name }}</option>
                {% endfor %}
            </select>
        </div>
    </div>
    <button type=\"button\" class=\"btn btn-secondary btn-block my-2\" id=\"btn_checked\" onclick=\"checkChoice();\">เลือกทั้งหมด</button>
    <div class=\"block_btn my-2\">
        {# onclick=\"sendAll();\" #}
        <button type=\"button\" id=\"btn_receive\" class=\"btn btn-success btn-block\" onclick=\"showdatatosendall();\">ส่งเอกสาร</button> 
        <button type=\"button\" id=\"btn_not_found\" class=\"btn btn-warning btn-block\" onclick=\"cancelAll();\">ไม่พบผู้รับ</button>
    </div>
\t<img id=\"loading\" src=\"../themes/images/loading.gif\">
    <div class=\"result_bch\">
        
    </div>
</div>
<div class=\"footer_bch\">
    
</div>

{# modal #}
<div class=\"modal fade\" id=\"modal-update\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLabel\">เลือกวิธีส่งงาน</h5>
\t\t</div>
\t\t<input type=\"hidden\" id=\"hidden-id\" value=\"\">
\t\t<div class=\"modal-body\">
        <button type=\"button\" class=\"btn btn-primary\" data-bs-dismiss=\"modal\" onclick=\"confirmApprove();\">ลงชื่อเข้ารับ</button>
        <button type=\"button\" class=\"btn btn-secondary\" onclick=\"uploadimage();\">
\t\t\tอัพโหลดใบคุม
\t\t</button>
\t\t</div>
\t  </div>
\t</div>
  </div>
{# modalall #}
<div class=\"modal fade\" id=\"modal-update-all\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-dialog-centered\">
\t  <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t  <h5 class=\"modal-title\" id=\"exampleModalLabel\">เลือกวิธีส่งงาน</h5>
\t\t</div>
\t\t<input type=\"hidden\" id=\"hidden-id-all\" value=\"\">
\t\t<div class=\"modal-body\">
        <button type=\"button\" class=\"btn btn-primary\" data-bs-dismiss=\"modal\" onclick=\"sendAll();\">ลงชื่อเข้ารับ</button>
        <button type=\"button\" class=\"btn btn-secondary\" onclick=\"uploadimageall();\">
\t\t\tอัพโหลดใบคุม
\t\t</button>
\t\t</div>
\t  </div>
\t</div>
  </div>

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}

", "messenger/send_branch_list.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\messenger\\send_branch_list.tpl");
    }
}

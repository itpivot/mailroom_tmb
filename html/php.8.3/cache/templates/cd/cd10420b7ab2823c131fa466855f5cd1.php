<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base_emp2.tpl */
class __TwigTemplate_d96ef973e2ec77e0c06fa9e1c1e02746 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'title' => [$this, 'block_title'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'Content' => [$this, 'block_Content'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
            'domReady' => [$this, 'block_domReady'],
            'domReady2' => [$this, 'block_domReady2'],
            'javaScript' => [$this, 'block_javaScript'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>

<!-- <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> -->
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" >
<head>
  ";
        // line 6
        $this->displayBlock('head', $context, $blocks);
        // line 54
        echo "  <style type=\"text/css\">
  ";
        // line 55
        $this->displayBlock('styleReady', $context, $blocks);
        // line 56
        echo "  nav#menu {
\tbackground-image: -ms-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: -moz-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: -o-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: -webkit-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
  }
  .logout{
\tborder-top: 1px solid #0266bb;
    margin-left:20px;
    margin-right:50px;
  }
  
  .list-group-item {
    padding: .25rem 1.25rem; */
    margin-bottom: -1px;
\tborder: 0px solid rgba(0,0,0,.125);
\t}
\t
\t.material-icons {
\t\tvertical-align: middle;
\t\tdisplay: inline; 
\t}
\t
\t.btn-success span {
\t\tvertical-align: middle;
\t}
\t
\t.status {
\t\tborder: 1px solid #e0e0e0;
\t\tpadding: 5px;
\t\tpadding-left: 10px;
\t\tpadding-right: 10px;
\t\tborder-radius: 15px;
\t}
\t


\t.slideout-menu {
\t\tposition: fixed;
\t\ttop: 0;
\t\tbottom: 0;
\t\twidth: 290px;
\t\tmin-height: 100vh;
\t\toverflow-y: scroll;
\t\t-webkit-overflow-scrolling: touch;
\t\tz-index: 0;
\t\tdisplay: none;
\t}
\t
\t.slideout-menu-left {
\t\tleft: 0;
\t}
\t
\t.slideout-menu-right {
\t\tright: 0;
\t}
\t
\t.slideout-panel {
\t\tz-index: 1;
\t\t/* position: relative;
\t\twill-change: transform; */
\t\tbackground-color: #FFF; /* A background-color is required */
\t\tmin-height: 100vh;
\t}
\t
\t.slideout-open,
\t.slideout-open body,
\t.slideout-open .slideout-panel {
\t\toverflow: hidden;
\t}
\t
\t.slideout-open .slideout-menu {
\t\tdisplay: block;
\t}
\t
\t.panel-header{
\t\theight: 50px;
\t\tmargin-bottom: 20px;
\t\tcolor:\twhite;
\t\ttext-align: center;
\t\t//box-shadow: 2px 2px 1px 1px rgba(50,50,50,.4);
\t}
\t
\t
\t.js-slideout-toggle{
\t\tmargin: 13px 5px;
\t\tposition: absolute;
\t\tleft: 10px;
\t}
\t 
\t
\t.header-text {
\t\tmargin-top: 8px;
\t\tfont-size: 25px;
\t\tfont-weight:bold;
\t\tcolor: #696969;
\t\ttext-shadow: white 0.1em 0.1em 0.1em;
\t}

\t
\t@media screen and (max-width: 1080px) {
\t\t.header-text {
\t\t\tmargin-top: 13px;
\t\t\tfont-size: 18px;
\t\t\tfont-weight:bold;
\t\t\tcolor: #696969;
\t\t\ttext-shadow: white 0.1em 0.1em 0.1em;
\t\t}
\t}
  </style>
</head>
<body>
<!--Start Header-->




<nav id=\"menu\">  <!-- style=\"background-color: #0266bb;\" -->
\t\t\t\t
            <ul class=\"sidebar-nav\">
                <li class=\"sidebar-brand\">
                    <a href=\"#\">
\t\t\t\t\t\t";
        // line 179
        if ((($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Employee", [], "any", false, false, false, 179))) {
            // line 180
            echo "\t\t\t\t\t\t\t<a href=\"../data/profile.php\"><span class=\"white-text name\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 180), "html", null, true);
            echo "</span></a>
\t\t\t\t\t\t";
        } elseif ((        // line 181
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Messenger", [], "any", false, false, false, 181))) {
            echo "\t
\t\t\t\t\t\t\t<a href=\"../data/profile.php\"><span class=\"white-text name\">";
            // line 182
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 182), "html", null, true);
            echo "</span></a>
\t\t\t\t\t\t";
        } elseif ((        // line 183
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "MessengerBranch", [], "any", false, false, false, 183))) {
            echo "\t
\t\t\t\t\t\t\t<a href=\"../messenger/profile.php\"><span class=\"white-text name\">";
            // line 184
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 184), "html", null, true);
            echo "</span></a>
\t\t\t\t\t\t";
        } elseif ((        // line 185
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "MessengerHO", [], "any", false, false, false, 185))) {
            echo "\t
\t\t\t\t\t\t\t<a href=\"../messenger/profile.php\"><span class=\"white-text name\">";
            // line 186
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 186), "html", null, true);
            echo "</span></a>
\t\t\t\t\t\t";
        } else {
            // line 188
            echo "\t\t\t\t\t\t\t<span class=\"white-text name\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_username", [], "any", false, false, false, 188), "html", null, true);
            echo "</span>
\t\t\t\t\t\t";
        }
        // line 190
        echo "                    </a>
                </li>
                ";
        // line 192
        if ((($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Administrator", [], "any", false, false, false, 192))) {
            // line 193
            echo "\t\t\t\t\t
\t\t\t\t\t<li><a href=\"../mailroom/search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/user.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">group</i>แก้ปัญหา User </a></li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle='collapse' href='#collapse_mailroom'><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ประเมิณผล<i class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t<div id='collapse_mailroom' class='panel-collapse collapse'>
\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary.php\"><i class=\"material-icons\">keyboard_arrow_right</i> ผู้รับ</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_sender.php\"><i class=\"material-icons\">keyboard_arrow_right</i> ผู้ส่ง</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">description</i>รายงาน<i class=\"material-icons\">arrow_drop_down</i> </a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send.php\"> รายงานรับส่งเอกสาร(รายเดือน)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send_daily.php\">รายงานรับส่งเอกสาร(รายวัน)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"../manager/report_sla.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assignment_turned_in</i>รายงาน SLA </a></li>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t
\t\t\t\t";
        } elseif ((        // line 217
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Employee", [], "any", false, false, false, 217))) {
            // line 218
            echo "\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">content_paste</i>การสั่งงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/profile_check.php?type=1\">ปลายทาง => สำนักงานใหญ่</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/profile_check.php?type=2\">ปลายทาง => สาขา</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/work_byhand.php\">By Hand</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/work_post.php\">ส่งไปรษณีย์</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/create_work_import_excel.php\">Import Excel</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- <li class=\"list-group-item\" ><a href=\"../employee/create_work_byHand.php\">ไปรษณีย์&&Byhand(Pivot)</a></li> -->
\t\t\t\t\t\t\t\t<!-- <li class=\"list-group-item\" ><a href=\"work_out.php\">ส่งภายนอก</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_byhand.php\">By Hand(เอกสารถึงลูกค้า)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_post.php\">ส่งไปรษณีย์</a></li> -->
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t<!-- <li><a href=\"#\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  รับเอกสาร (Inbox) </a></li> -->
\t\t\t\t\t<li><a href=\"../employee/receive_work.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assignment_return</i> รับเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../employee/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  ติดตามเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../employee/search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li><a href=\"../employee/search_detail.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ค้นหาแบบละเอียด </a></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse2\" ><i class=\"material-icons\" style=\"padding-right:5px;\">description</i>รายงาน<i class=\"material-icons\">arrow_drop_down</i> </a></li>
\t\t\t\t\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/report_send.php\">รายงานการส่งออก</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/report_receive.php\">รายงานการรับเข้า</a></li>
\t\t\t\t\t\t\t<!-- \t<li class=\"list-group-item\" ><a href=\"work_out.php\">ส่งภายนอก</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_byhand.php\">By Hand(เอกสารถึงลูกค้า)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_post.php\">ส่งไปรษณีย์</a></li> -->
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<span class=\"permition_branch_menage\" style=\"display: none;\">
\t\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t\t<li ><a href=\"../mailroom/report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน</a></li>
\t\t\t\t\t\t<li ><a href=\"../employee/search_branch_menage.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>รายงาน(บริหารสาขา)</a></li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li ><a href=\"../mailroom/report.byhand.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>รายงาน By Hand</a></li>
\t\t\t\t\t\t<li ><a href=\"../mailroom/report.post.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน ปณ.</a></li>
\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.payment_report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>Payment-Report</a></li>
\t\t\t\t\t\t<li ><a href=\"../mailroom/access_log.php\"><i class=\"material-icons\">group</i>&nbsp;&nbsp;ประวัติการเข้าใช้งาน</a></li>
\t\t\t\t\t\t
\t\t\t\t\t</span>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../data/profile.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">perm_contact_calendar</i>แก้ไขข้อมูลส่วนตัว</a></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t";
        } elseif ((        // line 264
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "MessengerBranch", [], "any", false, false, false, 264))) {
            // line 265
            echo "\t\t\t\t\t\t<li><a href=\"../messenger/news.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">home</i>หน้าแรก</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#receive_branch'><i class=\"material-icons\" style=\"padding-right:5px;\">directions_bike</i>เข้ารับเอกสาร<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='receive_branch' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_list.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">transfer_within_a_station</i>รับเอกสารจากสาขา</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_mixed.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">library_add</i>รวมเอกสารส่ง
\t\t\t\t\t\t\t\t\t\t\tMailroom</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#receive_mailroom'><i class=\"material-icons\" style=\"padding-right:5px;\">domain</i>รับเอกสารจาก Mailroom<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='receive_mailroom' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_mailroom_to_branch.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">domain</i>รับเอกสารส่งสาขา</a> </li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_mailroom_to_byhand.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">motorcycle</i>รับเอกสาร By Hand</a> </li>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li><a href=\"../messenger/receive_hub.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">store_mall_directory</i>รับเอกสารจาก Hub</a> </li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#send_work'><i class=\"material-icons\" style=\"padding-right:5px;\">send</i>ส่งเอกสาร<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='send_work' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/send_branch_list.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">smartphone</i>ส่งเอกสาร สาขา</a> </li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/send_work_byhand.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">directions_bike</i>ส่งเอกสาร By Hand</a> </li>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<!-- <li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li> -->
\t\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t\t<li><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t
\t\t\t\t";
        } elseif ((        // line 305
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Messenger", [], "any", false, false, false, 305))) {
            // line 306
            echo "\t\t\t\t\t<li><a href=\"../messenger/news.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">home</i>หน้าแรก</a></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_left</i>รับเอกสาร <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workByhand.php\">รับเอกสาร(Byhand)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workPost.php\">รับเอกสาร(ป.ณ.)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse2\" ><i class=\"material-icons\" style=\"padding-right:5px;\">compare_arrows</i>รับเอกสาร Mailroom <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_IN.php\">ป.ณ.ขาเข้า</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_Byhand_IN.php\">Byhand ขาเข้า</a></li>

\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_right</i>ส่งเอกสาร<i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list.php\">ส่งเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_thai_post_in.php\">ป.ณ. ขาเข้า</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_byhand_in.php\">Byhand ขาเข้า</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t<li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t";
        } elseif ((        // line 336
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "MessengerHO", [], "any", false, false, false, 336))) {
            echo "\t
\t\t\t\t\t<li><a href=\"../messenger/news.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">home</i>หน้าแรก</a></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_left</i>รับเอกสาร <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workByhand.php\">รับเอกสาร(Byhand)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workPost.php\">รับเอกสาร(ป.ณ.)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse2\" ><i class=\"material-icons\" style=\"padding-right:5px;\">compare_arrows</i>รับเอกสาร Mailroom <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_IN.php\">ป.ณ.ขาเข้า</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_Byhand_IN.php\">Byhand ขาเข้า</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_right</i>ส่งเอกสาร<i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list.php\">ส่งเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_thai_post_in.php\">ป.ณ. ขาเข้า</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_byhand_in.php\">Byhand ขาเข้า</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li>
\t\t\t\t\t
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#receive_branch'><i class=\"material-icons\" style=\"padding-right:5px;\">directions_bike</i>เข้ารับเอกสารจากสาขา<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='receive_branch' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_list.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">transfer_within_a_station</i>รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_mixed.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">library_add</i>รวมเอกสารส่ง
\t\t\t\t\t\t\t\t\t\t\tMailroom</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li><a href=\"../messenger/receive_mailroom_to_branch.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">domain</i>รับเอกสารจาก
\t\t\t\t\t\t\t\tMailroom</a></li>
\t\t\t\t\t\t<li><a href=\"../messenger/receive_hub.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">store_mall_directory</i>รับเอกสารจาก
\t\t\t\t\t\t\t\tHub</a></li>
\t\t\t\t\t\t<li><a href=\"../messenger/send_branch_list.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">send</i>ส่งเอกสารที่สาขา</a></li>

\t\t\t\t\t\t<!-- <li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li> -->
\t\t\t\t\t\t

\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:5px;\"> <i class=\"material-icons\">power_settings_new</i>Logout</a></li>

\t\t\t\t";
        } elseif ((        // line 390
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Mailroom", [], "any", false, false, false, 390))) {
            // line 391
            echo "\t\t\t\t<!-- สั่งงาน -->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle=\"collapse\" href=\"#collapsesend\" ><i class=\"material-icons\" style=\"padding-right:5px;\">content_paste</i>การสั่งงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t</li>
\t\t\t\t\t<div id=\"collapsesend\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_post_out.php\">ส่งออก->ไปรษณีย์</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_byHand_out.php\">ส่งออก->Byhand</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_return.php\">ส่งออก->คืนสาขา</a></li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_post_in.php\">งานรับเข้า->ไปรษณีย์</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_byHand_in.php\">งานรับเข้า->Byhand</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_other_in.php\">งานรับเข้า->จากบุคคลภายนอก</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>\t
\t\t\t\t\t<!-- <li><a href=\"#\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  รับเอกสาร (Inbox) </a></li> -->

\t\t\t\t\t<!-- รับเอกสาร -->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle=\"collapse\" href=\"#collapsesend_resive\" ><i class=\"material-icons\" style=\"padding-right:5px;\">flight_land</i>การรับงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t</li>
\t\t\t\t\t<div id=\"collapsesend_resive\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/reciever.php\">รับเข้าเอกสาร</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/reciever_branch.php\">รับเอกสารจากสาขา</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/resive_work_post_out.php\">รับเข้าเอกสาร ไปรษณีย์ -> ออก</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/resive_work_post_in.php\">รับเข้าเอกสาร ไปรษณีย์ -> เข้า</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/resive_work_byHand_out.php\">รับเข้าเอกสาร Byhand -> ออก</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t\t<li><a href=\"../mailroom/sender.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">unarchive</i> ส่งออกเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/mailroom_import_barcode_TNT.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">flight_takeoff</i> import TNT Barcode </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/mailroom_sendWork.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">local_printshop</i> พิมพ์ใบนำส่งเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/mailroom_sendWorkTNT.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">flight_takeoff</i> ส่งออกเอกสารไปสาขา </a></li>
\t\t\t\t\t<!-- <li><a href=\"search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  ติดตามเอกสาร </a></li> -->
\t\t\t\t\t<li><a href=\"../mailroom/search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle='collapse' href='#collapse_mailroom'><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ประเมิณผล<i class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t<div id='collapse_mailroom' class='panel-collapse collapse'>
\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_sender.php\"><i class=\"material-icons\">keyboard_arrow_right</i> รับส่งเอกสาร(HO)</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_messenger.php\"><i class=\"material-icons\">keyboard_arrow_right</i> พนักงานเดินเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_HO_to_branch.php\"><i class=\"material-icons\">keyboard_arrow_right</i>สำนักงานใหญ่ถึงสาขา</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_branch_to_HO.php\"><i class=\"material-icons\">keyboard_arrow_right</i>สาขาถึงสำนักงานใหญ่</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_branch_to_branch.php\"><i class=\"material-icons\">keyboard_arrow_right</i> สาขาถึงสาขา</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_branch.php\"><i class=\"material-icons\">keyboard_arrow_right</i> งานสาขา</a></li>
\t\t\t\t\t\t\t\t<!-- <li class='list-group-item'><a href=\"../mailroom/summary.php\"><i class=\"material-icons\">keyboard_arrow_right</i> นำส่งเอกสาร</a></li> -->
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t<li><a href=\"../mailroom/receive_work.php\"><i class=\"material-icons\">gesture</i>ลงชื่อรับเอกสาร</a></li>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#seting\" ><i class=\"material-icons\" style=\"padding-right:5px;\">settings</i>ข้อมูลพื้นฐาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t<div id=\"seting\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/access_log.php\"><i class=\"material-icons\">group</i>&nbsp;&nbsp;ประวัติการเข้าใช้งาน</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/user.php\"><i class=\"material-icons\" >group</i>&nbsp;&nbsp;แก้ปัญหา User </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/import_excel.php\"><i class=\"material-icons\">contacts</i>&nbsp;&nbsp ข้อมูล พนักงาน</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/update_branch.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล สาขา</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.department.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล หน่วยงาน</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.floor.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล ชั้น </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.round.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล รอบการรับเอกสาร </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.postPrice.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูลราคา ปณ. </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.byhandPrice.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูลราคา Byhand </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.byhandZone.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;Zone Byhand </a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#report\" ><i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t<div id=\"report\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.post.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน ปณ.</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.byhand.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน By Hand</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.emp_resivework.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน ประวัติการรับเอกสาร.</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.payment_report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>Payment-Report</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../employee/search_branch_menage.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>รายงาน(บริหารสาขา)</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>\t
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t
\t\t\t\t";
        } elseif ((        // line 476
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Branch", [], "any", false, false, false, 476))) {
            // line 477
            echo "\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">content_paste</i>การสั่งงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_in.php\">ส่งสำนักงานใหญ่</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_out.php\">ส่งระหว่างสาขา</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหาและติดตามเอกสาร </a></li>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t";
        } elseif ((        // line 487
($context["role_id"] ?? null) == twig_get_attribute($this->env, $this->source, ($context["roles"] ?? null), "Manager", [], "any", false, false, false, 487))) {
            // line 488
            echo "\t\t\t\t\t<li><a href=\"search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/user.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">group</i>แก้ปัญหา User </a></li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle='collapse' href='#collapse_summary'><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ประเมิณผล<i class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t<div id='collapse_summary' class='panel-collapse collapse'>
\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_sender.php\"><i class=\"material-icons\">keyboard_arrow_right</i> รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_messenger.php\"><i class=\"material-icons\">keyboard_arrow_right</i> พนักงานเดินเอกสาร</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">description</i>รายงาน<i class=\"material-icons\">arrow_drop_down</i> </a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send.php\"> รายงานรับส่งเอกสาร(รายเดือน)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send_daily.php\">รายงานรับส่งเอกสาร(รายวัน)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"../manager/report_sla.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assignment_turned_in</i>รายงาน SLA </a></li>
\t\t\t\t\t<li><a href=\"../manager/access_log.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">group</i>การเข้าใช้งาน</a></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t\t
\t\t\t\t";
        }
        // line 512
        echo "            </ul>
</nav>    
<!-- /#sidebar-wrapper -->
<!--End Header-->
<!--Start Container-->
<div id=\"panel\" >
 <header class=\"panel-header\" style=\"background-color: #DCD9D4; 
 background-image: linear-gradient(to bottom, rgba(255,255,255,0.50) 0%, rgba(0,0,0,0.50) 100%), radial-gradient(at 50% 0%, rgba(255,255,255,0.10) 0%, rgba(0,0,0,0.50) 50%); 
 background-blend-mode: soft-light,screen;\">
        <span class=\"js-slideout-toggle\"><i class=\"material-icons\">menu</i> <b>MENU</b> </span>
\t\t<span class=\"header-nav\">
\t\t\t
\t\t\t\t<label class=\"header-text\" >The Digital PEON Book System</label>
\t\t\t<!-- <img class=\"img-responsive\" style=\"position:absolute;right:0px;padding:10px;\" src=\"../themes/images/logo-tmb_mini.png\" title=\"TMB\" />  \t -->
\t\t</span>
</header>
\t<div  class=\"container\">";
        // line 528
        $this->displayBlock('Content', $context, $blocks);
        echo "</div>
\t<div class=\"container-fluid\">";
        // line 529
        $this->displayBlock('Content2', $context, $blocks);
        echo "</div>

</div>

<!--End Container-->

<!--Start Footer-->
<footer class=\"footer\">
  <div class=\"container\">
    <p class=\"text-muted\">Copyright © 2017 Pivot Co., Ltd.</p>
  </div>
</footer>
<!--End Footer-->

<!--Start Debug-->

\t";
        // line 545
        $this->displayBlock('debug', $context, $blocks);
        // line 546
        echo "<!--End Debug-->

\t<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src=\"../themes/bootstrap/js/jquery.min.js\"></script> -->
    <script>window.jQuery || document.write('<script src=\"../themes/bootstrap/js/jquery.vendor.min.js\"><\\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=\"../themes/bootstrap/js/ie10-viewport-bug-workaround.js\"></script>
\t
<!-- \t<link href=\"../themes/jquery/jquery-ui.css\" rel=\"stylesheet\"> -->
\t  
\t<script src=\"../themes/alertifyjs/alertify.js\"></script> 
\t<script src=\"../themes/bootstrap_emp/dist/sweetalert2.min.js\"></script>
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js\"></script>
\t
    <script src=\"../themes/bootstrap_emp/popper/popper.min.js\"></script>
    <script src=\"../themes/bootstrap_emp/bootstrap/js/bootstrap.min.js\"></script>
\t
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js\"></script>
\t<script src=\"../themes/fancybox/jquery.fancybox.min.js\"></script>
\t<!-- <script src=\"../themes/jquery/jquery-ui.js\"></script> -->
\t<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
\t<!-- <script src=\"../themes/bootstrap/js/accounting.min.js\"></script> -->
\t<script type=\"text/javascript\">
\t\t\$(document).ready(function(){
\t\t
\t\t 
\t\t
\t\t var slideout = new Slideout({
\t\t\t'panel': document.getElementById('panel'),
\t\t\t'menu': document.getElementById('menu'),
\t\t\t'padding': 256,
\t\t\t'tolerance': 70
\t\t});
\t\t\t\t\t\t
\t\tdocument.querySelector('.js-slideout-toggle').addEventListener('click', function() {
          slideout.toggle();
        });\t\t
\t\t\t\t
\t\t
\t\t
\t\t
\t\t\$('.dropdown-toggle').dropdown()
\t\t";
        // line 590
        if ((($context["select"] ?? null) == "0")) {
            // line 591
            echo "
\t\t";
        } else {
            // line 593
            echo "\t\t\t\$('select').select2();
\t\t";
        }
        // line 595
        echo "
\t\tvar usrID = '";
        // line 596
        echo twig_escape_filter($this->env, ($context["userID"] ?? null), "html", null, true);
        echo "';
\t\t\$.ajax({
\t\t\turl: '../data/ajax/ajax_check_password_Date.php',
\t\t\tmethod: 'GET',
\t\t\tdata: { userID: usrID },
\t\t\tdataType: 'json',
\t\t\tsuccess: function (res) {
\t\t\t//console.log(res);
\t\t\t\tif(parseInt(res['diffdate']) >= 45 ) {
\t\t\t\t\t//alert('');
\t\t\t\t\talertify.alert('Alert!!', 'กรุณาเปลี่ยนรหัสผ่าน เนื่องจากรหัสผ่านของคุณมีอายุการใช้งานเกิน 45 วันแล้ว!', function(){ 
\t\t\t\t\t\t//alertify.success('Ok'); 
\t\t\t\t\t\twindow.location.href='../user/change_password.php?usr='+res['usrID'];
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}
\t\t});
\t\t\$.ajax({
\t\t\turl: '../user/ajax/ajax_permission_branch_menage.php',
\t\t\tmethod: 'GET',
\t\t\tdata: { userID: usrID },
\t\t\tdataType: 'json',
\t\t\tsuccess: function (res) {
\t\t\t\t//console.log('>>>222');
\t\t\t\tif(res.length > 0){
\t\t\t\t\t\$('.permition_branch_menage').show();
\t\t\t\t}else{
\t\t\t\t\t\$('.permition_branch_menage').hide();
\t\t\t\t}
\t\t\t\t
\t\t\t}
\t\t});
\t\t
\t\t\t";
        // line 629
        $this->displayBlock('domReady', $context, $blocks);
        // line 630
        echo "     \t    ";
        $this->displayBlock('domReady2', $context, $blocks);
        echo " 
\t\t});
\t   ";
        // line 632
        $this->displayBlock('javaScript', $context, $blocks);
        // line 634
        echo "
\t    //window.onbeforeunload = function (e) {
        //            e = e || window.event;
        //            // For IE and Firefox prior to version 4
        //            if (e) {
        //              // e.returnValue = 'ท่านต้องการปิดหน้านี้หรือไม่';
        //              if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //                 location.href = '../user/logout.php';
        //              }
        //            }
        //            // For Safari
        //        //return 'ท่านต้องการปิดหน้านี้หรือไม่';
        //       // if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //              // location.href = '../user/logout.php';
        //       // }
        //};
\t</script>
\t<!-- Global site tag (gtag.js) - Google Analytics -->
\t<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-116752839-1\"></script>
\t<script>
\t  window.dataLayer = window.dataLayer || [];
\t  function gtag(){dataLayer.push(arguments);}
\t  gtag('js', new Date());

\t  gtag('config', 'UA-116752839-1');
\t</script>

</body>
</html>
";
    }

    // line 6
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "\t\t<title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<meta name=\"description\" content=\"Pivot Mailroom Services\">
\t\t<meta name=\"author\" content=\"Pivot\">
\t\t<link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" />
\t\t
\t\t
\t\t   <!-- Bootstrap core CSS -->
    <link href=\"../themes/bootstrap_emp/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">
\t
\t
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/alertify.css\" id=\"alertifyCSS\">
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/themes/default.css\" >
\t  <!-- Datepicker -->
\t
    <!-- Custom styles for this template -->
    <link href=\"../themes/bootstrap_emp/bootstrap/css/simple-sidebar.css\" rel=\"stylesheet\">
\t\t
\t<link href=\"../themes/bootstrap/css/sticky-footer-navbar.css\" rel=\"stylesheet\">
\t\t\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/material_icon/material-icons.min.css\">
\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/fancybox/jquery.fancybox.min.css\">
\t 
\t<!-- <script src=\"../themes/bootstrap_emp/jquery/jquery.min.js\"></script> -->
\t<script src=\"../scripts/jquery-1.12.4.js\"></script>
\t
\t";
        // line 36
        $this->displayBlock('scriptImport', $context, $blocks);
        // line 37
        echo "\t<script src=\"../themes/bootstrap/js/ie-emulation-modes-warning.js\"></script>
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js\"></script>

\t<link rel=\"stylesheet\" href=\"../themes/bootstrap_emp/dist/sweetalert2.min.css\">
\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css\" rel=\"stylesheet\" />
\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/select2-bootstrap4.min.css\">
\t
\t
\t
\t<script src=\"../themes/bootstrap_emp/dist/slideout.min.js\"></script>
\t
\t\t<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
\t\t<!--[if lt IE 9]>
\t\t  <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
\t\t  <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
\t\t<![endif]-->
  ";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "The Digital PEON Book System";
    }

    // line 36
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 55
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 528
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 529
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 545
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 629
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 630
    public function block_domReady2($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 632
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 633
        echo "\t   ";
    }

    public function getTemplateName()
    {
        return "base_emp2.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  865 => 633,  861 => 632,  855 => 630,  849 => 629,  843 => 545,  837 => 529,  831 => 528,  825 => 55,  819 => 36,  812 => 7,  792 => 37,  790 => 36,  757 => 7,  753 => 6,  720 => 634,  718 => 632,  712 => 630,  710 => 629,  674 => 596,  671 => 595,  667 => 593,  663 => 591,  661 => 590,  615 => 546,  613 => 545,  594 => 529,  590 => 528,  572 => 512,  546 => 488,  544 => 487,  532 => 477,  530 => 476,  443 => 391,  441 => 390,  384 => 336,  352 => 306,  350 => 305,  308 => 265,  306 => 264,  258 => 218,  256 => 217,  230 => 193,  228 => 192,  224 => 190,  218 => 188,  213 => 186,  209 => 185,  205 => 184,  201 => 183,  197 => 182,  193 => 181,  188 => 180,  186 => 179,  61 => 56,  59 => 55,  56 => 54,  54 => 6,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>

<!-- <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> -->
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" >
<head>
  {% block head %}
\t\t<title>{% block title %}The Digital PEON Book System{% endblock %}</title>
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<meta name=\"description\" content=\"Pivot Mailroom Services\">
\t\t<meta name=\"author\" content=\"Pivot\">
\t\t<link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" />
\t\t
\t\t
\t\t   <!-- Bootstrap core CSS -->
    <link href=\"../themes/bootstrap_emp/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">
\t
\t
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/alertify.css\" id=\"alertifyCSS\">
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/themes/default.css\" >
\t  <!-- Datepicker -->
\t
    <!-- Custom styles for this template -->
    <link href=\"../themes/bootstrap_emp/bootstrap/css/simple-sidebar.css\" rel=\"stylesheet\">
\t\t
\t<link href=\"../themes/bootstrap/css/sticky-footer-navbar.css\" rel=\"stylesheet\">
\t\t\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/material_icon/material-icons.min.css\">
\t 
\t <link rel=\"stylesheet\" type=\"text/css\" href=\"../themes/fancybox/jquery.fancybox.min.css\">
\t 
\t<!-- <script src=\"../themes/bootstrap_emp/jquery/jquery.min.js\"></script> -->
\t<script src=\"../scripts/jquery-1.12.4.js\"></script>
\t
\t{% block scriptImport %}{% endblock %}
\t<script src=\"../themes/bootstrap/js/ie-emulation-modes-warning.js\"></script>
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js\"></script>

\t<link rel=\"stylesheet\" href=\"../themes/bootstrap_emp/dist/sweetalert2.min.css\">
\t<link href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css\" rel=\"stylesheet\" />
\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/select2-bootstrap4.min.css\">
\t
\t
\t
\t<script src=\"../themes/bootstrap_emp/dist/slideout.min.js\"></script>
\t
\t\t<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
\t\t<!--[if lt IE 9]>
\t\t  <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
\t\t  <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
\t\t<![endif]-->
  {% endblock %}
  <style type=\"text/css\">
  {% block styleReady %}{% endblock %}
  nav#menu {
\tbackground-image: -ms-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: -moz-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: -o-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: -webkit-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
\tbackground-image: linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
  }
  .logout{
\tborder-top: 1px solid #0266bb;
    margin-left:20px;
    margin-right:50px;
  }
  
  .list-group-item {
    padding: .25rem 1.25rem; */
    margin-bottom: -1px;
\tborder: 0px solid rgba(0,0,0,.125);
\t}
\t
\t.material-icons {
\t\tvertical-align: middle;
\t\tdisplay: inline; 
\t}
\t
\t.btn-success span {
\t\tvertical-align: middle;
\t}
\t
\t.status {
\t\tborder: 1px solid #e0e0e0;
\t\tpadding: 5px;
\t\tpadding-left: 10px;
\t\tpadding-right: 10px;
\t\tborder-radius: 15px;
\t}
\t


\t.slideout-menu {
\t\tposition: fixed;
\t\ttop: 0;
\t\tbottom: 0;
\t\twidth: 290px;
\t\tmin-height: 100vh;
\t\toverflow-y: scroll;
\t\t-webkit-overflow-scrolling: touch;
\t\tz-index: 0;
\t\tdisplay: none;
\t}
\t
\t.slideout-menu-left {
\t\tleft: 0;
\t}
\t
\t.slideout-menu-right {
\t\tright: 0;
\t}
\t
\t.slideout-panel {
\t\tz-index: 1;
\t\t/* position: relative;
\t\twill-change: transform; */
\t\tbackground-color: #FFF; /* A background-color is required */
\t\tmin-height: 100vh;
\t}
\t
\t.slideout-open,
\t.slideout-open body,
\t.slideout-open .slideout-panel {
\t\toverflow: hidden;
\t}
\t
\t.slideout-open .slideout-menu {
\t\tdisplay: block;
\t}
\t
\t.panel-header{
\t\theight: 50px;
\t\tmargin-bottom: 20px;
\t\tcolor:\twhite;
\t\ttext-align: center;
\t\t//box-shadow: 2px 2px 1px 1px rgba(50,50,50,.4);
\t}
\t
\t
\t.js-slideout-toggle{
\t\tmargin: 13px 5px;
\t\tposition: absolute;
\t\tleft: 10px;
\t}
\t 
\t
\t.header-text {
\t\tmargin-top: 8px;
\t\tfont-size: 25px;
\t\tfont-weight:bold;
\t\tcolor: #696969;
\t\ttext-shadow: white 0.1em 0.1em 0.1em;
\t}

\t
\t@media screen and (max-width: 1080px) {
\t\t.header-text {
\t\t\tmargin-top: 13px;
\t\t\tfont-size: 18px;
\t\t\tfont-weight:bold;
\t\t\tcolor: #696969;
\t\t\ttext-shadow: white 0.1em 0.1em 0.1em;
\t\t}
\t}
  </style>
</head>
<body>
<!--Start Header-->




<nav id=\"menu\">  <!-- style=\"background-color: #0266bb;\" -->
\t\t\t\t
            <ul class=\"sidebar-nav\">
                <li class=\"sidebar-brand\">
                    <a href=\"#\">
\t\t\t\t\t\t{% if role_id == roles.Employee %}
\t\t\t\t\t\t\t<a href=\"../data/profile.php\"><span class=\"white-text name\">{{ user_data.mr_user_username }}</span></a>
\t\t\t\t\t\t{% elseif role_id == roles.Messenger %}\t
\t\t\t\t\t\t\t<a href=\"../data/profile.php\"><span class=\"white-text name\">{{ user_data.mr_user_username }}</span></a>
\t\t\t\t\t\t{% elseif role_id == roles.MessengerBranch %}\t
\t\t\t\t\t\t\t<a href=\"../messenger/profile.php\"><span class=\"white-text name\">{{ user_data.mr_user_username }}</span></a>
\t\t\t\t\t\t{% elseif role_id == roles.MessengerHO %}\t
\t\t\t\t\t\t\t<a href=\"../messenger/profile.php\"><span class=\"white-text name\">{{ user_data.mr_user_username }}</span></a>
\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t<span class=\"white-text name\">{{ user_data.mr_user_username }}</span>
\t\t\t\t\t\t{% endif %}
                    </a>
                </li>
                {% if role_id == roles.Administrator %}
\t\t\t\t\t
\t\t\t\t\t<li><a href=\"../mailroom/search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/user.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">group</i>แก้ปัญหา User </a></li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle='collapse' href='#collapse_mailroom'><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ประเมิณผล<i class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t<div id='collapse_mailroom' class='panel-collapse collapse'>
\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary.php\"><i class=\"material-icons\">keyboard_arrow_right</i> ผู้รับ</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_sender.php\"><i class=\"material-icons\">keyboard_arrow_right</i> ผู้ส่ง</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>

\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">description</i>รายงาน<i class=\"material-icons\">arrow_drop_down</i> </a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send.php\"> รายงานรับส่งเอกสาร(รายเดือน)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send_daily.php\">รายงานรับส่งเอกสาร(รายวัน)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"../manager/report_sla.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assignment_turned_in</i>รายงาน SLA </a></li>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t
\t\t\t\t{% elseif role_id == roles.Employee %}
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">content_paste</i>การสั่งงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/profile_check.php?type=1\">ปลายทาง => สำนักงานใหญ่</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/profile_check.php?type=2\">ปลายทาง => สาขา</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/work_byhand.php\">By Hand</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/work_post.php\">ส่งไปรษณีย์</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/create_work_import_excel.php\">Import Excel</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- <li class=\"list-group-item\" ><a href=\"../employee/create_work_byHand.php\">ไปรษณีย์&&Byhand(Pivot)</a></li> -->
\t\t\t\t\t\t\t\t<!-- <li class=\"list-group-item\" ><a href=\"work_out.php\">ส่งภายนอก</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_byhand.php\">By Hand(เอกสารถึงลูกค้า)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_post.php\">ส่งไปรษณีย์</a></li> -->
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t<!-- <li><a href=\"#\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  รับเอกสาร (Inbox) </a></li> -->
\t\t\t\t\t<li><a href=\"../employee/receive_work.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assignment_return</i> รับเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../employee/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  ติดตามเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../employee/search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li><a href=\"../employee/search_detail.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ค้นหาแบบละเอียด </a></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse2\" ><i class=\"material-icons\" style=\"padding-right:5px;\">description</i>รายงาน<i class=\"material-icons\">arrow_drop_down</i> </a></li>
\t\t\t\t\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/report_send.php\">รายงานการส่งออก</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../employee/report_receive.php\">รายงานการรับเข้า</a></li>
\t\t\t\t\t\t\t<!-- \t<li class=\"list-group-item\" ><a href=\"work_out.php\">ส่งภายนอก</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_byhand.php\">By Hand(เอกสารถึงลูกค้า)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_post.php\">ส่งไปรษณีย์</a></li> -->
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<span class=\"permition_branch_menage\" style=\"display: none;\">
\t\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t\t<li ><a href=\"../mailroom/report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน</a></li>
\t\t\t\t\t\t<li ><a href=\"../employee/search_branch_menage.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>รายงาน(บริหารสาขา)</a></li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li ><a href=\"../mailroom/report.byhand.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>รายงาน By Hand</a></li>
\t\t\t\t\t\t<li ><a href=\"../mailroom/report.post.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน ปณ.</a></li>
\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.payment_report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>Payment-Report</a></li>
\t\t\t\t\t\t<li ><a href=\"../mailroom/access_log.php\"><i class=\"material-icons\">group</i>&nbsp;&nbsp;ประวัติการเข้าใช้งาน</a></li>
\t\t\t\t\t\t
\t\t\t\t\t</span>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../data/profile.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">perm_contact_calendar</i>แก้ไขข้อมูลส่วนตัว</a></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t{% elseif role_id == roles.MessengerBranch %}
\t\t\t\t\t\t<li><a href=\"../messenger/news.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">home</i>หน้าแรก</a></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#receive_branch'><i class=\"material-icons\" style=\"padding-right:5px;\">directions_bike</i>เข้ารับเอกสาร<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='receive_branch' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_list.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">transfer_within_a_station</i>รับเอกสารจากสาขา</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_mixed.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">library_add</i>รวมเอกสารส่ง
\t\t\t\t\t\t\t\t\t\t\tMailroom</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#receive_mailroom'><i class=\"material-icons\" style=\"padding-right:5px;\">domain</i>รับเอกสารจาก Mailroom<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='receive_mailroom' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_mailroom_to_branch.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">domain</i>รับเอกสารส่งสาขา</a> </li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_mailroom_to_byhand.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">motorcycle</i>รับเอกสาร By Hand</a> </li>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li><a href=\"../messenger/receive_hub.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">store_mall_directory</i>รับเอกสารจาก Hub</a> </li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#send_work'><i class=\"material-icons\" style=\"padding-right:5px;\">send</i>ส่งเอกสาร<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='send_work' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/send_branch_list.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">smartphone</i>ส่งเอกสาร สาขา</a> </li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/send_work_byhand.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">directions_bike</i>ส่งเอกสาร By Hand</a> </li>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<!-- <li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li> -->
\t\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t\t<li><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t
\t\t\t\t{% elseif role_id == roles.Messenger %}
\t\t\t\t\t<li><a href=\"../messenger/news.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">home</i>หน้าแรก</a></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_left</i>รับเอกสาร <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workByhand.php\">รับเอกสาร(Byhand)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workPost.php\">รับเอกสาร(ป.ณ.)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse2\" ><i class=\"material-icons\" style=\"padding-right:5px;\">compare_arrows</i>รับเอกสาร Mailroom <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_IN.php\">ป.ณ.ขาเข้า</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_Byhand_IN.php\">Byhand ขาเข้า</a></li>

\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_right</i>ส่งเอกสาร<i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list.php\">ส่งเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_thai_post_in.php\">ป.ณ. ขาเข้า</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_byhand_in.php\">Byhand ขาเข้า</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t<li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t{% elseif role_id == roles.MessengerHO %}\t
\t\t\t\t\t<li><a href=\"../messenger/news.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">home</i>หน้าแรก</a></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_left</i>รับเอกสาร <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workByhand.php\">รับเอกสาร(Byhand)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/receive_workPost.php\">รับเอกสาร(ป.ณ.)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse2\" ><i class=\"material-icons\" style=\"padding-right:5px;\">compare_arrows</i>รับเอกสาร Mailroom <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list.php\">รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_IN.php\">ป.ณ.ขาเข้า</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/mailroom_receive_list_Thaipost_Byhand_IN.php\">Byhand ขาเข้า</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">chevron_right</i>ส่งเอกสาร<i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list.php\">ส่งเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_thai_post_in.php\">ป.ณ. ขาเข้า</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../messenger/send_list_byhand_in.php\">Byhand ขาเข้า</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li>
\t\t\t\t\t
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle='collapse' href='#receive_branch'><i class=\"material-icons\" style=\"padding-right:5px;\">directions_bike</i>เข้ารับเอกสารจากสาขา<i
\t\t\t\t\t\t\t\t class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t\t<div id='receive_branch' class='panel-collapse collapse'>
\t\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_list.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">transfer_within_a_station</i>รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"../messenger/receive_branch_mixed.php\" style=\"padding-left: 30px;\"><i class=\"material-icons\" style=\"padding-right:5px;\">library_add</i>รวมเอกสารส่ง
\t\t\t\t\t\t\t\t\t\t\tMailroom</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</li>

\t\t\t\t\t\t<li><a href=\"../messenger/receive_mailroom_to_branch.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">domain</i>รับเอกสารจาก
\t\t\t\t\t\t\t\tMailroom</a></li>
\t\t\t\t\t\t<li><a href=\"../messenger/receive_hub.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">store_mall_directory</i>รับเอกสารจาก
\t\t\t\t\t\t\t\tHub</a></li>
\t\t\t\t\t\t<li><a href=\"../messenger/send_branch_list.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">send</i>ส่งเอกสารที่สาขา</a></li>

\t\t\t\t\t\t<!-- <li><a href=\"../messenger/search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>  ติดตามเอกสาร </a></li> -->
\t\t\t\t\t\t

\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:5px;\"> <i class=\"material-icons\">power_settings_new</i>Logout</a></li>

\t\t\t\t{% elseif role_id == roles.Mailroom %}
\t\t\t\t<!-- สั่งงาน -->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle=\"collapse\" href=\"#collapsesend\" ><i class=\"material-icons\" style=\"padding-right:5px;\">content_paste</i>การสั่งงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t</li>
\t\t\t\t\t<div id=\"collapsesend\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_post_out.php\">ส่งออก->ไปรษณีย์</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_byHand_out.php\">ส่งออก->Byhand</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_return.php\">ส่งออก->คืนสาขา</a></li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_post_in.php\">งานรับเข้า->ไปรษณีย์</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_byHand_in.php\">งานรับเข้า->Byhand</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/create_work_other_in.php\">งานรับเข้า->จากบุคคลภายนอก</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>\t
\t\t\t\t\t<!-- <li><a href=\"#\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  รับเอกสาร (Inbox) </a></li> -->

\t\t\t\t\t<!-- รับเอกสาร -->
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle=\"collapse\" href=\"#collapsesend_resive\" ><i class=\"material-icons\" style=\"padding-right:5px;\">flight_land</i>การรับงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t</li>
\t\t\t\t\t<div id=\"collapsesend_resive\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/reciever.php\">รับเข้าเอกสาร</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/reciever_branch.php\">รับเอกสารจากสาขา</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/resive_work_post_out.php\">รับเข้าเอกสาร ไปรษณีย์ -> ออก</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/resive_work_post_in.php\">รับเข้าเอกสาร ไปรษณีย์ -> เข้า</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../mailroom/resive_work_byHand_out.php\">รับเข้าเอกสาร Byhand -> ออก</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t\t<li><a href=\"../mailroom/sender.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">unarchive</i> ส่งออกเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/mailroom_import_barcode_TNT.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">flight_takeoff</i> import TNT Barcode </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/mailroom_sendWork.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">local_printshop</i> พิมพ์ใบนำส่งเอกสาร </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/mailroom_sendWorkTNT.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">flight_takeoff</i> ส่งออกเอกสารไปสาขา </a></li>
\t\t\t\t\t<!-- <li><a href=\"search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">inbox</i>  ติดตามเอกสาร </a></li> -->
\t\t\t\t\t<li><a href=\"../mailroom/search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle='collapse' href='#collapse_mailroom'><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ประเมิณผล<i class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t<div id='collapse_mailroom' class='panel-collapse collapse'>
\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_sender.php\"><i class=\"material-icons\">keyboard_arrow_right</i> รับส่งเอกสาร(HO)</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_messenger.php\"><i class=\"material-icons\">keyboard_arrow_right</i> พนักงานเดินเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_HO_to_branch.php\"><i class=\"material-icons\">keyboard_arrow_right</i>สำนักงานใหญ่ถึงสาขา</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_branch_to_HO.php\"><i class=\"material-icons\">keyboard_arrow_right</i>สาขาถึงสำนักงานใหญ่</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_branch_to_branch.php\"><i class=\"material-icons\">keyboard_arrow_right</i> สาขาถึงสาขา</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_branch.php\"><i class=\"material-icons\">keyboard_arrow_right</i> งานสาขา</a></li>
\t\t\t\t\t\t\t\t<!-- <li class='list-group-item'><a href=\"../mailroom/summary.php\"><i class=\"material-icons\">keyboard_arrow_right</i> นำส่งเอกสาร</a></li> -->
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t<li><a href=\"../mailroom/receive_work.php\"><i class=\"material-icons\">gesture</i>ลงชื่อรับเอกสาร</a></li>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#seting\" ><i class=\"material-icons\" style=\"padding-right:5px;\">settings</i>ข้อมูลพื้นฐาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t<div id=\"seting\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/access_log.php\"><i class=\"material-icons\">group</i>&nbsp;&nbsp;ประวัติการเข้าใช้งาน</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/user.php\"><i class=\"material-icons\" >group</i>&nbsp;&nbsp;แก้ปัญหา User </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/import_excel.php\"><i class=\"material-icons\">contacts</i>&nbsp;&nbsp ข้อมูล พนักงาน</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/update_branch.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล สาขา</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.department.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล หน่วยงาน</a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.floor.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล ชั้น </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.round.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูล รอบการรับเอกสาร </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.postPrice.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูลราคา ปณ. </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.byhandPrice.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;ข้อมูลราคา Byhand </a></li>
\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/seting.byhandZone.php\"><i class=\"material-icons\">build</i>&nbsp;&nbsp;Zone Byhand </a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#report\" ><i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t<div id=\"report\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.post.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน ปณ.</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.byhand.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน By Hand</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.emp_resivework.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">print</i>รายงาน ประวัติการรับเอกสาร.</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../mailroom/report.payment_report.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>Payment-Report</a></li>
\t\t\t\t\t\t\t<li class=\"list-group-item\"><a href=\"../employee/search_branch_menage.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">donut_small</i>รายงาน(บริหารสาขา)</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>\t
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t
\t\t\t\t{% elseif role_id == roles.Branch %}
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse1\" ><i class=\"material-icons\" style=\"padding-right:5px;\">content_paste</i>การสั่งงาน <i class=\"material-icons\">arrow_drop_down</i></a></li>
\t\t\t\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_in.php\">ส่งสำนักงานใหญ่</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"work_out.php\">ส่งระหว่างสาขา</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"search_follow.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหาและติดตามเอกสาร </a></li>
\t\t\t\t\t<li class=\"logout\"></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t{% elseif role_id == roles.Manager %}
\t\t\t\t\t<li><a href=\"search.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">search</i>ค้นหา </a></li>
\t\t\t\t\t<li><a href=\"../mailroom/user.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">group</i>แก้ปัญหา User </a></li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a data-toggle='collapse' href='#collapse_summary'><i class=\"material-icons\" style=\"padding-right:5px;\">assessment</i>ประเมิณผล<i class=\"material-icons\">arrow_drop_down</i></a>
\t\t\t\t\t\t<div id='collapse_summary' class='panel-collapse collapse'>
\t\t\t\t\t\t\t<ul class='list-group'>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_sender.php\"><i class=\"material-icons\">keyboard_arrow_right</i> รับเอกสาร</a></li>
\t\t\t\t\t\t\t\t<li class='list-group-item'><a href=\"../mailroom/summary_messenger.php\"><i class=\"material-icons\">keyboard_arrow_right</i> พนักงานเดินเอกสาร</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t
\t\t\t\t\t<li><a data-toggle=\"collapse\" href=\"#collapse3\" ><i class=\"material-icons\" style=\"padding-right:5px;\">description</i>รายงาน<i class=\"material-icons\">arrow_drop_down</i> </a></li>
\t\t\t\t\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\">
\t\t\t\t\t\t\t<ul class=\"list-group\" >
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send.php\"> รายงานรับส่งเอกสาร(รายเดือน)</a></li>
\t\t\t\t\t\t\t\t<li class=\"list-group-item\" ><a href=\"../manager/report_send_daily.php\">รายงานรับส่งเอกสาร(รายวัน)</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t<li><a href=\"../manager/report_sla.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">assignment_turned_in</i>รายงาน SLA </a></li>
\t\t\t\t\t<li><a href=\"../manager/access_log.php\"><i class=\"material-icons\" style=\"padding-right:5px;\">group</i>การเข้าใช้งาน</a></li>
\t\t\t\t\t<li ><a href=\"../user/logout.php\" style=\"margin-left:0px;\"> <i class=\"material-icons\" style=\"padding-right:5px;\">power_settings_new</i>Logout</a></li>
\t\t\t\t\t
\t\t\t\t{% endif %}
            </ul>
</nav>    
<!-- /#sidebar-wrapper -->
<!--End Header-->
<!--Start Container-->
<div id=\"panel\" >
 <header class=\"panel-header\" style=\"background-color: #DCD9D4; 
 background-image: linear-gradient(to bottom, rgba(255,255,255,0.50) 0%, rgba(0,0,0,0.50) 100%), radial-gradient(at 50% 0%, rgba(255,255,255,0.10) 0%, rgba(0,0,0,0.50) 50%); 
 background-blend-mode: soft-light,screen;\">
        <span class=\"js-slideout-toggle\"><i class=\"material-icons\">menu</i> <b>MENU</b> </span>
\t\t<span class=\"header-nav\">
\t\t\t
\t\t\t\t<label class=\"header-text\" >The Digital PEON Book System</label>
\t\t\t<!-- <img class=\"img-responsive\" style=\"position:absolute;right:0px;padding:10px;\" src=\"../themes/images/logo-tmb_mini.png\" title=\"TMB\" />  \t -->
\t\t</span>
</header>
\t<div  class=\"container\">{% block Content %}{% endblock %}</div>
\t<div class=\"container-fluid\">{% block Content2 %}{% endblock %}</div>

</div>

<!--End Container-->

<!--Start Footer-->
<footer class=\"footer\">
  <div class=\"container\">
    <p class=\"text-muted\">Copyright © 2017 Pivot Co., Ltd.</p>
  </div>
</footer>
<!--End Footer-->

<!--Start Debug-->

\t{% block debug %}{% endblock %}
<!--End Debug-->

\t<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src=\"../themes/bootstrap/js/jquery.min.js\"></script> -->
    <script>window.jQuery || document.write('<script src=\"../themes/bootstrap/js/jquery.vendor.min.js\"><\\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=\"../themes/bootstrap/js/ie10-viewport-bug-workaround.js\"></script>
\t
<!-- \t<link href=\"../themes/jquery/jquery-ui.css\" rel=\"stylesheet\"> -->
\t  
\t<script src=\"../themes/alertifyjs/alertify.js\"></script> 
\t<script src=\"../themes/bootstrap_emp/dist/sweetalert2.min.js\"></script>
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js\"></script>
\t
    <script src=\"../themes/bootstrap_emp/popper/popper.min.js\"></script>
    <script src=\"../themes/bootstrap_emp/bootstrap/js/bootstrap.min.js\"></script>
\t
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js\"></script>
\t<script src=\"../themes/fancybox/jquery.fancybox.min.js\"></script>
\t<!-- <script src=\"../themes/jquery/jquery-ui.js\"></script> -->
\t<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
\t<!-- <script src=\"../themes/bootstrap/js/accounting.min.js\"></script> -->
\t<script type=\"text/javascript\">
\t\t\$(document).ready(function(){
\t\t
\t\t 
\t\t
\t\t var slideout = new Slideout({
\t\t\t'panel': document.getElementById('panel'),
\t\t\t'menu': document.getElementById('menu'),
\t\t\t'padding': 256,
\t\t\t'tolerance': 70
\t\t});
\t\t\t\t\t\t
\t\tdocument.querySelector('.js-slideout-toggle').addEventListener('click', function() {
          slideout.toggle();
        });\t\t
\t\t\t\t
\t\t
\t\t
\t\t
\t\t\$('.dropdown-toggle').dropdown()
\t\t{% if select == '0' %}

\t\t{% else %}
\t\t\t\$('select').select2();
\t\t{% endif %}

\t\tvar usrID = '{{ userID }}';
\t\t\$.ajax({
\t\t\turl: '../data/ajax/ajax_check_password_Date.php',
\t\t\tmethod: 'GET',
\t\t\tdata: { userID: usrID },
\t\t\tdataType: 'json',
\t\t\tsuccess: function (res) {
\t\t\t//console.log(res);
\t\t\t\tif(parseInt(res['diffdate']) >= 45 ) {
\t\t\t\t\t//alert('');
\t\t\t\t\talertify.alert('Alert!!', 'กรุณาเปลี่ยนรหัสผ่าน เนื่องจากรหัสผ่านของคุณมีอายุการใช้งานเกิน 45 วันแล้ว!', function(){ 
\t\t\t\t\t\t//alertify.success('Ok'); 
\t\t\t\t\t\twindow.location.href='../user/change_password.php?usr='+res['usrID'];
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}
\t\t});
\t\t\$.ajax({
\t\t\turl: '../user/ajax/ajax_permission_branch_menage.php',
\t\t\tmethod: 'GET',
\t\t\tdata: { userID: usrID },
\t\t\tdataType: 'json',
\t\t\tsuccess: function (res) {
\t\t\t\t//console.log('>>>222');
\t\t\t\tif(res.length > 0){
\t\t\t\t\t\$('.permition_branch_menage').show();
\t\t\t\t}else{
\t\t\t\t\t\$('.permition_branch_menage').hide();
\t\t\t\t}
\t\t\t\t
\t\t\t}
\t\t});
\t\t
\t\t\t{% block domReady %}{% endblock %}
     \t    {% block domReady2 %}{% endblock %} 
\t\t});
\t   {% block javaScript %}
\t   {% endblock %}

\t    //window.onbeforeunload = function (e) {
        //            e = e || window.event;
        //            // For IE and Firefox prior to version 4
        //            if (e) {
        //              // e.returnValue = 'ท่านต้องการปิดหน้านี้หรือไม่';
        //              if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //                 location.href = '../user/logout.php';
        //              }
        //            }
        //            // For Safari
        //        //return 'ท่านต้องการปิดหน้านี้หรือไม่';
        //       // if(confirm(\"ท่านต้องการปิดหน้านี้หรือไม่\")) {
        //              // location.href = '../user/logout.php';
        //       // }
        //};
\t</script>
\t<!-- Global site tag (gtag.js) - Google Analytics -->
\t<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-116752839-1\"></script>
\t<script>
\t  window.dataLayer = window.dataLayer || [];
\t  function gtag(){dataLayer.push(arguments);}
\t  gtag('js', new Date());

\t  gtag('config', 'UA-116752839-1');
\t</script>

</body>
</html>
", "base_emp2.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\base_emp2.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/resive_work_byHand_out.tpl */
class __TwigTemplate_982e86360295a6e83d8c2327ed39efcd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_3' => [$this, 'block_menu_3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/resive_work_byHand_out.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "- List";
    }

    // line 5
    public function block_menu_3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 29
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "#btn_add_emp_modal{
\theight: 27px !important;
\tline-height: 0 !important;
}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}


";
    }

    // line 64
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "\t
\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t


  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'emp_id_send': {
\t\t\t//required: true
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: true
\t\t},
\t\t'name_re': {
\t\t\trequired: true
\t\t},
\t\t'lname_re': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\trequired: true
\t\t},
\t\t'address_re': {
\t\t\trequired: true
\t\t},
\t\t'sub_district_re': {
\t\t\t//required: true
\t\t},
\t\t'district_re': {
\t\t\t//required: true
\t\t},
\t\t'province_re': {
\t\t\t//required: true
\t\t},
\t\t'post_code_re': {
\t\t\t//required: true
\t\t},
\t\t'topic': {
\t\t\trequired: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'emp_id_send': {
\t\t  required: 'กรุณาระบุ ผู้ส่ง'
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: 'กรุณาระบุ หน่วยงานผู้ส่ง'
\t\t  },
\t\t'name_re': {
\t\t  required: 'กรุณาระบุ ชื่อผู้รับ'
\t\t},
\t\t'lname_re': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้รับ'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_re': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'sub_district_re': {
\t\t  required: 'กรุณาระบุ แขวง/ตำบล'
\t\t},
\t\t'district_re': {
\t\t  required: 'กรุณาระบุ เขต/อำเภอ'
\t\t},
\t\t'province_re': {
\t\t  required: 'กรุณาระบุ จังหวัด'
\t\t},
\t\t'post_code_re': {
\t\t  required: 'กรุณาระบุ รหัสไปรษณีย์'
\t\t},
\t\t'topic': {
\t\t  required: 'กรุณาระบุ หัวเรื่อง'
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });
  \$('#myform_data_update').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'messenger_user_id': {
\t\t\trequired: true
\t\t},
\t\t
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภท.'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบการจัดส่ง.'
\t\t},
\t\t'messenger_user_id': {
\t\t  required: 'กรุณาระบุ แมสเซ็นเจอร์.'
\t\t},
\t\t
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });



\$('#btn_update').click(function() {
\tif(\$('#myform_data_update').valid()) {
\t\tvar form = \$('#myform_data_update');
\t\t //var serializeData = form.serializeArray();
\t\t var serializeData = form.serialize();
\t\t \$.ajax({
\t\t   method: \"POST\",
\t\t   dataType:'json',
\t\t   url: \"ajax/ajax_save_resive_Work_byhand.php\",
\t\t   data: serializeData,
\t\t   beforeSend: function() {
\t\t\t   // setting a timeout
\t\t\t   \$(\"#bg_loader\").show();
\t\t   },
\t\t   error: function (error) {
\t\t\t alert('error; ' + eval(error));
\t\t\t \$(\"#bg_loader\").hide();
\t\t   // location.reload();
\t\t   }
\t\t })
\t\t .done(function( res ) {
\t\t   \$(\"#bg_loader\").hide();
\t\t   if(res['status'] == 505){
\t\t\t   //console.log(res);
\t\t\t   \$('#csrf_token').val(res['token']);
\t\t\t   alertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t   ,function(){
\t\t\t\t   //window.location.reload();
\t\t\t   });
\t\t   }else if(res['status'] == 200){
\t\t\t   load_data_bydate();
\t\t\t   \$('#csrf_token').val(res['token']);
\t\t\t   \$('#myform_data_update')[0].reset();
\t\t\t   

\t\t\t   //token
\t\t   }else{
\t\t\t   alertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t   ,function(){
\t\t\t\t   window.location.reload();
\t\t\t   });
\t\t   }
\t\t });
\t
   }

});
\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_byhand.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});




\$('#myform_data_add_emp').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'emp_code_add': {
\t\t\trequired: true
\t\t},
\t\t'emp_mail_add': {
\t\t\trequired: true
\t\t},
\t\t'emp_name_add': {
\t\t\trequired: true
\t\t},
\t\t'emp_lname_add': {
\t\t\trequired: true
\t\t},
\t\t'dep_id_add': {
\t\t\trequired: true
\t\t}
\t  
\t},
\tmessages: {
\t\t'emp_code_add': {
\t\t  required: 'กรุณาระบุ รหัสพนักงาน.'
\t\t},
\t\t'emp_mail_add': {
\t\t  required: 'กรุณาระบุ อีเมลล์'
\t\t},
\t\t'emp_name_add': {
\t\t  required: 'กรุณาระบุ ชื่อพนักงาน'
\t\t},
\t\t'emp_lname_add': {
\t\t  required: 'กรุณาระบุ นามสกุล'
\t\t},
\t\t'dep_id_add': {
\t\t\trequired: 'กรุณาระบุ หน่วยงาน'
\t\t}
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });

  \$('#btn_add_emp').click(function() {
\t
\tif(\$('#myform_data_add_emp').valid()) {
\t \tvar form = \$('#myform_data_add_emp');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_employee_page_byhand.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {

\t\t\t\$(\"#bg_loader\").hide();
\t\t\tsetTimeout(function(){ 
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').hide();
\t\t\t}, 3000);
\t\t\tif(res['st'] == 'error'){
\t\t\t\t\$('#error').text(res['msg']);
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').show();
\t\t\t}else{
\t\t\t\t\$('#success').text(res['msg']);
\t\t\t\t\$('#success').show();
\t\t\t\t\$('#error').hide();
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});


\$('#dep_id_add').select2();
\$('#dep_id_send').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'action'},
        {'data': 'mr_work_byhand_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_round_name'},
        {'data': 'mr_cus_name'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'},
        {'data': 'mess'}
    ]
});

load_data_bydate();

\$('#work_barcode').keypress(function(event){
\t\t\tvar keycode = (event.keyCode ? event.keyCode : event.which);
\t\t\tif(keycode == '13'){
\t\t\t\tvar barcode = \$(this).val();
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_get_data_bybacose_Byhand.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tbarcode\t: barcode,
\t\t\t\t\t\tpage\t: 'getdataBybarcode',
\t\t\t\t\t\tcsrf_token\t: \$('#csrf_token').val()
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);

\t\t\t\t\t\t//console.log(\"++++++++++++++\");
\t\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\tif(res['count']>0){
\t\t\t\t\t\t\t\tset_form_val_all(res['data'])
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\$('#btn-show-form-edit').attr('disabled','disabled');
\t\t\t\t\t\t\t\treset_price_form();
\t\t\t\t\t\t\t\treset_resive_form();
\t\t\t\t\t\t\t\treset_send_form();
\t\t\t\t\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#work_barcode\").val('');
\t\t\t\t\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}); 
\t\t\t}
\t\t  });\t
function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t\tconsole.log(res.data.mr_department_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_search.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.fullname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t//console.log(ui.item);
                return false;
            },
            focus: function(event, ui){
                \$( \"#name_re\" ).val( ui.item.fullname );
                \$( \"#address_re\" ).val( ui.item.mr_address );
                \$( \"#tel_re\" ).val( ui.item.mr_cus_tel );
                return false;
            },
        });
";
    }

    // line 644
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 645
        echo "
function chang_dep_id() {
\tvar dep = \$('#dep_id_send').select2('data'); 
\tvar emp = \$('#emp_id_send').select2('data'); 
\t
\tconsole.log(emp);
\tif(emp[0].id!=''){
\t\t\$(\"#emp_send_data\").val(emp[0].text+'\\\\n'+dep[0].text);
\t}else{
\t\t\$(\"#emp_send_data\").val(dep[0].text);
\t}
\t
}

function load_data_bydate() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_load_Work_resive_byHand_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function reset_send_form() {
\t\$('input[name=\"type_send\"]').attr('checked', false);
\t\$('#round').val(\"\");
\t\$('#emp_id_send').val(\"\").trigger('change');
\t\$('#emp_send_data').val(\"\");
}

function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_resive_byHand_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}
function reset_resive_form() {
\t\$(\"#name_re\").val(\"\");
\t\$(\"#lname_re\").val(\"\");
\t\$(\"#tel_re\").val(\"\");
\t\$(\"#address_re\").val(\"\");
\t\$(\"#sub_districts_code_re\").val(\"\");
\t\$(\"#districts_code_re\").val(\"\");
\t\$(\"#provinces_code_re\").val(\"\");
\t\$(\"#sub_district_re\").val(\"\");
\t\$(\"#district_re\").val(\"\");
\t\$(\"#province_re\").val(\"\");
\t\$(\"#post_code_re\").val(\"\");
\t\$(\"#quty\").val(\"1\");
\t\$(\"#topic\").val(\"\");
\t\$(\"#work_remark\").val(\"\");
}
function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\tconsole.log(11);
\t\t\$(\"#form_print\").attr('action', 'print_peper_byhand.php');
\t\t\$('#form_print').submit();
\t}else if(type == 2){
\t\tconsole.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_cover_byhand.php');
\t\t\$('#form_print').submit();
\t}else{
\t\talert('----');
\t}

}
function set_form_val_all(data) {
\t\$('#round').val(data.mr_round_id);
\t\$('#quty').val(data.quty);
\t\$('#topic').val(data.topic);
\t\$('#work_remark').val(data.work_remark);
\t\$('#update_emp_send_data').val(data.mr_send_emp_detail);
\t\$(\"input[value=\"+data.mr_work_byhand_type_id+\"]\").attr('checked', true); 
\t\$('#update_mr_work_main_id').val(data.mr_work_main_id);
\t\$('#update_mr_work_byhand_id').val(data.mr_work_byhand_id);


\tvar detail_receiver = '';
\tdetail_receiver += data.mr_cus_name;
\tdetail_receiver += ' '+data.mr_cus_lname;
\tdetail_receiver += ' '+data.mr_address;
\tdetail_receiver += ' '+data.mr_sub_districts_name;
\tdetail_receiver += ' '+data.mr_districts_name;
\tdetail_receiver += ' '+data.mr_provinces_name;
\tdetail_receiver += ' '+data.mr_post_code;
\tdetail_receiver += '  โทร: '+data.mr_cus_tel;
\t\$('#update_emp_resive_data').val(detail_receiver);

}

";
    }

    // line 777
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 778
        echo "<div  class=\"container-fluid\">



  <br>
  <div class=\"tab-content\" id=\"myTabContent\">
\t<div class=\"tab-pane fade show active\" id=\"tab-update\" role=\"tabpanel\" aria-labelledby=\"home-tab\">
\t<form id=\"myform_data_update\">
\t\t<input type=\"hidden\" id=\"update_mr_work_main_id\" name=\"update_mr_work_main_id\">
\t\t<input type=\"hidden\" id=\"update_mr_work_byhand_id\" name=\"mr_work_byhand_id\">
\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"";
        // line 788
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">
\t\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>รับงาน BY HAND</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"form-group row\">
\t\t<div class=\"col-sm-4\">
\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"work_barcode\" name=\"work_barcode\" placeholder=\"Enter Barcode\">
\t\t</div>
\t</div>

\t<div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลรายละเอียดงาน</h5>
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t<table style=\"width: 100%;\" class=\"\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_send\"></span>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 814
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการรับส่ง: </span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_type_send\"></span><br>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send1\" value=\"1\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งเอกสาร </span><br>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send2\" value=\"2\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > รับเอกสาร</span><br>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send3\" value=\"3\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งและรับเอกสารกลับ</span>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t";
        // line 832
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 833
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 833), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 833), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 835
        echo "\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t  </div>
\t\t\t\t\t  <hr>
\t\t\t\t\t 

\t\t\t\t\t  <hr>
\t\t\t\t\t  <h5 class=\"card-title\">แมสเซ็นเจอร์ (ทีวิ่งงานงาน)</h5>
\t\t\t\t\t  <div class=\"table-responsive\">
\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >ชื่อแมส:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_messenger_user_id\"></span>
\t\t\t\t\t\t\t\t\t<select data-error=\"#err_messenger_user_id\" class=\"form-control form-control-sm\" id=\"messenger_user_id\" name=\"messenger_user_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">1</option>
\t\t\t\t\t\t\t\t\t\t<option value=\"1\">2</option>
\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td><br>
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"1\" id=\"mr_work_byhand_payment_type_id1\" name=\"mr_work_byhand_payment_type_id\" checked>  ราคาปกติ  
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"2\" id=\"mr_work_byhand_payment_type_id2\" name=\"mr_work_byhand_payment_type_id\">  ราคาจ่ายแบบ Spare  
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t  </div>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <div class=\"col-12 text-center\">
\t\t\t\t\t  \t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_update\">บันทึกข้อมูล </button>
\t\t\t\t\t  </div>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t  <table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t  <td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t  <td>
\t\t\t\t\t\t\t\t  <textarea class=\"form-control\" id=\"update_emp_send_data\" name=\"update_emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t  </td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >รายละเอียดผู้ผู้รับ:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"update_emp_resive_data\" name=\"update_emp_resive_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t  </table>
\t\t\t\t\t</div>
\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลเอกสาร</h5>
\t\t\t\t\t\t<table style=\"width: 100%;\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\" readonly>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >หัวเรื่อง:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_topic\"></span>
\t\t\t\t\t\t\t\t\t<input id=\"topic\" name=\"topic\" data-error=\"#err_topic\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\" readonly>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_work_remark\"></span>
\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\" readonly></textarea>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t  </div><br>
\t\t\t\t\t  <div class=\"form-group text-center\">
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 933
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t\t
\t\t</div>
\t</form>
\t</div>
\t<div class=\"tab-pane fade\" id=\"tab-key\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>สั่งงาน BY HAND</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<br>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"card\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลรายละเอียดงาน</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table style=\"width: 100%;\" class=\"\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 965
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการรับส่ง: </span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_type_send\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send1\" value=\"1\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งเอกสาร </span><br>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send2\" value=\"2\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > รับเอกสาร</span><br>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send3\" value=\"3\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งและรับเอกสารกลับ</span>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 983
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 984
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 984), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 984), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 986
        echo "\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_send\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_send\" class=\"form-control form-control-sm\" id=\"emp_id_send\" name=\"emp_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ค้นหาพนักงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t</select>\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสหน่วยงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>

\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_id_send\" class=\"form-control form-control-sm\" id=\"dep_id_send\" name=\"dep_id_send\" onchange=\"chang_dep_id();\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 1015
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 1016
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_id", [], "any", false, false, false, 1016), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_code", [], "any", false, false, false, 1016), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_name", [], "any", false, false, false, 1016), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1018
        echo "\t\t\t\t\t\t\t\t\t\t\t\t</select>\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"emp_send_data\" name=\"emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">แมสเซ็นเจอร์ (ทีวิ่งงานงาน)</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >ชื่อแมส:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_messenger_user_id\"></span>
\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_messenger_user_id\" class=\"form-control form-control-sm\" id=\"messenger_user_id\" name=\"messenger_user_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">1</option>
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\">2</option>
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div><br>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t



\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้รับ</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_re\" id=\"name_re\" data-error=\"#err_name_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"นาย ณัฐวุฒิ\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >นามสกุล:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_lname_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"lname_re\" name=\"lname_re\" data-error=\"#err_lname_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"สามพ่วงบุญ\" val=\"\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์มือถือ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_re\" name=\"address_re\" data-error=\"#err_address_re\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลเอกสาร</h5>
\t\t\t\t\t\t\t\t<table style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >หัวเรื่อง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_topic\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"topic\" name=\"topic\" data-error=\"#err_topic\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\"></textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div><br>
\t\t\t\t\t\t\t<div class=\"form-group text-center\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t";
        // line 1165
        echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>\t\t\t\t\t
\t\t</div>
\t</div>
\t</div>

\t</div>
\t</div>


<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t";
        // line 1189
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["round"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 1190
            echo "\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_id", [], "any", false, false, false, 1190), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "mr_round_name", [], "any", false, false, false, 1190), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1192
        echo "\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"";
        // line 1196
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, ($context["today"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบงาน</button>
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">Update</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">ประเภท</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ผู้รับ</th>
\t\t\t\t<th width=\"15%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">เบอร์โทร</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">หมายเหตุ</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">แมส</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>





<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>






<!-- Modal -->
<div class=\"modal fade\" id=\"add_emp_modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLabel\">เพิ่มข้อมูลพนักงาน</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t<form id=\"myform_data_add_emp\">
\t\t\t
\t\t\t
\t\t\t<div class=\"form-row\">
\t\t\t\t
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_code_add\"></span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_mail_add\"></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t<input id=\"emp_code_add\" name=\"emp_code_add\" data-error=\"#err_emp_code_add\"  type=\"text\" class=\"form-control form-control-sm\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-9\">
\t\t\t\t\t<input id=\"emp_mail_add\" name=\"emp_mail_add\" data-error=\"#err_emp_mail_add\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"อีเมลล์\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<br>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t<span class=\"box_error\" id=\"err_emp_name_add\"></span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t<span class=\"box_error\" id=\"err_emp_lname_add\"></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t<input id=\"emp_name_add\" name=\"emp_name_add\" data-error=\"#err_emp_name_add\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"ชื่อ\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t<input id=\"emp_lname_add\" name=\"emp_lname_add\" data-error=\"#err_emp_lname_add\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"นามสกุล\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<br>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t <span class=\"box_error\" id=\"err_dep_id_add\"></span>
\t\t\t\t<select data-error=\"#err_dep_id_add\" class=\"form-control form-control-sm\" id=\"dep_id_add\" name=\"dep_id_add\" onchange=\"chang_dep_id();\" style=\"width:100%;\">
\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t";
        // line 1307
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 1308
            echo "\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_id", [], "any", false, false, false, 1308), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_code", [], "any", false, false, false, 1308), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["d"], "mr_department_name", [], "any", false, false, false, 1308), "html", null, true);
            echo "</option>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1310
        echo "\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<input type=\"hidden\" name=\"page\" id=\"page\" value=\"add_emp\">
\t\t</form>
\t\t
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
        <button id=\"btn_add_emp\" type=\"button\" class=\"btn btn-primary\">บันทึกข้อมูล</button>
\t\t\t 
      </div>
\t  <div class=\"form-row\">
\t\t<div class=\"col\">
\t\t\t<div id=\"error\" class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t  </div>
\t\t\t<div id=\"success\" class=\"alert alert-success\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t</div>
\t\t</div>
\t</div>
    </div>
  </div>
</div>





";
    }

    // line 1344
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 1346
        if ((($context["debug"] ?? null) != "")) {
            // line 1347
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 1349
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/resive_work_byHand_out.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1534 => 1349,  1528 => 1347,  1526 => 1346,  1519 => 1344,  1484 => 1310,  1471 => 1308,  1467 => 1307,  1351 => 1196,  1345 => 1192,  1334 => 1190,  1330 => 1189,  1304 => 1165,  1159 => 1018,  1146 => 1016,  1142 => 1015,  1111 => 986,  1100 => 984,  1096 => 983,  1073 => 965,  1039 => 933,  943 => 835,  932 => 833,  928 => 832,  905 => 814,  876 => 788,  864 => 778,  860 => 777,  725 => 645,  721 => 644,  135 => 64,  99 => 30,  95 => 29,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
\t\t<link rel=\"stylesheet\" href=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css\"></link>

\t\t<link rel=\"stylesheet\" href=\"../themes/jquery/jquery-ui.css\">
\t\t<script src=\"../themes/jquery/jquery-ui.js\"></script>

\t\t<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
\t\t<script src=\"../themes/jquery/jquery.validate.min.js\"></script>
\t\t<!-- dependencies for zip mode -->
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/zip.js/zip.js\"></script>
\t\t\t<!-- / dependencies for zip mode -->

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/JQL.min.js\"></script>
\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js\"></script>

\t\t\t<script type=\"text/javascript\" src=\"../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js\"></script>
\t\t\t
\t\t\t<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
\t\t\t<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}
#btn_add_emp_modal{
\theight: 27px !important;
\tline-height: 0 !important;
}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
\tfont-size:14px;
}
.box_error{
\tfont-size:12px;
\tcolor:red;
}
#loader{
\t  height:100px;
\t  width :100px;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:fixed;
\t\ttop:500px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}


{% endblock %}

{% block domReady %}\t
\$('#date_send').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t
\$('#date_report').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true,
\t\t\tformat: 'yyyy-mm-dd'
});\t


  \$('#myform_data_senderandresive').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'emp_id_send': {
\t\t\t//required: true
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: true
\t\t},
\t\t'name_re': {
\t\t\trequired: true
\t\t},
\t\t'lname_re': {
\t\t\t//required: true
\t\t},
\t\t'tel_re': {
\t\t\trequired: true
\t\t},
\t\t'address_re': {
\t\t\trequired: true
\t\t},
\t\t'sub_district_re': {
\t\t\t//required: true
\t\t},
\t\t'district_re': {
\t\t\t//required: true
\t\t},
\t\t'province_re': {
\t\t\t//required: true
\t\t},
\t\t'post_code_re': {
\t\t\t//required: true
\t\t},
\t\t'topic': {
\t\t\trequired: true
\t\t},
\t\t'quty': {
\t\t\trequired: true,
\t\t},
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภทการส่ง'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบจัดส่ง'
\t\t},
\t\t'emp_id_send': {
\t\t  required: 'กรุณาระบุ ผู้ส่ง'
\t\t},
\t\t'dep_id_send': {
\t\t\trequired: 'กรุณาระบุ หน่วยงานผู้ส่ง'
\t\t  },
\t\t'name_re': {
\t\t  required: 'กรุณาระบุ ชื่อผู้รับ'
\t\t},
\t\t'lname_re': {
\t\t  required: 'กรุณาระบุ นามสกุลผู้รับ'
\t\t},
\t\t'tel_re': {
\t\t  required: 'กรุณาระบุ เบอร์โทร'
\t\t},
\t\t'address_re': {
\t\t  required: 'กรุณาระบุ ที่อยู่'
\t\t},
\t\t'sub_district_re': {
\t\t  required: 'กรุณาระบุ แขวง/ตำบล'
\t\t},
\t\t'district_re': {
\t\t  required: 'กรุณาระบุ เขต/อำเภอ'
\t\t},
\t\t'province_re': {
\t\t  required: 'กรุณาระบุ จังหวัด'
\t\t},
\t\t'post_code_re': {
\t\t  required: 'กรุณาระบุ รหัสไปรษณีย์'
\t\t},
\t\t'topic': {
\t\t  required: 'กรุณาระบุ หัวเรื่อง'
\t\t},
\t\t'quty': {
\t\t  required: 'กรุณาระบุ จำนวน',
\t\t  number: 'กรุณาระบุ เป็นตัวเลข'
\t\t},
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });
  \$('#myform_data_update').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'date_send': {
\t\t\trequired: true
\t\t},
\t\t'type_send': {
\t\t\trequired: true
\t\t},
\t\t'round': {
\t\t\trequired: true
\t\t},
\t\t'messenger_user_id': {
\t\t\trequired: true
\t\t},
\t\t
\t  
\t},
\tmessages: {
\t\t'date_send': {
\t\t  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
\t\t},
\t\t'type_send': {
\t\t  required: 'กรุณาระบุ ประเภท.'
\t\t},
\t\t'round': {
\t\t  required: 'กรุณาระบุ รอบการจัดส่ง.'
\t\t},
\t\t'messenger_user_id': {
\t\t  required: 'กรุณาระบุ แมสเซ็นเจอร์.'
\t\t},
\t\t
\t\t 
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });



\$('#btn_update').click(function() {
\tif(\$('#myform_data_update').valid()) {
\t\tvar form = \$('#myform_data_update');
\t\t //var serializeData = form.serializeArray();
\t\t var serializeData = form.serialize();
\t\t \$.ajax({
\t\t   method: \"POST\",
\t\t   dataType:'json',
\t\t   url: \"ajax/ajax_save_resive_Work_byhand.php\",
\t\t   data: serializeData,
\t\t   beforeSend: function() {
\t\t\t   // setting a timeout
\t\t\t   \$(\"#bg_loader\").show();
\t\t   },
\t\t   error: function (error) {
\t\t\t alert('error; ' + eval(error));
\t\t\t \$(\"#bg_loader\").hide();
\t\t   // location.reload();
\t\t   }
\t\t })
\t\t .done(function( res ) {
\t\t   \$(\"#bg_loader\").hide();
\t\t   if(res['status'] == 505){
\t\t\t   //console.log(res);
\t\t\t   \$('#csrf_token').val(res['token']);
\t\t\t   alertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t   ,function(){
\t\t\t\t   //window.location.reload();
\t\t\t   });
\t\t   }else if(res['status'] == 200){
\t\t\t   load_data_bydate();
\t\t\t   \$('#csrf_token').val(res['token']);
\t\t\t   \$('#myform_data_update')[0].reset();
\t\t\t   

\t\t\t   //token
\t\t   }else{
\t\t\t   alertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t   ,function(){
\t\t\t\t   window.location.reload();
\t\t\t   });
\t\t   }
\t\t });
\t
   }

});
\$('#btn_save').click(function() {
\t
\tif(\$('#myform_data_senderandresive').valid()) {
\t \tvar form = \$('#myform_data_senderandresive');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_resive_Work_byhand.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t// location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {
\t\t\t\$(\"#bg_loader\").hide();
\t\t\tif(res['status'] == 505){
\t\t\t\t//console.log(res);
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\t//window.location.reload();
\t\t\t\t});
\t\t\t}else if(res['status'] == 200){
\t\t\t\tload_data_bydate();
\t\t\t\t\$('#csrf_token').val(res['token']);
\t\t\t\tif(\$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t}
\t\t\t\tif(\$(\"#reset_resive_form\").prop(\"checked\") == false && \$(\"#reset_send_form\").prop(\"checked\") == false){
\t\t\t\t\treset_resive_form();
\t\t\t\t\treset_send_form();
\t\t\t\t}
\t\t\t\t

\t\t\t\t//token
\t\t\t}else{
\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message
\t\t\t\t,function(){
\t\t\t\t\twindow.location.reload();
\t\t\t\t});
\t\t\t}
\t\t  });
     
\t}
});




\$('#myform_data_add_emp').validate({
\tonsubmit: false,
\tonkeyup: false,
\terrorClass: \"is-invalid\",
\thighlight: function (element) {
\t\tif (element.type == \"radio\" || element.type == \"checkbox\") {
\t\t\t\$(element).removeClass('is-invalid')
\t\t} else {
\t\t\t\$(element).addClass('is-invalid')
\t\t}
\t},
\trules: {
\t\t'emp_code_add': {
\t\t\trequired: true
\t\t},
\t\t'emp_mail_add': {
\t\t\trequired: true
\t\t},
\t\t'emp_name_add': {
\t\t\trequired: true
\t\t},
\t\t'emp_lname_add': {
\t\t\trequired: true
\t\t},
\t\t'dep_id_add': {
\t\t\trequired: true
\t\t}
\t  
\t},
\tmessages: {
\t\t'emp_code_add': {
\t\t  required: 'กรุณาระบุ รหัสพนักงาน.'
\t\t},
\t\t'emp_mail_add': {
\t\t  required: 'กรุณาระบุ อีเมลล์'
\t\t},
\t\t'emp_name_add': {
\t\t  required: 'กรุณาระบุ ชื่อพนักงาน'
\t\t},
\t\t'emp_lname_add': {
\t\t  required: 'กรุณาระบุ นามสกุล'
\t\t},
\t\t'dep_id_add': {
\t\t\trequired: 'กรุณาระบุ หน่วยงาน'
\t\t}
\t},
\terrorElement: 'span',
\terrorPlacement: function (error, element) {
\t\tvar placement = \$(element).data('error');
\t\t//console.log(placement);
\t\tif (placement) {
\t\t\t\$(placement).append(error)
\t\t} else {
\t\t\terror.insertAfter(element);
\t\t}
\t}
  });

  \$('#btn_add_emp').click(function() {
\t
\tif(\$('#myform_data_add_emp').valid()) {
\t \tvar form = \$('#myform_data_add_emp');
\t\t  //</link>var serializeData = form.serializeArray();
      \tvar serializeData = form.serialize();
\t\t  \$.ajax({
\t\t\tmethod: \"POST\",
\t\t\tdataType:'json',
\t\t\turl: \"ajax/ajax_save_employee_page_byhand.php\",
\t\t\tdata: serializeData,
\t\t\tbeforeSend: function() {
\t\t\t\t// setting a timeout
\t\t\t\t\$(\"#bg_loader\").show();
\t\t\t},
\t\t\terror: function (error) {
\t\t\t  alert('error; ' + eval(error));
\t\t\t  \$(\"#bg_loader\").hide();
\t\t\t location.reload();
\t\t\t}
\t\t  })
\t\t  .done(function( res ) {

\t\t\t\$(\"#bg_loader\").hide();
\t\t\tsetTimeout(function(){ 
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').hide();
\t\t\t}, 3000);
\t\t\tif(res['st'] == 'error'){
\t\t\t\t\$('#error').text(res['msg']);
\t\t\t\t\$('#success').hide();
\t\t\t\t\$('#error').show();
\t\t\t}else{
\t\t\t\t\$('#success').text(res['msg']);
\t\t\t\t\$('#success').show();
\t\t\t\t\$('#error').hide();
\t\t\t}
\t\t  });
     
\t}
});


\$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
\$district: \$('#sub_district_re'), // input ของตำบล
  \$amphoe: \$('#district_re'), // input ของอำเภอ
  \$province: \$('#province_re'), // input ของจังหวัด
  \$zipcode: \$('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      \$('#receiver_sub_districts_code').val('');
      \$('#receiver_districts_code').val('');
      \$('#receiver_provinces_code').val('');

      if(data) {
          \$('#sub_districts_code_re').val(data.district_code);
          \$('#districts_code_re').val(data.amphoe_code);
          \$('#provinces_code_re').val(data.province_code);
\t\t  //console.log(data);
      }
      
  }
});


\$('#dep_id_add').select2();
\$('#dep_id_send').select2();
\$('#emp_id_send').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getdataemployee_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\tconsole.log(e.params.data.id);
\tsetForm(e.params.data.id);
});
\$('#messenger_user_id').select2({
\tplaceholder: \"ค้นหาผู้ส่ง\",
\tajax: {
\t\turl: \"./ajax/ajax_getmessenger_select_search.php\",
\t\tdataType: \"json\",
\t\tdelay: 250,
\t\tprocessResults: function (data) {
\t\t\treturn {
\t\t\t\tresults : data
\t\t\t};
\t\t},
\t\tcache: true
\t}
}).on('select2:select', function(e) {
\t//console.log(e.params.data.id);
});

var tbl_data = \$('#tb_keyin').DataTable({ 
\t\"searching\": true,
\t \"fixedHeader\": {
        header: true,
    },
    \"Info\": false,
    \"language\": {
        \"emptyTable\": \"ไม่มีข้อมูล!\"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'action'},
        {'data': 'mr_work_byhand_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_round_name'},
        {'data': 'mr_cus_name'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'},
        {'data': 'mess'}
    ]
});

load_data_bydate();

\$('#work_barcode').keypress(function(event){
\t\t\tvar keycode = (event.keyCode ? event.keyCode : event.which);
\t\t\tif(keycode == '13'){
\t\t\t\tvar barcode = \$(this).val();
\t\t\t\t\$.ajax({
\t\t\t\t\turl: './ajax/ajax_get_data_bybacose_Byhand.php',
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata: {
\t\t\t\t\t\tbarcode\t: barcode,
\t\t\t\t\t\tpage\t: 'getdataBybarcode',
\t\t\t\t\t\tcsrf_token\t: \$('#csrf_token').val()
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\$('#csrf_token').val(res['token']);

\t\t\t\t\t\t//console.log(\"++++++++++++++\");
\t\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\tif(res['count']>0){
\t\t\t\t\t\t\t\tset_form_val_all(res['data'])
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\$('#btn-show-form-edit').attr('disabled','disabled');
\t\t\t\t\t\t\t\treset_price_form();
\t\t\t\t\t\t\t\treset_resive_form();
\t\t\t\t\t\t\t\treset_send_form();
\t\t\t\t\t\t\t\t\$(\"#detail_sender\").val('');
\t\t\t\t\t\t\t\t\$(\"#detail_receiver\").val('');
\t\t\t\t\t\t\t\t\$(\"#work_barcode\").val('');
\t\t\t\t\t\t\t\t\$(\"#work_barcode\").focus();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}); 
\t\t\t}
\t\t  });\t
function setForm(emp_code) {
\t\t\tvar emp_id = parseInt(emp_code);
\t\t\tconsole.log(emp_id);
\t\t\t\$.ajax({
\t\t\t\turl: './ajax/ajax_autocompress_name.php',
\t\t\t\ttype: 'POST',
\t\t\t\tdata: {
\t\t\t\t\tname_receiver_select: emp_id
\t\t\t\t},
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(res) {
\t\t\t\t\tconsole.log(\"++++++++++++++\");
\t\t\t\t\tif(res['status'] == 501){
\t\t\t\t\t\tconsole.log(res);
\t\t\t\t\t}else if(res['status'] == 200){
\t\t\t\t\t\t\$(\"#emp_send_data\").val(res.text_emp);
\t\t\t\t\t\t\$(\"#dep_id_send\").val(res.data.mr_department_id).trigger('change');
\t\t\t\t\t\tconsole.log(res.data.mr_department_id);
\t\t\t\t\t}else{
\t\t\t\t\t\talertify.alert('ผิดพลาด',\"  \"+res.message,function(){window.location.reload();});
\t\t\t\t\t}
\t\t\t\t}
\t\t\t})
\t\t}

\t\t\$(\"#name_re\").autocomplete({
            source: function( request, response ) {
                
                \$.ajax({
                    url: \"ajax/ajax_getcustommer_search.php\",
                    type: 'post',
                    dataType: \"json\",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
\t\t\t\t\t\tconsole.log(data);
                    }
                });
            },
            select: function (event, ui) {
                \$('#name_re').val(ui.item.fullname); // display the selected text
                \$('#address_re').val(ui.item.mr_address); // save selected id to input
                \$('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
\t\t\t\t//console.log(ui.item);
                return false;
            },
            focus: function(event, ui){
                \$( \"#name_re\" ).val( ui.item.fullname );
                \$( \"#address_re\" ).val( ui.item.mr_address );
                \$( \"#tel_re\" ).val( ui.item.mr_cus_tel );
                return false;
            },
        });
{% endblock %}
{% block javaScript %}

function chang_dep_id() {
\tvar dep = \$('#dep_id_send').select2('data'); 
\tvar emp = \$('#emp_id_send').select2('data'); 
\t
\tconsole.log(emp);
\tif(emp[0].id!=''){
\t\t\$(\"#emp_send_data\").val(emp[0].text+'\\\\n'+dep[0].text);
\t}else{
\t\t\$(\"#emp_send_data\").val(dep[0].text);
\t}
\t
}

function load_data_bydate() {
\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\turl: \"ajax/ajax_load_Work_resive_byHand_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\tif(res['status'] == 200){
\t\t\t\$('#tb_keyin').DataTable().clear().draw();
\t\t\t\$('#tb_keyin').DataTable().rows.add(res.data).draw();

\t\t}
\t  });
}

function reset_send_form() {
\t\$('input[name=\"type_send\"]').attr('checked', false);
\t\$('#round').val(\"\");
\t\$('#emp_id_send').val(\"\").trigger('change');
\t\$('#emp_send_data').val(\"\");
}

function cancle_work(id) {
\talertify.confirm('ยืนยันการลบ', 'กด \"OK\" เพื่อยกเลิกการส่ง',
\tfunction(){ 
\t\t\$.ajax({
\t\tmethod: \"POST\",
\t\tdataType:'json',
\t\tdata:{
\t\t\tid \t\t: id,
\t\t\tpage\t:'cancle'
\t\t},
\t\turl: \"ajax/ajax_load_Work_resive_byHand_out.php\",
\t\tbeforeSend: function() {
\t\t\t// setting a timeout
\t\t\t\$(\"#bg_loader\").show();
\t\t},
\t\terror: function (error) {
\t\t  alert('error; ' + eval(error));
\t\t  \$(\"#bg_loader\").hide();
\t\t// location.reload();
\t\t}
\t  })
\t  .done(function( res ) {
\t\t\$(\"#bg_loader\").hide();
\t\t\tload_data_bydate();
\t  });
\t  
\t}, function(){ 
\t\talertify.error('Cancel')
\t});
}
function reset_resive_form() {
\t\$(\"#name_re\").val(\"\");
\t\$(\"#lname_re\").val(\"\");
\t\$(\"#tel_re\").val(\"\");
\t\$(\"#address_re\").val(\"\");
\t\$(\"#sub_districts_code_re\").val(\"\");
\t\$(\"#districts_code_re\").val(\"\");
\t\$(\"#provinces_code_re\").val(\"\");
\t\$(\"#sub_district_re\").val(\"\");
\t\$(\"#district_re\").val(\"\");
\t\$(\"#province_re\").val(\"\");
\t\$(\"#post_code_re\").val(\"\");
\t\$(\"#quty\").val(\"1\");
\t\$(\"#topic\").val(\"\");
\t\$(\"#work_remark\").val(\"\");
}
function print_option(type){
\tconsole.log(type);
\tif(type == 1 ){
\t\tconsole.log(11);
\t\t\$(\"#form_print\").attr('action', 'print_peper_byhand.php');
\t\t\$('#form_print').submit();
\t}else if(type == 2){
\t\tconsole.log(22);
\t\t\$(\"#form_print\").attr('action', 'print_cover_byhand.php');
\t\t\$('#form_print').submit();
\t}else{
\t\talert('----');
\t}

}
function set_form_val_all(data) {
\t\$('#round').val(data.mr_round_id);
\t\$('#quty').val(data.quty);
\t\$('#topic').val(data.topic);
\t\$('#work_remark').val(data.work_remark);
\t\$('#update_emp_send_data').val(data.mr_send_emp_detail);
\t\$(\"input[value=\"+data.mr_work_byhand_type_id+\"]\").attr('checked', true); 
\t\$('#update_mr_work_main_id').val(data.mr_work_main_id);
\t\$('#update_mr_work_byhand_id').val(data.mr_work_byhand_id);


\tvar detail_receiver = '';
\tdetail_receiver += data.mr_cus_name;
\tdetail_receiver += ' '+data.mr_cus_lname;
\tdetail_receiver += ' '+data.mr_address;
\tdetail_receiver += ' '+data.mr_sub_districts_name;
\tdetail_receiver += ' '+data.mr_districts_name;
\tdetail_receiver += ' '+data.mr_provinces_name;
\tdetail_receiver += ' '+data.mr_post_code;
\tdetail_receiver += '  โทร: '+data.mr_cus_tel;
\t\$('#update_emp_resive_data').val(detail_receiver);

}

{% endblock %}
{% block Content2 %}
<div  class=\"container-fluid\">



  <br>
  <div class=\"tab-content\" id=\"myTabContent\">
\t<div class=\"tab-pane fade show active\" id=\"tab-update\" role=\"tabpanel\" aria-labelledby=\"home-tab\">
\t<form id=\"myform_data_update\">
\t\t<input type=\"hidden\" id=\"update_mr_work_main_id\" name=\"update_mr_work_main_id\">
\t\t<input type=\"hidden\" id=\"update_mr_work_byhand_id\" name=\"mr_work_byhand_id\">
\t\t<input type=\"hidden\" name=\"csrf_token\" id=\"csrf_token\" value=\"{{csrf_token}}\">
\t\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>รับงาน BY HAND</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<div class=\"form-group row\">
\t\t<div class=\"col-sm-4\">
\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"work_barcode\" name=\"work_barcode\" placeholder=\"Enter Barcode\">
\t\t</div>
\t</div>

\t<div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลรายละเอียดงาน</h5>
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t<table style=\"width: 100%;\" class=\"\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_send\"></span>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการรับส่ง: </span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_type_send\"></span><br>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send1\" value=\"1\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งเอกสาร </span><br>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send2\" value=\"2\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > รับเอกสาร</span><br>
\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send3\" value=\"3\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งและรับเอกสารกลับ</span>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t  </div>
\t\t\t\t\t  <hr>
\t\t\t\t\t 

\t\t\t\t\t  <hr>
\t\t\t\t\t  <h5 class=\"card-title\">แมสเซ็นเจอร์ (ทีวิ่งงานงาน)</h5>
\t\t\t\t\t  <div class=\"table-responsive\">
\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >ชื่อแมส:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_messenger_user_id\"></span>
\t\t\t\t\t\t\t\t\t<select data-error=\"#err_messenger_user_id\" class=\"form-control form-control-sm\" id=\"messenger_user_id\" name=\"messenger_user_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t<option value=\"\">1</option>
\t\t\t\t\t\t\t\t\t\t<option value=\"1\">2</option>
\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td><br>
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"1\" id=\"mr_work_byhand_payment_type_id1\" name=\"mr_work_byhand_payment_type_id\" checked>  ราคาปกติ  
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" value=\"2\" id=\"mr_work_byhand_payment_type_id2\" name=\"mr_work_byhand_payment_type_id\">  ราคาจ่ายแบบ Spare  
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t  </div>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <div class=\"col-12 text-center\">
\t\t\t\t\t  \t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_update\">บันทึกข้อมูล </button>
\t\t\t\t\t  </div>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t\t  <br>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t  <table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t  <td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t  <td>
\t\t\t\t\t\t\t\t  <textarea class=\"form-control\" id=\"update_emp_send_data\" name=\"update_emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t  </td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >รายละเอียดผู้ผู้รับ:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"update_emp_resive_data\" name=\"update_emp_resive_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t  </table>
\t\t\t\t\t</div>
\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลเอกสาร</h5>
\t\t\t\t\t\t<table style=\"width: 100%;\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\" readonly>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >หัวเรื่อง:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_topic\"></span>
\t\t\t\t\t\t\t\t\t<input id=\"topic\" name=\"topic\" data-error=\"#err_topic\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\" readonly>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_work_remark\"></span>
\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\" readonly></textarea>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t  </div><br>
\t\t\t\t\t  <div class=\"form-group text-center\">
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\t\t
\t\t\t\t\t\t{#
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_send_form();\">ล้างข้อมูลผู้ส่ง </button>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_resive_form();\">ล้างข้อมูลผู้รับ </button>
\t\t\t\t\t\t#}

\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t\t
\t\t</div>
\t</form>
\t</div>
\t<div class=\"tab-pane fade\" id=\"tab-key\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">
\t<div class=\"row\">
\t\t<div class=\"col\">
\t\t\t<div class=\"\">
\t\t\t\t<label><h3><b>สั่งงาน BY HAND</b></h3></label><br>
\t\t\t\t<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
\t\t   </div>\t
\t\t</div>
\t</div>
\t<br>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"card\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<form id=\"myform_data_senderandresive\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">รอบการนำส่งเอกสารประจำวัน</h5>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลรายละเอียดงาน</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table style=\"width: 100%;\" class=\"\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >วันที่นำส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_date_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_date_send\" name=\"date_send\" id=\"date_send\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th class=\"align-top\"><span class=\"text-muted font_mini\" >ประเภทการรับส่ง: </span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_type_send\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send1\" value=\"1\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งเอกสาร </span><br>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send2\" value=\"2\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > รับเอกสาร</span><br>
\t\t\t\t\t\t\t\t\t\t\t<input data-error=\"#err_type_send\" type=\"radio\" id=\"type_send3\" value=\"3\" name=\"type_send\" class=\"\"><span class=\"text-muted font_mini\" > ส่งและรับเอกสารกลับ</span>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td><span class=\"text-muted font_mini\" >รอบการรับส่ง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round\" name=\"round\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้สั่งงาน</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสพนักงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group input-group-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_id_send\"></span><br>
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_emp_id_send\" class=\"form-control form-control-sm\" id=\"emp_id_send\" name=\"emp_id_send\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">ค้นหาพนักงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t</select>\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >รหัสหน่วยงาน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>

\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_dep_id_send\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_dep_id_send\" class=\"form-control form-control-sm\" id=\"dep_id_send\" name=\"dep_id_send\" onchange=\"chang_dep_id();\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t{% for d in department %}
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ d.mr_department_id }}\">{{ d.mr_department_code }} - {{ d.mr_department_name }}</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t</select>\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียดผู้สั่งงาน: </span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" id=\"emp_send_data\" name=\"emp_send_data\" rows=\"3\" readonly></textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t<h5 class=\"card-title\">แมสเซ็นเจอร์ (ทีวิ่งงานงาน)</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\"><span class=\"text-muted font_mini\" >ชื่อแมส:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_messenger_user_id\"></span>
\t\t\t\t\t\t\t\t\t\t\t<select data-error=\"#err_messenger_user_id\" class=\"form-control form-control-sm\" id=\"messenger_user_id\" name=\"messenger_user_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">1</option>
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\">2</option>
\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"2\">2</option>
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div><br>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t



\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลผู้รับ</h5>
\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t<table class=\"\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" ><span class=\"text-muted font_mini\" >ชื่อผู้รับ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_name_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input name=\"name_re\" id=\"name_re\" data-error=\"#err_name_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"นาย ณัฐวุฒิ\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >นามสกุล:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_lname_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"lname_re\" name=\"lname_re\" data-error=\"#err_lname_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"สามพ่วงบุญ\" val=\"\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เบอร์มือถือ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_tel_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"tel_re\" name=\"tel_re\" data-error=\"#err_tel_re\" class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >ที่อยู่​:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_address_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"address_re\" name=\"address_re\" data-error=\"#err_address_re\"  class=\"form-control\"  rows=\"2\">-</textarea>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"sub_districts_code_re\" id=\"sub_districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"districts_code_re\" id=\"districts_code_re\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"provinces_code_re\" id=\"provinces_code_re\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>\t
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >แขวง/ตำบล:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_sub_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"sub_district_re\" name=\"sub_district_re\" data-error=\"#err_sub_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >เขต/อำเภอ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_district_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"district_re\" name=\"district_re\" data-error=\"#err_district_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >จังหวัด:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_province_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"province_re\" name=\"province_re\" data-error=\"#err_province_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รหัสไปรษณีย์:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_post_code_re\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"post_code_re\" name=\"post_code_re\" data-error=\"#err_post_code_re\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t\t<h5 class=\"card-title\">ข้อมูลเอกสาร</h5>
\t\t\t\t\t\t\t\t<table style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >จำนวน:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_quty\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"quty\" name=\"quty\" data-error=\"#err_quty\"  value=\"1\" class=\"form-control form-control-sm\" type=\"number\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td style=\"width: 180px;\" class=\"align-top\"><span class=\"text-muted font_mini\" >หัวเรื่อง:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_topic\"></span>
\t\t\t\t\t\t\t\t\t\t\t<input id=\"topic\" name=\"topic\" data-error=\"#err_topic\"  class=\"form-control form-control-sm\" type=\"text\" placeholder=\"-\">
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"align-top\"><span class=\"text-muted font_mini\" >รายละเอียด/หมายเหตุ:</span></td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"box_error\" id=\"err_\"></span>
\t\t\t\t\t\t\t\t\t\t\t<textarea id=\"work_remark\" name=\"work_remark\" data-error=\"#\"  class=\"form-control\" id=\"\" rows=\"4\"></textarea>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</div><br>
\t\t\t\t\t\t\t<div class=\"form-group text-center\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_send_form\" value=\"option1\">คงข้อมูลผู้ส่ง
\t\t\t\t\t\t\t\t<input class=\"\" type=\"checkbox\" id=\"reset_resive_form\" value=\"option1\">คงข้อมูลผู้รับ
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" id=\"btn_save\">บันทึกข้อมูล </button>
\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_send_form();\">ล้างข้อมูลผู้ส่ง </button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-sm\" onclick=\"reset_resive_form();\">ล้างข้อมูลผู้รับ </button>
\t\t\t\t\t\t\t\t#}

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>\t\t\t\t\t
\t\t</div>
\t</div>
\t</div>

\t</div>
\t</div>


<div class=\"row\">
\t<div class=\"col-md-12 text-right\">
\t<form id=\"form_print\" action=\"pp.php\" method=\"post\" target=\"_blank\">
\t\t<hr>
\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_round\"></span>
\t\t\t\t\t<select data-error=\"#err_round\" class=\"form-control form-control-sm\" id=\"round_printreper\" name=\"round_printreper\">
\t\t\t\t\t\t<option value=\"\">กรุณาเลือกรอบ</option>
\t\t\t\t\t\t{% for r in round %}
\t\t\t\t\t\t<option value=\"{{r.mr_round_id}}\">{{r.mr_round_name}}</option>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"box_error\" id=\"err_date_report\"></span>
\t\t\t\t\t<input data-error=\"#err_date_report\" name=\"date_report\" id=\"date_report\" class=\"form-control form-control-sm\" type=\"text\" value=\"{{today}}\" placeholder=\"{{today}}\">
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<button onclick=\"print_option(1);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบงาน</button>
\t\t\t\t\t<button onclick=\"print_option(2);\" type=\"button\" class=\"btn btn-sm btn-outline-secondary\" id=\"\">พิมพ์ใบคุม</button>
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</form>
\t</div>\t
</div>



<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<hr>
\t\t<h5 class=\"card-title\">รายการเอกสาร</h5>
\t\t<table class=\"table\" id=\"tb_keyin\">
\t\t\t<thead class=\"thead-light\">
\t\t\t  <tr>
\t\t\t\t<th width=\"5%\" scope=\"col\">#</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">Update</th>
\t\t\t\t<th width=\"5%\" scope=\"col\">ประเภท</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">วันที่</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">เลขที่เอกสาร</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">สถานะ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ชื่อผู้ส่ง</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">รอบ</th>
\t\t\t\t<th width=\"10%\" scope=\"col\">ผู้รับ</th>
\t\t\t\t<th width=\"15%\" scope=\"col\">ที่อยู่</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">เบอร์โทร</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">หมายเหตุ</th>
\t\t\t\t<th width=\"30%\"scope=\"col\">แมส</th>
\t\t\t  </tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t
\t\t\t</tbody>
\t\t</table>
\t</div>
</div>
</div>





<div id=\"bg_loader\" style=\"display: none;\">
\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
</div>






<!-- Modal -->
<div class=\"modal fade\" id=\"add_emp_modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLabel\">เพิ่มข้อมูลพนักงาน</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t<form id=\"myform_data_add_emp\">
\t\t\t
\t\t\t
\t\t\t<div class=\"form-row\">
\t\t\t\t
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_code_add\"></span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<span class=\"box_error\" id=\"err_emp_mail_add\"></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col-3\">
\t\t\t\t\t<input id=\"emp_code_add\" name=\"emp_code_add\" data-error=\"#err_emp_code_add\"  type=\"text\" class=\"form-control form-control-sm\" placeholder=\"รหัสพนักงาน\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-9\">
\t\t\t\t\t<input id=\"emp_mail_add\" name=\"emp_mail_add\" data-error=\"#err_emp_mail_add\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"อีเมลล์\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<br>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t<span class=\"box_error\" id=\"err_emp_name_add\"></span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t<span class=\"box_error\" id=\"err_emp_lname_add\"></span>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t<input id=\"emp_name_add\" name=\"emp_name_add\" data-error=\"#err_emp_name_add\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"ชื่อ\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col\">
\t\t\t\t<input id=\"emp_lname_add\" name=\"emp_lname_add\" data-error=\"#err_emp_lname_add\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"นามสกุล\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<br>
\t\t\t<div class=\"form-row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t <span class=\"box_error\" id=\"err_dep_id_add\"></span>
\t\t\t\t<select data-error=\"#err_dep_id_add\" class=\"form-control form-control-sm\" id=\"dep_id_add\" name=\"dep_id_add\" onchange=\"chang_dep_id();\" style=\"width:100%;\">
\t\t\t\t\t<option value=\"\" selected disabled >กรุณาเลือกหน่วยงาน</option>
\t\t\t\t\t{% for d in department %}
\t\t\t\t\t<option value=\"{{ d.mr_department_id }}\">{{ d.mr_department_code }} - {{ d.mr_department_name }}</option>
\t\t\t\t\t{% endfor %}
\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<input type=\"hidden\" name=\"page\" id=\"page\" value=\"add_emp\">
\t\t</form>
\t\t
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">ยกเลิก</button>
        <button id=\"btn_add_emp\" type=\"button\" class=\"btn btn-primary\">บันทึกข้อมูล</button>
\t\t\t 
      </div>
\t  <div class=\"form-row\">
\t\t<div class=\"col\">
\t\t\t<div id=\"error\" class=\"alert alert-danger\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t  </div>
\t\t\t<div id=\"success\" class=\"alert alert-success\" role=\"alert\" style=\"display: none;\">
\t\t\t\t...
\t\t\t</div>
\t\t</div>
\t</div>
    </div>
  </div>
</div>





{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/resive_work_byHand_out.tpl", "/var/www/html/web_8/mailroom_tmb/web/templates/mailroom/resive_work_byHand_out.tpl");
    }
}

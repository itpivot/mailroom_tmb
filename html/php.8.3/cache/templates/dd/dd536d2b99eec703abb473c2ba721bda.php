<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/confirmSendBranch.tpl */
class __TwigTemplate_5ba894476b300c79047ff94c503bec85 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg3' => [$this, 'block_menu_msg3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/confirmSendBranch.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PivotSend List ";
    }

    // line 5
    public function block_menu_msg3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        height: 100%;
        
    }

    


    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 800px;
        overflow: auto;
        margin-bottom: 50px;
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    .fixedContainer {
        position: fixed;
        width: 100%;
        padding: 10px 10px;
        left: 0;
        bottom: 0;
    }
  

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }

";
    }

    // line 111
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 112
        echo "    \$('#frmConfirm').submit(function(e) {
        e.preventDefault();

        \$.ajax({
            url: './confirmSendBranch.php',
            type: 'POST',
            data: \$(this).serialize(),
            success: function(resp) {
                if(parseInt(resp) == 1) {
                    if (confirm(\"ยืนยันการส่งเอกสาร!\")) {
                        var result_confirm = updateWorkOrders(\$('#frmConfirm').serializeArray());
                        result_confirm.done(function(rs) {
                            console.log(rs)
                             if(rs == \"success\") {
                                 window.location.href = \"rate_send_branch.php?id=";
        // line 126
        if ((($context["main_id"] ?? null) == "")) {
            echo twig_escape_filter($this->env, twig_join_filter(($context["work_id"] ?? null), ","), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, ($context["main_id"] ?? null), "html", null, true);
        }
        echo "\";
                                 //window.location.href = \"send_branch_list.php\";
                             }
                        });
                    }
                } else {
                    alert('ข้อมูลไม่ถูกต้องถูกต้อง');
                    \$(\"#username\").val('');
                    \$(\"#password\").val('');
                }
            }
        });
    });

";
    }

    // line 142
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 143
        echo "    function updateWorkOrders(arr) {
        return \$.post('./ajax/ajax_updateBranchReceived.php', { data: JSON.stringify(arr) });
    }

";
    }

    // line 149
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 150
        echo "
<div class=\"container\">
    <form action=\"confirmSendBranch.php\" method=\"POST\" id=\"frmConfirm\">
        <h3>ยืนยันการรับเอกสาร</h3>
        <div class=\"form-group\">
            <label for=\"username\">รหัสพนักงาน</label>
            <input ";
        // line 156
        echo twig_escape_filter($this->env, ($context["txt_disabled"] ?? null), "html", null, true);
        echo " type=\"text\"  class=\"form-control\" id=\"username\" name=\"username\" />
            <input type=\"hidden\" value=\"";
        // line 157
        echo twig_escape_filter($this->env, ($context["type"] ?? null), "html", null, true);
        echo "\" name=\"type\" id=\"type\">
            <input type=\"hidden\" value=\"";
        // line 158
        echo twig_escape_filter($this->env, ($context["main_id"] ?? null), "html", null, true);
        echo "\" name=\"main_id\" id=\"main_id\">
            ";
        // line 159
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["work_id"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["hid"]) {
            // line 160
            echo "                <input type=\"hidden\" value=\"";
            echo twig_escape_filter($this->env, $context["hid"], "html", null, true);
            echo "\" name=\"arrId[]\" id=\"arrId_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 160), "html", null, true);
            echo "\">
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hid'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 162
        echo "        </div>
        <div class=\"form-group\">
            <label for=\"password\">รหัสผ่าน</label>
            <input ";
        // line 165
        echo twig_escape_filter($this->env, ($context["txt_disabled"] ?? null), "html", null, true);
        echo " type=\"password\"  class=\"form-control\" id=\"password\" name=\"password\" />
        </div>
        <div class=\"form-group\">
\t\t";
        // line 168
        if ((($context["time_dif"] ?? null) > 0)) {
            // line 169
            echo "            <button type=\"submit\" class=\"btn btn-primary btn-block font-weight-bold\">ยืนยันการรับเอกสาร</button>
\t\t";
        } else {
            // line 171
            echo "\t\t<div class=\"alert alert-danger\" role=\"alert\">
\t\t  ไม่สามารถส่งได้ : <b> เนื่องจากเลยเวลา  ";
            // line 172
            echo twig_escape_filter($this->env, ($context["time_end"] ?? null), "html", null, true);
            echo "</b>
\t\t</div>
\t\t";
        }
        // line 175
        echo "\t\t
        </div>
    </form>
</div>

";
    }

    // line 183
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 185
        if ((($context["debug"] ?? null) != "")) {
            // line 186
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 188
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/confirmSendBranch.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 188,  339 => 186,  337 => 185,  330 => 183,  321 => 175,  315 => 172,  312 => 171,  308 => 169,  306 => 168,  300 => 165,  295 => 162,  276 => 160,  259 => 159,  255 => 158,  251 => 157,  247 => 156,  239 => 150,  235 => 149,  227 => 143,  223 => 142,  200 => 126,  184 => 112,  180 => 111,  77 => 10,  73 => 9,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        height: 100%;
        
    }

    


    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 800px;
        overflow: auto;
        margin-bottom: 50px;
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    .fixedContainer {
        position: fixed;
        width: 100%;
        padding: 10px 10px;
        left: 0;
        bottom: 0;
    }
  

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }

{% endblock %}

{% block domReady %}
    \$('#frmConfirm').submit(function(e) {
        e.preventDefault();

        \$.ajax({
            url: './confirmSendBranch.php',
            type: 'POST',
            data: \$(this).serialize(),
            success: function(resp) {
                if(parseInt(resp) == 1) {
                    if (confirm(\"ยืนยันการส่งเอกสาร!\")) {
                        var result_confirm = updateWorkOrders(\$('#frmConfirm').serializeArray());
                        result_confirm.done(function(rs) {
                            console.log(rs)
                             if(rs == \"success\") {
                                 window.location.href = \"rate_send_branch.php?id={% if main_id == '' %}{{ work_id|join(',') }}{% else %}{{main_id}}{% endif %}\";
                                 //window.location.href = \"send_branch_list.php\";
                             }
                        });
                    }
                } else {
                    alert('ข้อมูลไม่ถูกต้องถูกต้อง');
                    \$(\"#username\").val('');
                    \$(\"#password\").val('');
                }
            }
        });
    });

{% endblock %}

{% block javaScript %}
    function updateWorkOrders(arr) {
        return \$.post('./ajax/ajax_updateBranchReceived.php', { data: JSON.stringify(arr) });
    }

{% endblock %}

{% block Content %}

<div class=\"container\">
    <form action=\"confirmSendBranch.php\" method=\"POST\" id=\"frmConfirm\">
        <h3>ยืนยันการรับเอกสาร</h3>
        <div class=\"form-group\">
            <label for=\"username\">รหัสพนักงาน</label>
            <input {{txt_disabled}} type=\"text\"  class=\"form-control\" id=\"username\" name=\"username\" />
            <input type=\"hidden\" value=\"{{ type }}\" name=\"type\" id=\"type\">
            <input type=\"hidden\" value=\"{{ main_id }}\" name=\"main_id\" id=\"main_id\">
            {% for hid in work_id %}
                <input type=\"hidden\" value=\"{{ hid }}\" name=\"arrId[]\" id=\"arrId_{{ loop.index }}\">
            {% endfor %}
        </div>
        <div class=\"form-group\">
            <label for=\"password\">รหัสผ่าน</label>
            <input {{txt_disabled}} type=\"password\"  class=\"form-control\" id=\"password\" name=\"password\" />
        </div>
        <div class=\"form-group\">
\t\t{% if time_dif > 0 %}
            <button type=\"submit\" class=\"btn btn-primary btn-block font-weight-bold\">ยืนยันการรับเอกสาร</button>
\t\t{% else %}
\t\t<div class=\"alert alert-danger\" role=\"alert\">
\t\t  ไม่สามารถส่งได้ : <b> เนื่องจากเลยเวลา  {{  time_end }}</b>
\t\t</div>
\t\t{% endif %}
\t\t
        </div>
    </form>
</div>

{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}

", "messenger/confirmSendBranch.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\messenger\\confirmSendBranch.tpl");
    }
}

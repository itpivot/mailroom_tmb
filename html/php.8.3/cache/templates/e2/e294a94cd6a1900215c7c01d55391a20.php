<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/receive_hub.tpl */
class __TwigTemplate_17697c2dbb6b80e7f387c3b8590a8dab extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg3' => [$this, 'block_menu_msg3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/receive_hub.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PivotSend List ";
    }

    // line 5
    public function block_menu_msg3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
\t#loading{
\t\t\tdisplay: block;
\t\t\tmargin-left: auto;
\t\t\tmargin-right: auto;
\t\t\twidth: 50%;
\t\t
\t}
";
    }

    // line 105
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 106
        echo "    \$('.block_btn').hide();

    var receive_list = {};
    getReceiveHub();

    

";
    }

    // line 115
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 116
        echo "    var selectChoice = [];

    function getReceiveHub(branch_id)
    {

        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: \"GET\",
            cache: false,
            data: {
                type: 'get',
                branch_id: branch_id
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t
\t\t\t\t\$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);
            },
            complete: function() {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }


    function updateStatus(wId, actions)
    {
        var branch_id = \$('#lst_branch').val() || null;
        \$.ajax({
            url: './ajax/ajax_getReceiveHub.php',
            type: 'POST',
            cache: false,
            data: {
                wId: wId,
                action: actions,
                branch_id: branch_id,
                type: 'update'
            },
            dataType: 'json',
            beforeSend: function () {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function (resp) {
                // result
               \$('.result_bch').empty();
               \$('p span#counter_works').text(resp['counter']);
               \$('.result_bch').html(resp['data']);

               selectChoice.length = 0;
               \$('#btn_checked').html('เลือกทั้งหมด');
               \$('.block_btn').hide();
            },
            complete: function () {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }

    function selectedCard(wId, elm) {
        var foundArr = selectChoice.indexOf(wId);
        if (\$('#' + elm.id).is(':checked')) {
            if (foundArr == -1) {
                selectChoice.push(wId)
            }
        } else {
            if (foundArr != -1) {
                selectChoice.splice(foundArr, 1);
            }
        }


        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }
        console.log(selectChoice)
    }

    function checkChoice() {

        \$('.check_all').each(function (i, elm) {

            var wId = parseInt(\$('#' + elm.id).attr('data-value'));
            var foundArr = selectChoice.indexOf(wId);

            \$(elm).prop('checked', !elm.checked);
            if (elm.checked) {
                if (foundArr == -1) {
                    selectChoice.push(wId)
                } 
            } else {
                if (foundArr != -1) {
                    selectChoice.splice(foundArr, 1);
                }
            }
        });
        console.log(selectChoice)

        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }

    }

    function updateStatusAll(actions) {
        var branch_id = \$('#lst_branch').val() || null;
        \$.ajax({
            url: './ajax/ajax_getReceiveHub.php',
            type: 'POST',
            cache: false,
            data: {
                wId: JSON.stringify(selectChoice),
                action: actions,
                branch_id: branch_id,
                type: 'update_hub_all'
            },
            dataType: 'json',
            beforeSend: function () {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function (resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            },
            complete: function () {
                // loading hide
\t\t\t\t\$('#loading').hide();
            }
        });
    }

";
    }

    // line 276
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 277
        echo "
<div class=\"content_bch\">
<h4 class=\"text-center text-muted\">รับเอกสารที่ Hub </h4>
    <div class=\"header_bch\">
        <div class=\"form-group\">
            <p class=\"font-weight-bold text-muted\">ทั้งหมด <span id=\"counter_works\">0</span> งาน </p>
            <select name=\"lst_branch\" id=\"lst_branch\" class=\"form-control\" onchange=\"getReceiveHub(this.value);\">
                <option value=\"\">-- เลือกสาขา --</option>
                ";
        // line 285
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 286
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 286), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 286), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 288
        echo "            </select>
        </div>
    </div>
     <button type=\"button\" class=\"btn btn-secondary btn-block my-2\" id=\"btn_checked\" onclick=\"checkChoice();\">เลือกทั้งหมด</button>
    <div class=\"block_btn my-2\">
        <button type=\"button\" id=\"btn_receive\" class=\"btn btn-primary btn-block\" onclick=\"updateStatusAll(1);\">รับเอกสารทั้งหมด</button>
        <button type=\"button\" id=\"btn_not_found\" class=\"btn btn-warning btn-block\" onclick=\"updateStatusAll(2);\">ไม่พบเอกสาร</button>
    </div>
\t<img id=\"loading\" src=\"../themes/images/loading.gif\">
    <div class=\"result_bch\">
        
    </div>
</div>
<div class=\"footer_bch\">
    <a href=\"send_branch_list.php\" class=\"btn btn-secondary btn-block\">ไปหน้าส่งเอกสาร</a>
</div>


";
    }

    // line 309
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 311
        if ((($context["debug"] ?? null) != "")) {
            // line 312
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 314
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/receive_hub.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  421 => 314,  415 => 312,  413 => 311,  406 => 309,  384 => 288,  373 => 286,  369 => 285,  359 => 277,  355 => 276,  193 => 116,  189 => 115,  178 => 106,  174 => 105,  77 => 10,  73 => 9,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
\t#loading{
\t\t\tdisplay: block;
\t\t\tmargin-left: auto;
\t\t\tmargin-right: auto;
\t\t\twidth: 50%;
\t\t
\t}
{% endblock %}

{% block domReady %}
    \$('.block_btn').hide();

    var receive_list = {};
    getReceiveHub();

    

{% endblock %}

{% block javaScript %}
    var selectChoice = [];

    function getReceiveHub(branch_id)
    {

        \$.ajax({
            url: \"./ajax/ajax_getReceiveHub.php\",
            type: \"GET\",
            cache: false,
            data: {
                type: 'get',
                branch_id: branch_id
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t
\t\t\t\t\$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);
            },
            complete: function() {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }


    function updateStatus(wId, actions)
    {
        var branch_id = \$('#lst_branch').val() || null;
        \$.ajax({
            url: './ajax/ajax_getReceiveHub.php',
            type: 'POST',
            cache: false,
            data: {
                wId: wId,
                action: actions,
                branch_id: branch_id,
                type: 'update'
            },
            dataType: 'json',
            beforeSend: function () {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function (resp) {
                // result
               \$('.result_bch').empty();
               \$('p span#counter_works').text(resp['counter']);
               \$('.result_bch').html(resp['data']);

               selectChoice.length = 0;
               \$('#btn_checked').html('เลือกทั้งหมด');
               \$('.block_btn').hide();
            },
            complete: function () {
                // loading hide
\t\t\t\t\$('#loading').hide();
            } 
        });
    }

    function selectedCard(wId, elm) {
        var foundArr = selectChoice.indexOf(wId);
        if (\$('#' + elm.id).is(':checked')) {
            if (foundArr == -1) {
                selectChoice.push(wId)
            }
        } else {
            if (foundArr != -1) {
                selectChoice.splice(foundArr, 1);
            }
        }


        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }
        console.log(selectChoice)
    }

    function checkChoice() {

        \$('.check_all').each(function (i, elm) {

            var wId = parseInt(\$('#' + elm.id).attr('data-value'));
            var foundArr = selectChoice.indexOf(wId);

            \$(elm).prop('checked', !elm.checked);
            if (elm.checked) {
                if (foundArr == -1) {
                    selectChoice.push(wId)
                } 
            } else {
                if (foundArr != -1) {
                    selectChoice.splice(foundArr, 1);
                }
            }
        });
        console.log(selectChoice)

        if (selectChoice.length > 0) {
            \$('.block_btn').fadeIn();
            \$('#btn_checked').html('ยกเลิก');
        } else {
            \$('.block_btn').fadeOut();
            \$('#btn_checked').html('เลือกทั้งหมด');
        }

    }

    function updateStatusAll(actions) {
        var branch_id = \$('#lst_branch').val() || null;
        \$.ajax({
            url: './ajax/ajax_getReceiveHub.php',
            type: 'POST',
            cache: false,
            data: {
                wId: JSON.stringify(selectChoice),
                action: actions,
                branch_id: branch_id,
                type: 'update_hub_all'
            },
            dataType: 'json',
            beforeSend: function () {
                // loading show
\t\t\t\t\$('#loading').show();
\t\t\t\t\$('.result_bch').html('');
            },
            success: function (resp) {
                // result
                \$('.result_bch').empty();
                \$('p span#counter_works').text(resp['counter']);
                \$('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                \$('#btn_checked').html('เลือกทั้งหมด');
                \$('.block_btn').hide();
            },
            complete: function () {
                // loading hide
\t\t\t\t\$('#loading').hide();
            }
        });
    }

{% endblock %}

{% block Content %}

<div class=\"content_bch\">
<h4 class=\"text-center text-muted\">รับเอกสารที่ Hub </h4>
    <div class=\"header_bch\">
        <div class=\"form-group\">
            <p class=\"font-weight-bold text-muted\">ทั้งหมด <span id=\"counter_works\">0</span> งาน </p>
            <select name=\"lst_branch\" id=\"lst_branch\" class=\"form-control\" onchange=\"getReceiveHub(this.value);\">
                <option value=\"\">-- เลือกสาขา --</option>
                {% for b in branch %}
                    <option value=\"{{ b.mr_branch_id }}\">{{ b.mr_branch_name }}</option>
                {% endfor %}
            </select>
        </div>
    </div>
     <button type=\"button\" class=\"btn btn-secondary btn-block my-2\" id=\"btn_checked\" onclick=\"checkChoice();\">เลือกทั้งหมด</button>
    <div class=\"block_btn my-2\">
        <button type=\"button\" id=\"btn_receive\" class=\"btn btn-primary btn-block\" onclick=\"updateStatusAll(1);\">รับเอกสารทั้งหมด</button>
        <button type=\"button\" id=\"btn_not_found\" class=\"btn btn-warning btn-block\" onclick=\"updateStatusAll(2);\">ไม่พบเอกสาร</button>
    </div>
\t<img id=\"loading\" src=\"../themes/images/loading.gif\">
    <div class=\"result_bch\">
        
    </div>
</div>
<div class=\"footer_bch\">
    <a href=\"send_branch_list.php\" class=\"btn btn-secondary btn-block\">ไปหน้าส่งเอกสาร</a>
</div>


{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}

", "messenger/receive_hub.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\messenger\\receive_hub.tpl");
    }
}

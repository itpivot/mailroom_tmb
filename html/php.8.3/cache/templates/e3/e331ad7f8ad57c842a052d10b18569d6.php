<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/mailroom_receive_list_Thaipost_Byhand_IN.tpl */
class __TwigTemplate_cb5cf9b0b9571aba1880d79401552343 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg3' => [$this, 'block_menu_msg3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/mailroom_receive_list_Thaipost_Byhand_IN.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " receive List ";
    }

    // line 5
    public function block_menu_msg3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

";
    }

    // line 12
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "
    .card {
       margin: 8px 0px;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 70%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
    
";
    }

    // line 65
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "receive_list = {};
load_data();

    \$('#txt_search').on('keyup', function() {
        load_data();
    });

    \$('#btn_save_all').on('click', function() {
\t\tif(confirm(\"ยืนยันการรับงานทั้งหมด!\")) {
            \$.ajax({
                url: './ajax/ajax_updateReceiveAll.php',
                type: 'POST',
                data: {
                    data: JSON.stringify(receive_list)
                },
                success: function(res){
                    if(res == \"success\") {
                        load_data();
                    }
                }
            });
\t\t}
    });
\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
";
    }

    // line 96
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 97
        echo "    function updateReceiveById(id, work_status)
    {
        
        \$.ajax({
            url: './ajax/ajax_updateReceiveById.php',
            type: 'POST',
            data: {
                id: id,
                work_status: work_status
            },
            success: function(res) {
                if(res == \"success\") {
                    load_data();
                }
            }
        })

        
        
    }
\t

\tfunction updateNoReceiveById(id) {
        \$.ajax({
            url: './ajax/ajax_updateNoReceiveById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == \"success\") {
                    load_data();
                }
            }
        })
    }

\tfunction load_data() {
\treceive_list = {};
        var txt = \$('#txt_search').val();
        var date_s = \$('#start_date').val();
        var date_e = \$('#end_date').val();
        var str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_get_Mailroom_work_byhand_in.php\",
            type: \"POST\",
            data: {
                txt\t\t: txt,
                date_s\t: date_s,
                date_e\t: date_e
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                console.log(res);
                document.getElementById(\"badge-show\").textContent=res.length;
                var set_style_time = '' ;
                var str = '';
                if( res != \"\" ){
                    for(var i = 0; i < res.length; i++) {
                        receive_list[i] = res[i]['mr_work_main_id'];
    
                        if( res[i]['diff_time'] == 1 ){
                            set_style_time = 'bg-danger';
                        }else{
                            set_style_time = '';
                        }
                            
                            if( res[i]['mr_status_receive'] == 2 ){
                                    str += '<div class=\"card bg-warning\">'; 
                                    str += '<div class=\"card-body space-height\">';
                                        str += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
                                        str += '<p><b>เลข เอกสาร. : </b>'+res[i]['num_doc']+' </p>';
                                        if(res[i]['name_re']){
                                         str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
                                        }
                                         if(res[i]['mr_emp_mobile'] && res[i]['mobile_re']){
                                             str += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mobile_re']+'</a> / <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
                                          }      
                                        str += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
                                        str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
                                        str += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
                                        str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
                                        str += '<hr>';
                                        str += '<div class=\"row\">'
                                        <!-- str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );\"><b>ยกเลิก</b></a></div>' -->
                                        str += '<div class=\"col-12 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 4 );\"><b>รับเอกสาร</b></a></div>'
                                        str += '</div>';
                                        str += '</div>';
                                str += '</div>';
                            }else{
        
                                str += '<div class=\"card '+set_style_time+'\">'; 
                                    str += '<div class=\"card-body space-height\">';
                                        str += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
                                        str += '<p><b>เลข เอกสาร. : </b>'+res[i]['num_doc']+' </p>';
                                         if(res[i]['name_re']){
                                                str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
                                            }
                                        if(res[i]['mr_emp_mobile'] && res[i]['mobile_re']){
                                            str += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mobile_re']+'</a> / <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
                                        }
                                        str += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
                                        str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
                                        str += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
                                        str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
                                        str += '<hr>';
                                        str += '<div class=\"row\">'
                                        str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoReceiveById('+res[i]['mr_work_main_id']+');\"><b>ไม่พบเอกสาร</b></a></div>'
                                        <!-- str += '<div class=\"col-4 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );\"><b>ยกเลิก</b></a></div>' -->
                                        str += '<div class=\"col-6 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 4 );\"><b>รับเอกสาร</b></a></div>'
                                        str += '</div>';
                                        str += '</div>';
                                str += '</div>';
                            }
        
        
                    }
\t\t\t\t\t\$('#btn_save_all').show();
                }else{
                    \$('#btn_save_all').hide();
                    str = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                     
                }
                \$('#data_list').html(str);
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
\t
\t}
";
    }

    // line 235
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 236
        echo "    <div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
\t  <label>วันที่ mailroom รับงาน</label>
\t<div class=\"input-group pb-3\">
\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t<input  autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\" value=\"";
        // line 240
        echo twig_escape_filter($this->env, ($context["date_s"] ?? null), "html", null, true);
        echo "\"/>
\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">to</label>
\t\t\t<input autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\" value=\"";
        // line 242
        echo twig_escape_filter($this->env, ($context["date_e"] ?? null), "html", null, true);
        echo "\"/>
\t\t</div>
\t\t<div class=\"input-group-append\">
\t\t\t<button class=\"btn btn-outline-secondary\" type=\"button\" onclick=\"load_data();\">
\t\t\t\t<i class=\"material-icons\">
\t\t\t\tsearch
\t\t\t\t</i>
\t\t\t</button>
\t\t</div>
  </div>
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหา,หมายเลขงาน,ชื่อ,นามสกุล,ชั้น\">
            </div>
        </form>
    </div>
\t
\t
    <div class=\"text-center\">
        <label>จำนวนเอกสาร</label>
        
            <span id=\"badge-show\" class=\"badge badge-secondary badge-pill badge-dark\"></span>

         <label>ฉบับ</label>
    </div>
\t
\t<button type=\"button\" class=\"btn btn-primary btn-block btn-lg\" id=\"btn_save_all\">รับเอกสารทั้งหมด</button>
\t<div id=\"data_list\">
\t</div>
\t
";
    }

    // line 276
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 278
        if ((($context["debug"] ?? null) != "")) {
            // line 279
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 281
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/mailroom_receive_list_Thaipost_Byhand_IN.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  382 => 281,  376 => 279,  374 => 278,  367 => 276,  331 => 242,  326 => 240,  320 => 236,  316 => 235,  176 => 97,  172 => 96,  140 => 66,  136 => 65,  82 => 13,  78 => 12,  71 => 8,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %} receive List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/bootstrap-datepicker.min.css\">
<script src=\"../themes/bootstrap/js/bootstrap-datepicker.min.js\" charset=\"utf-8\"></script>

{% endblock %}
{% block styleReady %}

    .card {
       margin: 8px 0px;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 70%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
    
{% endblock %}

{% block domReady %}
receive_list = {};
load_data();

    \$('#txt_search').on('keyup', function() {
        load_data();
    });

    \$('#btn_save_all').on('click', function() {
\t\tif(confirm(\"ยืนยันการรับงานทั้งหมด!\")) {
            \$.ajax({
                url: './ajax/ajax_updateReceiveAll.php',
                type: 'POST',
                data: {
                    data: JSON.stringify(receive_list)
                },
                success: function(res){
                    if(res == \"success\") {
                        load_data();
                    }
                }
            });
\t\t}
    });
\t\$('.input-daterange').datepicker({
\t\t\ttodayHighlight: true,
\t\t\tautoclose: true,
\t\t\tclearBtn: true
\t\t});\t
{% endblock %}

{% block javaScript %}
    function updateReceiveById(id, work_status)
    {
        
        \$.ajax({
            url: './ajax/ajax_updateReceiveById.php',
            type: 'POST',
            data: {
                id: id,
                work_status: work_status
            },
            success: function(res) {
                if(res == \"success\") {
                    load_data();
                }
            }
        })

        
        
    }
\t

\tfunction updateNoReceiveById(id) {
        \$.ajax({
            url: './ajax/ajax_updateNoReceiveById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == \"success\") {
                    load_data();
                }
            }
        })
    }

\tfunction load_data() {
\treceive_list = {};
        var txt = \$('#txt_search').val();
        var date_s = \$('#start_date').val();
        var date_e = \$('#end_date').val();
        var str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_get_Mailroom_work_byhand_in.php\",
            type: \"POST\",
            data: {
                txt\t\t: txt,
                date_s\t: date_s,
                date_e\t: date_e
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                console.log(res);
                document.getElementById(\"badge-show\").textContent=res.length;
                var set_style_time = '' ;
                var str = '';
                if( res != \"\" ){
                    for(var i = 0; i < res.length; i++) {
                        receive_list[i] = res[i]['mr_work_main_id'];
    
                        if( res[i]['diff_time'] == 1 ){
                            set_style_time = 'bg-danger';
                        }else{
                            set_style_time = '';
                        }
                            
                            if( res[i]['mr_status_receive'] == 2 ){
                                    str += '<div class=\"card bg-warning\">'; 
                                    str += '<div class=\"card-body space-height\">';
                                        str += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
                                        str += '<p><b>เลข เอกสาร. : </b>'+res[i]['num_doc']+' </p>';
                                        if(res[i]['name_re']){
                                         str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
                                        }
                                         if(res[i]['mr_emp_mobile'] && res[i]['mobile_re']){
                                             str += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mobile_re']+'</a> / <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
                                          }      
                                        str += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
                                        str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
                                        str += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
                                        str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
                                        str += '<hr>';
                                        str += '<div class=\"row\">'
                                        <!-- str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );\"><b>ยกเลิก</b></a></div>' -->
                                        str += '<div class=\"col-12 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 4 );\"><b>รับเอกสาร</b></a></div>'
                                        str += '</div>';
                                        str += '</div>';
                                str += '</div>';
                            }else{
        
                                str += '<div class=\"card '+set_style_time+'\">'; 
                                    str += '<div class=\"card-body space-height\">';
                                        str += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
                                        str += '<p><b>เลข เอกสาร. : </b>'+res[i]['num_doc']+' </p>';
                                         if(res[i]['name_re']){
                                                str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
                                            }
                                        if(res[i]['mr_emp_mobile'] && res[i]['mobile_re']){
                                            str += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mobile_re']+'</a> / <a href=\"tel:'+res[i]['tel_re']+'\">'+res[i]['tel_re']+'</a></p>';
                                        }
                                        str += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
                                        str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
                                        str += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
                                        str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
                                        str += '<hr>';
                                        str += '<div class=\"row\">'
                                        str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoReceiveById('+res[i]['mr_work_main_id']+');\"><b>ไม่พบเอกสาร</b></a></div>'
                                        <!-- str += '<div class=\"col-4 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );\"><b>ยกเลิก</b></a></div>' -->
                                        str += '<div class=\"col-6 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 4 );\"><b>รับเอกสาร</b></a></div>'
                                        str += '</div>';
                                        str += '</div>';
                                str += '</div>';
                            }
        
        
                    }
\t\t\t\t\t\$('#btn_save_all').show();
                }else{
                    \$('#btn_save_all').hide();
                    str = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                     
                }
                \$('#data_list').html(str);
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
\t
\t}
{% endblock %}

{% block Content %}
    <div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
\t  <label>วันที่ mailroom รับงาน</label>
\t<div class=\"input-group pb-3\">
\t\t<div class=\"input-daterange input-group\" id=\"datepicker\" data-date-format=\"yyyy-mm-dd\">
\t\t\t<input  autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"start_date\" name=\"start_date\" placeholder=\"From date\" value=\"{{date_s}}\"/>
\t\t\t<label class=\"input-group-addon\" style=\"border:0px;\">to</label>
\t\t\t<input autocomplete=\"off\" type=\"text\" class=\"input-sm form-control\" id=\"end_date\" name=\"end_date\" placeholder=\"To date\" value=\"{{date_e}}\"/>
\t\t</div>
\t\t<div class=\"input-group-append\">
\t\t\t<button class=\"btn btn-outline-secondary\" type=\"button\" onclick=\"load_data();\">
\t\t\t\t<i class=\"material-icons\">
\t\t\t\tsearch
\t\t\t\t</i>
\t\t\t</button>
\t\t</div>
  </div>
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหา,หมายเลขงาน,ชื่อ,นามสกุล,ชั้น\">
            </div>
        </form>
    </div>
\t
\t
    <div class=\"text-center\">
        <label>จำนวนเอกสาร</label>
        
            <span id=\"badge-show\" class=\"badge badge-secondary badge-pill badge-dark\"></span>

         <label>ฉบับ</label>
    </div>
\t
\t<button type=\"button\" class=\"btn btn-primary btn-block btn-lg\" id=\"btn_save_all\">รับเอกสารทั้งหมด</button>
\t<div id=\"data_list\">
\t</div>
\t
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}

", "messenger/mailroom_receive_list_Thaipost_Byhand_IN.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\messenger\\mailroom_receive_list_Thaipost_Byhand_IN.tpl");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/access_log.tpl */
class __TwigTemplate_f7477a9c768d8d1efc6f9dd70a27b5c1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/access_log.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        width: 100%;
        height:100%;
      }

      .input-daterange {
          width: 450px;
      }

      #tb_access_log {
        table-layout: auto;
        font-size: 14px;
      }

      #tb_access_log > tbody > tr {
          margin: 10px 10px;
      }
      

      .loading {
          position: fixed;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          z-index: 1000;
      }


      .img_loading {
            width: 350px;
            height: auto;
            
      }

      .space {
          margin: 0px 30px;
      }

      .badge {
          padding: 5px 5px;
          font-size: 16px;
          font-weight: lighter;
      }
      

      .select2-container .select2-selection--single {
          min-height: 38px;
      }

      .select2-container--default .select2-selection--single {
          border: 1px solid rgba(0,0,0,.15);
      }

      .select2-container--default .select2-selection--single .select2-selection__rendered {
          line-height: 35px;
      }

      .select2-container--default .select2-selection--single .select2-selection__arrow {
          min-height: 35px;
          right: 3px;
      }

      .input-daterange .input-group-addon {
          margin: 0px 3px;
      }


      

";
    }

    // line 79
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 80
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/datepicker/bootstrap-datepicker3.min.css\">

<script type='text/javascript' src=\"../themes/datepicker/bootstrap-datepicker.min.js\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
";
    }

    // line 89
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 90
        echo "\t\$('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
    });
    
    var dataTable = \$('#tb_access_log').DataTable({
  \"scrollX\": true
})

        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        var status = \$('#lst_status').val();
        var activity = \$(\"#lst_activity\").val();
        getAccessLog(start_date, end_date, activity, status)


    \$('#btn_search').on('click', function() {
        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        var status = \$('#lst_status').val();
        var activity = \$(\"#lst_activity\").val();
        getAccessLog(start_date, end_date, activity,status);
    }); 

    \$('#btn_clear').on('click', function() {
        \$('#date_start').val(\"\");
        \$('#date_end').val(\"\");
        \$('#lst_status').val(0).trigger('change');
        \$(\"#lst_activity\").val(0).trigger('change');
        dataTable.clear().draw();
    });

    \$('#btn_excel').on('click', function() {
        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        var status = \$('#lst_status').val();
        var activity = \$(\"#lst_activity\").val();
        exportLog(start_date, end_date, activity,status);

    });

    
    \$('#lst_activity').select2({
        width: '100%',
    })
    ;
    \$('#lst_status').select2({
        width: '100%'
    });

    
   
   
    
";
    }

    // line 149
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 150
        echo "    var getAccessLog = function(start, end, activity, status) {
        \$.ajax({
            url: './ajax/ajax_getAccessLog.php',
            type: 'POST',
            data: {
                start: start,
                end: end,
                activity: activity,
                status: status
            },
            dataType: 'json',
            success: function(res) {

                if(res.status == 200){
                    \$('#tb_access_log').DataTable().clear();
                    \$('#tb_access_log').DataTable().rows.add(res.data).draw();
                }else{
                    \$('#tb_access_log').DataTable().clear().draw();
                }
            }
        })
    }

   \tvar exportLog = function(start, end, activity, status) {
        var data = {
            \"start\": start,
            \"end\": end,
            \"activity\": activity,
            \"status\": status
        };
        // var send = JSON.stringify(data);
        // window.open(\"export_access_log.php?data=\"+JSON.stringify(data));     
        location.href='export_access_log.php?data='+JSON.stringify(data);  
    }
";
    }

    // line 186
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 187
        echo "        <h1>รายการเข้าใช้งานระบบ</h1>
        <hr> 
      <div class=\"card\">
            
            <div class=\"card-body\">
                <div class=\"row\">
                    <div class=\"col-5\">
                        <form class=\"form-inline\">
                            <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">
                                <div class=\"input-group input-daterange\">
                                    <input type=\"text\" class=\"form-control\" id='date_start' placeholder='วันที่เริ่มต้น'>
                                    <div class=\"input-group-addon\" style='background-color: #fff; border-color: #fff; padding: 0px 10px;'><b>ถึง</b></div>
                                    <input type=\"text\" class=\"form-control\" id='date_end' placeholder='วันที่สิ้นสุด'>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class=\"col\">
                        <form class=\"form-inline\">
                           <select class=\"form-control\" id=\"lst_activity\">
                                <option value=\"0\">กิจกรรม</option>
                                <option value=\"Login\">Login</option>
                                <option value=\"Logout\">Logout</option>
                            </select>
                        </form>
                    </div>
                    <div class=\"col\">
                            <form class=\"form-inline\">
                                <select class=\"form-control\" id=\"lst_status\" style=\"padding: 10px 10px;\">
                                    <option value=\"0\">สถานะ</option>
                                    <option value=\"Success\">Success</option>
                                    <option value=\"Failed\">Fail</option>
                                </select>
                            </form>
                    </div>
                     <div class=\"col-2\">
                         <center>
                            <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"utils\">
                                <button type='button' class='btn' id='btn_search'><i class=\"material-icons\" title=\"ค้นหา\">search</i></button>
                                <button type='button' class='btn' title=\"ล้าง\" id='btn_clear'><i class=\"material-icons\">clear</i></button>
                                <button type='button' class='btn' id='btn_excel' title=\"ออกรายงาน\"><i class=\"material-icons\">description</i></button>
                            </div>
                         </center>
                    </div>
                </div>
              
            </div>
      </div>

      <div class='card' style='margin-top: 20px;'>
        <div class='card-header'>
            <b>รายการเข้าใช้งาน</b>
        </div>        
        <div class='card-body'>
            <table id='tb_access_log' class=\"table table-responsive table-sm\" cellspacing=\"0\">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>รหัส</th>
                        <th>ชื่อ-สกุล</th>
                        <th>Terminal</th>
                        <th>กิจกรรม</th>
                        <th>ผลการเข้าระบบ</th>
                        <th>วันเวลา</th>
                        <th>หมายเหตุ</th>
                    </tr>
                </thead>
                <tbody id='show_data'></tbody>
            </table>
        </div>
      </div>
\t
";
    }

    // line 262
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 264
        if ((($context["debug"] ?? null) != "")) {
            // line 265
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 267
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/access_log.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  362 => 267,  356 => 265,  354 => 264,  347 => 262,  271 => 187,  267 => 186,  229 => 150,  225 => 149,  165 => 90,  161 => 89,  150 => 80,  146 => 79,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        width: 100%;
        height:100%;
      }

      .input-daterange {
          width: 450px;
      }

      #tb_access_log {
        table-layout: auto;
        font-size: 14px;
      }

      #tb_access_log > tbody > tr {
          margin: 10px 10px;
      }
      

      .loading {
          position: fixed;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          z-index: 1000;
      }


      .img_loading {
            width: 350px;
            height: auto;
            
      }

      .space {
          margin: 0px 30px;
      }

      .badge {
          padding: 5px 5px;
          font-size: 16px;
          font-weight: lighter;
      }
      

      .select2-container .select2-selection--single {
          min-height: 38px;
      }

      .select2-container--default .select2-selection--single {
          border: 1px solid rgba(0,0,0,.15);
      }

      .select2-container--default .select2-selection--single .select2-selection__rendered {
          line-height: 35px;
      }

      .select2-container--default .select2-selection--single .select2-selection__arrow {
          min-height: 35px;
          right: 3px;
      }

      .input-daterange .input-group-addon {
          margin: 0px 3px;
      }


      

{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"../themes/datepicker/bootstrap-datepicker3.min.css\">

<script type='text/javascript' src=\"../themes/datepicker/bootstrap-datepicker.min.js\"></script>
<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>
{% endblock %}

{% block domReady %}
\t\$('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
    });
    
    var dataTable = \$('#tb_access_log').DataTable({
  \"scrollX\": true
})

        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        var status = \$('#lst_status').val();
        var activity = \$(\"#lst_activity\").val();
        getAccessLog(start_date, end_date, activity, status)


    \$('#btn_search').on('click', function() {
        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        var status = \$('#lst_status').val();
        var activity = \$(\"#lst_activity\").val();
        getAccessLog(start_date, end_date, activity,status);
    }); 

    \$('#btn_clear').on('click', function() {
        \$('#date_start').val(\"\");
        \$('#date_end').val(\"\");
        \$('#lst_status').val(0).trigger('change');
        \$(\"#lst_activity\").val(0).trigger('change');
        dataTable.clear().draw();
    });

    \$('#btn_excel').on('click', function() {
        var start_date = \$('#date_start').val();
        var end_date = \$('#date_end').val();
        var status = \$('#lst_status').val();
        var activity = \$(\"#lst_activity\").val();
        exportLog(start_date, end_date, activity,status);

    });

    
    \$('#lst_activity').select2({
        width: '100%',
    })
    ;
    \$('#lst_status').select2({
        width: '100%'
    });

    
   
   
    
{% endblock %}


{% block javaScript %}
    var getAccessLog = function(start, end, activity, status) {
        \$.ajax({
            url: './ajax/ajax_getAccessLog.php',
            type: 'POST',
            data: {
                start: start,
                end: end,
                activity: activity,
                status: status
            },
            dataType: 'json',
            success: function(res) {

                if(res.status == 200){
                    \$('#tb_access_log').DataTable().clear();
                    \$('#tb_access_log').DataTable().rows.add(res.data).draw();
                }else{
                    \$('#tb_access_log').DataTable().clear().draw();
                }
            }
        })
    }

   \tvar exportLog = function(start, end, activity, status) {
        var data = {
            \"start\": start,
            \"end\": end,
            \"activity\": activity,
            \"status\": status
        };
        // var send = JSON.stringify(data);
        // window.open(\"export_access_log.php?data=\"+JSON.stringify(data));     
        location.href='export_access_log.php?data='+JSON.stringify(data);  
    }
{% endblock %}

{% block Content2 %}
        <h1>รายการเข้าใช้งานระบบ</h1>
        <hr> 
      <div class=\"card\">
            
            <div class=\"card-body\">
                <div class=\"row\">
                    <div class=\"col-5\">
                        <form class=\"form-inline\">
                            <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">
                                <div class=\"input-group input-daterange\">
                                    <input type=\"text\" class=\"form-control\" id='date_start' placeholder='วันที่เริ่มต้น'>
                                    <div class=\"input-group-addon\" style='background-color: #fff; border-color: #fff; padding: 0px 10px;'><b>ถึง</b></div>
                                    <input type=\"text\" class=\"form-control\" id='date_end' placeholder='วันที่สิ้นสุด'>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class=\"col\">
                        <form class=\"form-inline\">
                           <select class=\"form-control\" id=\"lst_activity\">
                                <option value=\"0\">กิจกรรม</option>
                                <option value=\"Login\">Login</option>
                                <option value=\"Logout\">Logout</option>
                            </select>
                        </form>
                    </div>
                    <div class=\"col\">
                            <form class=\"form-inline\">
                                <select class=\"form-control\" id=\"lst_status\" style=\"padding: 10px 10px;\">
                                    <option value=\"0\">สถานะ</option>
                                    <option value=\"Success\">Success</option>
                                    <option value=\"Failed\">Fail</option>
                                </select>
                            </form>
                    </div>
                     <div class=\"col-2\">
                         <center>
                            <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"utils\">
                                <button type='button' class='btn' id='btn_search'><i class=\"material-icons\" title=\"ค้นหา\">search</i></button>
                                <button type='button' class='btn' title=\"ล้าง\" id='btn_clear'><i class=\"material-icons\">clear</i></button>
                                <button type='button' class='btn' id='btn_excel' title=\"ออกรายงาน\"><i class=\"material-icons\">description</i></button>
                            </div>
                         </center>
                    </div>
                </div>
              
            </div>
      </div>

      <div class='card' style='margin-top: 20px;'>
        <div class='card-header'>
            <b>รายการเข้าใช้งาน</b>
        </div>        
        <div class='card-body'>
            <table id='tb_access_log' class=\"table table-responsive table-sm\" cellspacing=\"0\">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>รหัส</th>
                        <th>ชื่อ-สกุล</th>
                        <th>Terminal</th>
                        <th>กิจกรรม</th>
                        <th>ผลการเข้าระบบ</th>
                        <th>วันเวลา</th>
                        <th>หมายเหตุ</th>
                    </tr>
                </thead>
                <tbody id='show_data'></tbody>
            </table>
        </div>
      </div>
\t
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/access_log.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\access_log.tpl");
    }
}

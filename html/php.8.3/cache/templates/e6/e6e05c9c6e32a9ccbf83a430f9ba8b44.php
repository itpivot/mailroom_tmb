<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* data/profile.tpl */
class __TwigTemplate_56c617f263664fe52f5fe170993ab4ad extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'styleReady' => [$this, 'block_styleReady'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "data/profile.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "\t\t

\t\t
\t\t
\t\t
\t\t\$(\"#mr_role_id\").change(function(){
\t\t    var role_id = \$(this).val();
\t\t\t//alert(role_id);
\t\t\talertify.confirm('แก้ไขสถานที่ปฏิบัติงาน','ยืนยันการเปลี่ยนสถานที่ปฏิบัติงาน', function(){ 
\t\t\t\t//alertify.alert(\"ok\");
\t\t\t\t\$.post('./ajax/ajax_UpdateUserRole.php',{role_id:role_id},
\t\t\t\t\tfunction(res) {\t\t\t\t\t\t\t//console.log(res)
\t\t\t\t\t\t\tif ( res == '' ){
\t\t\t\t\t\t\t\talertify.alert('',\"บันทึกสำเร็จ\",function(){window.location.reload();});
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\talertify.alert('error',\"บันทึกไม่สำเร็จ  \"+res,function(){window.location.reload();});
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}});
\t\t\t\t\t\t},function(res) {\t\t\t\t\t\t\t//console.log(res)
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t});
\t\t});
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\tvar name \t\t\t\t= \$(\"#name\").val();
\t\t\tvar last_name \t\t\t= \$(\"#last_name\").val();
\t\t\tvar tel \t\t\t\t= \$(\"#tel\").val();
\t\t\tvar tel_mobile \t\t\t= \$(\"#tel_mobile\").val();
\t\t\tvar email \t\t\t\t= \$(\"#email\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar department \t\t\t= \$(\"#department\").val();
\t\t\tvar floor \t\t\t\t= \$(\"#floor\").val();
\t\t\tvar\tstatus \t\t\t\t= true;
\t\t\t\$('.myErrorClass').removeClass( \"myErrorClass\" );
\t\t\t\tif( pass_emp == \"\" || pass_emp == null){
\t\t\t\t\t\$('#pass_emp').addClass( \"myErrorClass\" );
\t\t\t\t\tstatus = false;
\t\t\t\t}else{
\t\t\t\t}
\t\t\t\t
\t\t\t\tif(\tname == \"\" || name == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#name').addClass( \"myErrorClass\" );
\t\t\t\t}else{

\t\t\t\t}
\t\t\t\t
\t\t\t\tif( last_name == \"\" || last_name == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#last_name ').addClass( \"myErrorClass\" );
\t\t\t\t}else{
\t\t\t\t}
\t\t\t\t
\t\t\t\tif( email == \"\" || email == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#email ').addClass( \"myErrorClass\" );
\t\t\t\t}else{

\t\t\t\t}
\t\t\t\t
\t\t\t\tif( floor == \"\" || floor == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#select2-floor-container').addClass( \"myErrorClass\" );
\t\t\t\t}else{
\t\t\t\t}

\t\t\t\tif( department == 457 || department == \"\" || department == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#select2-department-container').addClass( \"myErrorClass\" );
\t\t\t\t}else{
\t\t\t\t}
\t\t\t\t
\t\t\t\t
\t\t\t\t//console.log(floor);
\t\t\t\t
\t\t\t\tif( status === true ){
\t\t\t\t\talertify.confirm('แก้ไขข้อมูล','ยืนยันการแก้ไขข้อมูล', function(){ 
\t\t\t\t\t\t


\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tmethod: \"POST\",
\t\t\t\t\t\t\t\turl: \"./ajax/ajax_update_emp.php\",
\t\t\t\t\t\t\t\tdata: { 
\t\t\t\t\t\t\t\t\tpass_emp\t : pass_emp,
\t\t\t\t\t\t\t\t\tuser_id\t\t : user_id,
\t\t\t\t\t\t\t\t\temp_id\t\t : emp_id,
\t\t\t\t\t\t\t\t\tname\t\t : name,\t\t
\t\t\t\t\t\t\t\t\tlast_name \t : last_name,
\t\t\t\t\t\t\t\t\ttel \t\t : tel,\t\t
\t\t\t\t\t\t\t\t\ttel_mobile \t : tel_mobile,
\t\t\t\t\t\t\t\t\temail \t\t : email,
\t\t\t\t\t\t\t\t\tdepartment \t : department,
\t\t\t\t\t\t\t\t\tcsrf_token\t : \$('#csrf_token').val(),
\t\t\t\t\t\t\t\t\tfloor \t\t : floor
\t\t\t\t\t\t\t\t }
\t\t\t\t\t\t\t  }).done(function(data1){
\t\t\t\t\t\t\t\t  if(data1['status'] == 200){
\t\t\t\t\t\t\t\t\t\talertify.alert('บันทึกสำเร็จ',data1['message'],
\t\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t);
\t\t\t\t\t\t\t\t  }else{
\t\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',data1['message'],
\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t);
\t\t\t\t\t\t\t\t  }
\t\t\t\t\t\t\t  });
\t\t\t\t\t},function(){});
\t\t\t\t}else{
\t\t\t\t\talert('กรุณากรอกข้อมูลให้ครบถ้วน');
\t\t\t\t}\t\t\t\t\t
\t\t\t\t
\t\t\t
\t\t});
\t\t
\t\t



\t\t
\$('#name_emp').select2({
\t\t\t\tdropdownParent: \$('#editCon'),
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"../branch/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\t\$.ajax({
\t\t\t  dataType: \"json\",
\t\t\t  method: \"POST\",
\t\t\t  url: \"../branch/ajax/ajax_getdataemployee.php\",
\t\t\t  data: { employee_id : \$('#name_emp').val() }
\t\t\t}).done(function(data1){
\t\t\t\t//alert('55555');
\t\t\t\t\$('#con_emp_tel').val(data1['mr_emp_tel']);
\t\t\t\t\$('#mr_emp_code').val(data1['mr_emp_code']);
\t\t\t\t\$('#con_emp_name').val(data1['mr_emp_name']+'  '+data1['mr_emp_lastname']);
\t\t\t\t//console.log(data1);
\t\t\t\t//console.log(\$('#name_emp').val());
\t\t\t});
\t\t\t
\t\t});

\t\t
\t\t
\t\t\t
\$( \"#btnCon_save\" ).click(function() {
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\tvar con_remark \t\t\t= \$('#con_remark').val();
\t\t\t\t\tvar con_floor \t\t\t= \$('#con_floor').val();
\t\t\t\t\tvar con_department_name \t\t\t= \$('#con_department_name').val();
\t\t\t\t\tvar mr_emp_tel \t\t\t= \$('#con_emp_tel').val();
\t\t\t\t\tvar con_emp_name \t\t= \$('#con_emp_name').val();
\t\t\t\t\tvar mr_emp_code \t\t= \$('#mr_emp_code').val();
\t\t\t\t\tvar mr_emp_id \t\t\t= \$('#name_emp').val();
\t\t\t\t\tvar department_code \t\t= \$('#department_code').val();
\t\t\t\t\tvar mr_contact_id \t\t= \$('#mr_contact_id').val();
\t\t\t\t\tvar type_add \t\t= \$('#type_add').val();
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t\$( \".myErrorClass\" ).removeClass( \"myErrorClass\" );
\t\t\t\t\t
\t\t\t\t\tif(mr_emp_id == ''){
\t\t\t\t\t\t\$( \"#name_emp\" ).addClass( \"myErrorClass\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_code == ''){
\t\t\t\t\t\t\$( \"#mr_emp_code\" ).addClass( \"myErrorClass\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_tel == ''){
\t\t\t\t\t\t\$( \"#mr_emp_tel\" ).addClass( \"myErrorClass\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t\t
\talertify.confirm(\"ตรวจสอบข้อมูล\", \"กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก\",
\t\t\tfunction(){
\t\t\t\t\$.ajax({
\t\t\t\t\t\t  dataType: \"json\",
\t\t\t\t\t\t  method: \"POST\",
\t\t\t\t\t\t  url: \"ajax/ajax_save_data_contact.php\",
\t\t\t\t\t\t  data: { 
\t\t\t\t\t\t\t\tdepartment_code \t\t\t \t : department_code \t\t\t \t,
\t\t\t\t\t\t\t\tcon_emp_name \t\t\t \t : con_emp_name \t\t\t \t,
\t\t\t\t\t\t\t\tcon_remark \t\t\t \t : con_remark \t\t\t \t,
\t\t\t\t\t\t\t\tcon_floor \t\t\t \t : con_floor \t\t\t \t,
\t\t\t\t\t\t\t\tcon_department_name \t : con_department_name \t,
\t\t\t\t\t\t\t\ttype_add \t : type_add \t,
\t\t\t\t\t\t\t\tmr_emp_tel \t : mr_emp_tel \t,
\t\t\t\t\t\t\t\tmr_emp_code  : mr_emp_code ,
\t\t\t\t\t\t\t\tmr_emp_id \t : mr_emp_id \t,
\t\t\t\t\t\t\t\tmr_contact_id: mr_contact_id
\t\t\t\t\t\t  }
\t\t\t\t\t\t}).done(function(data) {
\t\t\t\t\t\t\t\$('#csrf_token').val(data.token);

\t\t\t\t\t\t\talertify.alert(data['st'],data['msg'],
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t  },function(){
\t\t\t\t\talertify.error('Cancel');
\t  });
\t
\t
\t//alert( \"Handler for .click() called.\" );
\t//is-invalid
});



\t\t
";
    }

    // line 238
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 239
        echo "\t\t
function load_modal(mr_contact_id,type) {\t

 \$('#editCon').modal({backdrop: 'false'});

\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_data_contact.php\",
\t  data: { \t
\t\t\t\tmr_contact_id : mr_contact_id,
\t\t\t\tpage : 'getdata'
\t  }
\t}).done(function(data) {
\t\t//alert('55555');
\t\tif(type!= \"add\"){
\t\t\t\$('#mr_contact_id').val(data['mr_contact_id']);
\t\t\t
\t\t}
\t\t
\t\t\$('#type_add').val(type);
\t\t\$('#mr_emp_code').val(data['emp_code']);
\t\t\$('#con_department_name').val(data['department_name']);
\t\t\$('#con_floor').val(data['floor']);
\t\t\$('#con_remark').val(data['remark']);
\t\t\$('#department_code').val(data['department_code']);
\t});
\t
}


\t

\t\t
";
    }

    // line 275
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 276
        echo "
\t.btn {
\t\tbox-shadow: 0px 0px 0px 0px rgba(0,0,0,.0) !important;
\t}
\t.modal-backdrop {
\t\tposition: relative !important;
\t}
\t.modal {
\t\ttop: 40% !important;
\t}
\t.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}

";
    }

    // line 307
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 308
        echo "

\t
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้สั่งงาน </h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"";
        // line 314
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_user_id", [], "any", false, false, false, 314), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"";
        // line 315
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_id", [], "any", false, false, false, 315), "html", null, true);
        echo "\">
\t\t\t <input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"";
        // line 316
        echo twig_escape_filter($this->env, ($context["csrf"] ?? null), "html", null, true);
        echo "\">

\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"";
        // line 325
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_code", [], "any", false, false, false, 325), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"";
        // line 336
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_name", [], "any", false, false, false, 336), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"";
        // line 347
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_lastname", [], "any", false, false, false, 347), "html", null, true);
        echo "\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"";
        // line 357
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_tel", [], "any", false, false, false, 357), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"";
        // line 367
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_mobile", [], "any", false, false, false, 367), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"";
        // line 377
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_email", [], "any", false, false, false, 377), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tแผนก :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\">
\t\t\t\t\t\t\t";
        // line 389
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["department_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 390
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 390), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_id", [], "any", false, false, false, 390) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_id", [], "any", false, false, false, 390))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_code", [], "any", false, false, false, 390), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "mr_department_name", [], "any", false, false, false, 390), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 391
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"\"> ไม่มีข้อมูลชั้น</option>
\t\t\t\t\t\t\t";
        // line 405
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["floor_data"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
            // line 406
            echo "\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 406), "html", null, true);
            echo "\" ";
            if ((twig_get_attribute($this->env, $this->source, $context["f"], "mr_floor_id", [], "any", false, false, false, 406) == twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "floor_emp", [], "any", false, false, false, 406))) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["f"], "name", [], "any", false, false, false, 406), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 407
        echo "\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t";
        // line 412
        if (((($context["role_id"] ?? null) == 2) || (($context["role_id"] ?? null) == 5))) {
            // line 413
            echo "\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px;color:red;\">
\t\t\t\t***สถานที่ปฏิบัติงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_role_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"2\" ";
            // line 420
            if ((($context["role_id"] ?? null) == 2)) {
                echo "selected ";
            }
            echo "> สำนักงาน(พหลโยธิน)</option>\t\t\t\t
\t\t\t\t\t\t\t<option value=\"5\" ";
            // line 421
            if ((($context["role_id"] ?? null) == 5)) {
                echo "selected ";
            }
            echo "> สาขา/สาขาและอาคารอื่นๆ</option>\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t";
        }
        // line 427
        echo "\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึกการแก้ไข</button>
\t\t\t\t<br>
\t\t\t\t<center>
\t\t\t\t\t<a href = \"../user/change_password.php?usr=";
        // line 435
        echo twig_escape_filter($this->env, ($context["userID"] ?? null), "html", null, true);
        echo "\"><b> เปลี่ยนรหัสผ่าน คลิกที่นี่</b></a>
\t\t\t\t</center>
\t\t\t</div>
\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t
\t\t\t
\t\t\t";
        // line 443
        if ((twig_length_filter($this->env, ($context["con_data"] ?? null)) > 0)) {
            // line 444
            echo "\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<h4>ข้อมูลหน่วยงานที่อยู่ในความรับผิดชอบรับเอกสาร</h4><br>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<table class=\"table table-striped\" align=\"center\" border=\"1\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">#</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">พนักงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">รหัสหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">ชื่อหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">จัดการ <button type=\"button\" class=\"btn btn-link\"
\t\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('";
            // line 454
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["con_data"] ?? null), 0, [], "any", false, false, false, 454), "mr_contact_id", [], "any", false, false, false, 454), "html", null, true);
            echo "','add');\">
\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"material-icons\">add_circle</i>
\t\t\t\t\t\t\t\t\t\t\t</button></th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 459
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["con_data"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 460
                echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 461
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 461), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 462
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "emp_code", [], "any", false, false, false, 462), "html", null, true);
                echo "  ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_contact_name", [], "any", false, false, false, 462), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
                // line 463
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "department_code", [], "any", false, false, false, 463), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t   ";
                // line 465
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "department_name", [], "any", false, false, false, 465), "html", null, true);
                echo " [";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "remark", [], "any", false, false, false, 465), "html", null, true);
                echo "]   
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<button ";
                // line 468
                if ((twig_get_attribute($this->env, $this->source, $context["c"], "emp_code", [], "any", false, false, false, 468) != twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_emp_code", [], "any", false, false, false, 468))) {
                    echo " disabled ";
                }
                // line 469
                echo "\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "mr_contact_id", [], "any", false, false, false, 469), "html", null, true);
                echo "',null);\" class=\"btn btn-link\" style=\"text-decoration: underline;\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">create</i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 477
            echo "\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t";
        }
        // line 479
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t<!-- <div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_depart\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" value=\"";
        // line 491
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_code", [], "any", false, false, false, 491), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor\" placeholder=\"ชั้น\" value=\"";
        // line 502
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_floor", [], "any", false, false, false, 502), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart\" placeholder=\"ชื่อหน่วยงาน\" value=\"";
        // line 513
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user_data"] ?? null), "mr_department_name", [], "any", false, false, false, 513), "html", null, true);
        echo "\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t\t -->

\t
<!-- Modal -->
<div class=\"modal fade\" id=\"editCon\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อ - นามสกุล</label><br>
\t\t\t\t
\t\t\t\t<select class=\"form-control\" id=\"name_emp\" style=\"width:100%;\">
\t\t\t\t  <option value=\"\">";
        // line 535
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["con_data"] ?? null), 0, [], "any", false, false, false, 535), "emp_code", [], "any", false, false, false, 535), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["con_data"] ?? null), 0, [], "any", false, false, false, 535), "mr_contact_name", [], "any", false, false, false, 535), "html", null, true);
        echo "</option>
\t\t\t\t  ";
        // line 536
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["all_emp"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["emp"]) {
            // line 537
            echo "\t\t\t\t  <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_id", [], "any", false, false, false, 537), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_code", [], "any", false, false, false, 537), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_name", [], "any", false, false, false, 537), "html", null, true);
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emp"], "mr_emp_lastname", [], "any", false, false, false, 537), "html", null, true);
            echo "</option>
\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['emp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 539
        echo "\t\t\t\t</select>
\t\t\t\t
\t\t\t  </div>
\t\t\t 
\t\t\t  
\t\t\t <div class=\"form-group\">
\t\t\t\t<label for=\"\">เบอร์โทร</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_emp_tel\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"type_add\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_contact_id\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_emp_code\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"con_emp_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">รหัสหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"department_code\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_department_name\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชั้น</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_floor\" value=\"\"  readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">หมายเหตุ</label>
\t\t\t\t <textarea class=\"form-control\" id=\"con_remark\" rows=\"3\"></textarea >
\t\t\t  </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"button\" id=\"btnCon_save\" class=\"btn btn-primary\">Save changes</button>
      </div>
    </div>
  </div>
</div>
\t
\t\t\t\t
\t\t\t
\t\t\t
\t\t
\t
 
\t
\t
\t
\t
";
    }

    // line 590
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 592
        if ((($context["debug"] ?? null) != "")) {
            // line 593
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 595
        echo "
";
    }

    public function getTemplateName()
    {
        return "data/profile.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  862 => 595,  856 => 593,  854 => 592,  847 => 590,  795 => 539,  781 => 537,  777 => 536,  771 => 535,  746 => 513,  732 => 502,  718 => 491,  704 => 479,  700 => 477,  677 => 469,  673 => 468,  665 => 465,  660 => 463,  654 => 462,  650 => 461,  647 => 460,  630 => 459,  622 => 454,  610 => 444,  608 => 443,  597 => 435,  587 => 427,  576 => 421,  570 => 420,  561 => 413,  559 => 412,  552 => 407,  537 => 406,  533 => 405,  517 => 391,  500 => 390,  496 => 389,  481 => 377,  468 => 367,  455 => 357,  442 => 347,  428 => 336,  414 => 325,  402 => 316,  398 => 315,  394 => 314,  386 => 308,  382 => 307,  349 => 276,  345 => 275,  307 => 239,  303 => 238,  70 => 8,  66 => 7,  59 => 5,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
\t\t

\t\t
\t\t
\t\t
\t\t\$(\"#mr_role_id\").change(function(){
\t\t    var role_id = \$(this).val();
\t\t\t//alert(role_id);
\t\t\talertify.confirm('แก้ไขสถานที่ปฏิบัติงาน','ยืนยันการเปลี่ยนสถานที่ปฏิบัติงาน', function(){ 
\t\t\t\t//alertify.alert(\"ok\");
\t\t\t\t\$.post('./ajax/ajax_UpdateUserRole.php',{role_id:role_id},
\t\t\t\t\tfunction(res) {\t\t\t\t\t\t\t//console.log(res)
\t\t\t\t\t\t\tif ( res == '' ){
\t\t\t\t\t\t\t\talertify.alert('',\"บันทึกสำเร็จ\",function(){window.location.reload();});
\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\talertify.alert('error',\"บันทึกไม่สำเร็จ  \"+res,function(){window.location.reload();});
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}});
\t\t\t\t\t\t},function(res) {\t\t\t\t\t\t\t//console.log(res)
\t\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t});
\t\t});
\t\t\$(\"#btn_save\").click(function(){
\t\t\tvar pass_emp \t\t\t= \$(\"#pass_emp\").val();
\t\t\tvar name \t\t\t\t= \$(\"#name\").val();
\t\t\tvar last_name \t\t\t= \$(\"#last_name\").val();
\t\t\tvar tel \t\t\t\t= \$(\"#tel\").val();
\t\t\tvar tel_mobile \t\t\t= \$(\"#tel_mobile\").val();
\t\t\tvar email \t\t\t\t= \$(\"#email\").val();
\t\t\tvar emp_id \t\t\t\t= \$(\"#emp_id\").val();
\t\t\tvar user_id \t\t\t= \$(\"#user_id\").val();
\t\t\tvar department \t\t\t= \$(\"#department\").val();
\t\t\tvar floor \t\t\t\t= \$(\"#floor\").val();
\t\t\tvar\tstatus \t\t\t\t= true;
\t\t\t\$('.myErrorClass').removeClass( \"myErrorClass\" );
\t\t\t\tif( pass_emp == \"\" || pass_emp == null){
\t\t\t\t\t\$('#pass_emp').addClass( \"myErrorClass\" );
\t\t\t\t\tstatus = false;
\t\t\t\t}else{
\t\t\t\t}
\t\t\t\t
\t\t\t\tif(\tname == \"\" || name == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#name').addClass( \"myErrorClass\" );
\t\t\t\t}else{

\t\t\t\t}
\t\t\t\t
\t\t\t\tif( last_name == \"\" || last_name == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#last_name ').addClass( \"myErrorClass\" );
\t\t\t\t}else{
\t\t\t\t}
\t\t\t\t
\t\t\t\tif( email == \"\" || email == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#email ').addClass( \"myErrorClass\" );
\t\t\t\t}else{

\t\t\t\t}
\t\t\t\t
\t\t\t\tif( floor == \"\" || floor == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#select2-floor-container').addClass( \"myErrorClass\" );
\t\t\t\t}else{
\t\t\t\t}

\t\t\t\tif( department == 457 || department == \"\" || department == null){
\t\t\t\t\tstatus = false;
\t\t\t\t\t\$('#select2-department-container').addClass( \"myErrorClass\" );
\t\t\t\t}else{
\t\t\t\t}
\t\t\t\t
\t\t\t\t
\t\t\t\t//console.log(floor);
\t\t\t\t
\t\t\t\tif( status === true ){
\t\t\t\t\talertify.confirm('แก้ไขข้อมูล','ยืนยันการแก้ไขข้อมูล', function(){ 
\t\t\t\t\t\t


\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tmethod: \"POST\",
\t\t\t\t\t\t\t\turl: \"./ajax/ajax_update_emp.php\",
\t\t\t\t\t\t\t\tdata: { 
\t\t\t\t\t\t\t\t\tpass_emp\t : pass_emp,
\t\t\t\t\t\t\t\t\tuser_id\t\t : user_id,
\t\t\t\t\t\t\t\t\temp_id\t\t : emp_id,
\t\t\t\t\t\t\t\t\tname\t\t : name,\t\t
\t\t\t\t\t\t\t\t\tlast_name \t : last_name,
\t\t\t\t\t\t\t\t\ttel \t\t : tel,\t\t
\t\t\t\t\t\t\t\t\ttel_mobile \t : tel_mobile,
\t\t\t\t\t\t\t\t\temail \t\t : email,
\t\t\t\t\t\t\t\t\tdepartment \t : department,
\t\t\t\t\t\t\t\t\tcsrf_token\t : \$('#csrf_token').val(),
\t\t\t\t\t\t\t\t\tfloor \t\t : floor
\t\t\t\t\t\t\t\t }
\t\t\t\t\t\t\t  }).done(function(data1){
\t\t\t\t\t\t\t\t  if(data1['status'] == 200){
\t\t\t\t\t\t\t\t\t\talertify.alert('บันทึกสำเร็จ',data1['message'],
\t\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t);
\t\t\t\t\t\t\t\t  }else{
\t\t\t\t\t\t\t\t\talertify.alert('เกิดข้อผิดพลาด',data1['message'],
\t\t\t\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t);
\t\t\t\t\t\t\t\t  }
\t\t\t\t\t\t\t  });
\t\t\t\t\t},function(){});
\t\t\t\t}else{
\t\t\t\t\talert('กรุณากรอกข้อมูลให้ครบถ้วน');
\t\t\t\t}\t\t\t\t\t
\t\t\t\t
\t\t\t
\t\t});
\t\t
\t\t



\t\t
\$('#name_emp').select2({
\t\t\t\tdropdownParent: \$('#editCon'),
\t\t\t\tplaceholder: \"ค้นหาผู้รับ\",
\t\t\t\tajax: {
\t\t\t\t\turl: \"../branch/ajax/ajax_getdataemployee_select.php\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdelay: 250,
\t\t\t\t\tprocessResults: function (data) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t results : data
\t\t\t\t\t\t};
\t\t\t\t\t},
\t\t\t\t\tcache: true
\t\t\t\t}
\t\t}).on('select2:select', function(e) {
\t\t\t\$.ajax({
\t\t\t  dataType: \"json\",
\t\t\t  method: \"POST\",
\t\t\t  url: \"../branch/ajax/ajax_getdataemployee.php\",
\t\t\t  data: { employee_id : \$('#name_emp').val() }
\t\t\t}).done(function(data1){
\t\t\t\t//alert('55555');
\t\t\t\t\$('#con_emp_tel').val(data1['mr_emp_tel']);
\t\t\t\t\$('#mr_emp_code').val(data1['mr_emp_code']);
\t\t\t\t\$('#con_emp_name').val(data1['mr_emp_name']+'  '+data1['mr_emp_lastname']);
\t\t\t\t//console.log(data1);
\t\t\t\t//console.log(\$('#name_emp').val());
\t\t\t});
\t\t\t
\t\t});

\t\t
\t\t
\t\t\t
\$( \"#btnCon_save\" ).click(function() {
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\tvar con_remark \t\t\t= \$('#con_remark').val();
\t\t\t\t\tvar con_floor \t\t\t= \$('#con_floor').val();
\t\t\t\t\tvar con_department_name \t\t\t= \$('#con_department_name').val();
\t\t\t\t\tvar mr_emp_tel \t\t\t= \$('#con_emp_tel').val();
\t\t\t\t\tvar con_emp_name \t\t= \$('#con_emp_name').val();
\t\t\t\t\tvar mr_emp_code \t\t= \$('#mr_emp_code').val();
\t\t\t\t\tvar mr_emp_id \t\t\t= \$('#name_emp').val();
\t\t\t\t\tvar department_code \t\t= \$('#department_code').val();
\t\t\t\t\tvar mr_contact_id \t\t= \$('#mr_contact_id').val();
\t\t\t\t\tvar type_add \t\t= \$('#type_add').val();
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t\$( \".myErrorClass\" ).removeClass( \"myErrorClass\" );
\t\t\t\t\t
\t\t\t\t\tif(mr_emp_id == ''){
\t\t\t\t\t\t\$( \"#name_emp\" ).addClass( \"myErrorClass\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_code == ''){
\t\t\t\t\t\t\$( \"#mr_emp_code\" ).addClass( \"myErrorClass\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
\t\t\t\t\t\treturn;
\t\t\t\t\t}if(mr_emp_tel == ''){
\t\t\t\t\t\t\$( \"#mr_emp_tel\" ).addClass( \"myErrorClass\" );
\t\t\t\t\t\talertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
\t\t\t\t\t\treturn;
\t\t\t\t\t}
\t\t\t\t\t
\talertify.confirm(\"ตรวจสอบข้อมูล\", \"กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก\",
\t\t\tfunction(){
\t\t\t\t\$.ajax({
\t\t\t\t\t\t  dataType: \"json\",
\t\t\t\t\t\t  method: \"POST\",
\t\t\t\t\t\t  url: \"ajax/ajax_save_data_contact.php\",
\t\t\t\t\t\t  data: { 
\t\t\t\t\t\t\t\tdepartment_code \t\t\t \t : department_code \t\t\t \t,
\t\t\t\t\t\t\t\tcon_emp_name \t\t\t \t : con_emp_name \t\t\t \t,
\t\t\t\t\t\t\t\tcon_remark \t\t\t \t : con_remark \t\t\t \t,
\t\t\t\t\t\t\t\tcon_floor \t\t\t \t : con_floor \t\t\t \t,
\t\t\t\t\t\t\t\tcon_department_name \t : con_department_name \t,
\t\t\t\t\t\t\t\ttype_add \t : type_add \t,
\t\t\t\t\t\t\t\tmr_emp_tel \t : mr_emp_tel \t,
\t\t\t\t\t\t\t\tmr_emp_code  : mr_emp_code ,
\t\t\t\t\t\t\t\tmr_emp_id \t : mr_emp_id \t,
\t\t\t\t\t\t\t\tmr_contact_id: mr_contact_id
\t\t\t\t\t\t  }
\t\t\t\t\t\t}).done(function(data) {
\t\t\t\t\t\t\t\$('#csrf_token').val(data.token);

\t\t\t\t\t\t\talertify.alert(data['st'],data['msg'],
\t\t\t\t\t\t\tfunction(){
\t\t\t\t\t\t\t\twindow.location.reload();
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t  },function(){
\t\t\t\t\talertify.error('Cancel');
\t  });
\t
\t
\t//alert( \"Handler for .click() called.\" );
\t//is-invalid
});



\t\t
{% endblock %}\t\t\t\t
{% block javaScript %}
\t\t
function load_modal(mr_contact_id,type) {\t

 \$('#editCon').modal({backdrop: 'false'});

\$.ajax({
\t  dataType: \"json\",
\t  method: \"POST\",
\t  url: \"ajax/ajax_save_data_contact.php\",
\t  data: { \t
\t\t\t\tmr_contact_id : mr_contact_id,
\t\t\t\tpage : 'getdata'
\t  }
\t}).done(function(data) {
\t\t//alert('55555');
\t\tif(type!= \"add\"){
\t\t\t\$('#mr_contact_id').val(data['mr_contact_id']);
\t\t\t
\t\t}
\t\t
\t\t\$('#type_add').val(type);
\t\t\$('#mr_emp_code').val(data['emp_code']);
\t\t\$('#con_department_name').val(data['department_name']);
\t\t\$('#con_floor').val(data['floor']);
\t\t\$('#con_remark').val(data['remark']);
\t\t\$('#department_code').val(data['department_code']);
\t});
\t
}


\t

\t\t
{% endblock %}\t\t\t\t\t
\t\t\t\t
{% block styleReady %}

\t.btn {
\t\tbox-shadow: 0px 0px 0px 0px rgba(0,0,0,.0) !important;
\t}
\t.modal-backdrop {
\t\tposition: relative !important;
\t}
\t.modal {
\t\ttop: 40% !important;
\t}
\t.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}

{% endblock %}\t

{% block Content %}


\t
\t\t\t<div class=\"form-group\" style=\"text-align: center;\">
\t\t\t\t <label><h4> ข้อมูลผู้สั่งงาน </h4></label>
\t\t\t</div>\t
\t\t\t <input type=\"hidden\" id=\"user_id\" value=\"{{ user_data.mr_user_id }}\">
\t\t\t <input type=\"hidden\" id=\"emp_id\" value=\"{{ user_data.mr_emp_id }}\">
\t\t\t <input type=\"hidden\" id = \"csrf_token\" name=\"csrf_token\" value=\"{{csrf}}\">

\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสพนักงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_emp\" placeholder=\"รหัสพนักงาน\" value=\"{{ user_data.mr_emp_code }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"name\" placeholder=\"ชื่อ\" value=\"{{ user_data.mr_emp_name }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tนามสกุล :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"last_name\" placeholder=\"นามสกุล\" value=\"{{ user_data.mr_emp_lastname }}\" readonly>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์โทรศัพท์ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel\" placeholder=\"เบอร์โทรศัพท์\" value=\"{{ user_data.mr_emp_tel }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tเบอร์มือถือ :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"tel_mobile\" placeholder=\"เบอร์มือถือ\" value=\"{{ user_data.mr_emp_mobile }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tEmail :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"email\" placeholder=\"email\" value=\"{{ user_data.mr_emp_email }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tแผนก :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"department\" style=\"width:100%;\">
\t\t\t\t\t\t\t{% for s in department_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ s.mr_department_id }}\" {% if s.mr_department_id == user_data.mr_department_id %} selected=\"selected\" {% endif %}>{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"floor\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"\"> ไม่มีข้อมูลชั้น</option>
\t\t\t\t\t\t\t{% for f in floor_data %}
\t\t\t\t\t\t\t\t<option value=\"{{ f.mr_floor_id }}\" {% if f.mr_floor_id == user_data.floor_emp %} selected=\"selected\" {% endif %}>{{ f.name }}</option>
\t\t\t\t\t\t\t{% endfor %}\t\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t{% if role_id == 2 or role_id == 5%}
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px;color:red;\">
\t\t\t\t***สถานที่ปฏิบัติงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<select class=\"form-control-lg\" id=\"mr_role_id\" style=\"width:100%;\">
\t\t\t\t\t\t\t<option value=\"2\" {% if role_id == 2 %}selected {% endif %}> สำนักงาน(พหลโยธิน)</option>\t\t\t\t
\t\t\t\t\t\t\t<option value=\"5\" {% if role_id == 5 %}selected {% endif %}> สาขา/สาขาและอาคารอื่นๆ</option>\t\t\t\t
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t{% endif %}
\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary btn-block\" id=\"btn_save\">บันทึกการแก้ไข</button>
\t\t\t\t<br>
\t\t\t\t<center>
\t\t\t\t\t<a href = \"../user/change_password.php?usr={{ userID }}\"><b> เปลี่ยนรหัสผ่าน คลิกที่นี่</b></a>
\t\t\t\t</center>
\t\t\t</div>
\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t
\t\t\t
\t\t\t{% if con_data|length > 0 %}
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<h4>ข้อมูลหน่วยงานที่อยู่ในความรับผิดชอบรับเอกสาร</h4><br>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<table class=\"table table-striped\" align=\"center\" border=\"1\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">#</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">พนักงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">รหัสหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">ชื่อหน่วยงาน</th>
\t\t\t\t\t\t\t\t\t\t<th  class=\"text-center\">จัดการ <button type=\"button\" class=\"btn btn-link\"
\t\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('{{con_data.0.mr_contact_id}}','add');\">
\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"material-icons\">add_circle</i>
\t\t\t\t\t\t\t\t\t\t\t</button></th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t{% for c in con_data %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>{{ loop.index }}</td>
\t\t\t\t\t\t\t\t\t\t<td>{{ c.emp_code }}  {{ c.mr_contact_name }}</td>
\t\t\t\t\t\t\t\t\t\t<td>{{ c.department_code }}</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t   {{         c.department_name  }} [{{         c.remark  }}]   
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t<button {% if c.emp_code  != user_data.mr_emp_code %} disabled {% endif %}
\t\t\t\t\t\t\t\t\t\t\t\tonclick=\"load_modal('{{c.mr_contact_id}}',null);\" class=\"btn btn-link\" style=\"text-decoration: underline;\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">create</i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t<br>
\t\t\t<br>
\t\t\t<br>
\t\t\t<!-- <div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tรหัสหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"pass_depart\" placeholder=\"รหัสหน่วยงาน/รหัสค่าใช้จ่าย\" value=\"{{ user_data.mr_department_code }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชั้น :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"floor\" placeholder=\"ชั้น\" value=\"{{ user_data.mr_department_floor }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-4\" style=\"padding-right:0px\">
\t\t\t\t\t\tชื่อหน่วยงาน :
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-8\" style=\"padding-left:0px\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-sm\" id=\"depart\" placeholder=\"ชื่อหน่วยงาน\" value=\"{{ user_data.mr_department_name }}\">
\t\t\t\t\t</div>\t\t
\t\t\t\t</div>\t\t\t
\t\t\t</div>
\t\t\t\t -->

\t
<!-- Modal -->
<div class=\"modal fade\" id=\"editCon\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อ - นามสกุล</label><br>
\t\t\t\t
\t\t\t\t<select class=\"form-control\" id=\"name_emp\" style=\"width:100%;\">
\t\t\t\t  <option value=\"\">{{con_data.0.emp_code}} : {{con_data.0.mr_contact_name}}</option>
\t\t\t\t  {% for emp in all_emp %}
\t\t\t\t  <option value=\"{{ emp.mr_emp_id }}\">{{emp.mr_emp_code}} : {{emp.mr_emp_name}}{{emp.mr_emp_lastname}}</option>
\t\t\t\t  {% endfor %}
\t\t\t\t</select>
\t\t\t\t
\t\t\t  </div>
\t\t\t 
\t\t\t  
\t\t\t <div class=\"form-group\">
\t\t\t\t<label for=\"\">เบอร์โทร</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_emp_tel\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"type_add\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_contact_id\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"mr_emp_code\" value=\"\">
\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"con_emp_name\" value=\"\">
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">รหัสหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"department_code\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชื่อหน่วยงาน</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_department_name\" value=\"\" readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">ชั้น</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"con_floor\" value=\"\"  readonly>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t\t<label for=\"\">หมายเหตุ</label>
\t\t\t\t <textarea class=\"form-control\" id=\"con_remark\" rows=\"3\"></textarea >
\t\t\t  </div>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"button\" id=\"btnCon_save\" class=\"btn btn-primary\">Save changes</button>
      </div>
    </div>
  </div>
</div>
\t
\t\t\t\t
\t\t\t
\t\t\t
\t\t
\t
 
\t
\t
\t
\t
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "data/profile.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\data\\profile.tpl");
    }
}

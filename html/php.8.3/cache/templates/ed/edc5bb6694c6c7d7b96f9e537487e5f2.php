<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mailroom/mailroom_sendWorkTNT.tpl */
class __TwigTemplate_4b2ca71a10b1d567c1cd36b4c802e818 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_e1' => [$this, 'block_menu_e1'],
            'styleReady' => [$this, 'block_styleReady'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content2' => [$this, 'block_Content2'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "mailroom/mailroom_sendWorkTNT.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Pivot- List";
    }

    // line 5
    public function block_menu_e1($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 6
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}
      #tb_work_order {
        font-size: 13px;
      }
\t  .panel {
\t\tmargin-bottom : 5px;
\t  }
#loader{
\t  width:5%;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

";
    }

    // line 43
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\" integrity=\"sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i\" crossorigin=\"anonymous\"></script>

";
    }

    // line 55
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "\$('#bg_loader').hide()
\t\tload_data();
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'sys_timestamp'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'branch' },
\t\t\t\t{ 'data':'barcode_tnt' },
\t\t\t\t{ 'data':'send_round' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
\t\t\t
\t\t
\t\t
";
    }

    // line 86
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 87
        echo "\t\t
\tfunction saveround() {
\t\tvar dataall \t\t= [];
\t\tvar mr_user_id      = \$('#mr_user_id').val();
\t\tvar tnt_tracking    = \$('#tnt_tracking').val();
\t\tvar tbl_data \t\t= \$('#tb_work_order').DataTable();
\t\t
\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t //console.log(this.value);
\t\t\t dataall.push(this.value);
\t\t  });
\t\t  //console.log(tbl_data);
\t\t  hub_id = \$('#hub_id').val();
\t\t  if(hub_id == 'all_hub'){
\t\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่ ระบุ Hub\"); 
\t\t\t\t return;
\t\t\t}

\t\t  if(hub_id == 'all_branch'){
\t\t\t   if(tnt_tracking == '' || tnt_tracking == null){
\t\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่ ระบุ TNT Tracking\"); 
\t\t\t\t return;
\t\t\t   }
\t\t\t}else{
\t\t\t\tif(mr_user_id == ''){
\t\t\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือก Mess\"); 
\t\t\t\t\treturn;
\t\t\t\t}
\t\t\t}\t
\t\t  
\t\t  if(dataall.length < 1){
\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t return;
\t\t  }
\t\t  
\t\tvar newdataall = dataall.join(\",\");

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_saveround.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'main_id'\t\t: newdataall,
\t\t\t\t'mr_user_id'\t: mr_user_id,
\t\t\t\t'hub_id'\t\t: hub_id,
\t\t\t\t'tnt_tracking'\t: tnt_tracking
\t\t\t\t
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+msg.message,function(){window.location.reload();});
\t\t\t}else{
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#mr_branch_id').val('all_branch').trigger('change');
\t\t\t\t//\$('#hub_id').val('');
\t\t\t\tload_data();
\t\t\t}
\t\t});
\t}
\t
\tfunction print_all() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});
\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data1').val(newdataall);
\t\t\$('#print_1').submit();
\t}
\t
\tfunction print_byHub() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});

\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data2').val(newdataall);
\t\t\$('#print_2').submit();
\t}

\tfunction changHub(){
\t\tvar hub_id = \$('#hub_id').val();
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_branch_Byhub.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'html',
\t\t\tdata: {
\t\t\t\t'hub_id': hub_id
\t\t\t},
\t\tbeforeSend: function( xhr ) {
\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert('ผิดพลาด',msg.message);
\t\t\t}else{
\t\t\t\t\$('#mr_branch_id').html(msg.data);
\t\t\t\tload_data();
\t\t\t}
\t\t});
\t}
\tfunction load_data(){
\t\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\tvar hub_id = \$('#hub_id').val();
\t\tvar data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';
\t\tif(hub_id == 'all_branch'){
\t\t\t\$('#div_mr_user_id').hide();
\t\t\t\$('#div_tnt_tracking').show();
\t\t}else{
\t\t\t\$('#div_mr_user_id').show();
\t\t\t\$('#div_tnt_tracking').hide();
\t\t}
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_send_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_branch_id'\t: mr_branch_id,
\t\t\t\t'data_all'\t\t: data_all,
\t\t\t\t'hub_id'\t\t: hub_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();\t\t
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert('แจ้งเตือน',msg.message);
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t}else{
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t}
\t\t});\t\t
\t}
";
    }

    // line 239
    public function block_Content2($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 240
        echo "<form id=\"print_1\" action=\"print_sort_branch_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data1\" name=\"data1\">
</form>
<form id=\"print_2\" action=\"print_hab_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data2\" name=\"data2\">
</form>
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ส่งออกเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-12\">
\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">select Hub</label>
\t\t\t\t\t\t\t\t\t\t<select onchange=\"changHub();\" name=\"hub_id\" class=\"form-control\" id=\"hub_id\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"all_hub\">ทุก Hub</option>
\t\t\t\t\t\t\t\t\t\t  <option value=\"all_branch\">สาขาต่างจังหวัดทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t  ";
        // line 259
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hub"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 260
            echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_hub_id", [], "any", false, false, false, 260), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_hub_name", [], "any", false, false, false, 260), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 262
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-12\">
\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">select สาขา</label>
\t\t\t\t\t\t\t\t\t\t<select onchange=\"load_data();\" name=\"mr_branch_id\" class=\"form-control\" id=\"mr_branch_id\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"\">ทุกสาขา</option>
\t\t\t\t\t\t\t\t\t\t  ";
        // line 270
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["branch"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 271
            echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_id", [], "any", false, false, false, 271), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_code", [], "any", false, false, false, 271), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_branch_name", [], "any", false, false, false, 271), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 273
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-12\" id=\"div_tnt_tracking\">
\t\t\t\t\t\t\t\t\t\t<label for=\"tnt_tracking\">TNT Tracking</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"tnt_tracking\" placeholder=\"Ttracking No\"> 
\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t  <div class=\"form-group col-md-12\" id=\"div_mr_user_id\">
\t\t\t\t\t\t\t\t\t\t<label for=\"mr_user_id\">select Mess</label>
\t\t\t\t\t\t\t\t\t\t<select name=\"mr_user_id\" class=\"form-control\" id=\"mr_user_id\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"\">เลือก Mess</option>
\t\t\t\t\t\t\t\t\t\t  ";
        // line 287
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["emp"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 288
            echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_user_id", [], "any", false, false, false, 288), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_emp_code", [], "any", false, false, false, 288), "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_emp_name", [], "any", false, false, false, 288), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["b"], "mr_emp_lastname", [], "any", false, false, false, 288), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 290
        echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t <label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input onclick=\"load_data();\"id=\"data_all\" name=\"data_all\" type=\"checkbox\" class=\"custom-control-input\" value=\"1\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">แสดงงานที่สั่งวันนี้เท่านั้น</span>
\t\t\t\t\t\t\t\t\t</label>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-sm-12\"><br>
\t\t\t\t\t\t\t\t";
        // line 312
        echo "\t\t\t\t\t\t\t\t<button onclick=\"saveround();\"type=\"button\" class=\"btn btn-outline-primary\" id=\"\" onclick=\"\">บันทึกข้อมูลรอบ</button> 
\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>Date/Time</th>
\t\t\t\t\t\t\t\t<th>Bacode</th>
\t\t\t\t\t\t\t\t<th>Sender</th>
\t\t\t\t\t\t\t\t<th>Receiver</th>
\t\t\t\t\t\t\t\t<th>Type Work</th>
\t\t\t\t\t\t\t\t<th>Branch</th>
\t\t\t\t\t\t\t\t<th>TNT</th>
\t\t\t\t\t\t\t\t<th>SendRound</th>
\t\t\t\t\t\t\t\t<th class=\"text-center\">
\t\t\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือกทั้งหมด</span>
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
";
    }

    // line 367
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 369
        if ((($context["debug"] ?? null) != "")) {
            // line 370
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 372
        echo "
";
    }

    public function getTemplateName()
    {
        return "mailroom/mailroom_sendWorkTNT.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  513 => 372,  507 => 370,  505 => 369,  498 => 367,  442 => 312,  422 => 290,  407 => 288,  403 => 287,  387 => 273,  374 => 271,  370 => 270,  360 => 262,  349 => 260,  345 => 259,  324 => 240,  320 => 239,  166 => 87,  162 => 86,  131 => 56,  127 => 55,  114 => 44,  110 => 43,  71 => 7,  67 => 6,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
\t\t\t\tbackground-color: #555;
\t\t\t\tcolor: #fff;
\t\t\t\tcursor:pointer;
\t\t\t}
      #tb_work_order {
        font-size: 13px;
      }
\t  .panel {
\t\tmargin-bottom : 5px;
\t  }
#loader{
\t  width:5%;
\t  display:table;
\t  margin: auto;
\t  border-radius:50%;
\t}#bg_loader{
\t\tposition:absolute;
\t\ttop:0px;
\t\tbackground-color:rgba(255,255,255,0.7);
\t\theight:100%;
\t\twidth:100%;
\t}

{% endblock %}
{% block scriptImport %}
<link rel=\"stylesheet\" href=\"../themes/bootstrap/css/jquery.dataTables.css\">
<link rel=\"stylesheet\" href=\"css/chosen.css\">

<script src=\"../themes/bootstrap/js/jquery.dataTables.min.js\"></script>
<script src=\"js/chosen.jquery.js\" charset=\"utf-8\"></script>
<script src=\"js/daterange.js\" charset=\"utf-8\"></script>

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js\" integrity=\"sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i\" crossorigin=\"anonymous\"></script>

{% endblock %}

{% block domReady %}
\$('#bg_loader').hide()
\t\tload_data();
\tvar table = \$('#tb_work_order').DataTable({
\t\t\t'responsive': true,
\t\t\t'columns': [
\t\t\t\t{ 'data':'no' },
\t\t\t\t{ 'data':'sys_timestamp'},
\t\t\t\t{ 'data':'mr_work_barcode' },
\t\t\t\t{ 'data':'name_send' },
\t\t\t\t{ 'data':'name_receive' },
\t\t\t\t{ 'data':'mr_type_work_name' },
\t\t\t\t{ 'data':'branch' },
\t\t\t\t{ 'data':'barcode_tnt' },
\t\t\t\t{ 'data':'send_round' },
\t\t\t\t{ 'data':'mr_status_name' }
\t\t\t],
\t\t\t
\t\t\t'scrollCollapse': true 
\t\t});
\$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   \$('input[type=\"checkbox\"]', rows).prop('checked', this.checked);
});
\t\t\t
\t\t
\t\t
{% endblock %}


{% block javaScript %}
\t\t
\tfunction saveround() {
\t\tvar dataall \t\t= [];
\t\tvar mr_user_id      = \$('#mr_user_id').val();
\t\tvar tnt_tracking    = \$('#tnt_tracking').val();
\t\tvar tbl_data \t\t= \$('#tb_work_order').DataTable();
\t\t
\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\t //console.log(this.value);
\t\t\t dataall.push(this.value);
\t\t  });
\t\t  //console.log(tbl_data);
\t\t  hub_id = \$('#hub_id').val();
\t\t  if(hub_id == 'all_hub'){
\t\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่ ระบุ Hub\"); 
\t\t\t\t return;
\t\t\t}

\t\t  if(hub_id == 'all_branch'){
\t\t\t   if(tnt_tracking == '' || tnt_tracking == null){
\t\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่ ระบุ TNT Tracking\"); 
\t\t\t\t return;
\t\t\t   }
\t\t\t}else{
\t\t\t\tif(mr_user_id == ''){
\t\t\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือก Mess\"); 
\t\t\t\t\treturn;
\t\t\t\t}
\t\t\t}\t
\t\t  
\t\t  if(dataall.length < 1){
\t\t\t alertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\t return;
\t\t  }
\t\t  
\t\tvar newdataall = dataall.join(\",\");

\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_saveround.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'main_id'\t\t: newdataall,
\t\t\t\t'mr_user_id'\t: mr_user_id,
\t\t\t\t'hub_id'\t\t: hub_id,
\t\t\t\t'tnt_tracking'\t: tnt_tracking
\t\t\t\t
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert('ผิดพลาด',\"บันทึกไม่สำเร็จ  \"+msg.message,function(){window.location.reload();});
\t\t\t}else{
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#mr_branch_id').val('all_branch').trigger('change');
\t\t\t\t//\$('#hub_id').val('');
\t\t\t\tload_data();
\t\t\t}
\t\t});
\t}
\t
\tfunction print_all() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\t tbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});
\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data1').val(newdataall);
\t\t\$('#print_1').submit();
\t}
\t
\tfunction print_byHub() {
\t\tvar dataall = [];
\t\tvar tbl_data = \$('#tb_work_order').DataTable();
\t\ttbl_data.\$('input[type=\"checkbox\"]:checked').each(function(){
\t\t\tdataall.push(this.value);
\t\t});

\t\tif(dataall.length < 1){
\t\t\talertify.alert(\"alert\",\"ท่านยังไม่เลือกงาน\"); 
\t\t\treturn;
\t\t}
\t\tvar newdataall = dataall.join(\",\");
\t\t\$('#data2').val(newdataall);
\t\t\$('#print_2').submit();
\t}

\tfunction changHub(){
\t\tvar hub_id = \$('#hub_id').val();
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_branch_Byhub.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'html',
\t\t\tdata: {
\t\t\t\t'hub_id': hub_id
\t\t\t},
\t\tbeforeSend: function( xhr ) {
\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert('ผิดพลาด',msg.message);
\t\t\t}else{
\t\t\t\t\$('#mr_branch_id').html(msg.data);
\t\t\t\tload_data();
\t\t\t}
\t\t});
\t}
\tfunction load_data(){
\t\tvar mr_branch_id = \$('#mr_branch_id').val();
\t\tvar hub_id = \$('#hub_id').val();
\t\tvar data_all = \$(\"#data_all\").prop(\"checked\") ? '1' : '0';
\t\tif(hub_id == 'all_branch'){
\t\t\t\$('#div_mr_user_id').hide();
\t\t\t\$('#div_tnt_tracking').show();
\t\t}else{
\t\t\t\$('#div_mr_user_id').show();
\t\t\t\$('#div_tnt_tracking').hide();
\t\t}
\t\t\$.ajax({
\t\t\turl: \"ajax/ajax_load_data_send_branch.php\",
\t\t\ttype: \"post\",
\t\t\tdataType:'json',
\t\t\tdata: {
\t\t\t\t'mr_branch_id'\t: mr_branch_id,
\t\t\t\t'data_all'\t\t: data_all,
\t\t\t\t'hub_id'\t\t: hub_id
\t\t\t},
\t\t   beforeSend: function( xhr ) {
\t\t\t\t\$('#bg_loader').show();\t\t
\t\t\t}
\t\t}).done(function( msg ) {
\t\t\tif(msg.status == 500 || msg.status == 401){
\t\t\t\talertify.alert('แจ้งเตือน',msg.message);
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t}else{
\t\t\t\t\$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
\t\t\t\t\$('#bg_loader').hide();
\t\t\t\t\$('#tb_work_order').DataTable().clear().draw();
\t\t\t\t\$('#tb_work_order').DataTable().rows.add(msg.data).draw();
\t\t\t}
\t\t});\t\t
\t}
{% endblock %}

{% block Content2 %}
<form id=\"print_1\" action=\"print_sort_branch_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data1\" name=\"data1\">
</form>
<form id=\"print_2\" action=\"print_hab_all.php\" method=\"post\" target=\"_blank\">
  <input type=\"hidden\" id=\"data2\" name=\"data2\">
</form>
\t\t<div class=\"row\" border=\"1\">
\t\t\t<div class=\"col-12\">
\t\t\t\t<div class=\"card\">
\t\t\t\t\t<h4 class=\"card-header\">ส่งออกเอกสาร</h4>
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-12\">
\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">select Hub</label>
\t\t\t\t\t\t\t\t\t\t<select onchange=\"changHub();\" name=\"hub_id\" class=\"form-control\" id=\"hub_id\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"all_hub\">ทุก Hub</option>
\t\t\t\t\t\t\t\t\t\t  <option value=\"all_branch\">สาขาต่างจังหวัดทั้งหมด</option>
\t\t\t\t\t\t\t\t\t\t  {% for b in hub %}
\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{b.mr_hub_id}}\">{{b.mr_hub_name}}</option>
\t\t\t\t\t\t\t\t\t\t  {% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-12\">
\t\t\t\t\t\t\t\t\t\t<label for=\"exampleFormControlSelect1\">select สาขา</label>
\t\t\t\t\t\t\t\t\t\t<select onchange=\"load_data();\" name=\"mr_branch_id\" class=\"form-control\" id=\"mr_branch_id\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"\">ทุกสาขา</option>
\t\t\t\t\t\t\t\t\t\t  {% for b in branch %}
\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{b.mr_branch_id}}\">{{b.mr_branch_code}} : {{b.mr_branch_name}}</option>
\t\t\t\t\t\t\t\t\t\t  {% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t <div class=\"form-group col-md-12\" id=\"div_tnt_tracking\">
\t\t\t\t\t\t\t\t\t\t<label for=\"tnt_tracking\">TNT Tracking</label>
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"tnt_tracking\" placeholder=\"Ttracking No\"> 
\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t  <div class=\"form-group col-md-12\" id=\"div_mr_user_id\">
\t\t\t\t\t\t\t\t\t\t<label for=\"mr_user_id\">select Mess</label>
\t\t\t\t\t\t\t\t\t\t<select name=\"mr_user_id\" class=\"form-control\" id=\"mr_user_id\">
\t\t\t\t\t\t\t\t\t\t  <option value=\"\">เลือก Mess</option>
\t\t\t\t\t\t\t\t\t\t  {% for b in emp %}
\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{b.mr_user_id}}\">{{b.mr_emp_code}} : {{b.mr_emp_name}} {{b.mr_emp_lastname}}</option>
\t\t\t\t\t\t\t\t\t\t  {% endfor %}
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t  </div>\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t\t\t <label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input onclick=\"load_data();\"id=\"data_all\" name=\"data_all\" type=\"checkbox\" class=\"custom-control-input\" value=\"1\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">แสดงงานที่สั่งวันนี้เท่านั้น</span>
\t\t\t\t\t\t\t\t\t</label>\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-sm-12\"><br>
\t\t\t\t\t\t\t\t{#
\t\t\t\t\t\t\t\t<button onclick=\"print_byHub();\"type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมใหญ่</button>
\t\t\t\t\t\t\t\t<button onclick=\"print_all();\" type=\"button\" class=\"btn btn-outline-secondary\" id=\"\">พิมพ์ใบคุมย่อย</button>
\t\t\t\t\t\t\t\t#}
\t\t\t\t\t\t\t\t<button onclick=\"saveround();\"type=\"button\" class=\"btn btn-outline-primary\" id=\"\" onclick=\"\">บันทึกข้อมูลรอบ</button> 
\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t  <table class=\"table table-bordered table-hover display responsive no-wrap\" id=\"tb_work_order\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t<th>Date/Time</th>
\t\t\t\t\t\t\t\t<th>Bacode</th>
\t\t\t\t\t\t\t\t<th>Sender</th>
\t\t\t\t\t\t\t\t<th>Receiver</th>
\t\t\t\t\t\t\t\t<th>Type Work</th>
\t\t\t\t\t\t\t\t<th>Branch</th>
\t\t\t\t\t\t\t\t<th>TNT</th>
\t\t\t\t\t\t\t\t<th>SendRound</th>
\t\t\t\t\t\t\t\t<th class=\"text-center\">
\t\t\t\t\t\t\t\t\t<label class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t\t\t<input id=\"select-all\" name=\"select_all\" type=\"checkbox\" class=\"custom-control-input\">
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-indicator\"></span>
\t\t\t\t\t\t\t\t\t\t<span class=\"custom-control-description\">เลือกทั้งหมด</span>
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t  </table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:20px;\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-12 text-center\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<hr>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t<br>
\t\t 
\t
\t
\t
 <div id=\"bg_loader\" class=\"card\">
\t\t\t<img id = 'loader'src=\"../themes/images/spinner.gif\">
\t\t</div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}
", "mailroom/mailroom_sendWorkTNT.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\mailroom\\mailroom_sendWorkTNT.tpl");
    }
}

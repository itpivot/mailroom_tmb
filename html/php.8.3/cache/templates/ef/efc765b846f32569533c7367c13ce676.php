<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base_login.tpl */
class __TwigTemplate_074293e204ef6d15a5e3d321159ba392 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'styleReady' => [$this, 'block_styleReady'],
            'body' => [$this, 'block_body'],
            'domReady' => [$this, 'block_domReady'],
            'javascript' => [$this, 'block_javascript'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> -->
<!DOCTYPE html>
<!-- <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\"> -->
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
     
\t<meta name=\"author\" content=\"Pivot\">
\t<title>Pivot Mailroom Services</title>
\t<link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" />

   <!--  <link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" /> -->
\t <!-- Bootstrap core CSS -->
    <link href=\"../themes/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" />
    
    <!-- Custom styles for this template -->
    <link href=\"../themes/bootstrap/css/signin.css\" rel=\"stylesheet\" />
   <link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/alertify.css\" id=\"alertifyCSS\">
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/themes/default.css\" >
  <style type='text/css'>
    ";
        // line 22
        $this->displayBlock('styleReady', $context, $blocks);
        // line 23
        echo "    html, body {
      height: 100%;
    }
    #content {
      min-height: 85%;
    }

    .footer,
    .push {
      height: 10px;
      text-align: center;
    }

    @media (min-width: 481px) and (max-width: 767px) {
      font-size:12px;
    }



    @media (min-width: 320px) and (max-width: 480px) {
      font-size: 12px;
    }
  </style>
</head>
<body>
    <div class=\"container\" id='content'>
    ";
        // line 49
        $this->displayBlock('body', $context, $blocks);
        // line 50
        echo "    
    <div class=\"push\"></div>
    </div>
    <!--Start Footer-->
    <footer class=\"footer\">
      <div class=\"container\">
        <p class=\"text-muted\"><strong>ติดต่อห้อง Mailroom:</strong>02-299-1111 ต่อ 5694 </p>
        <p class=\"\"><b>ปัญหาการใช้งานระบบติดต่อที่:</b></p>
        <p class=\"text-muted\"><strong></strong>คุณจุฑารัตน์ เนียม ศรี เบอร์โทร 061-823-4346 อีเมล์ jutarat_ni@pivot.co.th</p>
        <p class=\"text-muted\"><strong></strong>คุณณิธิวัชรา อยู่ถิ่น เบอร์โทร 02-299-5694 (ห้องสารบรรณกลาง) อีเมล์ Mailroom@pivot.co.th</p>

        <p class=\"text-muted\"></p>
      </div>
    </footer>
    <!--End Footer-->
    <!-- <script src='../themes/bootstrap/js/jquery-3.1.1.min.js'></script> -->
    <script src=\"../themes/bootstrap/js/jquery-1.12.4.js\"></script>
    <!-- <script src='../themes/bootstrap_emp/dist/sweetalert2.min.js'></script> -->
    <script src=\"../themes/bootstrap/js/ie-emulation-modes-warning.js\"></script>
   <script src=\"../themes/alertifyjs/alertify.js\"></script> 
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=\"../themes/bootstrap/js/ie10-viewport-bug-workaround.js\"></script>
    <script type='text/javascript'>
        \$(document).ready(function() {
          ";
        // line 75
        $this->displayBlock('domReady', $context, $blocks);
        // line 76
        echo "        });
      
        ";
        // line 78
        $this->displayBlock('javascript', $context, $blocks);
        // line 79
        echo "    </script> 
        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-116752839-1\"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-116752839-1');
    </script>

  </body>
</html>";
    }

    // line 22
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 49
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 75
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 78
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "base_login.tpl";
    }

    public function getDebugInfo()
    {
        return array (  165 => 78,  159 => 75,  153 => 49,  147 => 22,  131 => 79,  129 => 78,  125 => 76,  123 => 75,  96 => 50,  94 => 49,  66 => 23,  64 => 22,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> -->
<!DOCTYPE html>
<!-- <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\"> -->
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
     
\t<meta name=\"author\" content=\"Pivot\">
\t<title>Pivot Mailroom Services</title>
\t<link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" />

   <!--  <link rel=\"icon\" href=\"../themes/bootstrap/css/favicon.ico\" /> -->
\t <!-- Bootstrap core CSS -->
    <link href=\"../themes/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" />
    
    <!-- Custom styles for this template -->
    <link href=\"../themes/bootstrap/css/signin.css\" rel=\"stylesheet\" />
   <link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/alertify.css\" id=\"alertifyCSS\">
\t<link rel=\"stylesheet\" href=\"../themes/alertifyjs/css/themes/default.css\" >
  <style type='text/css'>
    {% block styleReady %}{% endblock %}
    html, body {
      height: 100%;
    }
    #content {
      min-height: 85%;
    }

    .footer,
    .push {
      height: 10px;
      text-align: center;
    }

    @media (min-width: 481px) and (max-width: 767px) {
      font-size:12px;
    }



    @media (min-width: 320px) and (max-width: 480px) {
      font-size: 12px;
    }
  </style>
</head>
<body>
    <div class=\"container\" id='content'>
    {% block body %}{% endblock %}
    
    <div class=\"push\"></div>
    </div>
    <!--Start Footer-->
    <footer class=\"footer\">
      <div class=\"container\">
        <p class=\"text-muted\"><strong>ติดต่อห้อง Mailroom:</strong>02-299-1111 ต่อ 5694 </p>
        <p class=\"\"><b>ปัญหาการใช้งานระบบติดต่อที่:</b></p>
        <p class=\"text-muted\"><strong></strong>คุณจุฑารัตน์ เนียม ศรี เบอร์โทร 061-823-4346 อีเมล์ jutarat_ni@pivot.co.th</p>
        <p class=\"text-muted\"><strong></strong>คุณณิธิวัชรา อยู่ถิ่น เบอร์โทร 02-299-5694 (ห้องสารบรรณกลาง) อีเมล์ Mailroom@pivot.co.th</p>

        <p class=\"text-muted\"></p>
      </div>
    </footer>
    <!--End Footer-->
    <!-- <script src='../themes/bootstrap/js/jquery-3.1.1.min.js'></script> -->
    <script src=\"../themes/bootstrap/js/jquery-1.12.4.js\"></script>
    <!-- <script src='../themes/bootstrap_emp/dist/sweetalert2.min.js'></script> -->
    <script src=\"../themes/bootstrap/js/ie-emulation-modes-warning.js\"></script>
   <script src=\"../themes/alertifyjs/alertify.js\"></script> 
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src=\"../themes/bootstrap/js/ie10-viewport-bug-workaround.js\"></script>
    <script type='text/javascript'>
        \$(document).ready(function() {
          {% block domReady %}{% endblock %}
        });
      
        {% block javascript %}{% endblock %}
    </script> 
        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-116752839-1\"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-116752839-1');
    </script>

  </body>
</html>", "base_login.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\html\\php.8.3\\templates\\base_login.tpl");
    }
}

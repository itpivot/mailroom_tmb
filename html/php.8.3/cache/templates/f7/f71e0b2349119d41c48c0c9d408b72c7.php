<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messenger/receive_list.tpl */
class __TwigTemplate_2d9bc2226b4bc0d0b6f8c88e09cc4645 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'menu_msg3' => [$this, 'block_menu_msg3'],
            'scriptImport' => [$this, 'block_scriptImport'],
            'styleReady' => [$this, 'block_styleReady'],
            'domReady' => [$this, 'block_domReady'],
            'javaScript' => [$this, 'block_javaScript'],
            'Content' => [$this, 'block_Content'],
            'debug' => [$this, 'block_debug'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_emp2.tpl";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_emp2.tpl", "messenger/receive_list.tpl", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "PivotSend List ";
    }

    // line 5
    public function block_menu_msg3($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " active ";
    }

    // line 7
    public function block_scriptImport($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 9
    public function block_styleReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
    .card {
       margin: 8px 0px;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 70%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

";
    }

    // line 70
    public function block_domReady($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "    var receive_list = {};
    
    \$.ajax({
        url: './ajax/ajax_get_receiveMailroom.php',
        type: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            \$('#img_loading').show();
        },
        success: function(res) {
            document.getElementById(\"badge-show\").textContent=res.length;
\t\t\tlet set_style_time = '' ;
            let str = '';
\t\t\tif( res != \"\" ){
\t\t\t\tfor(let i = 0; i < res.length; i++) {
                    receive_list[i] = res[i]['mr_work_main_id'];

                    if( res[i]['diff_time'] == 1 ){
                        set_style_time = 'bg-danger';
                    }else{
                        set_style_time = '';
                    }
\t\t\t\t\t\t
\t\t\t\t\t\tif( res[i]['mr_status_receive'] == 2 ){
\t\t\t\t\t\t\t\tstr += '<div class=\"card bg-warning\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+'</a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-12 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else{
\t
\t\t\t\t\t\t\tstr += '<div class=\"card '+set_style_time+'\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoReceiveById('+res[i]['mr_work_main_id']+');\"><b>ไม่พบเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-4 text-right btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}
\t
\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\t\$('#btn_save_all').hide();
\t\t\t\tstr = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                 
\t\t\t}
            \$('#data_list').html(str);
        },
        complete: function() {
            \$('#img_loading').hide();
        }
    });

    \$('#txt_search').on('keyup', function() {
        receive_list = {};
        let txt = \$(this).val();
        let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_searchReceiveMailroom.php\",
            type: \"POST\",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                document.getElementById(\"badge-show\").textContent=res.length;
\t\t\tlet set_style_time = '' ;
            let str = '';
\t\t\tif( res != \"\" ){
\t\t\t\tfor(let i = 0; i < res.length; i++) {
                    receive_list[i] = res[i]['mr_work_main_id'];

                    if( res[i]['diff_time'] == 1 ){
                        set_style_time = 'bg-danger';
                    }else{
                        set_style_time = '';
                    }
\t\t\t\t\t\t
\t\t\t\t\t\tif( res[i]['mr_status_receive'] == 2 ){
\t\t\t\t\t\t\t\tstr += '<div class=\"card bg-warning\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+'</a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-12 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else{
\t
\t\t\t\t\t\t\tstr += '<div class=\"card '+set_style_time+'\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoReceiveById('+res[i]['mr_work_main_id']+');\"><b>ไม่พบเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-4 text-right btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}
\t
\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\t\$('#btn_save_all').hide();
\t\t\t\tstr = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                 
\t\t\t}
            \$('#data_list').html(str);
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
    });

    \$('#btn_save_all').on('click', function() {
\t\tif(confirm(\"ยืนยันการรับงานทั้งหมด!\")) {
            \$.ajax({
                url: './ajax/ajax_updateReceiveAll.php',
                type: 'POST',
                data: {
                    data: JSON.stringify(receive_list)
                },
                success: function(res){
                    if(res == \"success\") {
                        location.reload();
                    }
                }
            });
        }
    });
";
    }

    // line 260
    public function block_javaScript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 261
        echo "\t function goPageSend()
    {
\t\twindow.location.href = 'send_list.php'; 
\t}
\t
    function updateReceiveById(id, work_status)
    {
        if( work_status == 1 ){
\t\t\tif(confirm(\"ยืนยันการลบงาน!\")) {
\t\t\t\t//console.log('id :'+ id + ' status :' + work_status);
                \$.ajax({
                    url: './ajax/ajax_updateReceiveById.php',
                    type: 'POST',
                    data: {
                        id: id,
                        work_status: work_status
                    },
                    success: function(res) {
                        if(res == \"success\") {
                            location.reload();
                        }
                    }
                })
\t\t\t\talert('ลบงานเรียบร้อยแล้ว');
\t\t\t\t 
\t\t\t\t
\t\t\t}
        }else{
             \$.ajax({
                    url: './ajax/ajax_updateReceiveById.php',
                    type: 'POST',
                    data: {
                        id: id,
                        work_status: work_status
                    },
                    success: function(res) {
                        if(res == \"success\") {
                            location.reload();
                        }
                    }
                })
        }

        
        
    }
\t
\tfunction updateNoReceiveById(id) {
        \$.ajax({
            url: './ajax/ajax_updateNoReceiveById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == \"success\") {
                    location.reload();
                }
            }
        })
    }
";
    }

    // line 324
    public function block_Content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 325
        echo "    <div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหา\">
            </div>
        </form>
    </div>
    <div class=\"text-center\">
        <label>จำนวนเอกสาร</label>
        
            <span id=\"badge-show\" class=\"badge badge-secondary badge-pill badge-dark\"></span>

         <label>ฉบับ</label>
    </div>
\t<button type=\"button\" class=\"btn btn-primary btn-block btn-lg\" id=\"btn_save_all\">รับเอกสารทั้งหมด</button>
\t<div id=\"data_list\">
\t</div>
\t<div class=\"fixed-bottom text-right\">
\t\t<button onclick=\"goPageSend();\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">ส่งเอกสาร</button>
\t</div>


 </div>
";
    }

    // line 352
    public function block_debug($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 

\t";
        // line 354
        if ((($context["debug"] ?? null) != "")) {
            // line 355
            echo "\t\t<pre>";
            echo twig_escape_filter($this->env, ($context["debug"] ?? null), "html", null, true);
            echo "</pre>
\t";
        }
        // line 357
        echo "
";
    }

    public function getTemplateName()
    {
        return "messenger/receive_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  450 => 357,  444 => 355,  442 => 354,  435 => 352,  407 => 325,  403 => 324,  338 => 261,  334 => 260,  143 => 71,  139 => 70,  77 => 10,  73 => 9,  67 => 7,  60 => 5,  53 => 3,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base_emp2.tpl\" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .card {
       margin: 8px 0px;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 70%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
\t\tleft: 50%;
\t\ttop: 50%;
\t\t-webkit-transform: translate(-50%, -50%);
\t\ttransform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
\t\t
    }

\t.right {
\t\t // margin-right: -190px; 
\t\t// position: absolute;
\t\tright: 0px;
\t}
\t\t
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
\t
\t.fixed-bottom {
\t\tposition: sticky;
\t\tbottom: 0;
\t\t//top: 250px;
\t\tz-index: 1075;

\t}

{% endblock %}

{% block domReady %}
    var receive_list = {};
    
    \$.ajax({
        url: './ajax/ajax_get_receiveMailroom.php',
        type: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            \$('#img_loading').show();
        },
        success: function(res) {
            document.getElementById(\"badge-show\").textContent=res.length;
\t\t\tlet set_style_time = '' ;
            let str = '';
\t\t\tif( res != \"\" ){
\t\t\t\tfor(let i = 0; i < res.length; i++) {
                    receive_list[i] = res[i]['mr_work_main_id'];

                    if( res[i]['diff_time'] == 1 ){
                        set_style_time = 'bg-danger';
                    }else{
                        set_style_time = '';
                    }
\t\t\t\t\t\t
\t\t\t\t\t\tif( res[i]['mr_status_receive'] == 2 ){
\t\t\t\t\t\t\t\tstr += '<div class=\"card bg-warning\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+'</a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-12 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else{
\t
\t\t\t\t\t\t\tstr += '<div class=\"card '+set_style_time+'\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoReceiveById('+res[i]['mr_work_main_id']+');\"><b>ไม่พบเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-4 text-right btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}
\t
\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\t\$('#btn_save_all').hide();
\t\t\t\tstr = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                 
\t\t\t}
            \$('#data_list').html(str);
        },
        complete: function() {
            \$('#img_loading').hide();
        }
    });

    \$('#txt_search').on('keyup', function() {
        receive_list = {};
        let txt = \$(this).val();
        let str = \"\";
        \$.ajax({
            url: \"./ajax/ajax_searchReceiveMailroom.php\",
            type: \"POST\",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                \$('#img_loading').show();
                \$('#data_list').hide();
            },
            success: function(res){
                document.getElementById(\"badge-show\").textContent=res.length;
\t\t\tlet set_style_time = '' ;
            let str = '';
\t\t\tif( res != \"\" ){
\t\t\t\tfor(let i = 0; i < res.length; i++) {
                    receive_list[i] = res[i]['mr_work_main_id'];

                    if( res[i]['diff_time'] == 1 ){
                        set_style_time = 'bg-danger';
                    }else{
                        set_style_time = '';
                    }
\t\t\t\t\t\t
\t\t\t\t\t\tif( res[i]['mr_status_receive'] == 2 ){
\t\t\t\t\t\t\t\tstr += '<div class=\"card bg-warning\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+'</a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-12 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById('+res[i]['mr_work_main_id']+ ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}else{
\t
\t\t\t\t\t\t\tstr += '<div class=\"card '+set_style_time+'\">'; 
\t\t\t\t\t\t\t\tstr += '<div class=\"card-body space-height\">';
\t\t\t\t\t\t\t\t\tstr += '<h5 class=\"card-title text-right\">'+res[i]['mr_work_barcode']+'</h5>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p><b>เบอร์ติดต่อ : </b><a href=\"tel:'+res[i]['mr_emp_mobile']+'\">'+res[i]['mr_emp_mobile']+' </a> / <a href=\"tel:'+res[i]['mr_emp_tel']+'\">'+res[i]['mr_emp_tel']+'</a></p>';
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tstr += '<p id=\"departs\"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
\t\t\t\t\t\t\t\t\tstr += '<hr>';
\t\t\t\t\t\t\t\t\tstr += '<div class=\"row\">'
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-left btn-zone\"><a href=\"#\" class=\"btn btn-warning\" onclick=\"updateNoReceiveById('+res[i]['mr_work_main_id']+');\"><b>ไม่พบเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\t/* str += '<div class=\"col-4 text-right btn-zone\"><a href=\"#\" class=\"btn btn-danger right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );\"><b>ยกเลิก</b></a></div>' */
\t\t\t\t\t\t\t\t\tstr += '<div class=\"col-6 text-right btn-zone\"><a href=\"#\" class=\"btn btn-primary right\" onclick=\"updateReceiveById(' + res[i]['mr_work_main_id'] + ', 2 );\"><b>รับเอกสาร</b></a></div>'
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t\tstr += '</div>';
\t\t\t\t\t\t}
\t
\t
\t\t\t\t}
\t\t\t}else{
\t\t\t\t\$('#btn_save_all').hide();
\t\t\t\tstr = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                 
\t\t\t}
            \$('#data_list').html(str);
            },
            complete: function() {
                \$('#img_loading').hide();
                \$('#data_list').show();
            }
        });
    });

    \$('#btn_save_all').on('click', function() {
\t\tif(confirm(\"ยืนยันการรับงานทั้งหมด!\")) {
            \$.ajax({
                url: './ajax/ajax_updateReceiveAll.php',
                type: 'POST',
                data: {
                    data: JSON.stringify(receive_list)
                },
                success: function(res){
                    if(res == \"success\") {
                        location.reload();
                    }
                }
            });
        }
    });
{% endblock %}

{% block javaScript %}
\t function goPageSend()
    {
\t\twindow.location.href = 'send_list.php'; 
\t}
\t
    function updateReceiveById(id, work_status)
    {
        if( work_status == 1 ){
\t\t\tif(confirm(\"ยืนยันการลบงาน!\")) {
\t\t\t\t//console.log('id :'+ id + ' status :' + work_status);
                \$.ajax({
                    url: './ajax/ajax_updateReceiveById.php',
                    type: 'POST',
                    data: {
                        id: id,
                        work_status: work_status
                    },
                    success: function(res) {
                        if(res == \"success\") {
                            location.reload();
                        }
                    }
                })
\t\t\t\talert('ลบงานเรียบร้อยแล้ว');
\t\t\t\t 
\t\t\t\t
\t\t\t}
        }else{
             \$.ajax({
                    url: './ajax/ajax_updateReceiveById.php',
                    type: 'POST',
                    data: {
                        id: id,
                        work_status: work_status
                    },
                    success: function(res) {
                        if(res == \"success\") {
                            location.reload();
                        }
                    }
                })
        }

        
        
    }
\t
\tfunction updateNoReceiveById(id) {
        \$.ajax({
            url: './ajax/ajax_updateNoReceiveById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == \"success\") {
                    location.reload();
                }
            }
        })
    }
{% endblock %}

{% block Content %}
    <div id='img_loading'><img src=\"../themes/images/loading.gif\" id='pic_loading'></div>
    <div class=\"search_list\">
        <form>
            <div class=\"form-group\">
            <input type=\"text\" class=\"form-control\" id=\"txt_search\" placeholder=\"ค้นหา\">
            </div>
        </form>
    </div>
    <div class=\"text-center\">
        <label>จำนวนเอกสาร</label>
        
            <span id=\"badge-show\" class=\"badge badge-secondary badge-pill badge-dark\"></span>

         <label>ฉบับ</label>
    </div>
\t<button type=\"button\" class=\"btn btn-primary btn-block btn-lg\" id=\"btn_save_all\">รับเอกสารทั้งหมด</button>
\t<div id=\"data_list\">
\t</div>
\t<div class=\"fixed-bottom text-right\">
\t\t<button onclick=\"goPageSend();\" type=\"button\" class=\"btn btn-secondary btn-lg btn-block\">ส่งเอกสาร</button>
\t</div>


 </div>
{% endblock %}


{% block debug %} 

\t{% if debug != '' %}
\t\t<pre>{{ debug }}</pre>
\t{% endif %}

{% endblock %}

", "messenger/receive_list.tpl", "C:\\xampp\\htdocs\\mailroom_tmb\\web\\templates\\messenger\\receive_list.tpl");
    }
}

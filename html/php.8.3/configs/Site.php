<?php

return array(
  'rootUrl'         => '',
  'timezone'        => 'Asia/Bangkok',
	'uploadProposalDir'		=> 'document',
	'download' => 'download',
	'uploadSoftFileDir' => 'file',
	'downloads'=>'downloads',
	'etc' =>'etc',
	'ISO' =>'ISO',
	'office' =>'office',
	// 'serverPath' => 'https://www.pivot-services.com/mailroom_tmb/',
	'serverPath'=> '../',	
	
);


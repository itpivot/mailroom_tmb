<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$userDao 			= new Dao_User();

$usrID = $auth->getUser();

$getDiff = $userDao->getDiffDatePassword(intval($usrID));
$re['diffdate']	=	$getDiff['diffdate'];
$re['usrID']	= 	urlencode(base64_encode($usrID));
echo json_encode($re);
?>
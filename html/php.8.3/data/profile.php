<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'nocsrf.php';
require_once 'Dao/Send_work.php';
/* Check authentication */
$auth = new Pivot_Auth();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

//exit;

$req 				= new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();

$send_workDao		= new Dao_Send_work();



//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
$department_data = $departmentDao->select(
		"SELECT * FROM  `mr_department` 
				WHERE  `mr_department_name` NOT LIKE  'สาขา%'
				ORDER BY  `mr_department`.`mr_department_code` ASC 
				");
//$floor_data = $floorDao->fetchAll();
$floor_data  = $send_workDao->select('SELECT f.* FROM `mr_floor` f
left join `mr_building` as b on(b.mr_building_id = f.mr_building_id)
where b.mr_branch_id = 1 ORDER BY cast(f.floor_level as unsigned),name ASC');


$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuseridProfile($user_id);

$emp_id = $user_data['mr_emp_id'];

// echo "<pre>".print_r($user_data,true)."</pre>";
// $sql = "SELECT * FROM `mr_contact` WHERE `emp_code` LIKE '".$user_data['mr_emp_code']."'";
$params 	 	= array();
$contact 		= new Pivot_Dao('mr_contact');
$contact_db 	= $contact->getDb();

$sql = 
	"
		SELECT c.* FROM mr_contact c
				left join mr_emp e on(c.emp_code =e.mr_emp_code)
		WHERE c.department_code in (
		  select 
			department_code
		  from mr_contact 
		  where emp_code LIKE ?
		  and department_code != ''
		  group by department_code
		) and c.acctive = 1 
		and e.mr_emp_code is not null
		 or c.emp_code LIKE ?
";

// prepare statement SQL
$stmt = new Zend_Db_Statement_Pdo($contact_db, $sql);
	 
array_push($params, trim($user_data['mr_emp_code']));
array_push($params, trim($user_data['mr_emp_code']));

$con_data = array();
if($user_data['mr_emp_code'] != ''){
	// $con_data = $employeeDao->select($sql);
	// Execute Query
	$stmt->execute($params);
	$con_data = $stmt->fetchAll();
}


$template = Pivot_Template::factory('data/profile.tpl');
$template->display(array(
	//'debug' => print_r($user_data,true),
	//'userRoles' 		=> $userRoles,
	'con_data' 			=> $con_data,
	//'success' 			=> $success,
	'department_data' 	=> $department_data,
	//'userRoles' 		=> $userRoles,
	//'users' 			=> $users,
	'user_data' 		=> $user_data,
	'role_id' 			=> $auth->getRole(),
	'roles' 			=> Dao_UserRole::getAllRoles(),
	'userID' 			=> urlencode(base64_encode($user_id)),
	'empID' 			=> $emp_id,
	'csrf' 				=>  NoCSRF::generate( 'csrf_token'),
	'floor_data' 		=> $floor_data
));
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
ini_set('memory_limit', '-1');

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
                                                                            
$data_search['barcode']  		=  $req->get('barcode');                           
$data_search['receiver']  		=  $req->get('receiver');                         
$data_search['sender']  		=  $req->get('sender');                             
$data_search['start_date']  	=  $req->get('start_date');                     
$data_search['end_date']  		=  $req->get('end_date');                                 
$data_search['status']  		=  $req->get('status');      
                           
		

$report = $work_inoutDao->searchMailroom( $data_search );
foreach( $report as $num => $value ){
	
	
	$report[$num]['con_log'] = $value['con_log'];
	$report[$num]['no'] = $num +1;
	$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
	$report[$num]['name_send'] = $value['mr_emp_name']." ".$value['mr_emp_lastname'];
	
	$txt =  "";
	if($value['mr_type_work_id']!=2){
		$txt .=  '<a class="btn-sm btn btn-outline-info" href="edit_floor_send.php?id='.$value['mr_work_inout_id'].'" target="_blank"><b>'.$value['depart_floor_receive'].'</b></a>';
	}else{
		$txt .=  '<a class="btn-sm btn btn-outline-info" href="edit_floor_send.php?id='.$value['mr_work_inout_id'].'" target="_blank"
		data-toggle="tooltip" data-placement="right" title="แก้ไขชั้นผู้รับ"><b>
		<i class="material-icons">
		edit
		</i></b></a>';
	}
	$report[$num]['depart_floor_receive'] = $txt;
	
	$txt_barcode = "";
	$txt_barcode .=  '<a href="detail_receive.php?barcode='.$value['mr_work_barcode'].'" target="_blank"><b>'.$value['mr_work_barcode'].'</b></a>';
	if ( $value['mr_status_id'] == 5 ) {
		$report[$num]['mr_work_barcode'] = $txt_barcode;
	}else{
		$report[$num]['mr_work_barcode'] = $value['mr_work_barcode'];
	}
	
	
	$txtdropdown='<div class="btn-group" role="group" aria-label="Basic example">';
	$txtdropdown2.='</div>';
	if( $value['mr_status_id'] != 15 ){
		$txtdropdown.='
		<a class="btn btn-sm btn-outline-success" href="success_work_by_mailroom.php?id='.$value['mr_work_main_id'].'" target="_blank"
		data-toggle="tooltip" data-placement="right" title="สำเร็จนอกระบบ"
		><b><i class="material-icons">
		done_all
		</i></b></a>';
	}
				
	
	
	
	
	
	$txt_cancle =  "";
	$txt_cancle .=  '<a class="btn btn-sm btn-outline-warning" href="cancle_work_by_mailroom.php?id='.$value['mr_work_main_id'].'" target="_blank" 
	data-toggle="tooltip" data-placement="right" title="ยกเลิกการส่ง"
	><b>
	<i class="material-icons">report_problem</i></b></a>';
	
	$txt_success =  "";
	$txt_success .=  '<a class="btn btn-outline-success" href="success_work_by_mailroom.php?id='.$value['mr_work_main_id'].'" target="_blank"><b>สำเร็จนอกระบบ</b></a>';
	
	
	$report[$num]['cancle']= $txtdropdown;
	$report[$num]['cancle'] .= $txt_cancle;
	if ( $value['mr_status_id'] == 1 || $value['mr_status_id'] == 2 || $value['mr_status_id'] == 3 ) {
		//$report[$num]['cancle'] .= $txt_cancle;
	}else if ( $value['mr_status_id'] == 4 ) {
		//$report[$num]['cancle'] .= $txt_success;
	}else if ( $value['mr_status_id'] == 5 ) {
		$report[$num]['cancle'] = '';
		$report[$num]['depart_floor_receive'] = '';
	}else if ( $value['mr_status_id'] == 12 ) {
		$report[$num]['cancle'] = '';
		$report[$num]['depart_floor_receive'] = '';
	}else if ( $value['mr_status_id'] == 15 ) {
		$report[$num]['cancle'] = '';
		$report[$num]['depart_floor_receive'] = '';
	}else{
		$report[$num]['cancle'] .= '';
		
	}
	
	$report[$num]['cancle'] .= $txtdropdown2;
	//$report[$num]['cancle'] = '';
	
	
}

$arr['data'] = $report;
// echo '<pre>'.print_r($report,true).'</pre>';
// exit;
// echo json_encode($data_search);
echo json_encode($arr);
exit();
 ?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
	exit();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();
$user_id		= $auth->getUser();
//$user_data 		= $userDao->getEmpDataByuserid($user_id);
$data 			= $work_mainDao->usergetData_receive_emp($user_id);
// echo print_r($data,true) ;
$newdata = array();
$no 	 = 0;

$st_arr = array(3,4,9,10,11,13,14);

// wp.mr_cus_name as wp_cus_name,
// wp.mr_cus_lname as wp_cus_lname,
// wp.num_doc as wp_num_doc,
// wbh.mr_cus_name as wbh_cus_name,
// wbh.mr_cus_lname as wbh_cus_lname,
// wbh.work_barcode_re as work_barcode_re


if(count($data) > 0) {
	foreach($data as $i => $val_i){
		//3,4,9,10,11,13,14
		if(in_array($val_i['mr_status_id'],$st_arr)){
			$txt_disabled='';
		}else{
			$txt_disabled='disabled';
		}
	
		$newdata[$i]['no'] 	= ($no+1);
		$newdata[$i]['send_work_no'] 			= $val_i['mr_work_barcode'];
	
		if($val_i['mr_status_id']==6){
			$newdata[$i]['mr_status_name'] 		= '<p class="text-danger">'.$val_i['mr_status_name'].'</p>';
		}elseif($val_i['mr_status_id']==11){
			$newdata[$i]['mr_status_name'] 		= '<p class="text-success">'.$val_i['mr_status_name'].'</p>';
		}else{
			$newdata[$i]['mr_status_name'] 		= '<p class="text-secondary">'.$val_i['mr_status_name'].'</p>';
		}

		$newdata[$i]['send_name'] 				= $val_i['send_name'].' '.$val_i['send_lname'];
		if($val_i['mr_type_work_id']==5){
			$newdata[$i]['send_name'] 				= $val_i['wp_cus_name'].'  '.$val_i['wp_cus_lname'];
			$newdata[$i]['tnt_tracking_no'] 		= $val_i['wp_num_doc'];
			$newdata[$i]['mr_type_work_name'] 		='<span class="badge bg-warning text-dark">'.$val_i['mr_type_work_name'].'</span>';

		}else if($val_i['mr_type_work_id']==7){
			$newdata[$i]['send_name'] 			= $val_i['wbh_cus_name'].'  '.$val_i['wbh_cus_lname'];
			$newdata[$i]['tnt_tracking_no'] 	= $val_i['work_barcode_re'];
			$newdata[$i]['mr_type_work_name'] 	='<span class="badge bg-info text-dark">'.$val_i['mr_type_work_name'].'</span>';
		}else{
			$newdata[$i]['send_name'] 			= $val_i['re_name'].'  '.$val_i['re_lname'];
			$newdata[$i]['tnt_tracking_no'] 	= $val_i['tnt_tracking_no'];
			$newdata[$i]['mr_type_work_name'] 	='<span class="badge bg-secondary text-dark">'.$val_i['mr_type_work_name'].'</span>';
		}

		$newdata[$i]['res_name'] 			= $val_i['re_name'].'  '.$val_i['re_lname'];
		$newdata[$i]['cre_date'] 			= $val_i['sys_timestamp'];
		if(isset($val_i['log_sys_timestamp'])){
			$newdata[$i]['update_date'] 		= ($val_i['log_sys_timestamp']!='')?$val_i['log_sys_timestamp']:$val_i['sys_timestamp'];
		}else{
			$newdata[$i]['update_date'] 		= $val_i['sys_timestamp'];
		}

		if($txt_disabled == ''){
			$newdata[$i]['action'] 		= '<input type="checkbox" name="vehicle1" value="'.$val_i['mr_work_main_id'].'" '.$txt_disabled.'>';
			$newdata[$i]['btn'] 	= '<button type="button" class="btn btn-success" onclick="click_resave(\''.$val_i['mr_work_barcode'].'\');">  รับ </button>
			<a 
	data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="work_info_employee.php?id='.urlencode(base64_encode($val_i['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
	dehaze
	</span></a>';
	//ทำถึงตรงนี้
		}else{
			$newdata[$i]['action'] 		= '' ;
			$newdata[$i]['btn'] 		= '<button type="button" class="btn btn-secondary" disabled>  รับ </button>
			<a 
	data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="work_info_employee.php?id='.urlencode(base64_encode($val_i['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
	dehaze
	</span></a>';
		}
			/* if($val_i['mr_status_id'] == 5 or $val_i['mr_status_id'] == 14  ){
		$newdata[$i]['action'] 				= '<i class="text-success material-icons">check_circle_outline</i>';
		}else{
			$newdata[$i]['action'] 				= '';
		} */
		
		$no++;
	}
}

echo json_encode(array_values($newdata));
 ?>
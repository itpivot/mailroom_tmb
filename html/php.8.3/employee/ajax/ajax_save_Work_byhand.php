<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'nocsrf.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
   echo "Failed";
   exit;
} else {

	
try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
 
   exit();
}


$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 		= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 			= new Dao_Sub_district();
$user_id			= $auth->getUser();


// $barcode = '';
// for($j=0;$j<=10;$j++){
// 	$barcode 							= $work_mainDao->landom_Barcode();
// 	$last_barcode 						= $work_mainDao->checkBarcode($barcode);
// 	if($last_barcode['mr_work_barcode'] == ''){
// 		break;
// 	}
// }
// $barcodeok	 		= trim($barcode);






try{

    $save_main['mr_work_date_sent']			= $req->get('send_date');
    //$save_main['mr_work_barcode']			= $barcodeok;
    $save_main['mr_work_remark']			= $req->get('work_remark');
    $save_main['mr_type_work_id']			= 4;
    $save_main['mr_floor_id']				= $req->get('floor_id_send');
    $save_main['mr_branch_id']				= $req->get('mr_branch_id');
	$save_main['mr_status_id']				= 1;
    $save_main['mr_user_id']				= $user_id;
    $save_main['mr_round_id']				= $req->get('round');
    $save_main['mr_topic']					= $req->get('topic');
    $save_main['quty']						= $req->get('quty');

	$mr_work_main_id 							= $work_mainDao->save($save_main);
	if(strlen($mr_work_main_id)<=1){
		$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."000000".$mr_work_main_id;
	}else if(strlen($mr_work_main_id)<=2){
		$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."00000".$mr_work_main_id;
	}else if(strlen($mr_work_main_id)<=3){
		$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."0000".$mr_work_main_id;
	}else if(strlen($mr_work_main_id)<=4){
		$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."000".$mr_work_main_id;
	}else if(strlen($mr_work_main_id)<=5){
		$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."00".$mr_work_main_id;
	}else if(strlen($mr_work_main_id)<=6){
		$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."0".$workmr_work_main_id_main_id;
	}else if(strlen($mr_work_main_id)<=7){
		$Nbarcode['mr_work_barcode'] = "BHO".date("dmy").$mr_work_main_id;
	}else{
			$num_run 			= substr($mr_work_main_id, -7); 
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy").$num_run;
	} 
	
	$barcodeok = $Nbarcode['mr_work_barcode'];
	$work_mainDao->save($Nbarcode,$mr_work_main_id);

////////////////////////////////////////////////////////////
    $save_byhand['mr_work_main_id']			= $mr_work_main_id;
    $save_byhand['mr_work_byhand_type_id']	= $req->get('type_send');
    $save_byhand['mr_send_emp_id']			= $req->get('emp_id_send'); 
    $save_byhand['mr_send_emp_detail']		= $req->get('emp_send_data');
    $save_byhand['mr_cus_name']				= $req->get('name_re')." ".$req->get('lname_re');
    $save_byhand['mr_cus_tel']				= $req->get('tel_re');
    $save_byhand['mr_address']				= $req->get('address_re');
    $save_byhand['mr_cost_id']				= $req->get('mr_cost');
	if($req->get('post_code_re')!= ''){
    	$save_byhand['mr_post_code']			= $req->get('post_code_re');
	}
	if($req->get('dep_id_send')!= ''){
    	$save_byhand['mr_send_dep_id']			= $req->get('dep_id_send');
	}

	$sub_districts_code_re											=  $req->get('sub_districts_code_re');
	$districts_code_re												=  $req->get('districts_code_re');
	$provinces_code_re												=  $req->get('provinces_code_re');
		if(!empty($sub_districts_code_re)) {
			$locate = $sub_districtDao->getLocation($sub_districts_code_re, $districts_code_re, $provinces_code_re);
			$save_byhand['mr_sub_district_id'] 					= (!empty($locate['mr_sub_districts_id']) ? (int)$locate['mr_sub_districts_id'] : NULL);;
			$save_byhand['mr_district_id'] 						= (!empty($locate['mr_districts_id']) ? (int)$locate['mr_districts_id'] : NULL);
			$save_byhand['mr_province_id'] 						= (!empty($locate['mr_provinces_id']) ? (int)$locate['mr_provinces_id'] : NULL);;
		}
	
	$work_inout_id	= $work_byhandDao->save($save_byhand);
	$save_log['mr_user_id'] 									= $user_id;
	$save_log['mr_work_main_id'] 								= $mr_work_main_id;
	$save_log['mr_status_id'] 									= 1;
	$save_log['remark'] 										= "สร้างราายการ BY HAND";
	$work_logDao->save($save_log);



	echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
		

}
catch ( Exception $e )
{
	echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}	
		

}



 ?>
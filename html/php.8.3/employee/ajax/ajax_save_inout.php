<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'nocsrf.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
   echo "Failed";
} else {


$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$userDao 			= new Dao_User();




//echo "<pre>".print_r($user_data,true)."</pre>";
//exit();

$now = date('Y-m-d');

$user_id 			= $req->get('user_id');
$work_order_id 		= $req->get('work_order_id');

if(!empty($work_order_id)) {
	if(preg_match('/<\/?[^>]+(>|$)/', $work_order_id)) {
		echo "Failed";
		exit();
	}	
}

if(!empty($user_id)) {
	if(preg_match('/<\/?[^>]+(>|$)/', $user_id)) {
		echo "Failed";
		exit();
	}
}



$user_data = $userDao->getEmpDataByuseridProfile($user_id);

$barcode = '';
// for($j=0;$j<=10;$j++){
// 	$barcode 							= $work_mainDao->landom_Barcode();
// 	$last_barcode 						= $work_mainDao->checkBarcode($barcode);
// 	if($last_barcode['mr_work_barcode'] == ''){
// 		break;
// 	}
// }
$barcodeok	 		= trim($barcode);


$floor_id = $req->get('floor_id');
$floor_send = $req->get('floor_send');

if(!empty($floor_id)) {
	if(preg_match('/<\/?[^>]+(>|$)/', $floor_id)) {
		echo "Failed";
		exit();
	}	
}
try
 {
     // Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
     NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
     // form parsing, DB inserts, etc.
     // ...
     $result = 'CSRF check passed. Form parsed.';
 }
 catch ( Exception $e )
 {
     // CSRF attack detected
   $result = $e->getMessage() . ' Form ignored.';
	echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณาลองใหม่' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
 }

$time_round = date("H:i:s");
$round = $userDao->GetRound( $time_round );

if(!empty($round)){
	$save_data_main['mr_round_id'] 								= $round;
}if(!empty($barcodeok)){
	$save_data_main['mr_work_barcode'] 							= $barcodeok;
}if(!empty($now)){
	$save_data_main['mr_work_date_sent'] 						= $now;
}

$remark = $req->get('remark');
if(!empty($remark)){
	$save_data_main['mr_work_remark'] 							= $remark;
}

$user_id = $req->get('user_id');
if(!empty($user_id)){
	$save_data_main['mr_user_id'] 								= $user_id;
}

$topic = $req->get('topic');
if(!empty($topic)){
	$save_data_main['mr_topic'] 								= $topic;
}

$qty = $req->get('qty');
if(!empty($qty)){
	$save_data_main['quty'] 								= $qty;
}

if(!empty($user_data['mr_floor_id_emp'])){
	$save_data_main['mr_floor_id'] 								= $user_data['mr_floor_id_emp'];
}

	$save_data_main['mr_type_work_id'] 							= 1;
	$save_data_main['mr_status_id']								= 1;

//echo "<pre>".print_r($floor_id,true)."</pre>";



	$work_main_id = $work_mainDao->save($save_data_main);
	if(strlen($work_main_id)<=1){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."000".$work_main_id;
	}else if(strlen($work_main_id)<=2){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."00".$work_main_id;
	}else if(strlen($work_main_id)<=3){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."0".$work_main_id;
	}else if(strlen($work_main_id)<=4){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."".$work_main_id;
	}else if(strlen($work_main_id)<=5){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."".$work_main_id;
	}else{
			$num_run 			= substr($work_main_id, -6); 
			$Nbarcode['mr_work_barcode'] = "TM".date("dmy").$num_run;
	} 
	$barcodeok = $Nbarcode['mr_work_barcode'];
	$work_mainDao->save($Nbarcode,$work_main_id);
	
	//echo substr("Hello world",-4)."<br>";

	//$barcode = 'TM'.date('dmy');
// echo $Nbarcode['mr_work_barcode']  ;
// exit;
if(intval($work_main_id)){
	if ( $floor_send != 0 ){
		if(!empty($floor_send)){
			$save_data_inout['mr_floor_id'] 						= $floor_send;
		}
	}else{
		if(!empty($floor_id)){
			$save_data_inout['mr_floor_id'] 						= $floor_id;
		}
	}
	
	$emp_id = $req->get('emp_id');
	if(!empty($emp_id)){
		$save_data_inout['mr_emp_id'] 								= $emp_id;
	}if(!empty($work_main_id)){
		$save_data_inout['mr_work_main_id'] 						= $work_main_id;
	}

	$work_inoutDao->save($save_data_inout);
}

	$save_log['mr_user_id'] 									= $req->get('user_id');;
	$save_log['mr_status_id'] 									= 1;
	$save_log['mr_work_main_id'] 								= $work_main_id;

	$work_logDao->save($save_log);
	echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
	//echo $barcodeok;
}



 ?>
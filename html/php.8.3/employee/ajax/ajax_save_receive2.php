<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/User.php';
require_once 'PHPMailer.php';
require_once 'Dao/Confirm_Log.php';
error_reporting(E_ALL & ~E_NOTICE);

$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
$work_logDao 	= new Dao_Work_log();
$confirm_log_Dao	= new Dao_Confirm_Log();
$sendmail 		= new SendMail();

if (!$auth->isAuth()) {
	$st = array();
	$st['st'] = 'error'; 
	$st['msg'] = 'Access Denied.'; 
	echo json_encode($st);
	exit;
}

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();
$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
//$code_branch_sender = $user_data['mr_branch_id'];
$mr_emp_id = $user_data['mr_emp_id'];


$all_id = $req->get('all_id');
$barcode = $req->get('barcode');

$time_start = microtime(true);

$sql_select = "
	SELECT 
		m.mr_work_main_id,				
		m.mr_status_id,			
		m.mr_type_work_id,
		m.mr_work_barcode			
	FROM mr_work_main  m 
	left join mr_work_inout io on(io.mr_work_main_id = m.mr_work_main_id)
		where m.mr_status_id in(3,4,10,11,13,14)
		and io.mr_emp_id = ".$mr_emp_id."
";

$params 	 	= array();
$work_main 		= new Pivot_Dao('mr_work_main');
$work_main_db 	= $work_main->getDb();
$stmt = new Zend_Db_Statement_Pdo($work_main_db, $sql_select);

array_push($params, trim($barcode));
header('Content-Type: text/html; charset=utf-8');
$stmt->execute($params);
$data_n = $stmt->fetchAll();
$barcodedata = array();
if(empty($data_n)){
	$sql_select = "
	SELECT 
		m.mr_work_main_id,				
		m.mr_status_id,			
		m.mr_type_work_id			
	FROM mr_work_main  m 
		where m.mr_work_barcode like ?
		and m.mr_status_id in(3,4,9,10,11,13,14)
	";
	$params 	 	= array();
	$work_main 		= new Pivot_Dao('mr_work_main');
	$work_main_db 	= $work_main->getDb();
	$stmt = new Zend_Db_Statement_Pdo($work_main_db, $sql_select);
	array_push($params, trim($barcode));
	$stmt->execute($params);
	$data = $stmt->fetchAll();
	echo "ว่าง 1 <br>";
}else{
	echo "ไม่ว่าง 1 <br>";
	foreach($data_n as $val){
		$barcodedata[$val['mr_work_barcode']] =  $val;
	}	
	if(isset($barcodedata[$barcode])) {
		$data[0] = $barcodedata[trim($barcode)];
		echo "ไม่ว่าง 2 <br>";
	}else{
		$sql_select = "
		SELECT 
			m.mr_work_main_id,				
			m.mr_status_id,			
			m.mr_type_work_id			
		FROM mr_work_main  m 
			where m.mr_work_barcode like ?
			and m.mr_status_id in(3,4,9,10,11,13,14)
		";
		$params 	 	= array();
		$work_main 		= new Pivot_Dao('mr_work_main');
		$work_main_db 	= $work_main->getDb();
		$stmt = new Zend_Db_Statement_Pdo($work_main_db, $sql_select);
		array_push($params, trim($barcode));
		$stmt->execute($params);
		$data = $stmt->fetchAll();
		echo "ว่าง 2 <br>";
	}
}




$time_end = microtime(true);
$time = $time_end - $time_start;

echo "ime: $time seconds\n <br>";
// $data = $send_workDao->select($sql_select);
echo count($data_n);
echo "<pre>".print_r($data,true);
exit;
 if(!empty($data)){
	 if($data[0]['mr_status_id']==5){
		$st['st'] = 'error'; 
		$st['msg'] = 'เอกสารถูกรับแล้ว'; 
	 }else{
		$main_update['mr_status_id']=5;
		
		$sqlupdate1 = 'update mr_work_main 
					set mr_status_id = 5  ,mr_work_date_success = "'.date("Y-m-d").'"
					WHERE mr_work_main_id in('.$data[0]['mr_work_main_id'].');';
					
		$sqlupdate2 = 'update mr_work_inout 
					set mr_user_id = '.$user_id.'
					WHERE mr_work_main_id in('.$data[0]['mr_work_main_id'].');';
					
		$ree = $send_workDao->query2($sqlupdate1,$sqlupdate2);
		//$ss = $work_mainDao->save($main_update,$data[0]['mr_work_main_id']);
		
		$save_log['mr_user_id'] 									= $auth->getUser();
		$save_log['mr_status_id'] 									= 5;
		$save_log['mr_work_main_id'] 								= $data[0]['mr_work_main_id'];
		if($data[0]['mr_work_main_id'] != ''){
			$work_logDao->save($save_log);
		}
			
			$cfLogs['sys_timestamp'] 	= date('Y-m-d H:i:s');
			if(!empty($data[0]['mr_work_main_id'])) {
				$cfLogs['mr_work_main_id'] 	= intval($data[0]['mr_work_main_id']);
			}
			if(!empty($mr_emp_id)) {
				$cfLogs['mr_emp_id'] 		= $mr_emp_id;
			}
			$cfLogs['mr_status'] 		= "Success";
			$cfLogs['descriptions'] 	= "ReceiveSuccess";
			if($data[0]['mr_work_main_id'] != ''){
				$cf = $confirm_log_Dao->save($cfLogs);
			}
		//echo print_r($save_log,true);
		$st['st'] = 'success';
		$st['msg'] = 'สำเร็จ';
		$st['mr_work_main_id'] = $data[0]['mr_work_main_id'];
		
		
		//gg
		
		//$datasByEmail = $work_mainDao->getDatasByEmail(intval($data[0]['mr_work_main_id']));
		//	$barcode			=  $datasByEmail['barcode'];
		//	$send_name			=  $datasByEmail['send_name'];
		//	$send_lastname		=  $datasByEmail['send_lastname'];
		//	$send_email			=  $datasByEmail['send_email'];
		//	$reciever_name		=  $datasByEmail['reciever_name'];
		//	$reciever_lastname  =  $datasByEmail['reciever_lastname'];
		//
		//	$Subjectmail = "รายการส่งเอกสารเลขที่ $barcode ผู้รับได้รับเอกสารเรียบร้อยแล้ว ";
		//	$body = "";
		//	$body .= "เรียน คุณ <b>$send_name $send_lastname </b><br><br>";
		//	$body .= "รายการส่งเอกสารเลขที่  <b> $barcode  </b> ผู้รับ <b>$reciever_name $reciever_lastname </b> $result_recheck<br>";
		//	$body .= "ได้รับเอกสารเรียบร้อยแล้วค่ะ<br>";
		//	$body .= "<br>";
		//	$body .= "Pivot MailRoom Auto-Response Message <br>";
		//	$body .= "(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";
		//	$result_mail = $sendmail->mailNotice($body,$Subjectmail, $send_email);
		//
		//
		//
		//
	 }
	 
 }else{
	  $st['st'] = 'error'; 
	  $st['msg'] = ' ไม่พบข้อมูลงานที่กำลังนำส่ง'; 
 }
echo json_encode($st);
exit;
















//$re = $send_workDao->query2($sql1,$sql2);
echo $barcode ;
exit;
$main_id = explode(",", $all_id);
$sqllog="INSERT INTO mr_work_log (`mr_work_main_id`, `mr_user_id`, `mr_status_id`) VALUES ";
$loop = 1;
foreach($main_id as $i => $data_i){
	if( $i>= (count($main_id)-1)){
		$sqllog.="(".$data_i.", ".$user_id.", '14');";
	}else{
		if($loop>= 1000){
			$loop = 0;
			$sqllog.="(".$data_i.", ".$user_id.", '14');";
			$sqllog.="INSERT INTO mr_work_log (`mr_work_main_id`, `mr_user_id`, `mr_status_id`) VALUES ";
		}else{
			$sqllog.="(".$data_i.", ".$user_id.", '14'),";
		}
	}
	$loop++;
}

$sqlupdate_mail = 'update mr_work_main 
					set mr_status_id = 14
					WHERE mr_work_main_id in('.$all_id.');';
$sqlupdate_mail .= 'update mr_work_inout 
					set mr_user_id = '.$user_id.'
					WHERE mr_work_main_id in('.$all_id.');';
$savedata[3]=$sqllog;
$savedata[4]=$sqlupdate_mail;
$re= array();

$ree = $send_workDao->query2($sqllog,$sqlupdate_mail);
if($ree){
	$re['st'] = 'success';
}else{
	$re['st'] = 'ereeor';
}
echo json_encode($re);
 ?>
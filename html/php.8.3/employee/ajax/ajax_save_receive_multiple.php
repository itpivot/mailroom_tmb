<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/User.php';
require_once 'Dao/Confirm_Log.php';
require_once 'PHPMailer.php';
error_reporting(E_ALL & ~E_NOTICE);

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$work_mainDao 		= new Dao_Work_main();
$send_workDao 		= new Dao_Send_work();
$work_logDao 		= new Dao_Work_log();
$confirm_log_Dao	= new Dao_Confirm_Log();
$sendmail 			= new SendMail();

if (!$auth->isAuth()) {
	$st = array();
	$st['st'] = 'error'; 
	$st['msg'] = 'Access Denied.'; 
	echo json_encode($st);
	exit();
}

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();
$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
//$code_branch_sender = $user_data['mr_branch_id'];
$mr_emp_id 		= $user_data['mr_emp_id'];


$maim_id = $req->get('maim_id');

if(preg_match('/<\/?[^>]+(>|$)/', $maim_id)) {
	$st = array();
	$st['st'] = 'error'; 
	$st['msg'] = 'รูปแบบข้อมูลไม่ถูกต้อง'; 
	echo json_encode($st);
	exit();
}


$st['st'] = 'error'; 
$st['msg'] = ' ไม่พบข้อมูลงานที่กำลังนำส่ง'; 
if(!empty($maim_id)) {
	$maim_id_condition = array(); // condition: generate ?,?,?
	$maim_id_params = explode(',', $maim_id);
	$maim_id_condition = str_repeat('?,', count($maim_id_params) - 1) . '?'; // example: ?,?,?

}
$params 	 	= array();
$sql_select = "
	SELECT 
		m.mr_work_main_id,				
		m.mr_status_id,			
		m.mr_type_work_id			
	FROM mr_work_main  m 
		where mr_status_id in(3,4,9,10,11,13,14)
";


if($maim_id != ''){
	$sql_select .= " and  m.mr_work_main_id in(".$maim_id_condition.") ";
	$params = array_merge($params,$maim_id_params);
}


$work_main 		= new Pivot_Dao('mr_work_main');
$work_main_db 	= $work_main->getDb();
$stmt = new Zend_Db_Statement_Pdo($work_main_db, $sql_select);
$stmt->execute($params);
$order_data = $stmt->fetchAll();

// echo "<pre>".print_r($sql_select,true)."</pre>";
// echo "<pre>".print_r($maim_id_condition,true)."</pre>";
// echo "<pre>".print_r($maim_id_params,true)."</pre>";
// echo "<pre>".print_r($params,true)."</pre>";
// echo "<pre>".print_r($maim_id,true)."</pre>";
// echo "<pre>".print_r($order_data,true)."</pre>";


// exit;
// $order_data = $send_workDao->select($sql_select);
$_success 	= 0;
$_error 	= 0;

if(count($order_data) > 0) {
	foreach($order_data as $key => $data){
		if(!empty($data)){
			if($data['mr_status_id']==5){
				$st['st'] = 'error'; 
				$st['msg'] = 'เอกสารถูกรับแล้ว'; 
				$_error++;
			}else{
				$main_update['mr_status_id']=5;
				
				$sqlupdate1 = 'update mr_work_main 
							set mr_status_id = 5 ,mr_work_date_success = "'.date("Y-m-d").'"
	
							WHERE mr_work_main_id in('.$data['mr_work_main_id'].');';
							
				$sqlupdate2 = 'update mr_work_inout 
							set mr_user_id = '.$user_id.'
							WHERE mr_work_main_id in('.$data['mr_work_main_id'].');';
							
				$ree = $send_workDao->query2($sqlupdate1, $sqlupdate2);
				//$ss = $work_mainDao->save($main_update,$data[0]['mr_work_main_id']);
				
				$save_log['mr_user_id'] 									= $auth->getUser();
				$save_log['mr_status_id'] 									= 5;
				$save_log['mr_work_main_id'] 								= $data['mr_work_main_id'];
				if($data['mr_work_main_id'] != ''){
					$work_logDao->save($save_log);
				}
	
	
				$cfLogs['sys_timestamp'] 	= date('Y-m-d H:i:s');
				$cfLogs['mr_work_main_id'] 	= intval($data['mr_work_main_id']);
				$cfLogs['mr_emp_id'] 		= $mr_emp_id;
				$cfLogs['mr_status'] 		= "Success";
				$cfLogs['descriptions'] 	= "ReceiveSuccess";
				if($data['mr_work_main_id'] != ''){
					$cf = $confirm_log_Dao->save($cfLogs);
				}
	
	
	
				//echo print_r($save_log,true);
				$st['st'] = 'success';
				$st['msg'] = 'สำเร็จ';
				$_success ++;
				
				
				
				
				//$datasByEmail = $work_mainDao->getDatasByEmail(intval($data['mr_work_main_id']));
				//	$barcode			=  $datasByEmail['barcode'];
				//	$send_name			=  $datasByEmail['send_name'];
				//	$send_lastname		=  $datasByEmail['send_lastname'];
				//	$send_email			=  $datasByEmail['send_email'];
				//	$reciever_name		=  $datasByEmail['reciever_name'];
				//	$reciever_lastname  =  $datasByEmail['reciever_lastname'];
				//
				//	$Subjectmail = "รายการส่งเอกสารเลขที่ $barcode ผู้รับได้รับเอกสารเรียบร้อยแล้ว ";
				//	$body = "";
				//	$body .= "เรียน คุณ <b>$send_name $send_lastname </b><br><br>";
				//	$body .= "รายการส่งเอกสารเลขที่  <b> $barcode  </b> ผู้รับ <b>$reciever_name $reciever_lastname </b> $result_recheck<br>";
				//	$body .= "ได้รับเอกสารเรียบร้อยแล้วค่ะ<br>";
				//	$body .= "<br>";
				//	$body .= "Pivot MailRoom Auto-Response Message <br>";
				//	$body .= "(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";
				//	$result_mail = $sendmail->mailNotice($body,$Subjectmail, $send_email);
				//
				//
				//
				//
			}
			
		}else{
			$st['st'] = 'error'; 
			$st['msg'] = ' ไม่พบข้อมูลงานที่กำลังนำส่ง'; 
			$_error ++;
		}
	}
}else{
	$st['st'] = 'error'; 
	$st['msg'] = ' ไม่พบข้อมูลงานที่กำลังนำส่ง'; 
	$_error ++;
} 


if($_success > 0){
	$st['st'] = 'success'; 
	$st['msg'] = ' รับสำเร็จ '.$_success.'  รายการ'; 
}else{
	$st['st'] = 'error'; 
	$st['msg'] = ' รับสำเร็จ '.$_success.'  รายการ'; 
}

echo json_encode($st);
exit;
 ?>
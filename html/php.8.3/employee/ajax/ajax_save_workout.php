<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'nocsrf.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 500, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit;
}
else{

	
try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณาลองใหม่' ,'token'=>NoCSRF::generate( 'csrf_token' )));
   exit();
}



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$userDao 			= new Dao_User();



$now = date('Y-m-d');
$work_order_id 		= $req->get('work_order_id');
$user_id 			= $req->get('user_id');
$mr_branch_id 		= $req->get('mr_branch_id');
$branch_send 		= $req->get('branch_send');
$remark 			= $req->get('remark');
$topic 				= $req->get('topic');
$emp_id 			= $req->get('emp_id');
$mr_branch_floor 	= $req->get('mr_branch_floor');
$qty 				= $req->get('qty');

try {
	if(preg_match('/<\/?[^>]+(>|$)/', $user_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	
	if(preg_match('/<\/?[^>]+(>|$)/', $work_order_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	
	
	if(preg_match('/<\/?[^>]+(>|$)/', $remark)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	
	if(preg_match('/<\/?[^>]+(>|$)/', $topic)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	
	if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_floor)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}

	$user_data = $userDao->getEmpDataByuseridProfile($user_id);

	$barcode = '';
	// for($j = 0; $j<=10; $j++){
	// 	$barcode 							= $work_mainDao->landom_Barcode();
	// 	$last_barcode 						= $work_mainDao->checkBarcode($barcode);
	// 	if($last_barcode['mr_work_barcode'] == ''){
	// 		break;
	// 	}
	// }

	
	$barcodeok	 							= trim($barcode);
	$time_round 							= date("H:i:s");
	$round 									= $userDao->GetRound($time_round);
	
	
	if(!empty($round)){
		$save_data_main['mr_round_id'] 			= $round;
	}if(!empty($barcodeok)){
		$save_data_main['mr_work_barcode'] 		= $barcodeok;
	}if(!empty($now)){
		$save_data_main['mr_work_date_sent'] 	= $now;
	}if(!empty($remark)){
		$save_data_main['mr_work_remark'] 		= trim($remark);
	}
	
	if(!empty($user_id)){
		$save_data_main['mr_user_id'] 			= $user_id;
	}if(!empty($topic)){
		$save_data_main['mr_topic'] 			= trim($topic);
	}
	if(!empty($user_data['mr_floor_id_emp'])){
		$save_data_main['mr_floor_id'] 			= $user_data['mr_floor_id_emp'];
	}
	if(!empty($qty)){
		$save_data_main['quty'] 			= $qty;
	}
	
	$save_data_main['mr_type_work_id'] 		= 2;
	$save_data_main['mr_status_id']			= 1;


	
	$work_main_id = $work_mainDao->save($save_data_main);
	if(strlen($work_main_id)<=1){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."000".$work_main_id;
	}else if(strlen($work_main_id)<=2){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."00".$work_main_id;
	}else if(strlen($work_main_id)<=3){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."0".$work_main_id;
	}else if(strlen($work_main_id)<=4){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."".$work_main_id;
	}else if(strlen($work_main_id)<=5){
		$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."".$work_main_id;
	}else{
			$num_run 			= substr($work_main_id, -6); 
			$Nbarcode['mr_work_barcode'] = "TM".date("dmy").$num_run;
	} 

	$barcodeok = $Nbarcode['mr_work_barcode'];
	$save_data_main['mr_work_barcode'] 		= $barcodeok;
	$work_mainDao->save($Nbarcode,$work_main_id);

	
	$save_data_main['work_main_id']= $work_main_id;
	$save_data_main['encode']= urlencode(base64_encode($work_main_id));

	if ( $branch_send != 0 && $branch_send != ''){
		if(!empty($branch_send)){
			$save_data_inout['mr_branch_id'] 						= $branch_send;
		}
	}else{
		if(!empty($mr_branch_id)){
			$save_data_inout['mr_branch_id'] 						= $mr_branch_id;
		}
	}
	
	if(!empty($emp_id)){
		$save_data_inout['mr_emp_id'] 								= $emp_id;
		
	}if(!empty($mr_branch_floor)){
		$save_data_inout['mr_branch_floor'] 						= $mr_branch_floor;
		
	}if(!empty($work_main_id)){
		$save_data_inout['mr_work_main_id'] 						= $work_main_id;
		
	}
	if(!empty($work_main_id)){
		$work_inoutDao->save($save_data_inout);
	}

	$save_log['mr_user_id'] 									= $user_id;
	$save_log['mr_status_id'] 									= 1;
	$save_log['mr_work_main_id'] 								= $work_main_id;

	$work_logDao->save($save_log);
	
	//echo json_encode($save_data_main);
	$array2 = array('status' => 200, 'message' => 'บันทึกข้อมูลสำเร็จ' ,'token'=>NoCSRF::generate( 'csrf_token' ));
	$result = array_merge($save_data_main, $array2);

	echo json_encode($result);

} catch(Exception $e) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}









	

}



 ?>
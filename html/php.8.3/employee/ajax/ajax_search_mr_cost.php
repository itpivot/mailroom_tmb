<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Cost.php';

/* Check authentication */
$auth 					= new Pivot_Auth();
$userdao 				= new Dao_User();
$req 					= new Pivot_Request();
$employeedao 			= new Dao_Employee();
$CostDao                = new Dao_Cost();

$users = $auth->getUser();
$mr_cost = $req->get('mr_cost');
$txt = (isset($_GET['q']))?$_GET['q']:'';
$json = array();

if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
	$txt = "";
}

$cost_data = $CostDao->getcostdata($txt);

	if(count($cost_data) > 0) {
		foreach ($cost_data as $key => $value) {
			$fullname = $value['mr_cost_code']." - ". $value['mr_cost_name'];
			//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
			$json[] = array(
				"id" => $value['mr_cost_id'],
				"text" => $fullname
			);
		}
	}

echo json_encode($json);
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'nocsrf.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	$arr = array();
	$arr['data'] = array();
    echo json_encode(array());
    exit();
};   

$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
   
$user_id= $auth->getUser();
$emp_data = $userDao->getEmpDataByuserid( $user_id );    


$barcode 						= $req->get('barcode');  
$receiver 						= $req->get('receiver');  
$sender 						= $req->get('sender');  
$start_date 					= $req->get('start_date');  
$end_date 						= $req->get('end_date');  
$status 						= $req->get('status');  

  
$data_search['barcode']  		=  trim($barcode);                           
$data_search['receiver']  		=  trim($receiver);                           
$data_search['sender']  		=  trim($sender);     
$data_search['start_date']  	=  trim($start_date);                     
$data_search['end_date']  		=  trim($end_date);                                 
$data_search['status']  		=  $status; 
$data_search['mr_emp_id']  		=  $emp_data['mr_emp_id'];  
$data_search['user_id']  		=  $user_id;  

// echo print_r($data_search);
// exit();
//echo "<pre>".print_r($data_search,true)."</pre>"; 
//exit();		
//


try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 401, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณาลองใหม่' ,'token'=>NoCSRF::generate( 'csrf_token' )));
   exit();
}



$report = array();
$arr 	= array();
try {
	if(!empty($barcode)) {
		if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
			throw new Exception('รูปแบบข้อมูลไม่ถูกต้อง');
		}
	}

	if(!empty($receiver)) {
		if(preg_match('/<\/?[^>]+(>|$)/', $receiver)) {
			throw new Exception('รูปแบบข้อมูลไม่ถูกต้อง');
		}
	}

	if(!empty($sender)) {
		if(preg_match('/<\/?[^>]+(>|$)/', $sender)) {
			throw new Exception('รูปแบบข้อมูลไม่ถูกต้อง');
		}
	}

	if(!empty($start_date)) {
		if(preg_match('/<\/?[^>]+(>|$)/', $start_date)) {
			throw new Exception('รูปแบบข้อมูลไม่ถูกต้อง');
		}
	}

	if(!empty($end_date)) {
		if(preg_match('/<\/?[^>]+(>|$)/', $end_date)) {
			throw new Exception('รูปแบบข้อมูลไม่ถูกต้อง');
		}
	}

	if(!empty($status)) {
		if(preg_match('/<\/?[^>]+(>|$)/', $status)) {
			throw new Exception('รูปแบบข้อมูลไม่ถูกต้อง');
		}
	}



	$report = $work_inoutDao->searchUser( $data_search );

	foreach( $report as $num => $value ){
		$report[$num]['no'] = $num +1;
		$report[$num]['action'] = '<a data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="work_info_employee.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><span class="material-icons">dehaze</span></a>';
		$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
		$report[$num]['name_send'] = $value['mr_emp_name']." ".$value['mr_emp_lastname'];
		$report[$num]['ch_data'] = '
			<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
					<input name="ch_bog[]" value="'.$value['mr_work_main_id'].'" type="checkbox" class="custom-control-input">
					<span class="custom-control-indicator"></span>
				</label>
		';
		
	}

	$arr['data'] = $report;
	$arr['status'] = 200;
	$arr['message'] = 'สำเร็จ';
	$arr['token'] = NoCSRF::generate( 'csrf_token');
	//echo json_encode(array('status' => 500, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	//exit;

} catch(Exception $e) {
	$report = array();
	$arr['data'] = $report;
	$arr['status'] = 500;
	$arr['message'] = $report;
	$arr['token'] = NoCSRF::generate( 'csrf_token');
}


echo json_encode($arr);





 ?>

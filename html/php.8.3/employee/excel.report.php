<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'PHPExcel.php';
require_once 'Dao/User.php';
include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');
set_time_limit(0);

ob_start();

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();     

$data_search		    		=  json_decode($req->get('params'),true);    
$data_search = array_map(function($p) { return addslashes(strip_tags(trim($p))); }, $data_search);


$user_id= $auth->getUser();
$emp_data = $userDao->getEmpDataByuserid( $user_id );  
$data_search['mr_emp_id']  		=  $emp_data['mr_emp_id'];  
$data_search['user_id']  		=  $user_id;  

$data = $work_inoutDao->searchUser( $data_search );



function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}

//foreach( $data as $num => $d ){
//	if ( $d['group_user_id'] == 16 ){
//		if ( $data[$num]['status'] == "รอส่ง TMB" ) {
//			$data[$num]['status'] = "สำเร็จ";
//		}else{
//			$data[$num]['status'] = $d['status'];
//		}
//	}
//	$data_log = $work_logDao->SLAlog( $d['mr_work_main_id'] );
//	
//	foreach( $data_log as $num_log => $dl ){
//		if ( $dl['mr_status_id'] == 1 ){
//			$data[$num]['sys_timestamp_log_1']	= $dl['sys_timestamp'];
//		}else if ( $dl['mr_status_id'] == 2 ){
//			$data[$num]['sys_timestamp_log_2']	= $dl['sys_timestamp'];
//		}else if ( $dl['mr_status_id'] == 3 ){
//			$data[$num]['sys_timestamp_log_3']	= $dl['sys_timestamp'];
//		}else if ( $dl['mr_status_id'] == 4 ){
//			$data[$num]['sys_timestamp_log_4']	= $dl['sys_timestamp'];
//		}else if ( $dl['mr_status_id'] == 5 ){
//			$data[$num]['sys_timestamp_log_5']	= $dl['sys_timestamp'];
//		}
//	}
//	
//}



/* new Excel */
/* new Excel */
/* new Excel */
$sheet1 = 'Detail';
$headers = array(
	'No.',                           
	'Barcode',                            
	'ชื่อผู้รับ',                            
	'ที่อยู่',                            
	'ชื่อผู้ส่ง',                            
	'ที่อยู่',    
	'ชื่อเอกสาร',
	'วันที่ส่ง',                            
	'วันที่สำเร็จ',                            
	'สถานะงาน',                            
	 'ประเภทการส่ง', 
	 'หมายเหตุ'                           
	 //'วันเวลาที่สั่งงาน'     
	// 'วันเวลาที่พนักงานเดินเอกสารรับเอกสารแล้ว',     
	// 'วันเวลาพนักงานเดินเอกสารอยู่ในระหว่างนำส่ง',     
	// 'วันเวลาเอกสารถึงผู้รับเรียบร้อยแล้ว',     
	// 'วันเวลา',     
	// 'หมายเหตุ',     
);

// Add Data
foreach($data as $keys => $vals) {
	if( $vals['mr_work_date_sent'] ){
		list( $y, $m, $d ) = explode("-", $vals['mr_work_date_sent']);
		$date_order[$keys]['mr_work_date_sent'] = $d."/".$m."/".$y;
	}	
	
	if( $vals['mr_work_date_success'] ){
		list( $y, $m, $d ) = explode("-", $vals['mr_work_date_success']);
		$date_meet[$keys]['mr_work_date_success'] = $d."/".$m."/".$y;
	}	
	
	if(  $vals['name_re'] ){
		$name_receive[$keys]['name_re'] 				= $vals['name_re']." ".$vals['lastname_re'];
		if($vals['mr_type_work_id'] == 2){
			$name_receive[$keys]['depart_receive'] 		= $vals['branch_re'];
		}else{
			$name_receive[$keys]['depart_receive'] 		= $vals['depart_code_receive']." - ".$vals['depart_name_receive']." ชั้น ".$vals['depart_floor_receive'];
		}
	}			
	
	if(  $vals['mr_emp_name'] ){
		$name_send[$keys]['send_name'] 				= $vals['mr_emp_name']." ".$vals['mr_emp_lastname'];
		if($vals['mr_user_role_send'] == 2){ //mr_user_role_send
			$name_send[$keys]['depart_send'] 				= $vals['mr_department_code']." - ".$vals['mr_department_name']." ชั้น ".$vals['depart_floor_send'];
		}else{
			$name_send[$keys]['depart_send'] 				= $vals['branch_sen'];
		}
	}

$ex_data[$keys] = array(
	($keys+1),
	$vals['mr_work_barcode'],
	$name_receive[$keys]['name_re'],
	$name_receive[$keys]['depart_receive'],
	$name_send[$keys]['send_name'],
	$name_send[$keys]['depart_send'],
	$vals['mr_topic'],
	$date_order[$keys]['mr_work_date_sent'],
	$date_meet[$keys]['mr_work_date_success'],
	$vals['mr_status_name'],
	$vals['mr_type_work_name'],
	$vals['mr_work_remark']
);
}





// echo count($data)."<br>";
// echo "done";
// exit;


$file_name = 'TMB_Report.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');

$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');

$writer->writeSheetRow($sheet1,$headers,$styleHead);
foreach ($ex_data as $v) {
    $writer->writeSheetRow($sheet1,$v,$styleRow);
 }


$writer->writeToStdOut();

exit;
/* End new Excel */
/* End new Excel */








$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma')->setSize(10);
$objPHPExcel->getProperties()->setCreator("Pivot Co.,Ltd")->setLastModifiedBy("Pivot")->setTitle("Report Agent")->setSubject("Report")->setDescription("Report document for Excel, generated using PHP classes.");

$columnIndexs = range('A','Q');
$columnNames[0] = array(
		0 => 'No.',                           
		1 => 'Barcode',                            
		2 => 'ชื่อผู้รับ',                            
		3 => 'ที่อยู่',                            
		4 => 'ชื่อผู้ส่ง',                            
		5 => 'ที่อยู่',    
		6 => 'ชื่อเอกสาร',
		7 => 'วันที่ส่ง',                            
		8 => 'วันที่สำเร็จ',                            
		9 => 'สถานะงาน',                            
		10 => 'ประเภทการส่ง',                            
		11 => 'วันเวลาที่สั่งงาน',     
		//12 => 'วันเวลาที่พนักงานเดินเอกสารรับเอกสารแล้ว',     
		//13 => 'วันเวลาพนักงานเดินเอกสารอยู่ในระหว่างนำส่ง',     
		//14 => 'วันเวลาเอกสารถึงผู้รับเรียบร้อยแล้ว',     
		//15 => 'วันเวลา',     
		//16 => 'หมายเหตุ',     
);

// Set column widths
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setWidth(30);
    
$row = 1;
foreach ($columnNames as $key => $value) {
	for($index = 0; $index < count($columnIndexs); $index++) {
		$leadColumns[$key][$index]['index'] = $columnIndexs[$index].$row;
		$leadColumns[$key][$index]['name'] = $columnNames[$key][$index];
	}
	$row++;
}

foreach($leadColumns as $head) {
	foreach ($head as $headColumn) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($headColumn['index'], $headColumn['name'])->getStyle($headColumn['index']);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLACK);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->getStartColor()->setARGB('FFDCDCDC');
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);																																 
	}
}

 $indexs = 2;
// Add Data
foreach($data as $keys => $vals) {
			if( $vals['mr_work_date_sent'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_sent']);
				$date_order[$keys]['mr_work_date_sent'] = $d."/".$m."/".$y;
			}	
			
			if( $vals['mr_work_date_success'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_success']);
				$date_meet[$keys]['mr_work_date_success'] = $d."/".$m."/".$y;
			}	
			
			if(  $vals['name_re'] ){
				$name_receive[$keys]['name_re'] 				= $vals['name_re']." ".$vals['lastname_re'];
				if($vals['mr_type_work_id'] == 2){
					$name_receive[$keys]['depart_receive'] 		= $vals['branch_re'];
				}else{
					$name_receive[$keys]['depart_receive'] 		= $vals['depart_code_receive']." - ".$vals['depart_name_receive']." ชั้น ".$vals['depart_floor_receive'];
				}
			}			
			
			if(  $vals['mr_emp_name'] ){
				$name_send[$keys]['send_name'] 				= $vals['mr_emp_name']." ".$vals['mr_emp_lastname'];
				if($vals['mr_user_role_send'] == 2){ //mr_user_role_send
					$name_send[$keys]['depart_send'] 				= $vals['mr_department_code']." - ".$vals['mr_department_name']." ชั้น ".$vals['depart_floor_send'];
				}else{
					$name_send[$keys]['depart_send'] 				= $vals['branch_sen'];
				}
			}		

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$indexs, ($keys+1))->getStyle('A'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$indexs, $vals['mr_work_barcode'])->getStyle('B'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$indexs, $name_receive[$keys]['name_re'])->getStyle('C'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$indexs, $name_receive[$keys]['depart_receive'])->getStyle('D'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$indexs, $name_send[$keys]['send_name'])->getStyle('E'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$indexs, $name_send[$keys]['depart_send'])->getStyle('F'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$indexs, $vals['mr_topic'])->getStyle('G'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$indexs, $date_order[$keys]['mr_work_date_sent'])->getStyle('H'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$indexs, $date_meet[$keys]['mr_work_date_success'])->getStyle('I'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$indexs, $vals['mr_status_name'])->getStyle('J'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$indexs, $vals['mr_type_work_name'])->getStyle('K'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$indexs, $vals['mr_work_remark'])->getStyle('L'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet(0)->getStyle('A'.$indexs.':L'.$indexs)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$indexs++;
}

$nickname = "report_daily";
$filename = $nickname.'.xls';

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Daily Report');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$filename.'"'); 
header('Cache-Control: max-age=0');
header('Pragma: public');
ob_end_clean();
$objWriter->save('php://output');
exit();
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Round.php';
require_once 'nocsrf.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//
$req = new Pivot_Request();
$userDao = new Dao_User();

$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$userRoleDao		= new Dao_UserRole();
$branchDao			= new Dao_Branch();
$roundDao			= new Dao_Round();
$send_workDao		= new Dao_Send_work();	



$user_id			= $auth->getUser();
$user_data 			= $userDao->getempByuserid($user_id);
$user_data_show 	= $userDao->getEmpDataByuseridProfile($user_id);
$round 				= $roundDao->getRoundByhand();
$department		 	= $departmentDao->fetchAll();

// echo "<pre>".print_r($department,true)."</pre>";//mr_emp_code
// exit;

//$emp_data 		= $employeeDao->getEmpDataSelect();

$emp_send_data = '';
if(!empty($user_data_show)){
	$emp_send_data .= $user_data_show['mr_emp_code'].'  '.$user_data_show['mr_emp_name'].'  '.$user_data_show['mr_emp_lastname'];
	$emp_send_data .= '  '.$user_data_show['mr_department_code'].'  '.$user_data_show['mr_department_name'].'  '.$user_data_show['name'];
	$emp_send_data .= '  '.$user_data_show['mr_emp_email'].'  '.$user_data_show['mr_emp_tel'];

	if($user_data_show['mr_user_role_id'] == 2){
		//echo "<pre>".print_r($user_data_show,true)."</pre>";//mr_emp_code
		if($user_data_show['mr_branch_id'] != 1 and $user_data_show['mr_emp_id']!=''){
			$update_branch['mr_branch_id'] = 1;
			$employeeDao->save($update_branch,$user_data_show['mr_emp_id']);
		}
	}
}

if($user_data_show['emp_type'] == '' || $user_data_show['emp_type'] == Null || $user_data_show['emp_type'] == 'NULL'){
	$show_select = false; 
}else if($user_data_show['emp_type'] == 'outsource'){
	$show_select = true;
}else{
	$show_select = false;
}
//echo "<pre>".print_r($user_data_show,true)."</pre>";//mr_emp_code
$template = Pivot_Template::factory('employee/work_byhand.tpl');
$template->display(array(
	//'debug' 			=> print_r($path_pdf,true),
	'userRoles' 		=> (isset($userRoles) ? $userRoles : null),
	'success' 			=> (isset($success) ? $success : null),
	'department' 		=> $department,
	'emp_send_data' 		=> $emp_send_data,
	'user_data' 		=> $user_data,
	'round' 			=> $round,
	'user_data_show' 	=> $user_data_show,
	'select' 			=> '0',
	'today' 			=> date('Y-m-d'),
	'role_id' 			=> $auth->getRole(),
	'csrf_token' 		=>  NoCSRF::generate( 'csrf_token'),
	'roles'	 			=> Dao_UserRole::getAllRoles(),
	'serverPath' 		=> $_CONFIG->site->serverPath,
	'show_select'       => $show_select
));
<?php
require_once 'Pivot/Dao.php';


class Dao_Branch extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_branch');
    }
	
	
	
	function getBranch()
	{
		$sql =
			'
			select * 
			from mr_branch 
			where active = 1
			ORDER BY  `mr_branch`.`mr_branch_code` ASC
				
			
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	function getBranchData_forExcel()
	{
		$sql =
			'
			SELECT
				b.mr_branch_id,
				b.mr_branch_name,
				b.mr_branch_code,
				b.mr_hub_id,
				b.mr_branch_type,
				b.mr_branch_category_id,
				h.mr_hub_name,
				bt.branch_type_name,
				bg.mr_branch_category_name
			FROM
				mr_branch b
			LEFT JOIN mr_branch_type bt ON
				(
					b.mr_branch_type = bt.mr_branch_type_id
				)
			LEFT JOIN mr_branch_category bg ON
				(
					b.mr_branch_category_id = bg.mr_branch_category_id
				)
			LEFT JOIN mr_hub h ON
				(b.mr_hub_id = h.mr_hub_id)
				
			
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
		
	function getBranchTXT( $txt )
	{
		$params = array();
		$sql =
			'
			select * 
			from mr_branch 
			where mr_branch_id != 1
				and active = 1
				and ( mr_branch_code LIKE ? or mr_branch_name LIKE ? )
				ORDER BY  `mr_branch`.`mr_branch_code` ASC
				limit 10 
			
		';
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}

	function getBranchCodeByID( $id )
	{
		$params = array();
		$sql =
			'
			select mr_branch_code 
			from mr_branch 
			where
				mr_branch_id = ?
		';
		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}
	
	function getBranchCodeByhub( $hub_id )
	{
		$params = array();
		$sql = "
		SELECT 
			GROUP_CONCAT( mr_branch_id ) AS mr_branch_id
		FROM mr_branch
		WHERE mr_hub_id =?
		GROUP BY mr_hub_id
		";

		array_push($params, (int)$hub_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}
	
	
	function searchWorkBranch( $data_search )
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				m.mr_work_date_sent,
				m.mr_work_date_success,
				i.sys_timestamp as time_test,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				u.mr_user_id,
				x.mr_emp_name,
				x.mr_emp_lastname,
				t.mr_type_work_name,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				y.mr_department_name,
				y.mr_department_code,
				z.name as depart_floor_send,
				s.mr_status_name,
				x.mr_emp_id as emp_re,
				e.mr_emp_id as emp_send,
				i.mr_user_id,
				e.mr_emp_id,
				u.mr_user_id,
				m.mr_work_main_id
				
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			
			WHERE
				m.mr_status_id <> 0
			
		';

		if($data_search['sender'] == "" && $data_search['receiver'] == ""  ){
			$sql .= ' AND ( u.mr_user_id = ? OR e.mr_emp_id = ? ) ';
			array_push($params, (int)$data_search['user_id']);
			array_push($params, (int)$data_search['mr_emp_id']);
		}else {
			$sql .= ' AND x.mr_emp_code = ? AND e.mr_emp_code = ?';
			array_push($params, (string)$data_search['sender']);
			array_push($params, (string)$data_search['receiver']);
		}
		// if( $data_search['sender'] != ""  ){
		// 	$sql .= ' AND x.mr_emp_code = "'.$data_search['sender'].'" AND e.mr_emp_id ="'.$data_search['mr_emp_id'].'"';
		// }
		
		// if( $data_search['receiver'] != ""  ){
		// 	$sql .= ' AND e.mr_emp_code = "'.$data_search['receiver'].'" AND u.mr_user_id ="'.$data_search['user_id'].'"';
		// }
		
		
		if( $data_search['barcode'] != "" ){
			$sql .= ' AND m.mr_work_barcode LIKE ? ';
			array_push($params, (string)'%'.$data_search['barcode'].'%');
		}
		
		if( $data_search['status'] != "" ){
			$sql .= ' AND m.mr_status_id = ?';
			array_push($params, (int)$data_search['status']);
		}
		
		
		if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
			//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
			
			$sql .= '  AND ( m.mr_work_date_sent BETWEEN ? AND ? )';
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
			array_push($params, (string)$data_search['start_date']);
			array_push($params, (string)$data_search['end_date']);
		}

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();

	}
	
	function selectdata($sql,$params=array())
	{
		if($sql != ''){
			// prepare statement SQL
			$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
			// Execute Query
			if(!empty($params)){
				$stmt->execute($params);
			}else{
				$stmt->execute();
			}
			return $stmt->fetchAll();
		}else{
			return 'No SQL';
		}
	}
	
	function select($sql,$params=array())
	{
		if($sql != ''){
			// prepare statement SQL
			$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
			// Execute Query
			if(!empty($params)){
				$stmt->execute($params);
			}else{
				$stmt->execute();
			}
			return $stmt->fetchAll();
		}else{
			return 'No SQL';
		}
	}
	
}
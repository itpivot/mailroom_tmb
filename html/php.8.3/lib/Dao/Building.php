<?php
require_once 'Pivot/Dao.php';


class Dao_Building extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_building');
    }
	
	function getBuildingByBranchId( $mr_branch_id )
	{
		$params = array();
		$sql =
			'
			select 
				bd.mr_building_name,
				bd.mr_building_id,
				bd.mr_branch_id
			from mr_building bd
			where bd.mr_branch_id = ?
			
		';
		array_push($params, (int)$mr_branch_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}

	
}
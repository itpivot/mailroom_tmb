<?php
require_once 'Pivot/Dao.php';


class Dao_Cost extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_cost');
    }

	
	
	function fetchCostByCostCode( $cost_code )
	{
		$params = array();
		$sql =
		'
			SELECT 
				mr_cost_id
			FROM mr_cost
			WHERE
				mr_cost_code = ? 
		';

		array_push($params, (string)$cost_code);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}
	function fetchCostByCodeOrname( $txt )
	{
		$params = array();
		$sql =
		'
			SELECT 
				mr_cost_id,
				mr_cost_code,
				mr_cost_name
			FROM mr_cost
		';
		if($txt!=''){
				$sql .=
				' WHERE
						mr_cost_code = ? 
						or mr_cost_name like ? 
				';
				array_push($params, (string)$txt);
				array_push($params, (string)$txt);
		}
		$sql .=
		'order by mr_cost_code asc 
		LIMIT 0,100
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}
	function checkCostByCostCode( $cost_code )
	{
		$params = array();
		$sql =
		'
			SELECT 
				mr_cost_id,
				mr_cost_code,
				mr_cost_name
			FROM mr_cost
			WHERE
				mr_cost_code = ? 
		';

		array_push($params, (string)$cost_code);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}
	function getcostdata($txt){
		$params = array();
		$searchTerm = '%' . $txt . '%';
		$sql = 'select * from mr_cost where mr_cost_name like ? limit 10';
		array_push($params, (string)$searchTerm);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

		$stmt->execute($params);

		return $stmt->fetchAll();
	}
	
   	
}
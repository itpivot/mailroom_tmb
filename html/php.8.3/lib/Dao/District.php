<?php
require_once 'Pivot/Dao.php';


class Dao_District extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_district');
    }

    public function getLocationWithName($amphure_name, $district_name)
    {
        $params = [];
        $sql    = '
        select
            sd.pvs_districts_id,
            sd.pvs_districts_code,
            sd.pvs_districts_name,
            ap.pvs_amphures_id,
            ap.pvs_amphures_code,
            ap.pvs_amphures_name,
            pv.pvs_provinces_id,
            pv.pvs_provinces_code,
            pv.pvs_provinces_name,
            sd.pvs_geography_id,
            sd.zipcode
        from ' . $this->_tableName . ' sd
            left join pvs_amphures ap on (ap.pvs_amphures_code = sd.pvs_amphures_code)
            left join pvs_provinces pv on (pv.pvs_provinces_code = ap.pvs_provinces_code)
        where
            sd.pvs_districts_name like ? and
            ap.pvs_amphures_name like ?
        ';

        array_push($params, trim($district_name));
        array_push($params, trim($amphure_name));

        // prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        // Execute Query
        if (!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }

        return $stmt->fetch();

        // return $this->_db->fetchRow($sql);
    }
	
}
<?php
require_once 'Pivot/Dao.php';


class Dao_Employee extends Pivot_Dao
{   
    function __construct()
    {
        parent::__construct('mr_emp');
    }
	
	function getEmployeeid($code)
    {
		$params = array();
		
        $sql = '
			SELECT 
				mr_emp_id as id 
			FROM ' . $this->_tableName . ' 
			WHERE code = ?
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

		array_push($params, trim($code));
        //echo  "<br><br><br><br><br><br>".$sql ;
        $id = $stmt->fetchAll();
        return $id[0]['id'];
	}	
	
	
	
	function getEmp_code($mr_emp_code)
    {
		$params = array();

        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_code,
					e.mr_branch_id,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					fl.name as mr_department_floor,
					e.mr_floor_id,
					b.mr_branch_name,
					b.mr_branch_code,
					d.mr_department_name,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				left join mr_floor fl on ( fl.mr_floor_id = e.mr_floor_id)
				WHERE e.mr_emp_code = ?
		';

			// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
		array_push($params, trim($mr_emp_code));
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetch();
		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	
	function getEmpByID($mr_emp_id)
    {
		$params = array();
		
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_tel,
					e.mr_branch_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					e.mr_cost_id,
					d.mr_department_id ,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_name,
					b.mr_branch_name,
					b.mr_branch_code,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_user u on ( e.mr_emp_id = e.mr_emp_id)
				WHERE e.mr_emp_id = ?
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		 
		array_push($params, (int)$mr_emp_id);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();


		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	
	function getEmpcontact($mr_emp_id)
    {
		$params = array();
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_tel,
					e.mr_branch_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_name,
					b.mr_branch_name,
					b.mr_branch_code,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				WHERE e.mr_emp_id = ? 
		';

		array_push($params, (int)$mr_emp_id);
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	// MARCH MARCH MARCH 
	function getEmpData($data)
    {
		$params = array();
        $sql = 'SELECT 
					e.*,
					d.*,
					u.mr_user_id as u_id
				FROM mr_emp e
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				WHERE e.mr_emp_code = ?
					AND e.mr_emp_name = ?
					AND e.mr_emp_lastname = ?
		';

		array_push($params, (string)$data['mr_emp_code']);
		array_push($params, (string)$data['mr_emp_name']);
		array_push($params, (string)$data['mr_emp_lastname']);

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	
	function getEmpDataSelect( )
    {
        $sql = 'SELECT 
					e.mr_emp_id,
					e.update_date,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_code,
					e.mr_emp_tel,
					e.mr_emp_mobile,
					e.mr_emp_email,
					e.mr_department_id,
					e.mr_position_id,
					e.mr_floor_id,
					e.mr_cost_id,
					e.mr_branch_id,
					e.mr_branch_floor,
					e.mr_date_import,
					e.mr_hub_id,
					e.emp_type
				FROM mr_emp e
				WHERE e.mr_emp_code IS NOT NULL 
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}
	function getEmpDataUser_forExcel( )
    {
        $sql = 'SELECT
					e.mr_emp_id,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_code,
					e.mr_emp_tel,
					e.mr_emp_mobile,
					e.mr_emp_email,
					e.mr_department_id,
					e.mr_position_id,
					e.mr_floor_id,
					e.mr_cost_id,
					e.mr_branch_id,
					e.mr_branch_floor,
					e.mr_workplace,
					e.mr_workarea,
					e.mr_hub_id,
					u.mr_user_username,
					u.mr_user_id,
					u.mr_user_role_id
				FROM
					mr_emp e
				LEFT JOIN mr_user u ON
					(e.mr_emp_id = u.mr_emp_id)
				';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}


	function getEmpDataWithTXT($txt)
	{
		$params = array();
		if($txt == ''){
			$sql =
			"SELECT * FROM mr_emp 
			WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
			ORDER BY mr_emp_id DESC limit 10 ";
		}else{
			$sql =
				"
				SELECT * 
				FROM mr_emp
				WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
				and  
				MATCH (mr_emp_code,mr_emp_name,mr_emp_lastname)
				AGAINST ('".$txt."' IN NATURAL LANGUAGE MODE)
				limit 10 
				";
		}

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();

		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getEmpDataWithTXT_fileImport($txt)
	{
		if($txt != ''){
		
		$params = array();
		
		$sql =
			'
			select * 
			from mr_emp e
			left join mr_branch b on(b.mr_branch_id = e.mr_branch_id)
			left join mr_position p on(p.mr_position_id = e.mr_position_id)
			where
					( 						
						b.mr_branch_name like ? or 
						e.mr_emp_lastname like ? or 
						e.mr_emp_lastname like ?
					) and
					e.mr_emp_code NOT LIKE "%Resign%" and 
					e.mr_emp_code is not null limit 100 
		';
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();

		//echo $sql;
		// return $this->_db->fetchAll($sql);
		}else{
			$sql =
			'
			select * 
			from mr_emp e
			left join mr_branch b on(b.mr_branch_id = e.mr_branch_id)
			left join mr_position p on(p.mr_position_id = e.mr_position_id)
			where
					e.mr_emp_code NOT LIKE "%Resign%" and 
					e.mr_emp_code is not null limit 100 
			
		';
		return $this->_db->fetchAll($sql);
		}
	}
	
	
	function getEmpCode()
	{
		$sql =
			'
			select mr_emp_code
			from mr_emp e
			
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	function getEmpDataWithTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or 
					 e.mr_emp_code like ?
					 ) and
					 e.mr_department_id != 0 and 
					 e.mr_floor_id != 0 and 
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 10 
		';

		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function byhandgetEmpDataWithTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or 
					 e.mr_emp_code like ?
					 ) and
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 10 
		';

		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getMessDataWithTXTsearch($txt){
		$params = array();

		$sql =
			'
			select * 
			from mr_emp e
			left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or 
					 e.mr_emp_code like ?
					 ) and
					 u.mr_user_role_id = 8 and
					 u.mr_user_username is not null and 
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 10 
		';

		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getMessDataFullTXTsearch($txt)
	{
		$params = array();

		$sql =
			"
			SELECT 
				* 
			FROM mr_emp e
			left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
			WHERE 
				( 
					
					MATCH(e.mr_emp_name, e.mr_emp_lastname, e.mr_emp_code, e.mr_emp_tel, e.mr_emp_mobile)
					AGAINST('%".$txt."%' IN NATURAL LANGUAGE MODE)
				)
				and
					 u.mr_user_role_id = 8 and
					 u.mr_user_username is not null  
			
		";
		//echo $sql;
		return $this->_db->fetchAll($sql);
		exit;
		//echo $sql 
		array_push($params,$txt);

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	
	
	function ImportEmp( $sql )
	{
		return $this->_db->query($sql);
		//echo $sql;
		
	}
	
	function getEmpDataResign( )
    {
        $sql = 'SELECT *
				FROM mr_emp e
				LEFT JOIN mr_user m on ( e.mr_emp_id = m.mr_emp_id )
				WHERE e.mr_emp_code IS NOT NULL AND
				m.active = 0
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}
	
	
	function updateEmpByID(  $datas, $mr_emp_code  )
	{	
		$sql = ' SELECT mr_emp_id FROM mr_emp WHERE mr_emp_code = "'.$mr_emp_code.'" ';
		$id = $this->_db->fetchOne($sql);
		if(!empty($id)){
        	$this->_db->update($this->_tableName, $datas,  'mr_emp_code = "' .$mr_emp_code.'"');
		}else{
			$id = 0;
		}
		//echo $sql;
		return $id;
	}
	
	
	function checkEmpResign( $mr_emp_code )
    {
        $sql = 
		'
			SELECT 
				mr_emp_email
			FROM 
				mr_emp
			WHERE 
				mr_emp_code = "'.$mr_emp_code.'" 

		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchOne($sql);
	}
	
	
	function getEmpDataWithTXTBranch($txt)
	{
		if($txt == ''){
			$sql =
			"SELECT * FROM mr_emp 
			WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
			ORDER BY mr_emp_id DESC limit 10 ";
		}else{
				$sql =
				"
				SELECT * 
				FROM mr_emp
				WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
				and  
				MATCH (mr_emp_code,mr_emp_name,mr_emp_lastname)
				AGAINST ('".$txt."' IN NATURAL LANGUAGE MODE)
				limit 10 
				";
		}
	
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	
	
	function getEmpByIDBranch($mr_emp_id)
    {
		$params = array();
		
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					e.mr_emp_tel,
					e.mr_branch_floor,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_id,
					d.mr_department_name,
					e.mr_workplace,
					e.mr_workarea,
					b.*
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				WHERE e.mr_emp_id = ?
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
				
		array_push($params, (int)$mr_emp_id);
		// Execute Query
		$stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
		
		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	//function getEmp_name( $name )
    //{
    //    $sql = 'SELECT e.mr_emp_id,
	//				e.mr_emp_code,
	//				e.mr_emp_name,
	//				e.mr_emp_lastname,
	//				e.mr_emp_email,
	//				d.mr_department_code ,
	//				d.mr_department_floor,
	//				d.mr_department_name
	//			FROM mr_emp e
	//			left join mr_department d on ( d.mr_department_id = e.mr_department_id)
	//			left join mr_floor f on ( f.mr_floor_id = d.mr_department_floor)
	//			WHERE e.mr_emp_name LIKE "%'.$name.'%" OR e.mr_emp_lastname LIKE "%'.$name.'%"
	//	';
	//	//echo  "<br><br><br><br><br><br>".$sql ;
	//	return $this->_db->fetchAll($sql);
	//}


	function getEmpProfileBy_emp_id($emp_id)
	{
		$params = array();
		
		$sql =
		'
		SELECT 
			e.mr_emp_id,
			e.sys_time,
			e.update_date,
			e.mr_emp_code,
			e.old_emp_code,
			e.mr_department_id,
			e.mr_position_id,
			e.mr_floor_id,
			e.mr_cost_id,
			e.mr_branch_id,
			e.mr_branch_floor,
			e.mr_date_import,
			e.mr_workplace,
			e.mr_workarea,
			e.mr_hub_id,
			u.mr_user_id,
			e.emp_type,
			e.mr_emp_name, 
			e.mr_emp_lastname, 
			e.mr_emp_tel, 
			e.mr_emp_mobile,
			e.mr_emp_email, 
			d.mr_department_id ,
			d.mr_department_name  ,
			d.mr_department_code  ,
			d.mr_department_floor ,
			f.mr_floor_id as floor_emp,
			f.mr_floor_id as mr_floor_id_emp,
			b.mr_branch_code,
			b.mr_branch_type,
			bt.branch_type_name,
			b.mr_branch_name as branch_name,
			concat(b.mr_branch_code," : ",b.mr_branch_name) as mr_branch_name,
			concat(p.mr_position_code," : ",p.mr_position_name) as mr_position_name,
			f.name
		FROM mr_emp e 
		left join mr_user u on (e.mr_emp_id = u.mr_emp_id )
		Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
		Left join mr_floor f on ( e.mr_floor_id = f.mr_floor_id )
		Left join mr_branch b on ( e.mr_branch_id = b.mr_branch_id )
		Left join mr_branch_type bt on ( b.mr_branch_type  = bt.mr_branch_type_id )

		Left join mr_position p on ( e.mr_position_id = p.mr_position_id )
		WHERE e.mr_emp_id = ?
			
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		 
		array_push($params, (int)$emp_id);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	
	}

	function getEmpDataById($empId)
	{
		$params = array();
		$sql =
		'
			select 	* from '.$this->_tableName.' where mr_emp_id = ?
		';

		array_push($params, (int)$empId);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}

	function select_employee($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
    
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	function select_employee_row($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
    
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetch();
	}
}
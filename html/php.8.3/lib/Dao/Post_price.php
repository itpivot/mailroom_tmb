<?php
require_once 'Pivot/Dao.php';


class Dao_Post_price extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_post_price');
    }

    function getprice($type_id,$weight)
	{
		$params = array(); 
		$sql =
		'
        SELECT * 
        FROM mr_post_price 
        WHERE mr_type_post_id = ? 
        AND weight >= ? 
        ORDER BY weight ASC
        limit 0,1
		';
    array_push($params, floatval($type_id));
	array_push($params, floatval($weight));

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
    function load_postPrice($type_id,$mr_post_price_id = null)
	{
		$params = array(); 
		$sql =
		'
        SELECT 
            pp.mr_post_price_id,
            pp.create_by,
            pp.mr_type_post_id,
            pp.weight,
            pp.min_weight,
            pp.max_weight,
            pp.description,
            pp.post_price,
            t.mr_type_post_name
        FROM mr_post_price pp
        left join mr_type_post t  on(t.mr_type_post_id=pp.mr_type_post_id)
        WHERE pp.mr_post_price_id is not null ';
       if($mr_post_price_id != ''){
            $sql .=' and pp.mr_post_price_id = ? ';
            array_push($params, floatval($mr_post_price_id));
       }
       if($type_id != ''){
            $sql .=' and pp.mr_type_post_id = ? ';
            array_push($params, floatval($type_id));
       }

        $sql .=' ORDER BY pp.mr_type_post_id,pp.post_price  ASC ';
        

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
}
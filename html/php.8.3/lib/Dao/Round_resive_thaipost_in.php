<?php
require_once 'Pivot/Dao.php';


class Dao_Round_resive_thaipost_in extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_round_resive_thaipost_in');
    }
	

function select($sql,$params=array())
	{
		if($sql != ''){
			// prepare statement SQL
			$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
			// Execute Query
			if(!empty($params)){
				$stmt->execute($params);
			}else{
				$stmt->execute();
			}
			return $stmt->fetchAll();
		}else{
			return 'No SQL';
		}
	}

	function query2($sql1,$sql2){
			$this->_db->beginTransaction();
			try {
				$this->_db->query($sql1);
			    $this->_db->query($sql2);
				$this->_db->commit();
				$re = "success";
				
			}catch (exception $e) {
				$this->_db->rollback();
				$re = "ereeor";
			}
			return  $re;
			
		}function query1($sql1){
			$this->_db->beginTransaction();
			try {
				$this->_db->query($sql1);
				$this->_db->commit();
				$re = "success";	
			}catch (exception $e) {
				$this->_db->rollback();
				$re = "ereeor";
			}
			return  $re;
			
		}

		function select_send_work($sql,$params){
		
			// prepare statement SQL
			$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
			// Execute Query
			$stmt->execute($params);
			
			return $stmt->fetchAll();
		}
	function Ch_barcode($barcode)
	{
		$params = array(); 
		$sql =
		'
		SELECT
			mr_round_resive_thaipost_in_id,
			is_receiver
		FROM
			mr_round_resive_thaipost_in
		WHERE
			barcode LIKE ?
		';
		array_push($params, strval($barcode));
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
	}
	function barcode_resive_thaipost_in($val)
	{
		$params = array(); 
		$sql =
		'
			SELECT
				rn.mr_round_resive_thaipost_in_id,
				rn.is_receiver,
				rn.detail_work,
				rn.remark,
				rn.sysdate,
				rn.date_send,
				rn.mr_round_id,
				rn.mr_user_id,
				rn.barcode,
				r.mr_round_name
			FROM
				mr_round_resive_thaipost_in rn
			LEFT JOIN mr_round r ON
				(rn.mr_round_id = r.mr_round_id)
			WHERE
				rn.date_send like ?
			order by rn.is_receiver desc
		';
		array_push($params, strval($val['date']));
		if($val['mr_round_id'] !=''){
			$sql .=' and rn.mr_round_id = ?';
			array_push($params, intval($val['mr_round_id']));
		}
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql
		$stmt->execute($params);
		return $stmt->fetchAll();
	}
	function report_scan_post($data_search) {
		$params = array();
		$sql = 'SELECT rrti.sysdate,
					rrti.date_send,
					rrti.time_receive,
					rrti.remark,
					rrti.barcode,
					r.mr_round_name,
					e.mr_emp_name,
                    rrti.detail_work
				FROM mr_round_resive_thaipost_in as rrti
				LEFT JOIN mr_round as r on r.mr_round_id = rrti.mr_round_id
				LEFT JOIN mr_user as u on u.mr_user_id = rrti.mr_user_id
				LEFT JOIN mr_emp as e on e.mr_emp_id = u.mr_emp_id
				Where rrti.active = 1
				';
		// ตรวจสอบว่าค่า start_date ไม่เป็นค่าว่าง
		if ($data_search['start_date'] != '') {
			$sql .= ' AND rrti.sysdate >= ?';
			$sql .= ' AND rrti.sysdate <= ?';
			array_push($params, $data_search['start_date'] . " 00:00:00");
			array_push($params, $data_search['end_date'] . " 23:59:59");
		}
	
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// ดำเนินการคำสั่ง SQL
		$stmt->execute($params);
		// echo $sql;  // ย้ายบรรทัดนี้มาก่อนการ return ผลลัพธ์ เพื่อดูคำสั่ง SQL
		return $stmt->fetchAll();
	}
	function getreport_scan_post($data_date){
		$params = array();
		$sql = 'SELECT rrti.sysdate,
					rrti.date_send,
					rrti.time_receive,
					rrti.remark,
					rrti.barcode,
					r.mr_round_name,
					e.mr_emp_name,
                    rrti.detail_work,
					cl.sys_timestamp,
                    em.mr_emp_name
				FROM mr_round_resive_thaipost_in as rrti
				LEFT JOIN mr_round as r on r.mr_round_id = rrti.mr_round_id
				LEFT JOIN mr_user as u on u.mr_user_id = rrti.mr_user_id
				LEFT JOIN mr_emp as e on e.mr_emp_id = u.mr_emp_id
				LEFT JOIN mr_work_post as wp on rrti.barcode = wp.num_doc
                LEFT JOIN mr_work_main as wm on wp.mr_work_main_id = wm.mr_work_main_id
                LEFT JOIN mr_confirm_log as cl on wm.mr_work_main_id = cl.mr_work_main_id
                LEFT JOIN mr_emp as em on cl.mr_emp_id = em.mr_emp_id
				Where rrti.active = 1
				';
		// ตรวจสอบว่าค่า start_date ไม่เป็นค่าว่าง
		if ($data_date['data1'] != '') {
			$sql .= ' AND rrti.sysdate >= ?';
			$sql .= ' AND rrti.sysdate <= ?';
			array_push($params, $data_date['data1'] . " 00:00:00");
			array_push($params, $data_date['data2'] . " 23:59:59");
		}
	
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// ดำเนินการคำสั่ง SQL
		$stmt->execute($params);
		// echo $sql;  // ย้ายบรรทัดนี้มาก่อนการ return ผลลัพธ์ เพื่อดูคำสั่ง SQL
		return $stmt->fetchAll();
	}
	
}
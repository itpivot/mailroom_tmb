<?php
require_once 'Pivot/Dao.php';


class Dao_Round_resive_work extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_round_resive_work');
    }
	
	function check_barcode($barcode)
	{
		$params = array();
		$sql = "
		SELECT mr_work_main_id,mr_type_work_id FROM `mr_work_main` WHERE `mr_work_barcode` LIKE ?
		";
		array_push($params, (string)$barcode);
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
        // Execute Query
        $stmt->execute($params);
		return $stmt->fetch();

	}

	
    
}
<?php
require_once 'Pivot/Dao.php';


class Dao_Sub_district extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_sub_districts');
    }
	public function getLocation($sub_district_code, $district_code, $province_code)
    {
        $params = array();
        
        $sql =
        '
            SELECT 
                sd.mr_sub_districts_id,
                sd.mr_sub_districts_name,
                d.mr_districts_id,
                d.mr_districts_name,
                p.mr_provinces_id,
                p.mr_provinces_name,
                sd.zipcode
            FROM '.$this->_tableName.' AS sd
                LEFT JOIN mr_districts AS d ON (sd.mr_districts_code = d.mr_districts_code)
                LEFT JOIN mr_provinces AS p ON (d.mr_provinces_code = p.mr_provinces_code)
            WHERE 
                sd.mr_sub_districts_id IS NOT NULL
        ';


        if(!empty($sub_district_code)) {
            $sql .= " AND sd.mr_sub_districts_code = ? ";
            array_push($params, $sub_district_code);
        }

        if(!empty($district_code)) {
            $sql .= " AND sd.mr_districts_code = ? ";
            array_push($params, $district_code);
        }

        if(!empty($province_code)) {
            $sql .= " AND p.mr_provinces_code = ? ";
            array_push($params, $province_code);
        }
        
        
        // prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
    }
	
}
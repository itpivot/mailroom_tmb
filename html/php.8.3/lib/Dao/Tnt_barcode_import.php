<?php
require_once 'Pivot/Dao.php';


class Dao_Tnt_barcode_import extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_tnt_barcode_import');
    }
    
	function getdatareport_out_tnt($datadate)
	{
		$params = array();
		$sql = 'SELECT 
        wm.mr_work_barcode,
        tbi.barcode_tnt,
        tbi.sys_date,
        r.mr_round_name,
        e.mr_emp_name as send_emp_name,
        e.mr_emp_code as send_emp_code,
        d.mr_department_name as send_department_name,
        d.mr_department_code as send_department_code,
        b.mr_branch_name as send_branch_name,
        b.mr_branch_code as send_branch_code,
        e_re.mr_emp_name as re_emp_name,
        e_re.mr_emp_code as re_emp_code ,
        d_re.mr_department_name as re_department_name,
        d_re.mr_department_code as re_department_code ,
        b_re.mr_branch_name as re_branch_name,
        b_re.mr_branch_code as re_branch_code
        FROM mr_work_main as wm
        LEFT JOIN mr_work_inout as wi on wm.mr_work_main_id = wi.mr_work_main_id
        LEFT JOIN mr_tnt_barcode_import as tbi on wi.mr_branch_id = tbi.mr_branch_id
        LEFT JOIN mr_round as r on wm.mr_round_id = r.mr_round_id
        -- ผู้ส่ง 
        LEFT JOIN mr_user as u on wm.mr_user_id = u.mr_user_id
        LEFT JOIN mr_emp as e on u.mr_emp_id = e.mr_emp_id
        LEFT JOIN mr_department as d on e.mr_department_id = d.mr_department_id
        LEFT JOIN mr_branch as b on e.mr_branch_id = b.mr_branch_id
        -- ผู้รับ 
        LEFT JOIN mr_emp as e_re on wi.mr_emp_id = e_re.mr_emp_id
        LEFT JOIN mr_department as d_re on e_re.mr_department_id = d_re.mr_department_id
        LEFT JOIN mr_branch as b_re on e_re.mr_branch_id = b_re.mr_branch_id
        LEFT JOIN mr_confirm_log as cl on wm.mr_work_main_id = cl.mr_work_main_id
        LEFT JOIN mr_emp as em on cl.mr_emp_id = em.mr_emp_id 
        where wm.mr_type_work_id = 2 ';

		if ($datadate['data1'] != '') {
			$sql .= ' AND tbi.sys_date >= ?';
			$sql .= ' AND tbi.sys_date <= ?';
			$sql .= ' AND wm.sys_timestamp >= ?';
			$sql .= ' AND wm.sys_timestamp <= ?';
			// $sql .= ' tbi.mr_tnt_barcode_import_id ';

			array_push($params, $datadate['data1'] . " 00:00:00");
			array_push($params, $datadate['data2'] . " 23:59:59");
            array_push($params, $datadate['data1'] . " 00:00:00"); 
            array_push($params, $datadate['data2'] . " 23:59:59");
		}
		$sql .= 'ORDER BY wm.mr_status_id DESC';
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function exportdatareport_out_tnt($datadate)
	{
		$params = array();
		$sql = 'SELECT 
        wm.mr_work_barcode,
        tbi.barcode_tnt,
        tbi.sys_date,
        r.mr_round_name,
        e.mr_emp_name as send_emp_name,
        e.mr_emp_code as send_emp_code,
        d.mr_department_name as send_department_name,
        d.mr_department_code as send_department_code,
        b.mr_branch_name as send_branch_name,
        b.mr_branch_code as send_branch_code,
        e_re.mr_emp_name as re_emp_name,
        e_re.mr_emp_code as re_emp_code ,
        d_re.mr_department_name as re_department_name,
        d_re.mr_department_code as re_department_code ,
        b_re.mr_branch_name as re_branch_name,
        b_re.mr_branch_code as re_branch_code,
        cl.sys_timestamp as confirm_timstamp,
        em.mr_emp_name
        FROM mr_work_main as wm
        LEFT JOIN mr_work_inout as wi on wm.mr_work_main_id = wi.mr_work_main_id
        LEFT JOIN mr_tnt_barcode_import as tbi on wi.mr_branch_id = tbi.mr_branch_id
        LEFT JOIN mr_round as r on wm.mr_round_id = r.mr_round_id
        -- ผู้ส่ง 
        LEFT JOIN mr_user as u on wm.mr_user_id = u.mr_user_id
        LEFT JOIN mr_emp as e on u.mr_emp_id = e.mr_emp_id
        LEFT JOIN mr_department as d on e.mr_department_id = d.mr_department_id
        LEFT JOIN mr_branch as b on e.mr_branch_id = b.mr_branch_id
        -- ผู้รับ 
        LEFT JOIN mr_emp as e_re on wi.mr_emp_id = e_re.mr_emp_id
        LEFT JOIN mr_department as d_re on e_re.mr_department_id = d_re.mr_department_id
        LEFT JOIN mr_branch as b_re on e_re.mr_branch_id = b_re.mr_branch_id
        LEFT JOIN mr_confirm_log as cl on wm.mr_work_main_id = cl.mr_work_main_id
        LEFT JOIN mr_emp as em on cl.mr_emp_id = em.mr_emp_id 
        where wm.mr_type_work_id = 2 ';

		if ($datadate['start_date'] != '') {
			$sql .= ' AND tbi.sys_date >= ?';
			$sql .= ' AND tbi.sys_date <= ?';
			$sql .= ' AND wm.sys_timestamp >= ?';
			$sql .= ' AND wm.sys_timestamp <= ?';
			// $sql .= ' tbi.mr_tnt_barcode_import_id ';

			array_push($params, $datadate['start_date'] . " 00:00:00");
			array_push($params, $datadate['end_date'] . " 23:59:59");
            array_push($params, $datadate['start_date'] . " 00:00:00"); 
            array_push($params, $datadate['end_date'] . " 23:59:59");
		}
		$sql .= 'ORDER BY wm.mr_status_id DESC';
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

}
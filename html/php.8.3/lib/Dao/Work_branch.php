<?php
require_once 'Pivot/Dao.php';


class Dao_Work_branch extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_branch');
    }
	
	
	
	function searchUserBranch( $data_search )
	{
		$params = array(); 
		$sql =
		'
			SELECT 
				t.mr_type_work_name,
				m.mr_user_id as m_user_id,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				m.mr_work_date_sent,
				m.mr_work_date_success,
				br.sys_timestamp as time_test,
				e.mr_emp_id as id_re,
				e.mr_emp_code as code_re,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				u.mr_user_id,
				x.mr_emp_name as name_se,
				x.mr_emp_lastname as lastname_se,
				t.mr_type_work_name,
				y.mr_department_name as d_name_se,
				y.mr_department_code as d_code_se,
				f.name as depart_floor_se,
				s.mr_status_id,
				s.mr_status_name,
				x.mr_emp_id as emp_se,
				e.mr_emp_id as emp_re,
				br.mr_user_id as b_mr_user_id,
				br.mr_branch_floor,
				m.mr_work_main_id,
				d.mr_department_name as d_name_re,
				d.mr_department_code as d_code_re,
				b.mr_branch_name as branch_name_re,
				b.mr_branch_code as branch_code_re,
				b2.mr_branch_name as branch_name_re2,
				b2.mr_branch_code as branch_code_re2,
				bz.mr_branch_name as branch_name_se,
				bz.mr_branch_code as branch_code_se,
				u2.mr_user_id as user_id_re2,
				u2.mr_user_role_id as role_id_re2,
				
				e2.mr_emp_id as id_re2,
				e2.mr_emp_code as code_re2,
				e2.mr_emp_name as name_re2,
				e2.mr_emp_lastname as lastname_re2,
				b3.mr_branch_id as branch_id_re3,
				b3.mr_branch_name as branch_name_re3,
				b3.mr_branch_code as branch_code_re3
			FROM mr_work_inout br
			left join mr_work_main m on ( m.mr_work_main_id = br.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = br.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id )
			Left join mr_branch b2 on ( b2.mr_branch_id = br.mr_branch_id )
	
			
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = x.mr_floor_id )
			Left join mr_branch bz on ( bz.mr_branch_id = x.mr_branch_id )

			Left join mr_user u2 on (u2.mr_user_id = br.mr_user_id)
			Left join mr_emp e2 on (e2.mr_emp_id = u2.mr_emp_id)
			Left join mr_branch b3 on ( b3.mr_branch_id = e2.mr_branch_id )
			WHERE
				m.mr_status_id <> 0
			AND (m.mr_user_id = ? or br.mr_emp_id = ? )
			AND m.mr_type_work_id in (2,3)
		';

		array_push($params, (int)$data_search['sender']);
		array_push($params, (int)$data_search['receiver']);

		if( $data_search['mr_type_work_id'] != "" ){
			$sql .= ' AND m.mr_type_work_id = ? ';
			array_push($params, (int)$data_search['mr_type_work_id']);
		}
		
		if( $data_search['barcode'] != "" ){
			$sql .= ' AND m.mr_work_barcode LIKE ? ';
			array_push($params, (string)'%'.$data_search['barcode'].'%');
		}
		
		
		if( $data_search['status'] != "" ){
			$sql .= ' AND m.mr_status_id = ? ';
			array_push($params, (int)$data_search['status']);
		}
		
		
		if( $data_search['check_emp'] != "empty" ){
			if( $data_search['emp_type'] == "sender" ){
				$sql .= ' AND ( m.mr_user_id = ? ) ';
				array_push($params, (int)$data_search['sender']);
			}else {
					
				$sql .= ' AND br.mr_emp_id = ? ';
				array_push($params, (int)$data_search['receiver']);
			}
		}
		// if( $data_search['sender'] != ""  ){
		// 	$sql .= ' AND x.mr_emp_code = "'.$data_search['sender'].'" AND e.mr_emp_id ="'.$data_search['mr_emp_id'].'"';
		// }
		
		// if( $data_search['receiver'] != ""  ){
		// 	$sql .= ' AND e.mr_emp_code = "'.$data_search['receiver'].'" AND u.mr_user_id ="'.$data_search['user_id'].'"';
		// }
		
		
		if( $data_search['department_id'] != "" ){
			if( $data_search['department_type'] == "sender" ){
				$sql .= ' AND d.mr_department_id = ? ';
				array_push($params, (int)$data_search['department_id']);

			}else if ( $data_search['department_type'] == "receiver" ){
				$sql .= ' AND y.mr_department_id = ? ';
				array_push($params, (int)$data_search['department_id']);
			}
		}
		
		if( $data_search['branch_id'] != "" ){
			if( $data_search['branch_type'] == "sender" ){
				$sql .= ' AND b.mr_branch_id = ? ';
				array_push($params, (int)$data_search['branch_id']);

			}else if ( $data_search['branch_type'] == "receiver" ){
				$sql .= ' AND bz.mr_branch_id = ? ';
				array_push($params, (int)$data_search['branch_id']);
			}
		}
		
		
		
		
		if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
			//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
			
			$sql .= '  AND ( m.mr_work_date_sent BETWEEN ? AND ? )';
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
			array_push($params, (string)$data_search['start_date']);
			array_push($params, (string)$data_search['end_date']);
		}
		$sql .= '
		order by m.mr_work_date_sent desc
		limit 0,1000';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();

	}
	
	

	function getWorkBranchById($id)
	{
		$params = array(); 
		$sql =
		'
			select 
				wb.*
			from '.$this->_tableName.' as wb
			where wb.mr_work_main_id = '.$id.' 
		';
		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
	
}
<?php
require_once 'Pivot/Dao.php';
class Dao_work_byhand_price extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_byhand_price');
    }

    function getpriceByhandAll()
	{
		$params = array(); 
		$sql =
		'
        SELECT 
            bhp.mr_work_byhand_price_id,
            bhp.sys_time,
            bhp.price,
            bht.payment_type_name,
            bhz.byhand_zone_name
       FROM mr_work_byhand_price bhp 
       left join mr_work_byhand_zone bhz on(bhp.mr_work_byhand_zone_id = bhz.mr_work_byhand_zone_id)
       left join mr_work_byhand_payment_type bht on (bhp.mr_work_byhand_payment_type_id = bht.mr_work_byhand_payment_type_id)
		order by  bht.payment_type_name,bhz.byhand_zone_name asc
       ';
    //array_push($params, floatval($type_id));
	//array_push($params, floatval($weight));

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
}
<?php
require_once 'Pivot/Dao.php';
class Dao_work_byhand_zone_location extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_byhand_zone_location');
    }

    function getzone_locationAll($work_byhand_zone_id=null)
	{
		$params = array(); 
		$sql =
		'
        SELECT 
            zl.sys_time ,
            zl.mr_work_byhand_zone_location_id,
            sd.mr_sub_districts_name,
            sd.zipcode,
            z.byhand_zone_name
        FROM mr_work_byhand_zone_location zl
        LEFT JOIN mr_work_byhand_zone z on(zl.mr_work_byhand_zone_id = z.mr_work_byhand_zone_id)
        LEFT JOIN mr_sub_districts sd on(zl.mr_sub_districts_id = sd.mr_sub_districts_id)
       ';
    if($work_byhand_zone_id!= ''){
        $sql .=
		' where zl.mr_work_byhand_zone_id = ?';
        array_push($params, intval($work_byhand_zone_id));
    }
    
    
	//array_push($params, floatval($weight));

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
}
<?php
require_once 'Pivot/Dao.php';


class Dao_Work_log extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_log');
    }
	
	function SLAlog( $id )
	{
		$params = array();
		$sql =
		'
			SELECT 
				l.*
			FROM mr_work_log l
			WHERE
				l.mr_work_main_id = ?
				and l.mr_status_id in(2,5)
		';
		
		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function getWorkLogById($id)
	{
		$params = array();
		$sql =
		'
			select 
				l.mr_work_log_id,
				l.mr_status_id,
				l.sys_timestamp,
				e.mr_emp_id,
				e.mr_emp_name,
				e.mr_emp_lastname,
				e.mr_emp_code
			from '.$this->_tableName.' as l
				left join mr_user u ON ( u.mr_user_id = l.mr_user_id )
				left join mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			where
				l.mr_work_main_id = ?
			order by l.mr_work_log_id asc
		';

		array_push($params, (int)$id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function getWorkLogstarff($id)
	{
		$params = array();
		$sql =
		'
			select 
				l.mr_work_log_id,
				l.mr_status_id,
				l.sys_timestamp,
				e.mr_emp_id,
				e.mr_emp_name,
				e.mr_emp_lastname,
				e.mr_emp_code
			from '.$this->_tableName.' as l
				left join mr_user u ON ( u.mr_user_id = l.mr_user_id )
				left join mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			where
				l.mr_work_main_id = ?
				and (u.mr_user_role_id NOT IN (2,5) or u.mr_user_id = 984)
			order by l.mr_work_log_id asc
		';

		array_push($params, (int)$id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getdatareport_tnt($datadate)
	{
		$params = array();
		$sql = 'SELECT 
		wi.tnt_no, 
		wl.sys_timestamp,
		wm.mr_work_barcode,
		r.mr_round_name,
		e.mr_emp_name as send_emp_name,
		e.mr_emp_lastname as send_emp_lastname,
		e.mr_emp_code as send_emp_code,
		d.mr_department_code as send_department_code,
		d.mr_department_name as send_department_name,
		b.mr_branch_code as send_branch_code,
		b.mr_branch_name as send_branch_name,
		e_re.mr_emp_name as re_emp_name,
		e_re.mr_emp_lastname as re_emp_lastname,
		e_re.mr_emp_code as re_emp_code,
		d_re.mr_department_code as re_department_code,
		d_re.mr_department_name re_department_name,
		b_re.mr_branch_code as re_branch_code,
		b_re.mr_branch_name as re_branch_name,
		wl.remark,
		COALESCE(cl.sys_timestamp, wl.sys_timestamp) AS confirm_timestamp,
    	COALESCE(em.mr_emp_name, e.mr_emp_name) AS emp_name
  		FROM mr_work_log AS wl
  		LEFT JOIN mr_work_main AS wm ON wl.mr_work_main_id = wm.mr_work_main_id
  		LEFT JOIN mr_work_inout AS wi ON wm.mr_work_main_id = wi.mr_work_main_id
  		LEFT JOIN mr_round AS r ON wl.mr_round_id = r.mr_round_id
        LEFT JOIN mr_user as u ON wm.mr_user_id = u.mr_user_id
        LEFT JOIN mr_emp AS e ON u.mr_emp_id = e.mr_emp_id
        LEFT JOIN mr_department as d on e.mr_department_id = d.mr_department_id
        LEFT JOIN mr_branch as b on e.mr_branch_id = b.mr_branch_id
        
        LEFT JOIN mr_emp as e_re on wi.mr_emp_id = e_re.mr_emp_id
        LEFT JOIN mr_department as d_re on e_re.mr_department_id = d_re.mr_department_id
        LEFT JOIN mr_branch as b_re on e_re.mr_branch_id = b_re.mr_branch_id
		LEFT JOIN mr_confirm_log as cl on wm.mr_work_main_id = cl.mr_work_main_id
		LEFT JOIN mr_emp as em on cl.mr_emp_id = em.mr_emp_id
		LEFT JOIN mr_user as uwl on wl.mr_user_id = uwl.mr_user_id
		LEFT JOIN mr_emp as ewl on uwl.mr_emp_id = ewl.mr_emp_id
		LEFT JOIN mr_status as s on wm.mr_status_id = s.mr_status_id
		WHERE wl.remark LIKE "%เลขที่ TNT : %" ';

		if ($datadate['data1'] != '') {
			$sql .= ' AND wl.sys_timestamp >= ?';
			$sql .= ' AND wl.sys_timestamp <= ?';
			array_push($params, $datadate['data1'] . " 00:00:00");
			array_push($params, $datadate['data2'] . " 23:59:59");
		}
		$sql .= 'AND s.mr_status_id in (5,12)';
		$sql .= ' GROUP BY wi.tnt_no ';
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function expoetdatareport_tnt($data_search)
	{
		$params = array();
		$sql = 'SELECT 
		wi.tnt_no, 
		wl.sys_timestamp,
		wm.mr_work_barcode,
		r.mr_round_name,
		e.mr_emp_name as send_emp_name,
		e.mr_emp_lastname as send_emp_lastname,
		e.mr_emp_code as send_emp_code,
		d.mr_department_code as send_department_code,
		d.mr_department_name as send_department_name,
		b.mr_branch_code as send_branch_code,
		b.mr_branch_name as send_branch_name,
		e_re.mr_emp_name as re_emp_name,
		e_re.mr_emp_lastname as re_emp_lastname,
		e_re.mr_emp_code as re_emp_code,
		d_re.mr_department_code as re_department_code,
		d_re.mr_department_name re_department_name,
		b_re.mr_branch_code as re_branch_code,
		b_re.mr_branch_name as re_branch_name,
		wl.remark,
		cl.sys_timestamp as confirm_timestamp,
        em.mr_emp_name,
		COALESCE(cl.sys_timestamp, wl.sys_timestamp) AS confirm_timestamp,
    	COALESCE(em.mr_emp_name, e.mr_emp_name) AS emp_name
  		FROM mr_work_log AS wl
  		LEFT JOIN mr_work_main AS wm ON wl.mr_work_main_id = wm.mr_work_main_id
  		LEFT JOIN mr_work_inout AS wi ON wm.mr_work_main_id = wi.mr_work_main_id
  		LEFT JOIN mr_round AS r ON wl.mr_round_id = r.mr_round_id
        LEFT JOIN mr_user as u ON wm.mr_user_id = u.mr_user_id
        LEFT JOIN mr_emp AS e ON u.mr_emp_id = e.mr_emp_id
        LEFT JOIN mr_department as d on e.mr_department_id = d.mr_department_id
        LEFT JOIN mr_branch as b on e.mr_branch_id = b.mr_branch_id
        
        LEFT JOIN mr_emp as e_re on wi.mr_emp_id = e_re.mr_emp_id
        LEFT JOIN mr_department as d_re on e_re.mr_department_id = d_re.mr_department_id
        LEFT JOIN mr_branch as b_re on e_re.mr_branch_id = b_re.mr_branch_id
		LEFT JOIN mr_confirm_log as cl on wm.mr_work_main_id = cl.mr_work_main_id
        LEFT JOIN mr_emp as em on cl.mr_emp_id = em.mr_emp_id
		LEFT JOIN mr_status as s on wm.mr_status_id = s.mr_status_id
		WHERE wl.remark LIKE "%เลขที่ TNT : %" ';

		if ($data_search['start_date'] != '') {
			$sql .= ' AND wl.sys_timestamp >= ?';
			$sql .= ' AND wl.sys_timestamp <= ?';
			array_push($params, $data_search['start_date'] . " 00:00:00");
			array_push($params, $data_search['end_date'] . " 23:59:59");
		}
		$sql .= 'AND s.mr_status_id in (5,12)';
		$sql .= ' GROUP BY wm.mr_work_main_id ';
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	
	
	
	
}
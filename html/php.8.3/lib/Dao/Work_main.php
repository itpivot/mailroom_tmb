<?php
require_once 'Pivot/Dao.php';


class Dao_Work_main extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_main');
    }
	
	function landom_Barcode(){
		 $sql =
            '
			SELECT
			CONCAT("TM",DATE_FORMAT(current_date, "%d%m%y"), FLOOR( 1000 + ( RAND( ) *8999 ) )) as random_number
		FROM mr_work_main
		WHERE "random_number" NOT IN (SELECT mr_work_barcode FROM mr_work_main )
		LIMIT 1
        ';

        $barcode = $this->_db->fetchOne($sql);
        // $barcode =  $sql;
        if (empty($barcode)) {
            $sql =
                '
                SELECT
                    CONCAT("TM",DATE_FORMAT(current_date, "%d%m%y"), FLOOR( 1000 + ( RAND( ) *8999 ) )) as random_number
                LIMIT 1
            ';

            $barcode = $this->_db->fetchOne($sql);
        }
        return $barcode;
	
	}
	function checkBarcode($barcode)
	{
		$params = array();

		$sql =
		'
			SELECT 
				mr_work_barcode
			FROM mr_work_main
			WHERE
				mr_work_barcode like ?
				and mr_status_id != 6
			ORDER BY mr_work_barcode DESC
		';

		array_push($params, trim($barcode)."%");

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetch();
		//echo $sql;
        // return $this->_db->fetchOne($sql);
	}
	
	
	function getIDbyBarcode( $barcode )
	{
		$params = array();
		$sql =
		'
			SELECT 
				*
			FROM mr_work_main
			WHERE
				mr_work_barcode = ?
		';
		array_push($params, (string)$barcode);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	function getBarcodeByid($id)
	{
		$params = array();
		$sql =
		'
			SELECT
				mr_work_barcode,
				mr_user_id
			FROM mr_work_main
			WHERE 
				mr_work_main_id = ?
		';
		array_push($params, (int)$id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	
	
	function getMonthsSend( $year,$user_id )
	{
		$params = array();
		$sql =
		'
			select 
			DISTINCT( date_format(date(m.sys_timestamp),"%m")) as month,
			date_format(date(m.sys_timestamp),"%Y")  as year 
			from mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			where date_format(date(m.sys_timestamp),"%Y") = ?
			AND m.mr_user_id = ?
			order by m.sys_timestamp ASC
		';
		array_push($params, (string)$year);
		array_push($params, (int)$user_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	
	function getMonthsReceive( $year, $user_id )
	{
		$params = array();
		$sql =
		'
			select 
			DISTINCT( date_format(date(m.sys_timestamp),"%m")) as month,
			date_format(date(m.sys_timestamp),"%Y")  as year 
			from mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			where date_format(date(m.sys_timestamp),"%Y") = ?
			AND i.mr_emp_id = ?
			order by m.sys_timestamp ASC
		';
		array_push($params, (string)$year);
		array_push($params, (int)$user_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	
	function getDatasByEmail($mr_work_main_id)
	{
		//$mr_work_main_id = 5 ;
		
		$params = array();
		$sql =
		'
			SELECT
				wm.mr_work_barcode as barcode,
				wm.mr_user_id as send_user_id,
				e.mr_emp_name as send_name,
				e.mr_emp_lastname as send_lastname,
				e.mr_emp_email as send_email,
				i.mr_user_id as reciever_user_id,
				i.mr_emp_id as receiver_emp_id,
				x.recheck_user_id ,  
				x.reciever_name,
				x.reciever_lastname
			FROM mr_work_main as wm
			LEFT JOIN mr_user as u on u.mr_user_id = wm.mr_user_id 
			LEFT JOIN mr_emp as e on e.mr_emp_id = u.mr_emp_id
			LEFT JOIN mr_work_inout as i on i.mr_work_main_id = wm.mr_work_main_id

			JOIN (
				SELECT
					i.mr_work_main_id,
					i.mr_user_id as recheck_user_id,
					ex.mr_emp_id,
					ex.mr_emp_name as reciever_name,
					ex.mr_emp_lastname as reciever_lastname
				FROM mr_work_inout as i
				LEFT JOIN mr_emp as ex on ex.mr_emp_id = i.mr_emp_id
				LEFT JOIN mr_user as u on u.mr_user_id = i.mr_user_id 
				WHERE
					mr_work_main_id = ?
				
			)x on x.mr_work_main_id = i.mr_work_main_id
			WHERE 
				wm.mr_work_main_id = ?
		';

		array_push($params, (int)$mr_work_main_id);
		array_push($params, (int)$mr_work_main_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetch();
	}
	

	//  ================================== MARCH ============================

	function getReportSendMonthly( $year)
	{
		$params = array();
		$sql =
		'
			select 
			DISTINCT( date_format(date(m.sys_timestamp),"%m")) as month,
			date_format(date(m.sys_timestamp),"%Y")  as year 
			from mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			where date_format(date(m.sys_timestamp),"%Y") = ?
			order by m.sys_timestamp ASC
		';

		array_push($params, (string)$year);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	
	function getReportSendMonthlySLA( $year)
	{
		$params = array();
		$sql =
		'
			select 
			DISTINCT( date_format(date(m.sys_timestamp),"%m")) as month,
			date_format(date(m.sys_timestamp),"%Y")  as year 
			from mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			Join(
					select
						*
					from  mr_work_log
					where mr_status_id = 2
				)r on ( r.mr_work_main_id = m.mr_work_main_id )
			Join(
					select
						*
					from  mr_work_log
					where mr_status_id = 5
				)l on ( l.mr_work_main_id = m.mr_work_main_id )
			where date_format(date(m.sys_timestamp),"%Y") = ?
			order by m.sys_timestamp ASC
		';

		array_push($params, (string)$year);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	

	function getReportSendDaily()
	{
		$sql =
		'
			select 
			DISTINCT( date_format(date(m.sys_timestamp),"%Y-%m-%d")) as date
			from mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			order by m.sys_timestamp ASC
		';
		//echo $sql;
        return $this->_db->fetchAll($sql);
	}
	
	function getReceiveMailroom2($user_id)
	{
		$startDay = Date("Y-m-d 00:00:00", strtotime(date('Y-m-d')." -7 Day"));// 2013-01-31
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				i.sys_timestamp,
				e.mr_emp_name,
				e.mr_emp_lastname,
				e.mr_emp_tel ,
				e.mr_emp_mobile,
				d.mr_department_name,
				f.name as mr_department_floor,
				s.mr_status_name,
				i.mr_status_receive,
				z.*
			FROM mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = m.mr_floor_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = m.mr_floor_id )
			WHERE
				m.mr_status_id in(1,7,8) AND 
				m.mr_type_work_id in(1,2,3) AND
				z.mr_user_id = ? AND 
				(
					i.mr_status_receive = 1  or 
					i.mr_status_receive is null or 
					(
						i.mr_status_receive = 2
						and m.sys_timestamp >= ?
					)
				)
			group by m.mr_work_main_id
			ORDER BY i.mr_status_receive ASC, m.mr_work_date_sent DESC
			
		';

		array_push($params, (int)$user_id);
		array_push($params, (string)$startDay);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}


	// march
	function getMonthBranchSuccess($user_id, $years = null) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				DISTINCT 
					MONTH(mr_work_date_sent) as month_success,
					YEAR(mr_work_date_sent) as year_success
			FROM '.$this->_tableName.' 
			WHERE mr_user_id = ? AND mr_work_date_sent IS NOT NULL
		';

		array_push($params, (int)$user_id);

		if(!empty($years)) {
			$sql .= ' AND  YEAR(mr_work_date_sent) = ? ';
			array_push($params, (string)$years);
		}

		$sql .= " ORDER BY YEAR(mr_work_date_sent), MONTH(mr_work_date_sent) ASC";
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}

	function getDataBranchSuccess($users_id, $month, $year)
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_date_success,
				m.mr_topic,
				m.mr_work_remark,

				e.mr_emp_code as sender_code,
				concat(e.mr_emp_name," ", e.mr_emp_lastname) as sender_name,
				e.mr_emp_tel as sender_tel,
				e.mr_emp_mobile as sender_mobile,
				
				b.mr_branch_id as sender_branch_id,
				b.mr_branch_name as sender_branch_name,
				b.mr_branch_code as sender_branch_code,
				e.mr_branch_floor as sender_floor,
				
				f.mr_floor_id as sender_floor_id,
				f.name as sender_floor_name,
				f.floor_level as sender_floor_level,
				f.floor_builder as sender_floor_builder,
				
				wb.mr_branch_floor as receive_floor,
				f3.name as receive_floor2,
				e2.mr_branch_floor as receive_floor3,
				f2.name as receive_floor_name,
				
				
				d.mr_department_code as re_department_code,
				d.mr_department_name as re_department_name,
				e2.mr_emp_code as receive_code,
				concat(e2.mr_emp_name," ",e2.mr_emp_lastname) as receive_name,
				e2.mr_emp_tel as receive_tel,
				e2.mr_emp_mobile as receive_mobile,

				e3.mr_emp_code as real_receive_code,
				concat(e3.mr_emp_name," ",e3.mr_emp_lastname) as real_receive_name,
				e3.mr_emp_tel as real_receive_tel,
				e3.mr_emp_mobile as real_receive_mobile,

				b2.mr_branch_id as receive_branch_id,
				b2.mr_branch_name as receive_branch_name,
				b2.mr_branch_code as receive_branch_code,
				
				f2.mr_floor_id as receive_floor_id,
				f2.floor_level as receive_floor_level,
				f2.floor_builder as receive_floor_builder,

				s.mr_status_id,
				s.mr_status_name,

				tw.mr_type_work_id,
				tw.mr_type_work_name

			FROM '.$this->_tableName.' m
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )

				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )

				LEFT JOIN mr_user u2 ON ( u2.mr_user_id = wb.mr_user_id )
				LEFT JOIN mr_emp e3 ON ( e3.mr_emp_id = u2.mr_emp_id )

				LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
				LEFT JOIN mr_status s ON ( s.mr_status_id = m.mr_status_id )
				
				LEFT JOIN mr_branch b ON ( b.mr_branch_id = m.mr_branch_id )
				LEFT JOIN mr_branch b2 ON ( b2.mr_branch_id = wb.mr_branch_id )
				
				LEFT JOIN mr_floor f ON ( f.mr_floor_id = m.mr_floor_id )
				LEFT JOIN mr_floor f2 ON ( f2.mr_floor_id = wb.mr_floor_id )
				LEFT JOIN mr_floor f3 ON ( f3.mr_floor_id = e2.mr_floor_id )
				LEFT JOIN mr_department d ON ( d.mr_department_id = e2.mr_department_id )
				

			WHERE m.mr_user_id = ? and MONTH(m.sys_timestamp) = ? and YEAR(m.sys_timestamp) = ?
		';

		array_push($params, (int)$users_id);
		array_push($params, (string)$month);
		array_push($params, (string)$year);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}


	function getWorkMainById($id = null)
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_date_success,
				m.mr_topic,
				m.mr_work_remark,

				e.mr_emp_id as sender_id,
				e.mr_emp_code as sender_code,
				concat(e.mr_emp_name," ", e.mr_emp_lastname) as sender_name,
				e.mr_emp_tel as sender_tel,
				e.mr_emp_mobile as sender_mobile,
				b.mr_branch_id as sender_branch_id,
				b.mr_branch_name as sender_branch_name,
				b.mr_branch_code as sender_branch_code,
				f.mr_floor_id as sender_floor_id,
				f.name as sender_floor_name,
				f.floor_level as sender_floor_level,
				f.floor_builder as sender_floor_builder,
				
				e2.mr_emp_id as receive_id,
				e2.mr_emp_code as receive_code,
				concat(e2.mr_emp_name," ",e2.mr_emp_lastname) as receive_name,
				e2.mr_emp_tel as receive_tel,
				e2.mr_emp_mobile as receive_mobile,

				e3.mr_emp_id as real_receive_id,
				e3.mr_emp_code as real_receive_code,
				concat(e3.mr_emp_name," ",e3.mr_emp_lastname) as real_receive_name,
				e3.mr_emp_tel as real_receive_tel,
				e3.mr_emp_mobile as real_receive_mobile,

				b2.mr_branch_id as receive_branch_id,
				b2.mr_branch_name as receive_branch_name,
				b2.mr_branch_code as receive_branch_code,
				f2.mr_floor_id as receive_floor_id,
				f2.name as receive_floor_name,
				f2.floor_level as receive_floor_level,
				f2.floor_builder as receive_floor_builder,
				u.mr_user_role_id,

				s.mr_status_id,
				s.mr_status_name,

				tw.mr_type_work_id,
				wb.mr_branch_floor,
				wb.mr_floor_id,
				tw.mr_type_work_name,

				b3.mr_branch_id as receive_branch_id3,
				b3.mr_branch_name as receive_branch_name3,
				b3.mr_branch_code as receive_branch_code3

			FROM '.$this->_tableName.' m
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )

				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				LEFT JOIN mr_user u2 ON ( u2.mr_user_id = wb.mr_user_id )
				LEFT JOIN mr_emp e3 ON ( e3.mr_emp_id = u2.mr_emp_id )

				LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
				LEFT JOIN mr_status s ON ( s.mr_status_id = m.mr_status_id )
				LEFT JOIN mr_branch b ON ( b.mr_branch_id = m.mr_branch_id )
				LEFT JOIN mr_branch b2 ON ( b2.mr_branch_id = wb.mr_branch_id )
				LEFT JOIN mr_floor f ON ( f.mr_floor_id = m.mr_floor_id )
				LEFT JOIN mr_floor f2 ON ( f.mr_floor_id = wb.mr_floor_id )

				Left join mr_branch b3 on ( b3.mr_branch_id = e3.mr_branch_id )
				
			WHERE m.mr_work_main_id = ?
		';

		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetch();
	}


	// MOS //
	// MOS //
	function select($sql){
		if($sql != "" and $sql = null){
			return $this->_db->fetchRow($sql);
		}else{
			return false;
		}
	}
	function usergetDataAll_receive($user_id)
	{
		$params = array();
		$sql =
		'
			select 
				m.mr_work_main_id,
				m2.mr_work_barcode,
				m2.sys_timestamp,
				rs.tnt_tracking_no,
				e.mr_emp_name as send_name,
				e.mr_emp_lastname  as send_lname,
				e2.mr_emp_name as re_name,
				e2.mr_emp_lastname  as re_lname,
				st.mr_status_name,
				st.mr_status_id,
				u2.mr_user_id
			from (
					select 
						mr_work_main_id,
						mr_user_id
					from mr_work_main 
					where 
						mr_user_id = ? and 
						mr_type_work_id in(1,2,3) and 
						mr_status_id in(1,2,3,4,7,8,9,10,11,13,14) and
						DATE(sys_timestamp) < curdate()
				union all 
					select 
						mr_work_main_id,
						mr_user_id
					from mr_work_main 
					where mr_user_id = ? and 
					mr_type_work_id in(1,2,3) and 
					DATE(sys_timestamp) = curdate()
				union all 		
					select 
						io.mr_work_main_id,
						u2.mr_user_id 
					from mr_work_inout as io 
						LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = io.mr_emp_id )
						LEFT JOIN mr_user u2 ON ( u2.mr_emp_id = e2.mr_emp_id )
						LEFT JOIN mr_work_main m on (m.mr_work_main_id = io.mr_work_main_id)
					where
						u2.mr_user_id = ? and 
						m.mr_status_id in(1,2,3,4,7,8,9,10,11,13,14) and
						DATE(io.sys_timestamp) <= curdate()
			) as m 
			LEFT JOIN mr_work_main m2 on (m2.mr_work_main_id = m.mr_work_main_id)
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id)
			LEFT JOIN mr_user u ON ( u.mr_user_id = m2.mr_user_id )
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
			LEFT JOIN mr_round_send_work rs ON ( rs.mr_work_main_id = m.mr_work_main_id )
			LEFT JOIN mr_status st ON (st.mr_status_id = m2.mr_status_id)
			LEFT JOIN mr_user u2 ON ( u2.mr_emp_id = e2.mr_emp_id )
			where u2.mr_user_id = ?
			group by m2.mr_work_main_id
		';

		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	
	
	function seart_receive($barcode=null)
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.*,
				e.mr_emp_name as send_name,
				e.mr_emp_lastname  as send_lname,
				e2.mr_emp_name as re_name,
				e2.mr_emp_lastname  as re_lname,
				d.mr_department_code as re_mr_department_code,
				d.mr_department_name as re_mr_department_name,
				b.mr_branch_code as re_mr_branch_code,
				b.mr_branch_name as re_mr_branch_name,
				st.mr_status_name,
				st.mr_status_id,
				rs.tnt_tracking_no,
				wb.sys_timestamp as log_sys_timestamp
			FROM '.$this->_tableName.' m 
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				LEFT JOIN mr_status st using(mr_status_id)
				left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				LEFT JOIN mr_user u2 ON ( u2.mr_emp_id = e2.mr_emp_id )
				LEFT JOIN mr_round_send_work rs ON ( rs.mr_work_main_id = m.mr_work_main_id )
				Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
				Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
				where m.mr_work_barcode like ?
				group by m.mr_work_main_id 
				order by m.mr_status_id asc
			

		';

		array_push($params, (string)$barcode);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	function usergetData_receive_emp($user_id)
	{

		$params = array();
		
		$sql =
		'
			select 
				m.mr_work_main_id,
				m2.mr_work_barcode,
				m2.sys_timestamp,
				m2.mr_type_work_id,
				rs.tnt_tracking_no,
				tw.mr_type_work_name,
				e.mr_emp_name as send_name,
				e.mr_emp_lastname  as send_lname,
				e2.mr_emp_name as re_name,
				e2.mr_emp_lastname  as re_lname,
				st.mr_status_name,
				st.mr_status_id,
				u2.mr_user_id,
				wp.mr_cus_name as wp_cus_name,
				wp.mr_cus_lname as wp_cus_lname,
				wp.num_doc as wp_num_doc,
				wbh.mr_cus_name as wbh_cus_name,
				wbh.mr_cus_lname as wbh_cus_lname,
				wbh.work_barcode_re as work_barcode_re
			from (
					select 
						mr_work_main_id,
						mr_user_id
					from mr_work_main 
					where 
						mr_user_id = ? and 
						mr_type_work_id in(1,2,3) and 
						mr_status_id in(1,2,3,4,7,8,9,10,11,13,14) and
						DATE(sys_timestamp) < curdate()
				union all 
					select 
						mr_work_main_id,
						mr_user_id
					from mr_work_main 
					where mr_user_id = ? and 
					mr_type_work_id in(1,2,3) and 
					DATE(sys_timestamp) = curdate()
				union all 		
					select 
						io.mr_work_main_id,
						u2.mr_user_id 
					from mr_work_inout as io 
						LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = io.mr_emp_id )
						LEFT JOIN mr_user u2 ON ( u2.mr_emp_id = e2.mr_emp_id )
						LEFT JOIN mr_work_main m on (m.mr_work_main_id = io.mr_work_main_id)
					where
						u2.mr_user_id = ? and 
						m.mr_status_id in(1,2,3,4,7,8,9,10,11,13,14) and
						DATE(io.sys_timestamp) <= curdate()
			) as m 
			LEFT JOIN mr_work_main m2 on (m2.mr_work_main_id = m.mr_work_main_id)
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id)
			LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m2.mr_type_work_id)
			LEFT JOIN mr_work_post wp ON ( m2.mr_work_main_id = wp.mr_work_main_id)
			LEFT JOIN mr_work_byhand wbh ON ( m2.mr_work_main_id = wbh.mr_work_main_id)
			LEFT JOIN mr_user u ON ( u.mr_user_id = m2.mr_user_id )
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
			LEFT JOIN mr_round_send_work rs ON ( rs.mr_work_main_id = m.mr_work_main_id )
			LEFT JOIN mr_status st ON (st.mr_status_id = m2.mr_status_id)
			LEFT JOIN mr_user u2 ON ( u2.mr_emp_id = e2.mr_emp_id )
			where u2.mr_user_id = ?
			
			order by  m2.mr_work_main_id desc
			limit 0,1000

		';
		// ใส่ group by แล้วไม่ได้ group by m2.mr_work_main_id

		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
        // Execute Query
        $stmt->execute($params);
		return $stmt->fetchAll();

		// return $this->_db->fetchAll($sql);
	}
	
	function BranchgetDataAll_send($mr_user_id)
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.*,
				e.mr_emp_name as send_name,
				e.mr_emp_lastname  as send_lname,
				e2.mr_emp_name as re_name,
				e2.mr_emp_lastname  as re_lname,
				d.mr_department_code as re_mr_department_code,
				d.mr_department_name as re_mr_department_name,
				b.mr_branch_code as re_mr_branch_code,
				b.mr_branch_name as re_mr_branch_name,
				st.mr_status_name
			FROM '.$this->_tableName.' m 
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
				Left join mr_branch b on ( b.mr_branch_id = wb.mr_branch_id )
				Left join mr_status st on ( st.mr_status_id = m.mr_status_id )
				
				
				
				where m.mr_user_id = ?
				and m.mr_status_id in(7,8)
				group by m.mr_work_main_id 
			

		';

		array_push($params, (int)$mr_user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
        // Execute Query
        $stmt->execute($params);
		return $stmt->fetchAll();
	}
	

	// MARCH
	function getReceiveBranch($status, $workType, $hub_id = null, $txt = null) 
	{
		$fixDay = date('Y-m-d 00:00:00', strtotime('-3 month'));// 2013-01-31
		$startDay = Date("Y-m-d 00:00:00", strtotime(date('Y-m-d')." -7 Day"));// 2013-01-31
		$params = array();
		$in_status_params = array();
		$in_workType_params = array();

		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}

		$sql = 
		'
			select 
				m.mr_work_main_id,
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
				m.mr_round_id,
				b.mr_branch_id,
				b.mr_branch_name,
				b.mr_branch_code,
				b.mr_branch_type,
				h.mr_hub_id,
				h.mr_hub_name,
				s.mr_status_id,
				s.mr_status_name,
				s.mr_type_work_id,
				d.mr_department_id,
				d.mr_department_name,
				d.mr_department_code,
				d.mr_department_floor,
				es.mr_emp_name,
				es.mr_emp_lastname,
				es.mr_emp_code,
				es.mr_emp_tel,
				es.mr_emp_mobile,
				es.mr_emp_email,
				es.mr_workplace,
				es.mr_workarea,
				io.mr_status_send,
				io.mr_status_receive
			from mr_work_main m
				left join mr_work_inout as io on (io.mr_work_main_id = m.mr_work_main_id)
				left join mr_user as u on ( u.mr_user_id = m.mr_user_id )
				left join mr_emp as es on (es.mr_emp_id = u.mr_emp_id)
				left join mr_department d on (d.mr_department_id = es.mr_department_id)
				left join mr_branch as b on (b.mr_branch_id = m.mr_branch_id)
				left join mr_hub as  h on (h.mr_hub_id = b.mr_hub_id)
				left join mr_status as s on ( s.mr_status_id = m.mr_status_id )
				left join mr_floor as f on (f.mr_floor_id = m.mr_floor_id) 	
			where 
		';
			$sql .= "  
			(
				io.mr_status_receive = 1  or 
				io.mr_status_receive is null or 
				(
					io.mr_status_receive = 2
					and m.sys_timestamp >= ?
				)
			)
			and m.sys_timestamp >= '".$fixDay."'
			";
			array_push($params, (string)$startDay);

		if(!empty($txt)) {
			$sql .= " and ( m.mr_work_barcode like ? or ";
			$sql .= " es.mr_emp_name like ? or ";
			$sql .= " es.mr_emp_lastname like ? or ";
			$sql .= " b.mr_branch_name like ? ) ";
//
			array_push($params, (string)$txt.'%');
			array_push($params, (string)$txt.'%');
			array_push($params, (string)$txt.'%');
			array_push($params, (string)$txt.'%');

			//array_push($params, (string)$year.'');
		}
		
		

		if(!empty($hub_id)) {
			$sql .= " and b.mr_hub_id = ? and b.mr_hub_id is not null ";
			array_push($params, (int)$hub_id);
		}
		if($status != ''){
			$sql .= " and m.mr_status_id in (".$in_status_condition.") ";
			$params = array_merge($params,$in_status_params);
		}

		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
			$params = array_merge($params,$in_workType_params);
		}

		$sql .= "order by m.mr_work_main_id desc";
		
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        //$params = array_merge($params,$in_status_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function getReceiveWorkByhandOut($user_id)
	{
		$startDay = Date("Y-m-d 00:00:00", strtotime(date('Y-m-d')." -7 Day"));// 2013-01-31
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				m.sys_timestamp,
				
				e.mr_emp_tel as mr_emp_tel,
				e.mr_emp_name as mr_emp_name,
				e.mr_emp_lastname  as mr_emp_lastname,
				e.mr_emp_email  as mr_emp_email,
				e.mr_emp_mobile  as mr_emp_mobile,


				d.mr_department_name,
				f.name as mr_department_floor,
				s.mr_status_name,
				i.mr_status_receive,
				r.mr_round_name,
				z.*
			FROM mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = m.mr_floor_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = m.mr_floor_id )
			LEFT join  mr_round r on(r.mr_round_id = m.mr_round_id)
			WHERE
				m.mr_type_work_id = 4 AND 
				m.mr_status_id = 1 AND 
				z.mr_user_id = ? AND 
				(
					i.mr_status_receive = 1  or 
					i.mr_status_receive is null or 
					(
						i.mr_status_receive = 2
						and m.sys_timestamp >= ?
					)
				)
				
				
			group by m.mr_work_main_id
			ORDER BY i.mr_status_receive ASC, m.mr_work_date_sent DESC
			
		';
		//echo $startDay;
		
		array_push($params, (int)$user_id);
		array_push($params, (string)$startDay);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	function getReceiveWorkPostOut($user_id)
	{
		$startDay = Date("Y-m-d 00:00:00", strtotime(date('Y-m-d')." -7 Day"));// 2013-01-31
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				m.sys_timestamp,
				e.mr_emp_tel as mr_emp_tel,
				e.mr_emp_name as mr_emp_name,
				e.mr_emp_lastname  as mr_emp_lastname,
				e.mr_emp_email  as mr_emp_email,
				e.mr_emp_mobile  as mr_emp_mobile,


				d.mr_department_name,
				f.name as mr_department_floor,
				s.mr_status_name,
				i.mr_status_receive,
				z.*
			FROM mr_work_main m
			left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = m.mr_floor_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = m.mr_floor_id )
			WHERE
				m.mr_type_work_id = 6 AND 
				m.mr_status_id = 1 AND 
				z.mr_user_id = ? AND 
				(
					i.mr_status_receive = 1  or 
					i.mr_status_receive is null or 
					(
						i.mr_status_receive = 2
						and m.sys_timestamp >= ?
					)
				)
				
				
			group by m.mr_work_main_id
			ORDER BY i.mr_status_receive ASC, m.mr_work_date_sent DESC
			
		';
		//echo $startDay;
		
		array_push($params, (int)$user_id);
		array_push($params, (string)$startDay);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}

	function select_work_main($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
}
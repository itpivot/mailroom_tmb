<?php
require_once 'Pivot/Dao.php';


class Dao_Work_other extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_other');
    }
	function loadDataDaily($date,$round=null)
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send,
			w_m.mr_work_barcode,
			w_m.mr_work_remark,
			w_o.mr_address,
			w_o.mr_cus_tel,
			w_o.mr_cus_name as sendder_name,
			w_o.mr_cus_lname as sendder_lname,
			concat(w_o.mr_cus_name," ",w_o.mr_cus_lname) as name_send,
			w_o.mr_address as sendder_address,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_name,
			emp_send.mr_emp_lastname,
			concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_resive,
			r.mr_round_name,
			s.mr_status_id,
			s.mr_status_name
		FROM
			mr_work_main w_m
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_work_inout w_io on(w_io.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_work_other w_o on(w_o.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_department dep on(dep.mr_department_id = w_o.mr_department_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_io.mr_emp_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
		WHERE
			w_m.mr_type_work_id = 8
			and w_m.mr_status_id != 6';
		if(!empty($date)){
			
				$sql .=' and (w_m.mr_work_date_sent like ? 
				or w_m.sys_timestamp LIKE ?
				)';
				array_push($params, trim($date)."%");
				array_push($params, trim($date)."%");
		}
		if(!empty($round)){
			$sql .=' and w_m.mr_round_id = ? ';
			array_push($params,$round );
		}
		$sql .=' group by w_m.mr_work_main_id';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
}
<?php

require_once 'Zend/Db.php';
require_once 'Zend/Config/Ini.php';

class Pivot_Dao
{
    protected $_tableName = '';
    protected $_orderFields = '';
    protected $_db;
    protected $_db_config;

    function __construct($tableName, $db=null)
    {
        global $_CONFIG;
        $this->_tableName = $tableName;
        $this->_db_config = $_CONFIG->database;

        if (is_null($db)) {
            $db = Zend_Db::factory($_CONFIG->database);
        }
        $this->_db = $db;
    }

    public function getDb()
    {
        return $this->_db;
    }

    public function count()
    {
        $sql = 'SELECT count(*) FROM ' . $this->_tableName;
        return $this->_db->fetchOne($sql);
    }
    
    public function fetchAll()
    {
        $sql  = 'SELECT * FROM ' . $this->_tableName;
        if ($this->_orderFields != '') {
            $sql .= 'ORDER BY ' . $this->_orderFields;
        }
        return $this->_db->fetchAll($sql);
    }

    public function getEncryptKey()
    {
        // -- local --
        $path_conf = '../configs/config.ini';
        if (!file_exists($path_conf)) {
            $path_conf = '../' . $path_conf;

            if (!file_exists($path_conf)) {
                $path_conf = '../' . $path_conf;

                if (!file_exists($path_conf)) {
                    $path_conf = '../' . $path_conf;
                }
            }
        }

        // -- production --
        // test-drop-code
        // $path_conf = '/var/www/html/hero/configs/config.ini';
        $encrypt = new Zend_Config_Ini($path_conf, 'production');

        return $encrypt;
    }



    public function get($id)
    {
        return $this->_db->fetchRow('SELECT * FROM ' . $this->_tableName . ' WHERE '.$this->_tableName.'_id = ?', $id);
    }

    public function remove($id)
    {
        $id = $this->_db->quote($id, 'INTEGER');
        return $this->_db->delete($this->_tableName, $this->_tableName.'_id = ' . $id);
    }
	function select( $sql )
    {
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}

    public function truncateTemp()
    {
        return $this->_db->delete($this->_tableName, "1=1" );
    }

    public function save($datas, $id = null)
    {
       // echo "======<pre>",print_r($this->_db_config,true);
       // exit;
        if (is_numeric($id)) {
            $this->_db->update($this->_tableName, $datas,  $this->_tableName.'_id = ' . $id);
        } else {
            $this->_db->insert($this->_tableName, $datas);
            $id = $this->_db->lastInsertId();
        }

        return $id;
    }
	
    public function orderBy($fields)
    {
        $this->_orderFields = $fields;
    }

    public static function dateFormat($date)
    {
        global $_CONFIG;

        $d = split('-', $date);
        if (count($d) >= 3) {
            $date = $_CONFIG->view->dateFormat;
            $date = str_replace('%Y', $d[0], $date);
            $date = str_replace('%m', $d[1], $date);
            $date = str_replace('%d', $d[2], $date);
        }

        return $date;
    }
	
	public function setDateFormat($date,$type)
    {	
		if($date){
			if( $type == 1 ){ // save
				list($d,$m,$y) = explode("-",$date);
				$date = $y."-".$m."-".$d;		
			}else if( $type == 2 )	{ // show
				list($y,$m,$d) = explode("-",$date);
				$date = $d."/".$m."/".$y;			
			}
		}	
		return $date;
    }

    public function spliceDateTime($dateTime = null) 
    {   
        $date = '';
        $time = '';
        if($dateTime !=''){
            $arr_date = array();
            $arr_date = explode(" ",$dateTime);
            if(isset($arr_date[0])){
                $date = $arr_date[0];
            }
            if(isset($arr_date[1])){
                $time = $arr_date[1];
            }
        }
        $today = date('Y-m-d H:i:s') ;
        $fixed_time = 3 ;    //แก้เวลาตามชั่วโมง

        $d1=new DateTime($today); 
        $d2=new DateTime($time); 

        // $diff = $d2->diff($d1);
        $diff = $this->GetDateDiffFromNow($dateTime);

        // if( $diff->h >=  $fixed_time){
        if( $diff >=  $fixed_time){
            $result_time = 1;
        }else{
            $result_time = 0;
        }

        return $result_time;
    }

    function GetDateDiffFromNow($originalDate=null) 
    {
        $hours = null;
        if($originalDate != ''){
            $unixOriginalDate = strtotime($originalDate);
            $unixNowDate = strtotime('now');
            $difference = $unixNowDate - $unixOriginalDate ;
            $days = (int)($difference / 86400);
            $hours = (int)($difference / 3600);
            $minutes = (int)($difference / 60);
            $seconds = $difference;
        }

        // now do what you want with this now and return ...

        return $hours;
    }
	
	public function GetRound( $time_round ) 
    {
        
		if ( $time_round >= "08:30:00" && $time_round <= "09:29:59" ) {
			$round = 1;
		}else if ( $time_round > "09:30:00" && $time_round <= "10:29:59" ) {
			$round = 2;
		}else if ( $time_round >= "10:30:00" && $time_round <= "12:00:00" ) {
			$round = 3;                                     
		}else if ( $time_round >= "12:00:01" && $time_round <= "14:00:00" ) {
			$round = 4;                                     
		}else if ( $time_round >= "14:00:01" && $time_round <= "16:00:00" ) {
			$round = 5;
		}else if ( $time_round >= "16:00:01" ) {
			$round = 1;
		}
        return $round;
    }


    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}
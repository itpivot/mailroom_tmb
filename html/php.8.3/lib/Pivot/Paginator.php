<?php

/**
 * Pagination controller
 */
class Atapy_Paginator
{
    protected $_current = null;
    protected $_totalItems = null;
    protected $_pageSize = null;
    protected $_groupSize = null;
    protected $_obj = null;

    function __construct($current, $totalItems, $pageSize = null, $groupSize = null)
    {
        global $_CONFIG;

        $this->_current    = $current;
        $this->_totalItems = $totalItems;
        $this->_pageSize   = (!is_null($pageSize))  ? $pageSize  : $_CONFIG->view->pagination->pageSize;
        $this->_groupSize  = (!is_null($groupSize)) ? $groupSize : $_CONFIG->view->pagination->groupSize;

        $this->_offset = ($current - 1) * $this->_pageSize;

        /* If current page is over range, change to last page and offset in last page
           When remove item in last page and page is not changed, use new last page insteadly. */
        while ($this->_offset >= $totalItems && $totalItems > 0) {
            $this->_current--;
            $this->_offset -= $this->_pageSize;
        }

        $this->buildObject();
    }
    
    public function getPageSize()
    {
        return $this->_pageSize;
    }

    public function getCurrent()
    {
        return $this->_current;
    }
    
    public function getOffset()
    {
        return $this->_offset;
    }

    protected function buildObject()
    {
        $obj = array();

        $obj['current'] = $this->_current;
        $obj['totalItems'] = $this->_totalItems;
        $obj['totalPages'] = floor(($this->_totalItems - 1) / $this->_pageSize) + 1;

        /* Find start and end page in current group */
        $start = floor(($this->_current - 1) / $this->_groupSize) * $this->_groupSize + 1;
        $end   = $start + $this->_groupSize - 1;

        /* Set previous page of current group */
        if ($start > $this->_groupSize) {
            $obj['prevGroup'] = $start - 1;
        }

        /* Set previous page */
        if ($this->_current > 1) {
            $obj['prev'] = $this->_current - 1;
        }

        /* Set next page */
        if ($this->_current < $obj['totalPages']) {
            $obj['next'] = $this->_current + 1;
        }

        /* If end page in current group is not over total page, use total page */
        if ($obj['totalPages'] > $end) {
            $obj['nextGroup'] = $end + 1;
        } else {
            $end = $obj['totalPages'];
        }

        /* Set pages of current group */
        $pages = array();
        for ($i = $start; $i <= $end; $i++) {
            array_push($pages, $i);
        }
        $obj['pages'] = $pages;

        /* Set start and end item of current page */
        $obj['startItem'] = floor(($this->_current - 1) * $this->_pageSize) + 1;
        $obj['endItem'] = $obj['startItem'] + $this->_pageSize - 1;
        if ($obj['endItem'] > $obj['totalItems']) {
            $obj['endItem'] = $obj['totalItems'];
        }

        $this->_obj = $obj;
    }

    public function getObject()
    {
        return $this->_obj;
    }
}

<?php
require_once 'Pivot/Auth.php';
require_once 'Twig-3.x/vendor/autoload.php';

class Pivot_Template
{
    public static function factory($templateFile)
    {
		return new Pivot_Template_Processor($templateFile);
    }
}


class Pivot_Template_Processor
{
    private  $templateFile;
    function __construct($templateFile) {
        $this->templateFile = $templateFile;
    }

    public function display($context) {
        global $_CONFIG;
        $auth = new Pivot_Auth();

        /* Add more template value */
        $context['site'] = $_CONFIG->site->toArray();
        $context['view'] = $_CONFIG->view->toArray();

        // $context['site']['loginUser'] = array(
        //     'id' => $auth->getUser(), 
        //     'username' => $auth->getUserName()
        // );

        /* Twig configuration path */
        $templatePath = $_CONFIG->site->documentRoot . '/templates';
        $cachePath = $_CONFIG->site->documentRoot . '/cache/templates';

        // Twig_Autoloader::register();
        // $loader = new Twig_Loader_Filesystem($templatePath, $cachePath);
        // $twig = new Twig_Environment($loader);
        
        // mobile : checked header type 'application/json'
        

        // $loader = new Twig_Loader_Filesystem($templatePath);
        $loader = new \Twig\Loader\FilesystemLoader($templatePath);
        
        // $twig = new Twig_Environment($loader, array(
        $twig = new \Twig\Environment($loader, array(
            'debug' => true,
            'cache' => $cachePath
        ));


        // $template = $twig->loadTemplate($this->templateFile);
        $template = $twig->load($this->templateFile);
        $template->display($context);
    }
}
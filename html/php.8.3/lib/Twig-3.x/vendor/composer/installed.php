<?php return array(
    'root' => array(
        'name' => 'twig/twig',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '62da1d7088462e6920367ce1d6650ed6649c3b98',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(
            0 => '3.5.x-dev',
        ),
        'dev' => true,
    ),
    'versions' => array(
        'psr/container' => array(
            'pretty_version' => '1.x-dev',
            'version' => '1.9999999.9999999.9999999-dev',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/phpunit-bridge' => array(
            'pretty_version' => '6.3.x-dev',
            'version' => '6.3.9999999.9999999-dev',
            'reference' => '3c42a3bcdbe6e555ed0bb7ce8e4f3878d12d6838',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/phpunit-bridge',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(
                0 => '1.27.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(
                0 => '1.27.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '62da1d7088462e6920367ce1d6650ed6649c3b98',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(
                0 => '3.5.x-dev',
            ),
            'dev_requirement' => false,
        ),
    ),
);

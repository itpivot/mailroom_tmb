<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);
$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();

$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    // Pivot_Site::toLoginPage();
    echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
    exit();
}

$branch_id		=  $req->get('branch_id');
$emp_data                   = array();


try {
    
	$table_name = 'mr_contact';
		$sql = "
			SELECT f.* FROM `mr_floor` f
			left join `mr_building` as b on(b.mr_building_id = f.mr_building_id)
				where b.mr_branch_id = '".$branch_id."'
			";
	if(!empty($sql)){
		$dao 			= new Pivot_Dao($table_name);
		$dao_db 		= $dao->getDb();
	
		$stmt = new Zend_Db_Statement_Pdo($dao_db, $sql);
		$stmt->execute();
		
		$contactdata = $stmt->fetchAll();
	}
	$floor_setName ='';	
    //$emp_data = $employeeDao->getEmpByID( $name_receiver_select );
	$floor ='
		<option value="0" >ชั้น 1</option>
		<option value="0" >ชั้น 2</option>
		<option value="0" >ชั้น 3</option>	
		<option value="0" >ชั้น 4</option>	
		';
	if(empty($contactdata)){
	$floor ='
		<option value="0" >ชั้น G</option>
		<option value="0" >ชั้น 1</option>
		<option value="0" >ชั้น 2</option>
		<option value="0" >ชั้น 3</option>	
		<option value="0" >ชั้น 4</option>	
		';
		$floor_setName = $floor;
	}else{
		$floor ='';
		foreach($contactdata as $key=> $val){
			//$floor .='<option value="'.$val['name'].'" >'.$val['name'].'</option>';
			$floor .='<option value="'.$val['mr_floor_id'].'" >'.$val['name'].'</option>';
			$floor_setName .='<option value="'.$val['name'].'" >'.$val['name'].'</option>';

		}
	}

    echo json_encode(array(
        'status' => 200,
        'data' =>  $floor,
        'floor_setName' =>  $floor_setName
    ));
} catch(Exception $e) {
    echo json_encode(array('status' => 500, 'message' => 'Internal Server Error.'));
}



?>
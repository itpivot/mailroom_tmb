<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
error_reporting(E_ALL & ~E_NOTICE);


$auth 		= new Pivot_Auth();
if (!$auth->isAuth()) {
	Pivot_Site::toLoginPage();
    exit;
}
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRoleDao 			= new Dao_UserRole();
$work_inout_Dao 		= new Dao_Work_inout();
$work_main_Dao 			= new Dao_Work_main();
$work_log_Dao 			= new Dao_Work_log();

$user_id 				= $auth->getUser();

$mr_branch_id 			= $req->get('mr_branch_id');
$mr_work_inout_id 		= $req->get('mr_work_inout_id');
$floor 					= $req->get('floor');
$floor2 				= $req->get('floor2');
$floor2_text 				= $req->get('floor2_text');
$mr_work_main_id  		= $req->get('mr_work_main_id');
$mr_type_work_id  		= $req->get('mr_type_work_id');
$mr_status_id 	 		= $req->get('mr_status_id');
$type 			 		= $req->get('type');

if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_inout_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $floor)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $floor2)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_main_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_type_work_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_status_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $type)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$sql_work_main = '
					SELECT 	i.* ,
							m.mr_status_id,
							m.mr_type_work_id
					FROM  mr_work_inout i
					left join mr_work_main m on(m.mr_work_main_id = i.mr_work_main_id )
					WHERE m.mr_work_main_id = ?';
		
		$params_emp_data = array();
		array_push($params_emp_data, (int)$mr_work_main_id);

$data_inout = $work_inout_Dao->select_work_inout( $sql_work_main,$params_emp_data );

$remark_edit = '';
$status_edit = '';
$status = '';

if($type == "mailrommedit"){

	$data2['mr_type_work_id'] 	= $mr_type_work_id;
	$data['mr_floor_id'] 		= $floor;
	if($mr_type_work_id == 1){
		$data['mr_floor_id'] 		= $floor;
		if($mr_status_id == 10){
			$data2['mr_status_id'] = 3;
		}
	}elseif($mr_type_work_id == 2){
		
		$data['mr_branch_id'] 		= $mr_branch_id;
		$data['mr_branch_floor'] 	= $floor2;
		$data['mr_floor_id'] 		= $floor2;
		if($mr_status_id == 3){
			$data2['mr_status_id'] 		= 10;
			$data['mr_floor_id'] 		= null;
		}
		if($floor2==0){
			$data['mr_floor_id'] 		= null;
			
		}else{
			$data['mr_branch_floor'] 	= $floor2_text;
		}
	}
	if($mr_type_work_id == 3){
		$data['mr_floor_id'] 		= $floor;
		if($mr_status_id == 10){
			$data2['mr_status_id'] = 3;
		}
	}
	

	if($mr_work_main_id != ''){
		$respmain = $work_main_Dao->save($data2,$mr_work_main_id);
	}else{
		echo json_encode(array('status' => 500, 'message' => 'บันทึกไม่สำเร็จ'));
		exit();
	}


	if($mr_work_inout_id != ''){
		$resp = $work_inout_Dao->save($data,$mr_work_inout_id);
		if($resp != "") {
			$status = "success";
		} else {
			$status = "false";
		}
	}else{
		echo json_encode(array('status' => 500, 'message' => 'บันทึกไม่สำเร็จ'));
		exit();
	}
	if($data_inout[0]['mr_floor_id'] != $floor ){
		$remark_edit .= '|แก้ไข  mr_floor_id: '.$data_inout[0]['mr_floor_id'].' เป็น '.$floor ;
	}if($data_inout[0]['mr_branch_id'] != $data['mr_branch_id'] ){
		$remark_edit .= '|แก้ไข mr_branch_id: '.$data_inout[0]['mr_branch_id'].' เป็น '.$data['mr_branch_id'] ;
	}if($data_inout[0]['mr_type_work_id'] != $mr_type_work_id ){
		$remark_edit .= '|แก้ไข mr_type_work_id: '.$data_inout[0]['mr_type_work_id'].' เป็น '.$mr_type_work_id ;
	}if($data_inout[0]['mr_status_id'] != $mr_status_id ){
		$remark_edit .= '|แก้ไข mr_status_id: '.$data_inout[0]['mr_status_id'].' เป็น '.$mr_status_id ;
	}

}else{

	$data['mr_floor_id'] = $floor;
	$remark_edit .= '|แก้ไข  mr_floor_id: '.$data_inout[0]['mr_floor_id'].' เป็น '.$floor ;
	if($mr_work_inout_id != ''){
		$resp = $work_inout_Dao->save($data,$mr_work_inout_id);
	}else{
		echo json_encode(array('status' => 500, 'message' => 'บันทึกไม่สำเร็จ'));
		exit();
	}
	if($resp != "") {
		$status = "success";
	} else {
		$status =  "false";
	}
}

$save_log['mr_user_id'] 									=  $auth->getUser();
$save_log['remark'] 									    = 'แก้ไขข้อมูล >>'.$remark_edit;
$save_log['mr_status_id'] 									= $mr_status_id;
$save_log['mr_work_main_id'] 								= $mr_work_main_id;
if($mr_work_main_id != ''){
	$work_log_Dao->save($save_log);
}

if($status == 'success'){
	echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ'));
}else{
	echo json_encode(array('status' => 501, 'message' => 'บันทึกไม่สำเร็จ'));
}



?>
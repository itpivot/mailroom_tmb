<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'PHPMailer.php';
error_reporting(E_ALL & ~E_NOTICE);

$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao 	    = new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();
$sendmail           = new SendMail();


$empCode 	= $req->get('emp_code');
$mr_user_id = $req->get('mr_user_id');

if(preg_match('/<\/?[^>]+(>|$)/', $empCode)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$updateUser = array();
$usr = array();

function random_password( $length ) {
    $char_s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$";
    $password = substr( str_shuffle( $char_s ), 0, $length );
    return $password;
}

$resp 		= $employee_Dao->getEmp_code($empCode);
$user_data 	= $users_Dao->getempByEmpid($empCode);
//echo "<pre>".print_r( $user_data , true )."</pre>";


if(isset($user_data['mr_user_id'])){
	if( $user_data['mr_user_id'] != "" ){
		if($resp['mr_emp_id'] != "") {
			if($user_data['mr_user_role_id']== 2 or $user_data['mr_user_role_id']== 5){
				$result_search 		= $users_Dao->getUsersByEmpId(intval($resp['mr_emp_id']));
				$usr['username'] 	= $resp['mr_emp_code'];
				$usr['password'] 	= 'Pivot@'.rand(10000,99999).'!';//random_password(10);
				$mr_user_password    				= $auth->encryptAES((string)$usr['password']);
				$updateUser['mr_user_password'] 	= $mr_user_password ;//md5($usr['password']);
				$updateUser['sys_timestamp'] 		= date('Y-m-d H:i:s');
				$updateUser['active'] 			    = 1;
				$updateUser['isFailed'] 			= 0;
				$updateUser['is_first_login'] 		= 0;
				$result = $users_Dao->save($updateUser,$mr_user_id );
				if($result != "") {
					$Subjectmail = "ขอแจ้งข้อมูลการเปลี่ยนรหัสผ่านใหม่";
					$body="";
					$body.="เรียน คุณ <b>".$resp['mr_emp_name']." ".$resp['mr_emp_lastname']."</b><br><br>";
					$body.= "ขอแจ้งข้อมูลการเปลี่ยนรหัสผ่านใหม่ของท่านค่ะ<br><br>";
					$body.= "password : "."<b>".$usr['password']."</b><br>";
					$body.="<br>";
					$body.="Pivot MailRoom Auto-Response Message <br>";
					$body.="Email TO : ".$resp['mr_emp_email']."<br>";
					$body.="(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";
					$result_mail = $sendmail->mailNotice($body,$Subjectmail, $resp['mr_emp_email']);
					//$result_mail = 'success';
					if($result_mail == 'success') {
						echo json_encode(array('status' => 200, 'message' => 'กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ','html'=>$body));
						exit;
					}else{
						$result_mail = $sendmail->mailNotice2($body,$Subjectmail, $resp['mr_emp_email']);
						if($result_mail == 'success') {
							echo json_encode(array('status' => 200, 'message' => 'กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ','html'=>$body));
							exit;
						}else{
							$result_mail = $sendmail->mailforget_password($body,$Subjectmail, $resp['mr_emp_email']);
							if($result_mail == 'success') {
								echo json_encode(array('status' => 200, 'message' => 'กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ','html'=>$body));
								exit;
							}else{
								echo json_encode(array('status' => 200, 'message' => 'Reset รหัสผ่านแล้ว ระบบไม่สามารถส่ง E mail ได้ ','html'=>$body));
							}
						}
					}
				}
			}else{
				$usr['username'] 	= $resp['mr_emp_code'];
				$usr['password'] 	= 'Pivot@'.rand(10000,99999).'!';//random_password(10);
				$mr_user_password    				= $auth->encryptAES((string)$usr['password']);
				$updateUser['mr_user_password'] 	= $mr_user_password ;//md5($usr['password']);
				$updateUser['sys_timestamp'] 		= date('Y-m-d H:i:s');
				$updateUser['active'] 			    = 1;
				$updateUser['isFailed'] 			= 0;
				$result = $users_Dao->save($updateUser,$mr_user_id );
				$body="";
					$body.="เรียน คุณ <b>".$usr['username']."</b><br><br>";
					$body.= "ขอแจ้งข้อมูลการเปลี่ยนรหัสผ่านใหม่ของท่านค่ะ<br><br>";
					$body.= "password : "."<b>".$usr['password']."</b><br>";
					$body.="<br>";
					$body.="Pivot MailRoom Auto-Response Message <br>";
					$body.="Email TO : ".$resp['mr_emp_email']."<br>";
					$body.="(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";
					//$result_mail = $sendmail->mailNotice($body,$Subjectmail, $resp['mr_emp_email']);
					//$result_mail = 'success';
					echo json_encode(array('status' => 200, 'message' => 'ดำเนินการเรียบร้อย ','html'=>$body));
					exit;
			}
		} else {
			echo json_encode(array('status' => 500, 'message' => 'รหัสพนักงานของท่านไม่ถูกต้องค่ะ'));
		}
	}else{
		echo json_encode(array('status' => 500, 'message' => 'รหัสพนักงานของท่านไม่ถูกต้องค่ะ'));
	}
}else{
	echo json_encode(array('status' => 500, 'message' => 'รหัสพนักงานของท่านไม่ถูกต้องค่ะ'));
}
?>
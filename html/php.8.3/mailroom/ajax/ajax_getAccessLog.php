<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Access_status_log.php';

$auth 			       = new Pivot_Auth();
$req                   = new Pivot_Request();
$access_status_log_Dao = new Dao_Access_status_log();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

function PrettyDate($date) {
    if($date) {
        $old = explode('/', $date);
        return $old[2] . '-' . $old[1] . '-' . $old[0];
    } else {
        return null;
    }
   
}

$start          = $req->get('start');
$end            = $req->get('end');

$data = array();
$data['start_date'] = PrettyDate($start);
$data['end_date']   = PrettyDate($end);
$data['activity']   = $req->get('activity');
$data['status']     = $req->get('status');

if(!empty($data['start_date'])){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['start_date'])) {
        echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
        exit();
	}
}
if(!empty($data['end_date'])){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['end_date'])) {
        echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
        exit();
	}
}
if(preg_match('/<\/?[^>]+(>|$)/', $data['activity'])) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $data['status'])) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$acc = $access_status_log_Dao->fetchAccess_logs($data);
//exit;
usort($acc, function ($x, $y) {
 	return $y['sys_timestamp'] > $x['sys_timestamp'];
 });


foreach($acc as $k => $v) {
    $res[$k][0] = $k+1;
    $res[$k][1] = strtoupper($v['mr_user_username']);
    $res[$k][2] = $v['mr_emp_name']." ".$v['mr_emp_lastname'];
    $res[$k][3] = $v['terminal'];
    $res[$k][4] = strtoupper($v['activity']);
    if($v['status'] == 'Success') {
        $sts = "<span class='badge badge-success'>".strtoupper($v['status'])."</span>";
    } else {
        $sts = "<span class='badge badge-danger'>" . strtoupper($v['status']) . "</span>";
    }
    $res[$k][5] = $sts;
    $res[$k][6] = date('d/m/y H:i', strtotime($v['sys_timestamp']));
    $res[$k][7] = $v['description'];
}

echo json_encode(array(
    'status' => 200, 
    'data' => $res
));
?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Contact.php';

$auth 			        = new Pivot_Auth();



if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();       
$contact 			= new Dao_Contact();                          

//$emp_data 			= $empDao->getEmpDataSelect();
$sql = "
SELECT
    e.mr_emp_id,
    e.update_date,
    e.mr_emp_name,
    e.mr_emp_lastname,
    e.mr_emp_code,
    p.mr_position_name,
    c.mr_cost_code,
    d.mr_department_name
FROM
    mr_emp AS e
LEFT JOIN mr_position p ON ( e.mr_position_id = p.mr_position_id)
LEFT JOIN mr_department d ON (e.mr_department_id = d.mr_department_id)
LEFT JOIN mr_cost c ON (e.mr_cost_id = c.mr_cost_id)
group by mr_emp_id
";
$data = $contact->select($sql);
foreach( $data as $num => $v){
    $txt = '';
    $v['edit'] 				= $txt;
    $v['name'] 				= $v['mr_emp_name'].' '.$v['mr_emp_lastname'];
    $v['no'] 				= $num +1;
	$emp_data[$num] 	= $v;
}

echo json_encode(array(
    'status' => 200,
    'data' => $emp_data
));


 ?>

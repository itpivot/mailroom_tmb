<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Cost.php';

$auth 			= new Pivot_Auth();
$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();
$CostDao 			= new Dao_Cost();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$txt = (isset($_GET['q']))?$_GET['q']:'';
if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}


$emp_data = $CostDao->fetchCostByCodeOrname($txt);


$json = array();
foreach ($emp_data as $key => $value) {
    $cost_name = $value['mr_cost_code']." - ".$value['mr_cost_name'];
    //$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
	$json[] = array(
		"id" => $value['mr_cost_id'],
		"text" => $cost_name,
		"data" => $value
	);
}


echo json_encode($json);
<?php
ini_set('memory_limit', '-1');
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Department.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Position.php';
require_once 'Dao/Post_price.php';



$auth 			    = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();   
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();                              
$branchDao 			= new Dao_Branch();                                 
$departmentDao 		= new Dao_Department();                                 
$floorDao 			= new Dao_Floor();                                 
$positionDao 		= new Dao_Position();                                 
$post_priceDao 		= new Dao_Post_price();                                 

$mr_type_post_id = $req->get('mr_type_post_id');
$quty 		 	 = $req->get('quty');
$weight 		 = $req->get('weight');
$price 		 	 = $req->get('price');
$total_price 	 = $req->get('total_price');
$data =  $post_priceDao->getprice($mr_type_post_id,$weight);
if(!empty($data)){
	$data['totalprice'] = $quty*$data['post_price'];
}else{
	$data['totalprice'] = '';
	$data['post_price'] = '';
}

echo json_encode(array(
    'status' => 200,
    'data' => $data
));


 ?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Employee.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$department_Dao 		= new Dao_Department();
$work_inout_Dao 		= new Dao_Work_inout();
$floor_Dao 				= new Dao_Floor();
$employee_Dao 			= new Dao_Employee();




$start 					= $req->get('start_date');
$end 					= $req->get('end_date');


if($start == ''){
	$start = date('Y-m-d 00:00:00');
}else{
	$start = date('Y-m-d 00:00:00',strtotime($start));
}

if($end == ''){
	$end = date('Y-m-d 23:59:59');
}else{
	$end = date('Y-m-d 23:59:59',strtotime($end));
}


$data = array();
$data['start_date'] 	= $start; 
$data['end_date'] 		= $end; 


// if(preg_match('/<\/?[^>]+(>|$)/', $data['mr_status_id'])) {
//     echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
//     exit();
// }

$params 	= array();
$sql_emp 	= "
	SELECT 
		u.mr_user_id,
		e.mr_emp_id,
		e.mr_emp_name,
		e.mr_emp_lastname,
		e.mr_emp_code,
		e.mr_hub_id,
		h.mr_hub_name
	
	FROM `mr_emp` e
		left join mr_user u on(u.mr_emp_id = e.mr_emp_id) 
		left join mr_hub h on(h.mr_hub_id = e.mr_hub_id) 
	where u.mr_user_role_id = 8 
	ORDER BY e.mr_hub_id ASC
	";

$sql_order = "
				SELECT 
						i.messenger_user_id,
						i.mr_branch_id,
						h.mr_hub_id,
						concat(b.mr_branch_code,':',b.mr_branch_name) as b_name,
						h.mr_hub_name,
						t.mr_type_work_name,
						t.mr_type_work_id,
						sum(case WHEN m.mr_status_id = 1  	THEN 1 ELSE 0 END ) as 	st_1 ,
						sum(case WHEN m.mr_status_id = 2  	THEN 1 ELSE 0 END ) as 	st_2 ,
						sum(case WHEN m.mr_status_id = 3  	THEN 1 ELSE 0 END ) as 	st_3 ,
						sum(case WHEN m.mr_status_id = 7  	THEN 1 ELSE 0 END ) as 	st_4 ,
						sum(case WHEN m.mr_status_id = 5  	THEN 1 ELSE 0 END ) as 	st_5 ,
						sum(case WHEN m.mr_status_id = 6  	THEN 1 ELSE 0 END ) as 	st_6 ,
						sum(case WHEN m.mr_status_id = 7  	THEN 1 ELSE 0 END ) as 	st_7 ,
						sum(case WHEN m.mr_status_id = 8  	THEN 1 ELSE 0 END ) as 	st_8 ,
						sum(case WHEN m.mr_status_id = 9  	THEN 1 ELSE 0 END ) as 	st_9 ,
						sum(case WHEN m.mr_status_id = 10  	THEN 1 ELSE 0 END ) as st_10 ,
						sum(case WHEN m.mr_status_id = 11  	THEN 1 ELSE 0 END ) as st_11 ,
						sum(case WHEN m.mr_status_id = 12  	THEN 1 ELSE 0 END ) as st_12 ,
						sum(case WHEN m.mr_status_id = 13  	THEN 1 ELSE 0 END ) as st_13 ,
						sum(case WHEN m.mr_status_id = 14  	THEN 1 ELSE 0 END ) as st_14 ,
						sum(case WHEN m.mr_status_id = 15  	THEN 1 ELSE 0 END ) as st_15 
						FROM mr_work_main m
						left join mr_work_inout i on(m.mr_work_main_id = i.mr_work_main_id)
						left join mr_branch b on CASE
												   WHEN m.mr_type_work_id IN (2)
													   THEN b.mr_branch_id = i.mr_branch_id 
													   ELSE b.mr_branch_id = m.mr_branch_id 
													END
						
						left join mr_hub h on(h.mr_hub_id = b.mr_hub_id)
						left join mr_user u on(u.mr_user_id = i.messenger_user_id)
						left join mr_type_work t on(t.mr_type_work_id = m.mr_type_work_id)
						WHERE 
							m.mr_type_work_id IN (2,3) 
							and m.sys_timestamp >=  ? 
							and m.sys_timestamp <=  ?  
						group by h.mr_hub_id
							";

$emp 		= $employee_Dao->select_employee($sql_emp,$params);
$params = array();
$params[] = (string)$data['start_date'];
$params[] = (string)$data['end_date'];
$order 		= $employee_Dao->select_employee($sql_order,$params);
$printdata 	= array();





$setkey_order = array();
foreach($order as $key_i => $val_i){
	$messenger_user_id = ($val_i['messenger_user_id'] != '')?$val_i['messenger_user_id']:0;
	$setkey_order[$messenger_user_id] = $val_i;
	$mr_hub_id 		= $val_i['mr_hub_id'];	
	
	$st1  			= $val_i['st_1']+$val_i['st_7']+$val_i['st_8'];//รอพนักงานเดินเอกสาร
	$st2  			= $val_i['st_2']+$val_i['st_9'];//พนักงานเดินเอกสารรับเอกสารแล้ว
	$st3  			= $val_i['st_14'];//branch ถึง Hub
	$st4  			= $val_i['st_3']+$val_i['st_10'];//ห้องสารบรรณกลางรับเอกสาร
	$st5  			= $val_i['st_4']+$val_i['st_11'];//พนักงานเดินเอกสารอยู่ในระหว่างนำส่ง ออกจากเมลรูม
	$st6  			= $val_i['st_13'];//Mailroom ถึง Hub;
	$st7  			= $val_i['st_5']+$val_i['st_12'];//เอกสารถึงผู้รับเรียบร้อยแล้ว
	$st8  			= $val_i['st_6']+$val_i['st_15'];//	ยกเลิกการจัดส่ง
	$sum  			= $st1+$st2+$st3+$st4+$st5+$st6+$st7+$st8;
	if(empty($val_i['mr_hub_name'])){
		$val_i['mr_hub_name']			= 'ไม่พบข้อมูล';
	}
	
	$val_i['no']			= $key_i+1;
	$val_i['em_name']			= '';
	$val_i['st1']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'1,7,8\');" type="button" class="btn btn-link">'.$st1.'</button>';
	$val_i['st2']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'2,9\');" type="button" class="btn btn-link">'.$st2.'</button>';
	$val_i['st3']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'14\');" type="button" class="btn btn-link">'.$st3.'</button>';
	$val_i['st4']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'3,10\');" type="button" class="btn btn-link">'.$st4.'</button>';
	$val_i['st5']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'4,11\');" type="button" class="btn btn-link">'.$st5.'</button>';
	$val_i['st6']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'13\');" type="button" class="btn btn-link">'.$st6.'</button>';
	$val_i['st7']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'5,12\');" type="button" class="btn btn-link">'.$st7.'</button>';
	$val_i['st8']  			=	'<button onclick="detail_click(\''.$mr_hub_id.'\',\'6,15\');" type="button" class="btn btn-link">'.$st8.'</button>';
	$val_i['sum']  			=	$sum;	
	
	$order[$key_i] = $val_i;
	
}

$none_emp = array();
$none_emp['mr_user_id'] 			= 0;
$none_emp['mr_emp_code'] 			= 'ไม่พบข้อมูล' ;
$none_emp['mr_hub_name'] 			= '' ;
$none_emp['mr_emp_name'] 			= '' ;
$none_emp['mr_emp_lastname'] 		= '' ;
$emp[] = $none_emp;
foreach($emp as $key => $val){
	$mr_user_id 		= 	$val['mr_user_id'];
	
	$val['no'] 			=	($key +1);
	$val['em_name'] 	= 	$val['mr_emp_code'].' '. $val['mr_emp_name'].'  '. $val['mr_emp_lastname'] ;
	if(isset($setkey_order[$mr_user_id])){
		$num_val 				= $setkey_order[$mr_user_id];
		$st1  			= $num_val['st_1']+$num_val['st_7']+$num_val['st_8'];//รอพนักงานเดินเอกสาร
		$st2  			= $num_val['st_2']+$num_val['st_9'];//พนักงานเดินเอกสารรับเอกสารแล้ว
		$st3  			= $num_val['st_14'];//branch ถึง Hub
		$st4  			= $num_val['st_3']+$num_val['st_10'];//ห้องสารบรรณกลางรับเอกสาร
		$st5  			= $num_val['st_4']+$num_val['st_11'];//พนักงานเดินเอกสารอยู่ในระหว่างนำส่ง ออกจากเมลรูม
		$st6  			= $num_val['st_13'];//Mailroom ถึง Hub;
		$st7  			= $num_val['st_5']+$num_val['st_12'];//เอกสารถึงผู้รับเรียบร้อยแล้ว
		$st8  			= $num_val['st_6']+$num_val['st_15'];//	ยกเลิกการจัดส่ง
		$sum  			= $st1+$st2+$st3+$st4+$st5+$st6+$st7+$st8;

		


		
		$val['st1']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'1,7,8\');" type="button" class="btn btn-link">'.$st1.'</button>';
		$val['st2']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'2,9\');" type="button" class="btn btn-link">'.$st2.'</button>';
		$val['st3']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'14\');" type="button" class="btn btn-link">'.$st3.'</button>';
		$val['st4']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'3,10\');" type="button" class="btn btn-link">'.$st4.'</button>';
		$val['st5']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'4,11\');" type="button" class="btn btn-link">'.$st5.'</button>';
		$val['st6']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'13\');" type="button" class="btn btn-link">'.$st6.'</button>';
		$val['st7']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'5,12\');" type="button" class="btn btn-link">'.$st7.'</button>';
		$val['st8']  			=	'<button onclick="detail_click(\''.$mr_user_id.'\',\'6,15\');" type="button" class="btn btn-link">'.$st8.'</button>';
		$val['sum']  			=	$sum;
		
	}else{
		$val['st1'] 			= 0;
		$val['st2'] 			= 0;
		$val['st3'] 			= 0;
		$val['st4'] 			= 0;
		$val['st5'] 			= 0;
		$val['st6'] 			= 0;
		$val['st7'] 			= 0;
		$val['st8'] 			= 0;
		$val['sum'] 			= 0;
	}
	if($val['sum']>0){
		$printdata[] 			= 	$val;
	}
}


$dataset 	= array(
	'status' => 200, 
	'data' => $order,
	//'data' => $printdata,
	't2' => $setkey_order,
	'date' => $params,
);
echo json_encode($dataset);
exit;
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************


$work_inout1 = $work_inout_Dao->searchAutoSummary_branch_to_branch($data);
$newlydata = array();

foreach($work_inout1 as $i => $i_val){
	$mr_branch_id 								= $i_val['mr_branch_id'];
	$mr_status_id 								= $i_val['mr_status_id'];
	

	if(!isset($newlydata[$mr_branch_id])){

		$newlydata[$mr_branch_id]  				= 	$i_val;
		$newlydata[$mr_branch_id]['st1']		= 0;
		$newlydata[$mr_branch_id]['st2']		= 0;
		$newlydata[$mr_branch_id]['st3']		= 0;
		$newlydata[$mr_branch_id]['st4']		= 0;
		$newlydata[$mr_branch_id]['st5']		= 0;
		$newlydata[$mr_branch_id]['st6']		= 0;
		$newlydata[$mr_branch_id]['st7']		= 0;
		$newlydata[$mr_branch_id]['st8']		= 0;
		$newlydata[$mr_branch_id]['st9']		= 0;
		$newlydata[$mr_branch_id]['st10']		= 0;
		$newlydata[$mr_branch_id]['st11']		= 0;
		$newlydata[$mr_branch_id]['st12']		= 0;
		$newlydata[$mr_branch_id]['st13']		= 0;
		$newlydata[$mr_branch_id]['st14']		= 0;
		$newlydata[$mr_branch_id]['st15']		= 0;


	}
	if($mr_status_id == 1){
		$newlydata[$mr_branch_id]['st1']		+= 1;
	}else if($mr_status_id == 2){
		$newlydata[$mr_branch_id]['st2']		+= 1;
	}else if($mr_status_id == 3){
		$newlydata[$mr_branch_id]['st3']		+= 1;
	}else if($mr_status_id == 4){
		$newlydata[$mr_branch_id]['st4']		+= 1;
	}else if($mr_status_id == 5){
		$newlydata[$mr_branch_id]['st5']		+= 1;
	}else if($mr_status_id == 6){
		$newlydata[$mr_branch_id]['st6']		+= 1;
	}else if($mr_status_id == 7){
		$newlydata[$mr_branch_id]['st7']		+= 1;
	}else if($mr_status_id == 8){
		$newlydata[$mr_branch_id]['st8']		+= 1;
	}else if($mr_status_id == 9){
		$newlydata[$mr_branch_id]['st9']		+= 1;
	}else if($mr_status_id == 10){
		$newlydata[$mr_branch_id]['st10']		+= 1;
	}else if($mr_status_id == 11){
		$newlydata[$mr_branch_id]['st11']		+= 1;
	}else if($mr_status_id == 12){
		$newlydata[$mr_branch_id]['st12']		+= 1;
	}else if($mr_status_id == 13){
		$newlydata[$mr_branch_id]['st13']		+= 1;
	}else if($mr_status_id == 14){
		$newlydata[$mr_branch_id]['st14']		+= 1;
	}else if($mr_status_id == 15){
		$newlydata[$mr_branch_id]['st15']		+= 1;
	}

	
	$newlydata[$mr_branch_id]['no'] 	= 	count($newlydata);
	$newlydata[$mr_branch_id]['sum'] = 	$newlydata[$mr_branch_id]['st6']+
								$newlydata[$mr_branch_id]['st7']+
								$newlydata[$mr_branch_id]['st8']+
								$newlydata[$mr_branch_id]['st9']+
								$newlydata[$mr_branch_id]['st10']+
								$newlydata[$mr_branch_id]['st11']+
								$newlydata[$mr_branch_id]['st12']+
								$newlydata[$mr_branch_id]['st13']+
								$newlydata[$mr_branch_id]['st14']+
								$newlydata[$mr_branch_id]['st15'];
	
}

$index = 0;
foreach($newlydata as $i => $i_val){
	$printdata[$index] = $i_val;
	$index++;
}
//echo print_r($newlydata,true);
//exit;

echo json_encode(array(
	'status' => 200, 
	'data' => $printdata,
));

?>
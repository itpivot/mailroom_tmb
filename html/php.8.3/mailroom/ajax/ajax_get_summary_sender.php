<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Floor.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req                = new Pivot_Request();
$userDao            = new Dao_User();
$userRoleDao        = new Dao_UserRole();

$department_Dao 	= new Dao_Department();
$work_inout_Dao 	= new Dao_Work_inout();
$floor_Dao 			= new Dao_Floor();

$start              = $req->get('start_date');
$end                = $req->get('end_date');

function changeFormatDate($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode('/', $date);
        $result = $new_date[2]."-".$new_date[1]."-".$new_date[0];
        return $result;
    }
}

$data = array();

$data['start_date'] = changeFormatDate($start); 
$data['end_date'] = changeFormatDate($end); 
$data['mr_status_id'] = $req->get('status');

if(!empty($start)){
    if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['start_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
    }
}
if(!empty($end)){
    if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['end_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
    }
}
if(preg_match('/<\/?[^>]+(>|$)/', $data['mr_status_id'])) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$dept = $floor_Dao->getFloor();

//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************

 $work_inout1 = $work_inout_Dao->searchAutoSummary_status_1_to_3($data);
// echo json_encode($work_inout1);
// exit;


 //$work_inout2 = $work_inout_Dao->searchAutoSummary_status_4_to_6($data);
//  echo json_encode($work_inout1);
//  //echo count($work_inout1);
//  exit();

$new_data=array();
foreach($work_inout1 as $i =>$val_i){
    $floor_id = 'no';
    $mr_status_id 		= ($val_i['mr_status_id']!='')?$val_i['mr_status_id']:0;
    if($mr_status_id	< 3	){
        $floor_id 	= 	($val_i['floor_main_id']!='')?$val_i['floor_main_id']:'no';
    }else{
        $floor_id 	= 	($val_i['floor_inout_id']!='')?$val_i['floor_inout_id']:'no';
    }
    if(!isset($new_data[$floor_id][ $mr_status_id])){
        $new_data[$floor_id][$mr_status_id]=1;
    }else{
        $new_data[$floor_id][$mr_status_id]+=1;
    }
}

//  echo json_encode($new_data);
//  exit;


$end_data = array();
$sum = array();
$sum['newly'] 		                =0;
$sum['msg_receive']                 =0;
$sum['mailroom'] 	                =0;
$sum['msg_send'] 	                =0;
$sum['success'] 	                =0;
$sum['cancel'] 		                =0;
$sum['total'] 	                    =0;


$val_push['mr_department_floor'] 	= 'ไม่พบชั้น';
$val_push['mr_floor_id'] 			= 'no';
array_push($dept,$val_push);
$end_data =array();
foreach($dept as $k =>$val_k){

		
	$sum['no'] 	   					                = $k+2;
	$sum['mr_department_floor'] 	                = 'รวม';
	$mr_floor_id                                    = $val_k['mr_floor_id'];
    $end_data[$k]['no'] 			                = $k+1;
	$end_data[$k]['mr_department_floor'] 			= $val_k['mr_department_floor'];
    //$end_data[$k]['mr_department_floor'] 			= $mr_floor_id;
	$end_data[$k]['mr_floor_id'] 					= urlencode(base64_encode($val_k['mr_floor_id']));
	$end_data[$k]['newly'] 							= intval($new_data[$mr_floor_id]['1']);
	$end_data[$k]['msg_receive'] 					= intval($new_data[$mr_floor_id]['2']);
	$end_data[$k]['mailroom'] 						= intval($new_data[$mr_floor_id]['3']);
	$end_data[$k]['msg_send'] 						= intval($new_data[$mr_floor_id]['4']);
	$end_data[$k]['success'] 						= intval($new_data[$mr_floor_id]['5']);
	$end_data[$k]['cancel'] 						= intval($new_data[$mr_floor_id]['6']);
	$end_data[$k]['total'] 					        = $end_data[$k]['newly'] 		
											        +$end_data[$k]['msg_receive']
											        +$end_data[$k]['mailroom'] 	
											        +$end_data[$k]['msg_send'] 	
											        +$end_data[$k]['success'] 	
											        +$end_data[$k]['cancel'];	
	$sum['no']   		                            = '';
	$sum['mr_floor_id']                             = '';
	$sum['newly'] 		                            += $end_data[$k]['newly'];
	$sum['msg_receive']                             += $end_data[$k]['msg_receive'];														
	$sum['mailroom'] 	                            += $end_data[$k]['mailroom'];														
	$sum['msg_send'] 	                            += $end_data[$k]['msg_send'];														
	$sum['success'] 	                            += $end_data[$k]['success'];														
	$sum['cancel'] 		                            += $end_data[$k]['cancel6'];														
	$sum['total']                                   += $end_data[$k]['total'];														
															
}

array_push($end_data,$sum);
echo json_encode($end_data);
 exit();

//*******************END MOS UPDATE *****************
//*******************END MOS UPDATE *****************
//*******************END MOS UPDATE *****************
//*******************END MOS UPDATE *****************
//*******************END MOS UPDATE *****************

//*******************************เวอร์ชั้นพี่หนาม
//*******************************เวอร์ชั้นพี่หนาม
//*******************************เวอร์ชั้นพี่หนาม
//*******************************เวอร์ชั้นพี่หนาม

//$work_inout = $work_inout_Dao->searchAutoSummary_status_1_to_3($data);
$work_inout = $work_inout_Dao->searchAutoSummary($data);
$resp = array();
$sum_newly = 0;
$sum_msg_receive = 0;
$sum_mailroom = 0;
$sum_msg_send = 0;
$sum_success = 0;
$sum_cancel = 0;

foreach($work_inout as $k => $v) {
    // if($v['mr_department_sender_floor'] != "") {
    if($v['floor_name_inout'] != "") {
        switch(intval($v['mr_status_id'])) {
            case 1:
                $sum_newly += 1;
                break;
            case 2:
                $sum_msg_receive += 1;
                break;
            case 3:
                $sum_mailroom += 1;
                break;
            case 4:
                $sum_msg_send += 1;
                break;
            case 5:
                $sum_success += 1;
                break;
            case 6:
                $sum_cancel += 1;
                break;
        }
    }
}


// echo print_r($work_inout);
// echo "<br>";
// echo $sum_newly." ".$sum_msg_receive." ".$sum_mailroom." ".$sum_msg_send." ".$sum_success." ".$sum_cancel;


foreach($dept as $key => $val) {
    $resp[$key]['no'] = $key + 1;
    $resp[$key]['mr_department_floor'] = $val['mr_department_floor'];
    $newly = 0;
    $msg_receive = 0;
    $mailroom = 0;
    $msg_send = 0;
    $success = 0;
    $cancel = 0;
    foreach($work_inout as $keys => $values) {
        // if($values['mr_department_sender_floor'] != "") {
        if($values['floor_name_inout'] != "") {
            // if($values['mr_department_sender_floor'] == $val['mr_department_floor']) {
            if($values['floor_name_inout'] == $val['mr_department_floor']) {
                if(intval($values['mr_status_id']) == 1) {
                        $newly += 1;
                } else if(intval($values['mr_status_id']) == 2) {
                        $msg_receive += 1;
                } else if(intval($values['mr_status_id']) == 3) {
                        $mailroom += 1;
                } else if(intval($values['mr_status_id']) == 4) {
                        $msg_send += 1;
                } else if(intval($values['mr_status_id']) == 5) {
                        $success += 1;
                } else if(intval($values['mr_status_id']) == 6) {
                        $cancel += 1;
                }
            }
        }
    }
    $resp[$key]['newly'] = $newly;
    $resp[$key]['msg_receive'] = $msg_receive;
    $resp[$key]['mailroom'] = $mailroom;
    $resp[$key]['msg_send'] = $msg_send;
    $resp[$key]['success'] = $success;
    $resp[$key]['cancel'] = $cancel;
    $resp[$key]['total'] = intval($newly) + intval($msg_receive) + intval($mailroom) + intval($msg_send) + intval($success) + intval($cancel);
}

$resp[count($resp)] = array(
    'no' => '',
    'mr_department_floor' => 'รวม',
    'newly' => ($sum_newly == null) ? 0 : $sum_newly,
    'msg_receive' => ($sum_msg_receive == null) ? 0 : $sum_msg_receive,
    'mailroom' => ($sum_mailroom == null) ? 0 : $sum_mailroom,
    'msg_send' => ($sum_msg_send == null) ? 0 : $sum_msg_send,
    'success' => ($sum_success == null ) ? 0 : $sum_success,  
    'cancel' => ($sum_cancel == null) ? 0 : $sum_cancel,
    'total' => $sum_newly + $sum_msg_receive + $sum_mailroom + $sum_msg_send + $sum_success + $sum_cancel
);

// echo print_r($resp);
echo json_encode($resp);
// exit();
?>
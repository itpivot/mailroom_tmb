<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';

$auth 			= new Pivot_Auth();
$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$txt = $req->get('q');
if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}


$emp_data = $employeeDao->getMessDataWithTXTsearch($txt);
if(empty($emp_data)){
    $emp_data = $employeeDao->getMessDataWithTXTsearch($txt);
}


$json = array();
foreach ($emp_data as $key => $value) {
    $fullname = $value['mr_emp_code']." - ". $value['mr_emp_name']." ".$value['mr_emp_lastname'];
    //$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
	$json[] = array(
		"id" => $value['mr_user_id'],
		"text" => $fullname
	);
}
echo json_encode($json);
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_byhand.php';
require_once 'Dao/Round_resive_thaipost_in.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Tnt_barcode_import.php';

$auth 			= new Pivot_Auth();
$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();
$work_byhandDao = new Dao_Work_byhand();
$round_resive_thaipost = new Dao_Round_resive_thaipost_in();
$work_log       = new Dao_Work_log();
$Tnt_barcode_import       = new Dao_Tnt_barcode_import();

$datadate		=  array(); 

$date1 = $req->get('date_1');
$date2 = $req->get('date_2');

$datadate['data1'] = $date1;
$datadate['data2'] = $date2;

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

// $round = $round_resive_thaipost->getreport_scan_post($datadate);
$datareport_tnt = $Tnt_barcode_import ->getdatareport_out_tnt($datadate);
// echo '<pre>'.print_r($datareport_tnt,true).'</pre>';
// exit;


$json = array();
foreach ($datareport_tnt as $key => $value) {
	$json[] = array(
		"no" => ($key+1),
		"mr_work_barcode" => $value['mr_work_barcode'],
		"barcode_tnt" => $value['barcode_tnt'],
		"sys_date" => $value['sys_date'],
		"mr_round_name" => $value['mr_round_name'],
		"send_emp_name" => $value['send_emp_name'],
		"send_emp_code" => $value['send_emp_code'],
		"send_department_code" => $value['send_department_code'],
		"send_department_name" => $value['send_department_name'],
		"send_branch_code" => $value['send_branch_code'],
		"send_branch_name" => $value['send_branch_name'],
		"re_emp_name" => $value['re_emp_name'],
		"re_emp_code" => $value['re_emp_code'],
		"re_department_code" => $value['re_department_code'],
		"re_department_name" => $value['re_department_name'],
		"re_branch_code" => $value['re_branch_code'],
		"re_branch_name" => $value['re_branch_name'],
	);
}


echo json_encode(array("data" => $json));
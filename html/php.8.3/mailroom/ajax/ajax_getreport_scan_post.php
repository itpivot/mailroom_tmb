<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_byhand.php';
require_once 'Dao/Round_resive_thaipost_in.php';

$auth 			= new Pivot_Auth();
$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();
$work_byhandDao = new Dao_Work_byhand();
$round_resive_thaipost = new Dao_Round_resive_thaipost_in();
$datadate		=  array(); 

$date1 = $req->get('date_1');
$date2 = $req->get('date_2');

$datadate['data1'] = $date1;
$datadate['data2'] = $date2;

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$round = $round_resive_thaipost->getreport_scan_post($datadate);
// echo '<pre>'.print_r($round,true).'</pre>';
// exit;


$json = array();
foreach ($round as $key => $value) {
	$json[] = array(
		"no" => ($key+1),
		"barcode" => $value['barcode'],
		"sysdate" => $value['sysdate'],
		"time_receive" => $value['time_receive'],
        "mr_round_name" => $value['mr_round_name'],
        "date_send" => $value['date_send'],
        "mr_emp_name" => $value['mr_emp_name'],
        "detail_work" => $value['detail_work'],
        "sys_timestamp" => $value['sys_timestamp'],
        "mr_emp_name" => $value['mr_emp_name'],
        "remark" => $value['remark'],
	);
}


echo json_encode(array("data" => $json));
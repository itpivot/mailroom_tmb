<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_post.php';
require_once 'nocsrf.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
} else {

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_postDao 		= new Dao_Work_post();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();

$user_id			= $auth->getUser();


$page 	= $req->get('page');
$id 	= $req->get('id');
// echo json_encode($id);
// exit();
if( $page == 'cancle_multiple'){
	try{
		$all_id = explode(",", $id);
		foreach($all_id as $val_id){
			//echo $val_id;
			$id = $val_id;
			if($id!=''){
				$mainupdat['mr_status_id'] = 6;
				$work_mainDao->save($mainupdat,$id);
				$save_log['mr_user_id'] 									= $user_id;
				$save_log['mr_status_id'] 									= 6;
				$save_log['mr_work_main_id'] 								= $id;
				$save_log['remark'] 										= "ยกเลิกรายการ";
				$work_logDao->save($save_log);
			}
		}
		echo json_encode(
			array(
				'status' => 200, 
				'data' => $all_id, 
				'message' => 'สำเร็จ' 
				)
			);
		exit;
	}catch ( Exception $e){
		echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}	
}else if( $page == 'cancle'){
	if($id!=''){
			$mainupdat['mr_status_id'] = 6;
			$work_mainDao->save($mainupdat,$id);
			$save_log['mr_user_id'] 									= $user_id;
			$save_log['mr_status_id'] 									= 6;
			$save_log['mr_work_main_id'] 								= $id;
			$save_log['remark'] 										= "ยกเลิกรายการ";
			$work_logDao->save($save_log);
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $save_log, 
			'message' => 'สำเร็จ' 
			)
		);
		exit;
}else{
	$byVal['round_printreper'] 	= $req->get('round_printreper');
	$byVal['date_report'] 		= $req->get('date_report');

	//$data	= $work_postDao->getdatatoday($byVal);
	$data	= $work_inoutDao->getdataByval($byVal);
	// echo '<pre>'.print_r($data,true).'</pre>';
	// exit;
	foreach($data as $key=>$val){
		$data[$key]['num'] = $key+1;
		$data[$key]['action'] = '<button type="button" onclick="cancle_work('.$val['mr_work_main_id'].')" class="btn btn-danger btn-sm">ลบ</button>';
		$data[$key]['check']='	
		<label class="custom-control custom-checkbox"  >
			<input value="'.$val['mr_work_main_id'].'" id="select-all" name="select_data" id="ch_'.$val['mr_work_main_id'].'" type="checkbox" class="custom-control-input">
			<span class="custom-control-indicator"></span>
			<span class="custom-control-description"></span>
		</label>
	';
		$data[$key]['sys_timestamp'] = $val['sys_timestamp'];
		$data[$key]['d_send'] = $val['d_send'];
		$data[$key]['mr_work_barcode'] = $val['mr_work_barcode'];
		$data[$key]['mr_status_name'] = $val['mr_status_name'];
		$data[$key]['send'] = $val['mr_emp_code'] . '' .$val['mr_emp_name'] .''.$val['mr_emp_lastname'];
		$data[$key]['mr_round_name'] = $val['mr_round_name'];
		$data[$key]['resive'] = $val['mr_cost_name']; 
		$data[$key]['mr_cost_code'] = $val['mr_cost_code'];
		$data[$key]['mr_topic'] = $val['mr_topic']; 
		$data[$key]['mr_work_remark'] = $val['mr_work_remark'];
	;
	}
	
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);

			exit;
	}

}

 ?>
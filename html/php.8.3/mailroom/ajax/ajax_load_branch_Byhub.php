<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$barcode 			= $req ->get('barcode');
$hub_id  			= $req->get('hub_id');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $hub_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$mr_branch_id 		= '';
//echo $mr_branch_id;
$sql_select = "SELECT * FROM mr_branch  where mr_branch_id is not null";

if($hub_id =='all_hub'){
	$sql_select .= " and mr_hub_id is not null and mr_hub_id != 0";
}else if($hub_id ==''){
	
}else if($hub_id =='all_branch'){
	$sql_select .= " and mr_hub_id is null";
}else{
	$sql_select .= " and mr_hub_id = ".$hub_id."";
}
				//if($mr_branch_id != '' and $mr_branch_id!=null){
				//	$sql_select .= "  and wb.mr_branch_id = ".$mr_branch_id." ";
				//}
				//$sql_select .= "  group by m.mr_work_main_id 
				//";
//echo $sql_select;
//exit;
$data = $send_workDao->select($sql_select);
$text=	'<option value="">ทั้งหมด</option>';
foreach($data as $key => $val_){
		$text.='<option value="'.$val_['mr_branch_id'].'">'.$val_['mr_branch_code'].':'.$val_['mr_branch_name'].'</option>';
}

echo json_encode(array(
	'status' => 200,
	'data' => $text
));
?>

<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';
$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}



$date 					= $req ->get('date');
$mr_round_id  			= $req->get('mr_round_id');
$hub_id  				= $req->get('hub_id');
$mr_branch_id  			= $req->get('mr_branch_id');



if(preg_match('/<\/?[^>]+(>|$)/', $date)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_round_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

if(!empty($mr_round_id) and $mr_round_id != "null"){
	
	$mr_round_id  = json_decode($mr_round_id);
		foreach($mr_round_id as $k => $v){
			$mr_round_id[$k] = intval($v);
		}
		$mr_round_id  = implode(',',$mr_round_id);

}else{
	$mr_round_id = '';
	if(empty($date)){
		$date = date("Y-m-d");
	}
}



$params2 = array();
$sql_select2 = "
			SELECT 
				rw.mr_round_resive_work_id,
				m.mr_work_main_id,
				rw.sysdate as sys_timestamp,
				m.mr_work_barcode,
				tnt.mr_round as send_round,
				e.mr_emp_name as send_name,
				e.mr_emp_lastname  as send_lname,
				bre.mr_branch_code as re_branch_code,
				bre.mr_branch_name as re_branch_name,
				e2.mr_emp_name as re_name,
				e2.mr_emp_lastname  as re_lname,
				st.mr_status_name  as st_name,
				tnt.tnt_tracking_no,
				rs.mr_round_name ,
				tw.mr_type_work_name,
				wb.mr_floor_id,
				f.name as name_floor_resive
			FROM mr_round_resive_work rw

			left join mr_round rs on(rs.mr_round_id = rw.mr_round_id)
			LEFT JOIN mr_work_main m  ON ( m.mr_work_main_id = rw.mr_work_main_id )
			LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
			LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
			LEFT JOIN mr_floor f ON ( wb.mr_floor_id = f.mr_floor_id )

			LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
			left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
			Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
			Left join mr_branch bre on ( bre.mr_branch_id = wb.mr_branch_id )
			Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
			Left join mr_round_send_work tnt on ( tnt.mr_work_main_id = m.mr_work_main_id )
	where m.mr_work_main_id is not null ";
	//$sql_select2 .= " and rw.sysdate LIKE'".$date."%' ";
	$sql_select2 .= " and rw.date_send = '".$date."'";

if(!empty($mr_round_id)){
	$sql_select2 .= ' and rw.mr_round_id in('.$mr_round_id.') ';
}
if($hub_id ==''){
					
}else if($hub_id =='all_hub'){
	$sql_select2 .= " and bre.mr_hub_id is not null";
}else if($hub_id =='all_branch'){
	$sql_select2 .= " and bre.mr_hub_id is null";
}else{
	$sql_select2 .= " and bre.mr_hub_id = ? ";
	array_push($params2, (int)$hub_id);
}

if($mr_branch_id != '' and $mr_branch_id!=null){
	$sql_select2 .= "  and wb.mr_branch_id = ? ";
	array_push($params2, (int)$mr_branch_id);
}



//echo json_encode(array('status' => 500, 'message' => 'ไม่มีข้อมูล','xxx' => $sql_select2));
//exit;
$data = $send_workDao->select_send_work($sql_select2,$params2);
if(!empty($data)){
	foreach($data as $i => $val_ ){
		$data[$i]['no1'] = $sql_select2;
		$data[$i]['no'] = $i+1;
		$data[$i]['remove'] 		= '<button onclick="remove_mr_round_resive_work('.$val_['mr_round_resive_work_id'].')" type="button" class="btn btn-secondary btn-sm">ลบ</button>';
		$data[$i]['barcode_tnt'] 		= isset($val_['barcode_tnt'])?$val_['barcode_tnt']:'';
		$data[$i]['send_round'] 		= $val_['mr_round_name'];
		$data[$i]['name_send'] 			= $val_['send_name'].'  '.$val_['send_lname'];
		$data[$i]['name_receive'] 		= $val_['re_name'].'  '.$val_['re_lname'];
		$data[$i]['branch'] 			= $val_['re_branch_code'].' : '.$val_['re_branch_name'].' / '.$val_['name_floor_resive'];
		$data[$i]['mr_status_name']		= '<label class="custom-control custom-checkbox">
												<input value="'.$val_['mr_work_main_id'].'" id="select-all" name="select_data" id="ch_'.$val_['mr_work_main_id'].'" type="checkbox" class="custom-control-input">
												<span class="custom-control-indicator"></span>
												<span class="custom-control-description">เลือก</span>
											</label>';

	}

	echo json_encode(array(
		'status' 		=> 200,
		'mr_round_id' 	=> $mr_round_id,
		'date' 			=> $date,
		'sql_select2' 	=> $sql_select2,
		'data' 			=> $data
	));
}else{
	echo json_encode(array('status' => 500, 'message' => 'ไม่มีข้อมูล','xxx' => $sql_select2));
}

exit;

?>

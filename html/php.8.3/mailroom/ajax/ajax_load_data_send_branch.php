<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';
$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$barcode 			= $req ->get('barcode');
$hub_id  			= $req->get('hub_id');
$mr_branch_id  		= $req->get('mr_branch_id');
$data_all  			= $req->get('data_all');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $hub_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $data_all)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

//echo $mr_branch_id;
if($data_all != 1){
	$st = "10,3";
}else{
	$st = "10,3";
}
$params = array();
$sql_select = "
				SELECT 
					m.*,
					concat(rs.sys_date,':round(',rs.mr_round,')' ) as send_round,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					m.mr_status_id,
					d.mr_department_code as re_mr_department_code,
					d.mr_department_name as re_mr_department_name,
					b.mr_branch_code as re_mr_branch_code,
					bre.mr_branch_name as re_branch_name,
					bre.mr_branch_code as re_branch_code,
					sw.barcode as supper_barcode,
					tw.mr_type_work_name,
					st.mr_status_name,
					tnt.tnt_tracking_no
				FROM(
					SELECT 
						l.mr_work_main_id
					FROM mr_work_log l
					left join mr_work_main m on(m.mr_work_main_id = l.mr_work_main_id)
					WHERE l.sys_timestamp between date_sub(concat(current_date, ' 00:00:00'), interval 0 day) and date_sub(concat(current_date, ' 23:59:59'), interval 0 day)
					AND l.mr_status_id IN (".$st.")
					and m.mr_status_id IN (".$st.")
					UNION ALL
					SELECT 
						m.mr_work_main_id
					FROM mr_work_main  m 
					where m.mr_status_id in(".$st.")
					and m.mr_type_work_id = 2 
				)as um
				LEFT JOIN mr_work_main m ON ( um.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
				LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
				Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
				Left join mr_branch bre on ( bre.mr_branch_id = wb.mr_branch_id )
				Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
				Left join (
						SELECT 
						mr_branch_id,
						mr_tnt_barcode_import_id ,
						barcode_tnt 
					FROM mr_tnt_barcode_import 
					WHERE active = 0	
				) tnt on ( tnt.mr_branch_id = wb.mr_branch_id )
				Left join mr_round_send_work rs on ( rs.mr_work_main_id = m.mr_work_main_id )
				where m.mr_status_id in(".$st.")
				and m.mr_type_work_id = 2
				";
				
				if($hub_id ==''){
					
				}else if($hub_id =='all_hub'){
					$sql_select .= " and bre.mr_hub_id is not null";
				}else if($hub_id =='all_branch'){
					$sql_select .= " and bre.mr_hub_id is null";
				}else{
					$sql_select .= " and bre.mr_hub_id = ? ";
					array_push($params, (int)$hub_id);
				}
				if($mr_branch_id != '' and $mr_branch_id!=null){
					$sql_select .= "  and wb.mr_branch_id = ? ";
					array_push($params, (int)$mr_branch_id);
				}if($data_all == 1){
					$sql_select .= " and m.sys_timestamp between date_sub(concat(current_date, ' 00:00:00'), interval 0 day) and date_sub(concat(current_date, ' 23:59:59'), interval 0 day)";
				}
				$sql_select .= "  group by m.mr_work_main_id ";

//$data = $send_workDao->select_send_work($sql_select,$params);






$params2 = array();
$sql_select2 = "
				SELECT 
					m.mr_work_main_id,
					m.sys_timestamp,
					m.mr_work_barcode,
					tnt.mr_round as send_round,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					bre.mr_branch_code as re_branch_code,
					bre.mr_branch_name as re_branch_name,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					tnt.tnt_tracking_no,
					tw.mr_type_work_name
				FROM mr_work_main m 
				LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
				LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
				Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
				Left join mr_branch bre on ( bre.mr_branch_id = wb.mr_branch_id )
				Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
				Left join mr_round_send_work tnt on ( tnt.mr_work_main_id = m.mr_work_main_id )
				where m.mr_status_id in(".$st.")
				and m.mr_type_work_id = 2
				";
				
				if($hub_id ==''){
					
				}else if($hub_id =='all_hub'){
					$sql_select2 .= " and bre.mr_hub_id is not null";
				}else if($hub_id =='all_branch'){
					$sql_select2 .= " and bre.mr_hub_id is null";
				}else{
					$sql_select2 .= " and bre.mr_hub_id = ? ";
					array_push($params2, (int)$hub_id);
				}
				if($mr_branch_id != '' and $mr_branch_id!=null){
					$sql_select2 .= "  and wb.mr_branch_id = ? ";
					array_push($params2, (int)$mr_branch_id);
				}if($data_all == 1){
					$sql_select2 .= " and m.sys_timestamp like'".date('Y-m-d')."%'";
				}
				$sql_select2 .= "  group by m.mr_work_main_id ";

$data = $send_workDao->select_send_work($sql_select2,$params2);




if(!empty($data)){
	foreach($data as $i => $val_ ){
		$data[$i]['no1'] = $sql_select2;
		$data[$i]['no'] = $i+1;
		$data[$i]['barcode_tnt'] 		= $val_['barcode_tnt'];
		$data[$i]['name_send'] 			= $val_['send_name'].'  '.$val_['send_lname'];
		$data[$i]['name_receive'] 		= $val_['re_name'].'  '.$val_['re_lname'];
		$data[$i]['branch'] 			= $val_['re_branch_code'].' : '.$val_['re_branch_name'];
		$data[$i]['mr_status_name']='	
											<label class="custom-control custom-checkbox"  >
												<input value="'.$val_['mr_work_main_id'].'" id="select-all" name="select_data" id="ch_'.$val_['mr_work_main_id'].'" type="checkbox" class="custom-control-input">
												<span class="custom-control-indicator"></span>
												<span class="custom-control-description">เลือก</span>
											</label>
										';

	}

	echo json_encode(array(
		'status' => 200,
		'data' => $data
	));
}else{
	echo json_encode(array('status' => 500, 'message' => 'ไม่มีข้อมูล'));
}

?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$mess_id 			= $req->get('mess_id');

if(preg_match('/<\/?[^>]+(>|$)/', $mess_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$mess_id 			= preg_quote($mess_id, '/');
$report = $work_inoutDao->searchSendMailroom( $mess_id );

foreach( $report as $num => $value ){
	$report[$num]['no'] = $num +1;
	$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
	$report[$num]['name_send'] = $value['mr_emp_name']." ".$value['mr_emp_lastname'];
}

$arr['data'] = $report;
echo json_encode($arr);
 ?>

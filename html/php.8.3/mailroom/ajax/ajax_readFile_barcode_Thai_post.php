<?php 
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Tnt_barcode_import.php';
require_once 'Dao/Round_resive_thaipost_in.php';
require_once 'PHPExcel.php';
// require_once('PhpSpreadsheet-master/vendor/autoload.php');
// $reader 				= new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}
// exit;
$req 								= new Pivot_Request();
$userDao 							= new Dao_User();
$userRole_Dao 						= new Dao_UserRole();
$departmentDao						= new Dao_Department();
$employeeDao 						= new Dao_Employee();
$floorDao 							= new Dao_Floor();
$historyDao 						= new Dao_History_import_emp();
$costDao 							= new Dao_Cost();
$branchDao 							= new Dao_Branch();
$tnt_barcode_importDao 				= new Dao_Tnt_barcode_import();
$round_resive_thaipost_inDao 		= new Dao_Round_resive_thaipost_in();

// $sql = "SELECT mr_branch_id, mr_branch_name, mr_branch_code, mr_address_id, mr_group_id, mr_hub_id, mr_branch_type, sys_date FROM mr_branch";
// $branch_data =  $branchDao->selectdata($sql);


$name 			= $_FILES['file']['name'];
$tmp_name 		= $_FILES['file']['tmp_name'];
$size 			= $_FILES['file']['size'];
$err 			= $_FILES['file']['error'];
$file_type 		= $_FILES['file']['type'];

$date_send 					= $req->get('date_send');
$round 						= $req->get('round');

$allowfile 		= array('xlsx','xls');
$surname_file = strtolower(pathinfo($name, PATHINFO_EXTENSION));

if(!in_array($surname_file,$allowfile)){
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}


$file_name = explode( ".", $name );
$extension = $file_name[count($file_name) - 1]; // xlsx|xls
try {
    $inputFileType = PHPExcel_IOFactory::identify($tmp_name);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($tmp_name);

	// $objPHPExcel = $reader->load($tmp_name);
	// $sheet = $objPHPExcel->getSheet($objPHPExcel->getFirstSheetIndex());
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$ch_num = 0;
//  Loop through each row of the worksheet in turn
$try_data['error'] 		= array();
$try_data['success'] 	= array();
$ch_error				=0;
$save_data 				= array();

if($date_send == ''){
	array_push($try_data['error'], 'กรุณาเลือกวะนที่');
}

if($round == '' or $round == 'null'){
	array_push($try_data['error'], 'กรุณาเลือกรอบ');
}

for ($row = 1; $row <= $highestRow; $row++){ 
	$ch_error			=0;							
    $no 				= $sheet->getCell('A'.$row)->getValue();//ลำดับที่
    $barcode			= $sheet->getCell('B'.$row)->getValue();//หมายเลขสิ่งของ
    $date1 				= $sheet->getCell('C'.$row)->getValue();//วันที่ เวลา (ส่งข้อมูล)
    $date2 				= $sheet->getCell('D'.$row)->getValue();//วันที่ เวลา (สแกนสิ่งของ)
	$date_	 			= $sheet->getCell('E'.$row)->getValue();//รหัสที่ทำการ
	$date_	 			= $sheet->getCell('F'.$row)->getValue();//สถานที่
	$date_	 			= $sheet->getCell('G'.$row)->getValue();//ขั้นตอน
	$detail	 			= $sheet->getCell('H'.$row)->getValue();//รายละเอียด
	$date_	 			= $sheet->getCell('I'.$row)->getValue();//ผู้ดำเนินการ
	$remark	 			= $sheet->getCell('J'.$row)->getValue();//หมายเหตุ
	if($row == 1){
		if($no != 'ลำดับที่'){
			array_push($try_data['error'], 'รูปแบบไฟไม่ถูกต้อง > colum A:');
			$ch_error++;
		}
		if($barcode != 'หมายเลขสิ่งของ'){
			array_push($try_data['error'], 'รูปแบบไฟไม่ถูกต้อง > colum B:');
			$ch_error++;
		}
		if($detail != 'รายละเอียด'){
			array_push($try_data['error'], 'รูปแบบไฟไม่ถูกต้อง > colum H:');
			$ch_error++;
		}
		if($remark != 'หมายเหตุ'){
			array_push($try_data['error'], 'รูปแบบไฟไม่ถูกต้อง > colum J:');
			$ch_error++;
		}
		if($ch_error> 0){
			$txt_error = '';
			foreach($try_data['error'] as $key => $val){
				$txt_error .=  '<span class="badge badge-pill badge-danger">'.$val.'</span><br>';
			}
			$try_data['txt_error'] = $txt_error;
			echo json_encode(array(
				'status' => 500,
				'data' => $try_data
			));
			exit;
		}
	}else{
		if($no!=''){
				if($barcode == ''){
					array_push($try_data['error'], 'ไม่พบ Barcode ลำดับที่ :  '.$no);
					$ch_error++;
				}

				if($ch_error<=0){
					$sql_checkbarcode = "SELECT * FROM `mr_round_resive_thaipost_in` WHERE `barcode` LIKE '".$barcode."';";
					$checkbarcode =  $branchDao->select($sql_checkbarcode);
					if(!empty($checkbarcode)){
						array_push($try_data['error'], 'ลำดับ: '.$no.' barcode ซ้ำ  '.$barcode);
						$ch_error++;
						//$successid = $tnt_barcode_importDao->save($save_barcode);
					}else{
						$mr_user_id 					= $auth->getUser();
						$data_save 						= array();
						$data_save['date_send'] 		= $date_send ;
						$data_save['mr_round_id'] 		= $round ;
						$data_save['mr_user_id'] 		= $mr_user_id ;
						$data_save['barcode'] 		= $barcode;
						$data_save['detail_work'] 	= $detail;
						$data_save['remark'] 		= $remark;
						$data_save['is_receiver'] 	= 0;
						$data_save['active'] 		= 1;
						array_push($save_data, $data_save);

					}
				}
		}else{
			array_push($try_data['error'], 'ไม่มี no  '.$no);
		}
	}
}

// array_push($try_data['success'],$successid);

$try_data['count_error'] 		= count($try_data['error']);
$try_data['count_success'] 		= count($try_data['success']);

$txt_error = '';
foreach($try_data['error'] as $key => $val){
	$txt_error .=  '<span class="badge badge-pill badge-danger">'.$val.'</span><br>';
}
 
if($txt_error ==''){
	// echo print_r($save_data,true);
	$barcodeSave = array();
	foreach($save_data as $k => $v){
		$barcodeSave[] = $round_resive_thaipost_inDao->Save($v);
	}
	echo json_encode(array(
		'status' => 200,
		'data' => $save_data
	));
	exit;
}else{
	$try_data['txt_error'] = $txt_error;
	echo json_encode(array(
		'status' => 500,
		'data' => $try_data
	));
	exit;
}
?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Cost.php';

require_once 'nocsrf.php';

// require_once 'PHPExcel.php';
require_once('PhpSpreadsheet-master/vendor/autoload.php');
$reader 				= new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
   echo "Failed";
} else {


try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
 
   exit();
}


$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();
$empDao 			= new Dao_Employee();  
$costDao			= new Dao_Cost();
$departmentDao 		= new Dao_Department();

$user_id			= $auth->getUser();



$page = $req->get('page');


if($page == "Uploadfile"){
	
$name 			= $_FILES['file']['name'];
$tmp_name 		= $_FILES['file']['tmp_name'];
$size 			= $_FILES['file']['size'];
$err 			= $_FILES['file']['error'];
$file_type 		= $_FILES['file']['type'];
$import_round = $req->get('import_round');
$date_import =  $req->get('date_import');
//ตรวจสอบ รอบ
if($import_round == ''){
	echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ รอบ  </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}
//ตรวจสอบ วันที่
if($date_import == ''){
	echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ วันที่นำส่ง  </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}

$file_name = explode( ".", $name );
$extension = $file_name[count($file_name) - 1]; // xlsx|xls
try {
	$spreadsheet = $reader->load($tmp_name);
	$sheet = $spreadsheet->getSheet($spreadsheet->getFirstSheetIndex());
} catch(Exception $e) {
	die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}

try {
    // $inputFileType = PHPExcel_IOFactory::identify($tmp_name);
    // $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    // $objReader->setReadDataOnly(true);
    // $objPHPExcel = $objReader->load($tmp_name);

	$spreadsheet = $reader->load($tmp_name);
	$sheet = $spreadsheet->getSheet($spreadsheet->getFirstSheetIndex());
	

	$sql = "SELECT
			 	e.mr_emp_id,
			 	e.mr_cost_id,
			 	e.mr_emp_code,
			 	e.mr_emp_name,
			 	e.mr_emp_lastname,
			 	u.mr_user_id,
			 	u.mr_user_role_id

			FROM mr_emp e
			LEFT JOIN mr_user u on(e.mr_emp_id = u.mr_emp_id)
			WHERE mr_emp_code NOT LIKE '%Resign%' 
			ORDER BY `mr_emp_id` ASC";
	$department 	= $departmentDao->fetchAll();
	$costdata	 	= $costDao->fetchAll();
	$params 		= array();
	$emp 			= $work_mainDao->select_work_main($sql,$params);

	$department_arr  = array();
	foreach($department as $i => $val_i){
		$department_arr[$val_i['mr_department_code']]['id']		 	=  $val_i['mr_department_id']; 
		$department_arr[$val_i['mr_department_code']]['code'] 		= $val_i['mr_department_code'];
		$department_arr[$val_i['mr_department_code']]['name'] 		= $val_i['mr_department_name'];
		$department_arr[$val_i['mr_department_code']]['detail'] 	= $val_i['mr_department_code']." - ".$val_i['mr_department_name'];

	}
	$cost_arr  = array();
	foreach($costdata as $i => $val_i){
		$cost_arr[$val_i['mr_cost_code']]['id']		 	=  $val_i['mr_cost_id']; 
		$cost_arr[$val_i['mr_cost_code']]['code'] 		= $val_i['mr_cost_code'];
		$cost_arr[$val_i['mr_cost_code']]['name'] 		= $val_i['mr_cost_name'];
		$cost_arr[$val_i['mr_cost_code']]['detail'] 		= $val_i['mr_cost_code']." - ".$val_i['mr_cost_name'];

	}
	$emp_arr  = array();
	$mess_arr  = array();
	foreach($emp as $j => $val_j){
		if($val_j['mr_user_role_id']== 8){
			$mess_arr[$val_j['mr_emp_code']]['mr_user_id'] 	=  $val_j['mr_user_id']; 
			$mess_arr[$val_j['mr_emp_code']]['id'] 			=  $val_j['mr_emp_id']; 
			$mess_arr[$val_j['mr_emp_code']]['mr_cost_id'] 	=  $val_j['mr_cost_id']; 
			$mess_arr[$val_j['mr_emp_code']]['code'] 		=  $val_j['mr_emp_code']; 
			$mess_arr[$val_j['mr_emp_code']]['name'] 		=  $val_j['mr_emp_name']; 
			$mess_arr[$val_j['mr_emp_code']]['lname'] 		=  $val_j['mr_emp_lastname']; 
			$mess_arr[$val_j['mr_emp_code']]['detail'] 		=  $val_j['mr_emp_code']." - ".$val_j['mr_emp_name']."  ".$val_j['mr_emp_lastname']; 

		}else{
			$emp_arr[$val_j['mr_emp_code']]['id'] 			=  $val_j['mr_emp_id']; 
			$emp_arr[$val_j['mr_emp_code']]['mr_user_id'] 	=  $val_j['mr_user_id']; 
			$emp_arr[$val_j['mr_emp_code']]['mr_cost_id'] 	=  $val_j['mr_cost_id']; 
			$emp_arr[$val_j['mr_emp_code']]['code'] 		=  $val_j['mr_emp_code']; 
			$emp_arr[$val_j['mr_emp_code']]['name'] 		=  $val_j['mr_emp_name']; 
			$emp_arr[$val_j['mr_emp_code']]['lname'] 		=  $val_j['mr_emp_lastname']; 
			$emp_arr[$val_j['mr_emp_code']]['detail'] 		=  $val_j['mr_emp_code']." - ".$val_j['mr_emp_name']."  ".$val_j['mr_emp_lastname']; 
		}
	}
	// echo json_encode($emp_arr);
	// exit;
	

} catch(Exception $e) {
	echo json_encode(array('status' => 505, 'message' => 'Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage() ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}
// $sheet = $objPHPExcel->getSheet(0); 
// $highestRow = $sheet->getHighestRow(); 
// $highestColumn = $sheet->getHighestColumn();

$highestRow 	= $sheet->getHighestRow(); 
$highestColumn 	= $sheet->getHighestColumn();

$ch_num = 0;
//  Loop through each row of the worksheet in turn
$store_barcode = array();
$store_barcode_re = array();
$data_dave = array();


for ($row = 1; $row <= $highestRow; $row++){ //  Read a row of data into an array
    $no 				= $sheet->getCell('A'.$row)->getValue();
    $dep_code			= $sheet->getCell('B'.$row)->getValue();
    $emp_code 			= $sheet->getCell('C'.$row)->getValue();
    $emp_name 			= $sheet->getCell('D'.$row)->getValue();
    $resiver	 		= $sheet->getCell('E'.$row)->getValue();
    $tel				= $sheet->getCell('F'.$row)->getValue();
    $byhand_type		= $sheet->getCell('G'.$row)->getValue();
	$address			= $sheet->getCell('H'.$row)->getValue();
	$topic				= $sheet->getCell('I'.$row)->getValue();
	$remark 			= $sheet->getCell('J'.$row)->getValue();
	$qty				= $sheet->getCell('K'.$row)->getValue();
	$cost_center		= $sheet->getCell('L'.$row)->getValue();
	$mess				= $sheet->getCell('M'.$row)->getValue();
	
	
	
	
	
	if($row == 1){
		$arr_msg = '';
		$excollums = array('A','B','C','D','E','F','G','H','I','J','K','L','M','M','N');
		$byhand_types = array(
							'ส่งเอกสาร'=> 1,
							'รับเอกสาร'=>2,
							'ส่งและรับเอกสารกลับ'=>3 
						);
		$collumsname = array('ลำดับ','รหัสหน่วยงาน','รหัสพนักงาน','ชื่อผู้ส่ง'   ,'ชื่อผู้รับ '  ,'เบอร์โทร', 'ประเภท'      ,'ที่อยู่'   ,'หัวเรื่อง','หมายเหตุ' ,'จำนวนซอง','รหัสค่าใช้จ่าย','รหัสแมสเซ็นเจอร์');
		$collumsval  = array($no   , $dep_code   , $emp_code , $emp_name, $resiver , $tel     , $byhand_type , $address,$topic  ,$remark   , $qty  , $cost_center  ,$mess);
		foreach($collumsname as $key => $c_val){
			if($collumsval[$key] != $c_val){
				$arr_msg .= '<div class="alert alert-danger" role="alert"> ชื่อ collum '.$excollums[$key]." ไม่ตรงกับระบบ >>[".$collumsval[$key]."] != [".$c_val."]</div>";
			}							
		}
		if(!empty($arr_msg)){
			echo json_encode(array('status' => 505, 'message' => $arr_msg ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}
		
	}else{
		if($no == ''){
			break;  
		}

		//ตรวจสอบ หน่วยงาน
		if(!isset($department_arr[$dep_code])){
			echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">ไม่พบข้อมูลรหัสหน่วยงาน หรื่อไม่ตรงกับระบบ: '.$barcode.'  ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}else{
			$mr_send_department_id 		= $department_arr[$dep_code]['id'];
			$mr_send_department_detail 	= $department_arr[$dep_code]['detail'];
		}

		//ตรวจสอบ รหัสหนักงาน
		if(isset($emp_arr[$emp_code])){
			// echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">ไม่พบข้อมูลรหัสหนักงาน หรื่อไม่ตรงกับระบบ: '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			// exit();
			$mr_send_emp_id 	= $emp_arr[$emp_code]['id'];
			$mr_o_cost_id 		= $emp_arr[$emp_code]['mr_cost_id'];
			$mr_send_emp_detail = $emp_arr[$emp_code]['detail'];
		}else{
			$mr_send_emp_id = null;
			$mr_o_cost_id  = null;
			$mr_send_emp_detail = $emp_name;
		}

		//ตรวจสอบ ประเถทการส้ง
		if($byhand_type == ''){
			echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ประเถทการส่ง : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}else{
			//ตรวจสอบ ประเภทการส่ง
			if(isset($byhand_types[$byhand_type])){
				$byhand_type_id = $byhand_types[$byhand_type];
			}else{
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">ไม่พบข้อมูลประเภทการส่ง หรื่อชื่อไม่ตรงกับระบบ: '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}
		}
		

		
		//ตรวจสอบ จำนวน
		if($qty == ''){
			echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ จำนวน : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}else{
			if(is_numeric($qty)){
					if($qty < 1){
						echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ จำนวนมากกว่าเท่ากับ 1 : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
						exit();
					}
			}else{
				echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ จำนวนเป็นตัวเลข : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}
		}
		
		
		//ตรวจสอบ ที่อยู่ผู้รับ
		if($address == ''){
			echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ที่อยู่ผู้รับ : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}
		//ตรวจสอบ เบอร์โทร
		if($tel == ''){
			echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ เบอร์โทร : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}
		//ตรวจสอบ ผู้รับ
		if($resiver == ''){
			echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">กรุณาระบุ ผู้รับ : '.$barcode.'ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}
	


		//ตรวจสอบ แมส
		if(isset($mess_arr[$mess])){
			$mess_user_id 	= $mess_arr[$mess]['mr_user_id'];
			$mess_user_detail = $mess_arr[$mess]['detail'];
			$mr_status_id = 4;
		}else{
			$mess_user_id = null;
			$mr_status_id = 3;
		}

		if(!isset($cost_arr[$cost_center])){
			echo json_encode(array('status' => 505, 'message' => '<div class="alert alert-danger" role="alert">ไม่พบข้อมูลรหัสค่าใช้จ่าย หรื่อไม่ตรงกับระบบ: ลำดับที่ '.$no.' </div>' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}else{
			$mr_cost_id 				= $cost_arr[$cost_center]['id'];
			$mr_cost_detail 			= $cost_arr[$cost_center]['detail'];
			$emp_update['mr_cost_id']	= $mr_cost_id;

			if($mr_send_emp_id!='' and $mr_cost_id !=''){
				$empDao->save($emp_update,$mr_send_emp_id);
			}
		}
	
    	
	
		$data_dave[$row]['mr_work_date_sent'] 			= $date_import; //วันนำส่ง
		$data_dave[$row]['mr_work_remark']				= $remark;
		$data_dave[$row]['mr_type_work_id']				= 4;
		$data_dave[$row]['mr_status_id']				= $mr_status_id;
		$data_dave[$row]['mr_user_id']					= $user_id;
		$data_dave[$row]['mr_round_id']					= $req->get('import_round');
		$data_dave[$row]['quty']						= $qty;
		$data_dave[$row]['mr_cost_id']					= $mr_cost_id;
		$data_dave[$row]['mr_send_emp_id']				= $mr_send_emp_id;
		$data_dave[$row]['mr_send_department_id']		= $mr_send_department_id;
		$data_dave[$row]['mr_send_emp_detail']			= $mr_send_emp_detail;
		$data_dave[$row]['mr_cus_name']					= $resiver;
		$data_dave[$row]['mr_address']					= $address;
		$data_dave[$row]['byhand_type_id']				= $byhand_type_id;
		$data_dave[$row]['topic']						= $topic;
		$data_dave[$row]['tel']						= $tel;
		$data_dave[$row]['mess_user_id']				= $mess_user_id;
	}
	//exit;
	
	
}
// echo json_encode($data_dave);
// exit();
// exit;
	try{
		foreach($data_dave as $key_save => $val_save){
			//date_import
			//import_round
			$save_main['mr_work_date_sent']				=$date_import; //วันนำส่ง
			//$save_main['mr_work_barcode']				= $val_save['mr_work_barcode'];
			$save_main['mr_work_remark']				= $val_save['mr_work_remark'];
			$save_main['mr_topic']						= $val_save['topic'];
			$save_main['mr_type_work_id']				= $val_save['mr_type_work_id'];
			$save_main['mr_status_id']					= $val_save['mr_status_id'];
			$save_main['mr_user_id']					= $val_save['mr_user_id'];
			$save_main['mr_round_id']					= $val_save['mr_round_id'];
			$save_main['messenger_user_id']				= $val_save['mess_user_id'];
			$save_main['quty']							= $val_save['quty'];

			//echo print_r($save_main,true);
			//exit();
			//$mr_work_main_id 							= '1200';
			$mr_work_main_id 							= $work_mainDao->save($save_main);
			if(strlen($mr_work_main_id)<=1){
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."000000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=2){
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."00000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=3){
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."0000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=4){
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."000".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=5){
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."00".$mr_work_main_id;
			}else if(strlen($mr_work_main_id)<=6){
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."0".$workmr_work_main_id_main_id;
			}else if(strlen($mr_work_main_id)<=7){
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy").$mr_work_main_id;
			}else{
					$num_run 			= substr($mr_work_main_id, -7); 
					$Nbarcode['mr_work_barcode'] = "BHO".date("dmy").$num_run;
			} 
			
			$barcodeok = $Nbarcode['mr_work_barcode'];
			$work_mainDao->save($Nbarcode,$mr_work_main_id);

			$save_byhand['mr_work_main_id']			= $mr_work_main_id;
			$save_byhand['mr_work_byhand_type_id']	= $val_save['byhand_type_id'];
			$save_byhand['mr_send_emp_id']			= $val_save['mr_send_emp_id'];
			$save_byhand['mr_send_emp_detail']		= $val_save['mr_send_emp_detail'];
			$save_byhand['mr_cus_name']				= $val_save['mr_cus_name'];
			$save_byhand['mr_cus_tel']				= $val_save['tel'];
			$save_byhand['mr_address']				= $val_save['mr_address'];
			$save_byhand['mr_send_dep_id']			= $val_save['mr_send_department_id'];
			$save_byhand['mr_cost_id']				= $val_save['mr_cost_id'];
			$save_byhand['mr_messenger_id']		    = $val_save['mess_user_id'];
			//echo print_r($save_byhand,true);
			//echo json_encode($save_byhand);
			//exit();
			$work_inout_id	= $work_byhandDao->save($save_byhand);
		
			//exit();	
			$save_log['mr_user_id'] 									= $user_id;
			$save_log['mr_status_id'] 									= $val_save['mr_status_id'];
			$save_log['mr_work_main_id'] 								= $mr_work_main_id;
			$save_log['remark'] 										= "mailroom_สร้างราายการนำส่ง BY hand ส่งออก import Excel";
			$log_id = $work_logDao->save($save_log);
			//echo json_encode($save_log);
			// echo print_r($save_log,true);
			// exit();

		}

		echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
			

	}catch ( Exception $e){
		if(!empty($mr_work_main_id)){
			$work_mainDao->remove($mr_work_main_id);
		}
		if(!empty($work_post_id)){
			$work_postDao->remove($work_post_id);
		}
		if(!empty($log_id)){
			$work_logDao->remove($log_id);
		}
		echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}	

}else{
	try{

		$save_main['mr_work_date_sent']			= $req->get('date_send');
	// $save_main['mr_work_barcode']			= $barcodeok;
		$save_main['mr_work_remark']			= $req->get('work_remark');
		$save_main['mr_type_work_id']			= 4;

		if($req->get('messenger_user_id') != ''){
			$save_main['mr_status_id']				= 4;
			$save_main['messenger_user_id'] 		= $req->get('messenger_user_id');
			$save_byhand['mr_messenger_id'] 		= $req->get('messenger_user_id');

			$save_log['mr_status_id'] 				= 4;
		}else{
			$save_main['mr_status_id']				= 3;
			//$save_main['messenger_user_id'] 		= $userId;
			$save_log['mr_status_id'] 				= 3;
		}
		$save_main['mr_user_id']				= $user_id;
		$save_main['mr_round_id']				= $req->get('round');
		$save_main['mr_topic']					= $req->get('topic');
		$save_main['quty']						= $req->get('quty');

		$mr_work_main_id 							= $work_mainDao->save($save_main);
		if(strlen($mr_work_main_id)<=1){
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."000000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=2){
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."00000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=3){
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."0000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=4){
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=5){
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."00".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=6){
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy")."0".$workmr_work_main_id_main_id;
		}else if(strlen($mr_work_main_id)<=7){
			$Nbarcode['mr_work_barcode'] = "BHO".date("dmy").$mr_work_main_id;
		}else{
				$num_run 			= substr($mr_work_main_id, -7); 
				$Nbarcode['mr_work_barcode'] = "BHO".date("dmy").$num_run;
		} 
		$barcodeok = $Nbarcode['mr_work_barcode'];
		$work_mainDao->save($Nbarcode,$mr_work_main_id);
		
		
	////////////////////////////////////////////////////////////
		$save_byhand['mr_work_main_id']			= $mr_work_main_id;
		$save_byhand['mr_work_byhand_type_id']	= $req->get('type_send');
		$save_byhand['mr_send_emp_id']			= $req->get('emp_id_send'); 
		$save_byhand['mr_send_emp_detail']		= $req->get('emp_send_data');
		$save_byhand['mr_cus_name']				= $req->get('name_re')." ".$req->get('lname_re');
		$save_byhand['mr_cus_tel']				= $req->get('tel_re');
		$save_byhand['mr_address']				= $req->get('address_re');
		if($req->get('post_code_re')!= ''){
			$save_byhand['mr_post_code']			= $req->get('post_code_re');
		}
		if($req->get('dep_id_send')!= ''){
			$save_byhand['mr_send_dep_id']			= $req->get('dep_id_send');
		}
		if($req->get('mr_work_byhand_payment_type_id')!= ''){
			$save_byhand['mr_work_byhand_payment_type_id']			= $req->get('mr_work_byhand_payment_type_id');
		}

		$sub_districts_code_re											=  $req->get('sub_districts_code_re');
		$districts_code_re												=  $req->get('districts_code_re');
		$provinces_code_re												=  $req->get('provinces_code_re');
			if(!empty($sub_districts_code_re)) {
				$locate = $sub_districtDao->getLocation($sub_districts_code_re, $districts_code_re, $provinces_code_re);
				$save_byhand['mr_sub_district_id'] 					= (!empty($locate['mr_sub_districts_id']) ? (int)$locate['mr_sub_districts_id'] : NULL);;
				$save_byhand['mr_district_id'] 						= (!empty($locate['mr_districts_id']) ? (int)$locate['mr_districts_id'] : NULL);
				$save_byhand['mr_province_id'] 						= (!empty($locate['mr_provinces_id']) ? (int)$locate['mr_provinces_id'] : NULL);;
			}
		
		$work_inout_id	= $work_byhandDao->save($save_byhand);
		$save_log['mr_user_id'] 									= $user_id;
		$save_log['mr_work_main_id'] 								= $mr_work_main_id;
		$save_log['remark'] 										= "mailroom_สร้างราายการนำส่ง";
		$work_logDao->save($save_log);



		echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
			

	}
	catch ( Exception $e )
	{
		echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}	
}
		

}



 ?>
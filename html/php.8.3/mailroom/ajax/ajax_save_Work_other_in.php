<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'Dao/Work_other.php';
require_once 'nocsrf.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'sesstion หมดอายุ กรุณา LOG IN' ));
	exit();
} else {

	
try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
 
   exit();
}


$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();

$work_otherDao 		= new Dao_Work_other();
$user_id			= $auth->getUser();


$barcode = $req->get('post_barcode');

// echo json_encode(
// 	array(
// 		'status' 		=> 505, 
// 		'message' 		=> 'บันทึกสำเร็จ' ,
// 		'barcodeok' 	=> $barcodeok ,
// 		'token'			=>NoCSRF::generate( 'csrf_token' )
// 	));
// exit();



try{

    $save_main['mr_work_date_sent']			= $req->get('date_send');
    //$save_main['mr_work_barcode']			= $barcode;
    $save_main['mr_work_remark']			= $req->get('work_remark');
    $save_main['mr_type_work_id']			= 8;
    $save_main['mr_status_id']				= 3;
    $save_main['mr_user_id']				= $user_id;
    $save_main['mr_round_id']				= $req->get('round');
    $save_main['mr_topic']					= $req->get('topic');
    $save_main['quty']						= $req->get('quty');

	$mr_work_main_id 							= $work_mainDao->save($save_main);
		if(strlen($mr_work_main_id)<=1){
			$Nbarcode['mr_work_barcode'] = "WO".date("dmy")."000000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=2){
			$Nbarcode['mr_work_barcode'] = "WO".date("dmy")."00000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=3){
			$Nbarcode['mr_work_barcode'] = "WO".date("dmy")."0000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=4){
			$Nbarcode['mr_work_barcode'] = "WO".date("dmy")."000".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=5){
			$Nbarcode['mr_work_barcode'] = "WO".date("dmy")."00".$mr_work_main_id;
		}else if(strlen($mr_work_main_id)<=6){
			$Nbarcode['mr_work_barcode'] = "WO".date("dmy")."0".$workmr_work_main_id_main_id;
		}else if(strlen($mr_work_main_id)<=7){
			$Nbarcode['mr_work_barcode'] = "WO".date("dmy").$mr_work_main_id;
		}else{
				$num_run 			= substr($mr_work_main_id, -7); 
				$Nbarcode['mr_work_barcode'] = "WO".date("dmy").$num_run;
		} 
		$barcodeok = $Nbarcode['mr_work_barcode'];
		$work_mainDao->save($Nbarcode,$mr_work_main_id);
	
////////////////////////////////////////////////////////////
$emp_id_re = $req->get('emp_id_re');
    $save_inout['mr_work_main_id']			= $mr_work_main_id;
	if($emp_id_re != ''){
    	$save_inout['mr_emp_id']			= $emp_id_re ; 
	}
    $save_inout['mr_floor_id']			= $req->get('building'); 
	$work_inout_id	= $work_inoutDao->save($save_inout);


////////////////////////////////////////////////////////////
$save_other['mr_work_main_id']			= $mr_work_main_id;
$save_other['sys_timestamp']			= date("Y-m-d H:i:s");
$save_other['mr_cus_name']				= $req->get('name_send'); 
$save_other['mr_cus_lname']				= $req->get('lname_send'); 
$save_other['mr_send_emp_id']			= $emp_id_re;
$save_other['mr_address']				= $req->get('address_send'); 

if($req->get('dep_re') != ''){
	$save_other['mr_department_id']		= $req->get('dep_re'); 
}
if($req->get('cost_id') != ''){
	$save_other['mr_cost_id']				= $req->get('cost_id');	
}

$work_inout_id	= $work_otherDao->save($save_other);
	$save_log['mr_user_id'] 									= $user_id;
	$save_log['mr_status_id'] 									= 3;
	$save_log['mr_work_main_id'] 								= $mr_work_main_id;
	$save_log['remark'] 										= "mailroom_สร้างราายการนำส่ง";
	$work_logDao->save($save_log);

	echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
		

}catch ( Exception $e )
{
 	echo json_encode(
		array('status' => 500, 
		'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,
		'e' => $e->getMessage() ,
		'token'=>NoCSRF::generate( 'csrf_token' )));
 	exit();
 }	
		

}



 ?>
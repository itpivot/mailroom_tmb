<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';
require_once 'nocsrf.php';
require_once 'Dao/Work_post.php';
require_once 'Dao/Round_resive_thaipost_in.php';

$auth 			    = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$req 								= new Pivot_Request();                                  
$work_logDao 						= new Dao_Work_log();                                   
$work_mainDao 						= new Dao_Work_main();                                  
$work_inoutDao 						= new Dao_Work_inout();                                 
$userDao 							= new Dao_User();                                 
$empDao 							= new Dao_Employee();   
$work_postDao 						= new Dao_Work_post();
$round_resive_thaipost_inDao 		= new Dao_Round_resive_thaipost_in();



$page 				= $req->get('page');
$barcode 			= $req->get('barcode');
$date_send 			= $req->get('date_send');
$round 				= $req->get('round');
$csrf_token 		= $req->get('csrf_token');
$user_id			= $auth->getUser();

if(preg_match('/<\/?[^>]+(>|$)/', $page)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
 
   exit();
}


if($page =='cancle'){
	$id = $req->get('id');
	if($id!=''){
		$work_postdata 	= $round_resive_thaipost_inDao->remove($id);
	}
	$res['token'] = NoCSRF::generate( 'csrf_token' );
	echo json_encode($res);	
}else if($page =='cancle_multiple'){
	$id = $req->get('id');
	$all_id = explode(",", $id);
		foreach($all_id as $val_id){
			if($val_id!=''){
				$work_postdata 	= $round_resive_thaipost_inDao->remove($val_id);
			}
		}
	$res['token'] = NoCSRF::generate( 'csrf_token' );
	echo json_encode($res);	
}else if($page =='saveBarcode'){

	$work_postdata 	= array();
	$barcodeSave 	= array();
	$work_postdata 	= $round_resive_thaipost_inDao->Ch_barcode($barcode);
	$error  = '';
	$button = '';
	if(count($work_postdata)>=1){
		if(isset($work_postdata[count($work_postdata)-1]['mr_round_resive_thaipost_in_id'])){
			if(empty($work_postdata[count($work_postdata)-1]['mr_round_resive_thaipost_in_id'])){
				$error  = 'เกิดข้อผิดพลาดไม่พบ id ของระบบ';
			}

			if($work_postdata[count($work_postdata)-1]['is_receiver'] == 1){
				$error  = 'Barcode นี้รับเข้าระบบแล้ว';
			}
		}else{
			$error  = 'เกิดข้อผิดพลาดไม่พบ id ของระบบ';
		}
	}else{
		$error  = 'ไม่พบ Barcode ที่นำเข้าระบบ ต้องการบันทึกหรือไม่ ';
		// $button = '<button type="button" class="btn btn-primary btn-sm" onclick="save_newdata()">บันทึก</button>';
		$button = '<a href="#" onclick="save_newdata(); return false;">บันทึก</a>';
	}

	if($error==''){
		
	$byVal = array();
	$byVal['date_send'] 	= $date_send;
	$byVal['mr_round_id'] 	= $round;
	$byVal['time_receive'] 	= date("Y-m-d H:i:s");
	$byVal['mr_user_id'] 	= $user_id;
	$byVal['is_receiver'] 	= 1;
		$mr_round_resive_thaipost_in_id = $work_postdata[count($work_postdata)-1]['mr_round_resive_thaipost_in_id'];
		$barcodeSave = $round_resive_thaipost_inDao->Save($byVal,$mr_round_resive_thaipost_in_id);
	}

	$res = array();
	$res['data'] = empty($work_postdata)?array():$work_postdata[count($work_postdata)-1];
	$res['error'] = $error;
	$res['button'] = $button;
	$res['status'] = 200;
	$res['barcodeSave'] = $barcodeSave;
	$res['message'] = '-------------------';
	$res['token'] = NoCSRF::generate( 'csrf_token' );
	echo json_encode($res);	
	//echo "---------------------";
	exit;
}else if($page =='saveremark'){
	$mr_round_resive_thaipost_in_id = $req->get('mr_round_resive_thaipost_in_id');
	$remark = $req->get('remark');

	$data_ss['remark'] 	= $remark;
	// update data
	$id = $round_resive_thaipost_inDao->save($data_ss,$mr_round_resive_thaipost_in_id);
	if(!empty($id)){
		echo json_encode(array(
			'status' => 200,
			'message' => 'บันทึกเรียบร้อย'
		));
		exit;
	}else{
		echo json_encode(array(
			'status' => 500,
			'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'
		));
		exit();
	}
}else if($page =='savenewbarcode'){
	$barcode 			= $req->get('barcode');
	$date_send 			= $req->get('date_send');
	$round 				= $req->get('round');
	$csrf_token 		= $req->get('csrf_token');

	$data_ss['sysdate'] 		= date('Y-m-d H:i:s');
	$data_ss['time_receive'] 	= date('Y-m-d H:i:s');
	$data_ss['date_send'] 		= $date_send;
	$data_ss['mr_round_id'] 	= $round;
	$data_ss['barcode'] 		= $barcode;
	$data_ss['active'] 			= 1;
	$data_ss['is_receiver'] 	= 0;
	$data_ss['mr_user_id'] 		= $user_id;
	$data_ss['remark'] 			= "ไม่มีในรายการเอกการที่อัพโหลด";

	$id = $round_resive_thaipost_inDao->save($data_ss);
	if(!empty($id)){
		echo json_encode(array(
			'status' => 200,
			'message' => 'บันทึกเรียบร้อย'
		));
		exit;
	}else{
		echo json_encode(array(
			'status' => 500,
			'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'
		));
		exit();
	}
} else{
	
	$val = array();
	$val['date'] 		= $req->get('date_report');
	$val['mr_round_id'] = $req->get('round_printreper');
    $data = array();
	$data = $round_resive_thaipost_inDao->barcode_resive_thaipost_in($val);
	foreach($data as $key=>$val){
		if($val['is_receiver'] == 1){
			$data[$key]['is_ch'] = '<i class="material-icons text-success">check_box</i>';
		}else{
			$data[$key]['is_ch'] = '';
		}
		$data[$key]['num'] = $key+1;
		if($data[$key]['remark'] == "ไม่มีในรายการเอกการที่อัพโหลด"){
			$data[$key]['action'] = '<button type="button" onclick="cancle_work('.$val['mr_round_resive_thaipost_in_id'].')" class="btn btn-danger btn-sm">ลบ</button>';
		}else{
			$data[$key]['action'] = '<button type="button" onclick="cancle_work('.$val['mr_round_resive_thaipost_in_id'].')" class="btn btn-danger btn-sm">ลบ</button>&nbsp;<button type="button" onclick="chang(\''.$val['mr_round_resive_thaipost_in_id'].'\',\''.$val['remark'].'\')" class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalchang">แก้ไข</button>';
		}
		$data[$key]['check']='	
			<label class="custom-control custom-checkbox"  >
				<input value="'.$val['mr_round_resive_thaipost_in_id'].'" id="select-all" name="select_data" id="ch_'.$val['mr_round_resive_thaipost_in_id'].'" type="checkbox" class="custom-control-input">
				<span class="custom-control-indicator"></span>
				<span class="custom-control-description"></span>
			</label>
		';
	}
	
	echo json_encode(
		array(
			'status' => 200, 
			'token' => NoCSRF::generate( 'csrf_token' ),
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);

			exit;

	}
 ?>

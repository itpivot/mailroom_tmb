<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/User.php';
require_once 'Dao/Zone.php';
require_once 'Dao/History_import_emp.php';
$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();
$branchDao			= new Dao_Branch();
$userDao 			= new Dao_User();   
$historyDao 		= new Dao_History_import_emp();
$zoneDao 			= new Dao_zone();

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Atapy_Site::toLoginPage();
    exit;
}


$page 	= $req->get('page');


if($page=='resign'){
    $date_import                        = date('Y-m-d');
    $emp_code                           = $req->get('mr_emp_code');
    if($emp_code != ''){
        $resign = $employeeDao->checkEmpResign($emp_code);
        if ( $resign != "Resign" ) {
            $data_delete['mr_date_import'] 	= $date_import;
            $data_delete['mr_emp_email'] 	= "Resign";
            $data_delete['mr_emp_code'] 	= "Resign";
            $data_delete['old_emp_code'] 	=  $emp_code ;
            $history_data['mr_emp_id'] 				= $emp_id_delete;
            $history_data['action'] 				=   $emp_code;
            
            
            $emp_id_delete = $employeeDao->updateEmpByID($data_delete,$emp_code);
            $userDao->Update_delete_User($emp_code);
            $historyDao->save($history_data);
            
            $index_valueD++;
            $num++;
        }
        $msg['error'] = 'success';
        $msg['msg'] = 'บันทึกข้อมูลเรียบร้อย';
        echo json_encode($msg);
        exit;
    }else{
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "ไม่พบรหัสพนักงานนี้"';
        echo json_encode($msg);
        exit;
    }
}elseif($page == 'edit' ){
    
    $emp_id 					= $req->get('emp_id');
    $edit_email 				= $req->get('edit_email');
    $edit_name 					= $req->get('edit_name');
    $edit_lname 				= $req->get('edit_lname');
    $edit_branch 				= $req->get('edit_branch');
    $ss['mr_emp_email']         = $edit_email;
    $ss['mr_emp_name']          = $edit_name;
    $ss['mr_emp_lastname']      = $edit_lname;
    if($edit_branch == ''){
        $ss['mr_branch_id']         = null;
    }else{
        $ss['mr_branch_id']         = $edit_branch;
    }
    if($edit_branch != ''){
        $ss['mr_floor_id']          = null;
    }
    if ($emp_id!= ''){
        if($employeeDao ->save($ss,$emp_id)){
            $msg['error'] = 'success';
            $msg['msg'] = 'บันทึกข้อมูลเรียบร้อย';
        }else{
            $msg['error'] = 'error';
            $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';
        }
    }else{
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';  
    }
        echo json_encode($msg);
    exit;
}else{
    $emp_hub 	= $req->get('emp_hub');//mr_hub_id
    $emptype 	= $req->get('emptype');
    $group 	    = $req->get('group');
    $code 		= $req->get('code');
    $name 		= $req->get('name');
    $lname 		= $req->get('lname');
    $email 		= $req->get('email');
    $branch 	= $req->get('branch');
    $department = $req->get('department');
    $position 	= $req->get('position');
    $floor 	    = $req->get('floor');
    $mr_emp_tel = $req->get('tel');
    $sql        = "SELECT mr_emp_code FROM mr_emp WHERE mr_emp_code LIKE '".$code."'";
    $data       = $branchDao->selectdata($sql);
if(!empty($data)){
    $msg['error'] = 'error';
    $msg['msg'] = 'เกิดข้อผิดพลาด : "รหัสพนักงงานนี้มีข้อมูลไนระบบแล้วไม่สามารถเพิ่มได้"';
    echo json_encode($msg);
    exit;
}
if($group =="pivot"){
    if(empty($emptype)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุสถานที่ปฏิบัติงาน"';
        echo json_encode($msg);
        exit;
    }
    if(empty($code)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ รหัสพนักงาน"';
        echo json_encode($msg);
        exit;
    }
    if(empty($name)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ ชื่อ พนักงาน"';
        echo json_encode($msg);
        exit;
    }
    if(empty($lname)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ นามสกุล"';
        echo json_encode($msg);
        exit;
    }
    // if($emptype == 1 && empty($floor)){
    //     $msg['error'] = 'error';
    //     $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ ชั้น "';
    //     echo json_encode($msg);
    //     exit;
    // }
    if($emptype == 2 && empty($emp_hub)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ ศูน "';
        echo json_encode($msg);
        exit;
    }

}else{
   
    if(empty($emptype)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุสถานที่ปฏิบัติงาน"';
        echo json_encode($msg);
        exit;
    }
    if(empty($code)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ รหัสพนักงาน"';
        echo json_encode($msg);
        exit;
    }
    if(empty($name)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ ชื่อ พนักงาน"';
        echo json_encode($msg);
        exit;
    }
    if(empty($lname)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ นามสกุล"';
        echo json_encode($msg);
        exit;
    }
    if(empty($email)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ อีเมล"';
        echo json_encode($msg);
        exit;
    }
    if(empty($mr_emp_tel)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ เบอร์โทร"';
        echo json_encode($msg);
        exit;
    }
    if(empty($branch)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ ที่อยู่สาขา"';
        echo json_encode($msg);
        exit;
    }
    if(empty($department)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ แผนก"';
        echo json_encode($msg);
        exit;
    }
    if(empty($position)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ ตำแหน่ง"';
        echo json_encode($msg);
        exit;
    }
    if($emptype == 1 && empty($floor)){
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาระบุ ชั้น "';
        echo json_encode($msg);
        exit;
    }
}



    
    if(empty($data)){
        $ss['mr_date_import']          = date('Y-m-d');
        $ss['emp_type']            = $group ;
        $ss['mr_emp_name']          = $name;
        $ss['mr_emp_lastname']      = $lname;
        $ss['mr_emp_code']          = $code;
        $ss['old_emp_code']         = $code;
        $ss['mr_emp_tel']           = $mr_emp_tel;
        $ss['mr_emp_email']         = $email;
        if(!empty($department)){
            $ss['mr_department_id']     = $department;
        }
        if(!empty($position)){
            $ss['mr_position_id']       = $position;
        }



        if(!empty($emp_hub)){
            $ss['mr_hub_id']            = $emp_hub;
        }
        if($emptype == 1){
            if($floor!=''){
                $ss['mr_floor_id']          = $floor;
            }
        }
        if(!empty($branch)){
            $ss['mr_branch_id']         = $branch;
        }
        $emp_id  = $employeeDao ->save($ss);
        if($group =="pivot"){
            if($emp_id){
                $s_user['mr_user_password']     = "TGwzSVQ0cElKajY1eHk4RjZ4WHJxdz09";
                $s_user['mr_user_username']     = $code;
                $s_user['is_first_login']       = 1;
                $s_user['date_create']          = date('Y-m-d H:i:s');
                $s_user['change_password_date'] = "0000-00-00 00:00:00";
                $s_user['active']               = "1";
                $s_user['mr_emp_id']           = $emp_id;
                if($emptype == 1){
                    $s_user['mr_user_role_id']      = 4;
                    $user_id = $userDao->save($s_user);
                    if($user_id){
                        $msg['error'] = 'success';
                        $msg['msg'] = 'บันทึกข้อมูลเรียบร้อย user :'.$code.' pass:1234';
                    }else{
                        $msg['error'] = 'error';
                        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';
                    }
                    echo json_encode($msg);
                    exit;
                }else{
                    
                    $s_user['mr_user_role_id']      = 8;
                    //$mr_floor_id            = $floor;
                    //$s_zone['mr_user_id']   = $user_id;
                    //$zone_id                = $zoneDao->save();
                    $user_id                = $userDao->save($s_user);
                    if($user_id){
                        $msg['error'] = 'success';
                        $msg['msg'] = 'บันทึกข้อมูลเรียบร้อย user :'.$code.' pass:1234';
                    }else{
                        $msg['error'] = 'error';
                        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';
                    }
                    echo json_encode($msg);
                    exit;
                 
                }
            }
        }else{

                
                if($emp_id){
                    $msg['error'] = 'success';
                    $msg['msg'] = 'บันทึกข้อมูลเรียบร้อย';
                }else{
                    $msg['error'] = 'error';
                    $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';
                }
                echo json_encode($msg);
                exit;
        }
    }else{
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "รหัสพนักงงานนี้มีข้อมูลไนระบบแล้วไม่สามารถเพิ่มได้"';
        echo json_encode($msg);
        exit;
    }
}
?>

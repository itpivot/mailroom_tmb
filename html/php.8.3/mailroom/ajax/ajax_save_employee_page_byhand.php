<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/User.php';
require_once 'Dao/Zone.php';
require_once 'Dao/History_import_emp.php';

$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();
$branchDao			= new Dao_Branch();
$userDao 			= new Dao_User();   
$historyDao 		= new Dao_History_import_emp();
$zoneDao 			= new Dao_zone();

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Atapy_Site::toLoginPage();
    exit;
}




$page 	        = $req->get('page');

$emp_code_add 	= $req->get('emp_code_add');
$emp_mail_add 	= $req->get('emp_mail_add');
$emp_name_add 	= $req->get('emp_name_add');
$emp_lname_add 	= $req->get('emp_lname_add');
$dep_id_add 	= $req->get('dep_id_add');

if($page == "add_emp"){

    $sql        = "SELECT mr_emp_code FROM mr_emp WHERE mr_emp_code LIKE '".$emp_code_add."'";
    $data       = $branchDao->selectdata($sql);
    if(!empty($data)){
        $msg['st'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "รหัสพนักงงานนี้มีข้อมูลไนระบบแล้วไม่สามารถเพิ่มได้"';
        echo json_encode($msg);
        exit;
    }else{
        $ss['mr_date_import']          = date('Y-m-d');
        $ss['emp_type']             = "tmb" ;
        $ss['mr_emp_name']          = $emp_name_add;
        $ss['mr_emp_lastname']      = $emp_lname_add;
        $ss['mr_emp_code']          = $emp_code_add;
        $ss['old_emp_code']         = $emp_code_add;
        $ss['mr_emp_tel']           = $mr_emp_tel;
        $ss['mr_emp_email']         = $emp_mail_add;
        $ss['mr_department_id']     = $dep_id_add;
        $emp_id  = $employeeDao ->save($ss);
    }

     if($emp_id){
         $msg['st'] = 'success';
         $msg['msg'] = 'บันทึกข้อมูลเรียบร้อย';
     }else{
         $msg['st'] = 'error';
         $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';
     }
     echo json_encode($msg);
     exit;
    
}
?>

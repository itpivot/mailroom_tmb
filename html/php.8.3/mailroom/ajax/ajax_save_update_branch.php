<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Employee.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$branchDao 			= new Dao_Branch();                                 
$employeeDao 		= new Dao_Employee();   

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}                              
 
$page	= $req->get('page');
if(preg_match('/<\/?[^>]+(>|$)/', $page)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

if($page == 'add'){
	$update_branch 			= array(); 
	$type_id				= $req->get('mr_branch_type_id');
	$mr_branch_id			= $req->get('mr_branch_id');
	$branchname	    		= $req->get('branchname');
	$mr_branch_category	    = $req->get('mr_branch_category');
	$brancode				= $req->get('brancode');


	if(preg_match('/<\/?[^>]+(>|$)/', $brancode)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $branchname)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $type_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(empty($brancode)){
		echo json_encode(array('status' => 500, 'message' => 'กรุณาระบุ รหัสสาขา'));
		exit();
	}
	if(empty($branchname)){
		echo json_encode(array('status' => 500, 'message' => 'กรุณาระบุ ชื่อสาขา'));
		exit();
	}

	if($type_id<=2){
		$update_branch['mr_branch_type']= $type_id;
	}else{
		$update_branch['mr_branch_type']= $type_id;
		$update_branch['mr_hub_id']= null;
	}
	$update_branch['mr_branch_name']= $branchname;
	$update_branch['mr_branch_code']= $brancode;
	$update_branch['mr_branch_category_id']= $mr_branch_category;

	$sql2 = "SELECT 
					* 
				FROM mr_branch 
				WHERE mr_branch_code LIKE '".$brancode."'
				";
	$ch_code = $userDao ->select($sql2);
    
	if(empty($ch_code)){
		$val = $branchDao->save($update_branch);
		echo json_encode(array('status' => 200, 'message' => ' บันทึกเรียบร้อย'));
		exit;
	}else{
		echo json_encode(array('status' => 500, 'message' => 'Bar code ซ้ำ'));
		exit();
	}



}else if($page == 'edit'){
	$update_branch 			= array(); 
	$type_id				= $req->get('mr_branch_type_id');
	$mr_branch_id			= $req->get('mr_branch_id');
	$branchname	    		= $req->get('branchname');
	$mr_branch_category	    = $req->get('mr_branch_category');
	$brancode				= $req->get('brancode');

	
	if(preg_match('/<\/?[^>]+(>|$)/', $type_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}

	if($type_id<=2){
		$update_branch['mr_branch_type']= $type_id;
	}else{
		$update_branch['mr_branch_type']= $type_id;
		$update_branch['mr_hub_id']= null;
	}
	$update_branch['mr_branch_name']= $branchname;
	//$update_branch['mr_user_username_old']= $branchname_old;
	
		$sql2 = "SELECT 
					* 
				FROM mr_branch 
				WHERE mr_branch_code LIKE '".$branchname_old."'
				and mr_branch_id != ".$mr_branch_id."
				";
	$ch_code = $userDao ->select($sql2);
	if(empty($ch_code)){
		if($mr_branch_id!=''){
			$val = $branchDao->save($update_branch,$mr_branch_id);
			echo json_encode(array('status' => 200, 'message' => ' บันทึกเรียบร้อย'));
			exit();
		}else{
			echo json_encode(array('status' => 500, 'message' => 'ไม่พบ id ที่ต้องการแก้ไข'));
			exit();
		}
	}else{
		echo json_encode(array('status' => 500, 'message' => 'Bar code ซ้ำ'));
		exit();
	}
	
	
}else if($page == 'chang_type'){
	$update_branch 	= array(); 
	$type_id		= $req->get('type_id');
	$mr_branch_id	= $req->get('mr_branch_id');
	if(preg_match('/<\/?[^>]+(>|$)/', $type_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}

	if($type_id<=2){
		$update_branch['mr_branch_type']= $type_id;
	}else{
		$update_branch['mr_branch_type']= $type_id;
		$update_branch['mr_hub_id']= null;
	}
	if($mr_branch_id!=''){
		$val = $branchDao->save($update_branch,$mr_branch_id);
	}
}elseif($page == 'chang_hub'){
	$hub_id			= $req->get('hub_id');
	$mr_branch_id	= $req->get('mr_branch_id');
	if(preg_match('/<\/?[^>]+(>|$)/', $hub_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}

	$update_branch 	= array(); 

	$update_branch['mr_hub_id']= $hub_id;
	if($mr_branch_id!=''){
		$val = $branchDao->save($update_branch,$mr_branch_id);
	}
}elseif($page == 'remove'){
	$hub_id			= $req->get('hub_id');
	$mr_branch_id	= $req->get('mr_branch_id');
	if(preg_match('/<\/?[^>]+(>|$)/', $hub_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	$update_branch 	= array(); 
	$update_branch['active']= 0;
	if($mr_branch_id!=''){
		$val = $branchDao->save($update_branch,$mr_branch_id);
	}
}elseif($page == 'mess_chang_hub'){
	$hub_id	= $req->get('hub_id');
	$emp_id	= $req->get('emp_id');
	if(preg_match('/<\/?[^>]+(>|$)/', $hub_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	$update_branch 	= array(); 
	$update_branch['update_date']= date('Y-m-d H:i:s');
	$update_branch['mr_hub_id']= $hub_id;
	if($emp_id!=''){
		$val = $employeeDao->save($update_branch,$emp_id);
	}
}else{

}
	
//echo '<<>>';
echo json_encode($val);
 ?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';

$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();
$auth 				= new Pivot_Auth();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$mr_branch_id 	= '';
$main_id  		= $req->get('main_id');
$mr_user_id  	= $req->get('mr_user_id');
$tnt_tracking  	= $req->get('tnt_tracking');
$hub_id  		= $req->get('hub_id');

if(preg_match('/<\/?[^>]+(>|$)/', $main_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_user_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $tnt_tracking)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $hub_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$arr_main_id 	= explode(",",$main_id);
$date = date('H:i:s');
$user_id = $auth->getUser();
if($date<='10:00:00'){
	$mr_round = "1";
}else{
	$mr_round = "2";
}

$params_tnt_barcode 	= array(); // condition: generate ?,?,?
$params_tnt_barcode 	= str_repeat('?,', count($arr_main_id) - 1) . '?'; // example: ?,?,?

$sql_tnt_barcode = "
		SELECT 
		io.mr_work_main_id,
		tnt.mr_tnt_barcode_import_id,
		tnt.mr_branch_id as tnt_branch_id,
		io.mr_branch_id as out_branch_id,
		tnt.barcode_tnt 
		FROM mr_work_inout io
		left join(
		SELECT 
			mr_branch_id,
			mr_tnt_barcode_import_id ,
			barcode_tnt 
		FROM mr_tnt_barcode_import 
		WHERE active = 0
		) tnt on(tnt.mr_branch_id = io.mr_branch_id)
		WHERE io.mr_work_main_id in(".$params_tnt_barcode.")
	";
$tnt_barcode = $send_workDao->select_send_work($sql_tnt_barcode,$arr_main_id);


$barcode =array();
$arr_tnt_barcode_import_id =array();
foreach($tnt_barcode as $index_i 	=> $val_i){

	$mr_work_main_id 									= $val_i['mr_work_main_id'];
	$mr_tnt_barcode_import_id 							= $val_i['mr_tnt_barcode_import_id'];
	
	$barcode[$mr_work_main_id]							= $val_i['barcode_tnt'];
	$arr_tnt_barcode_import_id[$mr_work_main_id]		= $val_i['mr_tnt_barcode_import_id'];
}


$sql2 = "UPDATE mr_work_main 
			SET messenger_user_id = '".$user_id."' ,
		mr_status_id = 11
		WHERE mr_work_main_id in(".$main_id.");";
		
$sql3 = "UPDATE mr_work_main 
			SET mr_status_id = 11
		WHERE mr_work_main_id in(".$main_id.");";

$no = 1;
$date_ = date('Y-m-d H:i:s');
foreach($arr_main_id as $key => $val_){
	if(isset($barcode[$val_])){
		$tnt_tracking = $barcode[$val_];
	}
	if($tnt_tracking != '' or ($hub_id != 'all_branch' and $hub_id != 'all_hub' )){
		if( $no == 1 ){
			$sql = "INSERT INTO mr_round_send_work( mr_round, mr_user_id, mr_work_main_id,tnt_tracking_no) VALUES ";
			$sqllog = "INSERT INTO mr_work_log(sys_timestamp, mr_work_main_id, mr_user_id, mr_status_id) VALUES ";

		}
		if( (count($arr_main_id)-1)== $key  || $no >= 1000){
			$sql .= "('".$mr_round."',".$user_id.",".$val_.",'".$tnt_tracking."'); ";
			$sqllog .= "('".$date_."',".$val_.",".$user_id.",11); ";
		}else{
			$sql .= "('".$mr_round."',".$user_id.",".$val_.",'".$tnt_tracking."'), ";
			$sqllog .= "('".$date_."',".$val_.",".$user_id.",11), ";
		}
		if($no >= 1000){
			$no = 1;
		}else{
			$no += 1;
		}
	}else{
		unset($arr_main_id[$key]);
	}
}

$main_id 				= implode(",", $arr_main_id);
$tnt_barcode_import_id 	= implode(",", $arr_tnt_barcode_import_id);

$sql2 = "UPDATE mr_work_main 
			SET messenger_user_id = '".$user_id."' ,
		mr_status_id = 11
		WHERE mr_work_main_id in(".$main_id.");";
		
$sql3 = "UPDATE mr_work_main 
			SET mr_status_id = 11
		WHERE mr_work_main_id in(".$main_id.");";

$sql_barcode_import = "UPDATE mr_tnt_barcode_import
				SET  active = 1
		 WHERE mr_tnt_barcode_import_id in(".$tnt_barcode_import_id.");";

if($main_id != ''){
	if($sql != ''){
		if($hub_id == 'all_branch'){
			$res['msg'] = $send_workDao->query2($sql,$sql3);
		}else{
			$res['msg'] = $send_workDao->query2($sql,$sql2);
		}
		$res['msg2'] 	= $send_workDao->query2($sqllog,$sql_barcode_import);
		//$res['msg3'] = $sqllog;
	}else{
		$res['msg'] = 'error';
	}
}else{
	$res['msg'] = 'error';
}

if($res['msg'] == 'error'){
	echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาด'));
}else{

	echo json_encode(array(
		'status' => 200,
		'data' => $emp_data
	));
}
?>

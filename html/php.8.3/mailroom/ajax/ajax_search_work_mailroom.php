<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
ini_set('memory_limit', '-1');

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();   
$auth 				= new Pivot_Auth(); 

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$barcode 				=  $req->get('barcode');                           
$receiver 				=  $req->get('receiver');                         
$sender 				=  $req->get('sender');                             
$start_date 			=  $req->get('start_date');                     
$end_date 				=  $req->get('end_date');                                 
$status 				=  $req->get('status');  
$select_mr_contact_id 	=  $req->get('select_mr_contact_id');  


$data_search['barcode']  					=  $barcode;                           
$data_search['receiver']  					=  $receiver;                         
$data_search['sender']  					=  $sender;                             
$data_search['start_date']  				=  $start_date;                     
$data_search['end_date']  					=  $end_date;                                 
$data_search['status']  					=  $status;   
$data_search['select_mr_contact_id']  		=  $select_mr_contact_id;   

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $receiver)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $sender)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(!empty($start_date)){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$start_date)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
}

if(!empty($end_date)){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$end_date)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
}

if(preg_match('/<\/?[^>]+(>|$)/', $status)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$data_search['barcode']  		=  $barcode;                           
$data_search['receiver']  		=  $receiver;                         
$data_search['sender']  		=  $sender;                             
$data_search['start_date']  	=  $start_date;                     
$data_search['end_date']  		=  $end_date;                                 
$data_search['status']  		=  $status;     

$report = $work_inoutDao->searchMailroom( $data_search );

foreach( $report as $num => $value ){
	
	
	$report[$num]['cont'] = isset($value['cont'])?$value['cont']:'';
	$report[$num]['con_log'] = $value['con_log'];
	$report[$num]['no'] = $num +1;
	if($value['mr_type_work_id'] == 4){
		$report[$num]['name_receive'] = $value['bh_resive']." ";
		$report[$num]['name_send'] = $value['name_send_bh']." ".$value['lastname_send_bh'];
	}elseif($value['mr_type_work_id'] == 5){
		$report[$num]['name_receive'] = $value['bh_resive']." ";
		$report[$num]['name_send'] = $value['name_send_bh']." ".$value['lastname_send_bh'];
	}else{
		$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
		$report[$num]['name_send'] = $value['mr_emp_name']." ".$value['mr_emp_lastname'];	
	}
	
	$txt =  "";


	if($value['mr_type_work_id']<=3){
		if($value['mr_type_work_id']!=2){
			$txt .=  '<a class="btn-sm btn btn-outline-info" href="edit_floor_send.php?id='.urlencode(base64_encode($value['mr_work_inout_id'])).'" target="_blank"><b>'.$value['depart_floor_receive'].'</b></a>';
		}else{
			$txt .=  '<a class="btn-sm btn btn-outline-info" href="edit_floor_send.php?id='.urlencode(base64_encode($value['mr_work_inout_id'])).'" target="_blank"
			data-toggle="tooltip" data-placement="right" title="แก้ไขชั้นผู้รับ"><b>
			<i class="material-icons">
			edit
			</i></b></a>';
		}
	}else{
		$txt = '';
	}

	$report[$num]['depart_floor_receive'] = $txt;
	
	$txt_barcode = "";
	$txt_barcode .=  '<a href="detail_receive.php?barcode='.urlencode(base64_encode($value['mr_work_barcode'])).'" target="_blank"><b>'.$value['mr_work_barcode'].'</b></a>';
	if ( $value['mr_status_id'] == 5 ) {
		$report[$num]['mr_work_barcode'] = $txt_barcode;
	}else{
		$report[$num]['mr_work_barcode'] = $value['mr_work_barcode'];
	}
	
	
	$txtdropdown='<div class="btn-group" role="group" aria-label="Basic example">';
	$txtdropdown2='</div>';
	if( $value['mr_status_id'] != 15 ){
		$txtdropdown.='
		
		<a class="btn btn-sm btn-outline-success" href="success_work_by_mailroom.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"
		data-toggle="tooltip" data-placement="right" title="สำเร็จนอกระบบ"
		><b><i class="material-icons">
		done_all
		</i></b></a>';
	}
				
	
	
	
	
	
	$txt_cancle =  "";
	$txt_cancle .=  '<a 
	data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="../mailroom/work_info.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
	dehaze
	</span></a>';
	$txt_cancle .=  '<a class="btn btn-sm btn-outline-warning" href="cancle_work_by_mailroom.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank" 
	data-toggle="tooltip" data-placement="right" title="ยกเลิกการส่ง"
	><b>
	<i class="material-icons">report_problem</i></b></a>';
	
	$txt_success =  "";
	$txt_success .=  '<a class="btn btn-outline-success" href="success_work_by_mailroom.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><b>สำเร็จนอกระบบ</b></a>';
	
	
	$report[$num]['cancle']= $txtdropdown;
	$report[$num]['cancle'] .= $txt_cancle;
	
	if($value['mr_type_work_id'] == 4){
		$txt_cancle =  '<a 
		data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="../mailroom/work_info.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
		dehaze
		</span></a>';
		$report[$num]['cancle'] = $txt_cancle;
	}elseif($value['mr_type_work_id'] == 5){
		$txt_cancle =  '<a 
		data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="../mailroom/work_info.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
		dehaze
		</span></a>';
		$report[$num]['cancle'] = $txt_cancle;
	}else{
		if ( $value['mr_status_id'] == 1 || $value['mr_status_id'] == 2 || $value['mr_status_id'] == 3 ) {
			//$report[$num]['cancle'] .= $txt_cancle;
		}else if ( $value['mr_status_id'] == 4 ) {
			//$report[$num]['cancle'] .= $txt_success;
		}else if ( $value['mr_status_id'] == 5 ) {
			$txt_cancle =  '<a 
				data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="../mailroom/work_info.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
				dehaze
				</span></a>';
			$report[$num]['cancle'] = $txt_cancle;
			$report[$num]['depart_floor_receive'] = '';
		}else if ( $value['mr_status_id'] == 12 ) {
			$txt_cancle =  '<a 
				data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="../mailroom/work_info.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
				dehaze
				</span></a>';
			$report[$num]['cancle'] = $txt_cancle;
		}else if ( $value['mr_status_id'] == 14 ) {
			$txt_cancle =  '<a 
				data-toggle="tooltip" data-placement="right" title="ติดตามงาน" class="btn btn-sm btn-outline-info" href="../mailroom/work_info.php?id='.urlencode(base64_encode($value['mr_work_main_id'])).'" target="_blank"><span class="material-icons">
				dehaze
				</span></a>';
			$report[$num]['cancle'] = $txt_cancle;
		}else if ( $value['mr_status_id'] == 15 ) {
			$report[$num]['cancle'] = '';
			$report[$num]['depart_floor_receive'] = '';
		}else{
			$report[$num]['cancle'] .= '';
			
		}
	}

	if ( $value['mr_status_id'] != 5 or  $value['mr_status_id'] != 12) {
		$report[$num]['ch_data'] = '
				<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
						<input name="ch_bog[]" value="'.$value['mr_work_main_id'].'" type="checkbox" class="custom-control-input">
						<span class="custom-control-indicator"></span>
					</label>
			';
	}else{
		$report[$num]['ch_data'] = '';
	}
	
	$report[$num]['cancle'] .= $txtdropdown2;
	//$report[$num]['cancle'] = '';
	
	
}

$arr['data'] = $report;
echo json_encode(array(
	'status' => 200,  
	'data' => $arr,
	'qury' => $data_search
));

 ?>

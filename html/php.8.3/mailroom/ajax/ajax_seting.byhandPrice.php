<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'nocsrf.php';
require_once 'Dao/Post_price.php';    
require_once 'Dao/Work_byhand_price.php';    
require_once 'Dao/Floor.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$userDao 			= new Dao_User();
$post_priceDao 			= new Dao_Post_price();
$work_byhand_priceDao 			= new Dao_work_byhand_price();

$user_id			= $auth->getUser();





$page  			=  $req ->get('page');//save

$re 			= array();
if($page == "editPrice"){
	try
	{
		// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
		NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
		// form parsing, DB inserts, etc.
		// ...
		$result = 'CSRF check passed. Form parsed.';
	}
	catch ( Exception $e )
	{
		// CSRF attack detected
	$result = $e->getMessage() . ' Form ignored.';
	echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	
	exit();
	}
	try
	{
		$byhand_price_id  	=  $req ->get('byhand_price_id');
		$byhand_price  		=  $req ->get('byhand_price');
		$addType  			=  $req ->get('addType');
		


		$savPrice['price']    = $byhand_price  ;

		if($addType == "edit"){
			if($byhand_price_id == ''){
				echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				$byhand_price_id	= $work_byhand_priceDao->save($savPrice,$byhand_price_id );
			}
		}
		echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'byhand_price_id' => $byhand_price_id ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(
				array('status' => 505, 
				'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,
				'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}

}else if($page == "loadPrice"){
	$data	= $work_byhand_priceDao->getpriceByhandAll();	
	foreach($data as $key=>$val){
	    $data[$key]['no'] = $key+1;
		$data[$key]['action'] ='<div class="btn-group" role="group" aria-label="Action">';
	    $data[$key]['action'] 	.='<button type="button" class="btn btn-warning btn-sm" onclick="edit_price('.$val['mr_work_byhand_price_id'].','.$val['price'].')" title="แก้ไขข้อมูล"><i class="material-icons">edit</i></button>';
	    $data[$key]['action'] .='</div">';//post_price
	}

	$is_empty = 1;
	if(empty($data)){
		$is_empty = 0;
	}
	echo json_encode(
		array(
			'status' 	=> 200, 
			'is_empty' 	=> $is_empty, 
			'data' 		=> $data, 
			'token' 	=> NoCSRF::generate( 'csrf_token' ), 
			'message' => 'สำเร็จ' 
			)
		);

}else{
	echo json_encode(
		array('status' => 505, 
		'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,
		'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
}



 ?>
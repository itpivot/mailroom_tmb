<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'nocsrf.php';
require_once 'Dao/Post_price.php';    
require_once 'Dao/Work_byhand_price.php';    
require_once 'Dao/Work_byhand_zone_location.php';    
require_once 'Dao/Sub_district.php';
require_once 'Dao/Floor.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}

	



$req 							= new Pivot_Request();
$work_logDao 					= new Dao_Work_log();
$userDao 						= new Dao_User();
$post_priceDao 					= new Dao_Post_price();
$work_byhand_priceDao 			= new Dao_work_byhand_price();
$work_byhand_zone_locationDao 	= new Dao_work_byhand_zone_location();
$sub_districtDao 				= new Dao_Sub_district();
$user_id						= $auth->getUser();





$page  	=  $req ->get('page');
$re 	= array();

if($page == "remove"){
	try
	{
		$id  							=  $req ->get('id');
		if(!empty($id)) {
			$work_byhand_zone_location_id	= $work_byhand_zone_locationDao->remove($id);	
		}else{
			$result = $e->getMessage() . ' Form ignored.';
			echo json_encode(
					array('status' => 505, 
					'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,
					'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
		}
		echo json_encode(array('status' => 200, 'message' => 'ลบข้อมูลเรียบร้อย' ,'work_byhand_zone_location_id' => $work_byhand_zone_location_id ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(
				array('status' => 505, 
				'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,
				'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}

}else if($page == "addZoneLocation"){
	try
	{
		// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
		NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
		// form parsing, DB inserts, etc.
		// ...
		$result = 'CSRF check passed. Form parsed.';
	}
	catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}
	try
	{
		$mr_work_byhand_zone_id  							=  $req ->get('mr_work_byhand_zone_id');
		$sub_districts_code_re						= $req->get('sub_districts_code_re');
		$districts_code_re							= $req->get('districts_code_re');
		$provinces_code_re							= $req->get('provinces_code_re');
			
			if(!empty($sub_districts_code_re)) {
				$locate = $sub_districtDao->getLocation($sub_districts_code_re, $districts_code_re, $provinces_code_re);
				//echo json_encode($locate);
				//exit();	
				$savez_one_location['mr_sub_districts_id'] 					= (!empty($locate['mr_sub_districts_id']) ? (int)$locate['mr_sub_districts_id'] : NULL);;
				//$savez_one_location['mr_district_id'] 						= (!empty($locate['mr_districts_id']) ? (int)$locate['mr_districts_id'] : NULL);
				//$savez_one_location['mr_province_id'] 						= (!empty($locate['mr_provinces_id']) ? (int)$locate['mr_provinces_id'] : NULL);;
			}
	
		$savez_one_location['mr_work_byhand_zone_id']    = $mr_work_byhand_zone_id  ;

		$work_byhand_zone_location_id	= $work_byhand_zone_locationDao->save($savez_one_location);	
		echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'work_byhand_zone_location_id' => $work_byhand_zone_location_id ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(
				array('status' => 505, 
				'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,
				'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}

}else if($page == "loadZone_location"){
	$work_byhand_zone_id = $req->get('work_byhand_zone_id');
	$data	= $work_byhand_zone_locationDao->getzone_locationAll($work_byhand_zone_id);	
	foreach($data as $key=>$val){
	    $data[$key]['no'] = $key+1;
		$data[$key]['action'] ='<div class="btn-group" role="group" aria-label="Action">';
	    $data[$key]['action'] 	.='<button type="button" class="btn btn-danger btn-sm" onclick="remove_zone_location('.$val['mr_work_byhand_zone_location_id'].')" title="ลขข้อมูล"><i class="material-icons">close</i></button>';
	    $data[$key]['action'] .='</div">';//post_price
	}

	$is_empty = 1;
	if(empty($data)){
		$is_empty = 0;
	}
	echo json_encode(
		array(
			'status' 	=> 200, 
			'is_empty' 	=> $is_empty, 
			'data' 		=> $data, 
			'token' 	=> NoCSRF::generate( 'csrf_token' ), 
			'message' => 'สำเร็จ' 
			)
		);

}else{
	echo json_encode(
		array('status' => 505, 
		'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,
		'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
}



 ?>
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/User.php';
require_once 'Dao/Zone.php';
require_once 'Dao/History_import_emp.php';
$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();
$branchDao			= new Dao_Branch();
$userDao 			= new Dao_User();   
$historyDao 		= new Dao_History_import_emp();
$zoneDao 			= new Dao_zone();

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Atapy_Site::toLoginPage();
    exit;
}


$page 	= $req->get('page');

if($page == 'update' ){
    $mr_emp_id 					= $req->get('mr_emp_id');
    $mr_branch_id 				= $req->get('mr_branch_id');
    $floor 					    = $req->get('floor');
    $emp_name 				    = $req->get('emp_name');
    $last_name 				    = $req->get('last_name');
    $tel 				        = $req->get('tel');
    $tel_mobile 				= $req->get('tel_mobile');
    $email 				        = $req->get('email');
    $department 				= $req->get('department');
    $position 				    = $req->get('position');


    $ss['mr_emp_email']         = $email;
    $ss['mr_emp_name']          = $emp_name;
    $ss['mr_emp_lastname']      = $last_name;
    
    if($tel != ''){
        $ss['mr_emp_tel']      = $tel;
    }
    if($tel_mobile != ''){
        $ss['mr_emp_mobile']      = $tel_mobile;
    }
    if($email != ''){
        $ss['mr_emp_email']      = $email;
    }
    if($department != ''){
        $ss['mr_department_id']      = $department;
    }
    if($position != ''){
        $ss['mr_position_id']      = $position;
    }
    if($mr_branch_id == ''){
        $ss['mr_branch_id']         = null;
    }else{
        $ss['mr_branch_id']         = $mr_branch_id;
    }
    if($floor != '' && $floor != 0 ){
        $ss['mr_floor_id']          = $floor;
    }

    if ($mr_emp_id!= ''){
        if($employeeDao ->save($ss,$mr_emp_id)){
            $msg['error'] = 'success';
            $msg['msg'] = 'บันทึกข้อมูลเรียบร้อย';
        }else{
            $msg['error'] = 'error';
            $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';
        }
    }else{
        $msg['error'] = 'error';
        $msg['msg'] = 'เกิดข้อผิดพลาด : "กรุณาลองใหม่อีกครั้ง"';  
    }
        echo json_encode($msg);
    exit;
}else{
    $msg['error'] = 'error';
    $msg['msg'] = 'เกิดข้อผิดพลาด : "รหัสพนักงงานนี้มีข้อมูลไนระบบแล้วไม่สามารถเพิ่มได้"';
    echo json_encode($msg);
    exit;
}
?>

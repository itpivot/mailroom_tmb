<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$time_start = microtime(true);



$barcode 			= $req ->get('barcode');
$sub_barcode 		= $req ->get('sub_barcode');
$tnt_barcode 		= $req ->get('tnt_barcode');
$mr_round_id 		= $req ->get('mr_round_id');
$data			  	= array();

if($sub_barcode == '') {
    echo json_encode(array('status' => 500, 'message' => 'กรุณาระบุ barcode'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $sub_barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
 if(empty($mr_round_id)){
	 echo json_encode(array(
		'status' => 500,
		'title' => 'ผิดพลาด',
		'message' => 'ไม่พบข้อมูล round'
		));
 }
 
	$sql_select = "
				SELECT 
					m.mr_work_main_id,				
					m.mr_status_id,			
					m.mr_type_work_id			
				FROM mr_work_main  m 
					Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
					where m.mr_work_barcode = ?
	";
	$params[] 	= "".trim($sub_barcode)."";
	if(trim($barcode) !=""){
		$sql_select .= " and  sw.barcode =  ?";
		$params[] 	= "".trim($barcode)."";
	}
	$sql_select .= " and m.mr_status_id in(3,7,8,9,10,11,13,14) ";
	$data = $send_workDao->select($sql_select,$params);




 if(!empty($data)){
	 if($data[0]['mr_status_id']==3){
			echo json_encode(array(
				'status' => 501,
				'title' => 'ผิดพลาด',
				'message' => $sub_barcode.' : บันทึกรับเอกสารแล้ว'
			));
			exit;
	 }else{
		 
		$in_out_update['tnt_no']=$tnt_barcode;
		$main_update['mr_status_id']=10;
		if($data[0]['mr_type_work_id'] == 3 ){
			$main_update['mr_status_id']=3;
		}
		if($data[0]['mr_work_main_id']!=''){
			$ss = $work_mainDao->save($main_update,$data[0]['mr_work_main_id']);
			$io = $work_inoutDao->updateInOutWithMainId($in_out_update, $data[0]['mr_work_main_id']);
			$save_log['mr_user_id'] 									= $auth->getUser();
			$save_log['mr_status_id'] 									= 10;
			$save_log['mr_work_main_id'] 								= $data[0]['mr_work_main_id'];
			$save_log['mr_round_id'] 									= $mr_round_id;
			$save_log['remark'] 										= "ห้อง Mailroom รับเอกสารสาขา : ".$sub_barcode;
			if($tnt_barcode!=''){
				$save_log['remark'] 									.= " เลขที่ TNT : ".$tnt_barcode;
			}
			
			$work_logDao->save($save_log);
			

			echo json_encode(array(
				'status' => 200,
				'title' => 'สำเร็จ',
				'message' => $sub_barcode.' : บันทึกข้อมูลสำเร็จ'
			));
			exit;
		}else{
			echo json_encode(array(
				'status' => 500,
				'title' => 'ผิดพลาด',
				'message' => 'ไม่พบข้อมูล'
				));
				exit;
		}
	}
	 
 }else{

	echo json_encode(array(
		'status' => 500,
		'title' => 'ผิดพลาด',
		'message' => 'ไม่พบข้อมูลเอกสารที่กำลังนำส่ง'
		));
		exit;
 }
 ?>
 
 
 

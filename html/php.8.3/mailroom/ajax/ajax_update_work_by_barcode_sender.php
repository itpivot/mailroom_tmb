<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();


$users = $auth->getUser();
$barcode = $req->get('barcode');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$status = 4;

$work_data = $work_mainDao->getIDbyBarcode( $barcode );

$save_work_main['mr_status_id'] = $status;

$save_work_log['mr_status_id']		= $status;
$save_work_log['mr_user_id'] 		= $users;
$save_work_log['mr_work_main_id'] 	= $work_data['mr_work_main_id'];

if ( $work_data['mr_work_main_id'] == "" || $work_data['mr_status_id'] == 1 || $work_data['mr_status_id'] == 5 ){
	echo json_encode(array('status' => 501, 'message' => 'ไม่สามารถบันทึกได้ โปรดตรวจสอบความถูกต้อง'));
    exit();
}elseif ( $work_data['mr_status_id'] == 2 ){
	echo json_encode(array('status' => 502, 'message' => 'งานยังไม่ถูกรับเข้าโดยห้อง Mailroom โปรตรวจสอบอีกครั้ง'));
	exit();
}else{
	$work_mainDao->save($save_work_main,$work_data['mr_work_main_id']);
	$work_logDao->save($save_work_log);

	echo json_encode(array('status' => 200, 'message' => 'บันทึกข้อมูลสำเร็จ'));
}

?>

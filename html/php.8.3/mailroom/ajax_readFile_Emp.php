<?php 
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once '../lib/PHPExcel.php';
require_once '../lib/PHPExcel/IOFactory.php';



$auth 					= new Pivot_Auth();
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();



if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}


$employee = $employeeDao->getEmpCode();
$department_data = $departmentDao->fetchAll();


$employee_data = array();

if (!function_exists('array_column')) {
    function array_column($input, $column_key, $index_key = null) {
        $arr = array_map(function($d) use ($column_key, $index_key) {
            if (!isset($d[$column_key])) {
                return null;
            }
            if ($index_key !== null) {
                return array($d[$index_key] => $d[$column_key]);
            }
            return $d[$column_key];
        }, $input);

        if ($index_key !== null) {
            $tmp = array();
            foreach ($arr as $ar) {
                $tmp[key($ar)] = current($ar);
            }
            $arr = $tmp;
        }
        return $arr;
    }
}



$employee_data = array_column($employee, 'mr_emp_code');




$file = $_FILES['file']; // File 
$commit_date = $_POST['commit_date'];

$days = explode('/', $commit_date); 
$cm_date = $days[0].'-'.$days[1].'-'.$days[2];


//echo '<pre>'.print_r( $employee, true).'</pre>';
//exit();

$date_now  = date('Y-m-d'); // Now Date
$results = array();
    $file_name = $_FILES['file']['name'];
	$tmp_name = $_FILES['file']['tmp_name'];
	$file_size = $_FILES['file']['size'];
	$err = $_FILES['file']['error'];
	$file_type = $_FILES['file']['type'];

$path = '../download/';
$file_names = explode('.', $file_name);
$surename = $file_names[count($file_names) - 1];
$new_name = 'TMBmailroom_data_emp_('.$date_now.').'.$surename;


if(!is_dir($path)) {
    @mkdir($path); // create path
    move_uploaded_file($tmp_name, $path.$new_name);
}else {
   move_uploaded_file($tmp_name, $path.$new_name);
}

$data = array();
$values = array();
if(file_exists($path.$new_name)) {
			if( $surename == 'xls' ){
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
			}elseif ( $surename == 'xlsx' ){
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			}
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($path.$new_name);

            $index = 0;
           
				$objPHPExcel->setActiveSheetIndex( $index);
				$data[$index]['highestRow'] = $objPHPExcel->getActiveSheet( $index)->getHighestRow(); // 13
				$data[$index]['highestColumn'] =$objPHPExcel->getActiveSheet( $index)->getHighestColumn(); // I
				$data[$index]['highestColumnIndex'] = PHPExcel_Cell::columnIndexFromString($data[$index]['highestColumn']); // A - I = 9
           
          
			
            $index_value = 0;
            for($row = 2; $row <= intval($data[count($data) -1]['highestRow']); $row ++) {
				$work_location[ $index_value ][ 'location' ] = $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(4, $row)->getFormattedValue();
				$check_null[ $index_value ][ 'emp_code' ] = $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(0, $row)->getFormattedValue();
				$check_null[ $index_value ][ 'name' ] = $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(1, $row)->getFormattedValue();
				$check_null[ $index_value ][ 'email' ] = $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(2, $row)->getFormattedValue();
					if ( $work_location[ $index_value ][ 'location' ] == 'สาขา' || $work_location[ $index_value ][ 'location' ] == 'เวียงจันทร์' || $work_location[ $index_value ][ 'location' ] == 'เอไอเอ แคปปิตอล เซ็นเตอร์'  || $work_location[ $index_value ][ 'location' ] == "จัสมิน อินเตอร์ฯ ทาวเวอร์"  ) {
					}else if ( $check_null[ $index_value ][ 'emp_code' ] == '' || $check_null[ $index_value ][ 'emp_code' ] == 'null' || $check_null[ $index_value ][ 'name' ] == '' || $check_null[ $index_value ][ 'name' ] == 'null'  || $check_null[ $index_value ][ 'email' ] == '' || $check_null[ $index_value ][ 'email' ] == 'null' ) {
					}else{
						$values[ $index_value ]['id'] =  $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(0, $row)->getFormattedValue();
						list( $name_1, $name_2, $l_name_1, $l_name_2, $l_name_3 ) = explode(" ",$objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(1, $row)->getFormattedValue());
						
						if( $l_name_1 == "" && $l_name_2 == "" && $l_name_3 == "" ){
							$values[ $index_value ]['name'] =  $name_1;
							$values[ $index_value ]['last_name'] =  $name_2;
							
						}else if ( $l_name_1 == "นาย" || $l_name_1 == "นส" || $l_name_1 == "นางสาว" || $l_name_1 == "นาง" || $l_name_1 == "นส." ) {
							$values[ $index_value ]['name'] =  $name_2;
							$values[ $index_value ]['last_name'] =  trim($l_name_1." ".$l_name_2." ".$l_name_3);
							
						}else {
							$values[ $index_value ]['name'] =  $name_2;
							$values[ $index_value ]['last_name'] =  trim($l_name_1." ".$l_name_2." ".$l_name_3);
							
						}
						$values[$index_value]['email'] = $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(2, $row)->getFormattedValue();
						$values[$index_value]['tel'] =  $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(3, $row)->getFormattedValue();
						$values[$index_value]['department_code'] =  $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(5, $row)->getFormattedValue();
						$values[$index_value]['department_name'] =  $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(6, $row)->getFormattedValue();
						$values[$index_value]['cost_id'] =  $objPHPExcel->getActiveSheet($index)->getCellByColumnAndRow(7, $row)->getFormattedValue();
						
					}
					
                   
                    $index_value++;
			}
			
	
				
				//echo print_r($values);
				//exit();
				
			$val = array();
			$sql = " ";
			$num = 0;
			$date_import = date('Y-m-d');
			foreach( $values as $key => $vale){
				
				foreach($vale as $key_data =>$val_data){
					$values[$key_data] 	=  	preg_quote($val_data, '/');
				}
				
				if( !in_array($vale['id'], $employee_data) ){
					
					$depart_id = array_search($vale['department_code'], array_column($department_data, 'mr_department_code'));
					$mr_department_id = $department_data[$depart_id]['mr_department_id'];
					$mr_floor_id = $department_data[$depart_id]['mr_floor_id'];
					$department_code = $vale['department_code'];
					$mr_cost_id = $vale['cost_id'];
			
					$sql = 'INSERT INTO `mr_emp`( `mr_emp_name`, `mr_emp_lastname`, `mr_emp_code`, `mr_emp_tel`, `mr_emp_email`, `mr_department_id`, `mr_floor_id` , `mr_cost_id` , `mr_date_import` ) VALUES ';
					$sql .= "( '".$vale['name']."' , '".$vale['last_name'] ."','". $vale['id'] ."','". $vale['tel'] ."','". $vale['email'] ."',". $mr_department_id .",". $mr_floor_id .",". $mr_cost_id .",'". $date_import ."');";
				
					
					$num++;
					$employeeDao->ImportEmp($sql);
					//echo $sql;
					//
					//echo '<pre>'.print_r( $val, true).'</pre>';
				}
			
			}
			
	$result = $num;
		
}
 //echo '<pre>'.print_r( $values, true).'</pre>';
 //exit();
 echo $result;
 
?>
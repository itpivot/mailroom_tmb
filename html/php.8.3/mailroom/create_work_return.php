<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Round.php';
require_once 'Dao/Type_post.php';
require_once 'Dao/Cost.php';
require_once 'nocsrf.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//
$req = new Pivot_Request();
$userDao = new Dao_User();
$costDao = new Dao_Cost();

$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$userRoleDao		= new Dao_UserRole();
$branchDao			= new Dao_Branch();
$roundDao			= new Dao_Round();
$send_workDao			= new Dao_Send_work();
$type_postDao			= new Dao_Type_post();


$branch_data 		= $branchDao->getBranch();
$user_id			= $auth->getUser();
$user_data 			= $userDao->getempByuserid($user_id);
$user_data_show 	= $userDao->getEmpDataByuseridProfile($user_id);
$round 				= $roundDao->getRoundAll();
$type_post 			= $type_postDao->fetchAll();
$department		 	= $departmentDao->fetchAll();
$cost		 		= $costDao->fetchAll();
////$department = $departmentDao->fetchAll();
//$emp_data = $employeeDao->getEmpDataSelect();
//echo "<pre>".print_r($department,true)."</pre>";//mr_emp_code


// $tp['mr_type_post_name'] = 'ปณ.ลงทะเบียน-ตอบรับ';
// $type_postDao->save($tp,8);
$emp_send_data = '';
if(!empty($user_data_show)){
	$emp_send_data .= $user_data_show['mr_emp_code'].'  '.$user_data_show['mr_emp_name'].'  '.$user_data_show['mr_emp_lastname'];
	$emp_send_data .= '  '.$user_data_show['mr_department_code'].'  '.$user_data_show['mr_department_name'].'  '.$user_data_show['name'];
	$emp_send_data .= '  '.$user_data_show['mr_emp_email'].'  '.$user_data_show['mr_emp_tel'];

	if($user_data_show['mr_user_role_id'] == 2){
		//echo "<pre>".print_r($user_data_show,true)."</pre>";//mr_emp_code
		if($user_data_show['mr_branch_id'] != 1 and $user_data_show['mr_emp_id']!=''){
			$update_branch['mr_branch_id'] = 1;
			$employeeDao->save($update_branch,$user_data_show['mr_emp_id']);
		}
	}
}
// echo "<pre>".print_r($emp_send_data,true)."</pre>";//mr_emp_code
// exit;

$template = Pivot_Template::factory('mailroom/create_work_return.tpl');
$template->display(array(
	//'debug' 				=> print_r($contactdata,true),
	// 'contactdata' 			=> $contactdata,
	//'debug' 				=> print_r($path_pdf,true),
	'userRoles' 			=> (isset($userRoles) ? $userRoles : null),
	'success' 				=> (isset($success) ? $success : null),
	// 'barcode' 				=> $barcode,
	'type_post' 			=> $type_post,
	// 'floor_data' 			=> $floor_data,
	'branch_data' 			=> $branch_data,
	'emp_data' 				=> (isset($emp_data) ? $emp_data : null),
	'users' 				=> (isset($users) ? $users : null),
	// 'tomorrow' 				=> $tomorrow,
	'user_data' 			=> $user_data,
	'round' 				=> $round,
	'cost' 					=> $cost,
	'emp_send_data' 		=> $emp_send_data,
	'department' 			=> $department,
	'user_data_show' 		=> $user_data_show,
	'select' 				=> '0',
	'today' 				=> date('Y-m-d'),
	'role_id' 				=> $auth->getRole(),
	'csrf_token' 			=>  NoCSRF::generate( 'csrf_token'),
	'roles' 				=> Dao_UserRole::getAllRoles(),
	'serverPath' 			=> $_CONFIG->site->serverPath
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_post.php';
require_once 'PHPExcel.php';
require_once 'Dao/Send_work.php';
error_reporting(E_ALL & ~E_NOTICE);
header('Content-Type: text/html; charset=utf-8');

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
$work_postDao 	= new Dao_Work_post();

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$alert 			= '';

$data  			= array();
$round 	= $req->get('round_printreper');
$date 	= $req->get('date_report');
$data_search		    		=  json_decode($req->get('params'),true);     


	

//echo '<pre>'.print_r($date_report,true).'</pre>';

//exit;
if(preg_match('/<\/?[^>]+(>|$)/', $round_print)) {
	$alert = "
	$.confirm({
		title: 'Alert!',
		content: 'เกิดข้อผิดพลาด!',
		buttons: {
			OK: function () {
				location.href = 'create_work_post_in.php';
				}
			}
		});
	";
}else if(preg_match('/<\/?[^>]+(>|$)/', $date_report)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'create_work_post_in.php';
			}
		}
	});
		
	";
}else{
	$sql="SELECT
				w_m.mr_work_main_id,
				DATE_FORMAT(w_m.sys_timestamp, '%Y-%m-%d') as d_send,
				w_m.mr_work_barcode,
				w_m.mr_work_remark,
				w_m.quty,
				w_m.mr_topic,
				w_io.mr_floor_id,
				f.name as floor_name,
				emp_re.mr_emp_id as mr_resive_emp_id,
				emp_re.mr_emp_code ,
				emp_re.mr_emp_name,
				emp_re.mr_emp_lastname,
				s.mr_status_id,
				s.mr_status_name,
				u_re.mr_user_username as user_re,
				u_send.mr_user_username as user_send,
				
				e_re.mr_emp_code as r_code,
				e_re.mr_emp_name  as r_name,
				e_re.mr_emp_lastname as r_lname,

				e_send.mr_emp_code as s_code,
				e_send.mr_emp_name as s_name,
				e_send.mr_emp_lastname as s_lname


			FROM
				mr_work_main w_m
				LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
				LEFT join  mr_work_inout w_io on(w_io.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_floor f on(f.mr_floor_id = w_io.mr_floor_id)
				LEFT join  mr_emp emp_re on(emp_re.mr_emp_id = w_io.mr_emp_id)
				LEFT join  mr_user u_re on(u_re.mr_user_id = w_io.mr_user_id)
				LEFT join  mr_user u_send on(u_send.mr_user_id = w_m.mr_user_id)
				LEFT join  mr_emp e_re on(e_re.mr_emp_id = u_re.mr_emp_id)
				LEFT join  mr_emp e_send on(e_send.mr_emp_id = u_send.mr_emp_id)
			WHERE
			w_m.mr_user_id = 4450 and w_m.sys_timestamp like '2019-11%'
			   ";
		


		
		
	
		$data_qury 			= $send_workDao->select($sql);
		//echo "<pre>".print_r($data_qury,true)."</pre>";
		//exit;
	//$data_qury	= $work_postDao->getdatatoday_post_in($date,$round);
		// echo count($data);
		$new_data = array();
		foreach($data_qury as $key=>$val){
			$new_data[$key]['0'] = ($key+1);
			$new_data[$key]['1'] = $val['d_send'];
			$new_data[$key]['2'] = '"'.$val['mr_work_barcode'];
			$new_data[$key]['3'] = $val['mr_status_name'];
			$new_data[$key]['4'] = $val['floor_name'];
			$new_data[$key]['5'] = $val['s_code'].' : '.$val['s_name'].'  '.$val['s_lname'];
			$new_data[$key]['6'] = $val['mr_emp_code'].' : '.$val['mr_emp_name'].'  '.$val['mr_emp_lastname'];
			$new_data[$key]['7'] = $val['r_code'].' : '.$val['r_name'].'  '.$val['r_lname'];
			$new_data[$key]['8'] = $val['mr_topic'];
			$new_data[$key]['9'] = $val['mr_work_remark'];
		}
}









$arr_report1	= array();
$sheet1 		= 'PostInDetail';
$headers1  		= array(
	 'ลำดับ',                                                  
	 'วันที่ส่ง',                                                  
	 'เลขที่เอกสาร',                                       
	 'สถานะ',                                       
	 'ชั้นผู้รับ',                                            
	 'ผู้ส่ง',           
	 'ผู้รับ',                                                
	 'ผู้ลงชื่อรับ',                                                
	 'ชื่อเอกสาร',         
	 'หมายเหตุ',         
);

$file_name = 'TMB-Report-Thai-PostIn'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($new_data as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
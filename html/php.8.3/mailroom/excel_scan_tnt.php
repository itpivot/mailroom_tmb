<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_post.php';
require_once 'Dao/Round_resive_thaipost_in.php';
require_once 'Dao/Work_log.php';
require_once 'PHPExcel.php';
header('Content-Type: text/html; charset=utf-8');


include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();     
$work_postDao 		= new Dao_Work_post();
$thaipost_in 		= new Dao_Round_resive_thaipost_in();
$work_log           = new Dao_Work_log();
$user_role 			= $auth->getRole();

 
$data_search		=  array();       
$start_date 		= $req->get('date_1');
$end_date 			= $req->get('date_2');
// $mr_type_work 		= $req->get('mr_type_work');
$username 			= $auth->getUserName();



$data_search['start_date'] 	= $start_date;
$data_search['end_date'] 	= $end_date;

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}


$arr_report1	= array();
$data = $work_log->expoetdatareport_tnt($data_search);
$file_name = $username.'-Report-work-log'.DATE('y-m-d').'.xlsx';
$sheet1    = 'Detail_work_log';
    $headers1 = array(
        'ลำดับที่',
        'บาร์โค้ด',
        'หมายเลขTNT',
        'วันที่ เวลา (สแกน)',
        'รหัสพนักงานผู้ส่ง',
        'ชื่อผู้ส่ง',
        'รหัสแผนกผู้ส่ง',
        'ชื่อแผนกผู้ส่ง',
        'รหัสสาขาผู้ส่ง',
        'ชื่อสาขาผู้ส่ง',
        'รหัสพนักงานผู้รับ',
        'ชื่อผู้รับ',
        'รหัสแผนกผู้รับ',
        'ชื่อแผนกผู้รับ',
        'รหัสสาขาผู้รับ',
        'ชื่อสาขาผู้รับ',
        'สำเร็จเวลา',
        'แมสผู้ส่ง',
        'หมายเหตุ',
    );
    foreach($data as $keys => $val) {
        $arr_report1[$keys][] = ($keys+1);
		$arr_report1[$keys][] = "'" .$val['mr_work_barcode']; 
		$arr_report1[$keys][] = "'" .$val['tnt_no']; 
		$arr_report1[$keys][] = $val['sys_timestamp']; 
		$arr_report1[$keys][] = $val['send_emp_code']; 
		$arr_report1[$keys][] = $val['send_emp_name']; 
		$arr_report1[$keys][] = $val['send_department_code']; 
		$arr_report1[$keys][] = $val['send_department_name']; 
		$arr_report1[$keys][] = $val['send_branch_code']; 
		$arr_report1[$keys][] = $val['send_branch_name']; 
		$arr_report1[$keys][] = $val['re_emp_code']; 
		$arr_report1[$keys][] = $val['re_emp_name']; 
		$arr_report1[$keys][] = $val['re_department_code']; 
		$arr_report1[$keys][] = $val['re_department_name']; 
		$arr_report1[$keys][] = $val['re_branch_code']; 
		$arr_report1[$keys][] = $val['re_branch_name']; 
		$arr_report1[$keys][] = $val['confirm_timestamp']; 
		$arr_report1[$keys][] = $val['mr_emp_name']; 
		$arr_report1[$keys][] = $val['remark']; 
    }

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($arr_report1 as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
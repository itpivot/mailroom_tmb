<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Zone.php';
require_once 'Dao/Work_inout.php';
require_once 'PHPExcel.php';
include_once('xlsxwriter.class.php');
error_reporting(E_ALL & ~E_NOTICE);

header('Content-Type: text/html; charset=utf-8');

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$userRole_Dao   	= new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();
$branchDao 			= new Dao_Branch();
$floorDao 			= new Dao_Floor();
$zoneDao 			= new Dao_zone();
$work_inoutDao 			= new Dao_Work_inout();

$data = array();






$mess['mr_emp_code']   = 'TMK006';
$mess['mr_emp_name']   = 'นายสุขสวัสดิ์ ';
$mess['mr_emp_lastname']  = ' เชื้อสูง';
$mess['mr_hub_id']    = '';
//$employee_Dao_id    = $employee_Dao->save($mess);



//$addUser['mr_emp_id']     = $employee_Dao_id;
//$addUser['change_password_date']  = '0000-00-00 00:00:00';
//$addUser['mr_user_username']   = 'TMK006';
$addUser['mr_user_password']   = 'TGwzSVQ0cElKajY1eHk4RjZ4WHJxdz09';
$addUser['active']      = 1;
$addUser['is_first_login']    = 1;
$addUser['date_create']    = date('Y-m-d H:i:s');
$addUser['sys_timestamp']    = date('Y-m-d H:i:s');
//$addUser['mr_user_role_id']   = 4;
//$result_save       = $users_Dao->save($addUser,2933);



$sql 	= "
SELECT
    b.mr_branch_id,
    b.mr_branch_name,
    bt.branch_type_name,
    h.mr_hub_name,
   b.active,
   b.mr_branch_code,
   bc.mr_branch_category_name
FROM mr_branch b
    LEFT join mr_branch_type bt on(bt.mr_branch_type_id = b.mr_branch_type)
    LEFT join mr_hub h on(h.mr_hub_id = b.mr_hub_id)
    LEFT join mr_branch_category bc on(bc.mr_branch_category_id = b.mr_branch_category_id)";
$params	= array();
$b 		= $branchDao->select($sql,$params);
$sheet1 		= 'Detail';
$headers1  		= array(
	0 => 'No.',                           
	1 => 'mr_branch_code',                            
	2 => 'mr_branch_name',                            
	3 => 'branch_type_name',                            
	4 => 'mr_hub_name',                            
	5 => 'mr_branch_category_name',
	6 => 'status'
);



$add = array();
foreach($b as $k => $v){
	$ad= array(); 
	$ad[] = $k+1;
	$ad[] = "'".$v['mr_branch_code'];
	$ad[] = $v['mr_branch_name'];
	$ad[] = $v['branch_type_name'];
	$ad[] = $v['mr_hub_name'];
	$ad[] = $v['mr_branch_category_name'];
	$ad[] =($v['active'] == 0)?"ปิดใช้งานแล้ว":"";;
	
	//$branchDao->save($ad,$v['mr_branch_id']); 
	$add[] = $ad;
}

$file_name = 'TMB_Branch'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');




$writer 		= new  XLSXWriter();
$styleHead 		= array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow 		= array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');

$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($add as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
}
//echo "<pre>".print_r($b,true)."</pre>";
//echo "<pre>".print_r($ss,true)."</pre>";
$writer->writeToStdOut();
exit;

?>
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Work_inout.php';
require_once 'ajax/check_sla.php';

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$userDao 			= new Dao_User();   
$work_inoutDao 		= new Dao_Work_inout();
$class_check_sla 	= new Class_check_sla();


$array_success			= array('5','6','12','15');



$sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round WHERE active = 1 ORDER BY mr_delivery_round_id ASC";
$max_round_sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round 
WHERE mr_delivery_round_id in(
	SELECT max(mr_delivery_round_id)-1 
	FROM mr_delivery_round 
	group by mr_branch_type_send, mr_branch_type_resive)
and active = 1
";
$min_round_sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round 
WHERE mr_delivery_round_id in(
	SELECT min(mr_delivery_round_id)
	FROM mr_delivery_round 
	group by mr_branch_type_send, mr_branch_type_resive)
and active = 1
";

$sql_holiday = "SELECT * FROM mr_holiday WHERE  active = 1";

$delivery_round_all = $userDao ->select($sql);
$max_round_all 		= $userDao ->select($max_round_sql);
$min_round_all 		= $userDao ->select($min_round_sql);
$holiday_all 		= $userDao ->select($sql_holiday);


$delivery_round_data = array();
foreach($delivery_round_all as $v){
	$delivery_round_data[$v['mr_branch_type_send']][$v['mr_branch_type_resive']][]= $v;
}

$max_round_data = array();
foreach($max_round_all as $v2){
	$max_round_data[$v2['mr_branch_type_send']][$v2['mr_branch_type_resive']][]= $v2;
}
$min_round_data = array();
foreach($min_round_all as $v3){
	$min_round_data[$v3['mr_branch_type_send']][$v3['mr_branch_type_resive']][]= $v3;
}

$holiday_data = array();
foreach($holiday_all as $v4){
	$holiday_data[$v4['year']][$v4['month']][]= $v4['day'];
}



function TimeDiff($strTime1,$strTime2){
	return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}
function DateTimeDiff($strDateTime1,$strDateTime2){
return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}


function check_date_start($sla_date_end){
	$day = date ("D", strtotime($sla_date_end)); 
	if($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($sla_date_end))); 
	}elseif($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($sla_date_end))); 
	}else{
		$date_end = $sla_date_end;
	}
	return $date_end;
}

function wav_sla($sla_date_end){
	$day = date ("D", strtotime($sla_date_end)); 
	if($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($sla_date_end))); 
	}elseif($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($sla_date_end))); 
	}else{
		$date_end = $sla_date_end;
	}
	return $date_end;
}






$branch_type_re 	= $req->get('branch_type_re');
$branch_type 	= $req->get('branch_type');
$date_1 		= $req->get('date_1');
$date_2 		= $req->get('date_2');
$type 			= $req->get('type');
$status 			= $req->get('status');



if( $date_1 != ''){
	$date_1 	= date ("Y-m-d 17:00:01", strtotime("-1 day", strtotime($date_1)));
	$date_2 	= date ("Y-m-d 17:00:00", strtotime($date_2));
}


$resout = array();
$resout['date_1'] = $date_1;
$resout['date_2'] = $date_2;



$data_search['start_date']		= $date_1;
$data_search['end_date']		= $date_2; 
$data_search['type']			= $type; 
if($type == 'ho'){
	$type = 1; 
	$type_name = " สำนักงานใหญ่";
}else{
	
	if($branch_type == 3){
		$type_name = "ต่างจังหวัด";
		$type = 3;
	}else{
		$type_name = "กรุงเทพมและปริมณฑล";
		$type = 2;
	}
}

if($branch_type_re == 3){
	$type_name_re = "ต่างจังหวัด";
}elseif($branch_type_re == 2){
	$type_name_re = "กรุงเทพมและปริมณฑล";
}else{
	$type_name_re = " สำนักงานใหญ่";
}



$data  =   $work_inoutDao->mailroom_getreport_level1( $data_search );
//echo "<pre>".print_r($data ,true)."</pre>";
//exit;
$work_data = array();
$work_status = array();
foreach($data as $key => $val){


	$sys_time 				= $val['main_sys_time'];
	$time_send     			= date("H:i:s",strtotime($sys_time));
	//$sys_time 				= check_date_start($sys_time);


	$mr_status_id = $val['mr_status_id'];
	if($mr_status_id == 5 or $mr_status_id == 6 or $mr_status_id == 12 or $mr_status_id == 15){
		$mr_status = 5;
	}else{
		$mr_status = 6;
	}



	if($type != 1){
			//echo $val['mr_branch_type_id'].'<br>';
			
			if($val['mr_branch_type_id_re'] == 2 or $val['mr_branch_type_id_re'] == 3){ //3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
				$type_re = $val['mr_branch_type_id_re'];
				$val['b_re'] = $val['mr_branch_code_re'].' : '.$val['mr_branch_name_re'];
			}else{
				$type_re = 1;
				$val['b_re'] = $val['depart_code_receive'].' : '.$val['depart_name_receive'];
			}

			if($val['mr_branch_type_id'] == 3){ // 3 สาขาต่างจังหวัดสังงาน ถ้าไม่เป็นปริมนฑน
				$type_send = $val['mr_branch_type_id'];
				
			}else{
				$type_send = 2;
			}
			if($val['mr_branch_type_id_send'] == 2 or $val['mr_branch_type_id_send'] == 3){ //3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
				$val['b_send'] = $val['mr_branch_code_sen'].' : '.$val['mr_branch_name_sen'];
			}else{
				$val['b_send'] = $val['mr_department_code'].' : '.$val['mr_department_name'];
			}


			$val['no'] =  count($work_data[$type][$type_re])+1;
			$val['name_send'] = $val['mr_emp_code'].'-'.$val['mr_emp_name'].'  '.$val['mr_emp_lastname'] ;
			$val['name_receive'] = $val['mr_emp_code_re'].'-'.$val['name_re'].'  '.$val['lastname_re'] ;
			$val['name_get'] = $val['emp_code_get'].'-'.$val['emp_name_get'].'  '.$val['emp_lastname_get'] ;

			

					$sla_date_end = $class_check_sla->check_sla($type_send,$type_re,$sys_time,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);
					$val['sla_date_end'] = $sla_date_end;

			if ($mr_status == 6) {
				//if($mr_status_id != 5 and $mr_status_id != 12){				
						$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$sla_date_end);
						$gg['s']=$sys_time ;
						$gg['e']=$sla_date_end ;
						$gg['d']=$delivery_round ;
						$gg['dif']=$dif_date ;
						$gg['data']=$val ;
						// echo json_encode($gg);
						 //exit;
						if($dif_date<0){
							//$gg['CH']=1 ;
							$mr_status = 20;
							//$tt[] = $gg;
							//echo json_encode($tt);
							//exit;
						}else{
							//$gg['CH']=2 ;
							//echo json_encode($tt);
							//exit;
						}
		
					
				}



			$work_data[$type_send][$type_re][$key] = $val;
			$val['no'] = count($work_status[$type][$type_re][$mr_status])+1;
			$work_status[$type_send][$type_re][$mr_status][$key] = $val;
	}else{

		if($val['mr_branch_type_id_re'] == 2 or $val['mr_branch_type_id_re'] == 3){ //3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
			$type_re = $val['mr_branch_type_id_re'];
			$val['b_re'] = $val['mr_branch_code_re'].' : '.$val['mr_branch_name_re'];
		}else{
			$type_re = 1;
			$val['b_re'] = $val['depart_code_receive'].' : '.$val['depart_name_receive'];
		}		
		if($val['mr_branch_type_id_send'] == 2 or $val['mr_branch_type_id_send'] == 3){ //3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
				$val['b_send'] = $val['mr_branch_code_sen'].' : '.$val['mr_branch_name_sen'];
			}else{
				$val['b_send'] = $val['mr_department_code'].' : '.$val['mr_department_name'];
			}
		$val['no'] = count($work_data[$type][$type_re])+1;
		$val['name_send'] = $val['mr_emp_code'].'-'.$val['mr_emp_name'].'  '.$val['mr_emp_lastname'] ;
		$val['name_receive'] = $val['mr_emp_code_re'].'-'.$val['name_re'].'  '.$val['lastname_re'] ;
		$val['name_get'] = $val['emp_code_get'].'-'.$val['emp_name_get'].'  '.$val['emp_lastname_get'] ;
				$sla_date_end = $class_check_sla->check_sla($type,$type_re,$sys_time,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);
				// echo "<pre>".print_r($sla_date_end,true)."</pre>";
				// exit;
					$val['sla_date_end'] = $sla_date_end;

		if ($mr_status == 6) {
			//if($mr_status_id != 5 and $mr_status_id != 12){				
					$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$sla_date_end);
					$gg['s']=$sys_time ;
					$gg['e']=$sla_date_end ;
					$gg['d']=$delivery_round ;
					$gg['dif']=$dif_date ;
					$gg['data']=$val ;
					// echo json_encode($gg);
					 //exit;
					if($dif_date<0){
						$gg['CH']=1 ;
						$mr_status = 20;
						//$tt[] = $gg;
						//echo json_encode($tt);
						//exit;
					}else{
						$gg['CH']=2 ;
						//echo json_encode($tt);
						//exit;
					}
	
				
			}
		
		$work_data[$type][$type_re][$key] = $val;
		$val['no'] = count($work_status[$type][$type_re][$mr_status])+1;
		$work_status[$type][$type_re][$mr_status][$key] = $val;
	}
}


$str_html = '
<div class="row justify-content-center">
<div class="col-lg-4 col-12">
	<h2 class="text-center">รายงานการรับส่งเอกสารจาก ('.$type_name.' ถึง '.$type_name_re.' )</h2>
</div>
</div>
<div class="row">
		<div class="col-lg-4 col-12">
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่ออกรายงาน :</label>
				<input type="text" class="input-sm form-control" placeholder="To date" autocomplete="off" readonly value="'.date('y-m-d').'"/>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
			<div class="col-lg-4 col-12">
				<div class=" input-group">
					<label class="input-group-addon" style="border:0px;">วันที่ของข้อมูล :</label>
					<input type="text" class="input-sm form-control"  placeholder="To date" value="'.$date_1.'" autocomplete="off" readonly/>
					<label class="input-group-addon" style="border:0px;">  ถึง </label>
					<input type="text" class="input-sm form-control"  placeholder="To date" value="'.$date_2.'" autocomplete="off" readonly/>
				</div>
			</div>
	</div>
	<br>';

if($status == 1){
	$data = (isset($work_data[$type][$branch_type_re]))?$work_data[$type][$branch_type_re]:array();
}else{
	$data = (isset($work_status[$type][$branch_type_re][$status]))?$work_status[$type][$branch_type_re][$status]:array();
}










// echo $str_html;

// echo "<pre>".print_r($sla_date_end,true)."</pre>";
// exit;
$resout['html_head'] 	= $str_html;
$resout['data'] 		= (!empty($data))?array_values($data):$data;
$resout['data2'] 		= $work_data;





$reports = array();
$reports1 = (!empty($data))?array_values($data):$data;
foreach($reports1 as $key => $val){
	$data_ass = array();
	$data_ass = array(
				($key+1),
				' '.$val['mr_work_barcode'],
				$val['name_send'],
				$val['b_send'],
				$val['name_receive'],
				$val['b_re'],
				$val['mr_topic'],
				$val['mr_work_remark'],
				$val['main_sys_time'],
				$val['time_mail_re'],
				$val['sla_date_end'],
				$val['time_log'],
				$val['name_get'],
				$val['mr_status_name'],
				$val['mr_type_work_name']
	);
	array_push($reports,$data_ass);
}






$sheet = 'Detail';
$headers1 = array(
    '',
    '',
    'รายงานการรับส่งเอกสารจาก ('.$type_name.' ถึง '.$type_name_re.' )',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);
$headers2 = array(
	'',
    'วันที่ออกรายงาน',
    date('Y-m-d H:i:s'),
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);

$headers3 = array(
	'',
    'วันที่ของข้อมูล',
    $date_1.'  ถึง  '.$date_2,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);
$headers4 = array(
    'ลำดับ.',
    'บาร์โคด',
    'ผู้ส่ง	',
    'สาขา',
    'ผู้รับ',
    'สาขา',
    'ชื่อเอกสาร',
    'หมายเหตุ',
    'วันที่สร้างรายการ',
    'วันที่ห้องสารบัณกลางรับ',
    'วันครบกำหนด SLA',
    'วันที่สำเร็จ',
    'ผู้ที่ลงชื่อรับงาน',
    'สถานะงาน',
    'ประเภทการส่ง'
);

$file_name = 'TMB_Report.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet,$headers1,$styleRow);
$writer->writeSheetRow($sheet,$headers2,$styleRow);
$writer->writeSheetRow($sheet,$headers3,$styleRow);
$writer->writeSheetRow($sheet,$headers4,$styleHead);
foreach ($reports as $v) {
    $writer->writeSheetRow($sheet,$v,$styleRow);
 }
//exit;

$writer->writeToStdOut();

exit();
//echo print_r($work_type2,true);
//echo 'end <br>';

echo json_encode($resout);
//echo print_r($work_type2,true);
//echo 'end <br>';
?>

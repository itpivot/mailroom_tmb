<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
// require_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';
// require_once 'PHPExcel.php';
require_once 'Dao/Access_status_log.php';
set_time_limit(0);
ini_set('memory_limit', '-1');

$req = new Pivot_Request();
$access_status_log_Dao = new Dao_Access_status_log();

function PrettyDate($date)
{
    if ($date) {
        $old = explode('/', $date);
        return $old[2] . '-' . $old[1] . '-' . $old[0];
    } else {
        return null;
    }

}


$indexs = 2;
$columnNames = array(
    0 => '#',
    1 => 'USER',
    2 => 'ชื่อ-สกุล',
    3 => 'Terminal',
    4 => 'กิจกรรม',
    5 => 'ผลการเข้าสู่ระบบ',
    6 => 'วันเวลา',
    7 => 'หมายเหตุ',
    8 => 'user-role',
    9 => 'รหัสหน่วยงาน(4)',
    10 => 'หน่วยงาน',
    11 => 'สาขา',
    12 => 'Cost-center',
    13 => 'chief4',
    14 => 'coompany',
    15 => 'descr_150',
    16 => 'descr_1502',
);


  ob_start();
  $filename = 'export';
  $delimiter = ';';
  $enclosure = '"';
  $fname = '../download/Access_Logs'.date('ymdHis').'.csv';
  $df = fopen($fname, 'w');
  fputs($df, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
  fputcsv($df, $columnNames);
  //fputcsv($df, $columnNames);




  $param  = json_decode($req->get('data'), true);
  $start = PrettyDate($param['start']);
  $end = PrettyDate($param['end']);
  $start2 = $start;
  $end2 = date('Y-m-d',strtotime($start . "+15 days"));
  $i = 0;
  $ss_serch=array();
  if($end>$end2){
  while ($end>$end2) {
    $ss_serch[$i]['activity']   = $param['activity'];
    $ss_serch[$i]['status']     = $param['status'];
      if($i==0){
        
          //echo $start2."<<<<<<<<<<<<>>>>>>>>>>>>>>>>>".$end2."<br>";
          
          $ss_serch[$i]['start_date']	 	= $start2;
          $ss_serch[$i]['end_date'] 		= $end2;
  
          $start2 = date('Y-m-d',strtotime($start2 . "+16 days"));
          $end2 = date('Y-m-d',strtotime($end2 . "+15 days"));
  
          
          $i++;
      }else{
          $start2 = date('Y-m-d',strtotime($start2 . "+15 days"));
          $end2 = date('Y-m-d',strtotime($end2 . "+15 days"));
      }
      
      if($end2>$end){
          $end2 = $end;
      }
          $ss_serch[$i]['start_date']	 	= $start2;
          $ss_serch[$i]['end_date'] 		= $end2;
  
    //   echo $start2."<<<<<<<<<<<<>>>>>>>>>>>>>>>>>".$end2."<br>";
    //   echo "<br>";
    //   echo "---------------<br>";
    //   echo '<pre>'.print_r($ss_serch,true);
      $i++;
  }}else{
    $ss_serch[$i]['activity']   = $param['activity'];
    $ss_serch[$i]['status']     = $param['status'];
    $ss_serch[$i]['start_date']	 	= $start;
    $ss_serch[$i]['end_date'] 		= $end;
  }

//  echo '<pre>'.print_r($ss_serch,true);
//  exit;

foreach($ss_serch as $end => $data){

    $acc = $access_status_log_Dao->fetchAccess_logs($data);
    // echo '<pre>'.print_r(count($acc),true);
    // exit;
    $total = count($acc);
    usort($acc, function ($x, $y) {
        return $y['sys_timestamp'] > $x['sys_timestamp'];
    });

    
    foreach($acc as $k => $v) {
        $data= array();
        $data[] = $indexs;
        $data[] = $v['mr_user_username'];
        $data[] = $v['mr_emp_code'].' '.$v['mr_emp_name'].' '.$v['mr_emp_lastname'];
        $data[] = $v['terminal'];
        $data[] = $v['activity'];
        $data[] = $v['status'];
        $data[] = $v['sys_timestamp'];
        $data[] = $v['description'];
        $data[] = $v['mr_user_role_name'];
        $data[] = "'".$v['mr_department_code_4'];
        $data[] = $v['mr_department_code'].' '.$v['mr_department_name'];
        $data[] = $v['mr_branch_code'].' '.$v['mr_branch_name'];
        $data[] = "'".$v['mr_cost_code'];
        $data[] = "'".$v['chief4'];
        $data[] = "'".$v['coompany'];
        $data[] = "'".$v['descr_150'];
        $data[] = "'".$v['descr_1502'];
        // fputcsv($df, $data);
        fputcsv($df, $data);
        $indexs++;
    }
}



// exit;


  fclose($df);
  ob_get_clean();

  header("Content-disposition: attachment; filename=".$fname);
  header("location:".$fname);
exit;



?>
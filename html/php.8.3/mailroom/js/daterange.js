$.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex){
        var dateStart = Date.parse($("#start_date").val());
        var dateEnd = Date.parse($("#end_date").val());

        var evalDate= aData[1].substring(0,10);
        var newEval = Date.parse(evalDate);

        if (isNaN(dateStart) && isNaN(dateEnd)) {
            return true;
        }
        else{
          if(newEval >= dateStart && newEval <= dateEnd){
            return true;
          }else{
            return false;
          }
        }

    });

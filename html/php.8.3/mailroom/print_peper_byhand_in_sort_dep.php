<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Work_byhand.php';

error_reporting(E_ALL & ~E_WARNING );
set_time_limit(0);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
$work_byhandDao = new Dao_Work_byhand();

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$alert 			= '';


	
$round_printreper 	= $req->get('round_printreper');
$date_report 		= $req->get('date_report');

//echo '<pre>'.print_r($date_report,true).'</pre>';

//exit;
if(preg_match('/<\/?[^>]+(>|$)/', $round_printreper)) {
	$alert = "
	$.confirm({
		title: 'Alert!',
		content: 'เกิดข้อผิดพลาด!',
		buttons: {
			OK: function () {
				location.href = 'create_work_byHand_out.php';
				}
			}
		});
	";
}else if(preg_match('/<\/?[^>]+(>|$)/', $date_report)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{

//echo "<pre>".print_r($req,true)."</pre>";
// echo $date_report;
// echo ">>".$round_print;
// 	exit;


$data  				= array();
$form_data 			= array();

if($date_report !=''){
	$form_data['date_report'] = $date_report ;
}
if($round_printreper !=''){
	$form_data['round_printreper'] = $round_printreper ;
	
}
$form_data['mr_type_work_id'] = 7 ;
$data	= $work_byhandDao->getdataByhandINByval($form_data);
	$datasorce 		= array();
	$arr_sum_price 	= array();
	$arr_total_price 	= array();
	foreach($data as $j => $data_j){
		$mr_resive_emp_id 										= $data_j['mr_send_emp_id'];
		// $mr_floor_id 											= $data_j['mr_send_dep_id'].'_'.$data_j['mr_floor_id'];
		$mr_floor_id 											= $data_j['mr_send_dep_id'];

		$datasorce[$mr_floor_id][$mr_resive_emp_id][$j] 								=  $data_j;

		if(isset($arr_total_price[$mr_floor_id]['s_quty'])){
			$arr_total_price[$mr_floor_id]['s_quty'] 						+= $data_j['quty'];	
		}else{
			$arr_total_price[$mr_floor_id]['s_quty'] 						= $data_j['quty'];	
		}
			

		if(isset($arr_sum_price[$mr_floor_id][$mr_resive_emp_id]['s_quty'])){
			$arr_sum_price[$mr_floor_id][$mr_resive_emp_id]['s_quty'] 						+= $data_j['quty'];	
		}else{
			$arr_sum_price[$mr_floor_id][$mr_resive_emp_id]['s_quty'] 						= $data_j['quty'];	

		}

		

		
	}
	
//$sumcount = array_sum ( $  ) ;
//$price = array_sum ( $arr_price ) ;
//$sum_price = array_sum ( $arr_sum_price ) ;
// echo "<pre>".print_r($data,true)."</pre>";
// exit;
//echo $date_report;


	$newdata=array();
	$page = 1;
	foreach($datasorce as $m => $data_l){
		$no=1;
		$index 	= 1;
		$total  = (isset($arr_total_price[$m]['s_quty'])?$arr_total_price[$m]['s_quty']:0);
		foreach($data_l as $k => $data_k){
		
			$count  = $arr_sum_price[$m][$k]['s_quty'];
			foreach($data_k as $l 	=> $data_){
				$data_['no'] 							= $no;
				$data_['count'] 						= $count;
				$data_['pr'] 							= (isset($arr_sum_price[$m][$k]['s_mr_post_price'])?$arr_sum_price[$m][$k]['s_mr_post_price']:0);
				$data_['sumpr'] 						= (isset($arr_sum_price[$m][$k]['s_mr_post_totalprice'])?$arr_sum_price[$m][$k]['s_mr_post_totalprice']:0);

				$newdata['data'][$page]['data'][$l] 	=  $data_;
				$newdata['data'][$page]['head']['data'] =  $data_;
				$newdata['data'][$page]['head']['page'] =  $page;
				$newdata['allpage'] =  $page;
				$no++;
				if($index >= 30){
					if($count>0){
						$index =2;
					}else{
						$index =1;
					}
					$page++;
				}else{
					if($count>0){
						$index +=2;
					}else{
						$index +=1;
					}
					
				}
				$count = 0;
			}
		}
		$data_['no'] 							= $no;
		$data_['total'] 						= $total;
		

		$newdata['data'][$page]['data'][$l+1] 	=  $data_;
		$newdata['data'][$page]['head']['data'] =  $data_;
		$newdata['data'][$page]['head']['page'] =  $page;
		$newdata['allpage'] =  $page;
		$page++;
	}
}





// echo "<pre>".print_r($newdata,true)."</pre>";
// echo $date_report;
// // echo ">>".$round_print;
// exit;















$txt_html='
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Garuda";
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Garuda";
	font-size: 16px
 }
 tr,td,th{
	font-family: "Garuda";
	font-size: 12px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

    @media print {
	#print_p{
		  display:none;
		}
      html,
      body {
        width: 210mm;
        height: 297mm;
		 font-family: "Garuda";
		 font-size:12px;
      }
      .page {
		width: 210mm;
		height: 297mm;
		min-height: 297mm;
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: unset;
      }
    }
  </style>
</head>

<body>
  <div class="book">
   
   ';
foreach( $newdata['data'] as $i => $page){


//echo"kkkk<br>";
   $txt_html.='  <div class="page">
      <div class="subpage ">
        <div class="header mb-3">
          
          
	
		</div>
		<table width="100%">
			<tr>
				<td width="70%" align="center">
					<h5 class="">สารบรรณกลางและโลจิสติกส์  บริหารทรัพยากรอาคาร</h5>
					<h6 class="">ใบสรุปรายงาน-ส่งลูกค้า/หน่วยงาน</h6>
				</td>
				
				<td  align="right">
					หน้า  :&nbsp;&nbsp;&nbsp;<b><u>'.$page['head']['page'].'/'.$newdata['allpage'].'&nbsp;&nbsp;&nbsp;</u></b><br>
					วันที่  <b><u> '.$date_report.' รอบ '.$page['head']['data']['mr_round_name'].'</u></b></p>
					<p class="card-text">เวลาพิมพ์  <b><u>'.date('Y-m-d').'   &nbsp;&nbsp;&nbsp;'.date('H:i:s').'&nbsp;&nbsp;&nbsp;</u></b> </p>
				</td>
				
            </tr>
            <tr>
				<td colspan="2" align="left">
				หน่วยงานผู้รับ : '.$page['head']['data']['dep_code_resive'].' : '.$page['head']['data']['dep_resive'].' : '.$page['head']['data']['f_name'].'
				</td>
            </tr>
		 </table>
	
		 

        <div class="table-responsive" style="height:800px;">
          <table   class="table table-sm">
            <thead class="thead-light">
              <tr>
                <th width="60%" class="text-center">ผู้รับ/ผู้ส่ง</th>
                <th class="text-center"></th>
                <th class="text-center">เลขที่งาน</th>
                <th class="text-center">จำนวน</th>
                <th>หมายเหตุ</th>
              </tr>
            </thead>';
           $txt_html.='<tbody>';
		   $arr_total = array();
		   foreach( $page['data'] as $subd){
				//foreach( $emp as $subd){
					
					if($subd['no'] == "all"){
						$txt_html.='
						<tr>
							<th class="text-right"colspan="3"><u>รวมจำนวนซองทั้งสิ้น</u></th>
							<th class="text-center" ><u>{{subd.qty}}</u></th>
							<th></th>
						</tr>';

					}else{ 
						
						if(isset($subd['total']) and $subd['total']!=0){
							$arr_total = $subd;
						}else{
						
							if($subd['count']!=0){
								$txt_html.=' 
								<tr>
									<th class="text-left" colspan="3">'.$subd['mr_emp_code'].'	'.$subd['mr_emp_name'].' '.$subd['mr_emp_lastname'].'		</th>
									<th class="text-center" colspan="0">'.$subd['count'].'</th>
								</tr>';
								$txt_html.=' 
								<tr>
									<td class="text-right">'.$subd['mr_cus_name'].'	</td>
									<td class="text-center"></td>
									<td class="text-center">'.$subd['mr_work_barcode'].'</td>
									<td class="text-center">'.$subd['quty'].'</td>
									<td>'.$subd['mr_work_remark'].'</td>
								</tr>';

							}else{
								$txt_html.=' 
								<tr>
									<td class="text-right">'.$subd['mr_cus_name'].' 	</td>
									<td class="text-center"></td>
									<td class="text-center">'.$subd['mr_work_barcode'].'</td>
									<td class="text-center">'.$subd['quty'].'</td>
									<td>'.$subd['mr_work_remark'].'</td>
								</tr>';

							}
						}
						
						
					}
				//}
			}
			if(!isset($arr_total['total'])){
				$arr_total['total']= 0;
			}
			if($arr_total['total']!=0){
				$subd = $arr_total;
						$txt_html.=' 
						<tr>
							<th class="text-left" colspan="2">รวมทั้งหมด  		</th>
							<th class="text-center" colspan="0"></th>
							<th class="text-center" colspan="0">'.(isset($subd['total'])?$subd['total']:0).'</th>
							<th class="text-center" colspan="0"></th>
						</tr>';
					
			}
            $txt_html.='</tbody> ';
			
		// 	$txt_html.='
        //   </table>
        // </div>
		// <br>
		// 		<br>
		// 		<br>';
				if(!isset($subd['total'])){
					$subd['total']= 0;
				}
				if($subd['total']!=0){
					//$txt_html.='
					//<tr class="bg-primary">
					//	<th></th>
					//	<th></th>
					//	<th class="text-right"><u> &nbsp;&nbsp;&nbsp; รวม &nbsp;&nbsp;&nbsp;</u></th>
					//	<th class="text-center"><u>&nbsp;&nbsp;&nbsp;'.$price.'&nbsp;&nbsp;&nbsp;</u></th>
					//	<th class="text-center"><u>&nbsp;&nbsp;&nbsp;'.$sumcount.'&nbsp;&nbsp;&nbsp;</u></th>
					//	<th class="text-center"><u>&nbsp;&nbsp;&nbsp;'.$sum_price.'&nbsp;&nbsp;&nbsp;</u></th>
					//	<th></th>
					//	<th></th>
					//</tr>
					//</tbody> ';
					$txt_html.='';
			
					$txt_html.='
					
				  
				  </table>
				</div>
				<br>
						<br>
						<br>';


				$txt_html.='<center>
				
				
				<table border="0" width="100%"   class="center">
				<tr>
					<td align="center">
						<p>ลงชื่อ...........................................</p>
						<p>(..............................................)</p>
						<p>วันที่................................</p>
						<p>เจ้าหน้าที่ บ.ไพวอท</p>
					</td>
					<td align="center">
						<p>ลงชื่อ...........................................</p>
						<p>(..............................................)</p>
						<p>วันที่................................</p>
						<p>พนักงานนำส่ง บ.ไพวอท </p>
					</td>
					
					<td align="center">
						<p>ลงชื่อ...........................................</p>
						<p>(..............................................)</p>
						<p>วันที่................................</p>
						<p>ผู้รับเอกสาร</p>
					</td>
				</tr>

				
			</table>
			</center>';
				}else{
						$txt_html.='</tbody> ';
				
						$txt_html.='
					</table>
					</div>
					<br>
							<br>
							<br>';
				}
			$txt_html.='
			
      </div>
    </div>';

}
$txt_html.='  
  

 
</body>

</html>';




// echo '<pre>'.print_r($newdata,true).'</pre>';
// echo $txt_html;
// exit;
function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}





require_once 'mpdf-development/vendor/autoload.php';
 $mpdf = new \Mpdf\Mpdf();
 $pdf_orientation = 'P';//L&P;
 $left=1;
 $right=1;
 $top=1;
 $bottom=1;
 $header=0;
 $footer=1;
// $align = $__pdf_pgn_oalign;
$border = "border-top-";
// $pgf_o = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fo", $pgf_o);
// $align = $__pdf_pgn_ealign;
// $pgf_e = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fe", $pgf_e);
$mpdf->AddPage($pdf_orientation,'','','','',$left,$right,$top,$bottom,$header,$footer,'','','html_fo', 'html_fe', 0, 0, 1, 1);

 //$mpdf->AddPage('L'); 
 $mpdf->WriteHTML($txt_html);
 //$mpdf->setFooter('{PAGENO}');
 $mpdf->Output();
exit;


//fpr PHP5.6
// require_once 'ThaiPDF/thaipdf.php';
// $left=3;
// $right=3;
// $top=5;
// $bottom=5;
// $header=0;
// $footer=5;
// $filename='file.pdf';
// pdf_margin($left,$right,$top, $bottom,$header,$footer);
// pdf_html($txt_html);
// pdf_orientation('P');        
// pdf_echo();

// exit;
//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//exit;
$template = Pivot_Template::factory('mailroom/print_sort_branch_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$alert 			= '';

$data  			= array();
$round_print 	= $req->get('round_printreper');
$date_report 	= $req->get('date_report');

	

//echo '<pre>'.print_r($date_report,true).'</pre>';

//exit;
if(preg_match('/<\/?[^>]+(>|$)/', $round_print)) {
	$alert = "
	$.confirm({
		title: 'Alert!',
		content: 'เกิดข้อผิดพลาด!',
		buttons: {
			OK: function () {
				location.href = 'create_work_byHand_out.php';
				}
			}
		});
	";
}else if(preg_match('/<\/?[^>]+(>|$)/', $date_report)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{

//echo "<pre>".print_r($req,true)."</pre>";
// echo $date_report;
// echo ">>".$round_print;
// 	exit;

	$sql='SELECT
				w_m.mr_work_main_id,
				DATE_FORMAT(w_m.mr_work_date_sent, "%Y-%m-%d") as d_send,
				w_m.mr_work_barcode,
				w_m.mr_work_remark,
				w_p.mr_address,
				w_p.mr_post_price,
				w_p.mr_post_totalprice,
				w_p.sp_num_doc,
				w_p.num_doc,
				w_p.mr_post_weight,
				w_m.quty,
				pv.mr_provinces_name,
				w_p.mr_send_emp_detail,
				w_p.mr_cus_tel,
				w_p.mr_send_emp_id,
				w_p.mr_cus_name as sresive_name,
				w_p.mr_cus_lname as resive_lname,
				concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_resive,
				w_p.mr_address as sendder_address,
				dep.mr_department_name as dep_resive,
				dep.mr_department_code as dep_code_resive,
				emp_send.mr_emp_code ,
				emp_send.mr_emp_name,
				emp_send.mr_emp_lastname,
				concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_send,
				r.mr_round_name,
				s.mr_status_id,
				s.mr_status_name,
				w_p.mr_type_post_id,
				t_p.mr_type_post_name
			FROM
				mr_work_main w_m
				LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
				LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
				LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
				LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_p.mr_send_emp_id)
				LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
				LEFT join  mr_provinces pv on(pv.mr_provinces_id = w_p.mr_province_id)
			WHERE
				w_m.mr_work_date_sent like"'.$date_report.'%"
				and w_m.mr_type_work_id = 6
				and w_m.mr_status_id != 6
			   ';

		if($round_print!='') {
			 $sql.='
				and w_m.mr_round_id = 	'.$round_print.' ';
				}

		$sql.='
		group by w_m.mr_work_main_id 
		order by w_m.mr_work_main_id asc';	
		


		
		
	
	$data 			= $send_workDao->select($sql);
	$datasorce 		= array();
	$arr_sum_price 	= array();
	$arr_total_price 	= array();
	foreach($data as $j => $data_j){
		if($data_j['mr_post_totalprice']<=0){
			$data_j['mr_post_totalprice'] = $data_j['quty']*$data_j['mr_post_price'];
		}
		$mr_send_emp_id 										= $data_j['mr_send_emp_id'];
		$mr_type_post_id 										= $mr_send_emp_id.'_'.$data_j['mr_type_post_id'];

		$datasorce[$mr_type_post_id][$mr_send_emp_id][$j] 								=  $data_j;

		if(isset($arr_total_price[$mr_type_post_id]['s_quty'])){
			$arr_total_price[$mr_type_post_id]['s_quty'] 						+= $data_j['quty'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_price'] 				+= $data_j['mr_post_price'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_totalprice'] 			+= $data_j['mr_post_totalprice'];
		}else{
			$arr_total_price[$mr_type_post_id]['s_quty'] 						= $data_j['quty'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_price'] 				= $data_j['mr_post_price'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_totalprice'] 			= $data_j['mr_post_totalprice'];
		}
			

		if(isset($arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_quty'])){
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_quty'] 						+= $data_j['quty'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_price'] 				+= $data_j['mr_post_price'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_totalprice'] 			+= $data_j['mr_post_totalprice'];	
		}else{
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_quty'] 						= $data_j['quty'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_price'] 				= $data_j['mr_post_price'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_totalprice'] 			= $data_j['mr_post_totalprice'];	

		}

		

		
	}
	
//$sumcount = array_sum ( $  ) ;
//$price = array_sum ( $arr_price ) ;
//$sum_price = array_sum ( $arr_sum_price ) ;
// echo "<pre>".print_r($datasorce,true)."</pre>";
// exit;
//echo $date_report;


	$newdata=array();
	$page = 1;
	$no=1;
	foreach($datasorce as $m => $data_l){
		
		$index 	= 1;
		$total  = $arr_total_price[$m]['s_quty'];
		foreach($data_l as $k => $data_k){
		
			$count  = $arr_sum_price[$m][$k]['s_quty'];
			foreach($data_k as $l 	=> $data_){
				$data_['no'] 							= $no;
				$data_['mr_address'] 					= '<div class="text">'.$data_['mr_address']."</div>";
				$data_['count'] 						= $count;
				$data_['pr'] 							= $arr_sum_price[$m][$k]['s_mr_post_price'];
				$data_['sumpr'] 						= $arr_sum_price[$m][$k]['s_mr_post_totalprice'];
				$newdata['data'][$page]['data'][$l] 	=  $data_;
				$newdata['data'][$page]['head']['data'] =  $data_;
				$newdata['data'][$page]['head']['page'] =  $page;
				$newdata['allpage'] =  $page;
				$no++;
				if($index >= 15){
					if($count>=1){
						$index =1;
					}else{
						$index =1;
					}
					$page++;
				}else{
					if($count>=1){
						$index +=1;
					}else{
						$index +=1;
					}
				}
				$count = 0;
			}
			// $page++;
		}
		$data_['no'] 							= $no;
		$data_['total'] 						= $total;
		$data_['total_pr'] 						= $arr_total_price[$m]['s_mr_post_price'];
		$data_['total_sumpr'] 					= $arr_total_price[$m]['s_mr_post_totalprice'];

		$newdata['data'][$page]['data'][$l+1] 	=  $data_;
		$newdata['data'][$page]['head']['data'] =  $data_;
		$newdata['data'][$page]['head']['page'] =  $page;
		$newdata['allpage'] =  $page;
		$page++;
		$no=1;
	}
}





//  echo "<pre>".print_r($newdata,true)."</pre>";
// echo $date_report;
// // echo ">>".$round_print;
//  exit;



$txt_html='
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>ใบคำสั่งงานขาออก(รายวัน)</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700|Sarabun:400,700&display=swap" rel="stylesheet">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      font-family: \'Nunito\', \'Sarabun\', sans-serif;
      font-size: 12px;
    }

    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }

    .header {
      width: 100%;
    }
    
    .header_child {
        float: left;
    }

    .text_header {
        width: auto;
        text-align: center;
        margin-left: 8%;
        margin-top: 20px;
        
    }

    .lst_header {
       list-style-type: none;
    }

    .lst_header > li {
        text-align: center;
        line-height: 25px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    .img_logo {
        max-width: 80px;
    }

    .clearfix:after {
        content: "";
        visibility: hidden;
        display: block;
        height: 0;
        clear: both;
    }


    .tb_data, th {
        border: 1px solid black;
    }
	
    .tb_data, td {
        border: 0px solid #000 !important;
        border-bottom: 1px solid #000 !important;
    }

    .left {
        float:left; 
        width:33.3%;
    }

    .box-sizing { 
        box-sizing: border-box; margin: 0; 
    }

    .box-sizing > p {
        margin-bottom: 2px !important;
    }

    .cus_left {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        display: inline-block;
        float: left;
        padding: 2px;
        width: 50%;
    }

    .cus_right {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        display: inline-block;
        float: left;
        padding: 2px;
        width: 50%;
    }

    .cus_left > p ,
    .cus_right > p {
        margin-bottom: 3px !important;
    }

    .tb_docs tr th,
    .tb_docs tr td {
        border: 1px solid #fff;
        padding: 0;
    }

   

    .sign_block {
        padding: 5px;
        line-height: 12px;
    }
    
    .sign_name {
        margin-bottom: 12px !important;
    }
 
    
    .lst_kyc {
         padding-top: 2px !important;
    }
    
    .lst_kyc > li {
        list-style-type: none;
        float: left;
        padding-right: 50px;
    }

    .order_kyc > li > p {
        margin-bottom: 5px !important;
    }

    .dotted:after {
        margin-right: 20px;
    }

    .list-none {
        list-style-type: none;
    }
    @page {
      size: A4;
      margin: 0;
    }

    @media print {
      html,
      body {
        width: 210mm;
        height: 297mm;
        font-family: \'Nunito\', \'Sarabun\', sans-serif;
        font-size: 16px;
        
      }
      .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
      }

      .text_header {
          margin-left: 20%;
      }
	  .tb-border-none tr th,
    .tb-border-none tr td {
        border: 0px solid #fff !important; 
    }

	.tb_data tr th{
        border: 1px solid black !important;
        white-space: nowrap!important;
		height: 40px;
		text-align: center;
		vertical-align: middle;
    }
	.text {
		overflow: hidden;
		display: -webkit-box;
		-webkit-line-clamp: 2; /* number of lines to show */
				line-clamp: 2; 
		-webkit-box-orient: vertical;
		text-align: left;
	 }
	 .border{
        border: 2px solid #000 !important;
    }

    }
	.tb-border-none tr th,
    .tb-border-none tr td {
        border: 0px solid #fff !important; 
    }

	.tb_data tr th{
        border: 1px solid black !important;
        white-space: nowrap!important;
		height: 40px;
		text-align: center;
		vertical-align: middle;
    }
	.text {
		overflow: hidden;
		display: -webkit-box;
		-webkit-line-clamp: 2; /* number of lines to show */
				line-clamp: 2; 
		-webkit-box-orient: vertical;
		text-align: left;
	 }
	 .border{
        border: 2px solid #000 !important;
    }
	.border1{
        border: 1px solid #000 !important;
    }
	.border1 tr td{
        border: 1px solid #000 !important;
        border-bottom: 2px solid #000 !important;
    }
	.center{
		text-align: center;
	}
  </style>
</head>

<body>
  <div class="book">
   ';
foreach( $newdata['data'] as $i => $page){
//echo"kkkk<br>";
   $txt_html.='
   <div class="page">
      <div class="subpage ">
        <div class="header mb-3">
          
          
	
		</div>
		<table width="100%" class="tb-border-none">
			<tr>
				<td align="center" colspan="2">
					<h5 class="">สารบรรณกลางและโลจิสติกส์  บริหารทรัพยากรอาคาร</h5>
					<h6 class="">ใบสรุปรายงาน-ส่ง ปณ.</h6>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					วันที่: <u> '.$date_report.'</u>->
					รอบ: <u>'.$page['head']['data']['mr_round_name'].'</u>->
					เวลาพิมพ์: <u>'.date('Y-m-d H:i:s').'</u> 
				</td>
			</tr>
			<tr>
			<td width="70%" align="center">
				</td>
				<td  align="right">
					หน้า  :&nbsp;&nbsp;&nbsp;<u>'.$page['head']['page'].'/'.$newdata['allpage'].'</u>
				</td>
				
            </tr>
            <tr>
				<td colspan="2" align="left">
				<h6>ประเภทการส่ง : '.$page['head']['data']['mr_type_post_name'].'</h6>
				</td>
            </tr>
		 </table>
		 
		 
		 

		 <div class="" style="height:900px;">
          <table   class="tb_data table table-sm w-100 border">
            <thead class="thead-light">
              <tr>
				';
				$type_re = array(3,5);
				if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
				{
					$txt_html.='
					<th width="20%" colspan="2" class="text-center">ผู้ส่ง/ผู้รับ</th>
					<th class="text-center" width="25%">ปลายทาง</th>
					';
				}else{

					$txt_html.='
					<th width="20%" colspan="2" class="text-center">ผู้ส่ง/ผู้รับ</th>
					<th class="text-center" width="35%">ปลายทาง</th>
					';
				}

				$txt_html.='
                <th class="text-center" width="15%">ทะเบียน</th> 
				';
				$type_re = array(3,5);
				if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
				{
					$txt_html.='<th class="text-center" width="15%">เลขตอบรับ</th> ';
				}

				$txt_html.='
                <th class="text-center" width="5%">ราคา</th>
                <th class="text-center" width="5%" >จำนวน</th>
                <th class="text-center" width="5%">ราคารวม</th>
                <th>หมายเหตุ</th>
              </tr>
            </thead>';
           $txt_html.='<tbody>';
			$arr_total = array();
			$loop = 1;
		   foreach( $page['data'] as  $subd){
				//foreach( $emp as $subd){
					
					if($subd['no'] == "all"){
						$txt_html.='
						<tr>
							<th class="text-right"colspan="4"><u>รวมจำนวนซองทั้งสิ้น</u></th>
							<th class="text-center" ><u>{{subd.qty}}</u></th>
							<th></th>
						</tr>';

					}else{ 

						$arr_total= array();
						if(isset($subd['total'])){
							if($subd['total']!=0){
								$arr_total = $subd;
								
							}
						}
						
						if(empty($arr_total)){
							if($subd['count']!=0){
								$type_re = array(3,5);
								if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
									{
								$txt_html.=' 
								<tr>
									<td class="text-left" colspan="5"><h6>'.$subd['mr_send_emp_detail'].'</h6></td>
									<td class="text-center border1" colspan="0"></td>
									<td class="text-center border1" colspan="0">'.$subd['count'].'</td>
									<td class="text-center border1" colspan="0">'.$subd['sumpr'].'</td>
									<td class="text-center border1" colspan="0"></td>
								</tr>';
							}else{
								$txt_html.=' 
								
								<tr>
									<td class="text-left" colspan="4"><h6>'.$subd['mr_send_emp_detail'].'</h6></td>
									<td class="text-center border1" colspan="0"></td>
									<td class="text-center border1" colspan="0">'.$subd['count'].'</td>
									<td class="text-center border1" colspan="0">'.$subd['sumpr'].'</td>
									<td class="text-center border1" colspan="0"></td>
								</tr>';
							}


								$txt_html.=' 
								<tr>
									<td class="text-right tb-border-none">'.$subd['no'].'</td>
									<td class="text-right"><div class="text">'.((isset($subd['sresive_name']))?$subd['sresive_name']:'').' '.((isset($subd['sresive_lname']))?$subd['sresive_lname']:'').'		</div></td>

									<td class="text-center">'.$subd['mr_address'].'</td>
									<td class="text-center">'.$subd['num_doc'].'</td>
									';
									$type_re = array(3,5);
									if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
									{
										$txt_html.='<td lass="">'.$subd['sp_num_doc'].'</td> ';
									}

									$txt_html.='
									<td class="text-center border1">'.$subd['mr_post_price'].'</td>
									<td class="text-center border1">'.$subd['quty'].'</td>
									<td class="text-center border1">'.$subd['mr_post_totalprice'].'</td>
									<td lass="border1"><div class="text">'.$subd['mr_work_remark'].'</div></td>
								</tr>';

							}else{
								$txt_html.=' 
								<tr>
									<td class="text-right tb-border-none">'.$subd['no'].'</td>
									<td class="text-right"><div class="text">'.((isset($subd['sresive_name']))?$subd['sresive_name']:'').' '.((isset($subd['sresive_lname']))?$subd['sresive_lname']:'').'		</div></td>
									<td class="text-center">'.$subd['mr_address'].'</td>
									<td class="text-center">'.$subd['num_doc'].'</td>
									';
									$type_re = array(3,5);
									if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
									{
										$txt_html.='<td>'.$subd['sp_num_doc'].'</td> ';
									}

									$txt_html.='
									<td class="text-center border1">'.$subd['mr_post_price'].'</td>
									<td class="text-center border1">'.$subd['quty'].'</td>
									<td class="text-center border1">'.$subd['mr_post_totalprice'].'</td>
									<td class="border1 text"><div class="text">'.$subd['mr_work_remark'].'</div></td>
								</tr>';

							}
						}
						
					}
				//}
				$loop++;
			}
			
			if(!isset($arr_total['total'])){
				$arr_total['total']	= 0;
			}
			if($arr_total['total']!=0){
				$subd = $arr_total;
				$type_re = array(3,5);
				if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
					{
						$txt_html.=' 
						<tr>
							<th class="text-left" colspan="5">รวมทั้งหมด  		</th>
							<th class="text-center" colspan="0">'.$subd['total_pr'].'</th>
							<th class="text-center" colspan="0">'.$subd['total'].'</th>
							<th class="text-center" colspan="0">'.$subd['total_sumpr'].'</th>
							<th class="text-center" colspan="0"></th>
						</tr>';
					}else{
						$txt_html.=' 
						<tr>
							<th class="text-left" colspan="4">รวมทั้งหมด  		</th>
							<th class="text-center" colspan="0">'.$subd['total_pr'].'</th>
							<th class="text-center" colspan="0">'.$subd['total'].'</th>
							<th class="text-center" colspan="0">'.$subd['total_sumpr'].'</th>
							<th class="text-center" colspan="0"></th>
						</tr>';
					}
			}
			

            $txt_html.='</tbody> ';
				if(isset($subd['total'])){
					$txt_html.='
					</table>
						</div>
						<br>
								<br>
								<br>';


						$txt_html.='<center>
						
						
						<table width="100%"   class="center border1">
						<tr lass="border1">
							<td align="center border1">
								<p>ลงชื่อ...........................................</p>
								<p>(..............................................)</p>
								<p>วันที่................................</p>
								<p>เจ้าหน้าที่ บ.ไพวอท</p>
							</td>
							<td align="center border1">
								<p></p>
								<p></p>
								<p></p>
								<p>ประทับตราไปรษณีย์</p>
							</td>
							<td align="center border1">
								<p>ลงชื่อ...........................................</p>
								<p>(..............................................)</p>
								<p>วันที่................................</p>
								<p>เจ้าหน้าที่ไปรษณีย์</p>
							</td>
						</tr>

						
					</table>
					</center>';
				}else{
						$txt_html.='</tbody> ';
				
						$txt_html.='
					</table>
					</div>
					<br>
							<br>
							<br>';
				}
			$txt_html.='
			
      </div>
    </div>';

}
$txt_html.='  
  

 <script>
  window.print();
 </script>
</body>

</html>';




// echo '<pre>'.print_r($newdata,true).'</pre>';
echo $txt_html;
exit;

function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}





require_once 'mpdf-development/vendor/autoload.php';
 $mpdf = new \Mpdf\Mpdf();
 $pdf_orientation = 'P';//L&P;
 $left=3;
 $right=3;
 $top=5;
 $bottom=5;
 $header=0;
 $footer=1;
//$align = $__pdf_pgn_oalign;
$border = "border-top-";
// $pgf_o = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
$mpdf->DefHTMLFooterByName("fo", $pgf_o);
// $align = $__pdf_pgn_ealign;
// $pgf_e = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fe", $pgf_e);
$mpdf->AddPage($pdf_orientation,'','','','',$left,$right,$top,$bottom,$header,$footer,'','','html_fo', 'html_fe', 0, 0, 1, 1);

 //$mpdf->AddPage('L'); 
 $mpdf->WriteHTML($txt_html);
 //$mpdf->setFooter('{PAGENO}');
 $mpdf->Output();
exit;


//fpr PHP5.6
// require_once 'ThaiPDF/thaipdf.php';
// $left=3;
// $right=3;
// $top=5;
// $bottom=5;
// $header=0;
// $footer=5;
// $filename='file.pdf';
// pdf_margin($left,$right,$top, $bottom,$header,$footer);
// pdf_html($txt_html);
// pdf_orientation('P');        
// pdf_echo();

// exit;
//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//exit;
$template = Pivot_Template::factory('mailroom/print_sort_branch_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuserid($user_id);
$alert = '';
$data  = array();
$mr_send_work_id = $req->get('id');
if($mr_send_work_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'send_work_all.php';
			}
		}
	});
		
	";
}else{

	$sql="SELECT 
					m.*,
					sw.sender_tel as sw_sender_tel,
					sw.barcode as sw_barcode,
					wb.mr_branch_floor ,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					d.mr_department_code as re_mr_department_code,
					d.mr_department_name as re_mr_department_name,
					b.mr_branch_code as re_mr_branch_code,
					b2.mr_branch_code as send_mr_branch_code,
					b.mr_branch_name as re_mr_branch_name,
					b2.mr_branch_name as send_mr_branch_name
			FROM mr_work_main m
			left join mr_send_work sw using(mr_send_work_id)
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
					Left join mr_branch b2 on ( b2.mr_branch_id = e.mr_branch_id )
					where m.mr_send_work_id = ".$mr_send_work_id."
					group by m.mr_work_main_id
			";	
		


		
	$sql2="SELECT 
					*
			FROM  mr_send_work sw
			LEFT JOIN mr_user u ON ( u.mr_user_id = sw.mr_user_id )
			left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_work_inout b on ( b.mr_branch_id = e.mr_branch_id )
			Left join mr_branch bb on ( bb.mr_branch_id = e.mr_branch_id )
			where sw.mr_send_work_id = ".$mr_send_work_id."
			group by sw.mr_send_work_id
			";	
	$data 		= $send_workDao->select($sql);
	$data2 		= $send_workDao->select($sql2);

	$apush['re_mr_branch_code']='';
	array_push($data,$apush);
	//echo print_r($data,true);
	$page 	= 1;
	$index 	= 1;
	$no=1;
	$newdata = array();
	foreach($data as $i => $val_i){
		if(count($data)-1 == $i){
			$val_i=$data[($i-1)];
			$val_i['no']='all';
			$val_i['qty']=count($data)-1;
		}else{
			$val_i['no']=$no;
			$val_i['qty']=1;
		}
			$newdata[$page]['h']['d'] = $data2[count($data2)-1];
			$newdata[$page]['h']['p'] = $page;
			$newdata[$page]['h']['all'] = ceil(count($data)/10);  
			$newdata[$page]['d'][$index] = $val_i;
			if($index == 10){
				$index = 1;
				$page++;
			}else{
				$index++;
			}
			
		
			$no++;	
		}
}
$template = Pivot_Template::factory('mailroom/print_send_all.tpl');
$template->display(array(
	//'debug' => print_r($newdata,true),
	'data' => $newdata,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
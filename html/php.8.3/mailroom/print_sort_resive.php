<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
set_time_limit(0);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuserid($user_id);
$alert = '';
$data  = array();
$mr_work_main_id = $req->get('data_option');
$date 				= $req->get('date');

	

// echo '<pre>'.print_r($mr_work_main_id,true).'</pre>';

// exit;
if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_main_id)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else if($mr_work_main_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{

	$sql="SELECT 
					m.*,
					count(m.mr_work_main_id) as quty,
					hub.*,
					wb.mr_branch_floor ,
					wb.mr_emp_id  	as re_emp_id ,
					e.mr_emp_code as send_emp_code,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					e2.mr_emp_code as re_emp_code,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					d.mr_department_code as re_mr_department_code,
					d.mr_department_name as re_mr_department_name,
					b.mr_branch_code as re_mr_branch_code,
					b2.mr_branch_code as send_mr_branch_code,
					b.mr_branch_name as re_mr_branch_name,
					b2.mr_branch_name as send_mr_branch_name
					FROM mr_round_resive_work rw
					
					
					LEFT JOIN mr_work_main m  ON ( m.mr_work_main_id = rw.mr_work_main_id )
					LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = wb.mr_branch_id )
					Left join mr_hub hub on ( hub.mr_hub_id = b.mr_hub_id )

					
					Left join mr_branch b2 on ( b2.mr_branch_id = e.mr_branch_id )
					where m.mr_work_main_id in (".$mr_work_main_id.")
					and rw.date_send BETWEEN '".$date."' AND '".$date."'
					group by m.mr_work_main_id
					order by b2.mr_branch_code,m.mr_work_main_id asc

					
			";	
		


		
	
	$data 		= $send_workDao->select($sql);

	$dataN = array();
	foreach($data as $i => $val_i){
		$re_emp_id = $val_i['re_emp_id'];
		if($re_emp_id == ''){
			$re_emp_id = "000";
			$val_i['re_emp_id'] = '000';
			$val_i['re_name'] = 'ไม่พบข้อมูลสาขา';
		}

		$dataN[$re_emp_id][$i] = $val_i; 

	}

	// echo '<pre>'.print_r($dataN,true).'</pre>';

	// exit;
	$newdata=array();
	$newdatahub=array();
	$index 	= 1;
	$page = 1;
	$no=1;
	foreach($dataN as $i => $data_){
		$no=1;
		foreach($data_ as $i => $val_i){
			
			
			$re_emp_id = $val_i['re_emp_id'];
			if($re_emp_id == ''){
				$re_emp_id = "nobranch";
				$val_i['re_emp_id'] = '000';
				$val_i['re_name'] = 'ไม่พบข้อมูลสาขา';
			}
				
				// runpage*************************
				if(!isset($newdatahub[$re_emp_id]['no'])){
					$newdatahub[$re_emp_id]['no'] = 1;
					$newdatahub[$re_emp_id]['check'] = 1;
				}else{
					$newdatahub[$re_emp_id]['no'] += 1;
					$newdatahub[$re_emp_id]['check'] += 1;
				}
				if(!isset($newdatahub[$re_emp_id])){
					$newdatahub[$re_emp_id]['count']=1;
					$newdatahub[$re_emp_id]['dd'][$page]['head']['p'] = 1;
				}else{
					if(!isset($newdatahub[$re_emp_id]['dd'][$page])){
						if(!isset($newdatahub[$re_emp_id]['count'])){
							$newdatahub[$re_emp_id]['count']=0;
						}
						$newdatahub[$re_emp_id]['count']+=1;
						$newdatahub[$re_emp_id]['dd'][$page]['head']['p'] = $newdatahub[$re_emp_id]['count'];
					}
				}
				
				// end runpage*************************
			
				$val_i['no'] = $newdatahub[$re_emp_id]['no'];
				//$val_i['quty'] = 1;
				$newdatahub[$re_emp_id]['dd'][$page]['data'][$index] = $val_i;
				$newdatahub[$re_emp_id]['dd'][$page]['head']['d']		= $val_i;
				$newdatahub[$re_emp_id]['dd'][$page]['head']['date']		= date('d/m/Y');
				$newdatahub[$re_emp_id]['dd'][$page]['head']['time']		= date('H:i:s');
				$no++;
				
				// runpage*************************
				if($newdatahub[$re_emp_id]['check'] == 27){
					$newdatahub[$re_emp_id]['check'] = 0;
					$index = 1;
					$page++;
				}else{
					$index++;
				}
				// end runpage*************************
			
			
			
		}
	}
}


	// echo '<pre>'.print_r($newdatahub,true).'</pre>';

	// exit;



















$txt_html='
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Garuda";
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Garuda";
	font-size: 16px
 }
 tr,td,th{
	font-family: "Garuda";
	font-size: 12px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

    @media print {
	#print_p{
		  display:none;
		}
      html,
      body {
        width: 210mm;
        height: 297mm;
		 font-family: "Garuda";
		 font-size:12px;
      }
      .page {
		width: 210mm;
		height: 297mm;
		min-height: 297mm;
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: unset;
      }
    }
  </style>
</head>

<body>
  <div class="book">
   
   ';
foreach( $newdatahub as $i => $pa){
if(!isset($pa['dd'])){
	//$pa['dd'] = array();
}
foreach( $pa['dd'] as $dt){
//echo"kkkk<br>";
   $txt_html.='  <div class="page">
      <div class="subpage ">
        <div class="header mb-3">
          
          
	
		</div>
		<table width="100%">
			<tr>
				<td width="85%" align="center">
					<h5 class="">สารบรรณกลางและโลจิสติกส์  บริหารทรัพยากรอาคาร</h5>
					<h6 class="">ใบนำส่ง-ตอบรับเอกสาร ในอาคาร</h6>
				</td>
				
				<td  align="right">
					หน้า  :&nbsp;&nbsp;&nbsp;<b><u>'.$dt['head']['p'].'/'.$pa['count'].'&nbsp;&nbsp;&nbsp;</u></b><br>
					วันที่  <b><u>  '.$dt['head']['date'].' </u></b></p>
					<p class="card-text">เวลา  <b><u>&nbsp;&nbsp;&nbsp;'.$dt['head']['time'].'&nbsp;&nbsp;&nbsp;</u></b> </p>
				</td>
				
            </tr>
		 </table>
		 
		 
		 <div class="form-group">
            <label class="">ถึง : '.$dt['head']['d']['re_emp_code'].'
								  -'.$dt['head']['d']['re_name'].' 
								  '.$dt['head']['d']['re_lname'].'
								    '.$dt['head']['d']['re_mr_department_code'].'
								    '.$dt['head']['d']['re_mr_department_name'].'
								  </label>
        </div>

        <div class="table-responsive" style="height:785px;">
          <table   border="0" class="table table-sm">
            <thead class="thead-light">
              <tr>
				<th width="10%">ลำดับ</th>
                <th width="30%">ผู้ส่งเอกสาร</th>
                <th width="30%">สาขา</th>';

              // <th>ชื่อเอกสาร</th>
              // <th>หมายเหตุ</th>
                $txt_html.='
				<th width="10%">จำนวนซอง</th>
                <th width="20%">Barcode</th>
              </tr>
            </thead>';
           
			foreach( $dt['data'] as $subd){
			$txt_html.='<tbody>';
			if($subd['no'] == "all"){
			$txt_html.='<tr>
				<th class="text-right"colspan="3"><u>รวมจำนวนซองทั้งสิ้น</u></th>
				<th class="text-center" ><u>{{subd.qty}}</u></th>
				<th></th>
			</tr>';

			}else{ 
				$txt_html.=' <tr>
                
                <td class="text-center">'.$subd['no'].'</td>
				<td>'.$subd['send_emp_code'].' : '.$subd['send_name'].'  '.$subd['send_lname'].'		</td>
                <td>'.$subd['send_mr_branch_code'].' '.$subd['send_mr_branch_name'].'</td>';
				
               // <td>'.$subd['mr_topic'].'</td>
               // <td>'.$subd['mr_work_remark'].'</td>
				$txt_html.='
                <td class="text-center">'.$subd['quty'].'</td>
                <td>'.$subd['mr_work_barcode'].'</td>
              </tr>';
			}
            $txt_html.='</tbody> ';
			}
			$txt_html.='
          </table>
        </div>
		<br>
				<br>
				<br>
				<center>
				<table border="0" width="100%"   class="center">
				<tr>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้ออกใบคุมงาน</p>
					<p>(..............................................)</p>
					<p>วันที่................................</p>
					</td>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้เดินเอกสาร</p>
					<p>(..............................................)</p>
					<p>วันที่................................</p>
					</td>
				</tr>

				<tr>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้ตรวจสอบงาน</p>
					<p>(..............................................)</p>
					<p>วันที่................................</p>
					</td>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้รับงาน/รหัสพนักงาน</p>
					<p>(..............................................)</p>
					<p>วันที่................................</p>
					</td>
				</tr>
			</table>
			</center>
			
      </div>
    </div>';
}
}
$txt_html.='  
  

 
</body>

</html>';




// echo '<pre>'.print_r($newdata,true).'</pre>';
// echo $txt_html;
// exit;
function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}





require_once 'mpdf-development/vendor/autoload.php';
 $mpdf = new \Mpdf\Mpdf();
 $pdf_orientation = 'P';//L&P;
 $left=1;
 $right=1;
 $top=1;
 $bottom=1;
 $header=0;
 $footer=1;
//$align = $__pdf_pgn_oalign;
$border = "border-top-";
//$pgf_o = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
$mpdf->DefHTMLFooterByName("fo", $pgf_o);
// $align = $__pdf_pgn_ealign;
// $pgf_e = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fe", $pgf_e);
$mpdf->AddPage($pdf_orientation,'','','','',$left,$right,$top,$bottom,$header,$footer,'','','html_fo', 'html_fe', 0, 0, 1, 1);

 //$mpdf->AddPage('L'); 
 $mpdf->WriteHTML($txt_html);
 //$mpdf->setFooter('{PAGENO}');
 $mpdf->Output();
exit;



//fpr PHP5.6
// require_once 'ThaiPDF/thaipdf.php';
// $left=1;
// $right=1;
// $top=2;
// $bottom=2;
// $header=0;
// $footer=0;
// $filename='file.pdf';
// pdf_margin($left,$right,$top, $bottom,$header,$footer);
// pdf_html($txt_html);
// pdf_orientation('P');        
// pdf_echo();

// exit;
//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//exit;
$template = Pivot_Template::factory('mailroom/print_sort_branch_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php 
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Position.php';
ini_set('max_execution_time', '0');
error_reporting(E_ALL & ~E_NOTICE);




$auth 					= new Pivot_Auth();
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();
$positionDao 		    = new Dao_Position();




$employee 				= $employeeDao->getEmpCode();
$userData 				= $userDao->get_Username();
$department_data 		= $departmentDao->getdepartmentall();
$floor_data 			= $floorDao->getFloorAll();
$branch_data 			= $branchDao->getBranch();
$position_data 			= $positionDao->getPosition();




$department = array();
foreach($department_data as $i_depart => $v_depart ){
	$department[$v_depart['mr_department_code']] = $v_depart['mr_department_id'];
}

$floor = array();
foreach($floor_data as $i_floor => $v_floor ){
	$floor[$v_floor['name']] = $v_floor['mr_floor_id'];
}

$branch = array();
foreach($branch_data as $i_branch => $v_branch_data ){
	$branch[$v_branch_data['mr_branch_code']] = $v_branch_data['mr_branch_id'];
}

$position = array();
foreach($position_data as $i_position => $v_position ){
	$position[$v_position['mr_position_code']] = $v_position['mr_position_id'];
}

$employee_data = array();

function getCSV($file)
{
	$cols = array();
	$rows = array();
	
	$handle = fopen($file, 'r');
	$length = 1024;
	while (($cols = fgetcsv($handle, $length, ',')) !== FALSE) {
		foreach( $cols as $key => $val ) {
			$cols[$key] = trim( $cols[$key] );
			$tis = iconv('utf-8', 'tis-620', $cols[$key]) ;
			$cols[$key] = iconv('tis-620', 'utf-8', $tis) ;
			$cols[$key] = str_replace('""', '"', $cols[$key]);
		}
		$rows[] = $cols;
	}
	return $rows;
}


if (!function_exists('array_column')) {
    function array_column($input, $column_key, $index_key = null) {
        $arr = array_map(function($d) use ($column_key, $index_key) {
            if (!isset($d[$column_key])) {
                return null;
            }
            if ($index_key !== null) {
                return array($d[$index_key] => $d[$column_key]);
            }
            return $d[$column_key];
        }, $input);

        if ($index_key !== null) {
            $tmp = array();
            foreach ($arr as $ar) {
                $tmp[key($ar)] = current($ar);
            }
            $arr = $tmp;
        }
        return $arr;
    }
}



$user_data = array_column($userData, 'mr_user_username');
$employee_data = array_column($employee, 'mr_emp_code');
$file = $_FILES['file']; // File 
$commit_date = $_POST['commit_date'];

$days = explode('/', $commit_date); 
$cm_date = $days[0].'-'.$days[1].'-'.$days[2];


//echo '<pre>'.print_r( $position, true).'</pre>';
//exit();

$date_now  			= date('Y-m-d'); // Now Date
$results 			= array();
    $file_name 		= $_FILES['file']['name'];
	$tmp_name 		= $_FILES['file']['tmp_name'];
	$file_size 		= $_FILES['file']['size'];
	$err 			= $_FILES['file']['error'];
	$file_type 		= $_FILES['file']['type'];

$path = '../download/';
$file_names = explode('.', $file_name);
$surename = $file_names[count($file_names) - 1];
$new_name = 'TMBmailroom_data_emp_'.date('Ymdhis').'('.$date_now.').'.$surename;


if(!is_dir($path)) {
   @mkdir($path); // create path
   move_uploaded_file($tmp_name, $path.$new_name);
}else {
   move_uploaded_file($tmp_name, $path.$new_name);
}

$data = array();
$DATA_D = array();
$values = array();

$dir = 'resign/';
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
          
			if(trim($file) != '.' and trim($file) != '..' and trim($file) != '...' and trim($file) != ''){
				  echo "filename: .".$file."<br />";
				  if(file_exists($dir.$file)) {
					//if( $surename == 'csv' ){
								
							$path_file = $dir.$file;
							//$file_csv = file($path_file);
							$i = 0;
							$nData = 0;
							$text = getCSV($path_file);
							unset($text[0]);
							$index_value = 0;
							//echo '<pre>'.print_r( $DATA, true).'</pre>';
								//exit();
								$index_valueD = 0;
								$valueD = array();
								
								foreach($text as $valueD){
									$DATA_D 				= $valueD;
									
									$resign = $employeeDao->checkEmpResign( $DATA_D['1'] );
									
									echo '<pre>'.print_r( $DATA_D['1'], true).'</pre>';
									//exit();
									if ( $resign != "Resign" ) {
										$data_delete['mr_date_import'] 	= $date_import;
										$data_delete['mr_emp_email'] 	= "Resign";
										$data_delete['mr_emp_code'] 	= "Resign";
										$data_delete['old_emp_code'] 	= $DATA_D['1'];
										
										
										$emp_id_delete = $employeeDao->updateEmpByID($data_delete, $DATA_D['1']);
										// echo print_r($emp_id_delete,true) ;
										// exit;
										$userDao->Update_delete_User( $DATA_D['1'] );
										if($emp_id_delete!=0){
											$history_data['mr_emp_id'] 				= $emp_id_delete;
										}
											$history_data['action'] 				= $DATA_D['0']."::".$DATA_D['1'];
											$historyDao->save($history_data);
											$index_valueD++;
											$num++;
										
									}
									
								}
					//}
				}
			}

        }
        closedir($dh);
    }
}


?>
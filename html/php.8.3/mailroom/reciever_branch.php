<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Round.php';
require_once 'Dao/Send_work.php';
error_reporting(E_ALL & ~E_NOTICE);
/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$roundDao 		= new Dao_Round();
$send_workDao 		= new Dao_Send_work();


// $time_start = microtime(true);
// $startdate 	= Date("Y-m-d H:i:s", strtotime(date("Y-m-d")." -90 Day"));
// $end_time 	= Date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")." +10 minute "));
// $now 		= date("Y-m-d H:i:s");
// $filename 	= '../download/results.json';
// $sql_select = "
// 			SELECT 
// 				m.mr_work_main_id,				
// 				m.mr_status_id,			
// 				m.mr_type_work_id,
// 				sw.barcode,
// 				m.mr_work_barcode			
// 			FROM mr_work_main  m 
// 				Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
// 				where 
// 				m.mr_status_id in(7,8,9,11,13,14)
// 				and m.mr_type_work_id in(1,2,3)
// 				and m.sys_timestamp >= ?
// 				and m.mr_work_barcode is not null
// 				and m.mr_work_barcode != ''
// 				order by m.mr_work_main_id desc		
// 				limit 0,30000		
// ";

// //unset($_SESSION['reciever_branch']);
// $arrsection = array();
// if(!file_exists($filename)){
// 	$posts = array();
// 	$fp = fopen($filename, 'w');
// 	fwrite($fp, json_encode($posts, JSON_PRETTY_PRINT));   // here it will print the array pretty
// 	fclose($fp);
// }
// $string = file_get_contents($filename);
// $json_data = json_decode($string, true);
// $timeout = (isset($json_data["reciever_branch_time"]))?$json_data["reciever_branch_time"]:null;

// //$fp = fopen('results.json', 'w');
// if($timeout < $now or empty($json_data)){
// 	////echo "ไม่มี";
// 	$params = array();
// 	array_push($params,$startdate);
// 	$data = $send_workDao->select($sql_select,$params);
// 	foreach($data as $key=>$val){
// 		// if($val['barcode'] != ''){
// 		// 	$arrsection["reciever_branch"][$val['barcode']][$val['mr_work_barcode']] = $val;
// 		// }
// 		$arrsection["reciever_branch"][$val['mr_work_barcode']] = $val;
// 		$arrsection["reciever_branch_time"] = $end_time;
// 	}
// 	$fp = fopen($filename, 'w');
// 	fwrite($fp, json_encode($arrsection, JSON_PRETTY_PRINT));   // here it will print the array pretty
// 	fclose($fp);
// }

// $time_end = microtime(true);

// $time = $time_end - $time_start;

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();


$barcode_full 		= "TM".date("dmy");
$user_id			= $auth->getUser();
$round			= $roundDao->getRoundBranch();
$user_data 			= $userDao->getempByuserid($user_id);
// echo "<pre>".print_r($startdate,true)."</pre>";
// echo "<pre>".print_r($time,true)."</pre>";

// echo "<pre>".print_r($now,true)."</pre>";
// echo "<pre>".print_r($end_time,true)."</pre>";
// echo "<pre>".print_r($json_a,true)."</pre>";
$template = Pivot_Template::factory('mailroom/reciever_branch.tpl');
$template->display(array(
	// 'debug' 		=> print_r($path_pdf,true),
	//'userRoles' 	=> $userRoles,
	'select' 		=> 0,
	// 'success' 	=> $success,
	'barcode_full' 	=> $barcode_full,
	//'userRoles' 	=> $userRoles,
	'user_data'	 	=> $user_data,
	'round' 		=> $round,
	'today' 		=> date("Y-m-d"),
	//'users' 		=> $users,
	'role_id' 		=> $auth->getRole(),
	'roles' 		=> Dao_UserRole::getAllRoles()
));
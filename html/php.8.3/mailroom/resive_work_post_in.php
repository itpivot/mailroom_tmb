<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Round.php';
require_once 'Dao/Type_post.php';
require_once 'Dao/Cost.php';
require_once 'nocsrf.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//
$req = new Pivot_Request();
$userDao = new Dao_User();

$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$userRoleDao		= new Dao_UserRole();
$branchDao			= new Dao_Branch();
$roundDao			= new Dao_Round();
$send_workDao		= new Dao_Send_work();
$type_postDao		= new Dao_Type_post();
$costDao 			= new Dao_Cost();


$user_id			= $auth->getUser();
$user_data 			= $userDao->getempByuserid($user_id);
$user_data_show 	= $userDao->getEmpDataByuseridProfile($user_id);
$round 				= $roundDao->getRoundPost_in();
$type_post 			= $type_postDao->fetchAll();
$cost		 		= $costDao->fetchAll();
$department		 	= $departmentDao->fetchAll();
////$department = $departmentDao->fetchAll();
//$emp_data = $employeeDao->getEmpDataSelect();
//echo "<pre>".print_r($department,true)."</pre>";//mr_emp_code
$template = Pivot_Template::factory('mailroom/resive_work_post_in.tpl');
$template->display(array(
	//'debug' 				=> print_r($path_pdf,true),
	'userRoles' 			=> (isset($userRoles) ? $userRoles : null),
	'success' 				=> (isset($success) ? $success : null),
	'type_post' 			=> $type_post,
	'emp_data' 				=> (isset($emp_data) ? $emp_data : null),
	'users' 				=> (isset($users) ? $users : null),
	'user_data' 			=> $user_data,
	'round' 				=> $round,
	'cost' 				    => $cost,
	'department' 			=> $department,
	'user_data_show' 		=> $user_data_show,
	'select' 				=> '0',
	'today' 				=> date('Y-m-d'),
	'role_id' 				=> $auth->getRole(),
	'csrf_token' 			=>  NoCSRF::generate( 'csrf_token'),
	'roles' 				=> Dao_UserRole::getAllRoles(),
	'serverPath' 			=> $_CONFIG->site->serverPath
));
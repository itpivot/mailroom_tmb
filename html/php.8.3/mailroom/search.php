<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';
require_once 'Dao/Contact.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$statusDao = new Dao_Status();
$contactDao = new Dao_Contact();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();


$status_data = $statusDao->getStatusall();
$status_data2 = $statusDao->getStatusall3();
$sql = "
		SELECT 
			mr_contact_id,
			mr_contact_name, 
			remark, 
			acctive, 
			type,
			department_name  ,
			CASE
			WHEN type = 1 THEN 'รายชื่อติดต่อ'
			WHEN type = 2 THEN 'สาขาส่งบ่อย'
			ELSE 'อื่นๆ'
			END AS QuantityText
		FROM 
			mr_contact 
		WHERE acctive = 1 ";
$contactdata = $contactDao->select($sql);
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

//echo "<pre>".print_r($contactdata ,true)."</pre>";
$template = Pivot_Template::factory('mailroom/search.tpl');
$template->display(array(
	//'debug' => print_r($status_data2,true),
	'contactdata' => $contactdata,
	'status_data' => $status_data,
	'status_data2' => $status_data2,
	//'userRoles' => $userRoles,
	// 'success' => $success,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
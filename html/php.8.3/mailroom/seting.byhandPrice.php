<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Round.php';
require_once 'Dao/Type_post.php';
require_once 'nocsrf.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//
$req = new Pivot_Request();
$userDao = new Dao_User();

$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$userRoleDao		= new Dao_UserRole();
$branchDao			= new Dao_Branch();
$roundDao			= new Dao_Round();
$send_workDao		= new Dao_Send_work();
$type_postDao		= new Dao_Type_post();



$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);
$user_data_show = $userDao->getEmpDataByuseridProfile($user_id);

$type_postData = $type_postDao->fetchAll();


$template = Pivot_Template::factory('mailroom/seting.byhandPrice.tpl');
$template->display(array(
	//'debug' => print_r($contactdata,true),
	'userRoles' => (isset($userRoles) ? $userRoles : null),
	'success' => (isset($success) ? $success : null),
	'type_postData' => $type_postData,
	'user_data' => $user_data,
	'user_data_show' => $user_data_show,
	//'select' => '0',
	'today' => date('Y-m-d'),
	'role_id' => $auth->getRole(),
	'csrf_token' =>  NoCSRF::generate( 'csrf_token'),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
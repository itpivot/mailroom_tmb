<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Status.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_inout.php';
error_reporting(E_ALL & ~E_NOTICE);


$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRole_Dao		= new Dao_UserRole();
$work_main_Dao      = new Dao_Work_main();
$branchDao			= new Dao_Branch();
$floorDao 			= new Dao_Floor();
$statusDao          = new Dao_Status();
$logDao             = new Dao_Work_log();
$work_inoutDao 		= new Dao_Work_inout(); 

$user_id  = $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);
$work_id = $req->get('id');
$id = base64_decode(urldecode($work_id));
$data_search['mr_work_main_id'] = $id;
// $id = 9040;


$data           = $work_main_Dao->getWorkMainById($id);
// echo '<pre>'.print_r($data,true).'</pre>'; 
// exit;


$sqlconnf= "
SELECT 
	e.mr_emp_code,
	e.mr_emp_name,
	e.mr_emp_lastname
FROM mr_confirm_log con_l 
left join mr_emp e on(e.mr_emp_id = con_l.mr_emp_id) 
WHERE con_l.mr_work_main_id = ? ";



$sql_empre ="
SELECT
 	e.mr_emp_code,
	e.mr_emp_name,
	e.mr_emp_lastname,
	
	u.mr_user_id as mr_user_id2 
FROM mr_work_inout i
	left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
	Left join mr_user u on ( u.mr_user_id = i.mr_user_id )
	left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
WHERE
	m.mr_work_main_id = ? ";

	if($data['mr_type_work_id']==4 ){
		$sql_empsend ="
				SELECT 
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_branch_floor,
					u.mr_user_id as mr_user_id2,
					concat(mr_emp_tel,'||',mr_emp_mobile) as tell,
					b.mr_branch_code,
					b.mr_branch_name,
					d.mr_department_code,
					d.mr_department_name,
					f.name as floor_name,
					p.mr_position_code,
					p.mr_position_name,
					t.mr_type_work_name,
					u.mr_user_role_id,
					wp.mr_cus_name
				FROM mr_work_main m
				left join mr_work_byhand wbh on ( wbh.mr_work_main_id = m.mr_work_main_id )
			

				Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
				left join mr_emp e on ( e.mr_emp_id = wbh.mr_send_emp_id )
				Left join mr_user u on ( u.mr_emp_id = e.mr_emp_id )

				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id )
				left join mr_department d on ( d.mr_department_id = e.mr_department_id )
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id )
				left join mr_position p on ( p.mr_position_id = e.mr_position_id )
				left join mr_work_post wp on (m.mr_work_main_id = wp.mr_work_main_id)
				WHERE
				m.mr_work_main_id = ? 
		";
			
			
			
		$sql_emp_resive ="
				SELECT 
					m.mr_type_work_id,
					m.mr_topic,
					m.mr_work_remark,
					wbh.mr_cus_name as mr_emp_name,
					wbh.mr_cus_tel as tell,
					wbh.mr_address,
					t.mr_type_work_name,
					sts.mr_work_byhand_status_send_name,
					wbh.send_remark
				FROM mr_work_main m
				left join mr_work_byhand wbh on ( wbh.mr_work_main_id = m.mr_work_main_id )
				Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
				Left join mr_work_byhand_status_send sts on ( wbh.mr_work_byhand_status_send_id = sts.mr_work_byhand_status_send_id )

				WHERE
				m.mr_work_main_id = ? 
		";

	}else{

		$sql_empsend ="
				SELECT 
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_branch_floor,
					u.mr_user_id as mr_user_id2,
					concat(mr_emp_tel,'||',mr_emp_mobile) as tell,
					b.mr_branch_code,
					b.mr_branch_name,
					d.mr_department_code,
					d.mr_department_name,
					f.name as floor_name,
					p.mr_position_code,
					p.mr_position_name,
					t.mr_type_work_name,
					u.mr_user_role_id,
					wp.mr_cus_name
				FROM mr_work_main m
				Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
				Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
				left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id )
				left join mr_department d on ( d.mr_department_id = e.mr_department_id )
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id )
				left join mr_position p on ( p.mr_position_id = e.mr_position_id )
				left join mr_work_post wp on (m.mr_work_main_id = wp.mr_work_main_id)
				WHERE
				m.mr_work_main_id = ? 
		";
			
			
			
		$sql_emp_resive ="
				SELECT 
					i.mr_branch_floor,
					m.mr_type_work_id,
					m.mr_topic,
					m.quty,
					m.mr_work_remark,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					u.mr_user_id as mr_user_id2,
					concat(mr_emp_tel,'||',mr_emp_mobile) as tell,
					b.mr_branch_code,
					b.mr_branch_name,
					d.mr_department_code,
					d.mr_department_name,
					f.name as floor_name,
					p.mr_position_code,
					p.mr_position_name,
					t.mr_type_work_name,
					u.mr_user_role_id
				FROM mr_work_main m
				Left join mr_work_inout i on ( m.mr_work_main_id = i.mr_work_main_id )
				Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
				left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
				Left join mr_user u on ( u.mr_emp_id = e.mr_emp_id )
				left join mr_branch b on ( b.mr_branch_id = i.mr_branch_id )
				left join mr_department d on ( d.mr_department_id = e.mr_department_id )
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id )
				left join mr_position p on ( p.mr_position_id = e.mr_position_id )
				WHERE
				m.mr_work_main_id = ? 
		";
			
	}


$sqllog= "
SELECT 
    log.*,
	e.mr_emp_code,
	e.mr_emp_name,
	e.mr_emp_lastname,
	r.mr_round_name,
	s.mr_status_name
	FROM mr_work_log log 
	Left join mr_round r on ( r.mr_round_id = log.mr_round_id )
	Left join mr_user u on ( u.mr_user_id = log.mr_user_id )
	Left join mr_status s on ( s.mr_status_id = log.mr_status_id )
	left join mr_emp e on(e.mr_emp_id = u.mr_emp_id) 
	WHERE log.mr_work_main_id = ? 
	group by log.mr_work_log_id
";
$params = array();
array_push($params, (int)$id);

	$resive_data 		= $work_main_Dao ->select_work_main($sql_emp_resive,$params);
	$send_data 			= $work_main_Dao ->select_work_main($sql_empsend,$params);



$logdata 			= $work_main_Dao ->select_work_main($sqllog,$params);
$log_conf 			= $work_main_Dao ->select_work_main($sqlconnf,$params);

foreach($logdata  as  $key => $val){
	if($val['remark']!=''){
		$logdata[$key]['remark'] = str_replace("และเตรียมพิมพ์ใบนำส่ง ฺ(BOC)","",$val['remark']);
	}
}
if(empty($log_conf)){
	$log_conf 		= $work_main_Dao ->select_work_main($sql_empre,$params);
}


// echo '<pre>'.print_r($resive_data,true).'</pre>'; 
// exit;


//$branch_data    = $branchDao->getBranch();
//$floor_data     = $floorDao->fetchAll();
$status_data    = $statusDao->getStatusBranch();
// $log_data       = $logDao->getWorkLogById($id);
$user_data_show = $userDao->getEmpDataByuseridProfileBranch($user_id);
//$report            = $work_inoutDao->searchBranch_manege( $data_search );


$log            = $logDao->getWorkLogById($id);
$log_data  = array();
foreach($log  as $key => $l){
    $st = $l['mr_status_id'];
    switch ($st) {
        case 1:  $l['mr_status_id']     = 1 ;   break;
        case 2:  $l['mr_status_id']     = 2 ;   break;
        case 3:  $l['mr_status_id']     = 3 ;   break;
        case 4:  $l['mr_status_id']     = 4 ;   break;
        case 5:  $l['mr_status_id']     = 5 ;   break;
        case 6:  $l['mr_status_id']     = 6 ;   break;
        case 7:  $l['mr_status_id']     = 7 ;   break;
        case 8:  $l['mr_status_id']     = 8 ;   break;
        case 9:  $l['mr_status_id']     = 9 ;   break;
        case 10: $l['mr_status_id']     = 3 ;   break;
        case 11: $l['mr_status_id']     = 11 ;  break;
        case 12: $l['mr_status_id']     = 5 ;   break;
        case 13: $l['mr_status_id']     = 13 ;  break;
        case 14: $l['mr_status_id']     = 14 ;  break;
        case 15: $l['mr_status_id']     = 15 ;  break;
        default:
        $l['mr_status_id']     = 15 ;
    }

    $log_data[$key] = $l; 
}




$log_status = array();
$new_status_arr = array();
$run_status_txt = '';
$run_status_arr = array();
foreach($status_data as $key => $val) {
	$new_status_arr[$val['mr_status_id']] = $val;
}




if($data['mr_type_work_id']==1){//  สนญ - สนญ
    $run_status_txt = '1,2,3,4,5';
    $run_status_arr = explode(',',$run_status_txt);
}elseif($data['mr_type_work_id']== 4){ //by hand
	$run_status_txt = '1,2,3,4,5';
    $run_status_arr = explode(',',$run_status_txt);
}elseif($data['mr_type_work_id']== 2 and $data['mr_user_role_id']== 2 ){ // สนญ - ส่งสาขา
//1,2,3,10,11,13,5,12
$run_status_txt = '1,2,3,11,13,5';
$run_status_arr = explode(',',$run_status_txt);
}elseif($data['mr_type_work_id']== 3 and $data['mr_user_role_id']==5){ // สาขา - สนญ
//7,8,9,14,10,3,4,'5,12'
$run_status_txt = '7,8,9,14,3,4,5';
$run_status_arr = explode(',',$run_status_txt);
}else{//สาขา - สาขา
//7,8,9,14,10,3,11,13,5,12
$run_status_txt = '7,8,9,14,3,11,13,5';
$run_status_arr = explode(',',$run_status_txt);
}

$date_start=0;
$date_end='';
$end_step = '';
$is12 = 0;
$signatures = '';
foreach($run_status_arr as $key => $val2) {
   $val=$new_status_arr[$val2];
   foreach($log_data as $kk => $vv) {
		if($vv['mr_status_id'] == 12){
			$is12 ++;
		}
   }
    foreach($log_data as $k => $v) {
		if($data['mr_type_work_id']==4){
			if($v['mr_status_id']==5){
				$date		= date("Y-m",strtotime($v['sys_timestamp'])); 
				$signatures = '../signatures/'.$date.'/'.$id.'/'.$id.'.jpg';
				//echo $signatures;
				//exit;
			}
		}
		if($data['mr_type_work_id']==2){
				$date		= date("Y-m",strtotime($v['sys_timestamp'])); 
				$signatures_sendbranch = '../signatures_sendbranch/'.$date.'/'.$id.'/'.$id.'.jpg';
				//echo $signatures;
				//exit;
		}
        //$log_status[$key]['status_id'] = $val['mr_status_id'];
        //$log_status[$key]['name'] = $val['mr_status_name'];
		$date_end = $key;
        if($val['mr_status_id'] == $v['mr_status_id']) {
            $log_status[$key]['status_id'] = $val['mr_status_id'];
            $log_status[$key]['name'] = $val['mr_status_name'];
            $log_status[$key]['active'] = 'is-complete';
            $log_status[$key]['emp_code'] = $v['mr_emp_code'];
            $log_status[$key]['sys_timestamp'] = $v['sys_timestamp'];
            $log_status[$key]['icon'] = '<i class="material-icons">done</i>';
			$log_status[$key]['key_1'] = "";
			$log_status[$key]['key_2'] = "";
			
			
			if($key!=0){
				$dt1 = $log_status[$date_start]['sys_timestamp'];
				$dt2 = $log_status[$date_end]['sys_timestamp'];
				$log_status[$key]['key_1'] = $date_start; 
				$log_status[$key]['key_2'] =  $date_end;
				$arr = json_decode(json_encode(datetimeDiff($dt1, $dt2)), True);
				$d=($arr['day']!=0)?$arr['day'].' วัน  ':''; 
				$h=($arr['hour']!=0)?$arr['hour'].' ชั่วโมง  ':''; 
				$i=($arr['min']!=0)?$arr['min'].' นาที ':''; 
				$s=($arr['sec']!=0)?$arr['sec'].' วินาที  ':''; 
				$log_status[$key]['date'] =  $d.$h.$i.$s;
				
				$end_step =	$key+1;
			}
			$date_start=$date_end;	
			if($val['mr_status_id'] == $data['mr_status_id'] and $is12 == 0 and $data['mr_status_id']= 12){
				//$log_status[$key]['date'] = '';
				//$log_status[$key]['emp_code'] = '';
			}
			break;
        } else{
			$log_status[$key]['status_id'] = $val['mr_status_id'];
            $log_status[$key]['name'] = $val['mr_status_name'];
            $log_status[$key]['active'] = '';
            $log_status[$key]['emp_code'] = '';
            $log_status[$key]['sys_timestamp'] = $v['sys_timestamp'];
            $log_status[$key]['icon'] = '<i class="material-icons">done</i>';
            $log_status[$key]['icon'] = '';
            $log_status[$key]['key'] = '';
			//break;
		} 
    }
}

//echo '<pre>'.print_r($data,true).'</pre>';
//$end_step;
$received = "real";
if(!empty($data['real_receive_id'])) {
	if($data['receive_id'] == $data['real_receive_id']) {
		$received = "real";
	} else {
		$received = "replace";
	}
}

//echo '<pre>'.print_r($data['mr_user_role_id'],true).'</pre>';
//echo '<pre>'.print_r($data['mr_type_work_id'],true).'</pre>';
//echo '<pre>'.print_r($log_data,true).'</pre>';
//echo '<pre>'.print_r($status_data,true).'</pre>';



$dt1="2019-01-01 08:00:35";

$dt2="2019-01-02 08:00:45";



function datetimeDiff($dt1, $dt2){
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);

        $dtd = new stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2-$t1);
        $dtd->total_min = floor($dtd->total_sec/60);
        $dtd->total_hour = floor($dtd->total_min/60);
        $dtd->total_day = floor($dtd->total_hour/24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour -($dtd->total_day*24);
        $dtd->min = $dtd->total_min -($dtd->total_hour*60);
        $dtd->sec = $dtd->total_sec -($dtd->total_min*60);
        return $dtd;
    }
    
    

$report_custom = array();



//echo '<pre>'.print_r($report_custom,true).'</pre>'; 
// echo '<pre>'.print_r($signatures_sendbranch,true).'</pre>'; 
// exit;

$template = Pivot_Template::factory('mailroom/work_info.tpl');
$template->display(array(
    'role_id' 			=> $auth->getRole(),
    'roles' 			=> Dao_UserRole::getAllRoles(),
    'send_data' 		=> $send_data,
    'resive_data' 		=> $resive_data,
    'user_data' 		=> $user_data,
    'user_data_show' 	=> $user_data_show,
    'serverPath'		=> $_CONFIG->site->serverPath,
    'log_conf' 			=> $log_conf,
    'log' 				=> $logdata,
    'end_step' 			=> $end_step,
    'data' 				=> $data,
    'status' 			=> $log_status,
    'wid' 				=> $id,
    'signatures' 				=> $signatures,
	'signatures_sendbranch' => $signatures_sendbranch,
    'received' 			=> $received
));
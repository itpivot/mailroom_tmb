<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';


$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();

$pass_emp		=  $req->get('pass_emp');

$emp_data = $employeeDao->getEmp_code( $pass_emp );

//echo "<pre>".print_r($emp_data,true)."</pre>";
//exit();

echo json_encode($emp_data);
?>

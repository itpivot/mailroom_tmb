<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';


$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();

$name_receiver_select		=  $req->get('name_receiver_select');

$emp_data = $employeeDao->getEmpByID( $name_receiver_select );

//echo "<pre>".print_r($emp_data,true)."</pre>";
//exit();

echo json_encode($emp_data);
?>

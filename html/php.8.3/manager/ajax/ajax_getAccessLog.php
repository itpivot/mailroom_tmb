<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Access_status_log.php';



$req = new Pivot_Request();
$access_status_log_Dao = new Dao_Access_status_log();

function PrettyDate($date) {
    if($date) {
        $old = explode('/', $date);
        return $old[2] . '-' . $old[1] . '-' . $old[0];
    } else {
        return null;
    }
   
}

$data = array();
$data['start_date'] = PrettyDate($req->get('start'));
$data['end_date'] = PrettyDate($req->get('end'));
$data['activity'] = $req->get('activity');
$data['status'] = $req->get('status');



$acc = $access_status_log_Dao->fetchAccess_logs($data);

usort($acc, function ($x, $y) {
 	return $y['sys_timestamp'] > $x['sys_timestamp'];
 });


foreach($acc as $k => $v) {
    $res[$k][0] = $k+1;
    $res[$k][1] = strtoupper($v['mr_user_username']);
    $res[$k][2] = $v['mr_emp_name']." ".$v['mr_emp_lastname'];
    $res[$k][3] = $v['terminal'];
    $res[$k][4] = strtoupper($v['activity']);
    if($v['status'] == 'Success') {
        $sts = "<span class='badge badge-success'>".strtoupper($v['status'])."</span>";
    } else {
        $sts = "<span class='badge badge-danger'>" . strtoupper($v['status']) . "</span>";
    }
    $res[$k][5] = $sts;
    $res[$k][6] = date('d/m/y H:i', strtotime($v['sys_timestamp']));
    $res[$k][7] = $v['description'];
} 

echo json_encode($res);
?>

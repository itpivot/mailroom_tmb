<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';


$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
                                                                            
$data_search['barcode']  		=  $req->get('barcode');                           
$data_search['receiver']  		=  $req->get('receiver');                         
$data_search['sender']  		=  $req->get('sender');                             
$data_search['start_date']  	=  $req->get('start_date');                     
$data_search['end_date']  		=  $req->get('end_date');                                 
$data_search['status']  		=  $req->get('status');      
                           
//echo "<pre>".print_r($data_search,true)."</pre>"; 
//exit();		

$report = $work_inoutDao->searchMailroom( $data_search );

foreach( $report as $num => $value ){
	$report[$num]['no'] = $num +1;
	$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
	$report[$num]['name_send'] = $value['mr_emp_name']." ".$value['mr_emp_lastname'];
	
}

$arr['data'] = $report;

echo json_encode($arr);
 ?>

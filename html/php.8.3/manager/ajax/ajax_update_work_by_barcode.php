<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 	= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();


$users = $auth->getUser();
$barcode = $req->get('barcode');
$full_barcode = $req->get('full_barcode');
$status = 3;


if( $full_barcode == "true" ){
	$barcode_full = $barcode;
}else if( $full_barcode == "false" ){
	$barcode_full 	= "TM".date("dmy").$barcode;
}


//echo "<pre>".print_r($barcode_full,true)."</pre>";
//exit();



$work_data = $work_mainDao->getIDbyBarcode( $barcode_full );

$save_work_main['mr_status_id'] = $status;


$save_work_log['mr_status_id']		= $status;
$save_work_log['mr_user_id'] 		= $users;
$save_work_log['mr_work_main_id'] 	= $work_data['mr_work_main_id'];



if ( $work_data['mr_work_main_id'] == "" || $work_data['mr_status_id'] == 4 || $work_data['mr_status_id'] == 5 || $work_data['mr_status_id'] == 3 ){
	$result = 0;
}else{
	$result = 1;
	$work_mainDao->save($save_work_main,$work_data['mr_work_main_id']);
	$work_logDao->save($save_work_log);
	
}


echo $result;
?>

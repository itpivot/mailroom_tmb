<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();

$user_id	= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$today_year	= date('Y-m-d');
//$today_year	= "2016";

$date	=	$work_mainDao->getReportSendDaily();

$view_month =	$_CONFIG->view->toArray();


$months		=	$view_month['months'];


foreach($date as $key => $val)
{

    $old_date = explode('-', $val['date']);
    $date[$key]['date_show'] = $old_date[2]." ".$months[$old_date[1]]." ".$old_date[0];
	
}


//echo "<pre>".print_r($view_month['months'],true)."</pre>";
//echo "<pre>".print_r($month,true)."</pre>";
//exit();	



$template = Pivot_Template::factory('manager/report_send_daily.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'userRoles' => $userRoles,
	'date' => $date,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
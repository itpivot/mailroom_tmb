<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Branch.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRoleDao 			= new Dao_UserRole();
$work_main_Dao 			= new Dao_Work_main();
$work_inout_Dao 		= new Dao_Work_inout();
$employee_Dao 			= new Dao_Employee();
$work_order_logs_Dao 	= new Dao_Work_log();
$branch_Dao 			= new Dao_Branch();

$type = $req->get('type');
$branch_id = $req->get('branch_id');

$userId = $auth->getUser();

$empUser = $userDao->getempByuserid($userId);

$empData = $employee_Dao->getEmpDataById($empUser['mr_emp_id']);



if($branch_id == ''){
$mr_hub_id = (isset($empData['mr_hub_id']))?$empData['mr_hub_id']:0;
$branch = $branch_Dao->getBranchCodeByhub($mr_hub_id);
    if(!empty($branch )){
        $branch_id = $branch['mr_branch_id'];
    }
} 

//$data_receive = $work_main_Dao->getReceiveBranch($status, $work_type, $empData['mr_hub_id']);

switch($type) {
    case 'get':
        $work_type = '2, 3';
        $status = '11';
      //  echo print_r($branch_id,true);
      //  exit;
        $data_hub = $work_inout_Dao->getReceiveBranchHub($branch_id, $work_type, $status);
 $card['counter2'] = $branch_id;
 
 
        if(count($data_hub) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_hub as $key => $val) {
                
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" class="check_all" data-value="'.$val['mr_work_main_id'].'" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้รับ : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    case 'update':
        $work_main_id = $req->get('wId');
        $action = $req->get('action');
        $card['action'] 		= $action;
        $card['work_main_id'] 	= $work_main_id;
        $card['userId'] 	= $userId;
        if(intval($action) == 1) {
            // received
            $main['mr_status_id'] = 13; // แมสเข้ารับเอกสาร

            $inout['messenger_user_id'] = $userId;
            $inout['mr_status_receive'] = intval($action);
			$card['sss'] 	= "เข้า";
            $logs['sys_timestamp'] = date('Y-m-d H:i:s');
            $logs['mr_work_main_id'] = $work_main_id;
            $logs['mr_user_id'] = $userId;
            $logs['mr_status_id'] = 13;
        } else {
            // not found
			$card['sss'] 	= "ไม่เข้า";
            $logs['sys_timestamp'] = date('Y-m-d H:i:s');
            $logs['mr_work_main_id'] = $work_main_id;
            $logs['mr_user_id'] = $userId;
            $logs['mr_status_id'] = 13;
            
            $main['messenger_user_id'] = $userId;
            $inout['mr_status_receive'] = intval($action);
        }


        if(isset($main)){
            $m_Id = $work_main_Dao->save($main, $work_main_id);
        } 
        $logs_Id = $work_order_logs_Dao->save($logs);
        $inout_Id = $work_inout_Dao->updateInOutWithMainId($inout, $work_main_id);

        $work_type = '2, 3';
        $status = 11;
        $data_hub = $work_inout_Dao->getReceiveBranchHub($branch_id, $work_type, $status);

        if(count($data_hub) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_hub as $key => $val) {
                
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" class="check_all" data-value="'.$val['mr_work_main_id'].'" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้รับ : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    case 'update_hub_all':
        $work_mainAll = json_decode($req->get('wId'), true);
        $action = $req->get('action');

         if(intval($action) == 1) {
            foreach($work_mainAll as $k => $v) {
                $main_all[$k]['mr_status_id'] = 13;

                $inout_all[$k]['messenger_user_id'] = $userId;
                $inout_all[$k]['mr_status_receive'] = intval($action);

                $log_all[$k]['sys_timestamp'] = date('Y-m-d H:i:s');
                $log_all[$k]['mr_work_main_id'] = $v;
                $log_all[$k]['mr_user_id'] = $userId;
                $log_all[$k]['mr_status_id'] = 13;

                $m_multi[] = $work_main_Dao->save($main_all[$k], $v);
                $inout_multi[] = $work_inout_Dao->updateInOutWithMainId($inout_all[$k], $v);
                $logs_multi[] = $logs_Id = $work_order_logs_Dao->save($log_all[$k]);
            }
        } else {
            foreach($work_mainAll as $k => $v) {
               
                $inout_all[$k]['mr_status_receive'] = intval($action);
                $inout_multi[] = $work_inout_Dao->updateInOutWithMainId($inout_all[$k], $v);
            }
        }

        $work_type = '2, 3';
        $status = 11;
        $data_hub = $work_inout_Dao->getReceiveBranchHub($branch_id, $work_type, $status);

        if(count($data_hub) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_hub as $key => $val) {
                
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" class="check_all" data-value="'.$val['mr_work_main_id'].'" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้รับ : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    case 'send':
        $work_main_id = $req->get('wId');
        $action = $req->get('action');
        $str = '';
        $work_type = '2, 3';
        $status = '13, 15';
		$card['st'] = $status;
		$card['work_type'] = $work_type;
		$card['branch_id'] = $branch_id;
		$card['status'] = $status;
		$card['userId'] = $userId;
        $data_sendHub = $work_inout_Dao->getSendBranch($branch_id, $work_type, $status, $userId);
        //$data_sendHub = $work_inout_Dao->getSendBranch($branch_id, $work_type, $status, null);

        if(count($data_sendHub) > 0) {
            $counter = 0;
            foreach($data_sendHub as $k => $val) {
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_id'] == 15) {
                     $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" class="check_all" data-value="'.$val['mr_work_main_id'].'" id="check_'.$k.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้รับ : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="confirmCancel('.$val['mr_work_main_id'].');">ไม่พบผู้รับ</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm">';
                                $str .= '<button type="button" class="btn btn-success right mx-2" onclick="showdatatosend('.$val['mr_work_main_id'].');">ส่งเอกสาร</button>'; //onclick="confirmApprove('.$val['mr_work_main_id'].');"
                            $str .= '</div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        
        break;
    case 'update_send':
        $work_main_id = $req->get('wId');
        $action = $req->get('action');

        if(intval($action) == 1) {
            // received
            $main['mr_status_id'] = 14; // แมสเข้ารับเอกสาร
            $main['mr_work_date_success'] = date('Y-m-d');
            
            $inout['mr_status_send'] = intval($action);
            $inout['messenger_user_id'] = $userId;
            
            $logs['sys_timestamp'] = date('Y-m-d H:i:s');
            $logs['mr_work_main_id'] = $work_main_id;
            $logs['mr_user_id'] = $userId;
            $logs['mr_status_id'] = 14;
        } else {
            // not found
            $main['mr_status_id'] = 6; 

            $inout['mr_status_send'] = intval($action);
            $inout['messenger_user_id'] = $userId;

            $logs['sys_timestamp'] = date('Y-m-d H:i:s');
            $logs['mr_work_main_id'] = $work_main_id;
            $logs['mr_user_id'] = $userId;
            $logs['mr_status_id'] = 6;
        }

        $m_Id = $work_main_Dao->save($main, $work_main_id);
        $inout_Id = $work_inout_Dao->updateInOutWithMainId($inout, $work_main_id);

        if(isset($logs)) {
            $logs_Id = $work_order_logs_Dao->save($logs);
        }
        
        $work_type = '2, 3';
        $status = '13, 15';

        $data_sendHub = $work_inout_Dao->getSendBranch($branch_id, $work_type, $status, $userId);

        if(count($data_sendHub) > 0) {
            foreach($data_sendHub as $k => $val) {
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_id'] == 15) {
                     $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" class="check_all" data-value="'.$val['mr_work_main_id'].'" id="check_'.$k.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้รับ : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="confirmCancel('.$val['mr_work_main_id'].');">ไม่พบผู้รับ</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm">';
                                $str .= '<button type="button" class="btn btn-success right mx-2" onclick="confirmApprove('.$val['mr_work_main_id'].');">ส่งเอกสาร</button>';
                            $str .= '</div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        
        break;
    case 'cancel':
        $wId = $req->get('wId');

        $main['mr_status_id'] = 15;
        $work_main_Dao->save($main, $wId);

        $inout['mr_status_send'] = 2;
        $work_inout_Dao->updateInOutWithMainId($inout, $wId);

        $logs['sys_timestamp'] = date('Y-m-d H:i:s');
        $logs['mr_work_main_id'] = $val;
        $logs['mr_user_id'] = $userId;
        $logs['mr_status_id'] = 14;
        $resp = $work_order_logs_Dao->save($logs);

        $work_type = '2, 3';
        $status = '13, 15';

        $data_sendHub = $work_inout_Dao->getSendBranch($branch_id, $work_type, $status, $userId);

        if(count($data_sendHub) > 0) {
            foreach($data_sendHub as $k => $val) {
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_id'] == 15) {
                     $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" class="check_all" data-value="'.$val['mr_work_main_id'].'" id="check_'.$k.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้รับ : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="confirmCancel('.$val['mr_work_main_id'].');">ไม่พบผู้รับ</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm">';
                                $str .= '<button type="button" class="btn btn-success right mx-2" onclick="confirmApprove('.$val['mr_work_main_id'].');">ส่งเอกสาร</button>';
                            $str .= '</div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    case 'cancel_all':
        $wId_all = json_decode($req->get('wId'), true);


        foreach($wId_all as $key => $vals) {
            $main_all['mr_status_id'] = 15;
            $work_main_Dao->save($main_all, $vals);

            $inout_all['mr_status_send'] = 2;
            $work_inout_Dao->updateInOutWithMainId($inout_all, $vals);

            $logs['sys_timestamp'] = date('Y-m-d H:i:s');
            $logs['mr_work_main_id'] = $vals;
            $logs['mr_user_id'] = $userId;
            $logs['mr_status_id'] = 14;
            $resp = $work_order_logs_Dao->save($logs);
        }

        $work_type = '2, 3';
        $status = '13, 15';

        $data_sendHub = $work_inout_Dao->getSendBranch($branch_id, $work_type, $status, $userId);

        if(count($data_sendHub) > 0) {
            foreach($data_sendHub as $k => $val) {
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_id'] == 15) {
                     $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" class="check_all" data-value="'.$val['mr_work_main_id'].'" id="check_'.$k.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้รับ : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="confirmCancel('.$val['mr_work_main_id'].');">ไม่พบผู้รับ</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm">';
                                $str .= '<button type="button" class="btn btn-success right mx-2" onclick="confirmApprove('.$val['mr_work_main_id'].');">ส่งเอกสาร</button>';
                            $str .= '</div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
        
}
?>
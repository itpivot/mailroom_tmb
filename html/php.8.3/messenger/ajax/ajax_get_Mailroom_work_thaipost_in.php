<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();

$user_id = $auth->getUser();

$txt 	= $req->get('txt');
$date_s = $req->get('date_s');
$date_e = $req->get('date_e');
$date   = array();
if($date_s!=''){
	$date['date_s'] = $date_s;
}if($date_e!=''){
	$date['date_e'] = $date_e;
}

$log_main_id = $work_inout_Dao->getMainID_resiveMailroomThaipost_IN($date);

// echo "<pre>".print_r($log_main_id,true)."</pre>"; 
// exit();

$main_id  = '0';
if(!empty($log_main_id)){
	$main_id = $log_main_id['id'];
}
// echo $main_id;
// exit;
$sendData = $work_inout_Dao->getresiveMailroom_post_in(intval($user_id),$txt,$date,$main_id);

foreach ($sendData as $key => $value) {
	$sendData[$key]['mr_work_date_sent'] = $work_inout_Dao->setDateFormat($value['mr_work_date_sent'], 2) ;

	// $sendData[$key]['diff_time'] = $work_inout_Dao->spliceDateTime($value['sys_timestamp']) ;
}

// echo "<pre>".print_r($sendData,true)."</pre>"; 
// exit();

// echo "The time is " . date("h:i:sa");


echo json_encode($sendData);
?>
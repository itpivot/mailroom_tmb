<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Confirm_Log.php';
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();
$confirm_log_Dao = new Dao_Confirm_Log();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$user_id = $auth->getUser();

$filename = $req->get('filename');
$main_id = $req->get('main_id');
$type = $req->get('type');

if($type == "saveone"){
    try{
    $main['mr_status_id'] = 12;
    $main['mr_work_date_success'] = date('Y-m-d');
    $work_main_Dao->save($main, $main_id);

    $inout['mr_status_send'] = 1;
    $work_inout_Dao->updateInOutWithMainId($inout, $main_id);

    // echo '<pre>'.print_r($resp,true).'</pre>';
    // exit;
    
    // บันทึกรูป 
    $signaturePath = '../../signatures_sendbranch';
    if(!is_dir($signaturePath)) {
        mkdir($signaturePath , 0777, true);
    }
    
    $signaturePath = '../../signatures_sendbranch/'.date("Y-m");
    if(!is_dir($signaturePath)) {
        mkdir($signaturePath , 0777, true);
    }
    
    $img = base64_decode($filename);
    // echo '<pre>'.print_r($img,true).'</pre>';
    // exit;
    $filename = "{$signaturePath}/{$main_id}/{$main_id}.jpg";
    if(!is_dir($signaturePath)) {
        mkdir($signaturePath , 0777, true);

    }

    if(!is_dir("{$signaturePath}/{$main_id}/")) {
        mkdir("{$signaturePath}/{$main_id}/");
    }

    if($filename != ""){
        file_put_contents($filename, $img);
    }

    $logs['sys_timestamp'] = date('Y-m-d H:i:s');
    $logs['mr_work_main_id'] = $main_id;
    $logs['mr_user_id'] = $user_id;
    $logs['mr_status_id'] = 12;
    $logs['remark'] = "img|".$filename;
    $resp = $work_log_Dao->save($logs);
    
    $getEmpId = $userDao->getEmpDataByuserid($user_id);
    if($resp != "") {
        $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
        $cfLogs['mr_work_main_id'] = $main_id;
        $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
        $cfLogs['mr_status'] = 'Success';
        $cfLogs['descriptions'] = "Receive Success";
        
        $success = $confirm_log_Dao->save($cfLogs);

    
    } else {
        $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
        $cfLogs['mr_work_main_id'] = $main_id;
        $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
        $cfLogs['mr_status'] = 'Failed';
        $cfLogs['descriptions'] = "Receive Failed";
        
        $failed = $confirm_log_Dao->save($cfLogs);
        
    }

    if(isset($success)) {
        $send_data = 'success';
    } else {
        $send_data = 'failed';
    }
    echo json_encode(array(
        'status' => 'success',
        'image' => str_replace('../../../', '../../', $filename),
        'message' => 'upload signature succss'
    ));
    }catch(Exception $e) {
        echo json_encode(array(
            'status' => 500,
            'error' => $e->getMessage()
        ));
    }
    
}else if($type == "saveall"){
    try{
        if (is_array($main_id)) {
            foreach($main_id as $k => $v) {
            $main['mr_status_id'] = 12;
            $main['mr_work_date_success'] = date('Y-m-d');
            $work_main_Dao->save($main, $v);
            
            $inout['mr_status_send'] = 1;
            $work_inout_Dao->updateInOutWithMainId($inout, $v);
            
            // echo '<pre>'.print_r($resp,true).'</pre>';
            // exit;
            
            // บันทึกรูป 
            $basePath = '../../signatures_sendbranch/' . date("Y-m");
            if (!is_dir($basePath)) {
                mkdir($basePath, 0777, true);
            }

            $userPath = $basePath . '/' . $v;
            if (!is_dir($userPath)) {
                mkdir($userPath, 0777, true);
            }

            $img = base64_decode($filename);
            $filePath = $userPath . '/' . $v . '.jpg';

            if ($img === false) {
                throw new Exception('Base64 decode failed');
            }

            if (file_put_contents($filePath, $img) === false) {
                throw new Exception('Failed to write file: ' . $filePath);
            }
        
            $logs['sys_timestamp'] = date('Y-m-d H:i:s');
            $logs['mr_work_main_id'] = $v;
            $logs['mr_user_id'] = $user_id;
            $logs['mr_status_id'] = 12;
            $logs['remark'] = "img|".$filePath;
            $resp = $work_log_Dao->save($logs);
        
            $getEmpId = $userDao->getEmpDataByuserid($user_id);
            if($resp != "") {
                $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
                $cfLogs['mr_work_main_id'] = $v;
                $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
                $cfLogs['mr_status'] = 'Success';
                $cfLogs['descriptions'] = "Receive Success";
            
                $success = $confirm_log_Dao->save($cfLogs);
            
            
            } else {
                $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
                $cfLogs['mr_work_main_id'] = $main_id;
                $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
                $cfLogs['mr_status'] = 'Failed';
                $cfLogs['descriptions'] = "Receive Failed";
            
                $failed = $confirm_log_Dao->save($cfLogs);
            
            }
        
                if(isset($success)) {
                    $send_data = 'success';
                } else {
                    $send_data = 'failed';
                }
            }
            echo json_encode(array(
                'status' => "success",
                'image' => str_replace('../../../', '../../', $filename),
                'message' => 'upload signature succss'
            ));
        }else{
            echo json_encode(array(
                'status' => "noarray",
                'message' => "ไม่ใช่ array",
            ));
        }
    }catch(Exception $e) {
        echo json_encode(array(
            'status' => 500,
            'error' => $e->getMessage()
        ));
    }
}
?>
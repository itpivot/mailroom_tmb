<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();


$data_uri = $req->get('sign');
$send_id = $req->get('id');


$data['mr_status_id'] = 5;
$data['mr_work_date_success'] = date('Y-m-d');
$w_main = $work_main_Dao->getBarcodeByid(intval($send_id));
$barcode = $w_main['mr_work_barcode'];


$data_inout = $work_inout_Dao->getWorkByMainID($send_id);
$data_save_inout['mr_status_send'] = 3;
$work_inout_Dao->save($data_save_inout,$data_inout);



$path = '../../signature/';


if(!is_dir($path)) {
    @mkdir($path);
   
} 

function saveImage($data_uri, $path, $barcode) {
	$uri = explode(',',$data_uri);
    $encoded_image = $uri[1];
    $decoded_image = base64_decode($encoded_image, true);
    file_put_contents($path.$barcode.".png", $decoded_image);
    if(file_exists($path.$barcode.".png")) {
        return true;
    } else {
        return false;
    }
}

$chk = saveImage($data_uri, $path, $barcode);

if($chk  == true) {
    $result = $work_main_Dao->save($data, intval($send_id));
    if($result != ""){
        $logs['sys_timestamp'] = date('Y-m-d H:i:s');
        $logs['mr_work_main_id'] = $result;
        $logs['mr_user_id'] = $user_id;
        $logs['mr_status_id'] = 5;
        $resp = $work_log_Dao->save($logs);
    }
}



// echo $res;



if($resp != "") {
    echo "success";
} else {
    echo "false";
}

// echo json_encode($sendData);
?>
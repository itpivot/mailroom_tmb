<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Confirm_Log.php';


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();
$confirm_log_Dao = new Dao_Confirm_Log();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();

$type = $req->get('type');
$action = $req->get('action');
$wId = $req->get('wId');
$main_id = $req->get('main_id');

$cfLogs = array();

switch($type) {
	case 'all':
		$type = 'check_user';

		break;
	case 'check_user':
		$wId_arr = $req->get('arrId');
		$username = $req->get('username');
		$password =  $auth->encryptAES($req->get('password'));
		
		$empData = $userDao->getempByEmpid( $username );

		if($empData['mr_emp_code'] == $username && $empData['mr_user_password'] == $password) {
			foreach($wId_arr as $key => $val) {
				$insert['mr_user_id'] = $empData['mr_user_id'];
				if($val  != ''){
					$work_inout_Dao->updateInOutWithMainId($insert, $val);
				}
			}			
			$result = 1;
		} else {
			if($empData['mr_emp_id'] != "" || $empData['mr_emp_id'] != null) {
				foreach($wId_arr as $key => $val) { 
					$cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
					$cfLogs['mr_work_main_id'] = intval($val);
					$cfLogs['mr_emp_id'] = $empData['mr_emp_id'];
					$cfLogs['mr_status'] = "Failed";
					$cfLogs['descriptions'] = "Username or Password Invalid";

					$cf[] = $confirm_log_Dao->save($cfLogs);
				}
				if(count($cf) > 0) {	
					$result = 0;
				}	
			} else {
				$result = 0;
			}	
		}
		
		echo $result;
		exit();

		break;
	case 'one':
		$type = 'check_user_one';
		break;
	case 'check_user_one':
		$workId = $req->get('main_id');
		$username = $req->get('username');
		$password =  $auth->encryptAES($req->get('password'));

		$empData = $userDao->getempByEmpid( $username );

		if($empData['mr_emp_code'] == $username && $empData['mr_user_password'] == $password) {
			$insert['mr_user_id'] = $empData['mr_user_id'];
			$work_inout_Dao->updateInOutWithMainId($insert, $workId);			
			$result = 1;
		} else {
			if($empData['mr_emp_id'] != "" || $empData['mr_emp_id'] != null) {
				$cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
				$cfLogs['mr_work_main_id'] = intval($val);
				$cfLogs['mr_emp_id'] = $empData['mr_emp_id'];
				$cfLogs['mr_status'] = "Failed";
				$cfLogs['descriptions'] = "Username or Password Invalid";

				$cf = $confirm_log_Dao->save($cfLogs);

				if($cf != ""){
					$result = 0;
				}
			} else {
				$result = 0;
			}	
		}
		echo $result;
		exit();
		break;

}

function TimeDiff($strTime1,$strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

$time_naw 	= date('H:i:s');
$time_end 	= '18:00:00';
$time_dif =  TimeDiff($time_naw,$time_end);

$txt_disabled = '';
if($time_dif < 0){
	//$txt_disabled = 'disabled';
	$time_end .= " น.";
}
//$time_dif = 1;
//$txt_disabled = '';


// 
// echo $time_dif ;
// exit();


$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$template = Pivot_Template::factory('messenger/confirmSendBranch.tpl');
$template->display(array(
	// 'debug' => print_r($path_pdf,true),
	'time_end' => $time_end,
	'txt_disabled' => $txt_disabled,
	'time_dif' => $time_dif,
	// 'success' => $success,
	'user_data' => $user_data,
	'userRoles' => $userRoles,
	'users' => $users,
	'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles(),
    // 'send_id' => $id,
	// 'barcode' => $w_main['mr_work_barcode'],
	'serverPath' => $_CONFIG->site->serverPath,
	'type' => $type,
	'work_id' => (isset($wId) ? $wId : null),
	'main_id' => (isset($main_id) ? $main_id : null)
));
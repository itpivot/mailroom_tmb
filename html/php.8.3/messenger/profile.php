<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';

/* Check authentication */
$auth = new Pivot_Auth();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

//exit;

$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
$department_data = $departmentDao->select(
		"SELECT * FROM  `mr_department` 
				WHERE  `mr_department_name` NOT LIKE  'สาขา%'
				ORDER BY  `mr_department`.`mr_department_code` ASC 
				");
$hub_data 		= $departmentDao->select("SELECT * FROM mr_hub");
$floor_data 	= $floorDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuseridProfile($user_id);

$emp_id = $user_data['mr_emp_id'];

// echo "<pre>".print_r($user_data,true)."</pre>";
// $sql = "SELECT * FROM `mr_contact` WHERE `emp_code` LIKE '".$user_data['mr_emp_code']."'";
$sql = "SELECT * FROM mr_contact c
		WHERE c.department_code in (
		  select 
			department_code
		  from mr_contact
		  where emp_code LIKE '".$user_data['mr_emp_code']."'
		  and department_code != ''
		  group by department_code
		) and c.acctive = 1 
		 or c.emp_code LIKE '".$user_data['mr_emp_code']."'
	";
		
$con_data = array();
if($user_data['mr_emp_code'] != ''){
	$con_data = $employeeDao->select($sql);
}



//echo "<pre>".print_r($user_data,true)."</pre>";
$template = Pivot_Template::factory('messenger/profile.tpl');
$template->display(array(
	//'debug' => print_r($user_data,true),
	'userRoles' => $userRoles,
	'con_data' => $con_data,
	// 'success' => $success,
	'hub_data' => $hub_data,
	'department_data' => $department_data,
	'userRoles' => $userRoles,
	'users' => $users,
	'user_data' => $user_data,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'userID' => $user_id,
	'empID' => $emp_id,
	'floor_data' => $floor_data
));
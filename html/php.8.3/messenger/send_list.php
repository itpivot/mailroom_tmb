<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();


$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();

$user_id= $auth->getUser();

$H 			= date('H');
$D 			= date('D');
// $H 			= date('H',strtotime('2024-10-26 08:20:22'));
// $D 			= date('D',strtotime('2024-10-25'));
$is_time 	= 'off';
if($D == 'Sun' || $D == 'Sat'){
	$is_time = 'off';
}else{
	if($H >= 8 && $H <= 17 ){
		$is_time = 'on';
	}else{
		$is_time = 'off';
	}
}

// echo "<pre>".print_r($H,true);
// echo "<pre>".print_r($D,true);
// echo "<pre>".print_r($is_time,true);

// exit;
$user_data = $userDao->getempByuserid($user_id);
$template = Pivot_Template::factory('messenger/send_list.tpl');
$template->display(array(
	// 'debug' 		=> print_r($path_pdf,true),
	'userRoles' 	=> $userRoles,
	'is_time' 	=> 	$is_time,
	'user_data' 	=> $user_data,
	'users' 		=> $users,
	'role_id' 		=> $auth->getRole(),
	'roles' 		=> Dao_UserRole::getAllRoles(),
	'serverPath' 	=> $_CONFIG->site->serverPath
));

?>
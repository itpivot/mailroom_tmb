<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Floor.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$floorDao = new Dao_Floor();


$users 	   = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$floors = $floorDao->fetchAll();
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$H 			= date('H');
$D 			= date('D');

// $H 			= date('H',strtotime('2024-10-26 08:20:22'));
// $D 			= date('D',strtotime('2024-10-25'));
$is_time 	= 'off';
if($D == 'Sun' || $D == 'Sat'){
	$is_time = 'off';
}else{
	if($H >= 8 && $H <= 18 ){
		$is_time = 'on';
	}else{
		$is_time = 'off';
	}
}


$template = Pivot_Template::factory('messenger/send_list_byhand_in.tpl');
$template->display(array(
	// 'debug' 		=> print_r($path_pdf,true),
	'userRoles' 	=> $userRoles,
	'floors' 		=> $floors,
	'is_time' 		=> $is_time,
	// 'success' 	=> $success,
	'user_data' 	=> $user_data,
	'users' 		=> $users,
	'role_id' 		=> $auth->getRole(),
	'roles' 		=> Dao_UserRole::getAllRoles(),
	'serverPath' 	=> $_CONFIG->site->serverPath
));
?>
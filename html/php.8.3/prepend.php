<?php

/**
 * Append include path for library
 *
 * @param  string  $path  Path directory of library
 */
function append_include_path($path) {
    set_include_path(get_include_path() . PATH_SEPARATOR . $path);
}

$documentRoot = dirname(__FILE__);
append_include_path($documentRoot . '/lib');

require_once 'Zend/Session.php';
try {
    Zend_Session::start();
} catch (Zend_Session_Exception $e) {

//echo print_r($e,true);
    session_start();
}
// set security
// - Secure Flag Header should be enabled.
// - HTTPOnly Flag Header should be enabled. 
// ini_set('session.cookie_httponly', 1);
// ini_set('session.cookie_secure', 1);
// ini_set('session.cookie_samesite', 'Strict');




require_once 'Zend/Config.php';


/**
 * Configuration
 *
 * @global  stdClass  $GLOBALS['_CONFIG']
 * @name $_CONFIG
 */
$GLOBALS['_CONFIG'] = new stdClass;

/**
 * Database configuration
 *
 * @global  Zend_Config  $GLOBALS['_CONFIG']->database
 * @name $_CONFIG->database
 */
$GLOBALS['_CONFIG']->database = new Zend_Config(require 'configs/Database.php');

/**
 * Site configuration
 *
 * @global  Zend_Config  $GLOBALS['_CONFIG']->site
 * @name $_CONFIG->site
 */
$GLOBALS['_CONFIG']->site = new Zend_Config(require 'configs/Site.php', true);
if (isset($_CONFIG->site->timezone)) {
    date_default_timezone_set($_CONFIG->site->timezone);
}
if (!isset($_CONFIG->site->documentRoot)) {
    $_CONFIG->site->__set('documentRoot', $documentRoot);
}

/**
 * View configuration
 *
 * @global  Zend_Config  $GLOBALS['_CONFIG']->view
 * @name $_CONFIG->view
 */
$GLOBALS['_CONFIG']->view = new Zend_Config(require 'configs/View.php');


if( isset($_SERVER['HTTP_USER_AGENT']) ){

	$iPod    = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$iPhone  = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$iPad    = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
	$Android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
	$webOS   = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
}

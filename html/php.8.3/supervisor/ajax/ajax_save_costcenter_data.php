<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Cost_center.php';
$auth = new Pivot_Auth();

	$req 					= new Pivot_Request();
	$cost_center_dao 	= new Dao_Cost_center();
	$text_json 				=  $req->get('text_json');
	$text_json	 			=json_decode($text_json);
	//$ems_weight		 	=  $req->get('ems_weight');

	$savedata['scg_cost_center_code'] 	 =$text_json->{'cost_center_code'};
    $savedata['name'] 				 =$text_json->{'name'};
	$savedata['address'] 			 =$text_json->{'address'};
	$savedata['districts'] 			 =$text_json->{'districts'};
	$savedata['province'] 			 =$text_json->{'province'};
	$savedata['zip_code'] 			 =$text_json->{'zip_code'};
	$savedata['tax_id'] 			 =$text_json->{'tax_id'};
	$savedata['tel1'] 				 =$text_json->{'tel1'};
	$savedata['tel2'] 				 =$text_json->{'tel2'};
	$savedata['fax'] 				 =$text_json->{'fax'};
	
	if($req->get('page')=="remove"){
		$return = $cost_center_dao->remove($req->get('scg_cost_center_id'));
	}elseif($req->get('page')=="select"){
		$return = $cost_center_dao->getCost_centerByid($req->get('scg_cost_center_id'));
	}else{
		$return = $cost_center_dao->save($savedata,$text_json->{'scg_cost_center_id'});
	}
	//if(is_numeric($return)){
	//	$return=0;
	//}else{
	//	$return=1;
	//}


	echo print_r(json_encode($return),true);
?>

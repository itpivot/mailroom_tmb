{% extends "base.tpl" %}
{% block title %}Pivot- List{% endblock %}
{% block menu_ac1 %} active {% endblock %}
{% block scriptImport %}
	
		<script src="../themes/bootstrap/js/jquery-11.1.js"></script>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="css/chosen.css">
		<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<script src="js/chosen.jquery.js" charset="utf-8"></script>
		
		<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
		<script type='text/javascript' src="../themes/bootstrap/js/bootstrap-datepicker.min.js"></script>
{% endblock %}
{% block styleReady %}
	input[type=text] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		//background-color:yellow;
	}
	textarea{
		border: 1px solid #ccc;
		border-radius: 4px;
	}
	body{
	//background-color:#eee;
	}
	.panel{
		background-color:#eee;
		padding:5px;
		
	}
	legend{
		width:15%;
	}
	
	.fieldset {
		border: 1px solid #ccc;
		padding: 10px;
		padding-top: 0px;
	}
	
{% endblock %}
 
{% block domReady %}
$( "#ems_post_type_id" ).chosen();
$( "#scg_cost_center_id" ).chosen();
	$( "#clear" ).click(function(){
		window.location.reload(true);
	});
	$( "#print_all" ).click(function() {
		 var txt = $('#form_submit').serialize();
		// window.location.href="detail_report.php?"+txt+'target="_blank"';
		 window.open(
		  "detail_repot.php?"+txt,
		  '_blank' // <- This is what makes it open in a new window.
		);
	});
	$('.input-daterange').datepicker({
		autoclose: true,
		dateFormat: "dd/mm/yy"
	});
	//ajax_sert_data
	$( "#search" ).click(function() {
		 var txt = $('#form_submit').serialize();
		// window.location.href="print_sender_post_report.php?"+txt+'target="_blank"';
		$.ajax({
		  method: "POST",
		  url: "ajax/ajax_sert_dhl_report.php?"+txt,
		})
		.done(function( msg ) {//
			//console.log( "Data Saved: " + msg );
			$( "#tbody_work_order" ).html('');
			$( "#tbody_work_order" ).append( msg );
		});
	});
	
{% endblock %}
{% block javaScript %}
		
		
{% endblock %}
{% block Content2 %}
<div class="row">
<form id="form_submit" method="post" action="dhl_export_excel.php" target="_blank">
	<div class="col-md-12">
		<fieldset class="fieldset panel">
		<legend>Export excel</legend>
		<div class="col-md-6">
			<table class="" border='0'>
				<tr>
					<td>
						Date
					</td>
					<td>
						<div class="input-daterange input-group" id="datepicker">
							<input type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From Date"/>
							<label class="input-group-addon" style="border:0px;">to</label>
							<input type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="Date"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						Cost Center
					</td>
					<td style="padding-top:5px; padding-bottom:5px;">
						<select id="scg_cost_center_id" class="my_select_box" name="scg_cost_center_id" style="width:100%">
								<option value="" selected>Input cost center</option>
							{% for cente in center %}
								<option value="{{ cente.scg_cost_center_id }}">{{ cente.scg_cost_center_code }} : {{ cente.name }}</option>
							{% endfor %}
						</select>
					</td>
				</tr>
				
			</table>
			<br><br>
				<button type="button" class="btn btn-primary" id="search" name="search">  Search</button>
				<button type="button" class="btn btn-default" id="clear" name="clear">  Clear</button>
				<button type="button" class="btn btn-default" id="print_all" name="print_all" target="_blank">  Print Detail</button>
				<button type="submit" class="btn btn-default" id="print" name="print" target="_blank">  export_excel Total Costs</button>
				
			</div>
			
			<div class="col-md-2">
				
			</div>
			<div class="col-md-4">
			
			</div>
		</fieldset>	
	</div>
            <div class="col-md-2">
                
            </div>
            <div class="col-md-5 col-md-offset-5">
				
			</div>
</form>
</div>
<div class="row">
	<div class="col-md-12">
		<fieldset class="fieldset panel">
			<legend>Mail Room Data</legend>
				<table class="table table-responsive table-bordered" id="" border="" width="100%">
					<thead>
						
						<tr class="info">
							<th class="text-center">ชื่อบริษัท</th>	
							<th class="text-center">รหัสลูกค้า</th>
							<th class="text-center">รหัสหน่วยงาน</th>	
							<th class="text-center">จำนวนเงิน</th>
							<th class="text-center">ExtraCharges</th>
							<th class="text-center">Insurance</th>	
							<th class="text-center">Charges 8%</th>	
							<th class="text-center">รวมบริการ</th>
							<th class="text-center">Vat 7%</th>	
							<th class="text-center">ยอดรวมvat</th>
						</tr>
					</thead>
					<tbody id="tbody_work_order" style="background-color:white">
						
					</tbody>
				</table>
		</fieldset>
	</div>
	
</div>
	
	
	
{% endblock %}

{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_ac1 %} active {% endblock %}

{% block javaScript %}
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	function updateStatus( scg_work_order_id )
	{
		 window.location.href = 'work.php?scg_work_order_id=' + scg_work_order_id ;
	}

	function scanBarcode()
	{
		 window.location.href = 'work_scan_barcode.php';
	}



{% endblock %}
{% block styleReady %}
{% endblock %}
{% block Content %}
	 <div class="container">
       		<div class="row">
       		   <div class="page-header">
              <h3>DHL Reports</h3> 
             </div> 
             <ul>
                {% for list in date %}
                    <li><a href="dhl_export_excel.php?months={{ list.months }}&years={{ list.years }}" >{{ list.show_month }} : {{ list.years }} </a></li>
                {% endfor %}
              </ul>
       		</div>
       </div>
{% endblock %}
{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

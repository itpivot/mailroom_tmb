{% extends "base.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_ac2 %} active {% endblock %}

{% block javaScript %}
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	function updateStatus( scg_work_order_id )
	{
		 window.location.href = 'work.php?scg_work_order_id=' + scg_work_order_id ;
	}

	function scanBarcode()
	{
		 window.location.href = 'work_scan_barcode.php';
	}



{% endblock %}
{% block styleReady %}
{% endblock %}
{% block Content %}
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-center">Menu</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<a href="work_order_report.php"><button type="button" class="btn btn-default btn-lg btn-block">Search Work Order</button></a>
			<a href="sender_post_report.php"><button type="button" class="btn btn-default btn-lg btn-block">Search Work Order Post</button></a>
		</div>
	</div>
{% endblock %}
{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

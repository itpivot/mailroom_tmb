{% extends "base.tpl" %}
{% block title %}Pivot- List{% endblock %}
{% block menu_ac2 %} active {% endblock %}
{% block scriptImport %}
	
		<script src="../themes/bootstrap/js/jquery-11.1.js"></script>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="css/chosen.css">
		<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<script src="js/chosen.jquery.js" charset="utf-8"></script>
		
		<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
		<script type='text/javascript' src="../themes/bootstrap/js/bootstrap-datepicker.min.js"></script>
{% endblock %}

{% block styleReady %}
	input[type=text] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		//background-color:yellow;
	}
	textarea{
		border: 1px solid #ccc;
		border-radius: 4px;
	}
	body{
	//background-color:#eee;
	}
	.panel{
		background-color:#eee;
		padding:5px;
		
	}
	legend{
		width:15%;
	}
	
	.fieldset {
		border: 1px solid #ccc;
		padding: 10px;
		padding-top: 0px;
	}
	
{% endblock %}
 
{% block domReady %}
$( "#Status" ).chosen();
//$( "#round" ).chosen();
$( "#ems_post_type_id" ).chosen();
$( "#scg_cost_center_id" ).chosen();
$("#scg_provinces_id").chosen();		
$("#scg_amphures_id").chosen();		
$("#user").chosen();		
	
var table = $('#tb_work_order').DataTable({
		"ajax": {
			"url": "ajax/ajax_search_data_sender_post.php",
			"data": {
				"text_json": "ff"
			}
		},
		"columns": [
            { "data": "index" },
            { "data": "system_date" },
			{ "data": "ems_barcode" },
			{ "data": "scg_cost_center" },
			{ "data": "scg_sub_cost_center" },
			{ "data": "scg_employee_name" },
			{ "data": "scg_building_name" },
			{ "data": "scg_building_floor_name" },
			{ "data": "scg_customers_name" },
			{ "data": "scg_provinces_name" },
            { "data": "t_name" },
            //{ "data": "ems_weight" },
            { "data": "ems_quantity" },
            { "data": "scg_cost" },
            { "data": "username" },
            { "data": "scg_status_namr" }
            
        ],
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		 if (aData['scg_status_namr'] == "Newly") {
				 $(nRow).addClass('highlight');
				 $(nRow).css('background-color', '#7fff7f');
		 }

		 //console.log(aData['scg_status_name']);
		},
		"order": [[ 0, "desc"]],
      	"scrollCollapse": true
});	

$( "#scg_districts_id" ).chosen().change(function() {
		$.ajax({
		  url: "ajax/ajax_select_post_code.php",
		 method: "POST",
		 data: { 
			page : "scg_districts_id",
			scg_districts_id : $( "#scg_districts_id" ).val()

		 },
		})
		  .success(function( data ) {
			 var districts = JSON.parse(data)
			  $('#scg_amphures_id').html('');
			  for(var i = 0 ; i < districts.length;i++ ){
				  $('#scg_amphures_id').append($('<option>', {
						value: districts[i]['scg_amphures_id'],
						text: districts[i]['scg_amphures_id']+" : "+districts[i]['scg_amphures_name']
					}));
				$('#scg_amphures_id').val(0).trigger("chosen:updated");
			  }
			  //console.log(data);	
		  });
		 

	});
	
$('#scg_employee_building_office_id').chosen().change(function(){
					//console.log("building : " + this.value);
					$.ajax({
						url: "ajax/ajax_select_building_floor.php",
						type: "post",
						data: {
								'building': this.value
						},
						dataType: "json",
						success: function(res){
							$('#scg_employee_building_floor_id').empty();
							for(var i = 0; i < res.length;i++){
								$('#scg_employee_building_floor_id').append($('<option></option>',
								{
									value: res[i]['scg_building_floor_id'],
									text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
								}))
							}
							//$('#scg_employee_building_floor_id').trigger("chosen:updated");
							$('#scg_employee_building_floor_id').val(0).trigger("chosen:updated");
						}
					})
		});
		
		$("#scg_employee_building_floor_id").chosen();		
		$('#txt_sender_code').keydown(function(event){
				var keycode = event.keyCode || event.which;
				if(keycode == 9 || keycode == 13){
					var txt = $('#txt_sender_code').val();
					var count_txt = txt.length;
					if(count_txt == 2){
						<!-- console.log(txt); -->
						$.ajax({
							type: 'post',
							url: 'ajax/ajax_select_building.php',
							data: {
								'building': txt,
							},
							dataType: 'json',
							success: function(res){
									<!-- console.log(res.length); -->
									if(res.length != 0){
										$("#scg_employee_building_floor_id").focus();
										var office = res[0]['scg_building_office_id'];
										$('#scg_employee_building_office_id').val(office).trigger("chosen:updated");
										 getFloorSender(office);
									}else{
										$('#buildingafter').after("<span id ='after' style='color:red;'>« ข้อมูลไม่ถูกต้อง</span>"); 
										$("#after").fadeOut(4000);
										$( "#txt_sender_code" ).focus();
									}
							}
						});
					}
					if(count_txt == 3){
						<!-- console.log(txt); -->
						$.ajax({
							type: 'post',
							url: 'ajax/ajax_select_building.php',
							data: {
								'building': txt,
							},
							dataType: 'json',
							success: function(res){
									<!-- console.log(res.length); -->
									if(res.length != 0){
										$("#scg_employee_building_floor_id").focus();
										var office = res[0]['scg_building_office_id'];
										$('#scg_employee_building_office_id').val(office).trigger("chosen:updated");
										 getFloorSender(office);
									}else{
										$('#buildingafter').after("<span id ='after' style='color:red;'>« ข้อมูลไม่ถูกต้อง</span>"); 
										$("#after").fadeOut(4000);
										$( "#txt_sender_code" ).focus();
									}
							}
						});
					}
					if(count_txt == 5){
						var b_code = txt.substring(0,3);
						var f_code = txt.substring(3);
						$.ajax({
							type: 'post',
							url: 'ajax/ajax_select_location.php',
							data: {
								'building': b_code,
								'floor': f_code
							},
							dataType: 'json',
							success: function(res){
								if(res['floor'].length != 0 && res['building'].length != 0){
									//console.log(res);
									var office = res['building'][0]['scg_building_office_id'];
									var floor = res['floor'][0]['scg_building_floor_id'];
									$('#scg_employee_building_office_id').val(office).trigger("chosen:updated");
									getFloorSender(office, floor);
								}else{
									$('#buildingafter').after("<span id ='after' style='color:red;'>« ข้อมูลไม่ถูกต้อง</span>"); 
									$("#after").fadeOut(4000);
									$( "#txt_sender_code" ).focus();
								}
							}
						});
					}
					if(count_txt == 4){
						var b_code = txt.substring(0,2);
						var f_code = txt.substring(2);
						$.ajax({
							type: 'post',
							url: 'ajax/ajax_select_location.php',
							data: {
								'building': b_code,
								'floor': f_code
							},
							dataType: 'json',
							success: function(res){
								if(res['floor'].length != 0 && res['building'].length != 0){
									var office = res['building'][0]['scg_building_office_id'];
									var floor = res['floor'][0]['scg_building_floor_id'];
									$('#scg_employee_building_office_id').val(office).trigger("chosen:updated");
									getFloorSender(office, floor);
								}else{
									$('#buildingafter').after("<span id ='after' style='color:red;'>« ข้อมูลไม่ถูกต้อง</span>"); 
									$("#after").fadeOut(4000);
									$( "#txt_sender_code" ).focus();
								}
							}
						});
					}
				}
		});
		
		
	$( "#ems_postcodes" ).keydown(function() {
		var keycode = event.keyCode || event.which;
		if(keycode == 9 || keycode == 13){
			$.ajax({
			  url: "ajax/ajax_select_post_code.php",
			 method: "POST",
			 data: { 
				page : "ems_postcodes",
				post_code : $( "#ems_postcodes" ).val()

			 },
			})
			  .success(function( data ) {
				 var districts = JSON.parse(data);
				  $('#scg_districts_id').html('');
				  for(var i = 0 ; i < districts[0].length;i++ ){
					  $('#scg_districts_id').append($('<option>', {
							value: districts[0][i]['scg_districts_id'],
							text: districts[0][i]['scg_districts_id']+" : "+districts[0][i]['scg_districts_name']
						}));
					if(districts[0].length!=0){
						
						$('#scg_districts_id').val(0).trigger("chosen:updated");
					}else{
						$('#scg_districts_id').html('');
						$('#scg_districts_id').val('').trigger("chosen:updated");
						
					}
					
				  }
				  $('#scg_amphures_id').html('');
				  for(var i = 0 ; i < districts[1].length;i++ ){
					  $('#scg_amphures_id').append($('<option>', {
							value: districts[1][i]['scg_amphures_id'],
							text: districts[1][i]['scg_amphures_id']+" : "+districts[1][i]['scg_amphures_name']
						}));
						if(districts[1].length!=0){
							$('#scg_amphures_id').val(0).trigger("chosen:updated");
						}
				  }
				$('#scg_provinces_id').html('');
				  for(var i = 0 ; i < districts[2].length;i++ ){
					  $('#scg_provinces_id').append($('<option>', {
							value: districts[2][i]['scg_provinces_id'],
							text: districts[2][i]['scg_provinces_id']+" : "+districts[2][i]['scg_provinces_name']
						}));
						if(districts[2].length!=0){
							$('#scg_provinces_id').val(0).trigger("chosen:updated");
						}
				  }
				 //console.log(districts);	
			  });
		}

	});

	$( "#clear" ).click(function(){
		window.location.reload(true);
	});
	$( "#clost_job" ).click(function(){
			var txt;
			var r = confirm("Close job  จะไม่สามารถปริ้นงานได้อีก !!!");
			if (r == true) {
			$.ajax({
				type: 'post',
				url: 'ajax/ajax_update_statas_sender_entry_data.php',
				data: {
						'building':''
				},
				//dataType: 'json',
				success: function(res){
					//Sconsole.log(res);
					alert("บันทึกข้อมูลเรียบร้อยแล้ว");
					table.ajax.reload();
					
				}
			})	
			} else {
				
			}
		});
		
	$( "#search" ).click(function(){
		var text_json = '{' ;
		var from_data = $( '#form_submit' ).serializeArray();
				
			for(var i = 0 ; i < from_data.length;i++ ){
				if(i == from_data.length-1){
					text_json+='"'+from_data[i].name+'":"'+from_data[i].value+'"}';
				}else{
					text_json+=' "'+from_data[i].name+'":"'+from_data[i].value+'",';
				}
			}
		//text_json = JSON.parse(text_json);
		//console.log(text_json);
		//table.ajax.url( "simple4.php" );	
		//table.ajax.load();	
		table.destroy();
		table = $('#tb_work_order').DataTable({
		"ajax": {
			"url": "ajax/ajax_search_data_sender_post.php",
			"data": {
				"text_json": text_json
			}
		},
		"columns": [
           { "data": "index" },
            { "data": "system_date" },
			{ "data": "ems_barcode" },
			{ "data": "scg_cost_center" },
			{ "data": "scg_sub_cost_center" },
			{ "data": "scg_employee_name" },
			{ "data": "scg_building_name" },
			{ "data": "scg_building_floor_name" },
			{ "data": "scg_customers_name" },
			{ "data": "scg_provinces_name" },
            { "data": "t_name" },
            //{ "data": "ems_weight" },
            { "data": "ems_quantity" },
            { "data": "scg_cost" },
            { "data": "username" },
            { "data": "scg_status_namr" }
            
        ],
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		 if (aData['scg_status_namr'] == "Newly") {
				 $(nRow).addClass('highlight');
				 $(nRow).css('background-color', '#7fff7f');
		 }

		 //console.log(aData['scg_status_name']);
		},
		"order": [[ 0, "desc"]],
      	"scrollCollapse": true
		});
		//text_json = JSON.parse(text_json);
		//console.log(text_json);
	});
	$( "#print_all" ).click(function() {
		 var txt = $('#form_submit').serialize();
		// window.location.href="print_sender_post_report.php?"+txt+'target="_blank"';
		 window.open(
		  "print_sender_post_report.php?"+txt,
		  '_blank' // <- This is what makes it open in a new window.
		);
	});
{% endblock %}
{% block javaScript %}
		function getFloorSender(building, floor){
			$.ajax({
				type: 'post',
				url: 'ajax/ajax_select_floor.php',
				data: {
						'building': building,
				},
				dataType: 'json',
				success: function(res){
					$('#scg_employee_building_floor_id').empty();
					for(var i = 0; i < res.length;i++){
						$('#scg_employee_building_floor_id').append($('<option></option>',
						{
							value: res[i]['scg_building_floor_id'],
							text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
						}))
					}
					$('#scg_employee_building_floor_id').val(floor).trigger("chosen:updated");
				}
			});
		}
		$('.datepicker').datepicker({
		  dateFormat: "dd/mm/yy"
		});
		
		$('.input-daterange').datepicker({
			autoclose: true,
			dateFormat: "dd/mm/yy"
		});
		
		
{% endblock %}
{% block Content2 %}
<div class="row">
<form id="form_submit" method="post" action="print_total_costs_sender_post_report.php" target="_blank">
	<div class="col-md-12">
		<fieldset class="fieldset panel">
		<legend>Search</legend>
		<div class="col-md-4">
			<table class="" border='0'>
				<tr>
					<td>
						Barcode
					</td>
					<td>
						<input name="barcode" type="text" id="barcode" name="barcode" class="form-control" placeholder="Barcode" >
					</td>
				</tr>
				<tr>
					<td>
						Date
					</td>
					<td>
						<div class="input-daterange input-group" id="datepicker">
							<input type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From Date"/>
							<label class="input-group-addon" style="border:0px;">to</label>
							<input type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="Date"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						Status
					</td>
					<td>
						<select id="Status" class="form-control" name="Status" >
							<option value="" selected >Input Sender Status</option>
							{% for Status in statuse %}
								<option value="{{ Status.scg_status_id }}">{{ Status.scg_status_id }} : {{ Status.name }}</option>
							{% endfor %}
						</select>
								
					</td>
				</tr>
				<tr>
					<td>
						User
					</td>
					<td>
						<select id="user" class="form-control" name="user" >
							<!-- <option value="" selected >Input user</option> -->
							<option value="{{user_id}}" selected>{{username}}</option>
							<option value="">All</option>
						</select>
								
					</td>
				</tr>
				<tr>
					<td>
						Round
					</td>
					<td>
						<div class="input-group" id="" style="padding-top:5px; padding-bottom:5px;">
							<input type="text" class="input-sm form-control" id="start_round" name="start_round" placeholder="Start Round"/>
							<label class="input-group-addon" style="border:0px;">to</label>
							<input type="text" class="input-sm form-control" id="end_round" name="end_round" placeholder="End Round"/>
						</div>
					</td>
				</tr>
				
			</table>
			<br><br>
			
			</div>
			<div class="col-md-4">
			<table class="" border='0'>
						<tr>
							<td>
								Sender Name
							</td>
							<td>
								<input type="text" id="scg_employee_name" name="scg_employee_name" class="" placeholder="sender" style="width:300px;">
							</td>
						</tr>
						<td>
								Building
							</td>
							<td>
								<input type="text" name="txt_sender_code" id="txt_sender_code" class="" maxlength="5" style="width:75px;"placeholder="search">
								<select id="scg_employee_building_office_id" class="my_select_box" name="scg_employee_building_office_id" style="width:200px;">
									<option value="" selected>Input Sender Building</option>
									{% for offices in office %}
										<option value="{{ offices.scg_building_office_id }}">{{ offices.scg_building_code }} : {{ offices.scg_building_name }}</option>
									{% endfor %}
								</select>
							</td>
						</tr>
						<tr>
							<td id='buildingafter'>
							</td>
						</tr>
						<tr>
							<td>
								Floor 
							</td>
							<td>
								<select id="scg_employee_building_floor_id" class="" name="scg_employee_building_floor_id" style="width:300px;"> 
									 <option value=""  selected>Input Sender Floor</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								Cost Center
							</td>
							<td>
								<select id="scg_cost_center_id" class="my_select_box" name="scg_cost_center_id" style="width:300px;">
									<option value="" selected>Input cost center</option>
									{% for cente in center %}
										<option value="{{ cente.scg_cost_center_id }}">{{ cente.scg_cost_center_code }} : {{ cente.name }}</option>
									{% endfor %}
								</select>
							</td>
						</tr>
						<tr>
							<td>
								Ems Type
							</td>
							<td>
								<select id="ems_post_type_id" class="" name="ems_post_type_id" style="width:300px;">
											<option value=""  selected>Input Sender</option>
										{% for post_type in post_type %}
										<option value="{{ post_type.scg_post_type_id }}">{{ post_type.scg_post_type_id }} : {{ post_type.name }}</option>
										{% endfor %}
								</select>
							</td>
						</tr>
						
					</table>
			</div>
			<div class="col-md-4">
				<table class="" border='0'>
						<tr>
							<td>
								Receiver Name
							</td>
							<td>
								<input type="text" id="scg_customers_name" name="scg_customers_name" class="" placeholder="Receiver Name" style="width:280px;">
							</td>
						</tr>
						<td>
								Postcode
							</td>
							<td>
								<input type="text" id="ems_postcodes" name="ems_postcodes" class="" placeholder="Postcode" style="width:280px;">
							</td>
						</tr>
						<tr>
							<td>
								Sub District
							</td>
							<td>
								<select id="scg_districts_id" class="" name="scg_districts_id" style="width:280px;">
										<option value="">----</option>
								</select>
							</td>
						</tr><tr>
							<td>
								District
							</td>
							<td>
								<select id="scg_amphures_id" class="" name="scg_amphures_id" style="width:280px;">
										<option value="">----</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								Province
							</td>
							<td>
								<select id="scg_provinces_id" class="" name="scg_provinces_id" style="width:280px;">
										<option value="">----</option>
								</select>
							</td>
						</tr>
					</table>
			</div>
			
		</fieldset>	
	</div>
            <div class="col-md-2">
                <a href="check_duplicate.php">
                    <button type="button" class="btn btn-default btn-block">Check Duplicate</button>
                </a>
            </div>
            <div class="col-md-5 col-md-offset-5">
				<button type="button" class="btn btn-primary" id="search" name="search">  Search</button>
				<button type="button" class="btn btn-default" id="clear" name="clear">  Clear</button>
				<!-- - ++<a href="print_sender_post_report.php"  class="btn btn-default" id="print" name="print" target="_blank">  Print All Job</a>++ */ -->
				<button type="button" class="btn btn-default" id="print_all" name="print_all" target="_blank">  Print All Job</button>
				<button type="submit" class="btn btn-default" id="print" name="print" target="_blank">  Print Total Costs</button>
				<!-- <button type="button" class="btn btn-default" id="clost_job" name="danger">  Close Job </button> -->
			</div>
</form>
</div>
<div class="row">
	<div class="col-md-12">
		<fieldset class="fieldset panel">
			<legend>Mail Room Data</legend>
				<table class="hover" id="tb_work_order" cellspacing="0" >
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th	class="text-center" width="500px">Data</th>
							<th	class="text-center">Barcode</th>
							<th	class="text-center">CostCenter</th>
							<th	class="text-center">subCC</th>
							<th	class="text-center">Sender</th>
							<th	class="text-center">Building</th>
							<th	class="text-center">Floor</th>
							<th	class="text-center">Receiver</th>
							<th	class="text-center">Province</th>
							<th	class="text-center" width="220px">Service Name</th>
							<!-- <th	class="text-center">Weigth</th> -->
							<th	class="text-center">Qty</th>
							<th	class="text-center">TotalCost</th>
							<th	class="text-center">keyIn</th>
							<th	class="text-center">Status</th>
							
							
						</tr>
					</thead>
					<tbody id="tbody_work_order" >
					{% for table in order_post %}
						<tr>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							
							
						</tr>
						{% endfor %}
					</tbody>
				</table>
		</fieldset>
	</div>
	
</div>
	
	
	
{% endblock %}

{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base.tpl" %}

{% block title %}Pivot- รายการค้นหาข้อมูล{% endblock %}
    
{% block menu_1 %}
  <div id="menu_1_active">
    <div id="txtMenu">
      <a href="../admin/list.php"><font class="menuHead_active">แจ้งเตือน</font></a>
    </div>         
  </div>
{% endblock %}

{% block styleReady %}
	
	table{
		width : 100%;
	}	
	
	table tr td:first-child {
		text-align : center;
	}	
	
	td, th{
		padding : 5px;
	}
	
	#list{
		min-height:500px;
	}
	
{% endblock %}

{% block topContent %}รายการแจ้งเตือน{% endblock %}

{% block body %}
	<div id="list">
		<center>
			<table border="1">
				<tr align="center" >
					<th>ลำดับ</th>
					<th>เหตุการณ์</th>
					<th>จำนวนงาน</th>
					<th>เวลาออกเดินทาง</th>
					<th>เวลาถึงปลายเทาง</th>
					<th>ผู้วิ่งงาน</th>
					<th>เพิ่มเติม</th>
				</tr>
				<tr>
					<td>1</td>
					<td>งานเข้าจากชั้น 9 ตึก A</td>
					<td>3</td>
					<td align="center">8:00</td>
					<td align="center">9:00</td>
					<td>mess</td>
					<td align="center"><a href="" >ดูรายละเอียด</a></td>
				</tr>
				<tr>
					<td>2</td>
					<td>ส่งงานสาขาพญาไท ( 013 ) </td>
					<td>7</td>
					<td align="center">10:00</td>
					<td align="center">13:00</td>
					<td>mess</td>
					<td align="center"><a href="" >ดูรายละเอียด</a></td>
				</tr>
			</table>
		</center>	
	</div>	
{% endblock %}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  {% block head %}
		<title>{% block title %}Pivot Mailroom Services{% endblock %}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pivot Mailroom Services">
		<meta name="author" content="Pivot">
		<link rel="icon" href="../themes/bootstrap/css/favicon.ico" />
		<!-- Bootstrap core CSS -->
		<link href="../themes/bootstrap/css/bootstrap.css" rel="stylesheet">
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../themes/bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="../themes/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet">

		{% block scriptImport %}{% endblock %}
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../themes/bootstrap/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  {% endblock %}
  <style type="text/css">
  {% block styleReady %}{% endblock %}
  </style>
</head>
<body>
<!--Start Header-->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><span><img src="../themes/images/logo-pv_mini.png" title="Pivot Co., Ltd." /> Mailroom Services</span></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
		{% if role_id == roles.Administrator %}
			<li class="{% block menu_a1 %}{% endblock %}"><a href="list.php">News</a></li>
			<li class="{% block menu_a2 %}{% endblock %}"><a href="import.php">Import</a></li>
			<li><a href="../user/logout.php">Logout</a></li>
		{% elseif role_id == roles.Employee %}
			<li class="{% block menu_e1 %}{% endblock %}"><a href="news.php">News</a></li>
			<li class="{% block menu_e2 %}{% endblock %}"><a href="order.php">Order</a></li>
			<li class="{% block menu_e3 %}{% endblock %}"><a href="print.php">Print</a></li>
			<li class="{% block menu_e4 %}{% endblock %}"><a href="search.php">Search</a></li>
			<li class="{% block menu_e5 %}{% endblock %}"><a href="report.php">Report</a></li>
			<li><a href="../user/logout.php">Logout</a></li>
		{% elseif role_id == roles.Messenger %}
			<li class="{% block menu_m1 %}{% endblock %}"><a href="news.php">News</a></li>
			<li class="{% block menu_m2 %}{% endblock %}"><a href="work_list.php">Work</a></li>
			<li class="{% block menu_m3 %}{% endblock %}"><a href="search.php">Search</a></li>
			<li class="{% block menu_m4 %}{% endblock %}"><a href="report.php">Report</a></li>
			<li><a href="../user/logout.php">Logout</a></li>
		{% elseif role_id == roles.Mailroom %}
			<li class="{% block menu_mr1 %}{% endblock %}"><a href="news.php">News</a></li>
			<li class="{% block menu_mr2 %}{% endblock %}"><a href="work_list.php">Work</a></li>
			<li class="{% block menu_mr3 %}{% endblock %}"><a href="search.php">Search</a></li>
			<li class="{% block menu_mr4 %}{% endblock %}"><a href="report_sender_entry_data.php">Reports</a></li>
			<li><a href="../user/logout.php">Logout</a></li>
		{% elseif role_id == roles.Supervisor %}
			<li class="{% block menu_sp1 %}{% endblock %}"><a href="management_menu.php">Management Menu</a></li>
			<li><a href="../user/logout.php">Logout</a></li>
		{% elseif role_id == roles.Accounting %}
			<li class="{% block menu_ac1 %}{% endblock %}"><a href="../accounting/report.php">Reports</a></li>
			<li class="{% block menu_ac2 %}{% endblock %}"><a href="../accounting/search.php">Search</a></li>
			<li><a href="../user/logout.php">Logout</a></li>
		{% endif %}
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
<!--End Header-->
<!--Start Container-->
<div class="container">{% block Content %}{% endblock %}</div>
<div class="container-fluid">{% block Content2 %}{% endblock %}</div>
<!--End Container-->

<!--Start Footer-->
<footer class="footer">
  <div class="container">
    <p class="text-muted">Copyright © 2016 Pivot Co., Ltd.</p>
  </div>
</footer>
<!--End Footer-->

<!--Start Debug-->

	{% block debug %}{% endblock %}
<!--End Debug-->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="../themes/bootstrap/js/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="../themes/bootstrap/js/jquery.vendor.min.js"><\/script>')</script>
    <script src="../themes/bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../themes/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

	  <link href="../themes/jquery/jquery-ui.css" rel="stylesheet">
<!--       <script src="../themes/jquery/jquery-ui.js"></script> -->


	<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
	<!-- <script src="../themes/bootstrap/js/accounting.min.js"></script> -->
	<script type="text/javascript">
		$(document).ready(function(){
			{% block domReady %}{% endblock %}
     	    {% block domReady2 %}{% endblock %} 
		});
	   {% block javaScript %}{% endblock %}
	</script>
</body>
</html>

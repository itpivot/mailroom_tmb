<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	{% block head %}
		<title>{% block title %}Pivot{% endblock %}</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		{% block cssImport %}{% endblock %} 
		<link href="../themes/styles_admin.css" rel="stylesheet" type="text/css" />

		{% block scriptImport %}{% endblock %}     
		<script type="text/javascript">
			$(document).ready(function(){    
				{% block domReady %}{% endblock %} 
			})
			function selectFocus(x)
			{
				x.style.background="#f9ff4b";
			}
		   {% block javaScript %}{% endblock %}     
		</script> 
	{% endblock %} 
	<style type="text/css">
		{% block styleReady %}{% endblock %}
	</style> 
</head>
<body>
<div id="page">
	<center>
		<div style="background-color: #E3E3E3;border-bottom: 1px solid #4E4E4E;">
			<img src="../themes/images/logo_admin.png" />
		</div>
	</center>	
	<div id="sections">
		<ul>
			<li class="alert" >
				<a href="../admin/list.php">
					<h2>แจ้งเตือน</h2>
					<p>ดูรายการอัพเดดงานทั้งหมด</p>
				</a>
			</li>
			<li class="list_upload">
				<a href="../admin/list_upload.php">
					<h2>ไฟล์อัพโหลด</h2>
					<p>รายการอัพโหลดงานแต่ล่ะรอบ</p>
				</a>
			</li>
			<li class="print_work">
				<a href="../admin/list_order.php">
					<h2>ใบคุมงาน</h2>
					<p>สั่งปริ้นใบคุมงาน</p>
				</a>
			</li>
			<li class="order_branch">
				<a href="../admin/hub_add_work.php">
					<h2>สั่งงาน</h2>
					<p>สั่งงานระหว่างสาขา</p>
				</a>
			</li>
			<li class="update">
				<a href="../admin/update.php">
					<h2>คืนงาน</h2>
					<p>อัพเดดผลการวิ่งเอกสาร</p>
				</a>
			</li>
			<li class="cheque">
				<a href="../admin/list_cheque.php">
					<h2>รับเช็ค</h2>
					<p>อัพเดดยอดการรับเช็ค</p>
				</a>
			</li>
			<li class="search">
				<a href="../admin/search.php">
					<h2>ค้นหา</h2>
					<p>ค้นหาสถานะงานทั้งหมด</p>
				</a>
			</li>
			<li class="report">
				<a href="../admin/list_report.php">
					<h2>รายงาน</h2>
					<p>ดูรายงานการวิ่งงานทั้งหมด</p>
				</a>
			</li>
		</ul>
	</div>
	<div id="txtLogin">
		<a href="../user/logout.php"><h1>Logout</h1></a>
	</div>
	<div id="main_frame">
		{% block body %}
		{% endblock %}
	</div>
	<br />
</div>
<div id="footer">
<span class="txtfooter"><p>แจ้งปัญหาหรือคำแนะนำการใช้งานโปรแกรมได้ที่ admin@pivot.co.th </p></span>
<span class="txtfooter">&copy; Copyright 2013 Mail Room All Rights Reserved.</span>
</div>
</body>
</html>
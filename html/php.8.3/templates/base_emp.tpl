<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  {% block head %}
		<title>{% block title %}Pivot Mailroom Services{% endblock %}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pivot Mailroom Services">
		<meta name="author" content="Pivot">
		<link rel="icon" href="../themes/bootstrap/css/favicon.ico" />
		
		
		   <!-- Bootstrap core CSS -->
    <link href="../themes/bootstrap_emp/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../themes/bootstrap_emp/bootstrap/css/simple-sidebar.css" rel="stylesheet">
		
	<link href="../themes/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet">
		
	  <link rel="stylesheet" type="text/css" href="../themes/material_icon/material-icons.min.css">
		
	{% block scriptImport %}{% endblock %}
	<script src="../themes/bootstrap/js/ie-emulation-modes-warning.js"></script>

	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	
	
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  {% endblock %}
  <style type="text/css">
  {% block styleReady %}{% endblock %}
  
  #logout{
	border-top: 1px solid #0266bb;
    margin-left:20px;
    margin-right:50px;
  }
  
  .list-group-item {
    padding: .25rem 1.25rem; */
    margin-bottom: -1px;
	border: 0px solid rgba(0,0,0,.125);
}

.material-icons {
    vertical-align: middle;
	display: inline; 
}

.btn-success span {
    vertical-align: middle;
}

.status {
    border: 1px solid #e0e0e0;
    padding: 5px;
    padding-left: 10px;
    padding-right: 10px;
    border-radius: 15px;
}

.card {
	box-shadow: 2px 2px 1px 1px rgba(50,50,50,.4);
}

.btn{
	box-shadow: 2px 2px 1px 1px rgba(50,50,50,.4);
}

  
  </style>
</head>
<body>
<!--Start Header-->
<nav class="navbar navbar-light bg-faded navbar-fixed-top" style="background-image: linear-gradient(to right, #ffc3a0 0%, #ffafbd 100%);">  <!-- style="background-color: #0266bb;" -->
				
	<div class="navbar-header">
		<a href="#menu-toggle" id="menu-toggle" style="padding: 0px 0px;"><span class="navbar-toggler-icon"></span></a>
		<a class="navbar-brand" href="#"><span style="color:white;"><img src="../themes/images/logo-pv_mini.png" title="Pivot Co., Ltd." /> Mailroom Services</span></a>
	
	</div>
</nav>
 <div id="wrapper">
				
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
						<a href="#!name"><span class="white-text name">{{ user_data.mr_user_username }}</span></a>
						
                    </a>
                </li>
                {% if role_id == roles.Administrator %}
					<li class="{% block menu_a1 %}{% endblock %}"><a href="list.php">News</a></li>
					<li class="{% block menu_a2 %}{% endblock %}"><a href="import.php">Import</a></li>
					<li><a href="../user/logout.php">Logout</a></li>
				{% elseif role_id == roles.Employee %}
					<li><a data-toggle="collapse" href="#collapse1" ><i class="material-icons" style="padding-right:5px;">content_paste</i>การสั่งงาน <i class="material-icons">arrow_drop_down</i></a></li>
						<div id="collapse1" class="panel-collapse collapse">
							<ul class="list-group" >
								<li class="list-group-item" ><a href="work_in.php">รับส่งภายใน</a></li>
								<li class="list-group-item" ><a href="work_out.php">ส่งภายนอก</a></li>
								<li class="list-group-item" ><a href="work_byhand.php">By Hand(เอกสารถึงลูกค้า)</a></li>
								<li class="list-group-item" ><a href="work_post.php">ส่งไปรษณีย์</a></li>
							</ul>
						</div>
						
					<li><a href="#"><i class="material-icons" style="padding-right:5px;">inbox</i>  รับเอกสาร (Inbox) </a></li>
					<li><a href="search_follow.php"><i class="material-icons" style="padding-right:5px;">search</i>ค้นหาและติดตามเอกสาร (Outbox)</a></li>
					<li id="logout"></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				{% elseif role_id == roles.Messenger %}
					<li><a href="news.php">หน้าแรก</a></li>
					<li><a href="receive_list.php">รับเอกสาร</a></li>
					<li><a href="send_list.php">ส่งเอกสาร</a></li>
					<li><a href="../user/logout.php">Logout</a></li>
				{% elseif role_id == roles.Mailroom %}
					<li class="{% block menu_mr1 %}{% endblock %}"><a href="news.php">News</a></li>
					<li class="{% block menu_mr2 %}{% endblock %}"><a href="work_list.php">Work</a></li>
					<li class="{% block menu_mr3 %}{% endblock %}"><a href="search.php">Search</a></li>
					<li class="{% block menu_mr4 %}{% endblock %}"><a href="report_sender_entry_data.php">Reports</a></li>
					<li><a href="../user/logout.php">Logout</a></li>
				{% elseif role_id == roles.Branch %}
					<li><a data-toggle="collapse" href="#collapse1" ><i class="material-icons" style="padding-right:5px;">content_paste</i>การสั่งงาน <i class="material-icons">arrow_drop_down</i></a></li>
						<div id="collapse1" class="panel-collapse collapse">
							<ul class="list-group" >
								<li class="list-group-item" ><a href="work_in.php">ส่งสำนักงานใหญ่</a></li>
								<li class="list-group-item" ><a href="work_out.php">ส่งระหว่างสาขา</a></li>
							</ul>
						</div>
					<li><a href="search_follow.php"><i class="material-icons" style="padding-right:5px;">search</i>ค้นหาและติดตามเอกสาร (Outbox)</a></li>
					<li id="logout"></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				{% elseif role_id == roles.Accounting %}
					<li class="{% block menu_ac1 %}{% endblock %}"><a href="../accounting/report.php">Reports</a></li>
					<li class="{% block menu_ac2 %}{% endblock %}"><a href="../accounting/search.php">Search</a></li>
					<li><a href="../user/logout.php">Logout</a></li>
				{% endif %}
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
</div>
<!--End Header-->
<!--Start Container-->
<div class="container">{% block Content %}{% endblock %}</div>
<div class="container-fluid">{% block Content2 %}{% endblock %}</div>
<!--End Container-->




<!--Start Footer-->
<footer class="footer">
  <div class="container">
    <p class="text-muted">Copyright © 2017 Pivot Co., Ltd.</p>
  </div>
</footer>
<!--End Footer-->

<!--Start Debug-->

	{% block debug %}{% endblock %}
<!--End Debug-->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="../themes/bootstrap/js/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="../themes/bootstrap/js/jquery.vendor.min.js"><\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../themes/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

	<link href="../themes/jquery/jquery-ui.css" rel="stylesheet">
	  
	<script src="../themes/bootstrap_emp/jquery/jquery.min.js"></script>
    <script src="../themes/bootstrap_emp/popper/popper.min.js"></script>
    <script src="../themes/bootstrap_emp/bootstrap/js/bootstrap.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	  
	  
	  
<!--       <script src="../themes/jquery/jquery-ui.js"></script> -->


	<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
	<!-- <script src="../themes/bootstrap/js/accounting.min.js"></script> -->
	<script type="text/javascript">
		$(document).ready(function(){
		
		 $("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
				
		$('.dropdown-toggle').dropdown()
		
		
		$('select').select2();
		
		
			{% block domReady %}{% endblock %}
     	    {% block domReady2 %}{% endblock %} 
		});
	   {% block javaScript %}{% endblock %}
	</script>
</body>
</html>

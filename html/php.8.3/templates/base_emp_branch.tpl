<!DOCTYPE html>

<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" >
<head>
  {% block head %}
		<title>The Digital PEON Book System{% block title %}{% endblock %}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Pivot Mailroom Services">
		<meta name="author" content="Pivot">
		<link rel="icon" href="../themes/bootstrap/css/favicon.ico" />
		 <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		   <!-- Bootstrap core CSS 
   <link href="../themes/bootstrap-4.1.3/css/bootstrap.min.css" rel="stylesheet">
     --><link href="../themes/bootstrap_emp/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	-->
	<link rel="stylesheet" href="../themes/alertifyjs/css/alertify.css" id="alertifyCSS">
	<link rel="stylesheet" href="../themes/alertifyjs/css/themes/default.css" >
	
	  <!-- Datepicker -->
	
    <!-- Custom styles for this template -->
  
		
	<link href="../themes/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet">
			 
	 <link rel="stylesheet" type="text/css" href="../themes/material_icon/material-icons.min.css">
	 
	 <link rel="stylesheet" type="text/css" href="../themes/fancybox/jquery.fancybox.min.css">

	<!-- <link rel="stylesheet" href="../themes/progress-tracker/styles/site.css"> -->
    <link rel="stylesheet" href="../themes/progress-tracker/styles/progress-tracker.css">
	 
	<!-- <script src="../themes/bootstrap_emp/jquery/jquery.min.js"></script> -->
	<script src="../scripts/jquery-1.12.4.js"></script>
	
	{% block scriptImport %}{% endblock %}
	<script src="../themes/bootstrap/js/ie-emulation-modes-warning.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js"></script>

	<!-- <link rel="stylesheet" href="../themes/bootstrap_emp/dist/sweetalert2.min.css"> -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	
	
	
	<script src="../themes/bootstrap_emp/dist/slideout.min.js"></script>
	
	
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  {% endblock %}
  <style type="text/css">
  {% block styleReady %}{% endblock %}
	body{
	padding:0px !important;
	margin:0px !important;
	background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
		background-repeat: no-repeat;
		background-attachment: fixed;
	}
	.footer_ {
			bottom: 0;
			width:100%;
			background-color: #0074c0 ;
			color:#fff;
			vertical-align: middle;
			padding:1%;
			font-size:13;
	}

		.img{
		width:100%;
		margin-bottom:-3%;
		}
		.all{
		padding-right:0px;
		padding-left:0px;
		height:100%;
		-webkit-box-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
		-moz-box-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
		box-shadow: 0px 0px 18px -1px rgba(128,123,128,0.5);
		
		}
		.m_nav{background: #ece9e6; /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #ece9e6, #ece9e6); /* Chrome 10-25, Safari 5.1-6 */
		width:100%;
		//opacity: 0.9;
		//filter: alpha(opacity=90);
		}
		
		.nav-item>.active {
			color: #fff !important;
			background-color: #0074c0 !important;
			border-radius: 5px;
			padding-left:10px;
		}
		.navbar-nav .nav-link, 
		.show>.navbar-nav .nav-link {
			font-weight: bold !important;
			font-size:14px !important;
		}
		.nav-link:hover{
			color: #0077c1 !important;
		}
		.nav-item>.active:hover{
			color: #fff !important;
		}
		.m_nav a {
			color: #777777;
			text-decoration: none;
			background-color: transparent;
			-webkit-text-decoration-skip: objects;
		}
	.btn_logout{
		border: none;
		outline: 0;
		display: inline-block;
		padding: 8px;
		color: #000;
		text-align: center;
		cursor: pointer;
		width: 100%;
		font-size: 18px;
	}

  </style>
</head>
<body onload="resize()" onresize="resize()">
<!--
<body background="../themes/images/bg.png">
body>Start Header-->




 


<!--End Header-->
<!--Start Container-->
<div class="container-fluid">
<div class="row justify-content-center">
    <div class="all col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xl-offset-1 col-lg-offset-1">	
				
		<header class="panel-header" >
				<img class="img"  src="../themes/images/Artboard4.png" alt="Smiley face" class="img-fluid mx-auto " alt="Responsive image">
				<nav class="m_nav navbar navbar-expand-lg navbar-light bg-light">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					   <div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav mr-auto">
					
						<li class="nav-item" id="menu_1">
							<a class="nav-link {% block menu_1 %} {% endblock %}" href="./">หน้าหลัก</a>
						</li>
						  <li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle {% block menu_3_maim %} {% endblock %}" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									สร้างรายการนำส่ง
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown2">
									<a class="dropdown-item {% block menu_3_1 %} {% endblock %}" href="create_work.php">ส่งสาขาและอาคารต่างๆ</a>
									{% if user_data.mr_branch_id == 1386  or user_data.mr_branch_id == 1383 or user_data.mr_branch_id == 1384 or user_data.mr_branch_id == 1385 or user_data.mr_branch_id == 1419 or user_data.mr_branch_id == 88%}
										<div class="dropdown-divider"></div>
										<a class="dropdown-item {% block menu_3_2 %} {% endblock %}" href="work_byhand.php">By Hand</a>
									{% endif %}
									{% if user_data.mr_branch_id == 1383 or user_data.mr_branch_id == 88 %}
											<div class="dropdown-divider"></div>
										<a class="dropdown-item {% block menu_3_3 %} {% endblock %}" href="work_post.php">ส่งไปรษณีย์</a>
									{% endif %}
								</div>
							</li>
						<li class="nav-item">
								<a class="nav-link {% block menu_4 %} {% endblock %}" href="send_work_all.php">รวมเอกสารนำส่ง</a>
						</li>
						<li class="nav-item">
								<a class="nav-link {% block menu_2 %} {% endblock %}" href="search_work.php">รายการเอกสารนำส่ง</a>
						</li>
						{% if user_data.mr_user_username != "TL0001" and  user_data.mr_user_username != "88415" and  user_data.mr_user_username != "CL0193/63/013" %}
						<li class="nav-item">
								<a class="nav-link {% block menu_6 %} {% endblock %}" href="receive_work.php">รับเอกสาร</a>
						</li>
						{% else %}
						  <li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle {% block menu_7_maim %} {% endblock %}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									รับเอกสาร
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item {% block menu_7_1 %} {% endblock %}" href="receive_work.php">รับเอกสารส่วนตัว</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item {% block menu_7_2 %} {% endblock %}" href="reciever_branch.php">รับเอกสาร Mailroom</a>
								</div>
							</li>
							{% endif %}
						<li class="nav-item">
									<a class="nav-link {% block menu_5 %} {% endblock %}" href="reports.php">รายงาน</a>
						</li>
							
					</ul>
					<ul class="nav nav-pills">							
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								{{ user_data.mr_user_username }}
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown2">
									<div class="dropdown-divider"></div>
									<a href="profile.php" class="dropdown-item preview-item text-center">
										<div class="">
												<i style="font-size: 50px !important;"class="material-icons md-18">perm_identity</i>
										</div>
										<div class="preview-item-content">
											<h6 class="preview-subject font-weight-medium text-dark">Profile</h6>
											<p class="font-weight-light small-text">
												Settings
											</p>
										</div>
									</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item preview-item">
											<a class="btn_logout bg-primary text-white" href="profile.php">แก้ไขข้อมูลส่วนตัว</a>
										<div class="preview-thumbnail">
											<div class="preview-icon bg-info">
												<i class="mdi mdi-email-outline mx-0"></i>
											</div>
										</div>
										<div class="preview-item-content">
											<a class="btn_logout bg-primary text-white" href="../user/logout.php">ออกจากระบบ</a>
										</div>
									</a>	
							</div>
						</li>
					</ul>
					</div>
				</nav>
				
				
				
				
				
				
				
				


				
				
				
				
				
				
		</header> 
			
		
	

	<!--  <header class="panel-header" style="background-color: #DCD9D4; 
	background-image: linear-gradient(to bottom, rgba(255,255,255,0.50) 0%, rgba(0,0,0,0.50) 100%), radial-gradient(at 50% 0%, rgba(255,255,255,0.10) 0%, rgba(0,0,0,0.50) 50%); 
	background-blend-mode: soft-light,screen;">
			<span class="js-slideout-toggle"><i class="material-icons">menu</i> <b>MENU</b> </span>
			<span class="header-nav">
				
					<label class="header-text" >The Digital PEON Book System</label>
				<!-- <img class="img-responsive" style="position:absolute;right:0px;padding:10px;" src="../themes/images/logo-tmb_mini.png" title="TMB" />  	
			</span>
	</header>  -->
			
	
		
		<div  class="con">
			<div id="main">{% block Content %}{% endblock %}</div>
			<div>{% block Content2 %}{% endblock %}</div>
		</div>
	
	<!--End Container-->
	
<!--Start Footer-->
		<footer class="footer_ align-middle text-center">
			<div class="align-middle"> 
				<p class="align-middle">
					<span class="">
						<strong>ติดต่อห้อง Mailroom:</strong>
						02-299-1111 ต่อ 5694
					</span>
				</p>
				<p class=""><b>ปัญหาการใช้งานระบบติดต่อที่:</b></p>
				<p class=""><strong></strong>คุณจุฑารัตน์ เนียม ศรี เบอร์โทร 061-823-4346 อีเมล์ jutarat_ni@pivot.co.th</p>
				<p class=""><strong></strong>คุณณิธิวัชรา อยู่ถิ่น เบอร์โทร 02-299-5694 (ห้องสารบรรณกลาง) อีเมล์ Mailroom@pivot.co.th</p>
				<p class="align-middle">Copyright © 2018 Pivot Co., Ltd.</p>
			</div>
		</footer>
	<!--End Footer-->
	</div>
</div>







</div>


<!--Start Debug-->

	{% block debug %}{% endblock %}
<!--End Debug-->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="../themes/bootstrap/js/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="../themes/bootstrap/js/jquery.vendor.min.js"><\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../themes/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	
<!-- 	<link href="../themes/jquery/jquery-ui.css" rel="stylesheet"> -->
	  
	
<!-- 	<script src="../themes/bootstrap_emp/dist/sweetalert2.min.js"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
	
    <script src="../themes/bootstrap_emp/popper/popper.min.js"></script>
    <script src="../themes/bootstrap_emp/bootstrap/js/bootstrap.min.js"></script>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script src="../themes/fancybox/jquery.fancybox.min.js"></script>
	<link rel="stylesheet" href="../themes/bootstrap/css/select2-bootstrap4.min.css">

	<script src="../themes/progress-tracker/scripts/site.js"></script>
	<script src="../themes/moment/moment.min.js"></script>
	<script src="../themes/moment/moment-local.min.js"></script>

	<script src="../themes/jquery-validate/jquery.validate.min.js"></script>
	<script src="../themes/alertifyjs/alertify.js"></script> 
	<!-- <script src="../themes/jquery/jquery-ui.js"></script> -->
	<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
	<!-- <script src="../themes/bootstrap/js/accounting.min.js"></script> -->
	<script type="text/javascript">

	function resize() {
            var frame = document.getElementById("main");
            var windowheight = window.innerHeight;
				windowheight -= 290;
            //document.body.style.height = windowheight + "px";
           // frame.style.mi-height = windowheight - 290 + "px";
			$('#main').css('min-height', windowheight);
        }
		$(document).ready(function(){
		
		 
		
		
		
		
		
		$('.dropdown-toggle').dropdown()
	{% if select == 0 %}
	{% else %}
		$('select').select2(
			{
				theme: 'bootstrap4',
			}
		);
	{% endif %}
		
		var usrID = '{{ userID }}';
		$.ajax({
			url: '../data/ajax/ajax_check_password_Date.php',
			method: 'GET',
			data: { userID: usrID },
			dataType: 'json',
			success: function (res) {
			//console.log(res);
				if(parseInt(res['diffdate']) >= 45 ) {
					//alert('');
					alertify.alert('Alert!!', 'กรุณาเปลี่ยนรหัสผ่าน เนื่องจากรหัสผ่านของคุณมีอายุการใช้งานเกิน 45 วันแล้ว!', function(){ 
						//alertify.success('Ok'); 
						window.location.href='../user/change_password.php?usr='+res['usrID'];
					});
				}
			}
		});
		
			{% block domReady %}{% endblock %}
     	    {% block domReady2 %}{% endblock %} 
		});
	   {% block javaScript %}
	   {% endblock %}

	    //window.onbeforeunload = function (e) {
        //            e = e || window.event;
        //            // For IE and Firefox prior to version 4
        //            if (e) {
        //              // e.returnValue = 'ท่านต้องการปิดหน้านี้หรือไม่';
        //              if(confirm("ท่านต้องการปิดหน้านี้หรือไม่")) {
        //                 location.href = '../user/logout.php';
        //              }
        //            }
        //            // For Safari
        //        //return 'ท่านต้องการปิดหน้านี้หรือไม่';
        //       // if(confirm("ท่านต้องการปิดหน้านี้หรือไม่")) {
        //              // location.href = '../user/logout.php';
        //       // }
        //};
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116752839-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-116752839-1');
	</script>

	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
</body>
</html>

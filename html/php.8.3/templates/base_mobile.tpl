<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  {% block head %}
    <title>{% block title %}Pivot{% endblock %}</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    {% block cssImport %}{% endblock %} 
	<script type="text/javascript" src="../scripts/autocomplete/jquery.js"></script>
	<script type="text/javascript" src="../scripts/autocomplete/jquery.autocomplete.js"></script>
	<script type="text/javascript" src="../scripts/thickbox.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../themes/autocomplete/jquery.autocomplete.css" />
	<link href="../themes/thickbox.css" rel="stylesheet" type="text/css" /> 
    <link href="../themes/styles_mobile.css" rel="stylesheet" type="text/css" />    
	
    {% block scriptImport %}{% endblock %}     
    <script type="text/javascript">
		{% block domReady %}{% endblock %} 
		{% block javaScript %}{% endblock %}     
    </script> 
  {% endblock %} 
  <style type="text/css">
  {% block styleReady %}{% endblock %}
  </style>
</head>
<body>
	<div id="page">
		<div id="content">
			<div id="ctname">
				{% block topContent %}{% endblock %}
			</div>
			{% block body %}{% endblock %}
			{% block bottomContent %}{% endblock %}
		</div>
	</div>
</body>
</html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      font-family: 'Tahoma';
	//font-size: 14px

    }
	.border_r{
		border-right: solid;
		border-bottom: solid;
		border-width: 0.5px;
	}

    .page {
        background: white;
                width: 29.7cm;
                height: 21cm;
                display: block;
                margin: 0 auto 0.5cm;
                border: 1px solid white;
                font-size: 10px;
      }  
.page_doc{
	//border-right: solid;
	//border-width: 0.5px;
	position:relative;
	height:49%;
	padding-top:10px;
	overflow: hidden;
}
.text_p{
	margin-bottom:0px;
	font-size: 12px;
}
.span_code{
	border-style: solid;
	border-width: 2px;
	padding: 10px;
	 position:absolute;
    right: 20;
	top: 0;
	
}
.boxsub_rihth{
	
}
.box_left{
	//float:left;
	overflow: hidden;
	word-wrap:break-word;
	max-width:100%;
}
.box_rihth{
    position:absolute;
    right: 0;
    display: block;
    //border-style: solid;
    padding-right: 20px;
	//border-width: 2px;
	overflow: hidden;
	max-width:100%;
	min-width:50%;
	word-wrap:break-word;
}



@page {size: A4 landscape; }
@media print {
	
	#print_p{
		  display:none;
	}
     body{
	 font-family: 'Tahoma';
	 padding:0px;
	 margin:0px;
	 font-size: 14px;
	 }
      
      .page {
        margin: 0;
        padding: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
		position: relative;
		height: 100%;
		
      }
	  
	  
	  
		.border_r{
			border-right: solid;
			border-width: 0.5px;
		}
		.page_doc{
			//border-right: solid;
			//border-width: 0.5px;
			position:relative;
			
		}
		.text_p{
			margin-bottom:2px;
			font-size: 10px;
		}
		.span_code{
			border-style: solid;
			border-width: 2px;
			padding: 10px;
		}


}
  </style>
</head>

<body>
  <div class="book">
   {% if data|length > 0 %} {% for page in data %}

    <div class="page">
	  <div class="row">
	   {% for td in page.d %}
		  <div class="border_r page_doc col-6">
							<img width="50%"class ="barcode"src="barcode.php?barcode={{td.mr_work_barcode}}&amp;width=400&amp;height=70" align="absmiddle">
							<br>
							<br>
							{% if td.re_mr_branch_code != '' or td.re_mr_department_code != '' %}
								{% if td.mr_type_work_id == 2%}
								<center><span class="span_code">{{td.re_mr_branch_code}}</span></center>
								{% else %}
								<center><span class="span_code">{{td.re_mr_department_code}}</span></center>
								{% endif %}
							{% endif %}
				<div class="box_left">
				<h5>จาก</b></h5>
					<p class="text_p"><b>&nbsp;&nbsp; ชื่อผู้ส่ง : </b>{{td.send_name}}  {{td.send_lname}} </p>  
					<p class="text_p"><b>&nbsp;&nbsp; รหัสสาขา :  </b>{{td.send_mr_branch_code}}</p>
					<p class="text_p"><b>&nbsp;&nbsp; ชื่อสาขา  :  </b>{{td.re_mr_branch_name}}</p>
					<p class="text_p"><b>&nbsp;&nbsp; ชื่ออเอกสาร/จำนวน  :</b>{{td.mr_topic}} / {{td.quty}}</p>  	
					<p class="text_p"><b>&nbsp;&nbsp; หมายเหตุ : </b>{{td.mr_work_remark}}</p>    	
				</div>
				<div class="box_rihth">
				<h5>กรุณาส่ง</b></h5>
					<p class="text_p"><b>&nbsp;&nbsp; ชื่อผู้รับ : </b>{{td.re_name}}  {{td.re_lname}} </p>  
					{% if td.mr_type_work_id == 2%}
					<p class="text_p"><b>&nbsp;&nbsp; รหัสสาขา :  </b>{{td.re_mr_branch_code}}</p>
					<p class="text_p"><b>&nbsp;&nbsp; ชื่อสาขา  :  </b>{{td.re_mr_branch_name}}</p>
					<p class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>{{td.mr_branch_floor}}</p>
					{% else %}
					<p class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>{{td.re_mr_department_code}}</p>
					<p class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>{{td.re_mr_department_name}}</p>
					<p class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>{{td.re_floor}}</p>
					{% endif %}
				</div>	
				<br>
		  </div>
		  {% endfor %}
	  </div>
	  
		
			<div id="print_p">
					<center>
						<button onclick="window.print();">print</button>
					</center>
			</div>	
</div>
{% endfor %}  {% endif %}
</div>
{% if debug != "" %}
<pre>
{{debug}}
</pre>
{% endif %}
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

  <script>
  $(document).ready(function(){
	{{alertt}}
	{% if alertt == ''%}
	 window.print()
	{% endif %} 
  });
  </script>
</body>

</html>
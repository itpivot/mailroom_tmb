{% extends "base_emp_branch.tpl" %}
{% block title %}Pivot- List{% endblock %}
{% block menu_6 %} active {% endblock %}
{% block menu_7_maim %} active {% endblock %}
{% block menu_7_1 %} active {% endblock %}

{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}


{% block styleReady %}
#btn_save:hover{
	color: #FFFFFF;
	background-color: #055d97;

}

#btn_save{
	border-color: #0074c0;
	color: #FFFFFF;
	background-color: #0074c0;
}

#detail_sender_head h4{
	text-align:left;
	color: #006cb7;
	border-bottom: 3px solid #006cb7;
	display: inline;
}

#detail_sender_head {
	border-bottom: 3px solid #eee;
	margin-bottom: 20px;
	margin-top: 20px;
}
#loader{
	  width:100px;
	  height:100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:absolute;
		top:0px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}
{% endblock %}


{% block domReady %}
{{alertt}}
$('#barcode').keydown(function (e){
    if(e.keyCode == 13){
		var barcode =$('#barcode').val();
		var tel_receiver = $('#tel_receiver').val();
       $.ajax({
			dataType: "json",
			method: "POST",
			url: "ajax/ajax_save_receive.php",
			data: { 
				barcode: barcode, 
				tel: tel_receiver ,
				isEndcode  : 'yes' ,
			},
			beforeSend: function( xhr ) {
				$('#bg_loader').show();
			}
		}).done(function( msg ) {
			
			if(msg.status == 200){
				loaddata();
				$( "#succ" ).html('รับเรียบร้อย');
				$( "#succ" ).fadeToggle( "slow", "linear" );
				setTimeout(function(){ 
					$( "#succ" ).fadeToggle( "slow", "linear" );
				}, 3000);
				
			}else{
				alertify.alert('แจ้งเตือน',msg.message); 
			}
			$('#bg_loader').hide();
		});
    }
})

var tbl_data = $('#tableData').DataTable({ 
    "responsive": true,
	 "scrollX": true,
	//"searching": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
	"pageLength": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'btn'},
        {'data': 'mr_status_name'},
        {'data': 'send_work_no'},
        {'data': 'send_name'},
        {'data': 'tnt_tracking_no'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
		{'data': 'update_date'},
        {'data': 'action'}
    ]
});
$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});
loaddata();
//$('#bg_loader').hide();
{% endblock %}
{% block javaScript %}

function click_resave(barcode) {
	var dataall = [];
	var tel_receiver = $('#tel_receiver').val();
	var tbl_data = $('#tableData').DataTable();

	if(barcode == '' ){
		alertify.alert("ข้อมูลไม่ครบถ้วน","ท่านยังไม่เลือกงาน"); 
		return;
	}

	$.ajax({
		dataType: "json",
		method: "POST",
		url: "ajax/ajax_save_receive.php",
		data: { 
			barcode: barcode, 
			tel: tel_receiver 
		},
		beforeSend: function( xhr ) {
			$('#bg_loader').show();
		}
	}).done(function( msg ) {
		
		if(msg.status == 200){
			location.href = "rate_send.php?id="+msg.mr_work_main_id;
		}else{
			alertify.alert('แจ้งเตือน',msg.message); 
		}
		$('#bg_loader').hide();
	});
}

function save_click() {
	var dataall = [];
	var tel_receiver = $('#tel_receiver').val();
	var barcode = $('#barcode').val();
	var tbl_data = $('#tableData').DataTable();

	if(barcode == '' ){
		alertify.alert("ข้อมูลไม่ครบถ้วน","ท่านยังไม่เลือกงาน"); 
		return;
	}

	$.ajax({
	dataType: "json",
	method: "POST",
	url: "ajax/ajax_save_receive.php",
	data: { 
		barcode: barcode, 
		tel: tel_receiver 
	},
	beforeSend: function( xhr ) {
		$('#bg_loader').show();		
	},
	error: function(xhr, error){
		alert('เกิดข้อผิดพลาด !');
		location.reload();
	}
	}).done(function( msg ) {

		if(msg.status == 200){
			loaddata();
			alertify.alert('แจ้งเตือน',msg.message); 
		}else{
			alertify.alert('แจ้งเตือน',msg.message); 
		}

		$('#bg_loader').hide();
	});
}

function onseart_receive() {
	$.ajax({
	dataType: "json",
	method: "POST",
	url: "ajax/ajax_search_receive.php",
	data: { 	
		barcode: $('#seart_barcode').val(),
		status: '' 
	},beforeSend: function( xhr ) {
		$('#bg_loader').show();	
	},error: function(xhr, error){
		alert('เกิดข้อผิดพลาด !');
		location.reload();
	}
	}).done(function( msg ) {
		if(msg.status == 200){
			$('#bg_loader').hide();
			$('#tableData').DataTable().clear().draw();
			$('#tableData').DataTable().rows.add(msg.data).draw();
		}else{
			alertify.alert('แจ้งเตือน',msg.message); 
		}
	});
}
function onseart() {
 var barcode = $('#barcode').val();
 var ch_b = $('#b_'+barcode).val();
 if(ch_b){
	$('#b_'+barcode).prop("checked", true);
	$( "#succ" ).fadeToggle( "slow", "linear" );
		setTimeout(function(){ 
			$( "#succ" ).fadeToggle( "slow", "linear" );
		}, 3000);
 }else{
	$( "#err" ).fadeToggle( "slow", "linear" );
		setTimeout(function(){ 
			$( "#err" ).fadeToggle( "slow", "linear" );
		}, 3000);
 }

}
function loaddata() {
	$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_load_receive_work.php",
	  data: { 	id: '',
				status: '' 
		},
	   beforeSend: function( xhr ) {
			$('#bg_loader').show();	
		},
		error: function(xhr, error){
			alert('เกิดข้อผิดพลาด !');
			//location.reload();
	 }
	}).done(function( msg ) {
		if(msg.status == 200){
			$('#bg_loader').show();
			$('#bg_loader').hide();
			$('#tableData').DataTable().clear().draw();
			$('#tableData').DataTable().rows.add(msg.data).draw();
		}else{
			alertify.alert('แจ้งเตือน',msg.message); 
		}
	  });
}
{% endblock %}	
{% block Content %}

	
			<div class="container-fluid">
			<div class="" style="text-align: center;color:#0074c0;margin-top:20px;">
				 <label><h3><b>รายการเอกสาร</b></h3></label>
			</div>	
			<div class="form-group" style="" id="detail_sender_head">
				 <H4> ค้นหา </H4>
			</div>
				<div class="col-auto">
				  <input type="text" class="form-control mb-3" id="seart_barcode" placeholder="Barcode">
				</div>
				<div class="col-auto">
					<button onclick="onseart_receive();" type="button" class="btn btn-primary mb-2">ค้นหา</button>
					<button onclick="loaddata();" type="button" class="btn btn-secondary mb-2">ยกเลิก</button>
				</div>
				
				
			<div class="form-group" style="" id="detail_sender_head">
				 <H4> รับเอกสาร </H4>
			</div>
			<div class="form-row align-items-center">
				<div class="col-auto">
				  <input type="text" class="form-control mb-3" id="barcode" placeholder="Enter Barcode">
				</div>
				
				<div class="col-auto">
				{#
				  <button onclick="save_click();" type="button" class="btn btn-primary mb-2">บันทึกรับ</button>
				  #}
				</div>
				<div id="err" class="col-md-12 alert alert-danger" style="display:none;"role="alert">
				 ไม่พบข้อมูลงานที่กำลังนำส่ง
				</div>
				<div id="succ"class="col-md-12 alert alert-success " style="display:none;" role="alert">
				 ok!!
				</div>
			 </div>
			<div class="table-responsive">
				<table class="table table-bordered table-hover display responsive no-wrap" id="tableData">
					<thead class="">
						<tr>
						<th>ลำดับ</th>
						<th>Action</th>
						<th>สถานะงาน</th>
						<th>เลขที่เอกสาร</th>
						<th>ชื่อผู้ส่ง</th>
						<th>TNT</th>
						<th>ชื่อผู้รับ</th>
						<th>วันที่สร้างรายการ</th>
						<th>วันที่แก้ไขล่าสุด</th>
						<th>
							รับเอกสารแล้ว
						</th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
			</div>	
			
			
		
			
			
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
					
					</div>	
					
					
					<div class="col-4" style="padding-right:0px">
					</div>	
						
				</div>			
				
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
		</div>
		
		<div id="bg_loader" class="card">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>
		<div id="bog_link">
		</div>		
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

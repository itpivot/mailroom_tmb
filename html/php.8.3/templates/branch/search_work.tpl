{% extends "base_emp_branch.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_2 %} active {% endblock %}

{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">

<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}
{% block styleReady %}
	#btn_search:hover{
		color: #FFFFFF;
		background-color: #055d97;
	
	}
	
	#btn_search{
		border-color: #0074c0;
		color: #FFFFFF;
		background-color: #0074c0;
	}
 
	#btn_clear:hover{
		color: #FFFFFF;
		background-color: #055d97;
	
	}
	
	#detail_sender_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
	}
	
	#detail_sender_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 20px;
	}
	
	#detail_receiver_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
		
		
	}
	
	#detail_receiver_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 40px;
		
	}	
 
	.input-group-addon{
		background-color: #FFFFFF; 
		border: 1px solid #055d97;
	
	}

	.pt-primary {
		color: #03a9fa;
	}

	.pt-cyan {
		color: #00bcd4;
	}

	.pt-indigo {
		color: #5677fc;
	}

	.pt-teal {
		color: #009688;
	}

	.pt-cancel {
		color: #9e9e9e;
	}

	.pt-lemon {
		color: #8bc34a;
	}
	.pt-success {
		color: #259b24;
	}
	
	.pt-dark {
		color: #212121;
	}

	.pt-amber {
		color: #ffc107;
	}

	#tb_work_order tr th,
	#tb_work_order tr td {
		font-size: 14px;
	}
	
#loader{
	  width:100px;
	  height:100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:absolute;
		top:0px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
		display:none;
	}
 
{% endblock %}
{% block domReady %}

{{alertt}}

		$('.input-daterange').datepicker({
			autoclose: true,
			clearBtn: true,
			todayHighlight: true
		});	
		
		
		var table = $("#tb_work_order").DataTable();
		var mr_type_work_id 		= $("#mr_type_work_id").val();
			var barcode 			= $("#barcode").val();
			var status		 		= $("#status_id").val();
			var start_date 			= $("#start_date").val();
			var end_date 			= $("#end_date").val();
			var user_id 			= $("#user_id").val();
			var emp_id 				= $("#emp_id").val();
			var department_id 		= $("#department_id").val();
			var branch_id 			= $("#branch_id").val();
			
			if( $("#emp_data_1").is(':checked')){
				var emp_type = "sender";
			}else{
				var emp_type = "receiver";
			}
			
			if( $("#department_data_1").is(':checked')){
				var department_type = "sender";
			}else{
				var department_type = "receiver";
			}
			
			if( $("#branch_data_1").is(':checked')){
				var branch_type = "sender";
			}else{
				var branch_type = "receiver";
			}
			
			//alert(emp_id);
			//alert(user_id);
			//return;
			
			
				table.destroy();
				table = $("#tb_work_order").DataTable({
					"ajax": {
						"url": "ajax/ajax_search_work_mailroom.php",
						"type": "POST",
						"data": {
						'barcode': barcode,
						'status' :status,		 	
						'mr_type_work_id' : mr_type_work_id,	
						'start_date' : start_date,	
						'end_date' : end_date,		
						'user_id' : user_id,		
						'emp_id' : emp_id,			
						'department_id' : department_id,	
						'branch_id' : branch_id,		
						'emp_type' : emp_type,
						'department_type' : department_type,
						'branch_type' : branch_type
						
						}
					},
				
					'columns': [
					
						{ 'data':'action' },
						{ 'data':'status' },
						{ 'data':'barcode_show' },
						{ 'data':'time_test'},
						{ 'data':'name_receive' },
						{ 'data':'add_receiver' },
						{ 'data':'name_receive2' },
						{ 'data':'add_receiver2' },
						{ 'data':'name_send' },
						{ 'data':'add_sender' },
						{ 'data':'mr_topic' },
						{ 'data':'mr_type_work_name' },
						{ 'data':'mr_work_remark' }
					],
					/* "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						if (aData['scg_status_name'] == "Newly") {
								$(nRow).addClass('hilight');
							}
						else{
							$(nRow).removeClass('hilight');
							}
						}, */
					'scrollCollapse': true
				});
				
				
				
		<!-- Search -->
			$("#btn_search").click(function(){
			var mr_type_work_id 	= $("#mr_type_work_id").val();
			var barcode 			= $("#barcode").val();
			var status		 		= $("#status_id").val();
			var start_date 			= $("#start_date").val();
			var end_date 			= $("#end_date").val();
			var user_id 			= $("#user_id").val();
			var emp_id 				= $("#emp_id").val();
			var department_id 		= $("#department_id").val();
			var branch_id 			= $("#branch_id").val();
			
			if( $("#emp_data_1").is(':checked')){
				var emp_type = "sender";
			}else{
				var emp_type = "receiver";
			}
			
			if( $("#department_data_1").is(':checked')){
				var department_type = "sender";
			}else{
				var department_type = "receiver";
			}
			
			if( $("#branch_data_1").is(':checked')){
				var branch_type = "sender";
			}else{
				var branch_type = "receiver";
			}
			
			//alert(emp_id);
			//alert(user_id);
			//return;
			
			$.ajax({
				dataType: "json",
				method: "POST",
				url: "ajax/ajax_search_work_mailroom.php",
				data: { 	
					'barcode': barcode,
						'status' :status,		 	
						'mr_type_work_id' : mr_type_work_id,	
						'start_date' : start_date,	
						'end_date' : end_date,		
						'user_id' : user_id,		
						'emp_id' : emp_id,			
						'department_id' : department_id,	
						'branch_id' : branch_id,		
						'emp_type' : emp_type,
						'department_type' : department_type,
						'branch_type' : branch_type
				  },
				 beforeSend: function( xhr ) {
					  $('#bg_loader').show();
				  }
			  }).done(function( msg ) {
				  if(msg.status == 200){
					  $('#bg_loader').hide();
					  $('#tb_work_order').DataTable().clear().draw();
					  $('#tb_work_order').DataTable().rows.add(msg.data).draw();
				  }else{
					  alertify.alert('เกิดข้อผิดพลาด',msg.message);
					  return;
				  }
			  });
			});
			<!-- end Search -->
			
			
		$("#btn_clear").click(function() {
			location.reload();
			
		});
		
		$('#department_id').select2({
			placeholder: "เลือกหน่วยงาน",
			theme: 'bootstrap4',
			ajax: {
				url: "ajax/ajax_getdata_department_select.php",
				dataType: "json",
				delay: 250,
				processResults: function (data) {
					return {
						 results : data
					};
				},
				cache: true
			}
		});
		
		
		$('#branch_id').select2({
			theme: 'bootstrap4',
			placeholder: "เลือกสาขา",
			ajax: {
				url: "ajax/ajax_getdata_branch_select.php",
				dataType: "json",
				delay: 250,
				processResults: function (data) {
					return {
						 results : data
					};
				},
				cache: true
			}
		});
			
			
		$('#emp_id').select2({
			placeholder: "เลือกพนักงาน",
			theme: 'bootstrap4',
			
			ajax: {
					url: "ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					type:"post",
					data: function (params) {
					  var query = {
						search: params.term,
						csrf_token: $('#csrf_token').val(),
						q: params.term,
						type: 'public'
					  }

					  // Query parameters will be ?search=[term]&type=public
					  return query;
					},
					processResults: function (data) {
						$('#csrf_token').val(data.token);
						if(data.status == 401){
							location.reload();
						}else if(data.status == 200){
							return {
								 results : data.data
							};
						}else{
							
						}
						
					},
					cache: true
				}
		});
			
			
			
{% endblock %}


{% block javaScript %}
function cancel_order(id){
	alertify.confirm('ยกเลิกการจัดส่ง', 'ท่านต้องการยกเลิกการจัดส่ง', 
	function(){ 
		$.ajax({
		   url : 'ajax/ajax_save_cancel_work.php',
		   dataType : 'json',
		   type : 'POST',
		   data : {
					'id': id,
		   },
		   beforeSend: function( xhr ) {
					
			}
			
		}).done(function(data) {	
			if(data.status == 200){
				alertify.alert('แจ้งเตือน',data.message,function(){
					$("#btn_search").click();
				});
			}
		   
		});
	}, 
	function(){ 
		alertify.error('Cancel')
	});
	
		
	
}
{% endblock %}

{% block Content %}
<input id="csrf_token" type="hidden" value="{{csrf}}">
	<div  class="container-fluid">
		<div class="row">
					<div class="col-md-12" style="">
						<div class="" style="text-align: center; color:#0074c0;margin-top:20px;">
							<label><h3><b>รายการเอกสารนำส่ง</b></h3></label>
						</div>
						<div class="form-group" id="detail_sender_head">
							<h4>ค้นหารายการ</h4>
						</div>	
					</div>
					<div class="col-md-4" style="">
						<div class="form-group">
							<label for="mr_type_work_id">ประเภทการส่ง  : </label>
								<select id= "mr_type_work_id" name="mr_type_work_id" style="width:100%"> 
								  <option value="">เลือกประเภทการส่ง</option>
								  <option value="2">ส่งที่สาขา</option>
								  <option value="3">ส่งที่สำนักงานใหญ่</option>
							</select>
						</div>
						<div class="form-group">
							<label for="barcode">เลขที่เอกสาร :</label>
							<input type="text" class="form-control form-control-sm" id="barcode" placeholder="เลขที่เอกสาร">
						</div>
						<div class="form-group">
							<label for="status_id">สถานะเอกสาร  :</label>
							<select class="form-control-lg" id="status_id" style="width:100%;">
								<option value="" > เลือกสถานะ</option>
									<optgroup label="รับส่งที่สาขา">
									{% for s in status_data2 %}
										<option value="{{ s.mr_status_id }}" > {{ s.mr_status_name }}</option>
									{% endfor %}
									{% for s in status_data3 %}
										<option value="{{ s.mr_status_id }}" > {{ s.mr_status_name }}</option>
									{% endfor %}
								</optgroup>
								<optgroup label="รับส่งสำนักงานใหญ่">
									{% for s in status_data1 %}
										<option value="{{ s.mr_status_id }}" > {{ s.mr_status_name }}</option>
									{% endfor %}
								</optgroup>
							
						</select>
						
						</div>
						<div class="form-group">
							<label for="barcode">วันที่สร้างรายการ ::</label>
							<div class="input-daterange input-group" id="datepicker" data-date-format="yyyy-mm-dd">
								<input type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From date" autocomplete="off"/>
								<label class="input-group-addon" style="border:0px;">ถึง</label>
								<input type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="To date" autocomplete="off"/>
							</div>
						</div>
					</div>
					<div class="col-md-4" style="">
						<div class="form-group">
							<div class="col-md-12" style="">
								พนักงาน  :
							
								<label class="" style="padding-left:0px">
									  <input type="radio" aria-label="Radio button for following text input" name="emp_data" id="emp_data_1" value="1" checked>ผู้ส่ง
								</label>
								<label class="" style="padding-left:0px">
									  <input type="radio" aria-label="Radio button for following text input" name="emp_data" value="2" >ผู้รับ
								</label>:
							</div>
							<div class="col-md-12" style="">
								<select class="form-control-lg" id="emp_id" style="width:100%;">				
								</select>
							</div>
						</div>
						<div class="form-group">
						<div class="col-md-12" style="">
							<div class="col-md-12 text-left" style="">
								หน่วยงาน
							<label style="padding-left:0px">
								  <input type="radio" aria-label="Radio button for following text input" name="department_data" Id="department_data_1" value="1" checked>ผู้ส่ง
							</label>
							<label style="padding-left:0px">
								  <input type="radio" aria-label="Radio button for following text input" name="department_data" value="2">ผู้รับ
							</label>:
							</div>					
						</div>				
						<div class="col-md-12">
							<select class="form-control-lg" id="department_id" style="width:100%;">			
							</select>
						</div>		
						</div>	
						<div class="form-group">	
							<div class="col-md-12" style="">
								<div class="col-m-12 text-left" style="">
									สาขา
									
								<label style="padding-left:0px">
									  <input type="radio" aria-label="Radio button for following text input" name="branch_data" value="1" id="branch_data_1" checked>ผู้ส่ง
								</label>
								<label  style="padding-left:0px">
									  <input type="radio" aria-label="Radio button for following text input" name="branch_data" value="2" >ผู้รับ
								</label>
								</div>
							</div>
							
							<div class="col-md-12">
								<select class="form-control-lg" id="branch_id" style="width:100%;">
													
								</select>
							</div>	
						</div>		
						
						
					</div>
		</div>
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
		
			
			<div class="form-group">
				<div class="row">
					<div class="col-2" style="padding-left:20px">
						<button type="button" class="btn btn-outline-primary btn-block" id="btn_search"  >ค้นหา</button>

					</div>		
					<div class="col-2" style="padding-left:0px">
						<button type="button" class="btn btn-outline-primary btn-block" id="btn_clear"  >ล้าง</button>

					</div>	
						
				</div>			
				
			</div>
			
			
			
			
			
			
			<div class="form-group" id="detail_sender_head">
				 <h4>รายละเอียดผู้รับ</h4>
			</div>	
			
			
			
			<div class="panel panel-default" style="margin-top:50px;">
            <div class="panel-body">
              <div class="table-responsive" style="overflow-x:auto;">
                <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                  <thead>
                    <tr>
						<th>#</th>
						<th>สถานะ</th>
						<th>เลขที่</th>
						<th>วันที่</th>
						<th>ผู้รับ</th>
						<th>หน่วยงาน</th>
						<th>ผู้รับแทน</th>
						<th>หน่วยงานผู้รับแทน</th>
						<th>ผู้ส่ง</th>
						<th>ที่อยู่ผู้ส่ง</th>
						<th>สถานะ</th>
						<th>ชื่อเอกสาร</th>
						<th>ประเภทการส่ง</th>
						<th>หมายเหตุ</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
			
			
				
		
	</div>	
	<div id="bg_loader" class="card">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

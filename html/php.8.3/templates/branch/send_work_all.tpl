{% extends "base_emp_branch.tpl" %}
{% block title %}Pivot- List{% endblock %}
{% block menu_4 %} active {% endblock %}

{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}


{% block styleReady %}
#btn_save:hover{
	color: #FFFFFF;
	background-color: #055d97;

}

#btn_save{
	border-color: #0074c0;
	color: #FFFFFF;
	background-color: #0074c0;
}

#detail_sender_head h4{
	text-align:left;
	color: #006cb7;
	border-bottom: 3px solid #006cb7;
	display: inline;
}

#detail_sender_head {
	border-bottom: 3px solid #eee;
	margin-bottom: 20px;
	margin-top: 20px;
}
#loader{
	  width:100px;
	  height:100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:absolute;
		top:0px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}
{% endblock %}


{% block domReady %}
{{alertt}}
var tbl_data = $('#tableData').DataTable({ 
    "responsive": true,
	"searching": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
	"pageLength": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'send_work_no'},
        {'data': 'send_name'},
        {'data': 'res_depart'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
		{'data': 'mr_status_name'},
        {'data': 'action'}
    ]
});
$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});
loaddata();
$('#history_print').hide();
$('#toggle-sliding').click(function(){
    $('#history_print').slideToggle('slow');
	$.ajax({
	  method: 'POST',
	  dataType: 'json',
	  url: "ajax/ajax_load_history_print.php",
	  data: { 	id: '',
				status: '' 
		},
	   beforeSend: function( xhr ) {
			$('#bg_loader').show();
		}
	}).done(function( msg ) {
		if(msg.status == 200){
			$('#history_print').html(msg.data);
			$('#bg_loader').hide();
		}else{
			alertify.alert('เกิดข้อผิดพลาด',msg.message);
			return;
		}
	});
});
		
{% endblock %}
{% block javaScript %}
function save_click() {
	var dataall = [];
	var tel_receiver = $('#tel_receiver').val();
	var tbl_data = $('#tableData').DataTable();
	 tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		 dataall.push(this.value);
		 
      });
	  //console.log(tel_receiver);
	  if(tel_receiver == '' || tel_receiver == null){
			 setTimeout(function(){ 
				$('#tel_receiver').focus();
			}, 3000);
		 alertify.alert("ข้อผิดพลาด","กรุณาระบุเบอร์โทร",function() {
			 setTimeout(function(){ 
					$('#tel_receiver').focus();
				}, 2000);
		 }); 
		return;
	  }else if(dataall.length < 1){
		 alertify.alert("ข้อผิดพลาด","ท่านยังไม่เลือกงาน"); 
		 return;
	  }
	 // return;
	var newdataall = dataall.join(",");
	//console.log(newdataall);
	//console.log(tel_receiver);
	$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_save_send_work_all.php",
	  data: { 	
			all_id: newdataall, 
			tel: tel_receiver 
		},
	   beforeSend: function( xhr ) {
			$('#bg_loader').show();
		}
	}).done(function( msg ) {
		if(msg['st'] == 'success'){
			loaddata();
			window.open('print_send_work_all.php?id='+msg['re']+'');
			//$('#hid_print').html('<a href="print_send_work_all.php?id='+msg['re']+'" target="_blank">พิมพ์รายการล่าสุด</a>');
			
		}else{
			alertify.alert("ข้อผิดพลาด","เกิดข้อผิดพลาด  "+msg['re']+"  กรุณาลองใหม่" ); 
		}
			$('#bg_loader').hide();
	});
}
function print_click() {
	var dataall = [];
	var tel_receiver = $('#tel_receiver').val();
	var tbl_data = $('#tableData').DataTable();
	 tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		 dataall.push(this.value);
		 
      });
	  //console.log(tel_receiver);
	  if(dataall.length < 1){
		 alertify.alert("ตรวจสอบข้อมูล","ท่านยังไม่เลือกงาน"); 
		 return;
	  }
	 // return;
	var newdataall = dataall.join(",");
		window.open('printcoverpage.php?maim_id='+newdataall+'');
}		
	
function loaddata() {
	$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_load_send_work_all.php",
	  data: { 	id: '',
				status: '' 
		},
	   beforeSend: function( xhr ) {
			$('#bg_loader').show();
		}
	}).done(function( msg ) {
		if(msg.status == 200){
			$('#bg_loader').hide();
			$('#tableData').DataTable().clear().draw();
			$('#tableData').DataTable().rows.add(msg.data).draw();
		}else{
			alertify.alert('เกิดข้อผิดพลาด',msg.message);
			return;
		}
	});
}
{% endblock %}	
{% block Content %}

	
			<div class="container-fluid">
			
			<div class="" style="text-align: center;color:#0074c0;margin-top:20px;">
				 <label><h3><b>รวมรายการเอกสารนำส่ง</b></h3></label>
			</div>	
			<div class="form-group" style="" id="detail_sender_head">
				 <H4>รายการเอกสารนำส่งทั้งหมด </H4>
			</div>
			
			<div class="form-group row">
				<label for="tel_receiver" class="col-sm-4 col-md-3 col-lg-2 col-form-label">ระบุเบอร์โทรติดต่อผู้ส่ง :</label>
				<div class="col-sm-6">
				   <input value="{% if user_data.mr_emp_mobile == '' %}{{user_data.mr_emp_tel}} {% else %}{{user_data.mr_emp_mobile}}{% endif %}" type="text" class="form-control" id="tel_receiver" placeholder="ระบุเบอร์โทรติดต่อ">
				</div>
			  </div>
						
			
			<div class="table-responsive">
				<table class="table table-bordered table-hover display responsive no-wrap" id="tableData">
					<thead class="">
						<tr>
						<th>ลำดับ</th>
						<th>เลขที่เอกสาร</th>
						<th>ชื่อผู้ส่ง</th>
						<th>หน่วยงานรผู้รับ</th>
						<th>ชื่อผู้รับ</th>
						<th>วันที่สร้างรายการ</th>
						<th>Status ล่าสุด</th>
						<th>
							<label class="custom-control custom-checkbox">
								<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description">เลือก</span>
							</label>
						</th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
			</div>
<div id="hid_print">	
{# <a href="print_send_work_all.php?id={{id}}" target="_blank">พิมพ์รายการล่าสุด</a> #}
	<button id="toggle-sliding" target="_blank"  class="btn btn-link">ประวัติใบคุม</button>
</div>		
	<div id="history_print" class="card">
	  
	</div>	
		<div class="form-group">
			<div class="row">
				<div class="col-12 text-center" style="padding-left:0px">
					<button onclick="save_click();" type="button" class="btn btn-outline-primary" id="btn_save"  >บันทึกรายการและพิมพ์ใบคุม</button>
					<button onclick="print_click();" type="button" class="btn btn-outline-primary" id="btn_print"  >พิมพ์ใบปะหน้าซอง</button>
				</div>
			</div>	
		</div>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	</div>
		
 <div id="bg_loader" class="card">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>
<div id="bog_link">
</div>	

{% endblock %}


{% block debug %} 
	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

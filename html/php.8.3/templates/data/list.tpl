{% extends "base.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block scriptImport %}
	
	<script type="text/javascript" src="../themes/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="../themes/js//jquery-ui-1.8.10.offset.datepicker.min.js"></script>
	<link type="text/css" href="../themes/css/jquery-ui-1.8.10.custom.css" rel="stylesheet" />	
{% endblock %}

{% block styleReady %}
	
	.menu_div{
		height : 50px;
		width:30%;
		border: 1px solid gray;
		line-height: 50px;
		text-align : left;
		padding-left : 20px;
		font-size: 22px; 
		//color: #bbbbbb;
		margin : 5px;
	}		
	
	#list{
		padding-top : 40px;
		padding-bottom : 40px;
	}
	
	.menu_div:focus{
		background-color:#ffff99;
		font-weight:bold;
		color : black;
	}
	
	.menu_div:hover{
		cursor : pointer;
	}
	
	
	.collector_code{
		width:35px;
	}
	
	select {
    -webkit-appearance: menulist;
    box-sizing: border-box;
    align-items: center;
    border-image-source: initial;
    border-image-slice: initial;
    border-image-width: initial;
    border-image-outset: initial;
    border-image-repeat: initial;
    white-space: pre;
    -webkit-rtl-ordering: logical;
    color: black;
    background-color: white;
    cursor: default;
    border-width: 1px;
    border-style: solid;
    border-color: initial;
	
{% endblock %}

{% block javaScript %}

	window.onload = function() 
	{
		document.getElementById("menu1").focus();
	};
	
	function keyEvent(evt, item)
	{
		if(window.event)
		   var key = evt.keyCode;
		else if(evt.which)
		   var key = evt.which;
		   
		console.log(  "user pressed "+String.fromCharCode(key)+" ("+key+")" );
		
		if( key == 40 ){
			item += 1;
			document.getElementById("menu"+item).focus();
		}else if( key == 38 ){
			item -= 1;
			document.getElementById("menu"+item).focus();
		}else if( key == 13 ){
			if( item == 1 ){
				location.replace("../data/list_type.php");
			}else	if( item == 2 ){
				location.replace("../print/list_type.php");
			}else	if( item == 3 ){
				location.replace("../report/list_report.php");
			}else	if( item == 4 ){
				location.replace("../data/list_edit.php");
			}
		}
	}
	
	function nextPage( item ){
		if( item == 1 ){
			location.replace("../employee/add_works.php");
		}else	if( item == 2 ){
			location.replace("../print/list_type.php");
		}else	if( item == 3 ){
			location.replace("../report/list_report.php");
		}else	if( item == 4 ){
			location.replace("../data/list_edit.php");
		}
	}
	
{% endblock %} 

{% block topContent %}เมนู {% endblock %}

{% block body %}
	<div id="list">	
		
	</div>	
{% endblock %}


{% block Content %}
  
	<div class="starter-template">
		<h1>ยินดีต้อนรับ {{ userName }}</h1>
		<p class="lead">{{ date_Today }}</p>
	</div>
	
	<div class="row">
		<div class="col-md-12" >
				<center>
				<div id="menu1" class="menu_div" onkeyup ="keyEvent(event, 1);" onclick="nextPage(1);" tabindex="1">
					1. สั่งงาน
				</div>
				<div id="menu2" class="menu_div"  onkeyup ="keyEvent(event, 2);" onclick="nextPage(2);" tabindex="1" >
					2. พิมพ์ใบคุมงาน / ใบสั่งงาน
				</div>
				<div id="menu3" class="menu_div"  onkeyup ="keyEvent(event, 3);" onclick="nextPage(3);" tabindex="1">
					3. รายงาน
				</div>
				<div id="menu4" class="menu_div"  onkeyup ="keyEvent(event, 4);" onclick="nextPage(4);" tabindex="1">
					4. แก้ไขข้อมูล
				</div>
			</center>
		</div>
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
		

		
		
		
		$("#mr_role_id").change(function(){
		    var role_id = $(this).val();
			//alert(role_id);
			alertify.confirm('แก้ไขสถานที่ปฏิบัติงาน','ยืนยันการเปลี่ยนสถานที่ปฏิบัติงาน', function(){ 
				//alertify.alert("ok");
				$.post('./ajax/ajax_UpdateUserRole.php',{role_id:role_id},
					function(res) {							//console.log(res)
							if ( res == '' ){
								alertify.alert('',"บันทึกสำเร็จ",function(){window.location.reload();});
							}else{
								alertify.alert('error',"บันทึกไม่สำเร็จ  "+res,function(){window.location.reload();});
								
							}});
						},function(res) {							//console.log(res)
							
						
			});
		});
		$("#btn_save").click(function(){
			var pass_emp 			= $("#pass_emp").val();
			var name 				= $("#name").val();
			var last_name 			= $("#last_name").val();
			var tel 				= $("#tel").val();
			var tel_mobile 			= $("#tel_mobile").val();
			var email 				= $("#email").val();
			var emp_id 				= $("#emp_id").val();
			var user_id 			= $("#user_id").val();
			var department 			= $("#department").val();
			var floor 				= $("#floor").val();
			var	status 				= true;
			$('.myErrorClass').removeClass( "myErrorClass" );
				if( pass_emp == "" || pass_emp == null){
					$('#pass_emp').addClass( "myErrorClass" );
					status = false;
				}else{
				}
				
				if(	name == "" || name == null){
					status = false;
					$('#name').addClass( "myErrorClass" );
				}else{

				}
				
				if( last_name == "" || last_name == null){
					status = false;
					$('#last_name ').addClass( "myErrorClass" );
				}else{
				}
				
				if( email == "" || email == null){
					status = false;
					$('#email ').addClass( "myErrorClass" );
				}else{

				}
				
				if( floor == "" || floor == null){
					status = false;
					$('#select2-floor-container').addClass( "myErrorClass" );
				}else{
				}

				if( department == 457 || department == "" || department == null){
					status = false;
					$('#select2-department-container').addClass( "myErrorClass" );
				}else{
				}
				
				
				//console.log(floor);
				
				if( status === true ){
					alertify.confirm('แก้ไขข้อมูล','ยืนยันการแก้ไขข้อมูล', function(){ 
						


							$.ajax({
								dataType: "json",
								method: "POST",
								url: "./ajax/ajax_update_emp.php",
								data: { 
									pass_emp	 : pass_emp,
									user_id		 : user_id,
									emp_id		 : emp_id,
									name		 : name,		
									last_name 	 : last_name,
									tel 		 : tel,		
									tel_mobile 	 : tel_mobile,
									email 		 : email,
									department 	 : department,
									csrf_token	 : $('#csrf_token').val(),
									floor 		 : floor
								 }
							  }).done(function(data1){
								  if(data1['status'] == 200){
										alertify.alert('บันทึกสำเร็จ',data1['message'],
											function(){
												window.location.reload();
											}
										);
								  }else{
									alertify.alert('เกิดข้อผิดพลาด',data1['message'],
										function(){
											window.location.reload();
										}
									);
								  }
							  });
					},function(){});
				}else{
					alert('กรุณากรอกข้อมูลให้ครบถ้วน');
				}					
				
			
		});
		
		



		
$('#name_emp').select2({
				dropdownParent: $('#editCon'),
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "../branch/ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			$.ajax({
			  dataType: "json",
			  method: "POST",
			  url: "../branch/ajax/ajax_getdataemployee.php",
			  data: { employee_id : $('#name_emp').val() }
			}).done(function(data1){
				//alert('55555');
				$('#con_emp_tel').val(data1['mr_emp_tel']);
				$('#mr_emp_code').val(data1['mr_emp_code']);
				$('#con_emp_name').val(data1['mr_emp_name']+'  '+data1['mr_emp_lastname']);
				//console.log(data1);
				//console.log($('#name_emp').val());
			});
			
		});

		
		
			
$( "#btnCon_save" ).click(function() {
										
					var con_remark 			= $('#con_remark').val();
					var con_floor 			= $('#con_floor').val();
					var con_department_name 			= $('#con_department_name').val();
					var mr_emp_tel 			= $('#con_emp_tel').val();
					var con_emp_name 		= $('#con_emp_name').val();
					var mr_emp_code 		= $('#mr_emp_code').val();
					var mr_emp_id 			= $('#name_emp').val();
					var department_code 		= $('#department_code').val();
					var mr_contact_id 		= $('#mr_contact_id').val();
					var type_add 		= $('#type_add').val();
					
					
					$( ".myErrorClass" ).removeClass( "myErrorClass" );
					
					if(mr_emp_id == ''){
						$( "#name_emp" ).addClass( "myErrorClass" );
						alertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
						return;
					}if(mr_emp_code == ''){
						$( "#mr_emp_code" ).addClass( "myErrorClass" );
						alertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
						return;
					}if(mr_emp_tel == ''){
						$( "#mr_emp_tel" ).addClass( "myErrorClass" );
						alertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
						return;
					}
					
	alertify.confirm("ตรวจสอบข้อมูล", "กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก",
			function(){
				$.ajax({
						  dataType: "json",
						  method: "POST",
						  url: "ajax/ajax_save_data_contact.php",
						  data: { 
								department_code 			 	 : department_code 			 	,
								con_emp_name 			 	 : con_emp_name 			 	,
								con_remark 			 	 : con_remark 			 	,
								con_floor 			 	 : con_floor 			 	,
								con_department_name 	 : con_department_name 	,
								type_add 	 : type_add 	,
								mr_emp_tel 	 : mr_emp_tel 	,
								mr_emp_code  : mr_emp_code ,
								mr_emp_id 	 : mr_emp_id 	,
								mr_contact_id: mr_contact_id
						  }
						}).done(function(data) {
							$('#csrf_token').val(data.token);

							alertify.alert(data['st'],data['msg'],
							function(){
								window.location.reload();
							});
						});
		  },function(){
					alertify.error('Cancel');
	  });
	
	
	//alert( "Handler for .click() called." );
	//is-invalid
});



		
{% endblock %}				
{% block javaScript %}
		
function load_modal(mr_contact_id,type) {	

 $('#editCon').modal({backdrop: 'false'});

$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_save_data_contact.php",
	  data: { 	
				mr_contact_id : mr_contact_id,
				page : 'getdata'
	  }
	}).done(function(data) {
		//alert('55555');
		if(type!= "add"){
			$('#mr_contact_id').val(data['mr_contact_id']);
			
		}
		
		$('#type_add').val(type);
		$('#mr_emp_code').val(data['emp_code']);
		$('#con_department_name').val(data['department_name']);
		$('#con_floor').val(data['floor']);
		$('#con_remark').val(data['remark']);
		$('#department_code').val(data['department_code']);
	});
	
}


	

		
{% endblock %}					
				
{% block styleReady %}

	.btn {
		box-shadow: 0px 0px 0px 0px rgba(0,0,0,.0) !important;
	}
	.modal-backdrop {
		position: relative !important;
	}
	.modal {
		top: 40% !important;
	}
	.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}

{% endblock %}	

{% block Content %}


	
			<div class="form-group" style="text-align: center;">
				 <label><h4> ข้อมูลผู้สั่งงาน </h4></label>
			</div>	
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="{{ user_data.mr_emp_id }}">
			 <input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf}}">

	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน" value="{{ user_data.mr_emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name" placeholder="ชื่อ" value="{{ user_data.mr_emp_name }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="last_name" placeholder="นามสกุล" value="{{ user_data.mr_emp_lastname }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel" placeholder="เบอร์โทรศัพท์" value="{{ user_data.mr_emp_tel }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel_mobile" placeholder="เบอร์มือถือ" value="{{ user_data.mr_emp_mobile }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						Email :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="email" placeholder="email" value="{{ user_data.mr_emp_email }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						แผนก :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="department" style="width:100%;">
							{% for s in department_data %}
								<option value="{{ s.mr_department_id }}" {% if s.mr_department_id == user_data.mr_department_id %} selected="selected" {% endif %}>{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้น :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="floor" style="width:100%;">
							<option value=""> ไม่มีข้อมูลชั้น</option>
							{% for f in floor_data %}
								<option value="{{ f.mr_floor_id }}" {% if f.mr_floor_id == user_data.floor_emp %} selected="selected" {% endif %}>{{ f.name }}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			{% if role_id == 2 or role_id == 5%}
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px;color:red;">
				***สถานที่ปฏิบัติงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="mr_role_id" style="width:100%;">
							<option value="2" {% if role_id == 2 %}selected {% endif %}> สำนักงาน(พหลโยธิน)</option>				
							<option value="5" {% if role_id == 5 %}selected {% endif %}> สาขา/สาขาและอาคารอื่นๆ</option>				
						</select>
					</div>		
				</div>			
			</div>
			{% endif %}
			<br>
			<br>
			<br>
			<br>
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึกการแก้ไข</button>
				<br>
				<center>
					<a href = "../user/change_password.php?usr={{ userID }}"><b> เปลี่ยนรหัสผ่าน คลิกที่นี่</b></a>
				</center>
			</div>
			<br>
			<br>
			<br>
			
			
			{% if con_data|length > 0 %}
						<hr>
						<h4>ข้อมูลหน่วยงานที่อยู่ในความรับผิดชอบรับเอกสาร</h4><br>
							
							<table class="table table-striped" align="center" border="1">
									<tr>
										<th  class="text-center">#</th>
										<th  class="text-center">พนักงาน</th>
										<th  class="text-center">รหัสหน่วยงาน</th>
										<th  class="text-center">ชื่อหน่วยงาน</th>
										<th  class="text-center">จัดการ <button type="button" class="btn btn-link"
													onclick="load_modal('{{con_data.0.mr_contact_id}}','add');">
												 <i class="material-icons">add_circle</i>
											</button></th>
									</tr>
								
								{% for c in con_data %}
									<tr>
										<td>{{ loop.index }}</td>
										<td>{{ c.emp_code }}  {{ c.mr_contact_name }}</td>
										<td>{{ c.department_code }}</td>
										<td>
										   {{         c.department_name  }} [{{         c.remark  }}]   
										</td>
										<td>
											<button {% if c.emp_code  != user_data.mr_emp_code %} disabled {% endif %}
												onclick="load_modal('{{c.mr_contact_id}}',null);" class="btn btn-link" style="text-decoration: underline;">
												<i class="material-icons">create</i>
											</button>
											
										</td>
									</tr>
								
							{% endfor %}
							</table>
						{% endif %}
						
						
						
			<br>
			<br>
			<br>
			<!-- <div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสหน่วยงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_depart" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" value="{{ user_data.mr_department_code }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้น :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="floor" placeholder="ชั้น" value="{{ user_data.mr_department_floor }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อหน่วยงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="depart" placeholder="ชื่อหน่วยงาน" value="{{ user_data.mr_department_name }}">
					</div>		
				</div>			
			</div>
				 -->

	
<!-- Modal -->
<div class="modal fade" id="editCon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div class="form-group">
				<label for="">ชื่อ - นามสกุล</label><br>
				
				<select class="form-control" id="name_emp" style="width:100%;">
				  <option value="">{{con_data.0.emp_code}} : {{con_data.0.mr_contact_name}}</option>
				  {% for emp in all_emp %}
				  <option value="{{ emp.mr_emp_id }}">{{emp.mr_emp_code}} : {{emp.mr_emp_name}}{{emp.mr_emp_lastname}}</option>
				  {% endfor %}
				</select>
				
			  </div>
			 
			  
			 <div class="form-group">
				<label for="">เบอร์โทร</label>
				<input type="text" class="form-control" id="con_emp_tel" value="">
				<input type="hidden" class="form-control" id="type_add" value="">
				<input type="hidden" class="form-control" id="mr_contact_id" value="">
				<input type="hidden" class="form-control" id="mr_emp_code" value="">
				<input type="hidden" class="form-control" id="con_emp_name" value="">
			  </div>
			  <div class="form-group">
				<label for="">รหัสหน่วยงาน</label>
				<input type="text" class="form-control" id="department_code" value="" readonly>
			  </div>
			  <div class="form-group">
				<label for="">ชื่อหน่วยงาน</label>
				<input type="text" class="form-control" id="con_department_name" value="" readonly>
			  </div>
			  <div class="form-group">
				<label for="">ชั้น</label>
				<input type="text" class="form-control" id="con_floor" value=""  readonly>
			  </div>
			  <div class="form-group">
				<label for="">หมายเหตุ</label>
				 <textarea class="form-control" id="con_remark" rows="3"></textarea >
			  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btnCon_save" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
	
				
			
			
		
	
 
	
	
	
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

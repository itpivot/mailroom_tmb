{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
{% endblock %}
{% block styleReady %}
	#btn_save:hover{
		color: #FFFFFF;
		background-color: #055d97;
	
	}
	#modal_click {
		cursor: help;
		color:#46A6FB;
		}
	
	#btn_save{
		border-color: #0074c0;
		color: #FFFFFF;
		background-color: #0074c0;
	}
 
	#detail_sender_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
	}
	
	#detail_sender_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 20px;
	}
	
	#detail_receiver_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
		
		
	}
	
	#detail_receiver_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 40px;
		
	}	
 
	

		.modal-dialog {
			max-width: 2000px; 
			margin: 0rem auto;
		}
 
.valit_error{
	border:2px solid red;
	border-color: red;
	border-radius: 5px;
} 
 
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}
	
	
.disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}

select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}



	
	#modal_click {
		cursor: help;
		color:#46A6FB;
		}

	#detail_sender_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
	}
	
	#detail_sender_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 20px;
	}
	
	#detail_receiver_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
		
		
	}
	
	#detail_receiver_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 40px;
		
	}	
 
	

		.modal-dialog {
			max-width: 2000px; 
			margin: 0rem auto;
		}
 

{% endblock %}

{% block domReady %}
/* $('#bg_loader').show(); */
var from_obj = [];
var from_ch = 0;
 $('[data-toggle="tooltip"]').tooltip()
 {/* 
 $('#tb_con').DataTable();
 var tbl_data = $('#tb_addata').DataTable({ 
    "responsive": true,
	"scrollX": true,
	"searching": true,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
	"pageLength": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'type'},
        {'data': 'name'},
        {'data': 'tel'},
        {'data': 'strdep'},
        {'data': 'floor'},
        {'data': 'doc_title'},
        {'data': 'remark'}
    ]
}); 

*/}


 $(".alert").alert();

		$("#modal_click").click(function(){
			$('#modal_showdata').modal({ backdrop: false})
		})
		$("#btn_save").click(function(){
			var branch_floor = '';
					
					var emp_id 				= $("#emp_id").val();
					var user_id 			= $("#user_id").val();
					var status 				= true;
					if( $("#type_send_1").is(':checked')){
							
						
							var topic 				= $("#topic_head").val();
							var remark				= $("#remark_head").val();
							var destination 		= $("#floor_send").val();
							
							var type_send			= 1;
							//alert(floor_send);
					}else{
							
							var topic 				= $("#topic_branch").val();
							var remark  			= $("#remark_branch").val();
							
						
							var type_send			= 2;
					
					}
				
			if( type_send == 2 ){
				var branch_receiver 		= $.trim($("#branch_receiver").val());
				var branch_send				= $("#branch_send").val();
				    branch_floor				= $("#branch_floor").val();
				
				//console.log(branch_receiver);
				//console.log(branch_send);
				if( branch_receiver == "-" && branch_send == 0 ){
					status = false;
					alertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
					return;
				
				}
			}else{
				if( (destination == "-" || destination == "") && $('#floor_name').val() == 0 ){
					status = false;
					alertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
					return;
				
				}
			}
					if( type_send == 2 ){
						var branch_receiver 		= $.trim($("#branch_receiver").val());
						var destination 			= $("#branch_send").val();
							
						
						//console.log(destination);
						//console.log(branch_send);
						if( branch_receiver == "-" && destination == 0 ){
							status = false;
							alertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลสาขาผู้รับ!');
							return;
						
						}
					}
					
						
						if(emp_id == "" || emp_id == null){
							status = false;
							alertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
							return;
						}
						if(topic == "" || topic == null){
							status = false;
							alertify.alert('ตรวจสอบข้อมูล','กรุณากรอกข้อมูลชื่อเอกสาร !');
							return;
						}
						
						
						
						if( status === true ){
							var obj = {};
							obj = {
								
								emp_id: emp_id,
								user_id: user_id,
								remark: remark,
								topic: topic,
								destination: destination ,
								type_send: 	type_send, 			
								branch_floor: 	branch_floor 			
								
							}
						//console.log(obj);
						//return;
							alertify.confirm('ตรวจสอบข้อมูล',"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง",
								function(){
									saveBranch(obj);   
                                },
								function(){
									alertify.error('ยกเลิก');
								}).set('labels', {ok:'บันทึก', cancel:'ยกเลิก'});
								
									                                                                                  
							                 
							//$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});                              
							                                                                                          
						}                                                                                             
			
		});
		
		
		$('#branch_send').select2({
							placeholder: "ค้นหาสาขา",
							ajax: {
								url: "../branch/ajax/ajax_getdata_branch_select.php",
								dataType: "json",
								delay: 250,
								processResults: function (data) {
									return {
										 results : data
									};
								},
								cache: true
							}
						});
		
		$('#name_receiver_select').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "../branch/ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setForm(e.params.data);
		});
		
		$('.select_name').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "../branch/ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setForm(e.params.data);
		});


$('#bg_loader').hide(); 
$('#div_error').hide();		
{% endblock %}	



{% block javaScript %}

		$('#type_send_1').prop('checked',true);
		
		function check_type_send( type_send ){
			 if( type_send == 1 ){ 
				$("#type_1").show();
				$("#type_2").hide();
			 }else{
				$("#type_1").hide();
				$("#type_2").show();
			 }
		}
		
		function getemp(emp_code) {
			$.ajax({
					url: 'ajax/ajax_get_empID_bycode.php',
					type: 'POST',
					data: {
						emp_code: emp_code
					},
					dataType: 'json',
					success: function(res) {
						$('#modal_showdata').modal('hide');
					if(res.length > 0){
						//console.log(res);
						var obj = {};
							obj = {
								id: $.trim(res[0]['mr_emp_id'])
							}
							setForm(obj);
							$('#name_receiver_select').html('');
							$('#name_receiver_select').append('<option value="'+$.trim(res[0]['mr_emp_id'])+'">'+res[0]['mr_emp_code']+':'+res[0]['mr_emp_name']+'  '+res[0]['mr_emp_lastname']+'</option>');
							
						}
					}
				})
		}
		
		function setForm(data) {
				var emp_id = parseInt(data.id);
				var fullname = data.text;
				$.ajax({
					url: 'ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						if(res.status == 200) {
							if(res.data != false) {
								$("#depart_receiver").val(res['data']['department']);
								$("#emp_id").val(res['data']['mr_emp_id']);
								$("#tel_receiver").val(res['data']['mr_emp_tel']);
								$("#place_receiver").val(res['data']['mr_workarea']);
								$("#floor_name").val(res['data']['mr_department_floor']);
								$("#branch_receiver").val(res['data']['branch']);
								$('#branch_send').html('');
								$('#branch_send').append('<option value="'+$.trim(res['data']['mr_branch_id'])+'">'+res['data']['mr_branch_code']+':'+res['data']['mr_branch_name']+'</option>');		
							}
						}
					}
				})
		}
		function setrowdata(id,eli_name) {
			var emp_id = $('#emp_'+eli_name).val();
			//</link>console.log('>>>>>>>>'+eli_name+'>>>>>>'+emp_id)
				$.ajax({
					url: '../branch/ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						if(res.status == 200) {
							if(res.data != false) {
								if($('#val_type_'+eli_name).val() == 1){
									$("#floor_"+eli_name).val(res['data']['mr_floor_id']).trigger("change")
									$("#strdep_"+eli_name).html('');
									$("#strdep_"+eli_name).append('<option value="'+$.trim(res['data']['mr_department_id'])+'">'+res['data']['department']+'</option>');
									$("#strdep_"+eli_name).attr({'readonly': 'readonly'}).trigger('change');
									//console.log('555');
								}else{
									$("#strdep_"+eli_name).html('');
									$("#strdep_"+eli_name).append('<option value="'+$.trim(res['data']['mr_branch_id'])+'">'+res['data']['branch']+'</option>');
									$("#strdep_"+eli_name).removeAttr('readonly').trigger('change');
								}
								$("#tel_"+eli_name).val(res['data']['department']);
								ch_fromdata()
							}	
						}
					}
				})
		}
		
		
		
	

		var saveBranch = function(obj) {
			//return $.post('ajax/ajax_save_work_branch.php', obj);
			$.ajax({
					url: 'ajax/ajax_save_work_branch.php',
					type: 'POST',
					data: obj,
					dataType: 'json',
					success: function(res) {
						if(res) {
										var msg = '<h1><font color="red" ><b>' + res['barcodeok'] + '<b></font></h1>';
										alertify.confirm('ตรวจสอบข้อมูล','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\n'+ msg, 
											function(){
												setTimeout(function(){ 
															$("#name_receiver").val('');
															$("#pass_depart").val('');
															$("#floor").val('');
															$("#depart").val('');
															$("#pass_emp").val('');
															$("#topic").val('');
															$("#remark").val('');
															$("#floor_send").val(0).trigger('change');
															$("#name_receiver_select").val(0).trigger('change');
														}, 500);
														
														
												    alertify.success('print');
													//window.location.href="../branch/printcoverpage.php?maim_id="+res['work_main_id'];
													var url ="../branch/printcoverpage.php?maim_id="+res['work_main_id'];
													window.open(url);
											},function() {
												$("#emp_id").val('');
												$("#topic_head").val('');
												$("#topic_branch").val('');
												$("#remark_head").val('');
												$("#remark_branch").val('');
												$("#tel_receiver").val('');
												$("#floor_name").val('');
												$("#depart_receiver").val('');
												$("#name_receiver_select").val(0).trigger('change');
												$("#branch_send").val(0).trigger('change');
												$("#floor_send").val(0).trigger('change');
												alertify.success('save');
												//alertify.confirm('ตรวจสอบข้อมูล',"This is a confirm dialog.",
												//  function(){
												//	//alertify.success('print');
												//  },
												//  function(){
												//	//alertify.error('OK');
												//  }).set('labels', {ok:'Submit', cancel:'Cancel'});
												
										}).set('labels', {ok:'ปริ้นใบปะหน้า', cancel:'ตกลง'});

			
										                                                                              
									} 
					}
				})
			
		}
		
		function import_excel() {
			$('#div_error').hide();	
			var formData = new FormData();
			formData.append('file', $('#file')[0].files[0]);
			if($('#file').val() == ''){
				$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
				$('#div_error').show();
				return;
			}else{
			 var extension = $('#file').val().replace(/^.*\./, '');
			 if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
				 $('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
				$('#div_error').show();
				// console.log(extension);
				return;
			 }
			}
			//console.log(formData);
			$.ajax({
				   url : 'ajax/ajax_readFile_excel_work.php',
				   dataType : 'json',
				   type : 'POST',
				   data : formData,
				   processData: false,  // tell jQuery not to process the data
				   contentType: false,  // tell jQuery not to set contentType
				   success : function(data) {
					    // $('#tb_addata').DataTable().clear().draw();
						// $('#tb_addata').DataTable().rows.add(data).draw();
						var txt_html ='';
						$.each( data, function( key, value ) {
							 txt_html += '<tr>';
							 txt_html += '<td>'+value['no']+'</td>';
							 txt_html += '<td>'+value['type']+'</td>';
							 txt_html += '<td>'+value['name']+'</td>';
							 txt_html += '<td>'+value['tel']+'</td>';
							 txt_html += '<td>'+value['strdep']+'</td>';
							 txt_html += '<td>'+value['floor']+'</td>';
							 txt_html += '<td>'+value['doc_title']+'</td>';
							 txt_html += '<td>'+value['remark']+'</td>';
							 txt_html += '</tr>';
						  });

						$('#t_data').html(txt_html);
						$('.select_floor').select2().on('select2:select', function(e) {
							 ch_fromdata()

						});
						$('.select_type').select2().on('select2:select', function(e) {
							 var name = $(this).attr( "name" );
							 var v_al = $(this).val();

							 var myarr = name.split("_");
							 var extension = myarr[(myarr.length)-1]
							 
							 setrowdata(v_al,extension)
							 if(v_al == 2){
								  $('#floor_'+extension).html('');
								  $('#floor_'+extension).html($('#branch_floor').html());
							 }else{
								 $('#floor_'+extension).html('');
								 $('#floor_'+extension).html($('#floor_send').html());
								
								 
							 }
							 ch_fromdata()
							// console.log('floor_'+extension)
						});
						
						$('.select_name').select2({
							placeholder: "ค้นหาผู้รับ",
							ajax: {
								url: "ajax/ajax_getdataemployee_select_file_import.php",
								dataType: "json",
								delay: 250,
								processResults: function (data) {
									return {
										 results : data
									};
								},
								cache: true
							}
						}).on('select2:select', function(e) {
							var name = $(this).attr( "name" );
							var v_al = $(this).val();
							
							var myarr = name.split("_");
							var extension = myarr[(myarr.length)-1]
							 
							//var extension = name.replace(/^.*\./, '');
							setrowdata(v_al,extension)
							 //console.log(extension)
							 ch_fromdata()
						});
						$('.strdep').select2({
							placeholder: "ค้นหาสาขา",
							ajax: {
								url: "../branch/ajax/ajax_getdata_branch_select.php",
								dataType: "json",
								delay: 250,
								processResults: function (data) {
									return {
										 results : data
									};
								},
								cache: true
							}
						}).on('select2:select', function(e) {
							//var name = $(this).attr( "name" );
							//var v_al = $(this).val();
							//var extension = name.replace(/^.*\./, '');
							//setrowdata(v_al,extension)
							// //console.log(extension)
							 ch_fromdata()
						});
					  // console.log(data);
					   from_obj = data;
					   ch_fromdata()
					  // alert(data);
					   $('#bg_loader').hide();
				   }, beforeSend: function( xhr ) {
						$('#bg_loader').show();
						
					},
					error: function (error) {
						alert("sesstion หมดอายุ  กรุณา Login");
						//location.reload();
					}
				});
		
		}
		
		function ch_fromdata() {
		from_ch=0;
		$('#div_error').hide();
		$('#div_error').html('กรุณาตรวจสอบข้อมูล !!');
			$.each( from_obj, function( key, value ) {
				if($('#val_type_'+value['in']).val() == "" || $('#val_type_'+value['in']).val() == null){
					$('#type_error_'+value['in']).addClass('valit_error');
					$('#div_error').show();
					from_ch+=1;
				}else{
					$('#type_error_'+value['in']).removeClass('valit_error');
					
				}
				if($('#emp_'+value['in']).val() == "" || $('#emp_'+value['in']).val() == null){
					$('#name_error_'+value['in']).addClass('valit_error');
					$('#div_error').show();
					from_ch+=1;
				}else{
					$('#name_error_'+value['in']).removeClass('valit_error');
					
				}
				if($('#floor_'+value['in']).val() == "" || $('#floor_'+value['in']).val() == null || $('#floor_'+value['in']).val() == 0){
					$('#floor_error_'+value['in']).addClass('valit_error');
					$('#div_error').show();
					from_ch+=1;
				}else{
					$('#floor_error_'+value['in']).removeClass('valit_error');
				
				}
				
				if($('#title_'+value['in']).val() == "" || $('#title_'+value['in']).val() == null || $('#title_'+value['in']).val() == 0){
					$('#title_'+value['in']).addClass('valit_error');
					$('#div_error').show();
					from_ch+=1;
				}else{
					$('#title_'+value['in']).removeClass('valit_error');
				
				}
				if($('#val_type_'+value['in']).val() != 1){
					if($('#strdep_'+value['in']).val() == "" || $('#strdep_'+value['in']).val() == null || $('#strdep_'+value['in']).val() == 0){
						$('#dep_error_'+value['in']).addClass('valit_error');
						$('#div_error').show();
						from_ch+=1;
					}else{
						$('#dep_error_'+value['in']).removeClass('valit_error');
					
					}
				}else{
					$('#dep_error_'+value['in']).removeClass('valit_error');
				}
				
			
				//console.log('<<<<<<<<<<<<<>>>>>>>>>>>>>>>>');
			});
		}
		function start_save() {
			$('#div_error').hide();
			if( from_ch > 0 ){
				$('#div_error').show();
				$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล !! ');
				return;
			}
			if( from_obj.length < 1 ){
				$('#div_error').show();
				$('#div_error').html('ข้อมูลไม่ครบถ้วน กรุณาตรวจสอบข้อมูล  !! ');
				return;
			}
			var adll_data = $('#form_import_excel').serializeArray()
			var count_data = from_obj.length;
			data = JSON.stringify(adll_data);
			// console.log(adll_data);
			// console.log(from_obj.length);
			$.ajax({
					url: 'ajax/ajax_save_work_import.php?count='+count_data,
					type: 'POST',
					data: {
						'data_all' : data,
						'csrf_token' : $('#csrf_token').val(),
				},
					dataType: 'json',
					beforeSend: function( xhr ) {
						$('#bg_loader').show();
						
					},
					success: function(res) {
						if(res.status == 401){
							$('#csrf_token').val(res['token']);
							alert(res['message']);
							//window.location.reload();
							
						}else{
						//console.log(res);
							$('#bg_loader').hide();
							alertify.confirm('บันทึกข้อมูลสำเร็จ','ท่านต้องการปริ้นใบปะหน้าซองหรือไม่', 
									function(){
										//window.location.href="../branch/printcoverpage.php?maim_id="+res['arr_id']; 
										setTimeout(function(){ 
											window.location.reload();
										}, 3000);
										var url ="../branch/printcoverpage.php?maim_id="+res['arr_id']; 
										window.open(url,'_blank');
									},function() {
										window.location.reload();
								}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'ไม่'});
						}
					},
					error: function (error) {
					 alert('เกิดข้อผิดพลาด !!!');
					 window.location.reload();
					}
				})
			
		}
			

function dowload_excel(){
		alertify.confirm('Download Excel','คุณต้องการ Download Excel file', 
				function(){
					//window.location.href='../themes/TMBexcelimport.xlsx';
					window.open("../themes/TMBexcelimport.xlsx", "_blank");
				},function() {
			}).set('labels', {ok:'ตกลง', cancel:'ยกเลิก'});

}			
{% endblock %}
{% block Content2 %}

<div  class="container-fluid">
			<div class="" style="text-align: center;color:#0074c0;margin-top:20px;">
				 <label><h3><b>บันทึกรายการนำส่ง</b></h3></label>
			</div>	
			
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="">
			 <input type="hidden" id="barcode" value="{{ barcode }}">
			
		<!-- 	<div class="" style="text-align: left;">
				 <label>เลขที่เอกสาร :  -</label>
			</div>	 -->
			
				<div class="form-group" style="" id="detail_sender_head">
					 <h4>รายละเอียดผู้ส่ง </h4>
				</div>	
					<div class="row row justify-content-sm-center">
						<div class="col-sm-6" style="padding-right:0px">
							<div class="form-group">
								<label for="name_receiver">สาขาผู้ส่ง :</label>
								<input type="text" class="form-control form-control-sm" id="name_receiver" placeholder="ชื่อสาขา" value="{{ user_data_show.mr_branch_code }} - {{ user_data_show.mr_branch_name }}" disabled>

							</div>
						</div>	
		
						<div class="col-sm-6" style="padding-right:0px">
							<div class="form-group">
								<label for="mr_emp_tel">เบอร์โทร :</label>
								<input type="text" class="form-control form-control-sm" id="mr_emp_tel" placeholder="เบอร์โทร" value="{{  user_data_show.mr_emp_tel  }}" readonly>
							</div>
						</div>

						
						
					</div>
				
				
				<div class="" id="div_re_select">
					<div class="row">
						<div class="col-sm-6" style="padding-right:0px">
							<div class="form-group">
								<label for="name_receiver">ชื่อผู้ส่ง  :</label>
								<input type="text" class="form-control form-control-sm" id="name_receiver"  value="{{  user_data_show.mr_emp_code  }} - {{  user_data_show.mr_emp_name  }} {{  user_data_show.mr_emp_lastname  }}" disabled>
							</div>
						</div>	
						
						<div class="col-6" style="padding-left:0px">
						</div>			
					</div>			
							
				</div>
		
		<!-- ----------------------------------------------------------------------- 	รายละเอียดผู้รับ -->

		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  {#<li class="nav-item">
			<a class="nav-link" id="home-tab" data-toggle="tab" href="#page_create_work" role="tab" aria-controls="tab_1" aria-selected="true">สร้างรายการนำส่ง</a>
		  </li>#}
		  <li class="nav-item">
			<a class="nav-link active" id="profile-tab" data-toggle="tab" href="#page_inport_excel" role="tab" aria-controls="tab_2" aria-selected="false">Import Excel</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" onclick="dowload_excel();">Download Templates Excel</a>
		  </li>
		</ul>
	<div class="tab-content" id="myTabContent">
	<div id="page_inport_excel" class="tab-pane fade show active tab-pane fade" aria-labelledby="tab2-tab" role="tabpanel">
		<form id="form_import_excel">
			<div class="form-group">
			<a onclick="$('#modal_showdata').modal({ backdrop: false});" data-toggle="tooltip" data-placement="top" title="เลือกไฟล์ Excel ของท่าน"><i class="material-icons">help</i></a>
				<label for="file">
					<div class="form-group" style="" id="detail_receiver_head">
						<h4>Import File </h4>  
					</div>
				</label>
				<input type="file" class="form-control-file" id="file">
			</div>
	<br>
			<button onclick="import_excel();" id="btn_fileUpload" type="button" class="btn btn-warning">Upload</button>
			<button onclick="start_save()" type="button" class="btn btn-success">&nbsp;Save&nbsp;</button>
			<input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf}}"></input>

			<br>
			<hr>
			<br>
			<div id="div_error"class="alert alert-danger" role="alert">
			  A simple danger alert—check it out!
			</div>
	
		
			<div class="table table-responsive">
			<table id="tb_addata" class="table display nowrap table-striped table-bordered">
				  <thead>
					<tr>
					  <th scope="col">No</th>
					  <th scope="col">ประเภทการส่ง</th>
					  <th scope="col">ผู้รับงาน</th>
					  <th scope="col">ตำแหน่ง</th>
					  <th scope="col">แผนก</th>
					  <th scope="col">ชั้น</th>
					  <th scope="col">ชื่อเอกสาร</th>
					  <th scope="col">หมายเหตุ</th>
					</tr>
				  </thead>
				  <tbody id="t_data">
					
				  </tbody>
				</table>
				</div>
			
		</form>
	<br>
	<br>
	<br>
	<br>
	</div>		

	<div id="page_create_work" class="tab-pane fade" aria-labelledby="tab1-tab" role="tabpanel">
			<div class="form-group" style="" id="detail_receiver_head">
				 <h4>รายละเอียดผู้รับ</h4>  <a id="modal_click" data-toggle="tooltip" data-placement="top" title="ตรวจสอบชื่อผู้รับตามหน่วยงาน"><i class="material-icons">help</i></a>
			</div>
			
			<div class="" id="div_re_text">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="name_receiver">ประเภทการส่ง  :</label>
							<label class="btn btn-primary"for="type_send_1">
								<input type="radio" aria-label="Radio button for following text input" name="type_send" id="type_send_1" value="1" onclick="check_type_send('1');"> &nbsp;
								ส่งที่สำนักงานใหญ่
							</label>
							<label class="btn btn-primary"for="type_send_2">
								<input type="radio" aria-label="Radio button for following text input" name="type_send" id="type_send_2" value="2" onclick="check_type_send('2');"> &nbsp;
								ส่งที่สาขา
							</label>
						</div>	
					</div>	
					<!-- <div class="col-1" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name_receiver" placeholder="รหัสพนักงาน">
					</div>	 -->
						
					
					
				</div>			
			</div>
			
					
			<div class="" id="">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="name_receiver_select">ค้นหาผู้รับ  :<span style="color:red;">*</span></label>
								<select class="form-control-lg" id="name_receiver_select" style="width:100%;" >
								</select>
							</div>
							 
						</div>	
						<div class="col-sm-6">
							<div class="form-group">
								<label for="tel_receiver">เบอร์โทร :</label>
								<input type="text" class="form-control form-control-sm" id="tel_receiver" placeholder="เบอร์โทร" readonly>
							</div>
						</div>						
					</div>			
				</div>
			
			
			
			<div id="type_1" style="display:;">
				<div class="" id="">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="depart_receiver">แผนก : </label>
								<input type="text" class="form-control form-control-sm" id="depart_receiver" placeholder="ชื่อแผนก" disabled>
							</div>
						</div>	
						<div class="col-sm-6">
							<div class="form-group">
								<label for="floor_name">ชั้น : </label>
								<input type="text" class="form-control form-control-sm" id="floor_name" placeholder="ชั้น" disabled>
							</div>
						</div>		
					</div>			
							
				</div>	
					
				<div class="" id="">
					<div class="row">
						<div class="col-sm-6">				
							<div class="form-group">
								<label for="floor_send">ชั้นผู้รับ	: </label>
								<select class="form-control-lg" id="floor_send" style="width:100%;">
									<option value="0" > เลือกชั้นปลายทาง</option>
									{% for s in floor_data %}
										<option value="{{ s.mr_floor_id }}" > {{ s.name }}</option>
									{% endfor %}					
								</select>
							</div>
						</div>	
						<div class="col-sm-6" style="">
							<div class="form-group">
								<label for="topic_head">ชื่อเอกสาร :<span style="color:red;">*</span></label>
								<input type="text" class="form-control form-control-sm" id="topic_head" placeholder="ชื่อเอกสาร">
							</div>
						</div>		
					</div>			
							
				</div>	
					
				
				<div class="" id="">
					<div class="row">
						<div class="col-sm-6" >
							<div class="form-group">
								<label for="remark_head">หมายเหตุ :  </label>
								<textarea class="form-control form-control-sm" id="remark_head" placeholder="หมายเหตุ"></textarea>
							</div>
						</div>	

					</div>		
				</div>	
			</div>
			
			
			<div id="type_2" style="display:none;">
				
				<div class="form-group" id="">
					<div class="row">
						<div class="col-sm-6" >
							<div class="form-group">
								<label for="branch_receiver">สาขา :</label>
								<input type="text" class="form-control form-control-sm" id="branch_receiver" placeholder="ชื่อสาขา" disabled>
							</div>		
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label for="place_receiver">สถานที่อยู่ :</label>
								<input type="text" class="form-control form-control-sm" id="place_receiver" placeholder="สถานที่อยู่" disabled>
							</div>
						</div>		
					</div>			
							
				</div>	
					
				
				
				
				
						
						
				<div class="" id="">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="branch_send">สาขาผู้รับ : </label>
									<select class="form-control-lg" id="branch_send" style="width:100%;">
													
									</select>
							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label for="topic_branch">ชื่อเอกสาร :<span style="color:red;">*</span></label>
								<input type="text" class="form-control form-control-sm" id="topic_branch" placeholder="ชื่อเอกสาร">
							</div>
						</div>	
					</div>			
							
				</div>	
					
				
				<div class="form-group" id="">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="branch_floor">ชั้นผู้รับ : </label>
								<select class="form-control-lg" id="branch_floor" style="width:100%;">
											<option value="1" selected>ชั้น 1</option>			
											<option value="2" >ชั้น 2</option>			
											<option value="3" >ชั้น 3</option>			
								</select>							</div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">
								<label for="remark_branch">หมายเหตุ : </label>
								<textarea class="form-control form-control-sm" id="remark_branch" placeholder="หมายเหตุ"></textarea>
							</div>
						</div>	
					</div>		
				</div>	
				
			</div>
			
			
			
			
			
			<div class="form-group" style="margin-top:50px;">
				<div class="row">
					<div class="col-5" style="padding-right:0px">
					
					</div>	
					
					<div class="col-2" style="padding-left:0px">
						<button type="button" class="btn btn-primary" id="btn_save"  >บันทึกรายการ</button>

					</div>		
					<div class="col-5" style="padding-right:0px">
					
					</div>	
						
				</div>			
				
			</div>
			
	
		
	</div>
	</div>
</div>
	
	


<div class="modal fade" id="modal_showdata">
  <div class="modal-dialog modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">รายชื่อติดต่อในการจัดส่ง</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <div style="overflow-x:auto;">
        <table class="table" id="tb_con">
			  <thead>
			  
				<tr>
				  <th>ลำดับ</th>
				  <th>ชื่อหน่วยงาน</th>
				  <th>ชั้น</th>
				  <th>ผู้รับงาน</th>
				  <th>รหัสพนักงาน</th>
				  <th>เบอร์โทร</th>
				  <th>หมายเหตุ</th>
				</tr>
				
			  </thead>
			  <tbody>
			  {% for d in contactdata %}
				<tr>
				  <th scope="row">{{ loop.index }}</th>
				  <td>{{d.department_code}}:{{d.department_name}}</td>
				  <td>{{d.floor}}</td>
				  <td>{{d.mr_contact_name}}</td>
				  <td>{{d.emp_code}}</td>
				  <td>{{d.emp_tel}}</td>
				  <td>{{d.remark }}</td>
				</tr>
				{% endfor %}
			  </tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>


<div id="click_new_tab">
</div>
<div id="bg_loader">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base.tpl" %}

{% block title %}Pivot- รายการค้นหาข้อมูล{% endblock %}

{% block menu_1 %}
  <div id="menu_1_active">
    <div id="txtMenu">
      <a href="../admin/list.php"><font class="menuHead_active">แจ้งเตือน</font></a>
    </div>         
  </div>
{% endblock %}

{% block styleReady %}
	
	#content{
		padding : 0px;
	}

	#ctname{
		border-style : none;
	}
	.bgcolor{
		background-color 	:#FFFFE0;
		padding 			:20px;
		border-radius:5px 5px 5px 5px;
	}.batton{
		padding 			:50px;
	}
	.fail{
		color : red;
	}
{% endblock %}


 {% block cssImport %}
	
 {% endblock %} 
 
  {% block scriptImport %}
	<script src="js/order.js"></script>
	<script src="js/save_data_order.js"></script>
	
    <link rel="stylesheet" type="text/css" href="jquery/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="jquery/themes/icon.css">
    <script type="text/javascript" src="jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jquery/jquery.easyui.min.js"></script>
  {% endblock %}     

{% block topContent %}

{% endblock %}
{% block javaScript %}
 function setaction(data){
 document.getElementById('page').value=data;
  //alert(data);
 }
 
 $(function() {
 var availableTutorials = [{{arr}}];
 var availableTutorials1 = [{{arr1}}];
 //alert (availableTutorials);
	
       $( "#inresevre_id" ).autocomplete({
          source: availableTutorials,
          autoFocus:true
       });
	   $( "#employee_id" ).autocomplete({
          source: availableTutorials,
          autoFocus:true
       });
	 
	   $( "#companyname" ).autocomplete({
          source: availableTutorials1,
          autoFocus:true
       });
    });
		
		
		
{% endblock %}  
{% block Content %}
<form method = "get" action="">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2">
			<div id="dvTab">             
				<ul class="nav nav-tabs" role="tablist">
					<li onclick = "setaction('in');" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">internal</a></li>
					<li onclick = "setaction('ex');"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">external</a></li>
					<li onclick = "setaction('post');"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">post office</a></li>
				</ul>
			</div>
			<div class="row">
			<div class="tab-content" style="padding-top: 10px">	
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 bgcolor">
					<p class="text-center"><strong>ข้อมูลผู้สั่งงาน</strong></p><br>
					<label class="control-label">Employee Code<span class = "fail" id = "fail"><span></label>
					<input class="form-control" type="text" value="{{employeedata.0.code}}" id = "employee_id" name="employee_id">
					
					<label class="control-label">Name Surname</label>
					<input class="form-control" type="text" readonly="readonly" value="{{employeedata.0.name}}  {{employeedata.0.surname}}" id ="name">
					
					<label class="control-label">Address</label>
					<textarea class="form-control" rows="2" readonly="readonly" id = "address">ตึก/อาคาร  {{employeedata.0.address}}  ชั้น {{employeedata.0.address_floor}} แผนก  {{employeedata.0.dname}}</textarea>
					<label class="control-label">Telephone number</label>
					<input class="form-control" type="text" readonly="readonly" value="{{employeedata.0.telephone_number}}" id = "tel">
				</div>	
			<!--:::::::::::::::::::::::::::::::::::::::ภายไน:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
				<div role="tabpanel" class="col-xs-7 col-sm-7 col-md-7 col-lg-7 tab-pane active" id="tab1"><br>
					<p class="text-center"><strong>ข้อมูลผู้รับ</strong></p><br>
					<label class="control-label">Date Time</label>
					<div class="form-inline">
					  <div class="form-group">
						<label class="sr-only" for="exampleInputEmail3"></label>
						<input type="date"  value="{{date}}" class="form-control" readonly="readonly" id="indate" name = "indate">
					  </div>
					  <div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="time" value="{{time}}" class="form-control" readonly="readonly" id="intime" name = "intime">
					  </div>
					</div>		
					<label class="control-label">Employee Code<span class = "fail" id = "infail"><span></label>
					<input class="form-control" type="text" placeholder="Employee_id" onkeyup="getdataemployeeIN(this.value);" id="inresevre_id" name = "inresevre_id">
					
					<label class="control-label">Name Surname</label>
					<input class="form-control" type="text" placeholder="" readonly="readonly" id = "inname" name="inname">
					
					<label class="control-label">Address</label>
					<textarea class="form-control" rows="2" readonly="readonly" id = "inaddress" name="inaddress"></textarea>
					<label class="control-label">Telephone number</label>
					<input class="form-control" type="text" readonly="readonly" value="" id = "intel" name = "intel">
					<label class="control-label">Remark</label>
					<textarea class="form-control" rows="2" id = "inremark" name = "inremark"></textarea>
				</div>
			<!--:::::::::::::::::::::::::::::::::::::::ภายนอก:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
				<div role="tabpanel" class="col-xs-7 col-sm-7 col-md-7 col-lg-7 tab-pane" id="tab2"><br>
					<p class="text-center"><strong>ข้อมูลผู้รับ</strong></p><br>
					<label class="control-label">Date Time</label>
					<div class="form-inline">
					  <div class="form-group">
						<label class="sr-only" for="exampleInputEmail3"></label>
						<input type="date"  value="{{date}}" class="form-control" id="exdate" name="exdate">
					  </div>
					  <div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="time" value="{{time}}" class="form-control" id="extime" name = "extime">
					  </div>
					</div>		
					<label class="control-label">Name</label>
					<input class="form-control" type="text" placeholder="ชื่อ" id="exname" name="exname">
					<label class="control-label">Surname</label>
					<input class="form-control" type="text" placeholder="นามสกุล" id="exlname" name="exlname">
					
					<label class="control-label">Company Name</label>
					<!-- <br>
					<select  id="companyname" name="companyname">
						<option value="" selected></option>
						{% for d in  data1%}
						  <option value="{{d.building_name}}">{{d.building_name}}</option>
						{% endfor%}
					</select>
					</br> -->
					<input class="form-control" type="text" placeholder="ชือบริษัท/สำนักงาน" id="companyname" name="companyname">
					
					<label class="control-label">Address</label>
					<div class="form-inline">
					<div class="form-group">
						<label class="sr-only" for="exampleInputEmail3"></label>
						<input type="text"  value="" class="form-control" placeholder="เลขที่" id="exaddress_no" name="exaddress_no">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control" placeholder="แขวง/ตำบล" id="exaddress_tumbon" name="exaddress_tumbon">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control" placeholder="เขต/อำเภอ" id="exaddress_district" name="exaddress_district">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control" placeholder="จังหวัด" id="exaddress_province" name="exaddress_province">
					</div>
					</div>
					<label class="control-label">Telephone Number</label>
					<input class="form-control" type="text" value="" id = "extel" name = "extel">
					<label class="control-label">Remark</label>
					<textarea class="form-control" rows="3" id ="exremark" name="exremark"></textarea>
				</div>
			<!--:::::::::::::::::::::::::::::::::::::::ต่างประเทศ:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
				<div role="tabpanel" class="col-xs-7 col-sm-7 col-md-7 col-lg-7 tab-pane" id="tab3"><br>
					<p class="text-center"><strong>ข้อมูลผู้รับ</strong></p><br>
					<label class="control-label">Date Time</label>
					<div class="form-inline">
					  <div class="form-group">
						<label class="sr-only" for="exampleInputEmail3"></label>
						<input type="date"  value="{{date}}" class="form-control" id="postdate" name="postdate">
					  </div>
					  <div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="time" value="{{time}}" class="form-control" id="posttime" name="posttime">
					  </div>
					</div>		
					<label class="control-label">Name</label>
					<input class="form-control" type="text" placeholder="ชื่อผู้รับ" id="postname" name="postname">
					<label class="control-label">Surname</label>
					<input class="form-control" type="text" placeholder="นามสกุลผู้รับ" id="postlname" name="postlname">
					
				
					<label class="control-label">Address</label>
					<div class="form-inline">
					<div class="form-group">
						<label class="sr-only" for="exampleInputEmail3"></label>
						<input type="text"  value="" class="form-control" placeholder="เลขที่" id="postaddress_no" name="postaddress_no">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control" placeholder="หมู่" id="postaddress_moo" name="postaddress_moo">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control" placeholder="แขวง/ตำบล" id="postaddress_tumbon" name="postaddress_tumbon">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control"placeholder="เขต/อำเภอ" id="postaddress_district" name="postaddress_district">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control" placeholder="จังหวัด" id="postaddress_province" name="postaddress_province">
					</div>
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword3"></label>
						<input type="text" value="" class="form-control"  placeholder="รหัสไปรษณีย์" id="postaddress_clod" name="postaddress_clod">
					</div>
					</div>
					<label class="control-label">Telephone number</label>
					<input class="form-control" type="text" placeholder="โทรศัพท์" id="posttel" name="posttel">
					<label class="control-label">Remark</label>
					<textarea class="form-control" rows="3" id="postremark" name="postremark"></textarea>
				</div>
			</div>


			</div>
			<div class = "row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-3"><br><br><br><br>
					<label class="checkbox-inline"><input type="radio" name="radio" id = "radio1" value="1" checked>เอกสารธรรมดา</label>
					<label class="checkbox-inline"><input type="radio" name="radio" id = "radio2" value="2">เอกสารสำคัญ</label>
				</div>
			</div>
			<div class = "row">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 batton">
					<button type="button" class="btn btn-primary btn-block" onclick="savedata();">SAVE DATA</button>
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><input type = "hidden" name = "page" id = "page" value= "in"></div>
			</div>
			
		</div>
	</div>
</form>


{% endblock %}

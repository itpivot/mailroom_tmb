{% extends "base.tpl" %}

{% block title %}Pivot- รายการค้นหาข้อมูล{% endblock %}

{% block menu_1 %}
  <div id="menu_1_active">
    <div id="txtMenu">
      <a href="../admin/list.php"><font class="menuHead_active">แจ้งเตือน</font></a>
    </div>         
  </div>
{% endblock %}

{% block styleReady %}
	
	#content{
		padding : 0px;
	}

	#ctname{
		border-style : none;
	}
	.bgcolor{
		background-color 	:#FFFFE0;
		padding 			:20px;
		border-radius:5px 5px 5px 5px;
	}.batton{
		padding 			:50px;
	}
	.fail{
		color : red;
	}
{% endblock %}


 {% block cssImport %}
	
 {% endblock %} 
 
  {% block scriptImport %}
	<script src="js/order.js"></script>
	<script src="js/save_data_order.js"></script>
	
	

  {% endblock %}     

{% block topContent %}
	
{% endblock %}

{% block domReady %}

 
{% endblock %} 

{% block javaScript %}
 function setaction(data){
 document.getElementById('page').value=data;
  //alert(data);
 }
 function openpage(data){
	//alert(data);
	window.location.href="print_page.php?woke_id="+data;
 }
 	 
{% endblock %}  
{% block Content %}

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2">
	<form action="" method="post">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3">
		<center>
			<select class="form-control" name = "type">
			  <option value =''>เลือกปรเภทงาน</option>
			  <option value ='PI'>Internal</option>
			  <option value ='PE'>External</option>
			  <option value ='PP'>Post</option>
			</select>
		</center>
		<center>
			<input type="date" value="" class="form-control" placeholder="วันที่" id="date" name="date"><br>
			<button type="submit" class="btn btn-danger">search</button>
		</center><br>
	</div>
	
	</form>
		<table class="table">
			<thead>
			
			  <tr class="active">
				<th class="text-center">ACTION</th>
				<th class="text-center">NO</th>
				<th>วันที่/เวลา</th>
				<th>ชื่อผู้รับ</th>
				<th>เบอร์โทร</th>
				<th>status</th>
			  </tr>
			  
			</thead>
			<tbody>
			{% for data in workorderall %}
			  <tr class="warning">
				<td class="text-center">
					<button type="button" class="btn btn-link btn" onclick="openpage('{{data.scg_work_order_id}}');"><span class="glyphicon glyphicon-print" style = "font-size:15px;"></span></button>
					<a href="shostatus.php?o_id={{data.scg_work_order_id}}" class="btn btn-info btn-sm btn-block" >
						<i class="glyphicon glyphicon-zoom-in" aria-hidden="true"></i>
					</a>
				</td>
				<td>{{loop.index}}</td>
				<td>{{ data.system_date}}</td>
				<td>{{ data.receiver_name}}  {{ data.receiver_surname}}</td>
				<td>{{ data.receiver_telephone_number}}</td>
				<td>{{ data.status_name}}</td>
			  </tr>
			{% endfor %}
			</tbody>
		</table>
	</div>
</div>

	
{% endblock %}

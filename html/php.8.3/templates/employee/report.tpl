{% extends "base.tpl" %}
{% block domReady %}

{% endblock %} 
	 $("#flip").click(function(){
        $("#panel").slideToggle("slow");
    });
{% block javaScript %}

function actionbuttom(data) {
			$("#data"+data).slideToggle("slow");
	//console.log(data);
}
{% endblock %}
{% block css %}
	.detel {
   
    text-align: center;
    background-color: #CCCCFF;
    border: solid 1px #c3c3c3;
	}

	.detel {
		display: none;
	}
	#panel, #flip {
    padding: 5px;
    text-align: center;
    background-color: #e5eecc;
    border: solid 1px #c3c3c3;
	}

	#panel {
		padding: 50px;
		display: none;
	}
{% endblock %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e5 %} active {% endblock %}


{% block Content %}

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2">


			<center><b>Report</b></center>
			<form action="" method="post">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3">
				<center>
					<select class="form-control" name = "type">
					  <option value =''>เลือกประเภทงาน</option>
					  <option value ='PI'>Internal</option>
					  <option value ='PE'>External</option>
					  <option value ='PP'>Post</option>
					</select>
				</center>
				<center>
					<input type="date" value="" class="form-control" placeholder="วันที่" id="date" name="date"><br>
					<button type="submit" class="btn btn-danger" value = "sert" name = "action">search</button>
				</center><br>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div align="left" >
					<b>{{type_work}}  {{countworkor}}<b>
				</div>	
			</div>

	</form>
		
		<table class="table">
			<thead>
			
			  <tr class="active">
				<th class="text-center">NO</th>
				<th>Action</th>
				<th>วัน/เวลา ที่สั่งงาน</th>
				<th>ชื่อผู้รับ</th>
				<th>เบอร์โทร</th>
				<th>status</th>
				
			  </tr>
			  
			</thead>
			<tbody>
			{% for data in workorderall %}
			  <tr class="warning">
				<td class="text-center">{{loop.index}}</td>
				<td>
					<a href="shostatus.php?o_id={{data.scg_work_order_id}}" class="btn btn-info btn-sm btn-block" >
						<i class="glyphicon glyphicon-zoom-in" aria-hidden="true"></i>
					</a>
				</td>
				<td>{{ data.system_date}}</td>
				<td>{{ data.receiver_name}}  {{ data.receiver_surname}}</td>
				<td>{{ data.receiver_telephone_number}}</td>
				<td>{{ data.status_name}}</td>
				
				
			  </tr>
			{% endfor %}
			</tbody>
		</table>
		<div>
			<center><b>สรุปยอดงาน</b></center>
			<table class="table">
				<tbody>
				  <tr class="warning">
					<th>วันนี้ทั้งหมด</th>
					<td><a href='#'>{{today}}</a></td>
				  </tr>
				  <tr class="warning">
					<th>เดือนนี้ทั้งหมด</th>
					<td><a href='#'>{{tomoun}}</a></td>
				  </tr>
				</tbody>
			</table>
		</div>
		
		
		</div>
	</div>
	
{% endblock %}
{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

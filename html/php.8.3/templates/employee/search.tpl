{% extends "base_emp2.tpl" %}


{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .card {
       margin: 8px 0px;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 70%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
    }
    
    .btn {
        border-radius: 0px;
    }
{% endblock %}

{% block domReady %}
	
	$('#exampleModal').modal('hide');
	
	
	$('#img_loading').hide();
    $('#txt_search').on('keyup', function() {
        let txt = $(this).val();
        let str = "";
        $.ajax({
            url: "./ajax/ajax_search_work_emp.php",
            type: "POST",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                $('#img_loading').show();
                $('#data_list').hide();
            },
            success: function(res){
			//console.log(res);
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {	
						
						str += '<div class="card">'; 
							str += '<div class="card-body space-height">';
								str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
								str += '<p><b>ผู้รับ : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
								
								if( res[i]['mr_emp_tel'] == "" || res[i]['mr_emp_tel'] == null || res[i]['mr_emp_tel'] == "NULL" || res[i]['mr_emp_tel'] == "null" ){
									str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+'</a></p>';
								}else if( res[i]['mr_emp_mobile'] == "" || res[i]['mr_emp_mobile'] == null || res[i]['mr_emp_mobile'] == "NULL" || res[i]['mr_emp_mobile'] == "null" ){
									str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a></p>';
								}else{
									str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+' </a>/ <a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a></p>';
								}
								
								str += '<p><b>แผนก : </b></p>';
								str += '<p>'+res[i]['mr_department_name']+'</p>';
								str += '<p><b>ชั้น : </b>'+res[i]['floor_name']+'</p>';
								str += '<p><b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'</p>';
								str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<p>';
								str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<p>';
								str += '<p><b><h4>สถานะ : </b>'+res[i]['mr_status_name']+'</h4></p>';
								
								if ( res[i]['mr_status_id'] == 5 ){
									str += '<a href="detail_receive.php?barcode='+res[i]['mr_work_barcode']+'" class="btn btn-info"  >รายละเอียดการรับเอกสาร</a>';
										
								}
								str += '</div>';
						str += '</div>';
						
					}
					
                    $('#data_list').html(str);
					CountSearchEmp();
                }else {
					CountSearchEmp();
                    str = '<center>ไม่พบข้อมูล !</center>';
                    $('#data_list').html(str);
                }
            },
            complete: function() {
                $('#img_loading').hide();
                $('#data_list').show();
                $('#data_count').show();
				
            }
        });
    });
{% endblock %}

{% block javaScript %}
 
	
	function CountSearchEmp() {
     let cr = "";
	 let txt = $('#txt_search').val();
        $.ajax({
            url: "./ajax/ajax_search_work_emp_count.php",
            type: "POST",
			data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                $('#img_loading').show();
                $('#data_count').hide();
            },
            success: function(res){
                if(res != 0) {
						cr += '<center>จำนวนเอกสาร : '+ res+'   ฉบับ</center>'; 
                    $('#data_count').html(cr);
                }else {
                    cr = '<center>จำนวนเอกสาร :  0  ฉบับ</center>'; 
                    $('#data_count').html(cr);
                }
            },
            complete: function() {
				$('#data_count').show();
				
            }
        });
    }
{% endblock %}

{% block Content %}
    <div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
    
    <div class="search_list">
        <form>
            <div class="form-group">
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหาจาก Tracking number, ชื่อ-สกุล, เบอร์ติดต่อ, วันที่">
            </div>
			<div class="form-group" id="data_count">
				
            </div>
        </form>
    </div>
	<div id="data_list">
	
	
	
	</div>
		                                                                            
	
	
	
	
	
	
	
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

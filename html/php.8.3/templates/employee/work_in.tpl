{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block scriptImport %}


{% endblock %}


{% block domReady %}

		check_name_re();
		

		$("#btn_save").click(function(){
			
					var pass_emp 			= $("#pass_emp").val();
					var name_receiver 		= $("#name_receiver").val();
					var pass_depart 		= $("#pass_depart").val();
					var floor 				= $("#floor").val();
					var depart 				= $("#depart").val();
					var remark 				= $("#remark").val();
					var emp_id 				= $("#emp_id").val();
					var user_id 			= $("#user_id").val();
					var floor_send 			= $("#floor_send").val();
					var floor_id 			= $("#floor_id").val();
					var barcode 			= $("#barcode").val();
					var topic 				= $("#topic").val();
					var qty 				= $("#qty").val();
					var status 				= true;
					
						
						if(pass_emp == "" || pass_emp == null){
							status = false;
							alertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลผู้รับให้ครบถ้วน!');
						}
						if(topic == "" || topic == null){
							status = false;
							alertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชื่อเอกสาร !');
						}
						
						if(floor_id == "" || floor_id == null || floor_id == 0 ){
							if(floor_send == 0 || floor_send == null){
								status = false;
								alertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชั้นของผู้รับ!');
							}
						}
						
						
						
						//console.log(floor_id);
						if( status === true ){
							var obj = {};
							obj = {
								pass_emp: pass_emp,
								emp_id: emp_id,
								remark: remark,
								user_id: user_id,
								floor_send: floor_send,
								csrf_token	 : $('#csrf_token').val(),
								floor_id: floor_id,
								qty: qty,
								topic: topic
							}
							alertify.confirm('ยืนยันการบันทึกข้อมูล',"โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง", function() {
								$.when(saveInOut(obj)).done(function(res) {
									obj_d = JSON.parse(res);
									//console.log(obj_d);
									$('#csrf_token').val(obj_d['token']);
									if(obj_d.status == 200) {
										var msg = '<h1><font color="red" ><b>' +obj_d.barcodeok + '<b></font></h1>';
										alertify.alert('บันทึกสำเร็จ','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : \\n'+ msg, function() {
												$("#name_receiver").val('');
												$("#pass_depart").val('');
												$("#floor").val('');
												$("#depart").val('');
												$("#pass_emp").val('');
												$("#topic").val('');
												$("#remark").val('');
												$("#floor_send").val(0).trigger('change');
												$("#name_receiver_select").val(0).trigger('change');
										});


									}else{
										alertify.alert('เกิดข้อผิดพลาด',obj_d['message'],
										function(){
											window.location.reload();
										}
									);
									}
								});
								},function() {
							});		
							
							$('#name_receiver ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#pass_depart ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#floor ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#pass_emp ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#depart ').css({'border': '1px solid rgba(0,0,0,.15)'});
							$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});
							
						}
			
		});
		
		$("#pass_emp").keyup(function(){
			check_name_re();
			var pass_emp = $("#pass_emp").val();
			$.ajax({
				//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress2.php",
				url: url+"ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200) {
						if(res.data != false) {
							var fullname = res['data']['mr_emp_name'] + " " + res['data']['mr_emp_lastname'];
							$("#name_receiver").val(fullname);
							$("#pass_depart").val(res['data']['mr_department_code']);
							$("#floor").val(res['data']['mr_department_floor']);
							$("#floor_id").val(res['data']['mr_floor_id']);
							$("#depart").val(res['data']['mr_department_name']);
							$("#emp_id").val(res['data']['mr_emp_id']);
						} else {
							$("#name_receiver").val("");
							$("#pass_depart").val("");
							$("#floor").val("");
							$("#floor_id").val("");
							$("#depart").val("");
							$("#emp_id").val("");
						}
					
					}  else {
						$("#name_receiver").val("");
						$("#pass_depart").val("");
						$("#floor").val("");
						$("#floor_id").val("");
						$("#depart").val("");
						$("#emp_id").val("");
					}	
				}
												
			});
									
		});
		
		$("#name_receiver_select").click(function(){
			var name_receiver_select = $("#name_receiver_select").val();
			$.ajax({
				//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php",
				url: url+"ajax/ajax_autocompress_name.php",
				type: "post",
				data: {
					'name_receiver_select': name_receiver_select,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200) {
						if(res.data != false) {
							$("#mr_emp_code").val(res['data']['mr_emp_code']);
							$("#pass_depart").val(res['data']['mr_department_code']);
							$("#floor").val(res['data']['mr_department_floor']);
							$("#floor_id").val(res['data']['mr_floor_id']);
							$("#depart").val(res['data']['mr_department_name']);
							$("#emp_id").val(res['data']['mr_emp_id']);
						} 
					} 
			
				}
												
			});
									
		});

		$('#name_receiver_select').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php",
					url: url+"ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setForm(e.params.data);
		});


		
{% endblock %}				
{% block javaScript %}
var url = "https://www.pivot-services.com/mailroom_tmb/employee/";
url = "";
		function check_name_re(){
			var pass_emp = $("#pass_emp").val();
			 if(pass_emp == "" || pass_emp == null){ 
				$("#div_re_text").hide();
				$("#div_re_select").show();
			 }else{
				$("#div_re_text").show();
				$("#div_re_select").hide();
			 }
		}
		
		function setForm(data) {
				var emp_id = parseInt(data.id);
				var fullname = data.text;
				$.ajax({
					//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
					url: url+'ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						if(res.status == 200) {
							if(res.data != false) {
								$("#pass_emp").val(res['data']['mr_emp_code']);
								$("#pass_depart").val(res['data']['mr_department_code']);
								$("#floor").val(res['data']['mr_department_floor']);
								$("#floor_id").val(res['data']['mr_floor_id']);
								$("#depart").val(res['data']['mr_department_name']);
								$("#emp_id").val(res['data']['mr_emp_id']);
							}
						}
					
					}
				})
		}
		
		
		
		function name_re_chang(){
			var name_receiver_select = $("#name_receiver_select").val();
			$.ajax({
				//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php",
				url: url+"ajax/ajax_autocompress_name.php",
				type: "post",
				data: {
					'name_receiver_select': name_receiver_select,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200) {
						if(res.data != false) {
							$("#pass_emp").val(res['data']['mr_emp_code']);
							$("#pass_depart").val(res['data']['mr_department_code']);
							$("#floor").val(res['data']['mr_department_floor']);
							$("#floor_id").val(res['data']['mr_floor_id']);
							$("#depart").val(res['data']['mr_department_name']);
							$("#emp_id").val(res['data']['mr_emp_id']);
						}
					}
					
				}
												
			});
		}


		var saveInOut = function(obj) {
			//return $.post('https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_save_inout.php', obj);
			return $.post(url+'ajax/ajax_save_inout.php', obj);
		}
				
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
			<div class="form-group" style="text-align: center;">
				 <label><h4 >ข้อมูลผู้รับเอกสาร</h4></label>
			</div>	
			
			<input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf}}">
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="">
			 <input type="hidden" id="floor_id" value="">
			 <input type="hidden" id="barcode" value="{{ barcode }}">
			
			
			<div class="form-group" id="div_re_text">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อผู้รับ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name_receiver" placeholder="ชื่อผู้รับ">
					</div>		
				</div>			
			</div>
			
			
			<div class="form-group" id="div_re_select">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อผู้รับ <span style='color:red; font-size: 15px'>&#42;</span>:
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="name_receiver_select" style="width:100%;" >
							 {# < option value = "0" > ค้นหาผู้รับ < /option>
							 {% for e in emp_data %}
								<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
							 {% endfor %} #}
						</select>
					</div>		
				</div>			
			</div>	
			
			
			
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน <span style='color:red; font-size: 15px'>&#42;</span> :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสหน่วยงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_depart" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้น :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="floor" placeholder="ชั้น" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อหน่วยงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="depart" placeholder="ชื่อหน่วยงาน" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อเอกสาร<span style='color:red; font-size: 15px'>&#42;</span> :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="topic" placeholder="ชื่อเรื่อง, หัวข้อเอกสาร">
					</div>		
				</div>			
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						จำนวน<span style='color:red; font-size: 15px'>&#42;</span> :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="number" value="1" class="form-control form-control-sm" id="qty" placeholder="จำนวนเอกสาร 1,2,3,...">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้นของผู้รับ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="floor_send" style="width:100%;">
							<option value="0" > เลือกชั้นปลายทาง</option>
							{% for s in floor_data %}
								<option value="{{ s.mr_floor_id }}" > {{ s.name }}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>	
			
			
			
			
			<div class="form-group">
				<textarea class="form-control form-control-sm" id="remark" placeholder="หมายเหตุ"></textarea>
			</div>
			{% if tomorrow == 1 %}
				<div class="form-group">
					<div class="row">
						<div class="col-12" style="color:red;">
							<center>เอกสารที่ส่งหลัง 16.00 น. จะทำการส่งในวันถัดไป </center>
						</div>			
					</div>			
				</div>	
			{% endif %}
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save"  >Save</button>
			</div>
			
			
	
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

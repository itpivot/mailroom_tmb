{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
      }

    
     
{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">
<link rel="stylesheet" href="../themes/datepicker/bootstrap-datepicker3.min.css">

<script type='text/javascript' src="../themes/datepicker/bootstrap-datepicker.min.js"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>
{% endblock %}

{% block domReady %}
	$('#tb_details').DataTable();
{% endblock %}


{% block javaScript %}
	
{% endblock %}

{% block Content2 %}
 <p><center><strong>รายละเอียด</strong></center></p>
   <table id='tb_details' class="table table-responsive table-bordered table-striped table-sm" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date/Time</th>
                        <th>Barcode</th>
                        <th>Receiver</th>
                        <th>Sender</th>
                        <th>Type Work</th>
                       <!--  <th>Floor (Receiver)</th> -->
                        <th>Status</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    {% for r in result %}
                        <tr>
                            <td>{{ r.no }}</td>
                            <td>{{ r.time_true }}</td>
                            <td>{{ r.mr_work_barcode }}</td>
                            <td>{{ r.name_re }} {{ r.lastname_re }}</td>
                            <td>{{ r.mr_emp_name }} {{ r.mr_emp_lastname }}</td>
                            <td>{{ r.mr_type_work_name }}</td>
                           <!--  <td>{{ r.mr_department_floor }}</td> -->
                            <td>{{ r.mr_status_name }}</td>
                            <td>{{ r.mr_work_remark }}</td>
                        </tr>
                    {% endfor %}
                </tbody>
            </table>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

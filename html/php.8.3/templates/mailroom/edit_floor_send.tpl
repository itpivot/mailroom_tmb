{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
		
		{{alert}}
		
		$("#mr_type_work_id").change(function(){
			var type  = $(this).val();
			if(type == 2){
				$('#div_branch_id').show();
				$('#floor_branch').show();
				$('#floor_ho').hide();
			}else{
				$('#div_branch_id').hide();
				$('#floor_branch').hide();
				$('#floor_ho').show();
			}
		});

		$("#mr_branch_id").change(function(){
			var type  = $("#mr_type_work_id").val()
			var branch_id  = $(this).val();
			$.ajax({
				url: 'ajax/ajax_autocompress_chang_branch.php',
				type: 'POST',
				data: {
					branch_id: branch_id
				},
				dataType: 'json',
				success: function(res) {
					if(res.status == 200) {
						$('#floor2').html(res.data);
					}
					
				}
			})
			console.log(type);
		});


		$("#btn_save").click(function(){
			var floor 							= $("#floor").val();
			var floor2 							= $("#floor2").val();
			var mr_branch_id 				= $("#mr_branch_id").val();
			var mr_work_inout_id 				= $("#mr_work_inout_id").val();
			var mr_type_work_id 				= $("#mr_type_work_id").val();
			var mr_work_main_id 				= $("#mr_work_main_id").val();
			var mr_status_id 					= $("#mr_status_id").val();
			var data 							= $('#floor2').select2('data');
			
			if($('#floor2').val()){
				var floor2_text 					= data[0].text;
			}

			if(mr_type_work_id == 2 && mr_branch_id == 0){
				alert('กรุณาระบุสาขาปลายทาง');
				return;
			}else{
				$.post(
				'./ajax/ajax_editFloorSend.php',
				{
					mr_branch_id 		 : mr_branch_id,
					mr_work_main_id 		 : mr_work_main_id,
					mr_type_work_id 		 : mr_type_work_id,
					mr_work_inout_id 		 : mr_work_inout_id,
					mr_status_id 		 	 : mr_status_id,
					floor 					 : floor,
					floor2 					 : floor2,
					floor2_text 			: floor2_text,
					type 					 : "mailrommedit"
				},
				function(res) {
					if(res.status == 200){
						location.reload();
					}
					alert(res.message);
					
				},'json');
			}
		});
		
{% endblock %}				
{% block javaScript %}
				
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
			<div class="form-group" style="text-align: center;">
				 <label><h4> ข้อมูลผู้รับเอกสาร </h4></label>
			</div>	
			 <input type="hidden" id="mr_work_inout_id" value="{{ user_data.mr_work_inout_id }}">
			 <input type="hidden" id="mr_work_main_id" value="{{ user_data.mr_work_main_id }}">
			 <input type="hidden" id="mr_status_id" value="{{ user_data.mr_status_id }}">
				<div class="form-group">
					<div class="row">
						<div class="col-4" style="padding-right:0px">
							ประเภทการส่ง :
						</div>	
						<div class="col-8" style="padding-left:0px">
							<select class="form-control-lg" id="mr_type_work_id" style="width:100%;">
									<option value="1" {% if user_data.mr_type_work_id == 1 %} selected="selected" {% endif %}>รับส่งภายใน</option>				
									<option value="2" {% if user_data.mr_type_work_id == 2 %} selected="selected" {% endif %}>ส่งที่สาขา</option>				
									<option value="3" {% if user_data.mr_type_work_id == 3 %} selected="selected" {% endif %}>ส่งที่สำนักงานใหญ่</option>				
							</select>
						</div>		
					</div>			
				</div>
				
				<div class="form-group" id="div_branch_id" {% if user_data.mr_type_work_id == 2 %} {% else %}style="display:none;" {% endif %}>
					<div class="row">
						<div class="col-4" style="padding-right:0px">
							สาขาปลายทาง :
						</div>	
						<div class="col-8" style="padding-left:0px">
							<select class="form-control-lg" id="mr_branch_id" style="width:100%;">
							
								{% if user_data.mr_branch_id == '' %}
										<option value="0" selected="selected">ไม่มี</option>
								{% endif %}		
								{% for b in branch %}
									
									<option value="{{ b.mr_branch_id }}" {% if b.mr_branch_id == user_data.mr_branch_id %} selected="selected" {% endif %}>{{ b.mr_branch_code }}:{{ b.mr_branch_name }}</option>
								{% endfor %}					
							</select>
						</div>		
					</div>			
				</div>
	
	
	
	
	
	
	
	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน" value="{{ user_data.emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name" placeholder="ชื่อ" value="{{ user_data.name_re }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="last_name" placeholder="นามสกุล" value="{{ user_data.lastname_re }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel" placeholder="เบอร์โทรศัพท์" value="{{ user_data.tel_re }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel_mobile" placeholder="เบอร์มือถือ" value="{{ user_data.mobile_re }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						Email :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="email" placeholder="email" value="{{ user_data.mr_emp_email }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						แผนก :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="department" style="width:100%;" disabled>
							{% for s in department_data %}
								<option value="{{ s.mr_department_id }}" {% if s.mr_department_id == user_data.mr_department_id %} selected="selected" {% endif %}>{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px ;">
						ชั้น :
					</div>	
					<div id="floor_ho" class="col-8" style="padding-left:0px;  {% if user_data.mr_type_work_id != 2 %} {% else %}display:none; {% endif %} ">
						<select class="form-control-lg" id="floor" style="width:100%;">
							{% if user_data.mr_floor_id == '' %}
										<option value="0" selected="selected">ไม่มี</option>
							{% endif %}		
							{% for f in floor_data %}
								<option value="{{ f.mr_floor_id }}" {% if f.mr_floor_id == user_data.mr_floor_id %} selected="selected" {% endif %}>{{ f.name }}</option>
							{% endfor %}					
						</select>
					</div>	
					<div id="floor_branch" class="col-8" style="padding-left:0px;   {% if user_data.mr_type_work_id == 2 %} {% else %}display:none; {% endif %}">
						<select class="form-control-lg" id="floor2" style="width:100%;">
							{% if user_data.mr_floor_id == '' %}
								<option value="0" selected="selected">{{user_data.mr_branch_floor}}</option>
							{% endif %}
							{% for f in floor_data2 %}
								<option value="{{ f.mr_floor_id }}" {% if f.mr_floor_id == user_data.mr_floor_id %} selected="selected" {% endif %}>{{ f.mr_floor_mame }}</option>
							{% endfor %}	
						</select>
					</div>	
				</div>			
			</div>
			
			
			
			
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึกการแก้ไขชั้น</button>
				<br>
				
			</div>
			
			
	
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

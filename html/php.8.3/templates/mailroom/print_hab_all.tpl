<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Times New Roman", Times, serif;
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 16px
 }
 tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 14px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

    @media print {
	#print_p{
		  display:none;
		}
      html,
      body {
        width: 210mm;
        height: 297mm;
		 font-family: "Times New Roman", Times, serif;
      }
      .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
      }
    }
	.table th {
		padding: 5px;
	}.table td {
		padding: 3px;
	}
  </style>
</head>

<body>
  <div class="book">
   {% if data|length > 0 %} {% for pa in data %}
   {% for dt in pa.dd %}

    <div class="page">
      <div class="subpage ">
		<table width="100%">
		<tr>
			<td width="20%"></td>
			<td valign="top" align="center">
				<table width="100%">
				<tr ><td>
				<div class="mb-3 text-center">
				  <h6 class="">ใบบันทึกการนำส่งเอกสารรถยนต์ ประจำธนาคารทหารไทย จำกัด(มหาชล)</h6>
				  <h6 class="">{{dt.head.d.mr_hub_name}} ประจำวันที่  {{dt.head.date}}</h6>
				 {# <img src="barcode.php?barcode={{dt.h.d.barcode}}&amp;width=350&amp;height=60" align="absmiddle">
					#}
				</div>
				</td></tr>
				</table>
			</td>
			<td width="20%">
				<table width="100%">
					<tr>
						<td colspan="2" align="right">หน้า  :&nbsp;&nbsp;&nbsp;<b><u>{{dt.head.p}}/{{pa.count}}&nbsp;&nbsp;&nbsp;</u></b></td>
					</tr>
					  <tr>
						<td colspan="2" align="right"><p class="card-text">วันที่  <b><u>  {{dt.head.date}} </u></b></p></td>
					  </tr>
					  
					  <tr>
					  <td><p class="card-text"></td>
					  <td align="right"><p class="card-text">เวลา  <b><u>&nbsp;&nbsp;&nbsp;{{dt.head.time}}&nbsp;&nbsp;&nbsp;</u></b> </p></td>
					  </tr>
				 </table>
			</td>
		</tr>
		 </table>
        <div class="table-responsive">
          <table   border="1" class="table table-bordered">
            <thead class="thead-light">
              <tr>
				<th>ลำดับ</th>
                <th>รหัสสาขา</th>
                <th>สาขา</th>
                <th>จำนวนซอง</th>
                <th>หมายเหตุ</th>
              </tr>
            </thead>
           
            <tbody>
			{% for subd in dt.data %}
              <tr>
                
                <td>{{subd.no}}</td>
				{% if subd.re_mr_branch_code == ""%}
					<td class="text-center">{{ subd.re_mr_branch_code }}</td>
					<td>{{ subd.re_mr_branch_name }}</td>
				{% else %}
					<td class="text-center">{{ subd.re_mr_branch_code }}</td>
					<td>{{ subd.re_mr_branch_name }}
						{% if subd.mr_branch_floor != '' %}
							/ ชั้น  {{subd.mr_branch_floor }}
						{% endif %}
					</td>
				{% endif %}
				<td class="text-center" >{{ subd.count }}</td>
                <td></td>
              </tr>
			 {% endfor %}
			 {% if dt.head.p == pa.count %}
			<tr>
				<th class="text-right"colspan="3"><u>รวมจำนวนซองทั้งสิ้น</u></th>
				<th class="text-center" ><u>{{pa.trfooter}}</u></th>
				<th></th>
			</tr>
		 {% endif %}
            </tbody>
            
            
          </table>
        </div>
		<br>
				<br>
				<br>
				{# 
<p>โปรดยืนยันการนำส่งเอกสารข้างต้นนี้ หากมีรายการผิดพลาดกรุณาแจ้งทันที</p><br>
		<center>
			<p>ลงชื่อ...........................................ผู้ส่ง  เลขที่พนักงาน.......................</p>
			<p>ลงชื่อ...........................................ผู้รับรอง  เลขที่พนักงาน........................</p>
		</center>
<p>หมายเหตุซรับเอกสารหลังเวลาที่กำหนด  สารบรรณกลางและโลจีสติกส์  จะส่งให้หน่วยงานในวันถัดไป</p>
#}
			<div id="print_p">
				<br>
				<br>
				<br>
				<br>
					<center>
						<button onclick="window.print();">print</button>
					<center>
				</div>	
      </div>
    </div>

 {% endfor %}   
{% endfor %}  {% endif %}
  </div>
  {% if debug != "" %}
  <pre>
  {{debug}}
  </pre>
  {% endif %}
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

  <script>
  $(document).ready(function(){
	{{alertt}}
	{% if alertt == ''%}
	 //window.print()
	{% endif %} 
  });
  </script>
</body>

</html>
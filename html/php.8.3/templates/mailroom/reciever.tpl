{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
	  }


{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">

<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>
{% endblock %}

{% block domReady %}
		
		
		
		$("#barcode").focus();
		$("#btn_save").click(function(){
			var barcode 			= $("#barcode").val();
			var mr_round_id 			= $("#mr_round_id").val();
			var full_barcode 		= $('#check_full_barcode') .prop( 'checked' );
			var status 				= true;
			
			if(barcode == "" || barcode == null){
				$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
				status = false;
			}else{
				status = true;
			}
		
			//console.log(status);
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_update_work_by_barcode.php",
					type: "post",
					dataType: "json",
					data: {
						'barcode': barcode,
						'mr_round_id': mr_round_id,
						'full_barcode': full_barcode,
					},
					
					success: function(res){
						if(res.status == 200){
							$("#barcode").val('');
							$('#tb_work_order').DataTable().ajax.reload();	
						}else{
							alertify.alert(res.message);
						}
					}							
				});	
				$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
			}
		});
			

			var table = $('#tb_work_order').DataTable({
			'responsive': true,
			'pageLength': '100',
			'columns': [
				{ 'data':'no' },
				{ 'data':'time_send'},
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_type_work_name' },
				{ 'data':'mr_status_name' }
			],
			
			'scrollCollapse': true 
		});

	//	var table = $('#tb_work_order').DataTable({
	//		'responsive': true,
	//		 "ajax": {
	//			"url": "./ajax/ajax_load_work_mailroom.php",
	//			"type": "POST",
	//			"dataType": 'json'
	//		},
	//		'columns': [
	//			{ 'data':'no' },
	//			{ 'data':'time_send'},
	//			{ 'data':'mr_work_barcode' },
	//			{ 'data':'name_send' },
	//			{ 'data':'name_receive' },
	//			{ 'data':'mr_type_work_name' },
	//			{ 'data':'mr_status_name' }
	//		],
	//		
	//		'scrollCollapse': true 
	//	});

load_all_data();
{% endblock %}


{% block javaScript %}

function load_all_data(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_load_work_mailroom.php",
					type: "post",
					dataType:'json',
					data: {
						'barcode': barcode
					},
				   beforeSend: function( xhr ) {
						$('#bg_loader').show();
					}
				}).done(function( msg ) {
					if(msg.status == 200){
						$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
						//$('#bg_loader').hide();
						$('#tb_work_order').DataTable().clear().draw();
						$('#tb_work_order').DataTable().rows.add(msg.data).draw();
					}else{
						alertify.alert(msg.message);
					}
				});
			}
		}


		function check_barcode() {
			var full_barcode 	= $('#check_full_barcode') .prop( 'checked' );
			if ( full_barcode == true ) {
				$("#barcode_full").hide();
			}else{
				$("#barcode_full").show();
			}
		}


		function update_barcode() {
			var barcode 			= $("#barcode").val();
			var full_barcode 		= $('#check_full_barcode') .prop( 'checked' );
			var mr_round_id 		= $("#mr_round_id").val();
			var status 				= true;
			
			if(barcode == "" || barcode == null){
				$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
				status = false;
			}else{
				status = true;
			}
		
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_update_work_by_barcode.php",
					type: "post",
					dataType: "json",
					data: {
						'barcode': barcode,						
						'mr_round_id': mr_round_id,
						'full_barcode': full_barcode,
					},
					success: function(res){
						if(res.status == 200){
							$("#barcode").val('');
							 load_all_data();
						}else{
							alertify.alert(res.message);
						}
						$("#barcode").val('');
					}							
				});	
				
				$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
			}
		}

		function keyEvent( evt, barcode )
		{
			if(window.event)
				var key = evt.keyCode;
			else if(evt.which)
				var key = evt.which;
				
			if( key == 13 ){
				update_barcode();
			}
		}


{% endblock %}

{% block Content2 %}

		<div class="row" border="1">
			<div class="col">
				
			</div>
			<div class="col-9">
				<div class="card">
					<h4 class="card-header">รับเข้าเอกสาร</h4>
					<div class="card-body">
						<form>
							<div class="form-row justify-content-center">
								<div class="col-sm-3">
									<input type="text" class="form-control mb-5 mb-sm-0" id="barcode_full" value="{{ barcode_full }}" readonly>
									
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control mb-5 mb-sm-0" id="barcode" placeholder="Barcode" onkeyup="keyEvent(event,this.value);">
									
								</div>
								<div class="col-auto">
									<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึก</button>
								</div>
							</div>
							
							<div class="form-row justify-content-center" style="margin-top:20px;">
								<div class="form-check">
									<label class="form-check-label">
										<input type="checkbox" class="form-check-input" id="check_full_barcode" onclick="check_barcode();">
										ยิงบาร์โค้ดย้อนหลัง
									</label>
								</div>
									
							</div>
							
							<div class="row justify-content-center">
								<div class="col-sm-3">
									 <select class="form-control" id="mr_round_id">
										 {% for r in round %}
										 <option value="{{r.mr_round_id}}">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
										 {% endfor %}
										</select>
								</div>
							</div>
							
							
						</form>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	<br>
		 <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date/Time</th>
                            <th>Bacode</th>
							<th>Sender</th>
                            <th>Receiver</th>
                            <th>Type Work</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
	
	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

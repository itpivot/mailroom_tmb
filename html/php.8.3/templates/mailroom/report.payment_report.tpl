{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}

.loading {
	position: fixed;
	top: 40%;
	left: 50%;
	-webkit-transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%);
	transform: translate(-50%, -50%);
	z-index: 1000;
}

.img_loading {
	  width: 350px;
	  height: auto;
	  
}

{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}
		$('.type_work').select2();
		$('.input-daterange').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		});	
		var table = $('#tb_level3').DataTable({ 
			"scrollY": "800px",
			"scrollCollapse": true,
			"responsive": true,
			//"searching": false,
			"language": {
				"emptyTable": "ไม่มีข้อมูล!"
			},
			"pageLength": 10, 
			'columns': [
				{ 'data':'no' },
				{ 'data':'link_click' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_topic'},
				{ 'data':'mr_work_remark'},
				{ 'data':'main_sys_time'},
				{ 'data':'time_mail_re' },
				{ 'data':'sla_date_end' },
				{ 'data':'time_log' },
				{ 'data':'name_get' },
				{ 'data':'mr_status_name' },
				{ 'data':'mr_type_work_name' }
			]
		});
{% endblock %}


{% block javaScript %}
	 function getDetail_level1(){
			var date_1 = $('#date_1').val();
			var date_2 = $('#date_2').val();
			var type = $("input[name=type1]:checked").val();
			var branch_type = $("input[name=branch_type]:checked").val();
			if(date_1 == '' || date_2 == ''){
				alertify.alert("กิดข้อผิดผลาด !","กรุณาเลือกช่วงวันที่");
				return;	
			}
			if(type == 'ho'){
				$('#box_branch_type').hide();
				//console.log('hode');
			}else{
				$('#box_branch_type').show();
			}
			//console.log(type);
			$.ajax({
				method: "POST",
				dataType: "json",
				url: "ajax/ajax_getReport_level1.php",
				data: { 
					date_1		: date_1, 
					date_2		: date_2,
					type		: type,
					branch_type	: branch_type
				},
				beforeSend: function( xhr ) {
					$('.loading').show();
					$('#Detail_level1').hide();
					$('#Detail_level2').hide();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert('เกิดข้อผิดผลาด !');
					//location.reload();

				  }
			})
			.done(function( msg ) {
					$('.loading').hide();
					$('#Detail_level2').fadeIn('slow');
					$('#content_level2').html(msg['html']);
					$('#date_1_2').val(msg['date_1'])
					$('#date_2_2').val(msg['date_2'])
				  //alert( "Data Saved: " + msg );
			});
	}


	function getDetail_level3(status,branch_type_re){
		var date_1 = $('#date_1').val();
		var date_2 = $('#date_2').val();
		var type = $("input[name=type1]:checked").val();
		var branch_type = $("input[name=branch_type]:checked").val();

		if(type == 'ho'){
			$('#box_branch_type').hide();
			//console.log('hode');
		}else{
			$('#box_branch_type').show();
		}
		//console.log(type);
		$.ajax({
			method: "POST",
			dataType: "json",
			url: "ajax/ajax_getReport_level3.php",
			data: { 
				date_1				: date_1, 
				date_2				: date_2,
				status				: status,
				type				: type,
				branch_type			: branch_type,
				branch_type_re		: branch_type_re,
			},
			beforeSend: function( xhr ) {
				$('.loading').show();
				$('#Detail_level1').hide();
				$('#Detail_level2').hide();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert('เกิดข้อผิดผลาด !');
				//alertify.alert("กิดข้อผิดผลาด !");
				//location.reload();

			  }
		})
		.done(function( msg ) {
				$('.loading').hide();
				$('#Detail_level3').fadeIn('slow');
				$('#head_level3').html(msg['html_head']);
				$('#tb_level3').DataTable().clear().draw();
				$('#tb_level3').DataTable().rows.add(msg['data']).draw();

				var input1 = $("<input>", { type: "hidden", name: "status", 		value: status }); 
				var input2 = $("<input>", { type: "hidden", name: "branch_type_re",value: branch_type_re }); 
				$('#form_level_3').append(input1); 
				$('#form_level_3').append(input2); 


			//alert( "Data Saved: " + msg );
		});
}

	function click_back(){
		$('#Detail_level1').show();
		$('#Detail_level2').hide();
	}
	function click_backlevel3(){
		$('#Detail_level2').show();
		$('#Detail_level3').hide();
	}
	function click_branch_type_chang(){
		var branch_type = $("input[name=branch_type]:checked").val();
		//console.log(branch_type);
		if(branch_type == 3){
			$('#tb_branch_1').fadeIn(1000);
			$('#tb_branch_2').hide();
		}else{
			$('#tb_branch_2').fadeIn(1000);
			$('#tb_branch_1').hide();
		}

	}

	function click_export_level_1(){
			var date_1 = $('#date_1').val();
			var date_2 = $('#date_2').val();
			var type = $("input[name=type1]:checked").val();
			var branch_type = $("input[name=branch_type]:checked").val();


		var input1 = $("<input>", { type: "hidden", name: "date_1", 	value: date_1 }); 
		var input2 = $("<input>", { type: "hidden", name: "date_2", 	value: date_2 }); 
		var input3 = $("<input>", { type: "hidden", name: "type", 		value: type }); 
		var input4 = $("<input>", { type: "hidden", name: "branch_type",value: branch_type }); 

		$('#form_level_1').append(input1); 
		$('#form_level_1').append(input2); 
		$('#form_level_1').append(input3); 
		$('#form_level_1').append(input4); 
		$('#form_level_1').submit();
	}

function n_export(){
	 $('#form_submit_report').attr('action', "excel.payment_report_n.php").submit();
}

function click_export_level_3(){
	
			var date_1 = $('#date_1').val();
			var date_2 = $('#date_2').val();
			var type = $("input[name=type1]:checked").val();
			var branch_type = $("input[name=branch_type]:checked").val();


		var input1 = $("<input>", { type: "hidden", name: "date_1", 	value: date_1 }); 
		var input2 = $("<input>", { type: "hidden", name: "date_2", 	value: date_2 }); 
		var input3 = $("<input>", { type: "hidden", name: "type", 		value: type }); 
		var input4 = $("<input>", { type: "hidden", name: "branch_type",value: branch_type }); 

		$('#form_level_3').append(input1); 
		$('#form_level_3').append(input2); 
		$('#form_level_3').append(input3); 
		$('#form_level_3').append(input4); 
		$('#form_level_3').submit();
	}



{% endblock %}





{% block Content2 %}
<br>
<div id ="Detail_level1">
<div class="row justify-content-center">
	<div class="col-lg-4 col-12">
		<h2 class="text-center">ดึงรายงาน</h2>
	</div>
</div>
<br>
<form id="form_submit_report" action="excel.payment_report.php" method="post" target="_blank">
<div class="row justify-content-center">
	<div class="col-lg-4 col-12">
		<div class="input-daterange" id="datepicker" data-date-format="yyyy-mm-dd">
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่เริ่มต้น</label>
				<input type="text" class="input-sm form-control" id="date_1" name="date_1"  placeholder="From date" autocomplete="off" required/>
			</div>
			<br>
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่สิ้นสุด</label>
				<input type="text" class="input-sm form-control" id="date_2" name="date_2" placeholder="To date" autocomplete="off" required/>
			</div>
		</div>
		<br>
		<div class="form-group">
			<label for="type_work">ประเภทการส่ง</label>
			<select class="type_work" id="type_work" name="type_work[]" multiple="multiple" style="width:100%;">
			<option value="1">รับส่งภายใน</option>
			<option value="2">ส่งที่สาขา</option>
			<option value="3">ส่งที่สำนักงานใหญ่</option>
			<option value="7">BY HAND IN</option>
			<option value="4">BY HAND OUT</option>
			<option value="5">THAI POST IN</option>
			<option value="6">THAI POST OUT</option>
			<option value="9">Return Branch</option>
			<option value="8">เอกสารจากบุคคลภายนอก</option>
			</select>
		</div>
		
		
	</div>
</div>
<div class="row justify-content-center">
	<div class="col-lg-2 col-12">
		<div class="form-group">
			<label for="type_branch">ประเภทสาขา</label>
			<select class="type_work" id="type_branch" name="type_branch[]" multiple="multiple" style="width:100%;">
				<option value="">ทั้งหมด</option>
				{% for bt in branchType_data %}
					<option value="{{ bt.mr_branch_type_id }}">{{bt.branch_type_name}}</option>
				{% endfor %}
			</select>
		</div>
	</div>
	<div class="col-lg-2 col-12">
		<div class="form-group">
			<label for="branch">ชื่อสาขา</label>
			<select class="type_work" id="branch" name="branch[]" multiple="multiple" style="width:100%;">
				<option value="">ทั้งหมด</option>
				{% for b in branch_data %}
					<option value="{{ b.mr_branch_id }}">{{b.mr_branch_code}}:  {{b.mr_branch_name}}</option>
				{% endfor %}
			</select>
		</div>
	</div>
</div>
<br>
<br>
<hr>
<div class="row justify-content-center">
		<button type="submit" class="btn btn-primary btn-lg">EXPORT EXCEL</button>
		<button type="button" onclick="n_export();" class="btn btn-primary btn-lg">EXPORT EXCEL (N)</button>
</div>
</form>
</div>

<div class='loading' style="display: none;">
    <img src="../themes/images/loading.gif" class='img_loading'>
</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

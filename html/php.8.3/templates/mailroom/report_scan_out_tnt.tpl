{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}

.loading {
	position: fixed;
	top: 40%;
	left: 50%;
	-webkit-transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%);
	transform: translate(-50%, -50%);
	z-index: 1000;
}

.img_loading {
	  width: 350px;
	  height: auto;
	  
}
body {
        height:100%;
    }
    .table-hover tbody tr.hilight:hover {
        background-color: #555;
        color: #fff;
    }

    table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
    }

    table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}
	#tb_work_order tbody tr {
    height: 70px; 
}



{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}

var today = new Date();
var todayFormatted = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

$('.input-daterange').datepicker({
    todayHighlight: true,
    autoclose: true,
    clearBtn: true
});

$('#date_1').datepicker('setDate', todayFormatted);
$('#date_2').datepicker('setDate', todayFormatted);
		
	

{% endblock %}


{% block javaScript %}

function getdatatoexport(){
	var date_1 = $('#date_1').val();
	var date_2 = $('#date_2').val();
	if(date_1 == ""){
		$('#alertdate1').text('กรอกวันที่เริ่มต้น').css('color', 'red');
	}else if(date_2 == ""){
		$('#alertdate2').text('กรอกวันที่สิ้นสุด').css('color', 'red');
	}else{
		$.ajax({
			method: "POST",
			dataType: "json",
			url: "ajax/ajax_getreport_scan_out_tnt.php",
			data: { 
				date_1				: date_1, 
				date_2				: date_2,
			},
				beforeSend: function(xhr) {
                    $('.loading').show();
                    $('#tb_work_order').hide();
                },
                success: function(response) {
                    $('.loading').hide();
                    $('#tb_work_order').show();
                    var table = $('#tb_work_order').DataTable();
            		table.destroy(); 
            		$('#tb_work_order').show(); 
            		table = $('#tb_work_order').DataTable({
            		    data: response.data, 
            		    columns: [
            		        { 'data':'no' },
            		        { 'data':'mr_work_barcode' },
            		        { 'data':'barcode_tnt' },
            		        { 'data':'sys_date' },
            		        { 'data':'mr_round_name' },
							{ 'data':'send_emp_code' },
            		        { 'data':'send_emp_name'},       		        
            		        { 'data':'send_department_code' },
            		        { 'data':'send_department_name' },
            		        { 'data':'send_branch_code' },
            		        { 'data':'send_branch_name' },
							{ 'data':'re_emp_code'},
            		        { 'data':'re_emp_name'},
            		        { 'data':'re_department_code'},
            		        { 'data':'re_department_name'},
            		        { 'data':'re_branch_code'},
            		        { 'data':'re_branch_name'},
            		    ],
            		    "scrollY": "800px",
            		    "scrollCollapse": true,
            		    "responsive": true,
            		    "searching": true,
						"scrollX": true,
            		    "language": {
            		        "emptyTable": "ไม่มีข้อมูล!"
            		    },
            		    "pageLength": 10
            		});
					$('#alertdate1').remove();
					$('#alertdate2').remove();
                },
                error: function(xhr, status, error) {
                    $('.loading').hide();
                    alert('Error: ' + error);
                }
            });
	}
}

{% endblock %}



{% block Content2 %}
<br>
<div id ="Detail_level1">
<div class="row justify-content-center">
	<div class="col-lg-4 col-12">
		<h2 class="text-center">รายงาน ส่งออกTNT.</h2>
	</div>
</div>
<br>

<form id="form_submit_report" action="excel_out_tnt.php" method="post">
{# <div class="row justify-content-center">
	<div class="col-lg-4 col-12">
		<div class=" input-group">
			<label class="input-group-addon" style="border:0px;">ประเภทงาน</label>
			<select class="form-control" id="mr_type_work" name="mr_type_work" required>
				<option value="6" selected>ปณ.ขาออก</option>
				<option value="5">ปณ.ขาเข้า</option>
			  </select>
		</div>
	</div>
</div> #}
<br>
<div class="row justify-content-center">
	<div class="col-lg-4 col-12">
		<div class="input-daterange" id="datepicker" data-date-format="yyyy-mm-dd">
			
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่เริ่มต้น</label>
				<input type="text" class="input-sm form-control" id="date_1" name="date_1"  placeholder="From date" autocomplete="off" required/>
				<label id="alertdate1"></label>
			</div>
			<br>
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่สิ้นสุด</label>
				<input type="text" class="input-sm form-control" id="date_2" name="date_2" placeholder="To date" autocomplete="off" required/>
				<label id="alertdate2"></label>
			</div>
		</div>
	</div>
</div>
<br>
<br>
<hr>
<div class="row justify-content-center">
		<button type="button" id="search" onclick="getdatatoexport();" class="btn btn-primary btn-lg" style="margin: 20px;">ค้นหา</button>
		<button type="submit" class="btn btn-primary btn-lg" style="margin: 20px;">EXPORT EXCEL</button>
</div>
</form>
</div>
<hr>
<div>
<table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order" style="display: none;">
    <thead>
        <tr>
            <th>#</th>
			<th>บาร์โค้ด</th>
            <th>หมายเลขTNT</th>    
            <th>วันที่ เวลา (สแกน)</th>
            <th>รอบ</th>
			<th>รหัสพนักงานผู้ส่ง</th>
            <th>ชื่อผู้ส่ง</th>
            <th>รหัสแผนกผู้ส่ง</th>
            <th>ชื่อแผนกผู้ส่ง</th>
            <th>รหัสสาขาผู้ส่ง</th>
            <th>ชื่อสาขาผู้ส่ง</th>
			<th>รหัสพนักงานผู้รับ</th>
            <th>ชื่อผู้รับ</th>
            <th>รหัสแผนกผู้รับ</th>
            <th>ชื่อแผนกผู้รับ</th>
            <th>รหัสสาขาผู้รับ</th>
            <th>ชื่อสาขาผู้รับ</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>
</div>
<div class='loading' style="display: none;">
    <img src="../themes/images/loading.gif" class='img_loading'>
</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

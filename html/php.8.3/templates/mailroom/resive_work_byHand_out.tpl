{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>

		<link rel="stylesheet" href="../themes/jquery/jquery-ui.css">
		<script src="../themes/jquery/jquery-ui.js"></script>

		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
#btn_add_emp_modal{
	height: 27px !important;
	line-height: 0 !important;
}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}


{% endblock %}

{% block domReady %}	
$('#date_send').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	
$('#date_report').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	


  $('#myform_data_senderandresive').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'date_send': {
			required: true
		},
		'type_send': {
			required: true
		},
		'round': {
			required: true
		},
		'emp_id_send': {
			//required: true
		},
		'dep_id_send': {
			required: true
		},
		'name_re': {
			required: true
		},
		'lname_re': {
			//required: true
		},
		'tel_re': {
			required: true
		},
		'address_re': {
			required: true
		},
		'sub_district_re': {
			//required: true
		},
		'district_re': {
			//required: true
		},
		'province_re': {
			//required: true
		},
		'post_code_re': {
			//required: true
		},
		'topic': {
			required: true
		},
		'quty': {
			required: true,
		},
	  
	},
	messages: {
		'date_send': {
		  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
		},
		'type_send': {
		  required: 'กรุณาระบุ ประเภทการส่ง'
		},
		'round': {
		  required: 'กรุณาระบุ รอบจัดส่ง'
		},
		'emp_id_send': {
		  required: 'กรุณาระบุ ผู้ส่ง'
		},
		'dep_id_send': {
			required: 'กรุณาระบุ หน่วยงานผู้ส่ง'
		  },
		'name_re': {
		  required: 'กรุณาระบุ ชื่อผู้รับ'
		},
		'lname_re': {
		  required: 'กรุณาระบุ นามสกุลผู้รับ'
		},
		'tel_re': {
		  required: 'กรุณาระบุ เบอร์โทร'
		},
		'address_re': {
		  required: 'กรุณาระบุ ที่อยู่'
		},
		'sub_district_re': {
		  required: 'กรุณาระบุ แขวง/ตำบล'
		},
		'district_re': {
		  required: 'กรุณาระบุ เขต/อำเภอ'
		},
		'province_re': {
		  required: 'กรุณาระบุ จังหวัด'
		},
		'post_code_re': {
		  required: 'กรุณาระบุ รหัสไปรษณีย์'
		},
		'topic': {
		  required: 'กรุณาระบุ หัวเรื่อง'
		},
		'quty': {
		  required: 'กรุณาระบุ จำนวน',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });
  $('#myform_data_update').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'date_send': {
			required: true
		},
		'type_send': {
			required: true
		},
		'round': {
			required: true
		},
		'messenger_user_id': {
			required: true
		},
		
	  
	},
	messages: {
		'date_send': {
		  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
		},
		'type_send': {
		  required: 'กรุณาระบุ ประเภท.'
		},
		'round': {
		  required: 'กรุณาระบุ รอบการจัดส่ง.'
		},
		'messenger_user_id': {
		  required: 'กรุณาระบุ แมสเซ็นเจอร์.'
		},
		
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });



$('#btn_update').click(function() {
	if($('#myform_data_update').valid()) {
		var form = $('#myform_data_update');
		 //var serializeData = form.serializeArray();
		 var serializeData = form.serialize();
		 $.ajax({
		   method: "POST",
		   dataType:'json',
		   url: "ajax/ajax_save_resive_Work_byhand.php",
		   data: serializeData,
		   beforeSend: function() {
			   // setting a timeout
			   $("#bg_loader").show();
		   },
		   error: function (error) {
			 alert('error; ' + eval(error));
			 $("#bg_loader").hide();
		   // location.reload();
		   }
		 })
		 .done(function( res ) {
		   $("#bg_loader").hide();
		   if(res['status'] == 505){
			   //console.log(res);
			   $('#csrf_token').val(res['token']);
			   alertify.alert('ผิดพลาด',"  "+res.message
			   ,function(){
				   //window.location.reload();
			   });
		   }else if(res['status'] == 200){
			   load_data_bydate();
			   $('#csrf_token').val(res['token']);
			   $('#myform_data_update')[0].reset();
			   

			   //token
		   }else{
			   alertify.alert('ผิดพลาด',"  "+res.message
			   ,function(){
				   window.location.reload();
			   });
		   }
		 });
	
   }

});
$('#btn_save').click(function() {
	
	if($('#myform_data_senderandresive').valid()) {
	 	var form = $('#myform_data_senderandresive');
		  //</link>var serializeData = form.serializeArray();
      	var serializeData = form.serialize();
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_save_resive_Work_byhand.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				load_data_bydate();
				$('#csrf_token').val(res['token']);
				if($("#reset_send_form").prop("checked") == false){
					reset_send_form();
				}
				if($("#reset_resive_form").prop("checked") == false){
					reset_resive_form();
				}
				if($("#reset_resive_form").prop("checked") == false && $("#reset_send_form").prop("checked") == false){
					reset_resive_form();
					reset_send_form();
				}
				

				//token
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});




$('#myform_data_add_emp').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'emp_code_add': {
			required: true
		},
		'emp_mail_add': {
			required: true
		},
		'emp_name_add': {
			required: true
		},
		'emp_lname_add': {
			required: true
		},
		'dep_id_add': {
			required: true
		}
	  
	},
	messages: {
		'emp_code_add': {
		  required: 'กรุณาระบุ รหัสพนักงาน.'
		},
		'emp_mail_add': {
		  required: 'กรุณาระบุ อีเมลล์'
		},
		'emp_name_add': {
		  required: 'กรุณาระบุ ชื่อพนักงาน'
		},
		'emp_lname_add': {
		  required: 'กรุณาระบุ นามสกุล'
		},
		'dep_id_add': {
			required: 'กรุณาระบุ หน่วยงาน'
		}
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });

  $('#btn_add_emp').click(function() {
	
	if($('#myform_data_add_emp').valid()) {
	 	var form = $('#myform_data_add_emp');
		  //</link>var serializeData = form.serializeArray();
      	var serializeData = form.serialize();
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_save_employee_page_byhand.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			 location.reload();
			}
		  })
		  .done(function( res ) {

			$("#bg_loader").hide();
			setTimeout(function(){ 
				$('#success').hide();
				$('#error').hide();
			}, 3000);
			if(res['st'] == 'error'){
				$('#error').text(res['msg']);
				$('#success').hide();
				$('#error').show();
			}else{
				$('#success').text(res['msg']);
				$('#success').show();
				$('#error').hide();
			}
		  });
     
	}
});


$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
$district: $('#sub_district_re'), // input ของตำบล
  $amphoe: $('#district_re'), // input ของอำเภอ
  $province: $('#province_re'), // input ของจังหวัด
  $zipcode: $('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      $('#receiver_sub_districts_code').val('');
      $('#receiver_districts_code').val('');
      $('#receiver_provinces_code').val('');

      if(data) {
          $('#sub_districts_code_re').val(data.district_code);
          $('#districts_code_re').val(data.amphoe_code);
          $('#provinces_code_re').val(data.province_code);
		  //console.log(data);
      }
      
  }
});


$('#dep_id_add').select2();
$('#dep_id_send').select2();
$('#emp_id_send').select2({
	placeholder: "ค้นหาผู้ส่ง",
	ajax: {
		url: "./ajax/ajax_getdataemployee_select_search.php",
		dataType: "json",
		delay: 250,
		processResults: function (data) {
			return {
				results : data
			};
		},
		cache: true
	}
}).on('select2:select', function(e) {
	console.log(e.params.data.id);
	setForm(e.params.data.id);
});
$('#messenger_user_id').select2({
	placeholder: "ค้นหาผู้ส่ง",
	ajax: {
		url: "./ajax/ajax_getmessenger_select_search.php",
		dataType: "json",
		delay: 250,
		processResults: function (data) {
			return {
				results : data
			};
		},
		cache: true
	}
}).on('select2:select', function(e) {
	//console.log(e.params.data.id);
});

var tbl_data = $('#tb_keyin').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'action'},
        {'data': 'mr_work_byhand_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_round_name'},
        {'data': 'mr_cus_name'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'},
        {'data': 'mess'}
    ]
});

load_data_bydate();

$('#work_barcode').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				var barcode = $(this).val();
				$.ajax({
					url: './ajax/ajax_get_data_bybacose_Byhand.php',
					type: 'POST',
					data: {
						barcode	: barcode,
						page	: 'getdataBybarcode',
						csrf_token	: $('#csrf_token').val()
					},
					dataType: 'json',
					success: function(res) {
						$('#csrf_token').val(res['token']);

						//console.log("++++++++++++++");
						if(res['status'] == 501){
							console.log(res);
						}else if(res['status'] == 200){
							if(res['count']>0){
								set_form_val_all(res['data'])
							}else{
								$('#btn-show-form-edit').attr('disabled','disabled');
								reset_price_form();
								reset_resive_form();
								reset_send_form();
								$("#detail_sender").val('');
								$("#detail_receiver").val('');
								$("#work_barcode").val('');
								$("#work_barcode").focus();
							}
						}else{
							alertify.alert('ผิดพลาด',"  "+res.message,function(){window.location.reload();});
						}
					}
				}); 
			}
		  });	
function setForm(emp_code) {
			var emp_id = parseInt(emp_code);
			console.log(emp_id);
			$.ajax({
				url: './ajax/ajax_autocompress_name.php',
				type: 'POST',
				data: {
					name_receiver_select: emp_id
				},
				dataType: 'json',
				success: function(res) {
					console.log("++++++++++++++");
					if(res['status'] == 501){
						console.log(res);
					}else if(res['status'] == 200){
						$("#emp_send_data").val(res.text_emp);
						$("#dep_id_send").val(res.data.mr_department_id).trigger('change');
						console.log(res.data.mr_department_id);
					}else{
						alertify.alert('ผิดพลาด',"  "+res.message,function(){window.location.reload();});
					}
				}
			})
		}

		$("#name_re").autocomplete({
            source: function( request, response ) {
                
                $.ajax({
                    url: "ajax/ajax_getcustommer_search.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
						console.log(data);
                    }
                });
            },
            select: function (event, ui) {
                $('#name_re').val(ui.item.fullname); // display the selected text
                $('#address_re').val(ui.item.mr_address); // save selected id to input
                $('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
				//console.log(ui.item);
                return false;
            },
            focus: function(event, ui){
                $( "#name_re" ).val( ui.item.fullname );
                $( "#address_re" ).val( ui.item.mr_address );
                $( "#tel_re" ).val( ui.item.mr_cus_tel );
                return false;
            },
        });
{% endblock %}
{% block javaScript %}

function chang_dep_id() {
	var dep = $('#dep_id_send').select2('data'); 
	var emp = $('#emp_id_send').select2('data'); 
	
	console.log(emp);
	if(emp[0].id!=''){
		$("#emp_send_data").val(emp[0].text+'\\n'+dep[0].text);
	}else{
		$("#emp_send_data").val(dep[0].text);
	}
	
}

function load_data_bydate() {
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_load_Work_resive_byHand_out.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();

		}
	  });
}

function reset_send_form() {
	$('input[name="type_send"]').attr('checked', false);
	$('#round').val("");
	$('#emp_id_send').val("").trigger('change');
	$('#emp_send_data').val("");
}

function cancle_work(id) {
	alertify.confirm('ยืนยันการลบ', 'กด "OK" เพื่อยกเลิกการส่ง',
	function(){ 
		$.ajax({
		method: "POST",
		dataType:'json',
		data:{
			id 		: id,
			page	:'cancle'
		},
		url: "ajax/ajax_load_Work_resive_byHand_out.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
			load_data_bydate();
	  });
	  
	}, function(){ 
		alertify.error('Cancel')
	});
}
function reset_resive_form() {
	$("#name_re").val("");
	$("#lname_re").val("");
	$("#tel_re").val("");
	$("#address_re").val("");
	$("#sub_districts_code_re").val("");
	$("#districts_code_re").val("");
	$("#provinces_code_re").val("");
	$("#sub_district_re").val("");
	$("#district_re").val("");
	$("#province_re").val("");
	$("#post_code_re").val("");
	$("#quty").val("1");
	$("#topic").val("");
	$("#work_remark").val("");
}
function print_option(type){
	console.log(type);
	if(type == 1 ){
		console.log(11);
		$("#form_print").attr('action', 'print_peper_byhand.php');
		$('#form_print').submit();
	}else if(type == 2){
		console.log(22);
		$("#form_print").attr('action', 'print_cover_byhand.php');
		$('#form_print').submit();
	}else{
		alert('----');
	}

}
function set_form_val_all(data) {
	$('#round').val(data.mr_round_id);
	$('#quty').val(data.quty);
	$('#topic').val(data.topic);
	$('#work_remark').val(data.work_remark);
	$('#update_emp_send_data').val(data.mr_send_emp_detail);
	$("input[value="+data.mr_work_byhand_type_id+"]").attr('checked', true); 
	$('#update_mr_work_main_id').val(data.mr_work_main_id);
	$('#update_mr_work_byhand_id').val(data.mr_work_byhand_id);


	var detail_receiver = '';
	detail_receiver += data.mr_cus_name;
	detail_receiver += ' '+data.mr_cus_lname;
	detail_receiver += ' '+data.mr_address;
	detail_receiver += ' '+data.mr_sub_districts_name;
	detail_receiver += ' '+data.mr_districts_name;
	detail_receiver += ' '+data.mr_provinces_name;
	detail_receiver += ' '+data.mr_post_code;
	detail_receiver += '  โทร: '+data.mr_cus_tel;
	$('#update_emp_resive_data').val(detail_receiver);

}

{% endblock %}
{% block Content2 %}
<div  class="container-fluid">



  <br>
  <div class="tab-content" id="myTabContent">
	<div class="tab-pane fade show active" id="tab-update" role="tabpanel" aria-labelledby="home-tab">
	<form id="myform_data_update">
		<input type="hidden" id="update_mr_work_main_id" name="update_mr_work_main_id">
		<input type="hidden" id="update_mr_work_byhand_id" name="mr_work_byhand_id">
		<input type="hidden" name="csrf_token" id="csrf_token" value="{{csrf_token}}">
		<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>รับงาน BY HAND</b></h3></label><br>
				<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
		   </div>	
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-4">
		<input type="text" class="form-control form-control-sm" id="work_barcode" name="work_barcode" placeholder="Enter Barcode">
		</div>
	</div>

	<div>
			<div class="row">
				<div class="col-md-6">
					<h5 class="card-title">รอบการนำส่งเอกสารประจำวัน</h5>
					<h5 class="card-title">ข้อมูลรายละเอียดงาน</h5>
					<div class="table-responsive">
						<table style="width: 100%;" class="">
							<tr>
								<td style="width: 180px;" ><span class="text-muted font_mini" >วันที่นำส่ง:</span></td>
								<td>
									<span class="box_error" id="err_date_send"></span>
									<input data-error="#err_date_send" name="date_send" id="date_send" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
								</td>
							  </tr>
							<tr>
								<th class="align-top"><span class="text-muted font_mini" >ประเภทการรับส่ง: </span></td>
								<td>
									<span class="box_error" id="err_type_send"></span><br>
									<input data-error="#err_type_send" type="radio" id="type_send1" value="1" name="type_send" class=""><span class="text-muted font_mini" > ส่งเอกสาร </span><br>
									<input data-error="#err_type_send" type="radio" id="type_send2" value="2" name="type_send" class=""><span class="text-muted font_mini" > รับเอกสาร</span><br>
									<input data-error="#err_type_send" type="radio" id="type_send3" value="3" name="type_send" class=""><span class="text-muted font_mini" > ส่งและรับเอกสารกลับ</span>
								</td>
							  </tr>
							<tr>
								<td><span class="text-muted font_mini" >รอบการรับส่ง:</span></td>
								<td>
								<span class="box_error" id="err_round"></span>
								<select data-error="#err_round" class="form-control form-control-sm" id="round" name="round">
										<option value="">กรุณาเลือกรอบ</option>
										{% for r in round %}
										<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
										{% endfor %}
									</select>
								</td>
							  </tr>
						</table>
					  </div>
					  <hr>
					 

					  <hr>
					  <h5 class="card-title">แมสเซ็นเจอร์ (ทีวิ่งงานงาน)</h5>
					  <div class="table-responsive">
						<table class="" style="width: 100%;">
							<tr>
								<td style="width: 180px;"><span class="text-muted font_mini" >ชื่อแมส:</span></td>
								<td>
									<span class="box_error" id="err_messenger_user_id"></span>
									<select data-error="#err_messenger_user_id" class="form-control form-control-sm" id="messenger_user_id" name="messenger_user_id" style="width:100%;">
										<option value="">1</option>
										<option value="1">2</option>
										<option value="2">2</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td><br>
										<input type="radio" value="1" id="mr_work_byhand_payment_type_id1" name="mr_work_byhand_payment_type_id" checked>  ราคาปกติ  
										<input type="radio" value="2" id="mr_work_byhand_payment_type_id2" name="mr_work_byhand_payment_type_id">  ราคาจ่ายแบบ Spare  
								</td>
							</tr>
						</table>
					  </div>
					  <br>
					  <br>
					  <br>
					  <br>
					  <div class="col-12 text-center">
					  	<button type="button" class="btn btn-outline-primary btn-sm" id="btn_update">บันทึกข้อมูล </button>
					  </div>
					  <br>
					  <br>
					  <br>
					  <br>
				</div>
				<div class="col-md-6">
					<h5 class="card-title">ข้อมูลผู้สั่งงาน</h5>
					<div class="table-responsive">
					  <table class="" style="width: 100%;">
						  <tr>
							  <td class="align-top"><span class="text-muted font_mini" >รายละเอียดผู้สั่งงาน: </span></td>
							  <td>
								  <textarea class="form-control" id="update_emp_send_data" name="update_emp_send_data" rows="3" readonly></textarea>
							  </td>
							</tr>
							<tr>
								<td style="width: 180px;" ><span class="text-muted font_mini" >รายละเอียดผู้ผู้รับ:</span></td>
								<td>
									<textarea class="form-control" id="update_emp_resive_data" name="update_emp_resive_data" rows="3" readonly></textarea>
								</td>
							  </tr>
					  </table>
					</div>
						<h5 class="card-title">ข้อมูลเอกสาร</h5>
						<table style="width: 100%;">
							<tr>
								<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >จำนวน:</span></td>
								<td>
									<span class="box_error" id="err_quty"></span>
									<input id="quty" name="quty" data-error="#err_quty"  value="1" class="form-control form-control-sm" type="number" placeholder="-" readonly>
								</td>
							</tr>
							<tr>
								<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >หัวเรื่อง:</span></td>
								<td>
									<span class="box_error" id="err_topic"></span>
									<input id="topic" name="topic" data-error="#err_topic"  class="form-control form-control-sm" type="text" placeholder="-" readonly>
								</td>
							</tr>
							<tr>
								<td class="align-top"><span class="text-muted font_mini" >รายละเอียด/หมายเหตุ:</span></td>
								<td>
									<span class="box_error" id="err_work_remark"></span>
									<textarea id="work_remark" name="work_remark" data-error="#"  class="form-control" id="" rows="4" readonly></textarea>
								</td>
							</tr>
							
							<tr>
						</table>
					  </div><br>
					  <div class="form-group text-center">
					<br>
					<br>
						
						{#
						<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_send_form();">ล้างข้อมูลผู้ส่ง </button>
						<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_resive_form();">ล้างข้อมูลผู้รับ </button>
						#}

					</div>
				</div>	
			
		</div>
	</form>
	</div>
	<div class="tab-pane fade" id="tab-key" role="tabpanel" aria-labelledby="profile-tab">
	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>สั่งงาน BY HAND</b></h3></label><br>
				<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
		   </div>	
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<form id="myform_data_senderandresive">
					<div class="row">
						<div class="col-md-6">
							<h5 class="card-title">รอบการนำส่งเอกสารประจำวัน</h5>
							<h5 class="card-title">ข้อมูลรายละเอียดงาน</h5>
							<div class="table-responsive">
								<table style="width: 100%;" class="">
									<tr>
										<td style="width: 180px;" ><span class="text-muted font_mini" >วันที่นำส่ง:</span></td>
										<td>
											<span class="box_error" id="err_date_send"></span>
											<input data-error="#err_date_send" name="date_send" id="date_send" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
										</td>
									</tr>
									<tr>
										<th class="align-top"><span class="text-muted font_mini" >ประเภทการรับส่ง: </span></td>
										<td>
											<span class="box_error" id="err_type_send"></span><br>
											<input data-error="#err_type_send" type="radio" id="type_send1" value="1" name="type_send" class=""><span class="text-muted font_mini" > ส่งเอกสาร </span><br>
											<input data-error="#err_type_send" type="radio" id="type_send2" value="2" name="type_send" class=""><span class="text-muted font_mini" > รับเอกสาร</span><br>
											<input data-error="#err_type_send" type="radio" id="type_send3" value="3" name="type_send" class=""><span class="text-muted font_mini" > ส่งและรับเอกสารกลับ</span>
										</td>
									</tr>
									<tr>
										<td><span class="text-muted font_mini" >รอบการรับส่ง:</span></td>
										<td>
										<span class="box_error" id="err_round"></span>
										<select data-error="#err_round" class="form-control form-control-sm" id="round" name="round">
												<option value="">กรุณาเลือกรอบ</option>
												{% for r in round %}
												<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
												{% endfor %}
											</select>
										</td>
									</tr>
								</table>
							</div>
							<hr>
							<h5 class="card-title">ข้อมูลผู้สั่งงาน</h5>
							<div class="table-responsive">
								<table class="" style="width: 100%;">
									<tr>
										<td style="width: 180px;"><span class="text-muted font_mini" >รหัสพนักงาน:</span></td>
										<td>
											
											<div class="input-group input-group-sm">
												<span class="box_error" id="err_emp_id_send"></span><br>
												<select data-error="#err_emp_id_send" class="form-control form-control-sm" id="emp_id_send" name="emp_id_send" style="width:100%;">
													<option value="">ค้นหาพนักงาน</option>
												</select>	
												
											</div>
										</td>
									</tr>
									<tr>
										<td style="width: 180px;"><span class="text-muted font_mini" >รหัสหน่วยงาน:</span></td>
										<td>

											<span class="box_error" id="err_dep_id_send"></span>
												<select data-error="#err_dep_id_send" class="form-control form-control-sm" id="dep_id_send" name="dep_id_send" onchange="chang_dep_id();" style="width:100%;">
													<option value="" selected disabled >กรุณาเลือกหน่วยงาน</option>
													{% for d in department %}
													<option value="{{ d.mr_department_id }}">{{ d.mr_department_code }} - {{ d.mr_department_name }}</option>
													{% endfor %}
												</select>	
											
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >รายละเอียดผู้สั่งงาน: </span></td>
										<td>
											<textarea class="form-control" id="emp_send_data" name="emp_send_data" rows="3" readonly></textarea>
										</td>
									</tr>
									<tr>
								</table>
							</div>

							<hr>
							<h5 class="card-title">แมสเซ็นเจอร์ (ทีวิ่งงานงาน)</h5>
							<div class="table-responsive">
								<table class="" style="width: 100%;">
									<tr>
										<td style="width: 180px;"><span class="text-muted font_mini" >ชื่อแมส:</span></td>
										<td>
											<span class="box_error" id="err_messenger_user_id"></span>
											<select data-error="#err_messenger_user_id" class="form-control form-control-sm" id="messenger_user_id" name="messenger_user_id" style="width:100%;">
												<option value="">1</option>
												<option value="1">2</option>
												<option value="2">2</option>
											</select>
										</td>
									</tr>
									
									<tr>
								</table>
							</div><br>
							
							
							
						</div>
						
						



						<div class="col-md-6">
							<h5 class="card-title">ข้อมูลผู้รับ</h5>
							<div class="table-responsive">
								<table class="" style="width: 100%;">
									<tr>
										<td style="width: 180px;" ><span class="text-muted font_mini" >ชื่อผู้รับ:</span></td>
										<td>
											<span class="box_error" id="err_name_re"></span>
											<input name="name_re" id="name_re" data-error="#err_name_re" class="form-control form-control-sm" type="text" placeholder="นาย ณัฐวุฒิ">
										</td>
									</tr>

									<tr>
										<td class="align-top"><span class="text-muted font_mini" >นามสกุล:</span></td>
										<td>
											<span class="box_error" id="err_lname_re"></span>
											<input id="lname_re" name="lname_re" data-error="#err_lname_re"  class="form-control form-control-sm" type="text" placeholder="สามพ่วงบุญ" val="">
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >เบอร์มือถือ:</span></td>
										<td>
											<span class="box_error" id="err_tel_re"></span>
											<input id="tel_re" name="tel_re" data-error="#err_tel_re" class="form-control form-control-sm" type="text" placeholder="-">
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >ที่อยู่​:</span></td>
										<td>
											<span class="box_error" id="err_address_re"></span>
											<textarea id="address_re" name="address_re" data-error="#err_address_re"  class="form-control"  rows="2">-</textarea>
											<input type="hidden" name="sub_districts_code_re" id="sub_districts_code_re">
											<input type="hidden" name="districts_code_re" id="districts_code_re">
											<input type="hidden" name="provinces_code_re" id="provinces_code_re">
										</td>
									</tr>
								
									<tr>	
										<td class="align-top"><span class="text-muted font_mini" >แขวง/ตำบล:</span></td>
										<td>
											<span class="box_error" id="err_sub_district_re"></span>
											<input id="sub_district_re" name="sub_district_re" data-error="#err_sub_district_re"  class="form-control form-control-sm" type="text" placeholder="-">
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >เขต/อำเภอ:</span></td>
										<td>
											<span class="box_error" id="err_district_re"></span>
											<input id="district_re" name="district_re" data-error="#err_district_re"  class="form-control form-control-sm" type="text" placeholder="-">
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >จังหวัด:</span></td>
										<td>
											<span class="box_error" id="err_province_re"></span>
											<input id="province_re" name="province_re" data-error="#err_province_re"  class="form-control form-control-sm" type="text" placeholder="-">
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >รหัสไปรษณีย์:</span></td>
										<td>
											<span class="box_error" id="err_post_code_re"></span>
											<input id="post_code_re" name="post_code_re" data-error="#err_post_code_re"  class="form-control form-control-sm" type="text" placeholder="-">
										</td>
									</tr>
								</table>
								<hr>
								<h5 class="card-title">ข้อมูลเอกสาร</h5>
								<table style="width: 100%;">
									<tr>
										<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >จำนวน:</span></td>
										<td>
											<span class="box_error" id="err_quty"></span>
											<input id="quty" name="quty" data-error="#err_quty"  value="1" class="form-control form-control-sm" type="number" placeholder="-">
										</td>
									</tr>
									<tr>
										<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >หัวเรื่อง:</span></td>
										<td>
											<span class="box_error" id="err_topic"></span>
											<input id="topic" name="topic" data-error="#err_topic"  class="form-control form-control-sm" type="text" placeholder="-">
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >รายละเอียด/หมายเหตุ:</span></td>
										<td>
											<span class="box_error" id="err_"></span>
											<textarea id="work_remark" name="work_remark" data-error="#"  class="form-control" id="" rows="4"></textarea>
										</td>
									</tr>
									
									<tr>
								</table>
							</div><br>
							<div class="form-group text-center">
							
								<input class="" type="checkbox" id="reset_send_form" value="option1">คงข้อมูลผู้ส่ง
								<input class="" type="checkbox" id="reset_resive_form" value="option1">คงข้อมูลผู้รับ
							<br>
							<br>
								<button type="button" class="btn btn-outline-primary btn-sm" id="btn_save">บันทึกข้อมูล </button>
								{#
								<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_send_form();">ล้างข้อมูลผู้ส่ง </button>
								<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_resive_form();">ล้างข้อมูลผู้รับ </button>
								#}

							</div>
						</div>	
					</div>
				</form>
			</div>					
		</div>
	</div>
	</div>

	</div>
	</div>


<div class="row">
	<div class="col-md-12 text-right">
	<form id="form_print" action="pp.php" method="post" target="_blank">
		<hr>
		<table>
			<tr>
				<td>
					<span class="box_error" id="err_round"></span>
					<select data-error="#err_round" class="form-control form-control-sm" id="round_printreper" name="round_printreper">
						<option value="">กรุณาเลือกรอบ</option>
						{% for r in round %}
						<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
						{% endfor %}
					</select>
				</td>
				<td>
					<span class="box_error" id="err_date_report"></span>
					<input data-error="#err_date_report" name="date_report" id="date_report" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
				</td>
				<td>
					<button onclick="print_option(1);" type="button" class="btn btn-sm btn-outline-secondary" id="">พิมพ์ใบงาน</button>
					<button onclick="print_option(2);" type="button" class="btn btn-sm btn-outline-secondary" id="">พิมพ์ใบคุม</button>
				</td>
			</tr>
		</table>
	</form>
	</div>	
</div>



<div class="row">
	<div class="col-md-12">
		<hr>
		<h5 class="card-title">รายการเอกสาร</h5>
		<table class="table" id="tb_keyin">
			<thead class="thead-light">
			  <tr>
				<th width="5%" scope="col">#</th>
				<th width="5%" scope="col">Update</th>
				<th width="5%" scope="col">ประเภท</th>
				<th width="10%" scope="col">วันที่</th>
				<th width="10%" scope="col">เลขที่เอกสาร</th>
				<th width="10%" scope="col">สถานะ</th>
				<th width="10%" scope="col">ชื่อผู้ส่ง</th>
				<th width="10%" scope="col">รอบ</th>
				<th width="10%" scope="col">ผู้รับ</th>
				<th width="15%" scope="col">ที่อยู่</th>
				<th width="30%"scope="col">เบอร์โทร</th>
				<th width="30%"scope="col">หมายเหตุ</th>
				<th width="30%"scope="col">แมส</th>
			  </tr>
			</thead>
			<tbody>
		
			</tbody>
		</table>
	</div>
</div>
</div>





<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>






<!-- Modal -->
<div class="modal fade" id="add_emp_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">เพิ่มข้อมูลพนักงาน</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form id="myform_data_add_emp">
			
			
			<div class="form-row">
				
				<div class="col">
					<span class="box_error" id="err_emp_code_add"></span>
				</div>
				<div class="col">
					<span class="box_error" id="err_emp_mail_add"></span>
				</div>
			</div>
			<div class="form-row">
				<div class="col-3">
					<input id="emp_code_add" name="emp_code_add" data-error="#err_emp_code_add"  type="text" class="form-control form-control-sm" placeholder="รหัสพนักงาน">
				</div>
				<div class="col-9">
					<input id="emp_mail_add" name="emp_mail_add" data-error="#err_emp_mail_add" type="text" class="form-control form-control-sm" placeholder="อีเมลล์">
				</div>
			</div>
			<br>
			<div class="form-row">
				<div class="col">
				<span class="box_error" id="err_emp_name_add"></span>
				</div>
				<div class="col">
				<span class="box_error" id="err_emp_lname_add"></span>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
				<input id="emp_name_add" name="emp_name_add" data-error="#err_emp_name_add" type="text" class="form-control form-control-sm" placeholder="ชื่อ">
				</div>
				<div class="col">
				<input id="emp_lname_add" name="emp_lname_add" data-error="#err_emp_lname_add" type="text" class="form-control form-control-sm" placeholder="นามสกุล">
				</div>
			</div>
			<br>
			<div class="form-row">
				<div class="col">
				 <span class="box_error" id="err_dep_id_add"></span>
				<select data-error="#err_dep_id_add" class="form-control form-control-sm" id="dep_id_add" name="dep_id_add" onchange="chang_dep_id();" style="width:100%;">
					<option value="" selected disabled >กรุณาเลือกหน่วยงาน</option>
					{% for d in department %}
					<option value="{{ d.mr_department_id }}">{{ d.mr_department_code }} - {{ d.mr_department_name }}</option>
					{% endfor %}
				</select>
				</div>
			</div>
			
			<input type="hidden" name="page" id="page" value="add_emp">
		</form>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
        <button id="btn_add_emp" type="button" class="btn btn-primary">บันทึกข้อมูล</button>
			 
      </div>
	  <div class="form-row">
		<div class="col">
			<div id="error" class="alert alert-danger" role="alert" style="display: none;">
				...
			  </div>
			<div id="success" class="alert alert-success" role="alert" style="display: none;">
				...
			</div>
		</div>
	</div>
    </div>
  </div>
</div>





{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

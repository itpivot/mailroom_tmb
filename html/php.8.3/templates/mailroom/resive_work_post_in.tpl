{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>

		<link rel="stylesheet" href="../themes/jquery/jquery-ui.css">
		<script src="../themes/jquery/jquery-ui.js"></script>

		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

{% endblock %}

{% block domReady %}	

//console.log('55555');

$('#date_send').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	
$('#date_import').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	
$('#date_report').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	

  $('#myform_data_thaipost').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'work_barcode': {
			required: true
		},
		'date_send': {
			required: true
		},
		'round': {
			required: true
		}	  
	},
	messages: {
		'work_barcode': {
		  required: 'กรุณาระบุ barcode.'
		},
		'date_send': {
		  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
		},
		'round': {
		  required: 'กรุณาระบุ รอบจัดส่ง'
		},
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });


$('#btn-show-form-edit').click(function() {
	$('#div_detail_receiver_sender').hide();
	$('#acction_update').hide();
	$('#acction_edit').show();
	$('#div_sender').show();
	$('#div_receiver').show();
	$("#page").val('edit');
});
$('#btn_cancle_edit').click(function() {
	$('#div_detail_receiver_sender').show();
	$('#acction_update').show();
	$('#acction_edit').hide();
	$('#div_sender').hide();
	$('#div_receiver').hide();
	$("#page").val('update_status');
});

$('#btn_save').click(function() {
	
	if($('#myform_data_senderandresive').valid()) {
	 	var form = $('#myform_data_senderandresive');
		  //</link>var serializeData = form.serializeArray();
      	var serializeData = form.serialize();
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_save_resive_Work_post_out.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 401){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}else if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				//load_data_bydate();
				$('#csrf_token').val(res['token']);
				if($("#reset_price_form").prop("checked") == false){
					reset_price_form();
				}
				$("#detail_sender").val('');
				$("#detail_receiver").val('');
				$("#work_barcode").val('');
				$("#work_barcode").focus();
				
				

				//token
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});
$('#btn_edit').click(function() {
	
	if($('#myform_data_senderandresive').valid()) {
	 	var form = $('#myform_data_senderandresive');
		  //</link>var serializeData = form.serializeArray();
      	var serializeData = form.serialize();
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_save_resive_Work_post_out.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 401){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}else if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				alertify.alert('สำเร็จ',"  แก้ไขข้อมูลเรียบร้อยแล้ว"
				,function(){
					window.location.reload();
				});
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});


$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
$district: $('#sub_district_re'), // input ของตำบล
  $amphoe: $('#district_re'), // input ของอำเภอ
  $province: $('#province_re'), // input ของจังหวัด
  $zipcode: $('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      $('#receiver_sub_districts_code').val('');
      $('#receiver_districts_code').val('');
      $('#receiver_provinces_code').val('');

      if(data) {
          $('#sub_districts_code_re').val(data.district_code);
          $('#districts_code_re').val(data.amphoe_code);
          $('#provinces_code_re').val(data.province_code);
		  //console.log(data);
      }
      
  }
});

$('#dep_id_send').select2();
$('#cost_id').select2();
$('#emp_id_send').select2({
	placeholder: "ค้นหาผู้ส่ง",
	ajax: {
		url: "./ajax/ajax_getdataemployee_select_search.php",
		dataType: "json",
		delay: 250,
		processResults: function (data) {
			return {
				results : data
			};
		},
		cache: true
	}
}).on('select2:select', function(e) {
	console.log(e.params.data.id);
	setForm(e.params.data.id);
});
$('#messenger_user_id').select2({
	placeholder: "ค้นหาผู้ส่ง",
	ajax: {
		url: "./ajax/ajax_getmessenger_select_search.php",
		dataType: "json",
		delay: 250,
		processResults: function (data) {
			return {
				results : data
			};
		},
		cache: true
	}
}).on('select2:select', function(e) {
	//console.log(e.params.data.id);
});

var tbl_data = $('#tb_keyin').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'is_ch'},
        {'data': 'barcode'},
        {'data': 'date_send'},
        {'data': 'mr_round_name'},
		{ 'data':'detail_work' },
		{ 'data':'remark' }
    ]
});
$('#select-all').on('click', function(){
	// Check/uncheck all checkboxes in the table
	var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
	$('input[type="checkbox"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
			var emp_id = parseInt(emp_code);
			console.log(emp_id);
			$.ajax({
				url: './ajax/ajax_autocompress_name.php',
				type: 'POST',
				data: {
					name_receiver_select: emp_id
				},
				dataType: 'json',
				success: function(res) {
					console.log("++++++++++++++");
					if(res['status'] == 501){
						console.log(res);
					}else if(res['status'] == 200){
						$("#emp_send_data").val(res.text_emp);
						$("#dep_id_send").val(res.data.mr_department_id).trigger('change');
					}else{
						alertify.alert('ผิดพลาด',"  "+res.message,function(){window.location.reload();});
					}
				}
			})
		}

		$("#name_re").autocomplete({
            source: function( request, response ) {
                
                $.ajax({
                    url: "ajax/ajax_getcustommer_WorkPost.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
						console.log(data);
                    }
                });
            },
            select: function (event, ui) {
                $('#name_re').val(ui.item.name); // display the selected text
                $('#lname_re').val(ui.item.lname); // display the selected text
                $('#address_re').val(ui.item.mr_address); // save selected id to input
                $('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
				$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
				$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
				$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
				$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
				$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
				$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
				$('#post_code_re').val(ui.item.zipcode); // save selected id to input
				$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
		
		$('#work_barcode').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				if($('#myform_data_thaipost').valid()) {
					var barcode = $(this).val();
					
					alertify.confirm('ยืนยันการบันทึก', 'กด "OK" ',
					function(){ 
							$.ajax({
								url: './ajax/ajax_save_bacose_thaipost_in.php',
								type: 'POST',
								data: {
									barcode	: barcode,
									page	: 'saveBarcode',
									csrf_token	: $('#csrf_token').val(),
									date_send	: $('#date_send').val(),
									round	: $('#round').val()
								},
								dataType: 'json',
								success: function(res) {
									$('#csrf_token').val(res['token']);

									//console.log("++++++++++++++");
									if(res['status'] == 501){
										console.log(res);
									}else if(res['status'] == 200){
										if(res['error']==''){
											$('#err_work_barcode').html('');
											$('#work_barcode').val('');
											$('#work_barcode').removeClass('is-invalid');
											load_data_bydate();
										}else{
											$('#work_barcode').addClass('is-invalid');
											$('#err_work_barcode').html(res['error']);
											$('#err_button').html(res['button']);
											$('#btn-show-form-edit').attr('disabled','disabled');
											$("#work_barcode").focus();
										}
									}else{
										alertify.alert('ผิดพลาด',"  "+res.message,function(){window.location.reload();});
									}
								}
							}); 
						}, function(){ 
							alertify.error('Cancel')
							return;
						});
					
				}
			}
		  });	

		  $("#work_barcode").keypress(function(event){
			var ew = event.which;
			if(ew == 32)
				return true;
			if(48 <= ew && ew <= 57)
				return true;
			if(65 <= ew && ew <= 90)
				return true;
			if(97 <= ew && ew <= 122)
				return true;
			return false;
		});
		
		
{% endblock %}
{% block javaScript %}

function save_newdata(){
	var barcode = $('#work_barcode').val();
	if (barcode.length === 13) {
		alertify.confirm('ยืนยันการบันทึก', 'กด "OK" ',
		function(){
			$.ajax({
				url: './ajax/ajax_save_bacose_thaipost_in.php',
				type: 'POST',
				data: {
					barcode	: barcode,
					page	: 'savenewbarcode',
					csrf_token	: $('#csrf_token').val(),
					date_send	: $('#date_send').val(),
					round	: $('#round').val()
				},
				dataType: 'json',
				success: function( result ) {
							if(result.status == '200'){
								$('#csrf_token').val(result['token']);
								alert(result.message);
								location.reload();
							}else{
								alert(result.message);
								location.reload();
							}
						}
			});
		},function(){ 
			alertify.error('Cancel')
			return;
		});
	}else{
		$('#work_barcode').focus();
		$('#err_barcode_13').text('กรุณากรอกให้ครบ 13 หลัก');
	}
}

function load_data_bydate() {
	$('#tb_keyin').DataTable().clear().draw();
	var form = $('#form_print');
	var serializeData = form.serialize();
	$.ajax({
		method: "POST",
		dataType:'json',
		data:serializeData,
		url: "ajax/ajax_save_bacose_thaipost_in.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$('#csrf_token').val(res['token'])
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();

		}
	  });
}


function cancle_work(id) {
	alertify.confirm('ยืนยันการลบ', 'กด "OK" ',
	function(){ 
		$.ajax({
		method: "POST",
		dataType:'json',
		data:{
			id 		: id,
			page	:'cancle',
			csrf_token	: $('#csrf_token').val(),
		},
		url: "ajax/ajax_save_bacose_thaipost_in.php",
		beforeSend: function() {
			$("#bg_loader").show();
		},
		error: function (error) {
		  	alert('error; ' + eval(error));
		  	$("#bg_loader").hide();
		 	location.reload();
		}
	  })
	  .done(function( res ) {
		$('#csrf_token').val(res['token'])
		$("#bg_loader").hide();
		load_data_bydate();
	  });
	  
	}, function(){ 
		alertify.error('Cancel')
	});
}




function cancelwork() {
	var dataall = [];
	var tbl_data = $('#tb_keyin').DataTable();
	tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		dataall.push(this.value);
	});

	if(dataall.length < 1){
		alertify.alert("เกิดข้อผิดพลาด","ท่านยังไม่เลือกรายการ"); 
		return;
	}
	alertify.confirm('ยืนยันการลบ', 'กด "OK" ',
	function(){ 
		var newdataall = dataall.join(",");
		$.ajax({
		method: "POST",
		dataType:'json',
		data:{
			id 		: newdataall,
			page	:'cancle_multiple',
			csrf_token	: $('#csrf_token').val(),
		},
		url: "ajax/ajax_save_bacose_thaipost_in.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$('#csrf_token').val(res['token'])
		$("#bg_loader").hide();
		load_data_bydate();
	  });
	  
	}, function(){ 
		alertify.error('Cancel')
	});
	

}


function import_excel() {
	$('#div_error').hide();	
	var formData = new FormData();
	var date_send = $('#date_send2').val();
	var round = $('#round2').val();


	formData.append('date_send', date_send);
	formData.append('round', round);

	formData.append('file', $('#file')[0].files[0]);
	if($('#file').val() == ''){
		$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
		$('#div_error').show();
		return;
	}else{
	 var extension = $('#file').val().replace(/^.*\./, '');
	 if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
		 $('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
		$('#div_error').show();
		return;
	 }
	}
	$.ajax({
		   url : 'ajax/ajax_readFile_barcode_Thai_post.php',
		   dataType : 'json',
		   type : 'POST',
		   data : formData,
		   processData: false,  // tell jQuery not to process the data
		   contentType: false,  // tell jQuery not to set contentType
		   success : function(data) {

			if(data.status == 200){
				console.log('-------');
				if(data.data.count_error > 0){
					$('#div_error').html(data.data.txt_error)
					$('#div_error').show();
				}
				$('#bg_loader').hide();
				$('#file').val('');
				load_data_bydate();
			}else{
				console.log('22222222222');
				console.log(data.data.txt_error);
				$('#div_error').html(data.data.txt_error)
				$('#div_error').show();
				$('#bg_loader').hide();
			}
		}, beforeSend: function( xhr ) {
			$('#bg_loader').show();
		}
	});

}

function chang(mr_round_resive_thaipost_in_id,remark){
	$('#modal-update-route').modal({
		keyboard: false,
		backdrop: 'static'
	});
	$('#remark').val(remark);
	$('#mr_round_resive_thaipost_in_id').val(mr_round_resive_thaipost_in_id);
}

function update_data(){
		var remark 						= $('#remark').val();
		var mr_round_resive_thaipost_in_id 		= $('#mr_round_resive_thaipost_in_id').val();
		var token								=$('#csrf_token').val();
			$.ajax({
				url: "ajax/ajax_save_bacose_thaipost_in.php",
				dataType: "json",
				method: "POST",
				data: {
						remark	 								: remark , 
						mr_round_resive_thaipost_in_id	 		: mr_round_resive_thaipost_in_id 	, 
						page 									: 'saveremark' ,
						csrf_token		: $('#csrf_token').val(),
				},
				success: function( result ) {
					if(result.status == '200'){
						$('#csrf_token').val(result['token']);
						alert(result.message);
						location.reload();
					}else{
						alert(result.message);
						location.reload();
					}
				}
	});
}

{% endblock %}
{% block Content2 %}

<div  class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>ยิงรับเอกสารจาก ไปรษณีย์ไทย</b></h3></label><br>
				
		   </div>	
		</div>
	</div>
	  
	<div class="row">
		<div class="col-md-12">
			  <div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="key" role="tabpanel" aria-labelledby="key-tab">
					<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-body">
										<form id="myform_data_thaipost">
											<div class="form-row">
												<div class="col">
													<h5 class="card-title">รอบการนำส่งเอกสารประจำวัน</h5>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-3">
													<label for="date_send"><span class="text-muted font_mini" >วันที่นำส่ง:<span class="box_error" id="err_date_send"></span></span></label>
													<input data-error="#err_date_send" name="date_send" id="date_send" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
												  </div>
												 <div class="form-group col-md-3">
													<label for="round"><span class="text-muted font_mini" >รอบการนำส่ง:</span></label>
													<span class="box_error" id="err_round"></span>
													<select data-error="#err_round" class="form-control form-control-sm" id="round" name="round">
														<option value="" selected disabled>กรุณาเลือกรอบ</option>
														{% for r in round %}
														<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
														{% endfor %}
													</select>
												  </div>
												</div>
											
												<div class="form-row">
													<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"  data-backdrop="static">
														Upload Excel ป.ณ.
													  </button>
													<div class="form-group col-md-6">
														<label for="work_barcode"><span class="text-muted font_mini" >Barcode: <span class="box_error" id="err_work_barcode"></span></span></label>&nbsp;<span class="box_error" id="err_button"></span>
														<input data-error="#err_work_barcode" name="work_barcode" id="work_barcode" class="form-control form-control-sm" type="text" value="" placeholder="Enter Barcode">
														<span class="box_error" id="err_barcode_13"></span>
													</div>
												</div>
											</form>
										</div>
												
							</div>
						</div>
					</div>
				</div>
			  </div>
		</div>
	</div>
	

<div class="row">
	<div class="col-md-12 text-right">
	<form id="form_print" action="pp.php" method="post" target="_blank">
		<hr>
		<input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf_token}}"></input>
		<table>
			<tr>
				<td>
					<span class="box_error" id="err_round"></span>
					<select data-error="#err_round" class="form-control form-control-sm" id="round_printreper" name="round_printreper">
						<option value="">กรุณาเลือกรอบ</option>
						{% for r in round %}
						<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
						{% endfor %}
					</select>
				</td>
				
				<td>
					<span class="box_error" id="err_date_report"></span>
					<input data-error="#err_date_report" name="date_report" id="date_report" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
				</td>
				<td>
					<button onclick="load_data_bydate();" type="button" class="btn btn-sm btn-outline-secondary" id="">ค้นหา</button>
					<!-- <button onclick="print_option(1);" type="button" class="btn btn-sm btn-outline-info" id="">พิมพ์ใบงาน</button> -->
				<!-- 				
					<button onclick="print_option(2);" type="button" class="btn btn-sm btn-outline-secondary" id="">พิมพ์ใบคุม</button>
				-->
				</td>
			</tr>
		</table>
	</form>
	</div>	
</div>



<div class="row">
	<div class="col-md-12">
		<hr>
		<h5 class="card-title">รายการเอกสาร</h5>
		<table class="table" id="tb_keyin">
			<thead class="thead-light">
			  <tr>
				<th width="5%" scope="col">#</th>
			
				<th width="5%" scope="col">
							<label class="custom-control custom-checkbox">
								<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description"></span>
							</label>
					  </div>
					
				</th>
				<th width="5%" scope="col">
					<button onclick="cancelwork();" type="button" class="btn btn-danger btn-sm">
						ลบรายการ
					</button>
				</th>
				<th width="5%" scope="col">ตรวจรับงาน</th>
				<th width="5%" scope="col">เลขที่เอกสาร</th>
				<th width="10%" scope="col">วันที่จะส่ง</th>
				<th width="10%" scope="col">รอบ</th>
				<th>รายละเอียดงาน</th>
				<th>หมายเหตุ</th>
			  </tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
</div>





<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="date_send"><span class="text-muted font_mini" >วันที่นำส่ง:<span class="box_error" id="err_date_send"></span></span></label>
					<input data-error="#err_date_send" name="date_send2" id="date_send2" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
				  </div>
				 <div class="form-group col-md-6">
					<label for="round"><span class="text-muted font_mini" >รอบการนำส่ง:</span></label>
					<span class="box_error" id="err_round2"></span>
					<select data-error="#err_round2" class="form-control form-control-sm" id="round2" name="round2">
						<option value="" selected disabled>กรุณาเลือกรอบ</option>
						{% for r in round %}
						<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
						{% endfor %}
					</select>
				  </div>
				</div>
			<div class="form-row">
				<div class="card w-100">
					<div class="card-body">
						<div class="col-md-12 col-lg-12">	  
							<div class="form-group">
								<a onclick="$('#modal_showdata').modal({ backdrop: false});" data-toggle="tooltip" data-placement="top" title="เลือกไฟล์ Excel ของท่าน"></a>
								<input type="file" class="form-control-file" id="file">
							</div>
							<br>
							<a  href="template_th_post.xlsx" type="button" class="btn btn-dark" download> 	
								<i class="material-icons">vertical_align_bottom</i>template
							</a>
						</div>
					</div>
				</div>
			</div>

			
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  <button onclick="import_excel();" id="btn_fileUpload" type="button" class="btn btn-success"> 	
			<i class="material-icons">vertical_align_top</i> upload
		</button>
		</div>
		<div class="form-row"><div class="col-md-12 col-lg-12 p-2"><div id="div_error"></div></div></div>
	  </div>
	</div>
  </div>

{# modal chang #}
<div class="modal fade" id="modal-update-route" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog  modal-lg">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">พบเอกสารหรือไม่</h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="form-group col-md-6">
					<label for="Route">กรุณาเลือกเอกสาร</label>
				<br>
					<select id="remark" name="remark" class="form-control form-control-sm">
                        <option value="" disabled selected>เลือกตัวเลือก</option>
                        <option value="เอกสารสำเร็จ">เอกสารสำเร็จ</option>
                        <option value="ไม่พบเอกสาร">ไม่พบเอกสาร</option>
                        <option value="ไม่มีในรายการเอการ">ไม่มีในรายการเอกการที่อัพโหลด</option>
                    </select>
				</div>
				<input type="hidden" class="form-control" id="mr_round_resive_thaipost_in_id" name="mr_round_resive_thaipost_in_id" min="0">
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="$('#modal-update-route').modal('hide');">Close</button>
		  <button type="button" class="btn btn-primary" onclick="update_data();">
			Save
		</button>
		</div>
	  </div>
	</div>
  </div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

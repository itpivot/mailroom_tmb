{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}	

var tbl_floor = $('#tb_floor').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'sys_time'},
        {'data': 'byhand_zone_name'},
        {'data': 'payment_type_name'},
		{'data': 'price'},
		{'data': 'action'}
    ]
});



  $('#mr_type_post_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {

});

  $('#add_type_post_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {

});


$( "#min_weight" ).keyup(function() {
	var min_weight = $( "#min_weight" ).val();
	var max_weight = $( "#max_weight" ).val();
	var description = min_weight+" - "+max_weight;
	//console.log(description);
	$( "#description_weight" ).val(description);
  });
$( "#max_weight" ).keyup(function() {
	var min_weight = $( "#min_weight" ).val();
	var max_weight = $( "#max_weight" ).val();
	var description = min_weight+" - "+max_weight
	$( "#description_weight" ).val(description);
  });


  
  $('#myform_data_price').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'byhand_price': {
			required: true,
			min:1,
			number: true
		}	  
	},
	messages: {
		'byhand_price': {
		  required: 'กรุณาระบุ ราคา',
		  number: 'กรุณาระบุ เป็นตัวเลข',
		  min: 'กรุณาระบุ >= 1'
		},		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });

  $('#btn-edit-price').click(function() {
	if($('#myform_data_price').valid()) {
	 	var form = $('#myform_data_price');
	 	var csrf_token = $('#csrf_token').val();
      	var serializeData = form.serialize()+"&page=editPrice&addType=edit&csrf_token="+csrf_token;
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.byhandPrice.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				$('#modaiAddPrice').modal('hide')
				alertify.alert('Success',"  "+res.message
				,function(){				
					$('#myform_data_price')[0].reset();
					load_dataPrice();
					
					$('#csrf_token').val(res['token']);
				});
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});




$('#btn-add-price').click(function() {
	if($('#myform_data_price').valid()) {
	 	var form = $('#myform_data_price');
	 	var csrf_token = $('#csrf_token').val();
      	var serializeData = form.serialize()+"&page=addPrice&csrf_token="+csrf_token;
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.byhandPrice.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				$('#modaiAddPrice').modal('hide')
				alertify.alert('Success',"  "+res.message
				,function(){				
					$('#myform_data_price')[0].reset();
					load_dataPrice();
					
					$('#csrf_token').val(res['token']);
				});
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});




load_dataPrice();
{% endblock %}
{% block javaScript %}



function load_dataPrice() {
//var data =  $('#myform_data_floor').serialize()+"&page=loadPrice";
//console.log(data);
$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_seting.byhandPrice.php?page=loadPrice",
		//data:data,
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$('#csrf_token').val(res['token']);
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_floor').DataTable().clear().draw();
			$('#tb_floor').DataTable().rows.add(res.data).draw();

		}
	  });
}

function remove_price(id) {
	alertify.confirm('ยืนยันการลบ', 'กด "OK" เพื่อลบ',
	function(){ 
		var csrf_token = $('#csrf_token').val();
		$.ajax({
				method: "POST",
				dataType:'json',
				url: "ajax/ajax_seting.byhandPrice.php",
				data:{
					id 			: id,
					page 		: 'remove',
					csrf_token 	: csrf_token,
				},
				beforeSend: function() {
					// setting a timeout
					$("#bg_loader").show();
				},
				error: function (error) {
				alert('error; ' + eval(error));
				$("#bg_loader").hide();
				// location.reload();
				}
			})
			.done(function( res ) {
				$("#bg_loader").hide();
					if(res['status'] == 505){
						//console.log(res);
						$('#csrf_token').val(res['token']);
						alertify.alert('ผิดพลาด',"  "+res.message
						,function(){
							//window.location.reload();
						});
					}else if(res['status'] == 200){
						alertify.alert('Success',"  "+res.message
						,function(){				
							load_dataPrice();
							$('#csrf_token').val(res['token']);
						});
					}else{
						alertify.alert('ผิดพลาด',"  "+res.message
						,function(){
							window.location.reload();
						});
					}
			});
		}, function(){ 
			alertify.error('Cancel')
		});
}



function edit_price(id,price) {
	$('#byhand_price_id').val(id);
	$('#byhand_price').val(price);
	var csrf_token = $('#csrf_token').val();
	$("#bg_loader").hide();
	//var data = res['data'][0];
	//console.log(data);
	$('.div-btn-add').hide();
	$('.div-btn-edit').show();
	$('#modaiAddPrice').modal({
		keyboard: false,
		backdrop:'static'
	});					
}

function setpriceonly(id) {
	var price = $("#txt-set-price-"+id).val();
	if(price<=0){
		alertify.alert('ผิดพลาด',"  กรุณาระบุ ราคามากกว่า  0 บาท"
					,function(){
						//window.location.reload();
					});
		return;
	}
	var csrf_token = $('#csrf_token').val();
	$.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.byhandPrice.php",
			data:{
				id 			: id,
				page 		: 'setpriceonly',
				price 		: price,
				csrf_token 	: csrf_token,
			},
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			alert('error; ' + eval(error));
			$("#bg_loader").hide();
			// location.reload();
			}
		})
		.done(function( res ) {
			$("#bg_loader").hide();
				if(res['status'] == 505){
					//console.log(res);
					$('#csrf_token').val(res['token']);
					alertify.alert('ผิดพลาด',"  "+res.message
					,function(){
						//window.location.reload();
					});
				}else if(res['status'] == 200){
						$('#csrf_token').val(res['token']);
						load_dataPrice();
				}else{
					alertify.alert('ผิดพลาด',"  "+res.message
					,function(){
						window.location.reload();
					});
				}
		});
}

function check_click(id) {
	$('#btn-set-save-'+id).show();
	$('#btn-reset-save-'+id).show();
	$('#btn-set-change-'+id).hide();
	$("#txt-set-price-"+id).prop('readonly', false);
}
function resetfrome(id) {
	load_dataPrice();
}


{% endblock %}
{% block Content2 %}
<div  class="container-fluid">
	<input type="hidden" name="csrf_token" id="csrf_token" value="{{csrf_token}}">
<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">



	<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="modaiAddPrice" tabindex="-1" role="dialog" aria-labelledby="modaiAddPriceTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLongTitle">แก้ไขราคา</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<form id="myform_data_price">
				<input type="hidden" name="byhand_price_id" id="byhand_price_id" value="">
				<div class="row">
					<div class="col-12 col-md-6">
						<label for="byhand_price">ราคา<span class="box_error" id="err_byhand_price"></span></label>
						<input type="number" id="byhand_price" name="byhand_price" data-error="#err_byhand_price" class="form-control form-control-sm" placeholder="-">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer div-btn-add">
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
		  <button id="btn-add-price" type="button" class="btn btn-primary">บันทึก</button>
		</div>
		<div class="modal-footer div-btn-edit">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
			<button id="btn-edit-price" type="button" class="btn btn-primary">แก้ไข</button>
		</div>
	  </div>
	</div>
  </div>



	<br>
	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>ราคางาน Byhand.</b></h3></label><br>
				<label>ราคางาน Byhand. > อัปเดทราคา </label>
		   </div>	
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<hr>
					<table class="table table-hover" id="tb_floor">
						<thead class="thead-light">
						  <tr>
							<th width="5%" scope="col">#</th>
							<th width="10%" scope="col">วันที่อัปเดท</th>
							<th width="10%" scope="col">โซน</th>
							<th width="10%" scope="col">ประเภทการจ่าย/กรัม</th>
							<th width="10%" scope="col">ราคา</th>
							<th width="10%" scope="col">จัดการ</th>
						  </tr>
						</thead>
						<tbody>
						</tbody>
					  </table>
				</div>
			</div>
		</div>
	 </div>

</div>
</div>







<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

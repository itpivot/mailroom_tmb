{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}	

$.Thailand({
	database: '../themes/jquery.Thailand.js/database/geodb.json',
  $district: $('#sub_district_re'), // input ของตำบล
	$amphoe: $('#district_re'), // input ของอำเภอ
	$province: $('#province_re'), // input ของจังหวัด
	$zipcode: $('#post_code_re'), // input ของรหัสไปรษณีย์
	onDataFill: function (data) {
		$('#receiver_sub_districts_code').val('');
		$('#receiver_districts_code').val('');
		$('#receiver_provinces_code').val('');
  
		if(data) {
			$('#sub_districts_code_re').val(data.district_code);
			$('#districts_code_re').val(data.amphoe_code);
			$('#provinces_code_re').val(data.province_code);
			console.log(data);
		}
		
	}
  });
  
var tbl_floor = $('#tb_floor').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'sys_time'},
		{'data': 'mr_sub_districts_name'},
		{'data': 'zipcode'},
		{'data': 'byhand_zone_name'},
		{'data': 'action'}
    ]
});



  $('#work_byhand_zone_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {

});

  $('#mr_work_byhand_zone_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {

});


$( "#min_weight" ).keyup(function() {
	var min_weight = $( "#min_weight" ).val();
	var max_weight = $( "#max_weight" ).val();
	var description = min_weight+" - "+max_weight;
	//console.log(description);
	$( "#description_weight" ).val(description);
  });
$( "#max_weight" ).keyup(function() {
	var min_weight = $( "#min_weight" ).val();
	var max_weight = $( "#max_weight" ).val();
	var description = min_weight+" - "+max_weight
	$( "#description_weight" ).val(description);
  });


  
  $('#myform_data_price').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'add_type_post_id': {
			required: true,
		},
		'min_weight': {
			required: true,
			number: true
		},
		'max_weight': {
			required: true,
			number: true
		},
		'description_weight': {
			required: true,
		},
		'post_price': {
			required: true,
			number: true
		},
	  
	},
	messages: {
		'add_type_post_id': {
		  required: 'กรุณาระบุ ประเภท',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'min_weight': {
		  required: 'กรุณาระบุ น้ำหนัก',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'max_weight': {
		  required: 'กรุณาระบุ น้ำหนัก',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'description_weight': {
		  required: 'กรุณาระบุ น้ำหนัก',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'post_price': {
		  required: 'กรุณาระบุ ราคาไปรษณีย์ไทย',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });

$('#btn-add-zone-location').click(function() {
	if($('#myform_data_price').valid()) {
	 	var form = $('#myform_data_price');
	 	var csrf_token = $('#csrf_token').val();
      	var serializeData = form.serialize()+"&page=addZoneLocation&csrf_token="+csrf_token;
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.byhandZone.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				$('#modaiAddPrice').modal('hide')
				alertify.alert('Success',"  "+res.message
				,function(){				
					$('#myform_data_price')[0].reset();
					load_dataZone_location();
					
					$('#csrf_token').val(res['token']);
				});
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});


$('#btn-modaiAddPrice').click(function() {
	$('#add_type_post_id').val('').trigger('change');
	$('#modaiAddPrice').modal({
		keyboard: false,backdrop:'static'
	});
	$('#myform_data_price')[0].reset();
	$('.div-btn-edit').hide();
	$('.div-btn-add').show();
	
});



load_dataZone_location();
{% endblock %}
{% block javaScript %}



function load_dataZone_location() {
var data =  $('#myform_data_floor').serialize()+"&page=loadZone_location";
//console.log(data);
$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_seting.byhandZone.php",
		data:data,
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$('#csrf_token').val(res['token']);
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_floor').DataTable().clear().draw();
			$('#tb_floor').DataTable().rows.add(res.data).draw();

		}
	  });
}

function remove_zone_location(id) {
	alertify.confirm('ยืนยันการลบ', 'กด "OK" เพื่อลบ',
	function(){ 
		var csrf_token = $('#csrf_token').val();
		$.ajax({
				method: "POST",
				dataType:'json',
				url: "ajax/ajax_seting.byhandZone.php",
				data:{
					id 			: id,
					page 		: 'remove',
					csrf_token 	: csrf_token,
				},
				beforeSend: function() {
					// setting a timeout
					$("#bg_loader").show();
				},
				error: function (error) {
				alert('error; ' + eval(error));
				$("#bg_loader").hide();
				// location.reload();
				}
			})
			.done(function( res ) {
				$("#bg_loader").hide();
					if(res['status'] == 505){
						//console.log(res);
						$('#csrf_token').val(res['token']);
						alertify.alert('ผิดพลาด',"  "+res.message
						,function(){
							//window.location.reload();
						});
					}else if(res['status'] == 200){
						alertify.alert('Success',"  "+res.message
						,function(){				
							load_dataZone_location();
							$('#csrf_token').val(res['token']);
						});
					}else{
						alertify.alert('ผิดพลาด',"  "+res.message
						,function(){
							window.location.reload();
						});
					}
			});
		}, function(){ 
			alertify.error('Cancel')
		});
}





{% endblock %}
{% block Content2 %}
<div  class="container-fluid">
	<input type="hidden" name="csrf_token" id="csrf_token" value="{{csrf_token}}">
<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">



	<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="modaiAddPrice" tabindex="-1" role="dialog" aria-labelledby="modaiAddPriceTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLongTitle">เพิ่มราคาใหม่</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<form id="myform_data_price">
			<div class="row">
			<div class="form-group col-12">
				<h5 class="card-title">ประเภท <span class="box_error" id="err_mr_work_byhand_zone_id"></span></h5>
				<select  name="mr_work_byhand_zone_id" class="form-control " id="mr_work_byhand_zone_id"  data-error="#err_mr_work_byhand_zone_id">
						<option value="" selected>Byhand Zone</option>
						{% for type in byhand_zoneData %}
						<option value="{{type.mr_work_byhand_zone_id}}"> {{type.byhand_zone_name}}</option>
						{% endfor %}
				</select>
			</div>
			</div>
			<div class="row">
			<div class="form-group col-12">
				<table width="100%">
				<tr>	
					<td class="align-top"><span class="text-muted font_mini" >แขวง/ตำบล:</span></td>
					<td>
						<span class="box_error" id="err_sub_district_re"></span>
						<input id="sub_district_re" name="sub_district_re" data-error="#err_sub_district_re"  class="form-control form-control-sm" type="text" placeholder="-">
						<input type="hidden" name="sub_districts_code_re" id="sub_districts_code_re">
						<input type="hidden" name="districts_code_re" id="districts_code_re">
						<input type="hidden" name="provinces_code_re" id="provinces_code_re">
					</td>
				</tr>
				<tr>
					<td class="align-top"><span class="text-muted font_mini" >เขต/อำเภอ:</span></td>
					<td>
						<span class="box_error" id="err_district_re"></span>
						<input id="district_re" name="district_re" data-error="#err_district_re"  class="form-control form-control-sm" type="text" placeholder="-">
					</td>
				</tr>
				<tr>
					<td class="align-top"><span class="text-muted font_mini" >จังหวัด:</span></td>
					<td>
						<span class="box_error" id="err_province_re"></span>
						<input id="province_re" name="province_re" data-error="#err_province_re"  class="form-control form-control-sm" type="text" placeholder="-">
					</td>
				</tr>
				<tr>
					<td class="align-top"><span class="text-muted font_mini" >รหัสไปรษณีย์:</span></td>
					<td>
						<span class="box_error" id="err_post_code_re"></span>
						<input id="post_code_re" name="post_code_re" data-error="#err_post_code_re"  class="form-control form-control-sm" type="text" placeholder="-">
					</td>
				</tr>
			</table>
			</div>
			</div>
			
			</form>
		</div>
		<div class="modal-footer div-btn-add">
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
		  <button id="btn-add-zone-location" type="button" class="btn btn-primary">บันทึก</button>
		</div>
		<div class="modal-footer div-btn-edit">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
			<button id="btn-edit-price" type="button" class="btn btn-primary">แก้ไข</button>
		</div>
	  </div>
	</div>
  </div>



	<br>
	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>Byhand Zone</b></h3></label><br>
				<label>Byhand Zone > อัปเดท Zone </label>
		   </div>	
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<hr>
					<form id="myform_data_floor">
						<div class="form-group col-12 col-md-3">
							<h5 class="card-title">Zone</h5>
							<select  name="work_byhand_zone_id" class="form-control " id="work_byhand_zone_id" >
							<option value="" selected>Byhand Zone</option>
								{% for type in byhand_zoneData %}
								<option value="{{type.mr_work_byhand_zone_id}}"> {{type.byhand_zone_name}}</option>
								{% endfor %}
							</select>
						</div>
						<button type="button" class="btn btn-info" onclick="load_dataZone_location()">
							<i class="material-icons" style="padding-right:5px;">search</i>
						</button>
					
						<button type="button" class="btn btn-primary" id="btn-modaiAddPrice" data-backdrop="static">
							เพิ่มข้อมูล Zone
						  </button>
					</form>
					<br>
					<br>
					<br>
					<table class="table table-hover" id="tb_floor">
						<thead class="thead-light">
						  <tr>
							<th width="5%" scope="col">#</th>
							<th width="10%" scope="col">วันที่อัปเดท</th>
							<th width="10%" scope="col">เขต</th>
							<th width="10%" scope="col">รหัสไปรษณีย์</th>
							<th width="10%" scope="col">โซน</th>
							<th width="10%" scope="col">จัดการ</th>
						  </tr>
						</thead>
						<tbody>
						</tbody>
					  </table>
				</div>
			</div>
		</div>
	 </div>

</div>
</div>







<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

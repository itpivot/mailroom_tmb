{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}	

var tbl_floor = $('#tb_department').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_department_code'},
		{'data': 'mr_department_name'},
		{'data': 'department_sysdate'},
		{'data': 'action'}
    ]
});
 $('#tb_keyin').on('click', 'tr', function () {
        var data = tbl_data.row( this ).data();
        //alert( 'You clicked on '+data['mr_round_name']+'\'s row' );
		//$('#round_name').val(data['mr_round_name']);
		//$('#type_work').val(data['mr_type_work_id']);
		//$('#mr_round_id').val(data['mr_round_id']);
    } );


$('#myTab a').on('click', function (e) {
	e.preventDefault()
	var id = $(this).attr('id');
	if(id=="profile-tab"){
		load_data_emp_floor();
	}else{
		load_data_floor();
	}
	console.log(id);
  })

  $('#mr_floor_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
});
  $('#mr_user_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
});
  $('#mr_building_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
});
  $('#mr_branch_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {
  set_option_building();
  var data = e.params.data;
	$('#mr_branch_name').val(data['text']);
});



$('#btn-save-department').click(function() {
	if($('#myform_data_department').valid()) {
	var data =  $('#myform_data_department').serialize()+"&page=save_add_department";
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_department.php",
		data:data,
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		   //alert('error; ' + eval(error));

		  alertify.alert('error',eval(error)+'!');
		  $("#bg_loader").hide();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			alertify.alert('สำเร็จ',res['message']+'!');
			if(cancel_edit()){
				load_data_department();
			}else{
				load_data_department();
			}
			
		}else{
			 alertify.alert('เกิดข้อผิดพลาด',res['message']+'!');
		  	$("#bg_loader").hide();
		}
	  });
	}
});



$('#myform_data_department').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'department_code': {
			required: true
		},
		'department_name': {
			required: true
		},
	  
	},
	messages: {
		'department_code': {
		  required: 'This field is required.  !'
		},
		'department_name': {
		  required: 'This field is required.  !'
		},
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });

load_data_department();


{% endblock %}
{% block javaScript %}
function load_data_department() {
	var data =  $('#myform_data_department').serialize()+"&page=loadDepartment";
$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_department.php",
		data:data,
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_department').DataTable().clear().draw();
			$('#tb_department').DataTable().rows.add(res.data).draw();

		}
	  });
}

function Edit_department(id,code,name) {
	$('#mr_department_id').val(id);
	$('#department_code').val(code);
	$('#department_name').val(name);
	$('#btn-cancle').show();
}
function cancel_edit() {
	$('#mr_department_id').val('');
	$('#department_code').val('');
	$('#department_name').val('');
	$('#btn-cancle').hide();
}


{% endblock %}
{% block Content2 %}

<div  class="container-fluid">
			<form id="myform_data_department">
				<br>
				<div class="row">
					<div class="col">
						<div class="">
							<label><h3><b>ข้อมูลหน่วยงาน</b></h3></label><br>
							<label>หน่วยงาน > จัดการข้อมูลหน่วยงาน</label>
					   </div>	
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-12">
										<hr>
										<h5 class="card-title">หน่วยงาน</h5>
										<table class="table table-hover" id="tb_department">
											<thead class="thead-light">
											  <tr>
												<th scope="col" colspan="2">
													<div class="form-group">
														<label for="department_code">รหัสหน่วยงาน <span class="box_error" id="err_department_code"></span></label>
														<input type="text" class="form-control" id="department_code" name="department_code" data-error="#err_department_code"  placeholder="0100200299">
														<input type="hidden" class="form-control" id="mr_department_id" name="mr_department_id">
													  </div>
												</th>
												<th scope="col" colspan="2">
													<div class="form-group">
														<label for="department_name">ชื่อหน่วยงาน <span class="box_error" id="err_department_name"></span></label>
														<input type="text" class="form-control" id="department_name" name="department_name" data-error="#err_department_name" placeholder="ประจำกิจกรรมสังคมเพื่อความยั่งยืน">
													  </div>
												</th>
												
												<th width="10%" scope="col">
													<div class="form-group">
													<button type="button" class="btn btn-info" onclick="load_data_department()">
													<i class="material-icons" style="padding-right:5px;">search</i>
													</button>
													</div>
												</th>
											  </tr>
											  <tr>
												<th width="10%" colspan="6" scope="col">
													<button id="btn-save-department" type="button" class="btn btn-success" >
														บันทึก(เพิ่มข้อมูลหน่วยงาน/แก้ไข)
													</button>
													<button style="display: none;" id="btn-cancle" type="button" class="btn btn-danger" onclick="cancel_edit();">
														ยกเลิกการแก้ไข
													</button>
												</th>
											</tr>
											  <tr>
												<th width="5%" scope="col">#</th>
												<th width="10%" scope="col">รหัสหน่วยงาน</th>
												<th width="56%" scope="col">ชื่อหน่วยงาน</th>
												<th width="10%" scope="col">วันที่อัปเดท</th>
												<th width="10%" scope="col">จัดการ</th>
											  </tr>
											</thead>
											<tbody>
										
											</tbody>
										  </table>
			
			
									</div>
								</div>
								
			
							</div>
						  </div>
					
					</div>
				</div>
			</form>
			
		</div>






<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

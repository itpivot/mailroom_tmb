{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}	

var tbl_floor = $('#tb_floor').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_type_post_name'},
		{'data': 'min_weight'},
		{'data': 'max_weight'},
		{'data': 'description'},
		{'data': 'post_price'},
		{'data': 'action'}
    ]
});



  $('#mr_type_post_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {

});

  $('#add_type_post_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {

});


$( "#min_weight" ).keyup(function() {
	var min_weight = $( "#min_weight" ).val();
	var max_weight = $( "#max_weight" ).val();
	var description = min_weight+" - "+max_weight;
	//console.log(description);
	$( "#description_weight" ).val(description);
  });
$( "#max_weight" ).keyup(function() {
	var min_weight = $( "#min_weight" ).val();
	var max_weight = $( "#max_weight" ).val();
	var description = min_weight+" - "+max_weight
	$( "#description_weight" ).val(description);
  });


  
  $('#myform_data_price').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'add_type_post_id': {
			required: true,
		},
		'min_weight': {
			required: true,
			number: true
		},
		'max_weight': {
			required: true,
			number: true
		},
		'description_weight': {
			required: true,
		},
		'post_price': {
			required: true,
			number: true
		},
	  
	},
	messages: {
		'add_type_post_id': {
		  required: 'กรุณาระบุ ประเภท',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'min_weight': {
		  required: 'กรุณาระบุ น้ำหนัก',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'max_weight': {
		  required: 'กรุณาระบุ น้ำหนัก',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'description_weight': {
		  required: 'กรุณาระบุ น้ำหนัก',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		'post_price': {
		  required: 'กรุณาระบุ ราคาไปรษณีย์ไทย',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });

  $('#btn-edit-price').click(function() {
	if($('#myform_data_price').valid()) {
	 	var form = $('#myform_data_price');
	 	var csrf_token = $('#csrf_token').val();
      	var serializeData = form.serialize()+"&page=addPrice&addType=edit&csrf_token="+csrf_token;
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.postPrice.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				$('#modaiAddPrice').modal('hide')
				alertify.alert('Success',"  "+res.message
				,function(){				
					$('#myform_data_price')[0].reset();
					load_dataPrice();
					
					$('#csrf_token').val(res['token']);
				});
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});




$('#btn-add-price').click(function() {
	if($('#myform_data_price').valid()) {
	 	var form = $('#myform_data_price');
	 	var csrf_token = $('#csrf_token').val();
      	var serializeData = form.serialize()+"&page=addPrice&csrf_token="+csrf_token;
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.postPrice.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				$('#modaiAddPrice').modal('hide')
				alertify.alert('Success',"  "+res.message
				,function(){				
					$('#myform_data_price')[0].reset();
					load_dataPrice();
					
					$('#csrf_token').val(res['token']);
				});
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});


$('#btn-modaiAddPrice').click(function() {
	$('#add_type_post_id').val('').trigger('change');
	$('#modaiAddPrice').modal({
		keyboard: false,backdrop:'static'
	});
	$('#myform_data_price')[0].reset();
	$('.div-btn-edit').hide();
	$('.div-btn-add').show();
	
});



load_dataPrice();
{% endblock %}
{% block javaScript %}



function load_dataPrice() {
var data =  $('#myform_data_floor').serialize()+"&page=loadPrice";
//console.log(data);
$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_seting.postPrice.php",
		data:data,
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$('#csrf_token').val(res['token']);
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_floor').DataTable().clear().draw();
			$('#tb_floor').DataTable().rows.add(res.data).draw();

		}
	  });
}

function remove_price(id) {
	alertify.confirm('ยืนยันการลบ', 'กด "OK" เพื่อลบ',
	function(){ 
		var csrf_token = $('#csrf_token').val();
		$.ajax({
				method: "POST",
				dataType:'json',
				url: "ajax/ajax_seting.postPrice.php",
				data:{
					id 			: id,
					page 		: 'remove',
					csrf_token 	: csrf_token,
				},
				beforeSend: function() {
					// setting a timeout
					$("#bg_loader").show();
				},
				error: function (error) {
				alert('error; ' + eval(error));
				$("#bg_loader").hide();
				// location.reload();
				}
			})
			.done(function( res ) {
				$("#bg_loader").hide();
					if(res['status'] == 505){
						//console.log(res);
						$('#csrf_token').val(res['token']);
						alertify.alert('ผิดพลาด',"  "+res.message
						,function(){
							//window.location.reload();
						});
					}else if(res['status'] == 200){
						alertify.alert('Success',"  "+res.message
						,function(){				
							load_dataPrice();
							$('#csrf_token').val(res['token']);
						});
					}else{
						alertify.alert('ผิดพลาด',"  "+res.message
						,function(){
							window.location.reload();
						});
					}
			});
		}, function(){ 
			alertify.error('Cancel')
		});
}



function edit_price(id) {
	$('#mr_post_price_id').val(id);
	var csrf_token = $('#csrf_token').val();
	$.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.postPrice.php",
			data:{
				id 			: id,
				page 		: 'loadPrice',
				csrf_token 	: csrf_token,
			},
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			alert('error; ' + eval(error));
			$("#bg_loader").hide();
			// location.reload();
			}
		})
		.done(function( res ) {
			$("#bg_loader").hide();
				if(res['status'] == 505){
					//console.log(res);
					$('#csrf_token').val(res['token']);
					alertify.alert('ผิดพลาด',"  "+res.message
					,function(){
						//window.location.reload();
					});
				}else if(res['status'] == 200){
					if(res['is_empty'] == 1){
						var data = res['data'][0];
						console.log(data);
						$('.div-btn-add').hide();
						$('.div-btn-edit').show();
						$('#modaiAddPrice').modal({
							keyboard: false,
							backdrop:'static'
						});
						$('#csrf_token').val(res['token']);
						$('#add_type_post_id').val(data['mr_type_post_id']).trigger('change');
						$('#min_weight').val(data['min_weight']);
						$('#max_weight').val(data['max_weight']);
						$('#description_weight').val(data['description']);
						$('#post_price').val(data['post_price']);
					}else{
						alertify.alert('ผิดพลาด'," ไม่พบข้อมูล "
						,function(){
							window.location.reload();
						});
					}
				

				}else{
					alertify.alert('ผิดพลาด',"  "+res.message
					,function(){
						window.location.reload();
					});
				}
		});
}

function setpriceonly(id) {
	var price = $("#txt-set-price-"+id).val();
	if(price<=0){
		alertify.alert('ผิดพลาด',"  กรุณาระบุ ราคามากกว่า  0 บาท"
					,function(){
						//window.location.reload();
					});
		return;
	}
	var csrf_token = $('#csrf_token').val();
	$.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_seting.postPrice.php",
			data:{
				id 			: id,
				page 		: 'setpriceonly',
				price 		: price,
				csrf_token 	: csrf_token,
			},
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			alert('error; ' + eval(error));
			$("#bg_loader").hide();
			// location.reload();
			}
		})
		.done(function( res ) {
			$("#bg_loader").hide();
				if(res['status'] == 505){
					//console.log(res);
					$('#csrf_token').val(res['token']);
					alertify.alert('ผิดพลาด',"  "+res.message
					,function(){
						//window.location.reload();
					});
				}else if(res['status'] == 200){
						$('#csrf_token').val(res['token']);
						load_dataPrice();
				}else{
					alertify.alert('ผิดพลาด',"  "+res.message
					,function(){
						window.location.reload();
					});
				}
		});
}

function check_click(id) {
	$('#btn-set-save-'+id).show();
	$('#btn-reset-save-'+id).show();
	$('#btn-set-change-'+id).hide();
	$("#txt-set-price-"+id).prop('readonly', false);
}
function resetfrome(id) {
	load_dataPrice();
}


{% endblock %}
{% block Content2 %}
<div  class="container-fluid">
	<input type="hidden" name="csrf_token" id="csrf_token" value="{{csrf_token}}">
<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">



	<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="modaiAddPrice" tabindex="-1" role="dialog" aria-labelledby="modaiAddPriceTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLongTitle">เพิ่มราคาใหม่</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<form id="myform_data_price">
				<input type="hidden" name="mr_post_price_id" id="mr_post_price_id" value="">
			<div class="row">
			<div class="form-group col-12">
				<h5 class="card-title">ประเภท <span class="box_error" id="err_mr_type_post_id"></span></h5>
				<select  name="add_type_post_id" class="form-control " id="add_type_post_id"  data-error="#err_mr_type_post_id">
				<option value="" selected>ประเภทการส่ง</option>
					{% for type in type_postData %}
					<option value="{{type.mr_type_post_id}}"> {{type.mr_type_post_name}}</option>
					{% endfor %}
				</select>
			</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6">
					<label for="min_weight">น้ำหนักตั้งแต่/กรัม<span class="box_error" id="err_min_weight"></span></label>
					<input id="min_weight" name="min_weight" data-error="#err_min_weight" class="form-control form-control-sm" type="text" placeholder="-">
				</div>
				<div class="col-12 col-md-6">
					
					<label for="max_weight">น้ำหนักสูงสุด/กรัม<span class="box_error" id="err_max_weight"></span></label>
					<input id="max_weight" name="max_weight" data-error="#err_max_weight"  class="form-control form-control-sm" type="text" placeholder="-">
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-12 col-md-12">
					
					<label for="description">รายละเอียด<span class="box_error" id="err_description"></span></label>
					<input id="description_weight" name="description_weight" data-error="#err_description"  class="form-control form-control-sm" type="text" placeholder="-" readonly>
				</div>
				<div class="col-12 col-md-12">
					<label for="post_price">ราคา<span class="box_error" id="err_post_price"></span></label>
					<input id="post_price" name="post_price" data-error="#err_post_price"  class="form-control form-control-sm" type="number" placeholder="-">
				</div>
				
			</div>
			</form>
		</div>
		<div class="modal-footer div-btn-add">
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
		  <button id="btn-add-price" type="button" class="btn btn-primary">บันทึก</button>
		</div>
		<div class="modal-footer div-btn-edit">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
			<button id="btn-edit-price" type="button" class="btn btn-primary">แก้ไข</button>
		</div>
	  </div>
	</div>
  </div>



	<br>
	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>ราคา ไปรษณีย์ไทย.</b></h3></label><br>
				<label>ราคาไปรษณีย์ไทย > อัปเดทราคา </label>
		   </div>	
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<hr>
					<form id="myform_data_floor">
						<div class="form-group col-12 col-md-3">
							<h5 class="card-title">ประเภท</h5>
							<select  name="mr_type_post_id" class="form-control " id="mr_type_post_id" >
							<option value="" selected>ประเภทการส่ง</option>
								{% for type in type_postData %}
								<option value="{{type.mr_type_post_id}}"> {{type.mr_type_post_name}}</option>
								{% endfor %}
							</select>
						</div>
						<button type="button" class="btn btn-info" onclick="load_dataPrice()">
							<i class="material-icons" style="padding-right:5px;">search</i>
						</button>
					
						<button type="button" class="btn btn-primary" id="btn-modaiAddPrice" data-backdrop="static">
							ต้องการเพิ่มราคาใหม่
						  </button>
					</form>
					<br>
					<br>
					<br>
					<table class="table table-hover" id="tb_floor">
						<thead class="thead-light">
						  <tr>
							<th width="5%" scope="col">#</th>
							<th width="10%" scope="col">ประเภท</th>
							<th width="10%" scope="col">น้ำหนักตั้งแต่/กรัม</th>
							<th width="10%" scope="col">น้ำหนักสูงสุด/กรัม</th>
							<th width="10%" scope="col">รายละเอียด</th>
							<th width="10%" scope="col">ราคา/บาท</th>
							<th width="10%" scope="col">จัดการ</th>
						  </tr>
						</thead>
						<tbody>
						</tbody>
					  </table>
				</div>
			</div>
		</div>
	 </div>

</div>
</div>







<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
		{{alert}}
		
		$("#btn_save").click(function(){
			var remark_success 				= $("#remark_success").val();

				if( remark_success != "" ){
				//console.log(floor);
					$.post(
						'./ajax/ajax_success_work_all_mailroom.php',
						{
							remark_success 					 : remark_success,
							work_id 					 : '{{work_id}}',
							wo_status_id 					 : '{{wo_status_id}}',
						},
						function(res) {
							if(res.status == 200){
								location.href='search.php';
							}
							alert(res.message);
							
						},'json');
						
				}else{
					alert("โปรดกรอกหมายเหตุที่สำเร็จการส่งเอกสาร");
				}
		});
		
		





		
{% endblock %}				
{% block javaScript %}
				
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
	<form>
	<input type="hidden" value="{{work_id}}">
		<div class="form-group" style="text-align: center;">
		{% if wo_status_id == 5%}
			<label><h4> ปิดงานสำเร็จนอกระบบ  :  {{count_work_id}} รายการ </h4></label>
		{% else %}
			<label><h4> ยกเลิกรายการ  :  {{count_work_id}} รายการ </h4></label>
	    {% endif %}
		</div>	
		<div class="form-group">
			<label for="exampleFormControlTextarea1">หมายเหตุงานสำเร็จนอกระบบ :</label>
			<textarea  rows="5" type="text" class="form-control form-control-sm" id="remark_success" placeholder="หมายเหตุงานสำเร็จนอกระบบ" value=""></textarea >
		</div>
	</form>
			
			
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึกการสำเร็จการส่งเอกสาร</button>
				<br>
				
			</div>
			
			
	
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

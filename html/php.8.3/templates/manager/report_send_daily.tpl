{% extends "base_emp2.tpl" %}

{% block title %} Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block Content %}
<div class="row" >
    <div class="col-1">
    </div>
    <div class="col-10">
        <div class="card" style="width:100%">
            <div class="card-body">
                <h4 class="card-title">รายงานสรุปการส่งเอกสาร</h4>
                {% for d in date %}
					<a href="excel.report_send_daily_2.php?&date={{ d.date }}" target="_blank">{{ d.date_show }}</a>
                <br />
                {% endfor %}
            </div>
        </div>
    </div>
</div>


{% endblock %}


{% block debug %}

{% if debug != '' %}
<pre>{{ debug }}</pre>
{% endif %}

{% endblock %}

{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
      }
    
      .input-daterange {
          width: 450px;
      }

      #tb_summary,
      #tb_summary th{
          text-align:center;
          font-weight: bold;
      }
{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">
<link rel="stylesheet" href="../themes/datepicker/bootstrap-datepicker3.min.css">

<script type='text/javascript' src="../themes/datepicker/bootstrap-datepicker.min.js"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>
{% endblock %}

{% block domReady %}
	$('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
	});

    getSummary();

    $('#btn_search').on('click', function() {
        let start = $('#date_start').val();
        let end = $('#date_end').val();
        getSummary(start, end);
    }); 

    $('#btn_clear').on('click', function() {
        $('#date_start').val("");
        $('#date_end').val("");
        getSummary();
    });

{% endblock %}


{% block javaScript %}
    var getSummary = function(start = '', end = '') {
        // console.log(start, end)
        $.ajax({
            url: './ajax/ajax_get_sla.php',
            method: 'POST',
            data: { start_date: start, end_date: end },
            dataType: 'json',
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(res) {
                $('.loading').hide();
               let str = '';
               if(res != "") {
                    for(let i = 0; i < res.length; i++){
                        let newly;
                        let msg_receive;
                        let mailroom;
                        let msg_send;
                        let success;
                        let cancel;
                        let total;
                        
                        str += "<tr>";
                            str += "<td>"+res[i]['no']+"</td>";
                            str += "<td>"+res[i]['mr_work_barcode']+"</td>";
                            str += "<td>"+res[i]['order_time']+"</td>";
                            str += "<td>"+res[i]['mess_re']+"</td>";
                            str += "<td>"+res[i]['success']+"</td>";
                            str += "<td>"+res[i]['SLA']+"</td>";
                           
                           
                        str += "</tr>";
                    }
                    $('#show_data').html(str);
               }
               
            }
        });
    }

    var getDetail = function(floor, status) {
        let obj = {};
        let start_date = $('#date_start').val();
        let end_date = $('#date_end').val();
        const uri = 'detail_work_inout.php?param=';
        obj.mr_department_floor = floor;
        obj.status_id = status;
        obj.start_date = start_date;
        obj.end_date = end_date;
        param = JSON.stringify(obj); // encode parameter base64
        //window.open(uri+param, '_blank', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1000,height=350');
        window.open(uri+param, '_blank');
    }
	
{% endblock %}

{% block Content2 %}
      <div class="card">
            <div class='card-header'>
               <b>ติดตามงาน</b>
            </div>
               
            <div class="card-body">
               <form class='form-inline'>
                   <label><b>ช่วงวันที่ : </b>&emsp;</label>
                   <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group input-daterange">
                            <input type="text" class="form-control" id='date_start' placeholder='วันที่เริ่มต้น'>
                            <div class="input-group-addon" style='background-color: #fff; border-color: #fff; padding: 0px 10px;'><b>ถึง</b></div>
                            <input type="text" class="form-control" id='date_end' placeholder='วันที่สิ้นสุด'>
                        </div>
                   </div>
                   <button type='button' class='btn btn-primary' id='btn_search'><b>ค้นหา</b></button>&nbsp;&nbsp;
                   <button type='button' class='btn btn-secondary' id='btn_clear'><b>เคลียร์</b></button>
               </form>
            </div>
      </div>

      <div class='card' style='margin-top: 20px;'>
        <div class='card-header'>
            <b>สถานะงาน</b>
        </div>        
        <div class='card-body'>
            <table id='tb_summary' class="table table-responsive table-bordered table-striped table-sm" cellspacing="0">
                <thead class="thead-inverse">
                    <tr>
                        <th>ลำดับ</th>
                        <th>Barcode</th>
                        <th>สั่งงาน</th>
                        <th>พนักงานเดินเอกสารรับเอกสารแล้ว</th>
                        <th>เอกสารถึงผู้รับเรียบร้อยแล้ว</th>
                        <th>Result</th>
                    </tr>
                </thead>
                <tbody id='show_data'></tbody>
            </table>
            <div class='loading'><b><center>Loading....</center></b></div>
        </div>
      </div>
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

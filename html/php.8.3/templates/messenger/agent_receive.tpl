{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}
{% block styleReady %}
@font-face {
  font-family: 'password';
  font-style: normal;
  font-weight: 400;
  src: url(../themes/password.ttf);
}

.loginpass {
  font-family: 'password';
}
{% endblock %}
{% block domReady %}
    $('#btn_save').on('click', function() {
			var emp_id 				= $("#emp_id").val();
			var user_pass 			= $("#user_pass").val();
			var work_id 			= $("#work_id").val();
			var obj = { emp_id: emp_id, work_id: work_id, user_pass: user_pass };

			$.when(confirmReceive(obj)).then(function(res) {
				if(res == 1) {
					$('#img_loading').show();
					if(confirm("ยืนยันการส่งเอกสาร!")) {
						return updateWorkOrders(work_id, emp_id);
					}
				} else {
					alert('ข้อมูลไม่ถูกต้องถูกต้อง');
					$("#emp_id").val('');
					$("#user_pass").val('');
				}
			}).done(function(data) {
				if(data == 'success') {
					$('#img_loading').hide();
					// window.location.href = "../messenger/rate_send.php?id=" + work_id;
					window.location.href = "../messenger/rate_send.php?id=" + work_id;
				} 
			});

            // $.ajax({
            //     url: './ajax/ajax_check_agent_receive.php',
            //     type: 'POST',
            //     data: {
            //         'user_pass': user_pass,
			// 		'emp_id': emp_id,
			// 		'work_id': work_id
            //     },
				
            //     success: function(res){
			// 	//console.log(res);

            //         if( res == 1 ) {
			// 				if(confirm("ยืนยันการส่งเอกสาร!")) {
			// 					 $.ajax({
			// 						url: './ajax/ajax_updateSendMailroom.php',
			// 						type: 'POST',
			// 						data: {
			// 							id: work_id
			// 						},
			// 						dataType: 'html',
			// 						cache: false,
			// 							beforeSend: function() {
			// 								$('#img_loading').show();
			// 							},
			// 						success: function(res){
			// 							//console.log(res);
			// 								if(res == "success") {
			// 									location.href = "../messenger/rate_send.php?id="+work_id;
			// 								}
			// 						},
			// 						complete: function() {
			// 							$('#img_loading').hide();
			// 						}
			// 					})
			// 				}
                       
            //         }else{
			// 			alert('ข้อมูลไม่ถูกต้องถูกต้อง');
						
			// 			$("#emp_id").val('');	
			// 			$("#user_pass").val('');
			// 		}
            //     }
            // });
        
    });
{% endblock %}

{% block javaScript %}
		var confirmReceive = function(obj) {
			return $.post('../messenger/ajax/ajax_check_agent_receive.php', obj);
		}

		var updateWorkOrders = function(id, emp_id) {
			
			return $.post('../messenger/ajax/ajax_updateSendMailroom.php', { id: id, username: emp_id });
			
		} 
{% endblock %}

{% block Content %}

				<label><h3>ยืนยันการรับเอกสารแทน</h3></label>

				{% if is_time == 'on' %}
    
						<div class="form-group">
							<label>รหัสพนักงาน</label>
							<input type="text" class="form-control" id="emp_id" placeholder="รหัสพนักงาน" value="" autocomplete="off">
							<input type="hidden" class="form-control" id="work_id" value="{{ work_id }}">
							
						</div>
						<div class="form-group">
							<label>รหัสผ่านการเข้าสู่ระบบ</label>
							<input type="text" class="form-control loginpass" id="user_pass" placeholder="text" value="" autocomplete="new-password">
						</div>
						
						<div class="row">
							<div class="col-3"></div>
							
							<div class="col-6">
								<button type="btn" class="btn btn-primary" style="width:100%;" id="btn_save">Submit</button>
							</div>
							
							<div class="col-3"></div>
						</div>
					{% else %}
						<H3 class="pt-5 text-danger"><center>ไม่อยู่ในเวลาทำการ <br> (08.00 - 18.00) !</center></H3>
					{% endif %}
			
		

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

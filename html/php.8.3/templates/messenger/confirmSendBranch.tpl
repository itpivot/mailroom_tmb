{% extends "base_emp2.tpl" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        height: 100%;
        
    }

    


    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 800px;
        overflow: auto;
        margin-bottom: 50px;
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
	
	.fixed-bottom {
		position: sticky;
		bottom: 0;
		//top: 250px;
		z-index: 1075;

	}

    .space-height p#departs {
       display: inline-block;
    }

    .fixedContainer {
        position: fixed;
        width: 100%;
        padding: 10px 10px;
        left: 0;
        bottom: 0;
    }
  

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }

{% endblock %}

{% block domReady %}
    $('#frmConfirm').submit(function(e) {
        e.preventDefault();

        $.ajax({
            url: './confirmSendBranch.php',
            type: 'POST',
            data: $(this).serialize(),
            success: function(resp) {
                if(parseInt(resp) == 1) {
                    if (confirm("ยืนยันการส่งเอกสาร!")) {
                        var result_confirm = updateWorkOrders($('#frmConfirm').serializeArray());
                        result_confirm.done(function(rs) {
                            console.log(rs)
                             if(rs == "success") {
                                 window.location.href = "rate_send_branch.php?id={% if main_id == '' %}{{ work_id|join(',') }}{% else %}{{main_id}}{% endif %}";
                                 //window.location.href = "send_branch_list.php";
                             }
                        });
                    }
                } else {
                    alert('ข้อมูลไม่ถูกต้องถูกต้อง');
                    $("#username").val('');
                    $("#password").val('');
                }
            }
        });
    });

{% endblock %}

{% block javaScript %}
    function updateWorkOrders(arr) {
        return $.post('./ajax/ajax_updateBranchReceived.php', { data: JSON.stringify(arr) });
    }

{% endblock %}

{% block Content %}

<div class="container">
    <form action="confirmSendBranch.php" method="POST" id="frmConfirm">
        <h3>ยืนยันการรับเอกสาร</h3>
        <div class="form-group">
            <label for="username">รหัสพนักงาน</label>
            <input {{txt_disabled}} type="text"  class="form-control" id="username" name="username" />
            <input type="hidden" value="{{ type }}" name="type" id="type">
            <input type="hidden" value="{{ main_id }}" name="main_id" id="main_id">
            {% for hid in work_id %}
                <input type="hidden" value="{{ hid }}" name="arrId[]" id="arrId_{{ loop.index }}">
            {% endfor %}
        </div>
        <div class="form-group">
            <label for="password">รหัสผ่าน</label>
            <input {{txt_disabled}} type="password"  class="form-control" id="password" name="password" />
        </div>
        <div class="form-group">
		{% if time_dif > 0 %}
            <button type="submit" class="btn btn-primary btn-block font-weight-bold">ยืนยันการรับเอกสาร</button>
		{% else %}
		<div class="alert alert-danger" role="alert">
		  ไม่สามารถส่งได้ : <b> เนื่องจากเลยเวลา  {{  time_end }}</b>
		</div>
		{% endif %}
		
        </div>
    </form>
</div>

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


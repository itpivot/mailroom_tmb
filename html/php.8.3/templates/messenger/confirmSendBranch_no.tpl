{% extends "base_emp2.tpl" %}

{% block title %}PivotSend List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        height: 100%;
        
    }

    


    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 800px;
        overflow: auto;
        margin-bottom: 50px;
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
	
	.fixed-bottom {
		position: sticky;
		bottom: 0;
		//top: 250px;
		z-index: 1075;

	}

    .space-height p#departs {
       display: inline-block;
    }

    .fixedContainer {
        position: fixed;
        width: 100%;
        padding: 10px 10px;
        left: 0;
        bottom: 0;
    }
  

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }

{% endblock %}

{% block domReady %}

    $('#frmConfirm').submit(function(e) {
        e.preventDefault();
        const main_id_json = htmlDecode('{{main_id}}');
        const main_id = JSON.parse(main_id_json);
        const type = Array.isArray(main_id) && main_id.length > 1 ? 'saveall' : 'saveone';
        $.ajax({
            url: './ajax/ajax_save_image_sendbranch.php', 
            type: 'POST',
            data: {
                filename : FileNameimg,
                main_id : main_id,
                type : type
            },
            success: function(response) {
                response = JSON.parse(response);
                if(response.status == "success") {
                    window.location.href = "send_branch_list.php";
                }else{
                    alert('บันทึกข้อมูลไม่สำเร็จ');
                }
            }
        });
    });
{% endblock %}

{% block javaScript %}
function htmlDecode(input) {
    var doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
}
    function updateWorkOrders(arr) {
        return $.post('./ajax/ajax_updateBranchReceived.php', { data: JSON.stringify(arr) });
    }

let FileNameimg = '';

function displaySelectedImage(event, elementId) {
    const selectedImage = document.getElementById(elementId);
    const fileInput = event.target;

    if (fileInput.files && fileInput.files[0]) {
        const reader = new FileReader();
        const file = fileInput.files[0];
        
        reader.onload = function(e) {
            selectedImage.src = e.target.result;
            selectedImage.style.display = 'block'; 

            // Display the file name
            // const fileName = file.name;
            // FileNameimg = fileName;
            const base64String = e.target.result.replace(/^data:image\/[a-zA-Z]+;base64,/, '');
            FileNameimg = base64String;
        };

        reader.readAsDataURL(file);
    } else {
        selectedImage.style.display = 'none'; 
    }
}
{% endblock %}

{% block Content %}

<div class="container">
    <form action="confirmSendbranch_no.php" method="POST" id="frmConfirm">
        <h3 class= "text-center">อัพโหลดใบคุม</h3>
        <div class="form-group">
            <div>
                <div class="mb-4 d-flex justify-content-center">
                    <img id="selectedImage" src="../themes/images/no-image.jpg"
                    alt="example placeholder" style="width: 300px;" />
                </div>
                <div class="d-flex justify-content-center">
                    <div data-mdb-ripple-init class="btn btn-primary btn-rounded">
                        <label class="form-label text-white m-1" for="customFile1">Choose file</label>
                        <input type="file" class="form-control d-none" id="customFile1" onchange="displaySelectedImage(event, 'selectedImage')" />
                    </div>
                </div>
            </div>
            <br>
            <div class="d-flex justify-content-center">
             {% if time_dif > 0 %} 
                <button type="submit" class="btn btn-success">ยืนยันการรับเอกสาร</button>
             {% else %} 
            </div>
             <div class="alert alert-danger" role="alert">
		        ไม่สามารถส่งได้ : <b> เนื่องจากเลยเวลา  {{  time_end }}</b>
		    </div>
		    {% endif %} 
        </div>
    </form>
</div>

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


{% extends "base_emp2.tpl" %}

{% block title %} Send List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}

    .card {
       margin: 8px 0px;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 70%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
    
{% endblock %}

{% block domReady %}
receive_list = {};
load_data();

    $('#txt_search').on('keyup', function() {
        load_data();
    });

    $('#btn_save_all').on('click', function() {
		if(confirm("ยืนยันการรับงานทั้งหมด!")) {
            $.ajax({
                url: './ajax/ajax_updateReceiveAll.php',
                type: 'POST',
                data: {
                    data: JSON.stringify(receive_list)
                },
                success: function(res){
                    if(res == "success") {
                        load_data();
                    }
                }
            });
		}
    });
	$('.input-daterange').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		});	
{% endblock %}

{% block javaScript %}
    function updateReceiveById(id, work_status)
    {
        
        $.ajax({
            url: './ajax/ajax_updateReceiveById.php',
            type: 'POST',
            data: {
                id: id,
                work_status: work_status
            },
            success: function(res) {
                if(res == "success") {
                    load_data();
                }
            }
        })

        
        
    }
	

	function updateNoReceiveById(id) {
        $.ajax({
            url: './ajax/ajax_updateNoReceiveById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == "success") {
                    load_data();
                }
            }
        })
    }

	function load_data() {
	receive_list = {};
        var txt = $('#txt_search').val();
        var date_s = $('#start_date').val();
        var date_e = $('#end_date').val();
        var str = "";
        $.ajax({
            url: "./ajax/ajax_get_Mailroom_work_thaipost_in.php",
            type: "POST",
            data: {
                txt		: txt,
                date_s	: date_s,
                date_e	: date_e
            },
            dataType: 'json',
            beforeSend: function() {
                $('#img_loading').show();
                $('#data_list').hide();
            },
            success: function(res){
                console.log(res);
                document.getElementById("badge-show").textContent=res.length;
                var set_style_time = '' ;
                var str = '';
                if( res != "" ){
                    for(var i = 0; i < res.length; i++) {
                        receive_list[i] = res[i]['mr_work_main_id'];
    
                        if( res[i]['diff_time'] == 1 ){
                            set_style_time = 'bg-danger';
                        }else{
                            set_style_time = '';
                        }
                            
                            if( res[i]['mr_status_receive'] == 2 ){
                                    str += '<div class="card bg-warning">'; 
                                    str += '<div class="card-body space-height">';
                                        str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
                                        str += '<p><b>เลข ปณ. : </b>'+res[i]['num_doc']+' </p>';
                                        if(res[i]['name_re']){
                                         str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
                                        }
                                         if(res[i]['mr_emp_mobile'] && res[i]['mobile_re']){
                                             str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mobile_re']+'</a> / <a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
                                          }      
                                        str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
                                        str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
                                        str += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
                                        str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
                                        str += '<hr>';
                                        str += '<div class="row">'
                                        <!-- str += '<div class="col-6 text-left btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );"><b>ยกเลิก</b></a></div>' -->
                                        str += '<div class="col-12 text-right btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById('+res[i]['mr_work_main_id']+ ', 4 );"><b>รับเอกสาร</b></a></div>'
                                        str += '</div>';
                                        str += '</div>';
                                str += '</div>';
                            }else{
        
                                str += '<div class="card '+set_style_time+'">'; 
                                    str += '<div class="card-body space-height">';
                                        str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
                                        str += '<p><b>เลข ปณ. : </b>'+res[i]['num_doc']+' </p>';
                                         if(res[i]['name_re']){
                                                str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
                                            }
                                        if(res[i]['mr_emp_mobile'] && res[i]['mobile_re']){
                                            str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mobile_re']+'</a> / <a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
                                        }
                                        str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
                                        str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
                                        str += '<p><b>วันที่ : </b>'+res[i]['mr_work_date_sent']+'</p>';
                                        str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
                                        str += '<hr>';
                                        str += '<div class="row">'
                                        str += '<div class="col-6 text-left btn-zone"><a href="#" class="btn btn-warning" onclick="updateNoReceiveById('+res[i]['mr_work_main_id']+');"><b>ไม่พบเอกสาร</b></a></div>'
                                        <!-- str += '<div class="col-4 text-right btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );"><b>ยกเลิก</b></a></div>' -->
                                        str += '<div class="col-6 text-right btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById(' + res[i]['mr_work_main_id'] + ', 4 );"><b>รับเอกสาร</b></a></div>'
                                        str += '</div>';
                                        str += '</div>';
                                str += '</div>';
                            }
        
        
                    }
					$('#btn_save_all').show();
                }else{
                    $('#btn_save_all').hide();
                    str = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                     
                }
                $('#data_list').html(str);
            },
            complete: function() {
                $('#img_loading').hide();
                $('#data_list').show();
            }
        });
	
	}
{% endblock %}

{% block Content %}
    <div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
	  <label>วันที่ mailroom รับงาน</label>
	<div class="input-group pb-3">
		<div class="input-daterange input-group" id="datepicker" data-date-format="yyyy-mm-dd">
			<input  autocomplete="off" type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From date" value="{{date_s}}"/>
			<label class="input-group-addon" style="border:0px;">to</label>
			<input autocomplete="off" type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="To date" value="{{date_e}}"/>
		</div>
		<div class="input-group-append">
			<button class="btn btn-outline-secondary" type="button" onclick="load_data();">
				<i class="material-icons">
				search
				</i>
			</button>
		</div>
  </div>
    <div class="search_list">
        <form>
            <div class="form-group">
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหา,หมายเลขงาน,ชื่อ,นามสกุล,ชั้น">
            </div>
        </form>
    </div>
	
	
    <div class="text-center">
        <label>จำนวนเอกสาร</label>
        
            <span id="badge-show" class="badge badge-secondary badge-pill badge-dark"></span>

         <label>ฉบับ</label>
    </div>
	
	<button type="button" class="btn btn-primary btn-block btn-lg" id="btn_save_all">รับเอกสารทั้งหมด</button>
	<div id="data_list">
	</div>
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


{% extends "base_emp2.tpl" %}

{% block title %}Pivot- List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
		
		$("#btn_save").click(function(){
			var pass_emp 			= $("#pass_emp").val();
			var name 				= $("#name").val();
			var last_name 			= $("#last_name").val();
			var tel 				= $("#tel").val();
			var tel_mobile 			= $("#tel_mobile").val();
			var email 				= $("#email").val();
			var emp_id 				= $("#emp_id").val();
			var user_id 			= $("#user_id").val();
			var mr_hub_id 			= $("#mr_hub_id").val();
			var	status 				= true;
			
				if( pass_emp == "" || pass_emp == null){
					$('#pass_emp').css({'color':'red','border-style':'solid','border-color':'red'});
					status = false;
				}else{
					status = true;
				}
				
				if(	name == "" || name == null){
					status = false;
					$('#name').css({'color':'red','border-style':'solid','border-color':'red'});
				}else{
					status = true;
				}
				
				if( last_name == "" || last_name == null){
					status = false;
					$('#last_name ').css({'color':'red','border-style':'solid','border-color':'red'});
				}else{
					status = true;
				}
				
				if( email == "" || email == null){
					status = false;
					$('#email ').css({'color':'red','border-style':'solid','border-color':'red'});
				}else{
					status = true;
				}


				if( status === true ){
					alertify.confirm('แก้ไขข้อมูล','ยืนยันการแก้ไขข้อมูล', function(){ 
						$.post(
						'./ajax/ajax_update_emp.php',
						{
							pass_emp	 : pass_emp,
							user_id		 : user_id,
							emp_id		 : emp_id,
							name		 : name,		
							last_name 	 : last_name,
							tel 		 : tel,		
							tel_mobile 	 : tel_mobile,
							email 		 : email,
							mr_hub_id 		 : mr_hub_id
						},
						function(res) {
							console.log(res)
							if ( res != '' ){
								//alert('บันทึกสำเร็จ');
								alertify.alert('บันทึก','บันทึกสำเร็จ'); 
							}else{
								//alert('บันทึกไม่สำเร็จ');
								alertify.alert('บันทึก','บันทึกไม่สำเร็จ'); 
								
							}
						});

					},function(){});

				}else{
					alert('กรุณากรอกข้อมูลให้ครบถ้วน');
				}	
				
				//console.log(floor);
				
								
				
			
		});
		

{% endblock %}				
{% block javaScript %}
		
function load_modal(mr_contact_id) {	

 $('#editCon').modal({backdrop: 'false'});

$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "../branch/ajax/ajax_save_data_contact.php",
	  data: { 	
				mr_contact_id : mr_contact_id,
				page : 'getdata'
	  }
	}).done(function(data) {
		//alert('55555');
		$('#con_emp_tel').val(data['mr_emp_tel']);
		$('#mr_contact_id').val(data['mr_contact_id']);
		$('#mr_emp_code').val(data['emp_code']);
		$('#con_department_name').val(data['department_name']);
		$('#con_floor').val(data['floor']);
		$('#con_remark').val(data['remark']);
	});
	
}


	

		
{% endblock %}					
				
{% block styleReady %}

	.btn {
		box-shadow: 0px 0px 0px 0px rgba(0,0,0,.0) !important;
	}
	.modal-backdrop {
		position: relative !important;
	}
	.modal {
		top: 40% !important;
	}
{% endblock %}	

{% block Content %}
	
			<div class="form-group" style="text-align: center;">
				 <label><h4> ข้อมูลผู้ใช้ </h4></label>
			</div>	
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="{{ user_data.mr_emp_id }}">
	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน" value="{{ user_data.mr_emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name" placeholder="ชื่อ" value="{{ user_data.mr_emp_name }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="last_name" placeholder="นามสกุล" value="{{ user_data.mr_emp_lastname }}" >
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel" placeholder="เบอร์โทรศัพท์" value="{{ user_data.mr_emp_tel }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel_mobile" placeholder="เบอร์มือถือ" value="{{ user_data.mr_emp_mobile }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						Email :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="email" placeholder="email" value="{{ user_data.mr_emp_email }}">
					</div>		
				</div>			
			</div>
			{#
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px;color:red;">
				สถานที่ปฏิบัติงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="mr_hub_id" style="width:100%;">
							{% for hub in hub_data %}
							<option value="{{hub.mr_hub_id}}" {% if user_data.mr_hub_id == hub.mr_hub_id %} selected {% endif %}> {{hub.mr_hub_name}} </option>
							{% endfor %}				
						</select>
					</div>


					
			</div>	
			#}
	
			</div>

					
				</div>			
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึกการแก้ไข</button>
				<br>
				<center>
					<a href = "../user/change_password.php?usr={{ userID }}"><b> เปลี่ยนรหัสผ่าน คลิกที่นี่</b></a>
				</center>
			</div>
			<br>
			<br>
			<br>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

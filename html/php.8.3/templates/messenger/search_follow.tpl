{% extends "base_emp2.tpl" %}


{% block styleReady %}


.card {
       margin: 8px 0px;
    }

    #img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
    }
    
    .btn {
        border-radius: 0px;
    }
{% endblock %}



{% block domReady %}
				

	loadFollowEmp();
	
	
	

    $('#txt_search').on('keyup', function() {
        let txt = $(this).val();
        let str = "";
        $.ajax({
            url: "./ajax/ajax_follow_work_emp.php",
            type: "POST",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                $('#img_loading').show();
                $('#data_list').hide();
            },
            success: function(res){
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {	
						
						str += '<div class="card">'; 
							str += '<div class="card-body ">';
								str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
								str += '<b>ผู้รับ : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'<br>';
								
								if( res[i]['mr_emp_tel'] == "" || res[i]['mr_emp_tel'] == null ){
									str += '<b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+'</a><br>';
								}else{
									str += '<b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+' </a>/ <a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a><br>';
								}
								
								str += '<b>แผนก : </b>';
								str += ''+res[i]['mr_department_name']+'<br>';
								str += '<b>ชั้น : </b>'+res[i]['floor']+'<br>';
								str += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
								str += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
								str += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
								str += '<b>สถานะ : </b>'+res[i]['mr_status_name']+'<br>';
								
								if ( res[i]['mr_status_name'] == 'Success' ){
									str += '<a href="detail_receive.php?barcode='+res[i]['mr_work_barcode']+'" class="btn btn-info"  >รายละเอียดการรับเอกสาร</a>';
										
								}
								str += '</div>';
						str += '</div>';
						
					}
					
                    $('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    $('#data_list').html(str);
                }
            },
            complete: function() {
                $('#img_loading').hide();
                $('#data_list').show();
            }
        });
    });

	
	
	
	
	
	
	
{% endblock %}
{% block javaScript %}
function loadFollowEmp() {
     let str = "";
        $.ajax({
            url: "./ajax/ajax_follow_work_emp_curdate.php",
            type: "POST",
            dataType: 'json',
            beforeSend: function() {
                $('#img_loading').show();
                $('#data_list').hide();
            },
            success: function(res){
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {	
						
						str += '<div class="card">'; 
							str += '<div class="card-body">';
								str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
								str += '<b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'<br>';
								
								if( res[i]['tel_re'] == "" || res[i]['tel_re'] == null ){
									str += '<b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mobile_re']+'">'+res[i]['mobile_re']+'</a><br>';
								}else{
									str += '<b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mobile_re']+'">'+res[i]['mobile_re']+' </a>/ <a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a><br>';
								}
								
								str += '<b>แผนก : </b>';
								str += ''+res[i]['mr_department_name']+'<br>';
								str += '<b>ชั้น : </b>'+res[i]['mr_department_floor']+'<br>';
								str += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
								str += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
								str += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
								str += '<b>สถานะ : </b>'+res[i]['mr_status_name']+'<br>';
									
									if ( res[i]['mr_status_name'] == 'Success' ){
										str += '<a href="detail_receive.php?barcode='+res[i]['mr_work_barcode']+'" class="btn btn-info"  >รายละเอียดการรับเอกสาร</a>';
										
									}	
								str += '</div>';
						str += '</div>';
						
					}
					
                    $('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    $('#data_list').html(str);
                }
            },
            complete: function() {
                $('#img_loading').hide();
                $('#data_list').show();
            }
        });
    }
	
	
	
	
{% endblock %}

{% block Content %}
<div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
    
    <div class="search_list">
        <form>
            <div class="form-group">
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหาจาก Barcode, ชื่อ-สกุล, เบอร์ติดต่อ, วันที่">
            </div>
			<div class="form-group">
				<center>จำนวนเอกสาร :  {{ count_curdate.count_curday }}  ฉบับ </center>
            </div>
        </form>
    </div>
	<div id="data_list">
	</div>
	
	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

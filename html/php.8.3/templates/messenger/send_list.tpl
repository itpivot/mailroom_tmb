{% extends "base_emp2.tpl" %}

{% block title %} Send List {% endblock %}

{% block menu_msg2 %} active {% endblock %}



{% block styleReady %}
	 .card {
       margin: 8px 0px;
	   
    }

	
    .space-height {
        padding: 10px 15px;
        line-height: 100%;
		
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
	
	.fixed-bottom {
		position: -webkit-sticky;
		position: sticky;
		bottom: 0;
		z-index: 1075;

	}
	
	.wraps {
		display:block;
		word-break: break-all;
		width: 30px;
	}

	
{% endblock %}

{% block domReady %}

	{% if is_time == 'on' %}
		loadSendMailroom();
	{% else %}
		$('#img_loading').hide();
		let str = '<H3 class="pt-5 text-danger"><center>ไม่อยู่ในเวลาทำการ <br> (08.00 - 18.00) !</center></H3>';
		$('#data_list').html(str);
	{% endif %}
    

    $('#txt_search').on('keyup', function() {
		$('#img_loading').show();
		loadSendMailroom();
    });
	
	
	


{% endblock %}

{% block javaScript %}

	 function goPageSend()
    {
		window.location.href = 'receive_list.php'; 
	}
	
	
    function loadSendMailroom() {
		var txt_search = $('#txt_search').val();
        var str = '';
        $.ajax({
            url: '../messenger/ajax/ajax_get_sendMailroom.php',
            type: 'POST',
			dataType: 'json',
			data: {
                txt: txt_search
            },
            cache: false,
            beforeSend: function() {
                $('#img_loading').show();
            },
            success: function(res) {
				document.getElementById("badge-show").textContent=res.length;
				if( res != "" ){
					for(var i = 0; i < res.length; i++) {	
						if( res[i]['mr_status_send'] == 2 ){
							str += '<div class="card bg-warning">'; 
								str += '<div class="card-body space-height">';
									str += '<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">';
									str += '			<input onclick="ch_change();" name="ch_bog[]" value="'+res[i]['mr_work_main_id']+'" type="checkbox" class="custom-control-input">';
									str += '			<span class="custom-control-indicator"></span>';
									str += '			<span class="custom-control-description">เลือก</span>';
									str += '		</label>';
									
									str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
									str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
									if( res[i]['tel_re'] ){
										str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
									}else{	
										str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mobile_re']+'">'+res[i]['mobile_re']+' </a>/ <a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
									}
								
									str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
									str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'      <a href="edit_floor_send.php?id='+res[i]['mr_work_main_id']+'" ><b>แก้ไขชั้น</b></a></p>';
									str += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
									str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
									str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
									str += '<hr>';
									str += '<div class="row">'
									str += '<div class="col-5 text-left btn-zone"></div>'
									str += '<div class="col-4 text-center btn-zone"><a href="agent_receive.php?id='+res[i]['mr_work_main_id']+'" class="btn btn-success"><b>ส่ง - รับแทน</b></a></div>'
									str += '<div class="col-3 text-right btn-zone"><a href="confirm_receive.php?id='+res[i]['mr_work_main_id']+'" class="btn btn-success"><b>ส่ง</b></a></div>'
									str += '</div>';
									
									str += '</div>';
							str += '</div>';
						}else if( res[i]['mr_status_id'] == 5 ){
							str += '<div class="card">'; 
								str += '<div class="card-body space-height">';
									str += '<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">';
									str += '			<input onclick="ch_change();" name="ch_bog[]" value="'+res[i]['mr_work_main_id']+'" type="checkbox" class="custom-control-input">';
									str += '			<span class="custom-control-indicator"></span>';
									str += '			<span class="custom-control-description">เลือก</span>';
									str += '		</label>';
									str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
									str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
									if( res[i]['tel_re'] ){
										str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
									}else{	
										str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mobile_re']+'">'+res[i]['mobile_re']+' </a>/ <a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
									}
								
									str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
									str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
									str += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
									str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
									str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
									str += '<hr>';
									str += '<p><b>สถานะ : Success </b></p>';
									str += '</div>';
							str += '</div>';
						}else{
							str += '<div class="card">'; 
								str += '<div class="card-body space-height">';
									str += '<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">';
									str += '			<input onclick="ch_change();" name="ch_bog[]" value="'+res[i]['mr_work_main_id']+'" type="checkbox" class="custom-control-input">';
									str += '			<span class="custom-control-indicator"></span>';
									str += '			<span class="custom-control-description">เลือก</span>';
									str += '		</label>';
									str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
									str += '<p><b>ผู้รับ : </b>'+res[i]['name_re']+' '+res[i]['lastname_re']+'</p>';
									if( res[i]['tel_re'] ){
										str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
									}else{	
										str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mobile_re']+'">'+res[i]['mobile_re']+' </a>/ <a href="tel:'+res[i]['tel_re']+'">'+res[i]['tel_re']+'</a></p>';
									}
								
									str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
									str += '<p><b>ส่งที่ชั้น : </b>'+res[i]['mr_department_floor']+'     <a href="edit_floor_send.php?id='+res[i]['mr_work_main_id']+'" ><b>แก้ไขชั้น</b></a></p>';
									str += '<p><b>วันที่ : </b>'+res[i]['sys_timestamp']+'</p>';
									str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
									str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
									str += '<hr>';
									str += '<div class="row">'
									str += '<div class="col-5 text-left btn-zone"><a href="#" class="btn btn-warning" onclick="updateNoSendById('+res[i]['work_main_id']+');"><b>ไม่เจอผู้รับ</b></a></div>'
									str += '<div class="col-4 text-right btn-zone"><a href="agent_receive.php?id='+res[i]['mr_work_main_id']+'" class="btn btn-success"><b>ส่ง - รับแทน</b></a></div>'
									str += '<div class="col-3 text-right btn-zone"><a href="confirm_receive.php?id='+res[i]['mr_work_main_id']+'" class="btn btn-success"><b>ส่ง</b></a></div>'
									str += '</div>';
									str += '</div>';
							str += '</div>';
						}
					}
				}else{
					$('#btn_save_all').hide();
					str = '<center>ไม่มีเอกสารที่ต้องส่ง !</center>';
					 
				}
				$('#data_list').html(str);
                
				
                
            },
            complete: function() {
                $('#img_loading').hide();
            }
        });
    }


	function updateNoSendById(id) {
        $.ajax({
			url: '../messenger/ajax/ajax_updateNoSendById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == "success") {
                    location.reload();
                }
            }
        })
    }

function save_click_all(type) {
				var dataall = [];
				 $('input[type="checkbox"]:checked').each(function(){
					 //console.log(this.value);
					 dataall.push(this.value);
					 
				  });
				  //console.log(tel_receiver);
				  if(dataall.length < 1){
					 alertify.alert("ตรวจสอบข้อมูล","ท่านยังไม่เลือกงาน"); 
					 return;
				  }
				 // return;
				var newdataall = dataall.join(",");
				if(type == 1 ){
					window.location.href ='confirm_receive.php?id='+newdataall+'';
				}else{
					window.location.href ='agent_receive.php?id='+newdataall+'';
				}
				//console.log(newdataall)
			}

function ch_change() {
				var dataall = [];
				$('input[type="checkbox"]:checked').each(function(){
					 //console.log(this.value);
					 dataall.push(this.value);
					 
				  });
				  if(dataall.length < 1){
					$('#btn_all').hide()  ;
				  }else{
					$('#btn_all').show()  ;
				  }
}
{% endblock %}

{% block Content %}
    <div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
    
    <div class="search_list">
        {% if is_time == 'on' %}
            <div class="form-group">
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหา">
            </div>
       {% endif %}
    </div>
    <div class="text-center">
        <label>จำนวนเอกสาร</label>
        
            <span id="badge-show" class="badge badge-secondary badge-pill badge-dark"></span>

         <label>ฉบับ</label>
    </div>
    <div id="btn_all" style="display:none;">
	<button onclick="save_click_all(1);" type="button" class="btn btn-secondary btn-lg btn-block">ส่ง</button>
	<button onclick="save_click_all(2);" type="button" class="btn btn-secondary btn-lg btn-block">ส่ง-รับแทน</button>
	</div>
	<div id="data_list">
	</div>
	<br>
	<br>
	{% if is_time == 'on' %}  
		<div class="fixed-bottom text-right">
			<button onclick="goPageSend();" type="button" class="btn btn-secondary btn-lg btn-block">รับเอกสาร</button>
		</div>
    {% endif %}
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

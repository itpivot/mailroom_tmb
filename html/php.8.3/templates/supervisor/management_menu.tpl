{% extends "base.tpl" %}

{% block title %}Pivot- Management Menu{% endblock %}

{% block menu_sp1 %} active {% endblock %}

{% block javaScript %}

{% endblock %}
{% block styleReady %}
{% endblock %}
{% block Content %}
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
		<h1 class="text-center">Management Menu</h1>
		<hr>
			<a href="add_costcenter.php" ><button type="button" class="btn btn-default btn-lg btn-block">Cost Center</button></a>
			<a href="building_data.php" ><button type="button" class="btn btn-default btn-lg btn-block">Building And Route</button></a>
			<a href="floor_data.php" ><button type="button" class="btn btn-default btn-lg btn-block">Floor And Office</button></a>
		</div>

	</div>
{% endblock %}
{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

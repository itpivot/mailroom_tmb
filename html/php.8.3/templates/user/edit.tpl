{% extends "base.tpl" %}

{% block title %}Pivot-
  {% if action == 'edit' %}
    แก้ไขผู้ใช้
  {% else %}
    เพิ่มผู้ใช้
  {% endif %}
{% endblock %}


{% block topContent %}
  <h1>
    {% if caption == 'edit' %}
      แก้ไขผู้ใช้
    {% else %}
      เพิ่มผู้ใช้
    {% endif %}
  </h1>
{% endblock %}

{% block body %}
  <div class="success">{{  message }}</div>
  <form id="userForm" method="post" action="edit.php">
    <table>
    <tr>
      <td>ชื่อผู้ใช้</td>
    <td>
      <input id="username" name="username" type="text" value="{{ user.username }}" />
      <span class="req">*</span>
      <span class="error">{{ errors.username }}</span>
    </td>
    </tr>
    <tr>
      <td>รหัสผ่าน</td>
    <td>
      <input id="password" name="password" type="password" />
      {% if not isEdit %}
        <span class="req">*</span>
        <span class="error">{{ errors.password }}</span>
      {% else %}
        ระบุรหัสผ่านและยืนยันรหัสผ่าน เมื่อต้องการแก้ไข
      {% endif %}
    </td>
    </tr>
    <tr>
      <td>ยืนยันรหัสผ่าน</td>
    <td>
      <input id="confirm_password" name="confirm_password" type="password" />
      {% if isEdit and errors.password != '' %}
        <span class="error">{{ errors.password }}</span>
      {% endif %}
    </td>
    </tr>
    <tr>
      <td>ต่ำแหน่ง</td>
      <td>
        {% if role_id == roles.project_manager %}
          {% for role in userRoles %}
            {% if role.id == user.role_id %}{{ role.name }}{% endif %}
          {% endfor %}
          <input name="role_id" type="hidden" value="{{ user.role_id }}" />
        {% else %}
          <select id="role_id" name="role_id" onchange="setRole(this.value);">
            <option value="">&nbsp;</option>
            {% for role in userRoles %}
            	{% if role.name != "Researcher" %}
              		<option value="{{ role.id }}"{% if role.id == user.role_id %} selected {% endif %}>{{ role.name }}</option>
              	{% endif %}
            {% endfor %}
          </select>
          <span class="req">*</span>
          <span class="error">{{ errors.role_id }}</span>
        {% endif %}
      </td>
    </tr>
    <tr>
      <td>ชื่อ</td>
      <td>
        <input id="name" name="name" type="text" value="{{ employees.name }}" />
        <span class="req">*</span>
        <span class="error">{{ errors.name }}</span>
      </td>
    </tr>
    <tr>
      <td>โทร</td>
      <td><input id="phone" name="phone" type="text" value="{{ employees.phone }}" />
        <span class="req">*</span>
        <span class="error">{{ errors.phone }}</span>
      </td>
    </tr>
    <tr>
      <td>E-mail</td>
      <td>
        <input id="email" name="email" type="text" value="{{ employees.email }}" />
        <span class="error">{{ errors.email }}</span>
      </td>
    </tr>
    <tr>
      <td>
        <input name="action" type="hidden" value="save" />
        <input name="id" type="hidden" value="{{ user.id }}" />
        {% if isMyself %}
          <input name="myself" type="hidden" value="1" />
        {% endif %}
      </td>
      <td><input type="submit" value="บันทึก" /></td>
    </tr>
  </table>
  </form>
{% endblock %}
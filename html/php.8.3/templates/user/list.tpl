{% extends "base.tpl" %}

{% block title %}Pivot- User List{% endblock %}

{% block menu_2 %}
  <div id="user_active">
    <div id="txtMenu">     
      <a href="../user/list.php"><font class="menuHead_active">รายชื่อผู้ใช้</font></a>
    </div>         
  </div>
{% endblock %}

{% block topContent %}<h1>รายชื่อผู้ใช้</h1>{% endblock %}

{% block body %}
  <table class="list">
    <tr>
    <th>Name</th>
    <th>Position</th>
    <th>Telephone</th>
    <th>Email</th>
    <th>Action</th>
  </tr>
  {% for u in users %}
    {% if u.role_id !='' %}
      <tr{% if loop.index|even  %} class="even"{%endif%}>
        <td>
          <p class="data">
            {% for e in employees %}
              {% if e.id == u.id %}
                {{ e.name }}
              {% endif %}
            {% endfor %}
          </p>
        </td>
        <td>
          {% for role in userRoles %}
            {% if role.id == u.role_id %}{{ role.name }}{% endif %}
          {% endfor %}
        </td>
         <td>
          {% for e in employees %}
              {% if e.id == u.id %}
                {{ e.phone }}
              {% endif %}
            {% endfor %}
        </td>
        <td>
          {% for e in employees %}
              {% if e.id == u.id %}
                {{ e.email }}
              {% endif %}
            {% endfor %}
        </td>
        <td class="action">
          <a href="edit.php?id={{ u.id }}"><img src="../themes/images/btn/edit.png" title="แก้ไขข้อมูล" /></a>
          {% if u.id != site.loginUser.id %}
            <a href="list.php?action=delete&id={{ u.id }}" onclick="return confirm('Are you sure?');"><img src="{{ site.rootUrl }}../themes/images/btn/delete.png" title="ลบข้อมูล" /></a>
          {% endif %}
        </td>
      </tr>
    {% endif %}
  {% else %}
    <tr>
      <td colspan="5">ไม่มีผู้ใช้ในระบบ</td>
    </tr>
  {% endfor %}
  </table>
{% endblock %}

{% block bottomContent %}<a href="edit.php"><div class="add" title="เพิ่มข้อมูล"></div></a>{% endblock %}
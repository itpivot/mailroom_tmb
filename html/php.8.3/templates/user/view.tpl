{% extends "base.tpl" %}

{% block title %}Pivot- Preview User{% endblock %}

{% block sidebar_user %}
  {% if role_id == roles.admin or role_id == roles.project_manager or role_id == roles.project_coordinator %}
    <div title="ผู้ใช้" id="user_active" class="user_active" onclick="window.location='/user/list.php'"></div>
  {% endif %}
{% endblock %}

{% block topContent %}
    <h1>Preview User</h1>
{% endblock %}

{% block content %}
  <div class="success">{{  message }}</div>
  <form id="userForm" method="post" action="edit.php">
    <table>
	  <tr>
	    <td>ชื่อผู้ใช้</td>
		<td>{{ user.name }}</td>
	  </tr>
	  <tr>
	    <td>หน้าที่</td>
		<td>
            {% for role in userRoles %}
              {% if role.id == user.role_id %} {{ role.name }} {% endif %}
            {% endfor %}
		</td>
	  </tr>
	  <tr>
	    <td>ฝ่าย</td>
		<td>
          {% for d in departments %}
            {% if d.id == userProfile.department_id %} {{ d.name }} {% endif %}
          {% endfor %}
		</td>
	  </tr>
	  <tr>
	    <td>ชื่อ-ไทย</td>
		<td>{{ userProfile.name_th }}</td>
	  </tr>
	  <tr>
	    <td>ชื่อ-อังกฤษ</td>
		<td>{{ userProfile.name_en }}</td>
	  </tr>
	  <tr>
	    <td>ที่อยู่</td>
		<td>{{ userProfile.address }}</td>
	  </tr>
	  <tr>
	    <td>โทร</td>
		<td>{{ userProfile.phone }}</td>
	  </tr>
	  <tr>
	    <td>Email</td>
		<td>{{ userProfile.email }}</td>
	  </tr>
	</table>
  </form>
{% endblock %}
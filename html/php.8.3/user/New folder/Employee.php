<?php
require_once 'Pivot/Dao.php';


class Dao_Employee extends Pivot_Dao
{   
    function __construct()
    {
        parent::__construct('mr_emp');
    }
	
	function getEmployeeid($code)
    {
        $sql = 'SELECT mr_emp_id as id FROM ' . $this->_tableName . ' WHERE code = "'.$code.'"
		
		';
        //echo  "<br><br><br><br><br><br>".$sql ;
        $id = $this->_db->fetchAll($sql);
        return $id['0']['id'];
    }	
	
	function getEmp_code($mr_emp_code)
    {
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					CASE 
						WHEN fl.name !=  "" THEN fl.name
					ELSE d.mr_department_floor
					END AS mr_department_floor,
					e.mr_floor_id,
					d.mr_department_name
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_floor fl on ( fl.mr_floor_id = e.mr_floor_id)
				WHERE e.mr_emp_code LIKE "'.$mr_emp_code.'"
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchRow($sql);
	}
	
	function getEmpByID($mr_emp_id)
    {
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_name
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				WHERE e.mr_emp_id = "'.$mr_emp_id.'" AND e.mr_floor_id <> 0 AND e.mr_department_id <> 0
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchRow($sql);
	}
	
	// MARCH MARCH MARCH 
	function getEmpData($data)
    {
        $sql = 'SELECT 
					e.*,
					d.*
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				WHERE e.mr_emp_code = "'.$data['mr_emp_code'].'" 
					AND e.mr_emp_name = "'.$data['mr_emp_name'].'" 
					AND e.mr_emp_lastname = "'.$data['mr_emp_lastname'].'"
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		//return $sql;
		return $this->_db->fetchRow($sql);
	}
	
	
	function getEmpDataSelect( )
    {
        $sql = 'SELECT *
				FROM mr_emp e
				WHERE e.mr_emp_code IS NOT NULL 
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}


	function getEmpDataWithTXT($txt)
	{
		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like "%'.$txt. '%" or 
					 e.mr_emp_lastname like "%'.$txt.'%" ) and
					 e.mr_department_id != 0 and 
					 e.mr_floor_id != 0 and 
					 e.mr_emp_code is not null limit 10 
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	function getEmpCode()
	{
		$sql =
			'
			select mr_emp_code
			from mr_emp e
			
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	function getEmpDataWithTXTsearch($txt)
	{
		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like "%'.$txt. '%" or 
					 e.mr_emp_lastname like "%'.$txt.'%" or 
					 e.mr_emp_code like "%'.$txt.'%"
					 ) and
					 e.mr_department_id != 0 and 
					 e.mr_floor_id != 0 and 
					 e.mr_emp_code is not null limit 10 
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	function ImportEmp( $sql )
	{
		return $this->_db->query($sql);
		//echo $sql;
		
	}
	
	function getEmpDataResign( )
    {
        $sql = 'SELECT *
				FROM mr_emp e
				LEFT JOIN mr_user m on ( e.mr_emp_id = m.mr_emp_id )
				WHERE e.mr_emp_code IS NOT NULL AND
				m.active = 0
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}
	
	
	function updateEmpByID(  $datas, $mr_emp_code  )
	{
        $this->_db->update($this->_tableName, $datas,  'mr_emp_code = "' .$mr_emp_code.'"');
		$sql = ' SELECT mr_emp_id FROM mr_emp WHERE mr_emp_code = "'.$mr_emp_code.'" ';
		//echo $sql;
		return $this->_db->fetchOne($sql);
	}
	
	
	function checkEmpResign( $mr_emp_code )
    {
        $sql = 
		'
			SELECT 
				mr_emp_email
			FROM 
				mr_emp
			WHERE 
				mr_emp_code = "'.$mr_emp_code.'" 

		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchOne($sql);
	}
	
	
	
	
	//function getEmp_name( $name )
    //{
    //    $sql = 'SELECT e.mr_emp_id,
	//				e.mr_emp_code,
	//				e.mr_emp_name,
	//				e.mr_emp_lastname,
	//				e.mr_emp_email,
	//				d.mr_department_code ,
	//				d.mr_department_floor,
	//				d.mr_department_name
	//			FROM mr_emp e
	//			left join mr_department d on ( d.mr_department_id = e.mr_department_id)
	//			left join mr_floor f on ( f.mr_floor_id = d.mr_department_floor)
	//			WHERE e.mr_emp_name LIKE "%'.$name.'%" OR e.mr_emp_lastname LIKE "%'.$name.'%"
	//	';
	//	//echo  "<br><br><br><br><br><br>".$sql ;
	//	return $this->_db->fetchAll($sql);
	//}
}
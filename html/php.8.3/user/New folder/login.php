<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/User.php';
require_once 'Dao/Access_status_log.php';

$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao 				= new Dao_UserRole();
$user_Dao 					 	= new Dao_User();
$access_status_Dao 	  	= new Dao_Access_status_log();

$user 			= array();
$error;
$isLogin 		= false;
$logedIn 		= array();
$totalFail 		= 0;

  //check ip                
if (getenv('HTTP_X_FORWARDED_FOR')) {
	$ip = getenv('HTTP_X_FORWARDED_FOR');
} else {
	$ip = getenv('REMOTE_ADDR');
}

	function DateTimeDiff($strDateTime1,$strDateTime2)
	 {
			return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
	 }

	 function splitTime($time){ // เวลาในรูปแบบ มาตรฐาน 2006-03-27 21:39:12 
			$timeArr["Y"]= substr($time,2,2);
			$timeArr["M"]= substr($time,5,2);
			$timeArr["D"]= substr($time,8,2);
			$timeArr["h"]= substr($time,11,2);
			$timeArr["m"]= substr($time,14,2);
			$timeArr["s"]= substr($time,17,2);
		return $timeArr;
	}
	function dateDiv($t1,$t2){ // ส่งวันที่ที่ต้องการเปรียบเทียบ ในรูปแบบ มาตรฐาน 2006-03-27 21:39:12

			$t1Arr=splitTime($t1);
			$t2Arr=splitTime($t2);
			
			$Time1=mktime($t1Arr["h"], $t1Arr["m"], $t1Arr["s"], $t1Arr["M"], $t1Arr["D"], $t1Arr["Y"]);
			$Time2=mktime($t2Arr["h"], $t2Arr["m"], $t2Arr["s"], $t2Arr["M"], $t2Arr["D"], $t2Arr["Y"]);
			$TimeDiv=abs($Time2-$Time1);
			
			$Time["D"]=intval($TimeDiv/86400); // จำนวนวัน
			$Time["H"]=intval(($TimeDiv%86400)/3600); // จำนวน ชั่วโมง
			$Time["M"]=intval((($TimeDiv%86400)%3600)/60); // จำนวน นาที
			$Time["S"]=intval(((($TimeDiv%86400)%3600)%60)); // จำนวน วินาที
		return $Time;
	}
	
	
	
if ($req->get('action') == 'save') {
	$user['username'] 	= trim($req->get('username'));
	$user['password'] 	= trim($req->get('password'));

    $result = $auth->authenticate(trim($req->get('username')), trim($req->get('password')), false);
//exit;
	// Authentication success
	$isLogin = $result->isValid();
    
	$getLoginStatus = $auth->getLoginStatus();
	$getUser = $auth->getUser();
	$getFirstLogin = $auth->getFirstLogin();
	$getActive = $auth->getActive(); 

	$roles = $auth->getRole();

	if( $isLogin == 1 ){
		$login_succ = $access_status_Dao->fetch_logSucc( $getUser );
		$login_fail = date('Y-m-d H:i:s');
		
		//$login_succ = $access_status_Dao->fetchAccess_logSucc( $getUser );
		//$login_fail = $access_status_Dao->fetchAccess_logFail( $getUser );
	}
	
	//echo $getUser.'<br>';
	//echo $login_succ.'<br>';
	//echo $login_fail.'<br>';
	
	$date_diff = dateDiv($login_succ,$login_fail);
	

    if ($isLogin) {
		
		if(intval($getActive) == 0) {
			$emp = $user_Dao->getempByuserid(intval($getUser));

			if(intval($roles) != 3) {
				if(intval($emp['isFailed']) >= 3) {
					$error = 'บัญชีท่านถูกระงับเนื่องจากกดรหัสผ่านผิดเกิน 3 ครั้ง กรุณาติดต่อเจ้าหน้าที่';
				} else {
					$updateSucces['isFailed'] = 0;
					$user_Dao->save($updateSucces, intval($emp['mr_user_id']));
					
					//Pivot_Site::toLoginPage();
				}
			}
			
			

		} else {
			
			//9=ตรวจสอบ login ซ้อน
			  if(intval($getLoginStatus) == 0) {//******
				$save_log['sys_timestamp'] = date('Y-m-d H:i:s');
				$save_log['activity'] = "Login";
				$save_log['terminal'] = $ip;
				$save_log['status'] = "Success";
				$save_log['mr_user_id'] = intval($getUser);
				$save_log['description'] = "Login Success";
				$updateSucces['isFailed'] = 0;

				$user_Dao->save($updateSucces, intval($getUser));
				$access_status_Dao->save($save_log);
				$auth->saveSession();
			 }//****** 
			
			else {//******
			//echo $date_diff['M'];
					//if( $date_diff['M'] > 15){
					//		$getLoginStatus = 0;
					//		$save_log['sys_timestamp'] = date('Y-m-d H:i:s');
					//		$save_log['activity'] = "Login";
					//		$save_log['terminal'] = $ip;
					//		$save_log['status'] = "Success";
					//		$save_log['mr_user_id'] = intval($getUser);
					//		$save_log['description'] = "Login Success";
					//		$updateSucces['isFailed'] = 0;
			        //
					//		$user_Dao->save($updateSucces, intval($getUser));
					//		$access_status_Dao->save($save_log);
					//		$auth->saveSession();
					////echo '11';
					//}
					
			 		//else{                                                                 //******
			 				$btn_logOut='<button type="button" onclick="logout_All('.intval($getUser).');"class="btn btn-link">ออกจากระบบทั้งหมด</button>';
							$save_log['sys_timestamp'] = date('Y-m-d H:i:s');             //******
			 				$save_log['activity'] = "Login";                              //******
			 				$save_log['terminal'] = $ip;                                  //******
			 				$save_log['status'] = "Failed";                               //******
			 				$save_log['mr_user_id'] = intval($getUser);                   //******
			 				$save_log['description'] = "Duplicate Login Account";         //******
			                                                                            //******
			 				$access_status_Dao->save($save_log);                          //******
			 				$error = 'รหัสพนักงานนี้ได้ทำการเข้าใช้งานระบบอยู่แล้ว'.$btn_logOut;                              //******
			 		//echo '22';
					//}   
                                                                  //******
					
			}                                                                          //******
		}
    } else {
		$getDataUser = $user_Dao->getFailedByUser($user['username']);
		if(intval($getDataUser['isFailed']) < 3) {
			$updateFail['isFailed'] = intval($getDataUser['isFailed']) + 1;
			if(intval($updateFail['isFailed']) == 3) {
				$updateFail['active'] = 0;
			}
			$resultFail = $user_Dao->save($updateFail, intval($getDataUser['mr_user_id'])); 

			$save_log['sys_timestamp'] = date('Y-m-d H:i:s');
			$save_log['activity'] = "Login";
			$save_log['terminal'] = $ip;
			$save_log['status'] = "Failed";
			$save_log['mr_user_id'] = intval($getDataUser['mr_user_id']);
			$save_log['description'] = "Invalid Password. (".(intval($getDataUser['isFailed']) + 1).")";
		
			$access_status_Dao->save($save_log);

			$error = 'ชื่อผู้ใช้ หรือ พาสเวิร์ด ไม่ถูกต้อง';
		} else {
			$updateFail['active'] = 0;
			//$updateFail['isFailed'] = 0; //*****
			$save_log['sys_timestamp'] = date('Y-m-d H:i:s');
			$save_log['activity'] = "Login";
			$save_log['terminal'] = $ip;
			$save_log['status'] = "Failed";
			$save_log['mr_user_id'] = intval($getDataUser['mr_user_id']);
			$save_log['description'] = "User is Locked";
			$resultFail = $user_Dao->save($updateFail, intval($getDataUser['mr_user_id']));
			$access_status_Dao->save($save_log);
			$error = 'บัญชีท่านถูกระงับกรุณาติดต่อเจ้าหน้าที่ ';
		}		
	}
	
}

//echo $error;
//echo print_r($_SESSION,true);
//exit;
//exit;
if($auth->isAuth()) {
	 if(intval($getLoginStatus) == 0) { //******
		$empData = $user_Dao->getempByuserid(intval($getUser));
		
		if(intval($empData['is_first_login']) == 0) {
			header('Location: ../user/change_password.php?usr='.$empData['mr_user_id']);
		}if(intval($empData['isFailed']) < 3) {
				$url = '';
				if($url == "") {
					//exit;
					if ($auth->hasAccess(array(Dao_UserRole::role('Administrator')))) {
							Pivot_Site::toAdministratorPages();
					}else if ($auth->hasAccess(array(Dao_UserRole::role('Employee')))) {
							Pivot_Site::toEmployeePages();
					}else if ($auth->hasAccess(array(Dao_UserRole::role('Messenger')))) {
							Pivot_Site::toMessengerPages();
					}else if ($auth->hasAccess(array(Dao_UserRole::role('Mailroom')))) {
							Pivot_Site::toMailroomPages();
					}else if ($auth->hasAccess(array(Dao_UserRole::role('Branch')))) {
							Pivot_Site::toBranchPages();
					}else if ($auth->hasAccess(array(Dao_UserRole::role('Manager')))) {
							Pivot_Site::toManagerPages();
					}else if ($auth->hasAccess(array(Dao_UserRole::role('AIACapital')))) {
							Pivot_Site::toManagerPages();
					}else if ($auth->hasAccess(array(Dao_UserRole::role('MessengerBranch')))) {
							Pivot_Site::toMessengerBranchPages();
					}else{
							Pivot_Site::toDefaultPage();
					}
					header('Location: ' .$url);
				}
		}
	 } //******
}


$template = Pivot_Template::factory('user/login.tpl');
$template->display(array(
    'error' => $error,
    'returnUrl' => $req->get('returnUrl'),
    'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));


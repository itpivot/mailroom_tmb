<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Access_status_log.php';

$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao   	= new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();
$access_status_log_Dao       = new Dao_Access_status_log();

$data = array();
$user_id = $req->get('user_id');
//echo '55555555555555>>>>>>'.$user_id;

        if(!empty($user_id)) {
            //change in DB
            $userID = intval($user_id);
            $datas['tocent'] =	null;
            $datas['isLogin'] =	0;
            $datas['sys_timestamp'] = date("Y-m-d H:i:s");
            $users_Dao->save($datas,$userID);
            // change in properties 
            $ip = $_SERVER['REMOTE_ADDR'];
            $save_log['sys_timestamp'] = date('Y-m-d H:i:s');
            $save_log['activity'] = "Logout";
            $save_log['terminal'] = $ip;
            $save_log['status'] = "Success";
            $save_log['mr_user_id'] = $userID;
            $save_log['description'] = "Logout Success";
            $access_status_log_Dao->save($save_log);
			$re['st'] = 'success';
			$re['title'] = 'สำเร็จ';
			$re['msเ'] = 'กรุณาเข้าสู่ระบบ';
        }else{
			$re['st'] = 'error';
			$re['title'] = 'สำเร็จ';
			$re['msเ'] = 'กรุณาเข้าสู่ระบบ';
			
		}            


echo json_encode($re);
exit;



// 1 : ไม่มี Users
// 2 : มี Users แล้ว
// 3 : สำเร็จ

?>
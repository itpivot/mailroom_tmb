<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'PHPMailer.php';
error_reporting(E_ALL & ~E_NOTICE);
$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao 	    = new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();
$sendmail           = new SendMail();

$data = array();
$data['mr_emp_code'] 		= $req->get('empCode');
$data['mr_emp_name'] 		= $req->get('fname');
$data['mr_emp_lastname'] 	= $req->get('lname');
$radio = $req->get('radio');




$resp = $employee_Dao->getEmpData($data);
//$u_id = $resp['u_id'];
// echo print_r($resp,true);
// exit;
 
function random_password($length = 10) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#!@";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}

function prettyURI()
{
    $result = array();
    $cutUri = explode("/", $_SERVER['REQUEST_URI']);
    // array_push($result, $cutUri[1], $cutUri[2], $cutUri[3]); // on localhost
    array_push($result, $cutUri[1], $cutUri[2]); // on server
    $ori_path = implode("/", $result);
    return $_SERVER['HTTP_ORIGIN']."/".$ori_path."/";

}

// $pathTo = prettyURI();

$pathTo = 'https://www.pivot-services.com/mailroom_tmb/user/';

if($resp['mr_emp_id'] != "" || $resp['mr_emp_id'] != null) {
	if($radio!= 2 && $radio != 5){
		echo 5;
		exit;
	}
    $result_search = $users_Dao->getUsersByEmpIdch_user(intval($resp['mr_emp_id']));

    if($result_search['user_id'] != "" && $result_search['mr_user_username'] != null) {
        echo 2;
    } else {
        $usr = array();
        $addUser = array();

        $usr['username'] = $resp['mr_emp_code'];
        $usr['password'] = 'Pivot@'.rand(10000,99999).'!';//random_password(10);
        //$usr['password'] = random_password();
        //$usr['password'] = rand(1000,9999);
        // $usr['password'] = 'pivot123';
        
        $addUser['mr_emp_id'] = $resp['mr_emp_id'];
        $addUser['mr_user_username'] = $usr['username'];
        $addUser['mr_user_password'] = $auth->encryptAES((string)$usr['password']);
        $addUser['active'] = 1; // in production is 0
        $addUser['is_first_login'] = 0;  // in production is 0
        $addUser['isLogin'] = 0;
        $addUser['date_create'] = date('Y-m-d H:i:s');
        $addUser['sys_timestamp'] = date('Y-m-d H:i:s');
        $addUser['mr_user_role_id'] = $radio;   
        if($result_search['user_id'] == ""){ 
            $result_save = $users_Dao->save($addUser);
        }else{
            $result_save = $users_Dao->save($addUser,$result_search['user_id']); 
        }

        if($result_save != "" || $result_save != null) {
            $Subjectmail = "ขอแจ้งข้อมูลการลงทะเบียนการเข้าใช้งานระบบ";
            $body="";
            $body.= "เรียน คุณ <b>".$resp['mr_emp_name']." ".$resp['mr_emp_lastname']."</b><br><br>";
            $body.= "ขอแจ้งข้อมูลการลงทะเบียนการเข้าใช้งานระบบของท่านค่ะ<br><br>";
            $body.= "<h4><a href='".$pathTo."change_password.php?atp=".$usr['password']."&usr=".$result_save."'> >> เข้าใช้งานครั้งแรก คลิกที่นี่! </a></h4><br><br>";
            $body.= "username : "."<b>".$usr['username']."</b><br>";
            $body.= "password : "."<b>".$usr['password']."</b><br>";
            $body.= "<br>";
            $body.= "Pivot Mailroom Auto-Response Message <br>";
            $body.= "(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";
			
            $result_mail = $sendmail->mailregister($body,$Subjectmail, $resp['mr_emp_email']);
            if($result_mail == 'success') {
                echo 3;
            } else {
                echo 4;
            }
           
           
        }
        

    }

} else {
    echo 1;
}

// 1 : ไม่มี Users
// 2 : มี Users แล้ว
// 3 : สำเร็จ
// 4 : การส่ง Email ผิดพลาด
// 5 : ไม่เลือกที่ทำงาน

?>
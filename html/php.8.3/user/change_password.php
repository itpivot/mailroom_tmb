<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';

$auth 	= new Pivot_Auth();
$user_Dao  = new Dao_User();
$userRole_Dao 	= new Dao_UserRole();
$req 	= new Pivot_Request();

$userID = $_GET['usr'];
$atp = $_GET['atp'];
$usrID = $auth->getUser();

$userID = base64_decode(urldecode($userID));

if($userID == "") {
	$usr = $user_Dao->getUsersByEmpId(intval($usrID));
}else{
	$usr = $user_Dao->getUsersByEmpId(intval($userID));
}

if($usr == "") {
   Pivot_Site::toLoginPage();
}

$template = Pivot_Template::factory('user/change_password.tpl');
$template->display(array(
    'atp' => $atp,
    'user' => $user,
    'userRoles' => $userRoles,
    'userProfile' => $userProfile,
    'departments' => $departments,
    'role_id' => $auth->getRole(),
    'userID' => $usr['user_id'],
    'username' => $usr['mr_user_username'],
    'roles' => Dao_UserRole::getAllRoles()
));

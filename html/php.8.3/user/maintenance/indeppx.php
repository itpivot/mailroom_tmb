<?php
$d  = date('d');
if($d > 6){
	//header('Location: ../login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Coming Soon Pivot</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
<link rel="icon" href="../../themes/bootstrap/css/favicon.ico" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css?t=4">
<!--===============================================================================================-->
</head>
<body>
	
	<!--  -->
	<div class="">
		<!-- <div class="simpleslide100-item bg-img1" style="background-image: url('img/bg2.jpg');"></div> -->
		<!-- <div class="simpleslide100-item bg-img1" style="background-image: url('img/bg2.jpg');"></div> -->

	</div>

	<img class="bg-robot" src="img/robot.png?g=5">
	<img class="bg_logo image" src="img/logo.jpg?g=5">
	<div class="row">
		<div class="col-md-6">
			<div class="mt-5 text-center">
				<img class="  image" src="img/content3.png?t=6" width="90%">
			</div>
			<br>
			<div class="flex-col-c-m">
				<p class="m2-txt1 p-b-48">
					Our website is under construction, follow us for update now!
				</p>
	
				<div class="flex-w flex-c-m cd100 p-b-33">
					<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
						<span id="days"  class="l2-txt1 p-b-9">00</span>
						<span class="s2-txt1">Days</span>
					</div>
	
					<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
						<span id="hours"  class="l2-txt1 p-b-9">00</span>
						<span class="s2-txt1">Hours</span>
					</div>
	
					<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
						<span id="minutes"  class="l2-txt1 p-b-9">00</span>
						<span class="s2-txt1">Minutes</span>
					</div>
	
					<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-20">
						<span id="seconds" class="l2-txt1 p-b-9 ">00</span>
						<span class="s2-txt1">Seconds</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">

			
		</div>
	</div>





	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/moment.min.js"></script>
	<script src="vendor/countdowntime/moment-timezone.min.js"></script>
	<script src="vendor/countdowntime/moment-timezone-with-data.min.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script>
	
	
	
	
	
		// Set the date we're counting down to
		var countDownDate = new Date("Mar 07 2022 00:00:00").getTime();
		
		// Update the count down every 1 second
		var x = setInterval(function() {
		
		  // Get today's date and time
		  var now = new Date().getTime();
		
		  // Find the distance between now and the count down date
		  var distance = countDownDate - now;
		
		  // Time calculations for days, hours, minutes and seconds
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		  if(seconds<0){
			//location.reload();
		  }
		  // Display the result in the element with id="demo"
		//   document.getElementById("demo").innerHTML = days + "d " + hours + "h "
		//   + minutes + "m " + seconds + "s ";
			$('#seconds').html(seconds);
			$('#minutes').html(minutes);
			$('#hours').html(hours);
			$('#days').html(days);
		  // If the count down is finished, write some text
		  if (distance < 0) {
			clearInterval(x);
			document.getElementById("demo").innerHTML = "EXPIRED";
		  }
		}, 1000);
		
		
		
		
		</script>
</body>
</html>
<?php 
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'PHPExcel.php';
require_once 'PHPExcel/IOFactory.php';
$req = new Pivot_Request();

$file = $_FILES['file'];

if($file){
	$name = $_FILES['file']['name'];
	$tmp_name = $_FILES['file']['tmp_name'];
	$size = $_FILES['file']['size'];
	$err = $_FILES['file']['error'];
	$file_type = $_FILES['file']['type'];

	$file_name = explode( ".", $name );
	if($file_name[count($file_name) - 1] == "xlsx" || $file_name[count($file_name) - 1] == "xls" ){
		$surname = $file_name[count($file_name) - 1];

		$path = '../download/';
		$filename = "meta_data".date("d-m-Y").".".$surname;

		if(!is_dir($path)){
			mkdir($path);
			if(move_uploaded_file($tmp_name, $path.$filename)){
					echo "Upload Success";
			}else{
					echo "Upload Fail";
			}
		}else{
			if(move_uploaded_file($tmp_name, $path.$filename)){
					echo "Upload Success";
			}else{
					echo "Upload Fail";
			}
		}
		$path_file = $path.$filename;
		if (file_exists($path_file)) {
			echo "มี";
		}else{
			echo "ไม่มี";
		}

		// UPLOAD_ERR_INI_SIZE = Value: 1; The uploaded file exceeds the upload_max_filesize directive in php.ini.
		// UPLOAD_ERR_FORM_SIZE = Value: 2; The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
		// UPLOAD_ERR_PARTIAL = Value: 3; The uploaded file was only partially uploaded.
		// UPLOAD_ERR_NO_FILE = Value: 4; No file was uploaded.
		// UPLOAD_ERR_NO_TMP_DIR = Value: 6; Missing a temporary folder. Introduced in PHP 5.0.3.
		// UPLOAD_ERR_CANT_WRITE = Value: 7; Failed to write file to disk. Introduced in PHP 5.1.0.
		// UPLOAD_ERR_EXTENSION = Value: 8; A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.
	}
}


 ?>
<?php
require_once 'prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Zone.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';

require_once 'Dao/Work_log.php';
header('Content-Type: text/html; charset=utf-8');

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$userRole_Dao   	= new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();
$branchDao 			= new Dao_Branch();
$floorDao 			= new Dao_Floor();
$zoneDao 			= new Dao_zone();
$work_inoutDao 			= new Dao_Work_inout();
$work_mainDao 		= new Dao_Work_main();

$work_logDao 		= new Dao_Work_log();

$data = array();


$sql 	= "SELECT * FROM `mr_work_main` WHERE `mr_work_barcode` IN (
'429202105070001',
'412202105100001',
'339202105100001',
'422202105100003',
'555202105100003',
'647202105100005',
'335202105070004',
'335202105070004',
'658202105070001',
'477202105100003',
'477202105100003',
'386202105100003',
'632202105100003',
'344202105070006',
'344202105070005',
'382202105100003',
'600202105090001',
'434202105100002',
'307202105100004',
'335202105070004',
'325202105100002',
'307202105100004',
'314202105100007',
'314202105100007',
'434202105100002',
'325202105100002',
'302202105100003',
'535202105080002',
'327202105100003',
'464202105090001',
'307202105100004',
'418202105100002',
'473202105100004',
'628202105090007',
'344202105070005',
'587202105100002',
'512202105090001',
'485202105090001',
'420202105100003',
'797202105100002',
'509202105080004',
'790202105070004',
'368202105100002',
'477202105100003',
'543202105100001',
'422202105100003',
'427202105100001',
'485202105090001',
'529202105100002',
'640202105100002',
'513202105100003',
'386202105100003',
'307202105100004',
'307202105100004',
'835202105070001',
'630202105080001',
'617202105100004',
'307202105100004',
'318202105100001',
'318202105100001',
'310202105100004',
'303202105100009',
'617202105100004',
'643202105100004',
'630202105080001',
'527202105100001',
'453202105100004',
'555202105100002',
'638202105100001',
'747202105100001',
'645202105100002',
'428202105100001',
'434202105100002',
'536202105100005',
'310202105100004',
'585202105090003',
'643202105100008',
'BAK202105100005',
'357202105070003',
'643202105100007',
'311202105100002',
'822202105100002',
'428202105100001',
'637202105100004',
'428202105100001',
'632202105100003',
'407202105090002',
'303202105100007',
'303202105100008',
'303202105100009',
'638202105100001',
'877202105100003',
'594202105090001',
'643202105100006',
'643202105100005',
'424202105100006',
'815202105100001',
'322202105100004',
'322202105100004',
'374202105090001',
'313202105080001',
'477202105100003',
'420202105100003',
'320202105070005',
'518202105090001',
'303202105100008',
'532202105100002',
'696202105080001',
'772202105100002',
'783202105100001',
'900202105100001',
'816202105100002',
'786202105070001',
'816202105100002',
'000202105100006',
'640202105100002',
'651202105100001',
'739202105100001',
'821202105100003',
'870202105090001',
'681202105100001',
'797202105100001',
'742202105100001',
'414202105070002',
'444202105070004',
'414202105070002',
'261202105080004',
'909202105100003',
'414202105070002',
'647202105070005',
'451202105100002',
'328202105100002',
'520202105080002'
)";

$params	= array();
$b 		= $branchDao->select($sql,$params);

$sqlb 	= "SELECT * FROM `mr_round`
left join mr_type_work using(mr_type_work_id)
";
$params	= array();
$bb 		= $branchDao->select($sqlb,$params);


$date = date('Y-m-d');
$mr_round_id = 25;
			$sql_select2 = "
SELECT 
					m.mr_work_main_id,
					m.sys_timestamp,
					m.mr_work_barcode,
					tnt.mr_round as send_round,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					bre.mr_branch_code as re_branch_code,
					bre.mr_branch_name as re_branch_name,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					tnt.tnt_tracking_no,
					tw.mr_type_work_name
				FROM mr_work_main m 
				LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
				LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
				Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
				Left join mr_branch bre on ( bre.mr_branch_id = wb.mr_branch_id )
				Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
				Left join mr_round_send_work tnt on ( tnt.mr_work_main_id = m.mr_work_main_id )
				where m.mr_work_main_id in(";
$sql_select2 .= '
				SELECT mr_work_main_id FROM mr_work_log 
				WHERE sys_timestamp LIKE"'.$date.'%"';
				if(!empty($mr_round_id)){
					$sql_select2 .= ' and mr_round_id in('.$mr_round_id.') ';
				}
$sql_select2 .= "	) ";

$sql_select2bb 		= $branchDao->select($sql_select2,$params);				
echo "<pre>".print_r(count($sql_select2bb),true)."</pre>";
echo "<pre>".print_r($sql_select2bb,true)."</pre>";

$sql_select3 .= '
				SELECT mr_work_main_id,mr_round_id FROM mr_work_log 
				WHERE sys_timestamp LIKE"'.$date.'%"
				group by mr_round_id
				';
				if(!empty($mr_round_id)){
					//$sql_select3 .= ' and mr_round_id in(25) ';
				}
$sql_select2b 		= $branchDao->select($sql_select3,$params);				
echo "<pre>".print_r(count($sql_select2b),true)."</pre>";
echo "<pre>".print_r($sql_select2b,true)."</pre>";
//exit;


foreach($b as $ky => $data){
	 
	 $main_update['mr_status_id']=5;
	 if($data['mr_type_work_id'] == 3 ){
	 	//$main_update['mr_status_id']=3;
	 }
	if($data['mr_work_main_id']!=''){
		echo "<pre>".print_r($data['mr_work_barcode'],true)."</pre>";
		 //$ss = $work_mainDao->save($main_update,$data['mr_work_main_id']);
		 
		 $save_log['mr_user_id'] 									= $auth->getUser();
		 $save_log['mr_status_id'] 									= 3;
		 $save_log['mr_work_main_id'] 								= $data['mr_work_main_id'];
		 $save_log['mr_round_id'] 									= 25;
		 $save_log['remark'] 										= "ห้อง Mailroom รับเอกสาร";
		 
		 $work_logDao->save($save_log);
	 }
}
exit;

		
		







$sql 	= "SELECT * FROM `mr_user` WHERE `mr_user_username` LIKE 'Tma007'";
$params	= array();
$b 		= $branchDao->select($sql,$params);
echo "<pre>".print_r($b,true)."</pre>";
//echo "<pre>".print_r($ss,true)."</pre>";




$decrypted_txt = encrypt_decrypt('decrypt', $b[0]['mr_user_password']);
echo "Decrypted Text =" . $decrypted_txt . "<br>";
if ($plain_txt === $decrypted_txt) echo "SUCCESS";
else echo "FAILED";
echo "<br>";


echo "<pre>".print_r($b,true)."</pre>";
exit;




$sql = '
		SELECT
			m.mr_status_id,
			m.sys_timestamp,
			m.mr_work_main_id,
			i.mr_work_inout_id,
			i.mr_emp_id,
			m.`mr_work_barcode`
		FROM
			`mr_work_main` m
		LEFT JOIN mr_work_inout i ON
			(
				i.`mr_work_main_id` = m.mr_work_main_id
			)
		WHERE
			i.`mr_work_main_id` IS NULL
';
$params	= array();
$b 		= $branchDao->select($sql,$params);

foreach($b as $key => $vv){
	
	 $ss['mr_work_inout_id']              = 754993;
	 $ss['mr_work_main_id']               = $vv['mr_work_main_id'];
	 $ss['mr_emp_id']                     = 19;
	 $ss['mr_user_id']                    = NULL;
	 $ss['mr_floor_id']                   = 42;
	 $ss['mr_branch_id']                  = NULL;
	 $ss['mr_branch_floor']               = NULL;
	 $ss['mr_status_send']                = NULL;
	 $ss['mr_status_receive']             = NULL;
	 $ss['rate_send']                     = NULL;
	 $ss['rate_remark']                   = NULL;
	 $ss['messenger_user_id']             = NULL;
	 $ss['mr_contact_id']				  = 502;
	// $work_inoutDao ->save($ss);
	 
}

















	

$addBranch['mr_branch_code'] 		= "TBANK";
$addBranch['mr_branch_name'] 		= "ตึกสำนักสวนมะลิ (TBANK)";
$addBranch['mr_branch_category_id'] = "2";

//$ss = $branchDao->save($addBranch,1381);
//$ss = $branchDao->fetchAll();





//exit;
$addUser['change_password_date']  = date('Y-m-d H:i:s');
$addUser['mr_user_password']   = 'RE1QVlhyTXpEVmRPQlE1ZHJ0eGJidz09';
$addUser['active']      = 1;
$addUser['is_first_login']    = 1;
//$result_save       = $users_Dao->save($addUser,6940);
//$result_save       = $users_Dao->save($addUser,943);





function encrypt_decrypt($action, $string)
{
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'pivotsecretkey';
    $secret_iv = 'pivotsecretiv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}
$plain_txt = "1234";
echo "Plain Text =" . (string)$plain_txt . "<br>";
$encrypted_txt = encrypt_decrypt('encrypt', $plain_txt);
echo "Encrypted Text = " . $encrypted_txt . "<br>";


// $decrypted_txt = encrypt_decrypt('decrypt', $encrypted_txt);








$addUser['mr_emp_id']     = $employee_Dao_id;
$addUser['change_password_date']  = '0000-00-00 00:00:00';
$addUser['mr_user_username']   = 'TMA075';
$addUser['mr_user_password']   = 'TGwzSVQ0cElKajY1eHk4RjZ4WHJxdz09';
$addUser['active']      = 1;
$addUser['is_first_login']    = 1;
$addUser['date_create']    = date('Y-m-d H:i:s');
$addUser['sys_timestamp']    = date('Y-m-d H:i:s');
$addUser['mr_user_role_id']   = 4;
//$result_save       = $users_Dao->save($addUser);



//exit;

$mess['mr_emp_code']   = 'TMA075';
$mess['mr_emp_name']   = 'นางสาว มุกระวี ';
$mess['mr_emp_lastname']  = 'จันทร์ดิษฐวงษ์';
$mess['mr_hub_id']    = '';
//$employee_Dao_id    = $employee_Dao->save($mess);



$addUser['mr_emp_id']     = $employee_Dao_id;
$addUser['change_password_date']  = '0000-00-00 00:00:00';
$addUser['mr_user_username']   = 'TMA075';
$addUser['mr_user_password']   = 'TGwzSVQ0cElKajY1eHk4RjZ4WHJxdz09';
$addUser['active']      = 1;
$addUser['is_first_login']    = 1;
$addUser['date_create']    = date('Y-m-d H:i:s');
$addUser['sys_timestamp']    = date('Y-m-d H:i:s');
$addUser['mr_user_role_id']   = 4;
//$result_save       = $users_Dao->save($addUser);





$mess['mr_emp_code']   = 'TMA076';
$mess['mr_emp_name']   = 'นาย ธนวัฒน์ ';
$mess['mr_emp_lastname']  = 'สุขสวัสดิ์';
$mess['mr_hub_id']    = '';
//$employee_Dao_id    = $employee_Dao->save($mess);



$addUser['mr_emp_id']     = $employee_Dao_id;
$addUser['change_password_date']  = '0000-00-00 00:00:00';
$addUser['mr_user_username']   = 'TMA076';
$addUser['mr_user_password']   = 'TGwzSVQ0cElKajY1eHk4RjZ4WHJxdz09';
$addUser['active']      = 1;
$addUser['is_first_login']    = 1;
$addUser['date_create']    = date('Y-m-d H:i:s');
$addUser['sys_timestamp']    = date('Y-m-d H:i:s');
$addUser['mr_user_role_id']   = 4;
//$result_save       = $users_Dao->save($addUser);








$password = 'pivot123';
$addUser = array();


$floorData['name'] 						= '29A';
$floorData['floor_level'] 				= '29';
$floorData['floor_builder'] 			= 'A';
//$floor  = $floorDao->save($floorData);



$zoneData['mr_user_id'] 			= 1;
$zoneData['mr_floor_id'] 			= 43 ;
//$zone  = $zoneDao->save($zoneData);



$branch['mr_branch_name'] 				= 'มันนี่ปาร์ค';
$branch['mr_user_username_old'] 		= '';
$branch['mr_branch_code'] 				= '664';
//$branchs  = $branchDao->save($branch);

$mess['mr_emp_code'] 		= 'TMP020';
$mess['mr_emp_name'] 		= 'วิวัฒน์';
$mess['mr_emp_lastname'] 	= 'แสนหลายคำ';
//$mess['mr_hub_id'] 			= '';
//$employee_Dao_id 			= $employee_Dao->save($mess);


//$addUser['change_password_date'] 	= '0000-00-00 00:00:00';
//$addUser['mr_user_username']	 	= 'TMA074';
//$addUser['mr_user_password']	 	= 'TGwzSVQ0cElKajY1eHk4RjZ4WHJxdz09';
//$addUser['active'] 					= 1;
//$addUser['is_first_login'] 			= 0;
//$addUser['mr_user_role_id'] 		= 4;
//$result_save 						= $users_Dao->save($addUser);
//$result_save 						= $users_Dao->save($addUser,6524);

$addUser['mr_emp_id'] 				= $employee_Dao_id;
$addUser['change_password_date'] 	= '0000-00-00 00:00:00';
$addUser['mr_user_username'] 		= 'TMP020';
$addUser['mr_user_password'] 		= 'TGwzSVQ0cElKajY1eHk4RjZ4WHJxdz09';
$addUser['active'] 					= 1;
$addUser['is_first_login'] 			= 1;
$addUser['date_create'] 			= date('Y-m-d H:i:s');
$addUser['sys_timestamp'] 			= date('Y-m-d H:i:s');
$addUser['mr_user_role_id'] 		= 8;
//$result_save 						= $users_Dao->save($addUser);



$sql 	= "SELECT * FROM `mr_emp` WHERE `mr_emp_code` in ('TMA075','TMP020')";

$sql 	= "SELECT * FROM `mr_floor` WHERE `name` LIKE '%29%' ORDER BY `name` DESC";
$params	= array();
$b 		= $branchDao->select($sql,$params);
echo "<pre>".print_r($b,true)."</pre>";



echo print_r($result_save,true);



// 1 : ไม่มี Users
// 2 : มี Users แล้ว
// 3 : สำเร็จ

?>
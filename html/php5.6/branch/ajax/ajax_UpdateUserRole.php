<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_branch.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_branchDao 	= new Dao_Work_branch();
$userDao 			= new Dao_User();
$empDao 			= new Dao_Employee();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$user_id  	= $auth->getUser();
$role 		= $req->get('role_id');

if(preg_match('/<\/?[^>]+(>|$)/', $role)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$result = array();
$dataupdate['mr_user_role_id'] = $role;
if($user_id != '' && $user_id != null){
	$data = $userDao->save($dataupdate,$user_id);
	$result['status'] = 200;
	$result['error']['message'] = "บันทึกสำเร็จ";
}else{
	$result['status'] = 500;
	$result['error']['message'] = "เกิดข้อผิดพลาดระบบจะรีโหลดหน้าเว็บ !!";
}

echo json_encode($result);

?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_branch.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_branchDao 	= new Dao_Work_branch();
$userDao 			= new Dao_User();
$empDao 			= new Dao_Employee();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$user_id    = $auth->getUser();
$user_data  = $userDao->getempByuserid($user_id);

$act        = $_POST['actions'];

if(preg_match('/<\/?[^>]+(>|$)/', $act)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

if($act == "update") {

    $id = $req->get('wid');

    if(preg_match('/<\/?[^>]+(>|$)/', $id)) {
     echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
        exit();
    }

    $resp = array();
    if(isset($_POST['form'])) {
        $frm = $_POST['form'];
        $data = array();
        $wMain = array();
        $wBranch = array();
        foreach($frm as $k => $v) {
            $data[$v['name']] = $v['value'];
        }

        $typeWork = $data['type_send'];

        if($typeWork == 1) {
            // floor
            $empData = $empDao->getEmpByID($data['nameReceiver']);

            // echo "<pre>".print_r($empData, true)."</pre>";

            $wMain['sys_timestamp'] = date('Y-m-d H:i:s');
            $wMain['mr_type_work_id'] = 3;
            $wMain['mr_topic'] = $data['topic'];
            $wMain['mr_work_remark'] = $data['remark'];

            $wBranch['sys_timestamp'] = date('Y-m-d H:i:s');
            $wBranch['mr_emp_id'] = $data['nameReceiver'];
            $wBranch['mr_floor_id'] = (isset($data['floorRealReceiver']) ? $data['floorRealReceiver'] : $empData['mr_floor_id']) ;

            $mainData = $work_mainDao->getWorkMainById($id);
            $branchData = $work_inoutDao->getWorkByMainIDAll($id);

            $wLog['sys_timestamp'] = date('Y-m-d H:i:s');
            $wLog['mr_work_main_id'] = $id;
            $wLog['mr_user_id'] = $user_id;
            $wLog['mr_status_id'] = $mainData['mr_status_id'];

            $work_mainDao->save($wMain, $id);
            $work_inoutDao->save($wBranch, $branchData['mr_work_inout_id']);
            $work_logDao->save($wLog);

            $resp['status'] = "success";
            $resp['messeges'] = "บันทึกผลสำเร็จ";
            echo json_encode($resp);
        } else {
            // branch
            $empData = $empDao->getEmpByID($data['nameReceiver']);
            
            $wMain['sys_timestamp'] = date('Y-m-d H:i:s');
            $wMain['mr_type_work_id'] = 2;
            $wMain['mr_topic'] = $data['topicBranch'];
            $wMain['mr_work_remark'] = $data['remarkBranch'];

            $wBranch['sys_timestamp'] = date('Y-m-d H:i:s');
            $wBranch['mr_emp_id'] = $data['nameReceiver'];
            $wBranch['mr_branch_floor'] = $data['mr_branch_floor'];
            $wBranch['mr_branch_id'] = (isset($data['branchRealReceiver']) ? $data['branchRealReceiver'] : $data['branchReceiver']) ;

            $mainData = $work_mainDao->getWorkMainById($id);
            $branchData = $work_inoutDao->getWorkByMainIDAll($id);

            $wLog['sys_timestamp'] = date('Y-m-d H:i:s');
            $wLog['mr_work_main_id'] = $id;
            $wLog['mr_user_id'] = $user_id;
            $wLog['mr_status_id'] = $mainData['mr_status_id'];

            $work_mainDao->save($wMain, $id);
            $work_inoutDao->save($wBranch, $branchData['mr_work_inout_id']);
            $work_logDao->save($wLog);

            $resp['status'] = "success";
            $resp['messeges'] = "บันทึกผลสำเร็จ";
            echo json_encode($resp);
        }
    }else{
        echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
        exit();
    }
}

if($act == "cancel") {
    
    $id = $req->get('wid');

    if(preg_match('/<\/?[^>]+(>|$)/', $id)) {
        echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
        exit();
    }

    $wMain['sys_timestamp'] = date('Y-m-d H:i:s');
    $wMain['mr_status_id'] = 6;

    $wLog['sys_timestamp'] = date('Y-m-d H:i:s');
    $wLog['mr_work_main_id'] = $id;
    $wLog['mr_user_id'] = $user_id;
    $wLog['mr_status_id'] = 6;

    $work_mainDao->save($wMain, $id);
    $work_logDao->save($wLog);
    
    $resp['status'] = "success";
    $resp['messeges'] = "บันทึกผลสำเร็จ";
    echo json_encode($resp);
}

?>

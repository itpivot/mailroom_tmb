<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';

$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();
$auth 			= new Pivot_Auth();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
    exit();
}

$employee_id    = $req->get('employee_id');

if(preg_match('/<\/?[^>]+(>|$)/', $employee_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$employeedata = $employeeDao->getemployeeByuserid( $employee_id );
echo json_encode($employeedata);
//echo ""hrtyhjty;


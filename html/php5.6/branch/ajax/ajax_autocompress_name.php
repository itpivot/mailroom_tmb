<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';

$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();
$auth 			    = new Pivot_Auth();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
    exit();
}

$name_receiver_select		=  $req->get('name_receiver_select');

if(preg_match('/<\/?[^>]+(>|$)/', $name_receiver_select)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$emp_data = $employeeDao->getEmpByIDBranch( $name_receiver_select );
$emp_data['department'] = $emp_data['mr_department_code']." - ".$emp_data['mr_department_name'];
$emp_data['branch'] = $emp_data['mr_branch_code']." - ".$emp_data['mr_branch_name'];
//echo "<pre>".print_r($emp_data,true)."</pre>";
//exit();

echo json_encode($emp_data);
?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_branch.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Employee.php';

$req 			= new Pivot_Request();
$send_workDao	= new Dao_Send_work();
$auth 			= new Pivot_Auth();
$employee_dao 	= new Dao_Employee();



if (!$auth->isAuth()) {
	
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	
	exit();
}

$emcode = $req->get('emp_code');

if(preg_match('/<\/?[^>]+(>|$)/', $emcode)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
}

$sql = "SELECT * FROM mr_emp WHERE mr_emp_code LIKE '".$emcode."'";
if(!empty($emcode)){
	$params_emp_data = array();
	array_push($params_emp_data, (string)$emcode);
	$contactdata = $employee_dao->select_employee($sql,$params_emp_data);
}

$result['data'] = $contactdata;
echo json_encode($result);

 ?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';
error_reporting(E_ALL & ~E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();

$date_report 			= $req ->get('date_report');
$round_printreper 			= $req ->get('round_printreper');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$mr_user_id									= $auth->getUser();
$sql_select = "
			SELECT 
				m.mr_work_main_id,
				m.mr_work_remark,
				m.mr_work_barcode,
				m.sys_timestamp,";

$sql_select .= "				e.mr_emp_name as send_name,
				e.mr_emp_lastname  as send_lname,
				e2.mr_emp_name as re_name,
				e2.mr_emp_lastname  as re_lname,";

$sql_select .= "
				m.mr_status_id,
				sw.barcode as supper_barcode,
				tw.mr_type_work_name,
				st.mr_status_name
			FROM mr_work_main  m 
				LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
				LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
				where  m.mr_work_main_id in(
						SELECT 
							mr_work_main_id 
						FROM mr_work_log 
						WHERE 
							sys_timestamp LIKE '%".$date_report."%' 
							AND mr_user_id =".$mr_user_id."  AND 
							mr_round_id =".$round_printreper.") 
";
// echo $sql_select ;
// exit;
$data = array();
$data = $send_workDao->select($sql_select);
if(!empty($data)){
	foreach($data as $i => $val_ ){
		$data[$i]['no'] = $i+1;
		$data[$i]['name_send'] = $val_['send_name'].'  '.$val_['send_lname'];
		$data[$i]['name_receive'] = $val_['re_name'].'  '.$val_['re_lname'];
		if($val_['mr_status_id']==10 || $val_['mr_status_id']== 3 ){
			$data[$i]['acctiom']='<i class="text-success material-icons">assignment_turned_in</i>';
		}else{
			$data[$i]['acctiom']='<button onclick="cancle_work(\''.$val_['mr_work_main_id'].'\',\''.$val_['mr_work_remark'].'\')" type="button" class="btn btn-outline-danger">ยกเลิกกการจัดส่ง</button>';
		}
		$data[$i]['check_print']= '
		<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
			<input name="ch_bog[]" value="'.$val_['mr_work_main_id'].'" type="checkbox" class="custom-control-input">
			<span class="custom-control-indicator"></span>
		</label>';
	}
	echo json_encode(array(
		'status' => 200,
		'ttt' 	=> $$date_report,
		'data' 	=> $data,
		'message' => 'บันทึกสำเร็จ'
	));

}else{
	echo json_encode(array('status' => 500, 'message' => 'ไม่มีข้อมูล'));
}

 ?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';

$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id			= $auth->getUser();
$user_data 			= $userDao->getEmpDataByuserid($user_id);
$code_branch_sender = $user_data['mr_branch_id'];
$date 				= date('dmY');



$sql_ch='SELECT s.* ,
				st.mr_status_name
			FROM mr_send_work s
			left join mr_work_main m using(mr_send_work_id)
			left join mr_status st using(mr_status_id)
			WHERE s.mr_user_id = '.$user_id.'
			and m.mr_status_id in(6,7,8,9)
			group by s.mr_send_work_id
			';
$checksave = $send_workDao->select($sql_ch);
$txt='<div class="card-body">';
$txt.='<table class="table table-bordered">
			  <thead>
				<tr>
				  <th scope="col">#</th>
				  <th scope="col">วันที่</th>
				  <th scope="col">เลขที่ใบคุม</th>
				  <th scope="col">พิมพ์</th>
				</tr>
			  </thead>
			  <tbody>';

				
if(count($checksave )>0){
foreach($checksave as $i => $val ){
	$txt.='<tr>
			  <th scope="row">'.($i+1).'</th>
			  <td>'.date('d-m-Y',strtotime($val['sys_date'])).'</td>
			  <td>'.$val['barcode'].'</td>
			  <td><a href="print_send_work_all.php?id='.urlencode(base64_encode($val['mr_send_work_id'])).'" target="_blank">พิมพ์</a></td>
			</tr>';
	
}
}else{
	$txt.='<tr>
			<th colspan="4" class="text-center"></th>
		</tr>';
}

$txt.='		  </tbody>
			</table>	
		</div>';

echo json_encode(array(
		'status' => 200,
		'data' => $txt 
	));
		

//echo json_encode($checksave);
 ?>
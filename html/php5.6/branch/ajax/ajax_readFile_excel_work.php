<?php 
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'PHPExcel.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();

$sql = "SELECT * FROM `mr_floor` ORDER BY `mr_floor`.`mr_floor_id` ASC";
$floor =  $branchDao->selectdata($sql);
$stroption='';
foreach($floor as $f_key => $f_val){
	$stroption.='<option value="'.$f_val['mr_floor_id'].'">'.$f_val['name'].'</option>';
}

$name 			= $_FILES['file']['name'];
$tmp_name 		= $_FILES['file']['tmp_name'];
$size 			= $_FILES['file']['size'];
$err 			= $_FILES['file']['error'];
$file_type 		= $_FILES['file']['type'];

$allowfile = array('xlsx','xls');
$surname_file = strtolower(pathinfo($name, PATHINFO_EXTENSION));

if(!in_array($surname_file,$allowfile)){
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$file_name = explode( ".", $name );

$extension = $file_name[count($file_name) - 1]; // xlsx|xls

try {
    $inputFileType = PHPExcel_IOFactory::identify($tmp_name);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($tmp_name);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$ch_num = 0;
//  Loop through each row of the worksheet in turn
$emp_clode = array();
$str_tble = array();
$row_no=0;
for ($row = 6; $row <= $highestRow; $row++){ 
 $error_type = 0; 
 $error_emp = 0; 
$stroption_ed='<option value="">ไม่พบข้อมูล</option>';
$stroption_emp='<option value="">ไม่พบข้อมูล</option>';
$strtel='ไม่พบข้อมูล';
$strdep='ไม่พบข้อมูล';
    //  Read a row of data into an array
	
	
    $no 		= $sheet->getCell('A'.$row)->getValue();
    $w_type		= $sheet->getCell('B'.$row)->getValue();
    $emp_clode 			= $sheet->getCell('C'.$row)->getValue();
    $floor 				= $sheet->getCell('D'.$row)->getValue();
    $doc_title	 		= $sheet->getCell('E'.$row)->getValue();
    $remark				= $sheet->getCell('F'.$row)->getValue();
    $b_code				= $sheet->getCell('G'.$row)->getValue();
	 
	//exit;
	if($no!=''){		  		 
		 $rowdata[$row]['no']      				=     (( $no	=='')?null:$no)	   					   ;
		 $rowdata[$row]['w_type'] 		 		=     ( $w_type 		=='')?null:$w_type 		   ;
		 $rowdata[$row]['emp_clode'] 		    =     ( $emp_clode 			=='')?null:$emp_clode  ;
		 $rowdata[$row]['floor']       			=     ( $floor		=='')?null:$floor	 	       ;
		 $rowdata[$row]['doc_title']      		=     ( $doc_title	=='')?null:$doc_title	       ;
		 $rowdata[$row]['remark'] 		     	=     ( $remark			=='')?null:$remark		   ;
		
		 
		 if($floor != ''){
				$sql = "SELECT * FROM `mr_floor` WHERE `name` like'".$floor."'";
				$datafor =  $branchDao->select($sql);
				if(!empty($datafor)){
					$rowdata[$row]['ch_floor'] = $datafor;
					$stroption_ed.='<option value="'.$datafor['mr_floor_id'].'" selected>'.$datafor['name'].'</option>';
					
				}else{
					$rowdata[$row]['ch_floor'] = 'nodata';
				}
		 }else{
			 $rowdata[$row]['ch_floor'] = 'nodata';
		 }
		 
		 
		 
		 if($emp_clode != ''){
			 
				if (is_numeric($emp_clode)) {
					$emp_clode 		    =     ( $emp_clode 			=='')?null:$emp_clode  ;
				} else {
					$emp_clode 		    =     ( $emp_clode 			=='')?null:"'".$emp_clode."'"  ;
				}

				$in_emp_code_params = array(); // [1, 2, 3]
				$in_emp_code_params = explode(',',$emp_clode);
				$params_emp_code_data = array(); // condition: generate ?,?,?
				$params_emp_code_data = str_repeat('?,', count($in_emp_code_params) - 1) . '?'; // example: ?,?,?

				$sql_emp_data = "SELECT 
										em.mr_emp_id ,
										em.mr_emp_lastname,
										em.mr_emp_name,
										em.mr_emp_code,
										em.mr_floor_id,
										d.mr_department_id,
										d.mr_department_name,
										u.mr_user_role_id,
										b.mr_branch_id,
										b.mr_branch_code,
										b.mr_branch_name,
										f.name as f_name,
										em.mr_emp_tel ,
										p.mr_position_name							
									FROM mr_emp em
									left join mr_floor f on (f.mr_floor_id = em.mr_floor_id)
									left join mr_department d on (d.mr_department_id = em.mr_department_id)
									left join mr_user u on (u.mr_emp_id = em.mr_emp_id)
									left join mr_branch b on (b.mr_branch_id = em.mr_branch_id)
									left join mr_position p on (p.mr_position_id = em.mr_position_id)
									where mr_emp_code in(".$params_emp_code_data.")
								";

				
				$datatosave = $employeeDao->select_employee_row($sql_emp_data,$in_emp_code_params);

				if(!empty($datatosave)){
					$stroption_emp.='<option value="'.$datatosave['mr_emp_id'].'" selected>'.$datatosave['mr_emp_code'].':'.$datatosave['mr_emp_name']  .'  '.$datatosave['mr_emp_lastname'] .'</option>';
					
					$rowdata[$row]['ch_empcode'] = $datatosave;
					$strtel = ($datatosave['mr_position_name'] != '')?$datatosave['mr_position_name']:'ไม่พบข้อมูล';
					
					if($datatosave['mr_user_role_id'] == 5 ){
						$strdep = ($datatosave['mr_branch_name'] != '')?$datatosave['mr_branch_name']:'ไม่พบข้อมูล';
					}else{
						$strdep = ($datatosave['mr_department_name'] != '')?$datatosave['mr_department_name']:'ไม่พบข้อมูล';
						
						if($rowdata[$row]['ch_floor'] == 'nodata' and $datatosave['f_name'] != ''){
							$stroption_ed.='<option value="'.$datatosave['mr_floor_id'].'" selected>'.$datatosave['f_name'].'</option>';
						}
					}
					
				}else{
					$rowdata[$row]['ch_empcode'] = 'nodata';
				}
				
		}elseif($b_code != '' and (rtrim($doc_title) == 'card' or rtrim($doc_title) == 'pin')){
					
					if(rtrim($doc_title) == 'card'){
						$position_code=8;
					}else{
						$position_code=6;
					}
				if (is_numeric($b_code)) {
					$b_code 		    =     ( $b_code 			=='')?null:$b_code  ;
				} else {
					$b_code 		    =     ( $b_code 			=='')?null:"'".$b_code."'"  ;
				}
				$params = array();
				$in_branch_code_params = array(); // [1, 2, 3]
				$in_branch_code_params = explode(',',$b_code);
				$params_branch_code_data = array();// condition: generate ?,?,?
				$params_branch_code_data = str_repeat('?,', count($in_branch_code_params) - 1) . '?'; // example: ?,?,?

				 $sql = "SELECT 
							em.mr_emp_id ,
							em.mr_emp_lastname,
							em.mr_emp_name,
							em.mr_emp_code,
							em.mr_floor_id,
							d.mr_department_id,
							d.mr_department_name,
							u.mr_user_role_id,
							b.mr_branch_id,
							b.mr_branch_code,
							b.mr_branch_name,
							f.name as f_name,
							p.mr_position_name,
							em.mr_emp_tel  
						FROM mr_emp em
						left join mr_floor f on (f.mr_floor_id = em.mr_floor_id)
						left join mr_department d on (d.mr_department_id = em.mr_department_id)
						left join mr_user u on (u.mr_emp_id = em.mr_emp_id)
						left join mr_branch b on (b.mr_branch_id = em.mr_branch_id)
						left join mr_position p on (p.mr_position_id = em.mr_position_id)
						where and p.mr_position_id = ? and b.mr_branch_code in(".$params_branch_code_data.")";

						array_push($params, (int)$position_code);

						$params = array_merge($params,$in_branch_code_params);

				$datatosave =  $employeeDao->select_employee_row($sql,$params);
				
				if(!empty($datatosave)){
					$stroption_emp.='<option value="'.$datatosave['mr_emp_id'].'" selected>'.$datatosave['mr_emp_code'].':'.$datatosave['mr_emp_name']  .'  '.$datatosave['mr_emp_lastname'] .'</option>';
					
					$rowdata[$row]['ch_empcode'] = $datatosave;
					$strtel = ($datatosave['mr_position_name'] != '')?$datatosave['mr_position_name']:'ไม่พบข้อมูล';
					
					if($datatosave['mr_user_role_id'] == 5 ){
						$strdep = ($datatosave['mr_branch_name'] != '')?$datatosave['mr_branch_name']:'ไม่พบข้อมูล';
					}else{
						$strdep = ($datatosave['mr_department_name'] != '')?$datatosave['mr_department_name']:'ไม่พบข้อมูล';
						
						if($rowdata[$row]['ch_floor'] == 'nodata' and $datatosave['f_name'] != ''){
							$stroption_ed.='<option value="'.$datatosave['mr_floor_id'].'" selected>'.$datatosave['f_name'].'</option>';
						}
					}
				}else{
					$rowdata[$row]['ch_empcode'] = 'nodata';
				}
		}else{
			$rowdata[$row]['ch_empcode'] = 'nodata';
		}
		 
		$str_tble[$row_no]['error_emp'] = $error_emp;
		$str_tble[$row_no]['error_type'] = $error_type;
		$str_tble[$row_no]['in'] = $row_no;
		$str_tble[$row_no]['no'] = $no;
		$str_tble[$row_no]['type'] = '<div id="type_error_'.$row_no.'" class="form-group">
											<select  id ="val_type_'.$row_no.'" name="val_type_'.$row_no.'" class="valit_error select_type" style="width:100%">
											  <option value="">ไม่พบข้อมูล</option>
											  <option value="3" '.(( $w_type	==1)?'selected':'').'>ส่งที่สำนักงานใหญ่</option>
											  <option value="2" '.(( $w_type	==2)?'selected':'').'>ส่งที่สาขา</option>
											</select>
										</div>
										';
		$str_tble[$row_no]['name'] = '<div id="name_error_'.$row_no.'" class="form-group">
										<select id="emp_'.$row_no.'" name="emp_'.$row_no.'"   class="select_name" style="width:100%" >'.$stroption_emp.'</select>
									  </div>';
		if($w_type	==1){							  
		$str_tble[$row_no]['floor'] = '<div id="floor_error_'.$row_no.'" class="form-group">
										<select id="floor_'.$row_no.'" name="floor_'.$row_no.'" class="select_floor"   style="width:100%" >'.$stroption_ed.$stroption.'</select>
									</div>	
									';
		}else{
		$str_tble[$row_no]['floor'] = '<div id="floor_error_'.$row_no.'" class="form-group">
										<select id="floor_'.$row_no.'" name="floor_'.$row_no.'" class="select_floor"   style="width:100%" >'
										.' 	 
											  <option value="1">ชั่น 1</option>
											  <option value="2">ชั่น 2</option>
											  <option value="3">ชั่น 3</option>'.
										'</select>
									</div>';
		}							
									
		$str_tble[$row_no]['doc_title'] = '<input onkeyup="ch_fromdata();" type="text" id="title_'.$row_no.'" name="title_'.$row_no.'" value="'.$doc_title.'" >';
		$str_tble[$row_no]['remark'] = '<input  type="text" name="remark_'.$row_no.'" value="'.$remark.'" >';
		$str_tble[$row_no]['tel'] = '<input type="text" id="tel_'.$row_no.'" name="tel_'.$row_no.'" value="'.$strtel.'" readonly>';
		if($w_type	==1){
			$strdep = ' <option selected value="'.$datatosave['mr_department_id'].'">'.$datatosave['mr_department_code'].':'.$datatosave['mr_department_name'].'</option>';
		}elseif($w_type	==2){
			$strdep = ' <option selected value="'.$datatosave['mr_branch_id'].'">'.$datatosave['mr_branch_code'].':'.$datatosave['mr_branch_name'].'</option>';
		}else{
			$strdep = '';
		}
		//$str_tble[$row_no]['strdep'] = $strdep;
		
		$str_tble[$row_no]['strdep'] = '<div id="dep_error_'.$row_no.'">
											<select '.(( $w_type	==1)?' readonly ':'').'class="strdep" name="strdep_'.$row_no.'" id="strdep_'.$row_no.'" style="width:100%">'.$strdep.'</select>
										</div>';
		$row_no++;
	}
}
echo json_encode(array(
    'status' => 200,
    'data' => $str_tble
));
// echo json_encode($str_tble);
// //echo "<pre>".print_r($rowdata, true)."</pre>";
exit;
?>


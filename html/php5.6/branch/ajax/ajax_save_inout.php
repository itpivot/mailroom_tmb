<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$userDao 			= new Dao_User();

$user_id = $req->get('user_id');

if(preg_match('/<\/?[^>]+(>|$)/', $user_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$user_data = $userDao->getEmpDataByuseridProfile($user_id);

$now = date('Y-m-d');

$barcode 			= "TM".date("dmy");
$last_barcode 		= $work_mainDao->checkBarcode($barcode);
$num_run 			= substr( $last_barcode,8,4);

$num_run++;
$num = "0001";
if( strlen($num_run) == 1 ){
	$num = "000".$num_run;
}else if( strlen($num_run) == 2 ){
	$num = "00".$num_run;		
}else if( strlen($num_run) == 3 ){
	$num = "0".$num_run;		
}else if( strlen($num_run) == 4 ){
	$num = $num_run;
}
$barcodeok	 		= $barcode."".$num;


$floor_id 	= $req->get('floor_id');
$floor_send = $req->get('floor_send');
$remark 	= $req->get('remark');
$topic 		= $req->get('topic');
$emp_id 	= $req->get('emp_id');

if(preg_match('/<\/?[^>]+(>|$)/', $floor_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $floor_send)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $remark)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $topic)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$time_round = date("H:i:s");

$round = $userDao->GetRound( $time_round );

$save_data_main['mr_round_id'] 								= $round;
$save_data_main['mr_work_barcode'] 							= $barcodeok;
$save_data_main['mr_work_date_sent'] 						= $now;
$save_data_main['mr_work_remark'] 							= $remark;
$save_data_main['mr_type_work_id'] 							= 1;
$save_data_main['mr_status_id']								= 1;
$save_data_main['mr_user_id'] 								= $user_id;
$save_data_main['mr_topic'] 								= $topic ;
$save_data_main['mr_floor_id'] 								= $user_data['mr_floor_id_emp'];

$work_main_id = $work_mainDao->save($save_data_main);

if ( $floor_send != 0 ){
	$save_data_inout['mr_floor_id'] 						= $floor_send;
}else{
	$save_data_inout['mr_floor_id'] 						= $floor_id;
}	
$save_data_inout['mr_emp_id'] 								= $emp_id ;
$save_data_inout['mr_work_main_id'] 						= $work_main_id;

$work_inoutDao->save($save_data_inout);

$save_log['mr_user_id'] 									= $user_id;
$save_log['mr_status_id'] 									= 1;
$save_log['mr_work_main_id'] 								= $work_main_id;

$work_logDao->save($save_log);

echo $num;


?>

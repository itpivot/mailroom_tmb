<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_branch.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_inout.php';
require_once 'nocsrf.php';

	$auth = new Pivot_Auth();
	if (!$auth->isAuth()) {
		echo json_encode(array('status' => 401, 'message' => 'Access denied.' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}
	try
    {
        // Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
        NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
        // form parsing, DB inserts, etc.
        // ...
        $result = 'CSRF check passed. Form parsed.';
    }
    catch ( Exception $e )
    {
        // CSRF attack detected
        $result = $e->getMessage() . ' Form ignored.';
		 echo json_encode(array('status' => 500, 'message' => $result ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
    }
	


	$req 				= new Pivot_Request();
	$work_logDao 		= new Dao_Work_log();
	$work_mainDao 		= new Dao_Work_main();
	$work_inoutDao 		= new Dao_Work_inout();
	$work_branchDao 	= new Dao_Work_branch();
	$userDao 			= new Dao_User();
	$branchDao 			= new Dao_Branch();
	$employeeDao 		= new Dao_Employee();
	$user_id			= $auth->getUser();
	
	$user_data = $userDao->getEmpDataByuseridProfileBranch($user_id);

	$emp_id = $req->get('emp_id');

	if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง','token'=>NoCSRF::generate( 'csrf_token') ));
		exit();
	}

	$emp_data = $employeeDao->getEmpByIDBranch( $emp_id );
	
	$now = date('Y-m-d');
	$code_branch_sender = substr($user_data['mr_branch_code'], -3);
	$year02 = date('Ymd');

	$type_send = $req->get('type_send');

	if(preg_match('/<\/?[^>]+(>|$)/', $type_send)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token') ));
		exit();
	}
	
	if ( $type_send == 1 ){
		//$code_branch_receiver_h = "000";
		//$barcode = $code_branch_sender.$year02.$code_branch_receiver_h;
		$barcode = $code_branch_sender.$year02;
		$save_data_main['mr_type_work_id'] 							= 3;
		
	}else{
		$save_data_main['mr_type_work_id'] 							= 2;
		$destination    = $req->get('destination');

		if(preg_match('/<\/?[^>]+(>|$)/', $destination)) {
			echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง' ,'token'=>NoCSRF::generate( 'csrf_token') ));
			exit();
		}

		//if ( $destination != 0 ){
		//	$code_branch = $branchDao->getBranchCodeByID( $destination );
		//	$code_branch_receiver_b = substr($code_branch, -3);
		//	
		//}else{
		//	//list( $branch_receiv, $m, $d ) = explode(" ", $emp_data['mr_branch_code']);
		//	//$code_branch = $branchDao->getBranchCodeByID( $emp_data['mr_branch_code'] );
		//	$code_branch_receiver_b = substr($emp_data['mr_branch_code'], -3);
		//}
		
		//$barcode = $code_branch_sender.$year02.$code_branch_receiver_b;
		$barcode = $code_branch_sender.$year02;
	}

	//$last_barcode 		= $work_mainDao->checkBarcode($barcode);
	//$num_run 			= substr( $last_barcode['mr_work_barcode'],11,4);
	//
	////	echo "<pre>".print_r($num_run,true)."</pre>";
	// //  exit();
	//
	//$num_run++;
	//$num = "0001";
	//if( strlen($num_run) == 1 ){
	//	$num = "000".$num_run;
	//}else if( strlen($num_run) == 2 ){
	//	$num = "00".$num_run;		
	//}else if( strlen($num_run) == 3 ){
	//	$num = "0".$num_run;		
	//}else if( strlen($num_run) == 4 ){
	//	$num = $num_run;
	//}
		
	
	$mr_contact_id = $req->get('mr_contact_id');	
	$remark = $req->get('remark');	
	$topic 	= $req->get('topic');
	$emp_id = $req->get('emp_id');
	$quty = $req->get('quty');

	if(preg_match('/<\/?[^>]+(>|$)/', $remark)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง','token'=>NoCSRF::generate( 'csrf_token')));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $topic)) {
		 echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง','token'=>NoCSRF::generate( 'csrf_token')));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
		 echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง','token'=>NoCSRF::generate( 'csrf_token')));
		exit();
	}
if($quty!=''){
	$save_data_main['quty'] 									= $quty;
}

	//$save_data_main['mr_work_barcode'] 						= $barcodeok;
	$save_data_main['mr_work_date_sent'] 						= $now;
	$save_data_main['mr_work_remark'] 							= $remark;
	$save_data_main['mr_status_id']								= 7;
	$save_data_main['mr_user_id'] 								= $user_id;
	$save_data_main['mr_topic'] 								= $topic;
	$save_data_main['mr_floor_id'] 								= (isset($user_data['mr_floor_id_emp']))?$user_data['mr_floor_id_emp']:null;
	$save_data_main['mr_branch_id'] 							= $user_data['mr_branch_id'];
	
	$work_main_id = $work_mainDao->save($save_data_main);
	if( strlen($work_main_id) > 4 ){
		$num_run 			= substr($work_main_id, -4); 
		$barcodeok	 		= $barcode.$num_run;
	}else{
		$barcodeok	 		= $barcode.$work_main_id;
	}
	
	$save_data_main['mr_work_barcode'] 							= $barcodeok;
	$update_id = $work_mainDao->save($save_data_main,$work_main_id);
	
	
	$emp_data = $employeeDao->getEmpByIDBranch( $emp_id );
	
	if ( $type_send == 1 ){
		
		$floor_receive = $req->get('destination');
		if ( $floor_receive != 0 ){
			$save_data_branch['mr_floor_id'] 						= $floor_receive;
		}else{
			$save_data_branch['mr_floor_id'] 						= $emp_data['mr_floor_id'];
		}	
		
	}else if ( $type_send == 2 ){

		$save_data_branch['mr_branch_floor'] 	= $req->get('branch_floor');
		$floor_id 								= $req->get('floor_id');

		if($floor_id != '' and $floor_id != 0){
			$save_data_branch['mr_floor_id'] = $floor_id;
		}
		

		$branch_receive = $req->get('destination');
		if ( $branch_receive != 0 ){
			$save_data_branch['mr_branch_id'] 						= $branch_receive;
		}else{
			$save_data_branch['mr_branch_id'] 						= $emp_data['mr_branch_id'];
		}	
	}

	//$save_data_branch['mr_emp_id'] 								= $emp_id;
	//$save_data_branch['mr_contact_id'] 								= $mr_contact_id;
	//$save_data_branch['mr_work_main_id'] 						= $work_main_id;
	
	if(!empty($emp_id)){
		$save_data_branch['mr_emp_id'] 								= $emp_id;
	}
	if(!empty($mr_contact_id)){
		$save_data_branch['mr_contact_id'] 							= $mr_contact_id;
	}
	if(!empty($work_main_id)){
		$save_data_branch['mr_work_main_id'] 						= $work_main_id;
	}
	
	
	if($work_main_id!=''){
		$work_inout_id  =  $work_inoutDao->save($save_data_branch);
	}
	
	$save_log['mr_user_id'] 									= $user_id;
	$save_log['mr_status_id'] 									= 7;
	$save_log['mr_work_main_id'] 								= $work_main_id;
	$save_log['remark'] 										= "สร้างงานใหม่";
	
	$work_log_id = $work_logDao->save($save_log);
	
	$result = array();
	if(!empty($work_log_id)){
		$result['status'] 			= 200;
		$result['error']['message'] = 'บันทึกสำเร็จ';
		$result['work_main_id'] 	= urlencode(base64_encode($work_main_id));
		$result['barcodeok'] 		= $barcodeok;
		$result['token'] 			=  NoCSRF::generate( 'csrf_token');

	}else{
		$result['status'] 			= 500;
		$result['error']['message'] = 'บันทึกไม่สำเร็จ';
		$result['token'] 			=  NoCSRF::generate( 'csrf_token');
	}
	
	echo json_encode($result);
	
 ?>

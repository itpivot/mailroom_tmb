<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';

$auth           = new Pivot_Auth();
$req            = new Pivot_Request();
$userDao        = new Dao_User();
$userRoleDao    = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$txt = $req->get('txt');

if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$user_id = $auth->getUser();

$sendData = $work_inout_Dao->countSearchWorkEmp($txt, intval($user_id));

echo json_encode($sendData);
?>
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';
error_reporting(E_ALL & ~E_NOTICE);

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}


$user = $auth->getUserName();

$barcode 			= $req ->get('barcode');
$sub_barcode 		= $req ->get('sub_barcode');
$mr_round_id 		= $req ->get('mr_round_id');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $sub_barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
 if(empty($mr_round_id)){
	 echo json_encode(array(
		'status' => 500,
		'title' => 'ผิดพลาด',
		'message' => 'ไม่พบข้อมูล round'
		));
 }
 

$barcode 			=  	preg_quote($barcode, '/');
$sub_barcode 		=  	preg_quote($sub_barcode, '/');

$sql_select = "
			SELECT 
				m.mr_work_main_id,				
				m.mr_status_id,			
				m.mr_type_work_id			
			FROM mr_work_main  m 
				where m.mr_work_barcode like ?
				
";
$params[] 	= "".trim($sub_barcode)."";

if(trim($barcode) !=""){
	$sql_select .= " and  sw.barcode like  ?";
	$params[] 	= "".trim($barcode)."";
}


if($user == "88415" or $user=="CL0193/63/013"){
	//$sql_select .= " and m.mr_status_id in(3,4,10,11,13,14) ";
}else{
	//$sql_select .= " and m.mr_status_id in(3,4,10,11,13,14) ";
}


$data 	  	= $send_workDao->select($sql_select,$params);
// echo json_encode($data );
// exit;
$re_m = "";
 if(!empty($data)){
	 if($data[0]['mr_status_id']==3 or $data[0]['mr_status_id']==10){
		$re_m = " !!!!!! ยิงรับซ้ำ!!!!!!";
	 }
	 if($data[0]['mr_status_id']==6){
			echo json_encode(array(
				'status' => 501,
				'title' => 'ผิดพลาด',
				'message' => 'เอกสารถูกยกเลิกโดยผู้ส่ง'
			));
	 }else{
		 
		$main_update['mr_status_id']=10;
		if($data[0]['mr_type_work_id'] == 3 ){
			$main_update['mr_status_id']=3;
		}
		
		$ss = $work_mainDao->save($main_update,$data[0]['mr_work_main_id']);
		
		$save_log['mr_user_id'] 									= $auth->getUser();
		$save_log['mr_status_id'] 									= 10;
		$save_log['mr_work_main_id'] 								= $data[0]['mr_work_main_id'];
		$save_log['mr_round_id'] 									= $mr_round_id;
		$save_log['remark'] 										= "Mailroom Tbank รับเอกสาร";
		
		$work_logDao->save($save_log);

		echo json_encode(array(
			'status' => 200,
			'title' => 'สำเร็จ',
			'message' => 'บันทึกข้อมูลสำเร็จ >>>'.$re_m 
		));
	}
	 
 }else{

	echo json_encode(array(
		'status' => 500,
		'title' => 'ผิดพลาด',
		'message' => 'ไม่พบข้อมูล'
		));
 }
 ?>
 
 
 

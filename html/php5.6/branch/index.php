<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}


$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$employee_dao 	= new Dao_Employee();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getempByuserid($user_id);

if($auth->getFirstLogin() == 0){
	header('Location: ../user/change_password.php?usr='.$user_data['mr_emp_id']);
}

$emp_data = array();
$con_data = array();

$sql_emp_data = "
					SELECT * FROM mr_emp e 
					left join mr_branch b using(mr_branch_id)
					WHERE e.mr_emp_id = ?
				";

if(isset($user_data['mr_emp_id']) && $user_data['mr_emp_id'] != ''){
	$params_emp_data = array();
	array_push($params_emp_data, (int)$user_data['mr_emp_id']);
	$emp_data = $employee_dao->select_employee($sql_emp_data,$params_emp_data);
}

if(!empty($emp_data)){
	$params_con_data = array();
	$sql_con_data = "
						SELECT * FROM mr_contact c
						left join mr_emp e on (e.mr_emp_code=c.emp_code) 
						WHERE c.department_code in (
						select 
							department_code
						from mr_contact
						where emp_code LIKE ?
						and department_code != ''
						group by department_code
						) and c.acctive = 1 
						and e.mr_emp_code is not null
						or c.emp_code LIKE ?
					";

	array_push($params_con_data, (string)$emp_data[count($emp_data)-1]['mr_emp_code']);
	array_push($params_con_data, (string)$emp_data[count($emp_data)-1]['mr_emp_code']);

	$con_data = $employee_dao->select_employee($sql_con_data,$params_con_data);
}


//$all_emp = $employee_dao->getEmpDataSelect();




//echo "<pre>".print_r($con_data,true).'>>>';
//exit;	

/* 
$sql = "
		SELECT 
				
				*
		
		FROM mr_user u
		WHERE u.mr_user_role_id <>''
		";
$mr_user = $employee_dao->select($sql);
foreach($mr_user as $i => $val_i){
	$sql = "SELECT * FROM mr_emp e 
		WHERE e.mr_emp_code like '".$val_i['mr_user_username']."'";
		$mr_use = $employee_dao->select($sql);
		$i_d = count($mr_use)-1;
		if($i_d>=0){
			if($val_i['mr_emp_id']!=$mr_use[$i_d]['mr_emp_id']){
				//echo $val_i['mr_user_username']."<br>";
				echo $val_i['mr_user_id'].'<br>';
				echo $val_i['mr_emp_id'].':'.$val_i['mr_emp_id']."<br>".''.$mr_use[$i_d]['mr_emp_id'].':'.$mr_use[$i_d]['mr_emp_id'].'<br><br>';
				if($mr_use[$i_d]['mr_emp_id'] != ''){
					$update_val_i['mr_emp_id'] = $mr_use[$i_d]['mr_emp_id'];
					$userDao->save($update_val_i,$val_i['mr_user_id']);
				}
			}else{
				//echo '++'.$val_i['mr_user_username']."<br>".'++'.$mr_use[$i_d]['mr_emp_code'].'<br><br>';
			}
		}
} */

$template = Pivot_Template::factory('branch/index.tpl');
$template->display(array(
	//'debug' => print_r($con_data,true),
	'con_data' => $con_data,
	'emp_data' => $emp_data,
	// 'all_emp' => $all_emp,
	//'success' => $success,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	//'users' => $users,
	'role_id' => $auth->getRole(),
	//'roles' => Dao_UserRole::getAllRoles()
));
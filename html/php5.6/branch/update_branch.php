<?php 
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Address.php';
require_once 'PHPExcel.php';
header('Content-Type: text/html; charset=utf-8');




$auth 					= new Pivot_Auth();
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();
$addressDao 				= new Dao_Address();



try {
	$inputFileName = 'import_branch_tbank.xlsx';
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($inputFileName);
	
	
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}

$sheet 				= $objPHPExcel->getSheet(0); 
$highestRow 		= $sheet->getHighestRow(); 
$highestColumn 		= $sheet->getHighestColumn();
$ch_num 			= 0;
//  Loop through each row of the worksheet in turn
$emp_clode 			= array();
$str_tble 			= array();
$row_no				= 0;

for ($row = 2; $row <= $highestRow; $row++){ 
    $mr_address_open1					= $sheet->getCell('E'.$row)->getValue();
    $mr_address_open2					= $sheet->getCell('Q'.$row)->getValue();
	
    $address[$row]['mr_address_add']	= $sheet->getCell('D'.$row)->getValue();
    $address[$row]['mr_address_tel']	= $sheet->getCell('F'.$row)->getValue();
    $address[$row]['mr_address_open']	= $mr_address_open1;
	
	
	$branch[$row]['mr_branch_category_id']	= 2;
	$branch[$row]['mr_branch_name']			= $sheet->getCell('C'.$row)->getValue();
	//$branch[$row]['mr_branch_name_old']		= $sheet->getCell('C'.$row)->getValue();
	$branch[$row]['mr_branch_code']			= $sheet->getCell('B'.$row)->getValue();
	
	$mr_hub_id								= $sheet->getCell('G'.$row)->getValue();
	if($mr_hub_id != ''){
		$mr_branch_type = 2;
		$branch[$row]['mr_hub_id']			= $mr_hub_id;
	}else{
		$mr_branch_type = 3;
	}
	
	$mr_branch_type = 3;
	$branch[$row]['mr_branch_type']			= $mr_branch_type;
	$sql="
			select * 
				from mr_branch 
			where
				mr_branch_code LIKE ?
	";
	$params[0] = $branch[$row]['mr_branch_code'];
	$ch_b = $branchDao->select($sql,$params);
	if(empty($ch_b)){
		$mr_address_id 							= $addressDao->save($address[$row]);
		$branch[$row]['mr_address_id']			= $mr_address_id;
		$branch_id 							= $branchDao->save($branch[$row]);
	}else{
		echo "<pre> มีแล้ว : ".print_r($branch[$row], true)."</pre>";
	}
}
// UPDATE `mr_branch` SET `mr_branch_category_id` = '2' WHERE `mr_branch_code` LIKE '%_TBAK'
//echo json_encode($str_tble);
//echo "<pre>".print_r($branch, true)."</pre>";
exit;
?>
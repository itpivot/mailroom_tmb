<?php

return [
    'adapter' => 'pdo_mysql',
    'params'  => [
        'host'     => $_ENV['MYSQL_HOST'],
        'port'     => $_ENV['MYSQL_PORT'],
        'dbname'   => $_ENV['MYSQL_DATABASE'],
        'username' => $_ENV['MYSQL_USERNAME'] ,
        'password' => $_ENV['MYSQL_PASSWORD'],
        'charset'  => 'utf8',
    ],
];

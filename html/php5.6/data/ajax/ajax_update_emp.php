<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';
require_once 'nocsrf.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$userDao 			= new Dao_User();
$empDao 			= new Dao_Employee();

/* Check authentication */
$auth = new Pivot_Auth();
if(!$auth->isAuth() && !$auth->getLoginStatus()) {
   echo json_encode(array('status' => 401, 'message' => 'เกิดข้อผิดพลาด sesstion หมดอายุ' ,'token'=>NoCSRF::generate( 'csrf_token' )));
   exit;
}
try
    {
        // Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
        NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
        // form parsing, DB inserts, etc.
        // ...
        $result = 'CSRF check passed. Form parsed.';
    }
    catch ( Exception $e )
    {
        // CSRF attack detected
      $result = $e->getMessage() . ' Form ignored.';
		echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณาลองใหม่' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
    }


$emp_id 				                    = $req->get('emp_id');

$emp_name                                   = $req->get('name');
$emp_lastname                               = $req->get('last_name');
$emp_code                                   = $req->get('pass_emp');
$emp_telephone                              = $req->get('tel');
$emp_mobile                                 = $req->get('tel_mobile');
$emp_email                                  = $req->get('email');
$emp_department                             = $req->get('department');
$emp_floor_id                               = $req->get('floor');

$save_data_emp['mr_emp_name'] 				= trim($emp_name);
$save_data_emp['mr_emp_lastname'] 			= trim($emp_lastname);
$save_data_emp['mr_emp_code'] 				= trim($emp_code);
$save_data_emp['mr_emp_tel'] 				= trim($emp_telephone);
$save_data_emp['mr_emp_mobile']				= trim($emp_mobile);
$save_data_emp['mr_emp_email'] 				= trim($emp_email);
$save_data_emp['mr_department_id'] 			= trim($emp_department);
$save_data_emp['mr_floor_id'] 				= trim($emp_floor_id);

// update employee data
$result = $empDao->save($save_data_emp, $emp_id) ;
echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'token'=>NoCSRF::generate( 'csrf_token' )));
//echo $result;
exit;
 ?>
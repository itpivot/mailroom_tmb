<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';


$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();

$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    // Pivot_Site::toLoginPage();
    echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
    exit();
}

$name_receiver_select		=  $req->get('name_receiver_select');
$emp_data                   = array();


try {
    if(preg_match('/<\/?[^>]+(>|$)/', $name_receiver_select)) {
        throw new Exception('Internal Server Error.');
    }
    
    $emp_data = $employeeDao->getEmpByID( $name_receiver_select );

    echo json_encode(array(
        'status' => 200,
        'data' =>  $emp_data
    ));
} catch(Exception $e) {
    echo json_encode(array('status' => 500, 'message' => 'Internal Server Error.'));
}



?>
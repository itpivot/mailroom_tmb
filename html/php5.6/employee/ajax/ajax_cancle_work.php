<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo "false";
    exit();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();

$resp = "";
$work_id = $req->get('work_id');

if(!preg_match('/[0-9]/', $work_id)) {
    echo "false";
    exit();
}

if(!empty($work_id)) {
    $data['mr_status_id'] = 6;

    $work_main_Dao->save($data, $work_id);

    $data_log['mr_work_main_id'] 		= $work_id;
    $data_log['mr_user_id'] 			= $user_id;
    $data_log['mr_status_id'] 			= 6;

    $resp = $work_log_Dao->save($data_log);
}


if($resp != "") {
    echo "success";
} else {
    echo "false";
}

?>
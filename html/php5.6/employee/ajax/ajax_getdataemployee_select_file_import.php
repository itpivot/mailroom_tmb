<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';

$employeeDao = new Dao_Employee();
$req = new Pivot_Request();

$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    // Pivot_Site::toLoginPage();
    exit();
}

$json = array();
$txt = $_GET['q'];

if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
	$txt = "";
 }

	$emp_data = $employeeDao->getEmpDataWithTXT_fileImport($txt);

	if(count($emp_data) > 0) {
		foreach ($emp_data as $key => $value) {
			$fullname = $value['mr_emp_code']." : ".$value['mr_emp_name']." ".$value['mr_emp_lastname'];
			if($value['mr_branch_name'] != ''){
				$fullname .= "<<".$value['mr_branch_name'];
			}
			
			if($value['mr_position_name'] != ''){
				$fullname .= "<<".$value['mr_position_name'];
			}
			//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
			$json[] = array(
				"id" => $value['mr_emp_id'],
				"text" => $fullname
			);
		}
	
	}


echo json_encode($json);
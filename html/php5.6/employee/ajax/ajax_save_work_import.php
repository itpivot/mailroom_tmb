<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_branch.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_inout.php';
require_once 'nocsrf.php';
set_time_limit(-1);

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_branchDao 	= new Dao_Work_branch();
$userDao 			= new Dao_User();
$branchDao 			= new Dao_Branch();
$employeeDao 		= new Dao_Employee();
$round = $userDao->GetRound( $time_round );

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 500, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
 	exit;
} else {

try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 401, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณาลองใหม่' ,'token'=>NoCSRF::generate( 'csrf_token' )));
   exit();
}



		$user_id = $auth->getUser();
		$data = array();
		$count = $req->get('count');
		if($count>0){
			$data_all = $req->get('data_all');
			$d = json_decode($data_all, true);
			foreach($d as $kk_key => $v_val){
					$data[$v_val['name']] = $v_val['value'];		
			}
			//echo print_r($data,true);
		}else{
			exit;
		}
		
		
		$user_data = $userDao->getEmpDataByuseridProfileBranch($user_id);
		
		
		//echo print_r($user_data,true);
		//exit;
		$now = date('Y-m-d');
		$year02 = date('dmy');
		//$code_branch_sender = substr($user_data['mr_branch_code'], -3);
		//$barcode = $code_branch_sender.$year02;
		$barcode = 'TM'.$year02;
		$arr_id = array();
		
	for($i = 0; $i < $count; $i++){
		/*
		$last_barcode 		= $work_mainDao->checkBarcode($barcode);
		$num_run 			= substr( $last_barcode,8,4);
		$num_run++;
		$num = "0001";
		if( strlen($num_run) == 1 ){
			$num = "000".$num_run;
		}else if( strlen($num_run) == 2 ){
			$num = "00".$num_run;		
		}else if( strlen($num_run) == 3 ){
			$num = "0".$num_run;		
		}else if( strlen($num_run) == 4 ){
			$num = $num_run;
		}
		*/
		$barcode = '';
		//for($j=0;$j<=10;$j++){
		//	$barcode 							= $work_mainDao->landom_Barcode();
		//	$last_barcode 						= $work_mainDao->checkBarcode($barcode);
		//	if($last_barcode['mr_work_barcode'] == ''){
		//		//break;
		//	}
		//}
		//echo $barcode."<br>";
		//continue;
		$arrdata[$i]['barcodeok']	 		= $barcode;
		$arrdata[$i]['val_type']			=$data['val_type_'.$i];
		$arrdata[$i]['title']				=$data['title_'.$i];
		$arrdata[$i]['remark']				=$data['remark_'.$i];
		$arrdata[$i]['strdep']				=$data['strdep_'.$i];
		$arrdata[$i]['emp']					=$data['emp_'.$i];
		$arrdata[$i]['floor']				=$data['floor_'.$i];




		$save_data_main['mr_round_id'] 								= $round;
		$save_data_main['mr_work_barcode'] 							= $arrdata[$i]['barcodeok'];
		$save_data_main['mr_work_date_sent'] 						= $now;
		$save_data_main['mr_work_remark'] 							= $arrdata[$i]['remark'];
		$save_data_main['mr_type_work_id'] 							= $arrdata[$i]['val_type'];
		$save_data_main['mr_status_id']								= 1;
		$save_data_main['mr_user_id'] 								= $auth->getUser();
		$save_data_main['mr_topic'] 								= $arrdata[$i]['title'];
		$save_data_main['mr_floor_id'] 								= $user_data['mr_floor_id'];
		$save_data_main['mr_branch_id'] 							= $user_data['mr_branch_id'];
		$work_main_id = $work_mainDao->save($save_data_main);
		//if($last_barcode['mr_work_barcode'] != ''){
			if(strlen($work_main_id)<=1){
				$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."000000".$work_main_id;
			}else if(strlen($work_main_id)<=2){
				$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."00000".$work_main_id;
			}else if(strlen($work_main_id)<=3){
				$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."0000".$work_main_id;
			}else if(strlen($work_main_id)<=4){
				$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."000".$work_main_id;
			}else if(strlen($work_main_id)<=5){
				$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."00".$work_main_id;
			}else if(strlen($work_main_id)<=6){
				$Nbarcode['mr_work_barcode'] = "TM".date("dmy")."0".$work_main_id;
			}else{
				$Nbarcode['mr_work_barcode'] = "TM".date("dmy").$work_main_id;
			} 
			$work_mainDao->save($Nbarcode,$work_main_id);
			
		//}
		//$emp_data = $employeeDao->getEmpByIDBranch( $arrdata[$i]['emp'] );
		if($arrdata[$i]['val_type'] == 1){
			$save_data_branch['mr_floor_id'] 						= $arrdata[$i]['floor'];
		}else{
			$save_data_branch['mr_branch_floor']					= $arrdata[$i]['floor'];
			$save_data_branch['mr_branch_id']						= $arrdata[$i]['strdep'];
		}
			$save_data_branch['mr_emp_id'] 								= $arrdata[$i]['emp'];
			$save_data_branch['mr_work_main_id'] 						= $work_main_id;
			
		$work_inout_id  =  $work_inoutDao->save($save_data_branch);
	
			$save_log['mr_user_id'] 									= $auth->getUser();
			$save_log['mr_status_id'] 									= 1;
			$save_log['mr_work_main_id'] 								= $work_main_id;
			
		$work_logDao->save($save_log);
			
			
			
			 
			array_push($arr_id, urlencode(base64_encode($work_main_id)) );
			$out_[$i]['work_main_id'] 	= $work_main_id;
			$out_[$i]['barcodeok'] 		= $Nbarcode['mr_work_barcode'];
			
		
	}
	$mr_user_id = $auth->getUser();
	$all_id = implode(",", $arr_id);
	
	$_SESSION['sesstion_all_id'][$mr_user_id] = $all_id;

	$out_['arr_id'] = 'sesstion';

	//echo json_encode($out_);

	$array2 = array('status' => 200, 'message' => 'บันทึกข้อมูลสำเร็จ' ,'token'=>NoCSRF::generate( 'csrf_token' ));
	$result = array_merge($out_, $array2);
	echo json_encode($result);

	exit;
}



 ?>
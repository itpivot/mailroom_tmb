<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_order.php';
require_once 'Dao/Receiver.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Building_external.php';

/* Check authentication */
$auth 					= new Pivot_Auth();
$userdao 				= new Dao_User();
$req 					= new Pivot_Request();
$employeedao 			= new Dao_Employee();
$work_orderdao 			= new Dao_Work_order();
$receiverdao 			= new Dao_Receiver();
$work_logdao 			= new Dao_Work_log();
$building_externaldao 	= new Dao_Building_external();

$users = $auth->getUser();
if($req ->get('page')=='in'){
	
	$insavedataorder['scg_employee_id'] 				= $employeedao->getEmployeeid($req->get('employee_id'));	
	$insavedataorder['scg_receiver_internal_id'] 		= $employeedao->getEmployeeid($req->get('inresevre_id'));
	if($req->get('radio') == 2){
		$insavedataorder['barcode'] 						= genBarcode("SI".date('m'));	
	}
	else{
		$insavedataorder['barcode'] 						= genBarcode("PI".date('m'));
	}
	$insavedataorder['remark'] 							= $req->get('inremark');
	
	$work_log['scg_work_order_id']						= $work_orderdao->save($insavedataorder);
	$work_log['scg_status_id']							= 1;
	$save=$work_logdao->save($work_log);
	
	//echo print_r($insavedataorder,true);
	echo $work_log['scg_work_order_id']	;
	
}else if ($req ->get('page')=='ex'){
	$arr_companyname=(explode("   ",$req->get('companyname')));
	$dd=$building_externaldao->getBuildidBYnam($arr_companyname);
	
	$exsavedataresever['scg_receiver_id'] 				= $receiverID;
	$exsavedataresever['name'] 							= $req->get('exname');
	$exsavedataresever['surname'] 						= $req->get('exlname');
	$exsavedataresever['scg_receiver_type_id'] 			= 1;
	$exsavedataresever['address'] 						= 'บริษัท :'.$req->get('companyname').'  บ้านเลขที่ :'.$req->get('exaddress_no').'  แขวง/ตำบล :'.$req->get('exaddress_tumbon').'  เขต/อำเภอ :'.	$req->get('exaddress_district').'  จังหวัด :'.$req->get('exaddress_province');
	$exsavedataresever['telephone_number'] 				= $req->get('extel');
	$exsavedataresever['active'] 						= 1;
	$exsavedataresever['scg_building_external_id'] 		= $building_externaldao->getBuildidBYnam($arr_companyname);
	
	$exsavedataorder['scg_employee_id'] 				= $employeedao->getEmployeeid($req->get('employee_id'));	
	$exsavedataorder['scg_receiver_id'] 				= $receiverdao->save($exsavedataresever);	
	$exsavedataorder['system_date'] 					= $req->get('exdate')." ".$req->get('extime');
	if($req->get('radio')==2){
		$exsavedataorder['barcode'] 						= genBarcode("SE".date('m'));	
	}
	else{
		$exsavedataorder['barcode'] 						= genBarcode("PE".date('m'));
	}
	$exsavedataorder['remark'] 							= $req->get('exremark');
	
	$work_log['scg_work_order_id']						= $work_orderdao->save($exsavedataorder);
	$work_log['scg_status_id']							= 1;
	$save=$work_logdao->save($work_log);
	
	//echo print_r($exsavedataorder,true);
	//echo print_r($exsavedataresever,true);
	echo $work_log['scg_work_order_id']	;
	
		
	//echo print_r($arr_companyname);
	//echo print_r($dd);
		
		
}else if ($req ->get('page')=='post'){
	$postsavedataresever['scg_receiver_id'] 			= $receiverID;
	$postsavedataresever['name'] 						= $req->get('postname');
	$postsavedataresever['surname'] 					= $req->get('postlname');
	$postsavedataresever['scg_receiver_type_id'] 		= 2;
	$postsavedataresever['address'] 					= ' บ้านเลขที่ :'.$req->get('postaddress_no').'  หมู่ :'.$req->get('postaddress_moo').'  แขวง/ตำบล :'.$req->get('postaddress_tumbon').'  เขต/อำเภอ :'.	$req->get('postaddress_district').'  จังหวัด :'.$req->get('postaddress_province').'  รหัสไปรษณีย์ :'.$req->get('postaddress_clod');
	$postsavedataresever['telephone_number'] 			= $req->get('posttel');
	$postsavedataresever['active'] 						= 1;
	
	$postsavedataorder['scg_employee_id'] 				= $employeedao->getEmployeeid($req->get('employee_id'));	
	$postsavedataorder['scg_receiver_id'] 				= $receiverdao->save($postsavedataresever);	
	$postsavedataorder['system_date'] 					= $req->get('postdate')." ".$req->get('posttime');
	if($req->get('radio')==2){
		$postsavedataorder['barcode'] 						= genBarcode("SP".date('m'));	
	}
	else{
		$postsavedataorder['barcode'] 						= genBarcode("PP".date('m'));
	}
	$postsavedataorder['remark'] 						= $req->get('postremark');
	
	$work_log['scg_work_order_id']						= $work_orderdao->save($postsavedataorder);
	$work_log['scg_status_id']							= 1;
	$save=$work_logdao->save($work_log);
	
	//echo print_r($postsavedataorder,true);
	//echo print_r($postsavedataresever,true);
	echo $work_log['scg_work_order_id']	;
}

function genBarcode($barcode){
	$work_orderdao 	= new Dao_Work_order();
	$last_barcode 	= $work_orderdao->work_orderdao($barcode);

	$num_run 		= substr( $last_barcode, -4 );
	$num_run++;
	$num = "0001";
	if( strlen($num_run) == 1 ){
		$num = "000".$num_run;
	}else if( strlen($num_run) == 2 ){
		$num = "00".$num_run;		
	}else if( strlen($num_run) == 3 ){
		$num = "0".$num_run;		
	}else if( strlen($num_run) == 4 ){
		$num = $num_run;
	}

	$barcode	 		= $barcode."".$num;
	return  $barcode;
}
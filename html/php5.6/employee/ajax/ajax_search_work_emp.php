<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array());
    exit();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();

$txt = $req->get('txt');

if(!empty($txt)) {
    if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
        echo json_encode(array());
        exit();
    }
}

$user_id = $auth->getUser();

$sendData = $work_inout_Dao->searchWorkEmp($txt, intval($user_id));

//echo print_r($sendData);
//exit();
echo json_encode($sendData);
?>
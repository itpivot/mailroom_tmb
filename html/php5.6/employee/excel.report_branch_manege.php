<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'PHPExcel.php';
include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');


ob_start();

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();       
$userDao 			= new Dao_User();                          
 
  
$data_search['name_send_select']  		=  $req->get('name_send_select');                           
$data_search['send_branch_id']  		=  $req->get('send_branch_id');                           
$data_search['name_receiver_select']  	=  $req->get('name_receiver_select');     
$data_search['receiver_branch_id']  	=  $req->get('receiver_branch_id');                     
$data_search['barcode']  				=  $req->get('barcode');                                 
$data_search['start_date']  			=  $req->get('start_date'); 
$data_search['end_date']  				=  $req->get('end_date'); 


$check =  0;
foreach($data_search as $key => $val){
	if($val != ''){
		$check += 1;
	}
}

if($check == 0){
	$data_search['start_date'] 	= date('Y-m-d 00:00:00');
	$data_search['end_date'] 	= date('Y-m-d 23:59:59');
}


$report = $work_inoutDao->searchBranch_manege( $data_search );

$sheet1 = 'Detail';
$sheet2 = 'Summary';
$headers = array(
    'No.',
    'Barcode',
    'ชื่อผู้รับ',
    'ที่อยู่',
    'ชื่อผู้ส่ง',
    'ที่อยู่',
	'ชื่อเอกสาร',
	'หมายเหตุ',
    'วันที่สร้างรายการ',
    'วันที่ ห้องสารบัณกลางรับ',
    'วันที่สำเร็จ',
    'ผู้ที่ลงชื่อรับงาน',
    'สถานะงาน',
	'ประเภทการส่ง',
	'สั่งงานจาก'
);

$summary = array();



foreach($report as $i => $val){

	$mr_user_role_id 	= $val['mr_user_role_id'];
	$mr_type_work_id 	= $val['mr_type_work_id'];
	$mr_user_id_get 	= $val['mr_user_id_get'];
	$mr_status_id 		= $val['mr_status_id'];

	

	if($mr_type_work_id == 2){
		$address_re  = $val['mr_branch_code_re'].' : '.$val['mr_branch_name_re']."  ชั้น ".$val['mr_branch_floor_re'];
	}else{
		$address_re  = $val['depart_code_receive'].' : '.$val['depart_name_receive']."  ชั้น ".$val['depart_floor_receive'];
	}
	if($mr_user_role_id == 5 ){
		$address_send  = $val['mr_branch_code_sen'].' : '.$val['mr_branch_name_sen'];
	}else{
		$address_send  = $val['mr_department_code'].' : '.$val['mr_department_name']."  ชั้น ".$val['depart_floor_send'];
	}
	if($mr_user_id_get == 0 and ($val['mr_status_id'] == 5 or $val['mr_status_id'] == 12 )){
		$sql = "
			SELECT 
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname
				FROM mr_confirm_log  log
				left join mr_emp e on ( e.mr_emp_id = log.mr_emp_id )
				WHERE mr_work_main_id = ".$val['mr_work_main_id']."

		"; 
		$mr_emp = $userDao->select($sql);
		if(!empty($mr_emp)){
			$emp_getwork =  $mr_emp[0]['mr_emp_code'].' : '.$val[0]['mr_emp_name']." ".$val[0]['mr_emp_lastname'];
		}else{
			$emp_getwork = '';
		}
		
	}else{
		$emp_getwork =  $val['emp_code_get'].' : '.$val['emp_name_get'].' '.$val['emp_lastname_get'];
	}
	
	if($val['mr_work_date_success'] == ''){
		if($val['time_log'] != ''){
			$date_succ = date("Y-m-d",strtotime($val['time_log']));
		}else{
			$date_succ = '';	
		}
	}else{
		$date_succ = $val['mr_work_date_success'];
	}
	if($val['mr_status_id'] != 5 and $val['mr_status_id'] != 12 ){
		$emp_getwork 	= '';
		$date_succ		= '';
	}

	if($val['mr_user_role_id'] == 5){
		if($val['branch_type_name'] != ''){
			$branch_type_name = $val['branch_type_name'];
		}else{
			$branch_type_name = "สำนักงานใหญ่";
		}
	}else{
		$branch_type_name = "สำนักงานใหญ่";
	}

	if($val['mr_branch_type_id_re'] == 2 or $val['mr_branch_type_id_re'] == 3){ //3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
		$type_re = $val['mr_branch_type_id_re'];
	}else{
		$type_re = 1;
	}

	if($val['mr_branch_type_id'] == 3 or $val['mr_branch_type_id'] == 2){ // 3 สาขาต่างจังหวัดสังงาน ถ้าไม่เป็นปริมนฑน
		$type_send = $val['mr_branch_type_id'];
		
	}else{
		$type_send = 1;
	}
	if(!isset($summary[$type_send][$type_re][$mr_status_id])){
		$summary[$type_send][$type_re][$mr_status_id] = 1;
	}else{
		$summary[$type_send][$type_re][$mr_status_id] += 1;
	}




    $reports[$i] = array(
		($i+1),
        ' '.$val['mr_work_barcode'],
        $val['mr_emp_code_re'].": ".$val['name_re']." ".$val['lastname_re'],
        $address_re,//ที่อยู่ผู้รับ
		$val['mr_emp_code'].": ".$val['mr_emp_name']." ".$val['mr_emp_lastname'],
		$address_send ,//ที่อยู่ผู้ส่ง
		$val['mr_topic'],
		$val['mr_work_remark'],
		' '.$val['main_sys_time'],
		' '.$val['time_mail_re'],
		$date_succ,//วันที่สำเร็จ
		$emp_getwork,//ผู้ลงชื่อรับงาน
		$val['mr_status_name'],
		$val['mr_type_work_name'],
		$branch_type_name
    );
}



$type_name_send = array(
	"1" => "สั่งงานจากสำนักงานใหญ่", 
	"2" => "สั่งงานจากกรุงเทพและปริมณฑล", 
	"3" => "สั่งงานจากต่างจังหวัด",                           
);
$type_name_re = array(
	"0" => "สถานะงาน", 
	"1" => "ส่งที่สำนักงานใหญ่", 
	"2"=> "ส่งที่กรุงเทพและปริมณฑล", 
	"3" => "ส่งที่ต่างจังหวัด"                          
); 

$mr_status = array(
	'1'  => 'รอพนักงานเดินเอกสาร', 				
	'8'  => 'รวมเอกสารนำส่ง', 
	'2'  => 'พนักงานเดินเอกสารรับเอกสารแล้ว', 		
	'14' => 'branch ถึง Hub', 
	'3'  => 'เอกสารถึงห้อง Mailroom', 				
	'4'  => 'พนักงานเดินเอกสารอยู่ในระหว่างนำส่ง', 
	'13' => 'Mailroom ถึง Hub', 
	'5'  => 'เอกสารถึงผู้รับเรียบร้อยแล้ว', 		
	'6'  => 'ยกเลิกการจัดส่ง', 
	'15' => 'เอกสารตีกลับ'
  );


if($mr_status_id == 1 or $mr_status_id == 7){
	$id = '1';
	$mr_status_name = 'งานใหม่';

}else if($mr_status_id == 8){
	$id = '8';
	$mr_status_name = 'รวมเอกสารนำส่ง';

}else if($mr_status_id == 2){
	$id = '2';
	$mr_status_name = 'พนักงานเดินเอกสารรับเอกสารแล้ว';

}else if($mr_status_id == 14){
	$id = '14';
	$mr_status_name = 'branch ถึง Hub';

}else if($mr_status_id == 3 or $mr_status_id == 10){
	$id = '3';
	$mr_status_name = 'เอกสารถึงห้อง Mailroom';

}else if($mr_status_id == 4 or $mr_status_id == 11){
	$id = '4';
	$mr_status_name = 'พนักงานเดินเอกสารอยู่ในระหว่างนำส่ง';

}else if($mr_status_id == 13){
	$id = '13';
	$mr_status_name = 'Mailroom ถึง Hub';

}else if($mr_status_id == 5 or $mr_status_id == 12){
	$id = '5';
	$mr_status_name = 'เอกสารถึงผู้รับเรียบร้อยแล้ว';

}else if($mr_status_id == 6){
	$id = '6';
	$mr_status_name = 'ยกเลิกการจัดส่ง';

}else{
	$id = '15';
	$mr_status_name = 'เอกสารตีกลับ';

}

$summary_new = array();
foreach ($summary as $i => $val_i) {
	foreach ($val_i as $j => $val_j) {
		foreach ($val_j as $k => $val_k) {
			$mr_status_id = $k;
			if($mr_status_id == 1 or $mr_status_id == 7){
				$id = '1';
				$mr_status_name = 'งานใหม่';
			
			}else if($mr_status_id == 8){
				$id = '8';
				$mr_status_name = 'รวมเอกสารนำส่ง';
			
			}else if($mr_status_id == 2){
				$id = '2';
				$mr_status_name = 'พนักงานเดินเอกสารรับเอกสารแล้ว';
			
			}else if($mr_status_id == 14){
				$id = '14';
				$mr_status_name = 'branch ถึง Hub';
			
			}else if($mr_status_id == 3 or $mr_status_id == 10){
				$id = '3';
				$mr_status_name = 'เอกสารถึงห้อง Mailroom';
			
			}else if($mr_status_id == 4 or $mr_status_id == 11){
				$id = '4';
				$mr_status_name = 'พนักงานเดินเอกสารอยู่ในระหว่างนำส่ง';
			
			}else if($mr_status_id == 13){
				$id = '13';
				$mr_status_name = 'Mailroom ถึง Hub';
			
			}else if($mr_status_id == 5 or $mr_status_id == 12){
				$id = '5';
				$mr_status_name = 'เอกสารถึงผู้รับเรียบร้อยแล้ว';
			
			}else if($mr_status_id == 6){
				$id = '6';
				$mr_status_name = 'ยกเลิกการจัดส่ง';
			
			}else{
				$id = '15';
				$mr_status_name = 'เอกสารตีกลับ';
			
			}
			if(!isset($summary_new[$i][$j][$id])){
				$summary_new[$i][$j][$id] = $val_k;
			}else{
				$summary_new[$i][$j][$id] += $val_k;
			}
		}
	}
}
foreach ($summary_new as $i => $val_i) {
$summary_new2[$i][1][] = 'งานใหม่';
$summary_new2[$i][1][] = (is_numeric($summary_new[$i][1][1]))?$summary_new[$i][1][1]:0;
$summary_new2[$i][1][] = (is_numeric($summary_new[$i][2][1]))?$summary_new[$i][2][1]:0;
$summary_new2[$i][1][] = (is_numeric($summary_new[$i][3][1]))?$summary_new[$i][3][1]:0;
$summary_new2[$i][8][] = 'รวมเอกสารนำส่ง';
$summary_new2[$i][8][] = (is_numeric($summary_new[$i][1][8]))?$summary_new[$i][1][8]:0;
$summary_new2[$i][8][] = (is_numeric($summary_new[$i][2][8]))?$summary_new[$i][2][8]:0;
$summary_new2[$i][8][] = (is_numeric($summary_new[$i][3][8]))?$summary_new[$i][3][8]:0;
$summary_new2[$i][2][] = 'พนักงานเดินเอกสารรับเอกสารแล้ว';
$summary_new2[$i][2][] = (is_numeric($summary_new[1][1][2]))?$summary_new[1][1][2]:0;
$summary_new2[$i][2][] = (is_numeric($summary_new[1][2][2]))?$summary_new[1][2][2]:0;
$summary_new2[$i][2][] = (is_numeric($summary_new[1][3][2]))?$summary_new[1][3][2]:0;
$summary_new2[$i][14][] = 'branch ถึง Hub';
$summary_new2[$i][14][] = (is_numeric($summary_new[$i][1][14]))?$summary_new[$i][1][14]:0;
$summary_new2[$i][14][] = (is_numeric($summary_new[$i][2][14]))?$summary_new[$i][2][14]:0;
$summary_new2[$i][14][] = (is_numeric($summary_new[$i][3][14]))?$summary_new[$i][3][14]:0;
$summary_new2[$i][3][] = 'เอกสารถึงห้อง Mailroom';
$summary_new2[$i][3][] = (is_numeric($summary_new[$i][1][3]))?$summary_new[$i][1][3]:0;
$summary_new2[$i][3][] = (is_numeric($summary_new[$i][2][3]))?$summary_new[$i][2][3]:0;
$summary_new2[$i][3][] = (is_numeric($summary_new[$i][3][3]))?$summary_new[$i][3][3]:0;
$summary_new2[$i][4][] = 'พนักงานเดินเอกสารอยู่ในระหว่างนำส่ง';
$summary_new2[$i][4][] = (is_numeric($summary_new[$i][1][4]))?$summary_new[$i][1][4]:0;
$summary_new2[$i][4][] = (is_numeric($summary_new[$i][2][4]))?$summary_new[$i][2][4]:0;
$summary_new2[$i][4][] = (is_numeric($summary_new[$i][3][4]))?$summary_new[$i][3][4]:0;
$summary_new2[$i][13][] = 'Mailroom ถึง Hub';
$summary_new2[$i][13][] = (is_numeric($summary_new[$i][1][13]))?$summary_new[$i][1][13]:0;
$summary_new2[$i][13][] = (is_numeric($summary_new[$i][2][13]))?$summary_new[$i][2][13]:0;
$summary_new2[$i][13][] = (is_numeric($summary_new[$i][3][13]))?$summary_new[$i][3][13]:0;

$summary_new2[$i][5][] = 'เอกสารถึงผู้รับเรียบร้อยแล้ว';
$summary_new2[$i][5][] = (is_numeric($summary_new[$i][1][5]))?$summary_new[$i][1][5]:0;
$summary_new2[$i][5][] = (is_numeric($summary_new[$i][2][5]))?$summary_new[$i][2][5]:0;
$summary_new2[$i][5][] = (is_numeric($summary_new[$i][3][5]))?$summary_new[$i][3][5]:0;

$summary_new2[$i][6][] = 'ยกเลิกการจัดส่ง';
$summary_new2[$i][6][] = (is_numeric($summary_new[$i][1][6]))?$summary_new[$i][1][6]:0;
$summary_new2[$i][6][] = (is_numeric($summary_new[$i][2][6]))?$summary_new[$i][2][6]:0;
$summary_new2[$i][6][] = (is_numeric($summary_new[$i][3][6]))?$summary_new[$i][3][6]:0;

$summary_new2[$i][15][] = 'เอกสารตีกลับ';
$summary_new2[$i][15][] = (is_numeric($summary_new[$i][1][15]))?$summary_new[$i][1][15]:0;
$summary_new2[$i][15][] = (is_numeric($summary_new[$i][2][15]))?$summary_new[$i][2][15]:0;
$summary_new2[$i][15][] = (is_numeric($summary_new[$i][3][15]))?$summary_new[$i][3][15]:0;
}
//array_push($stack, "apple", "raspberry");

//echo "<pre>".print_r($summary,true)."</pre>";
//echo "<pre>".print_r($summary_new2,true)."</pre>";
//exit;




$file_name = 'TMB_Report.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
ksort($summary_new2);
//foreach ($summary_new2 as $i => $val_i) {
////for($i =1;$i<=3;$i++){
//	//ksort($val_i);
//	$sheet = $type_name_send[$i];
//	$writer->writeSheetRow($sheet,$type_name_re,$styleHead);
//	foreach ($val_i as $j => $val_j) {
//		$data = $val_j;
//		$writer->writeSheetRow($sheet,$data,$styleRow);
//		//echo $val_j;
//		//echo "<pre>".print_r($val_j,true)."</pre>";
//	}
//}

$writer->writeSheetRow($sheet1,$headers,$styleHead);
foreach ($reports as $v) {
    $writer->writeSheetRow($sheet1,$v,$styleRow);
 }
//exit;

$writer->writeToStdOut();

exit();
echo "<pre>".print_r($report,true)."</pre>";

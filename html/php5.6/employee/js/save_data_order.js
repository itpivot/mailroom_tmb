function checkForm(field,fieldth){
	//var field = new Array( "code", "date_order", "date_meet", "time_meet" , "cus_name" , "cus_lastname", "cus_phone", "cus_address", "cus_district", "cus_province_id", "first_name", "last_name", "phone" /*, "receiver", "re_phone" */ );
	//var fieldth = new Array( "รหัสพนักงาน", "วันที่สั่งงาน", "วันที่นัดลูกค้า", "ช่วงเวลาที่นัด" , "ชื่อลูกค้า" , "นามสกุล", "เบอร์", "สถานที่ติดต่อ", "เขต/อำเภอ", "จังหวัด", "ชื่อผู้สั่งงาน", "นามสกุลผู้สั่งงาน", "เบอร์ผู้สั่ง" /*, "ผู้รับงาน", "เบอร์ผู้รับงาน" */ );
	for( var i=0; i<field.length; i++ ){
		var field_data = document.getElementById( field[i] );
		//console.log("field_data.value = " + field_data.value );
		if( field_data.value == '' ){
			alert(" กรุณากรอกข้อมูล "+fieldth[i]);
			break;
		
		}else if( i == (field.length-1) ){
			return 1;
		}
	}
}
function savedata(){
	var page 				= document.getElementById('page').value;	
	if (page == "in"){
		var field = new Array( "name","employee_id", "inresevre_id", "indate", "intime");
		var fieldth = new Array( "ชื่อผู้สั่งงาน","รหัสพนักงานผู้ส่ง", "รหัสพนักงานผู้รับ", "วันที่", "ช่วงเวลา");
		var check = checkForm(field,fieldth);
		if (check == 1){
			insavedata();
		}
	}else if (page == "ex"){
		var field = new Array( "name","employee_id", "exdate", "extime" , "exname" , "exlname", "companyname", "exaddress_no", "exaddress_tumbon", "exaddress_district", "exaddress_province", "extel"/*, "phone" , "receiver", "re_phone" */ );
		var fieldth = new Array( "ชื่อผู้สั่งงาน","รหัสพนักงานผู้ส่ง" , "วันที่", "ช่วงเวลา", "ชื่อผู้รับ" , "นามสกุล" , "ชื่อบริษัท", "บ้านเลขที่", "แขวง/ตำบล", "เขต/อำเภอ", "จังหวัด", "เบอร์โทร"/*, "นามสกุลผู้สั่งงาน", "เบอร์ผู้สั่ง" , "ผู้รับงาน", "เบอร์ผู้รับงาน" */ );
		var check = checkForm(field,fieldth);
		if (check == 1){
			exsavedata();
		}
	}else if (page == "post"){
		var field = new Array( "name","employee_id","postdate", "posttime" , "postname" , "postlname", "postaddress_no","postaddress_moo", "postaddress_tumbon", "postaddress_district", "postaddress_province","postaddress_clod", "posttel"/*, "phone" , "receiver", "re_phone" */ );
		var fieldth = new Array( "ชื่อผู้สั่งงาน","รหัสพนักงานผู้ส่ง" ,"วันที่", "ช่วงเวลา"	, "ชื่อ", "นามสกุล", "บ้านเลขที่","หมู่"	, "แขวง/ตำบล", "เขต/อำเภอ", "จังหวัด","รหัสไปรษณีย์", "เบอร์โทร"/*, "นามสกุลผู้สั่งงาน", "เบอร์ผู้สั่ง" , "ผู้รับงาน", "เบอร์ผู้รับงาน" */ );
		var check = checkForm(field,fieldth);
		if (check == 1){
			postsavedata();
		}
	}
}
function insavedata(){
	var radio 		= document.getElementById('radio1');
	if(radio.checked == true){
		radio="1";
	}else{
		radio="2";
	}
	var employee_id 		= document.getElementById('employee_id').value;
	var inresevre_id 		= document.getElementById('inresevre_id').value;
	var indate 				= document.getElementById('indate').value;
	var intime 				= document.getElementById('intime').value;
	var inremark 			= document.getElementById('inremark').value;	
	//console.log (radio+">>>>>:"+employee_id +">>>>>:"+inresevre_id+">>>>>:"+indate+">>>>>:"+intime+">>>>>:"+inremark);	
	$.ajax({
		method: "get",
		url: "ajax/ajax_savedata_order.php",
		data: 
		{ 
		page      	: "in" ,
		radio		: radio ,
		employee_id : employee_id ,
		inresevre_id: inresevre_id,
		indate 		: indate,	
		intime 		: intime,	
		inremark    : inremark	
		}
		})
		.done(function( msg) {
			console.log (msg)
			//alert("บันทึกข้อมุลเรียบร้อยแล้ว");
			window.location.href="print_page.php?woke_id="+msg;
		});	
}
function exsavedata(){	
	var radio 		= document.getElementById('radio1');
	if(radio.checked == true){
		radio="1";
	}else{
		radio="2";
	}
	var employee_id 		= document.getElementById('employee_id').value;
	var exdate 				= document.getElementById('exdate').value;
	var extime 				= document.getElementById('extime').value;
	var exremark 			= document.getElementById('exremark').value;

	
	var exname 						= document.getElementById('exname').value;
	var exlname 					= document.getElementById('exlname').value;
	var companyname 				= document.getElementById('companyname').value;
	var exaddress_no 				= document.getElementById('exaddress_no').value;
	var exaddress_tumbon 			= document.getElementById('exaddress_tumbon').value;
	var exaddress_district 			= document.getElementById('exaddress_district').value;
	var exaddress_province 			= document.getElementById('exaddress_province').value;
	var extel 						= document.getElementById('extel').value;
	
	//console.log (">>>>>:"+employee_id +">>>>>:"+inresevre_id+">>>>>:"+indate+">>>>>:"+intime+">>>>>:"+inremark);	
	$.ajax({
		method: "get",
		url: "ajax/ajax_savedata_order.php",
		data: 
		{ 
		radio 						: radio,
		exname 						: exname,
		exlname 					: exlname,
		companyname 				: companyname,
		exaddress_no 				: exaddress_no,
		exaddress_tumbon 			: exaddress_tumbon,
		exaddress_district 			: exaddress_district,
		exaddress_province 			: exaddress_province,
		extel 						: extel,
		
		page      	: "ex" ,
		employee_id : employee_id ,
		exdate 		: exdate,	
		extime 		: extime,	
		exremark    : exremark	
		}
		})
		.done(function( msg) {
			console.log (msg)
			//alert("บันทึกข้อมุลเรียบร้อยแล้ว");
			window.location.href="print_page.php?woke_id="+msg;
		});	
}
function postsavedata(){
	var radio 		= document.getElementById('radio1');
	if(radio.checked == true){
		radio="1";
	}else{
		radio="2";
	}	
		
	var employee_id 			= document.getElementById('employee_id').value;
	var postdate 				= document.getElementById('postdate').value;
	var posttime 				= document.getElementById('posttime').value;
	var postremark 				= document.getElementById('postremark').value;

	
	var postname 					= document.getElementById('postname').value;
	var postlname 					= document.getElementById('postlname').value;
	
	var postaddress_no 				= document.getElementById('postaddress_no').value;
	var postaddress_moo 			= document.getElementById('postaddress_moo').value;
	var postaddress_tumbon 			= document.getElementById('postaddress_tumbon').value;
	var postaddress_district 		= document.getElementById('postaddress_district').value;
	var postaddress_province 		= document.getElementById('postaddress_province').value;
	var postaddress_clod 			= document.getElementById('postaddress_clod').value;
	var posttel 					= document.getElementById('posttel').value;
	
	//console.log (">>>>>:"+employee_id +">>>>>:"+inresevre_id+">>>>>:"+indate+">>>>>:"+intime+">>>>>:"+inremark);	
	$.ajax({
		method: "get",
		url: "ajax/ajax_savedata_order.php",
		data: 
		{ 
		radio 					: radio,
		postname 				: postname,
		postlname 				: postlname,
		
		postaddress_no 			: postaddress_no,
		postaddress_moo 		: postaddress_moo,
		postaddress_tumbon 		: postaddress_tumbon,
		postaddress_district 	: postaddress_district,
		postaddress_province 	: postaddress_province,
		postaddress_clod 		: postaddress_clod,
		posttel 				: posttel,
		
		page      	: "post" ,
		employee_id : employee_id ,
		postdate 	: postdate,	
		posttime 	: posttime,	
		postremark 	: postremark	
		}
		})
		.done(function( msg) {
			console.log (msg)
			//alert("บันทึกข้อมุลเรียบร้อยแล้ว");
			window.location.href="print_page.php?woke_id="+msg;
		});	
}
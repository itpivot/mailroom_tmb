<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
ini_set('max_execution_time', 0);
//error_reporting(E_ALL & ~E_WARNING | E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id	= $auth->getUser();
$user_data 	= $userDao->getEmpDataByuserid($user_id);
$alert 	= '';
$data  		= array();

//$_secret = $userDao->getEncryptKey();


$work_main_id = $req->get('maim_id');


$chm = $work_main_id;
if($work_main_id == 'sesstion' ){
	$work_main_id = $_SESSION['sesstion_all_id'][$user_id];
}

if($work_main_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'create_work.php';
			}
		}
	});
		
	";
}else{

		function decode_work_main_id($id)
		{
			return base64_decode(urldecode($id));
			//urlencode(base64_encode($work_main_id));
		}

		$params 				= array();
		$in_work_main_id_params = array(); // [1, 2, 3]
		$in_work_main_id_params = explode(',',$work_main_id);
		$params_work_main_id 	= array(); // condition: generate ?,?,?
		$params_work_main_id 	= str_repeat('?,', count($in_work_main_id_params) - 1) . '?'; // example: ?,?,?


// echo "<pre>".print_r($in_work_main_id_params,true);
// exit;

		$sql='
		SELECT
			w_m.mr_work_main_id,
			w_m.sys_timestamp as d_send,
			DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send2,
			w_m.mr_work_barcode,
			w_m.mr_work_remark,
			w_m.mr_topic,
			w_m.quty,
			sdt.mr_sub_districts_name,
			dt.mr_districts_name,
			pv.mr_provinces_name,
			w_bh.mr_cus_name, 
            w_bh.mr_cus_tel, 
            w_bh.mr_address,
            w_bh.mr_post_code,
			dep.mr_department_name as dep_send,
			dep.mr_department_code as dep_code_send,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_tel as send_tel,
			emp_send.mr_emp_name as send_name,
			emp_send.mr_emp_lastname as send_lastname,
			emp_send.mr_emp_email as send_email,
			r.mr_round_name,
			f.name as f_name,
			s.mr_status_id,
			s.mr_status_name,
			mess_send.mr_user_username as mess,
			type_bh.mr_work_byhand_name
 
		FROM
			mr_work_main w_m
            LEFT join mr_work_byhand w_bh on(w_bh.mr_work_main_id = w_m.mr_work_main_id)
			LEFT join  mr_user mess_send on(mess_send.mr_user_id = w_m.messenger_user_id)
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_bh.mr_send_emp_id)
            LEFT join  mr_work_byhand_type type_bh on(type_bh.mr_work_byhand_type_id = w_bh.mr_work_byhand_type_id)
            LEFT join  mr_department dep on(dep.mr_department_id = emp_send.mr_department_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
            LEFT join  mr_floor f on(f.mr_floor_id = emp_send.mr_floor_id)
            LEFT join  mr_sub_districts sdt on(w_bh.mr_sub_district_id  = sdt.mr_sub_districts_id )
            LEFT join  mr_districts dt on(w_bh.mr_district_id = dt.mr_districts_id)
            LEFT join  mr_provinces pv on(w_bh.mr_province_id  = pv.mr_provinces_id )
		WHERE
		w_m.mr_work_main_id in('.$params_work_main_id.')
		group by w_m.mr_work_main_id
		';

		if($chm == 'sesstion' ){
			$params = array_map('decode_work_main_id', $in_work_main_id_params);
		}else{
			$params = array_map('decode_work_main_id', $in_work_main_id_params);
		}
		$data = $send_workDao->select_send_work($sql,$params);


// echo "<pre>".print_r($in_work_main_id_params,true)."</pre>";
// echo "<pre>".print_r($data,true)."</pre>";
// exit;

		$page 	= 1;
		$index 	= 1;
		$no=1;
		$newdata = array();
		foreach($data as $i => $val_i){
			if(count($data)-1 == $i){
				//$val_i=$data[($i-1)];
				$val_i['no']='all';
				$val_i['qty']=count($data)-1;
			}else{
				//$val_i=$data[($i-1)];
				$val_i['no']=$no;
				$val_i['qty']=1;
			}
			$newdata[$page]['h']['p'] = $page;
			$newdata[$page]['h']['all'] = ceil(count($data)/4);  
			$newdata[$page]['d'][$index] = $val_i;
			if($index == 4){
				$index = 1;
				$page++;
			}else{
				$index++;
			}
			$no++;	
		}
}
// exit;


$txt_html ='<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
     
      font-family: "Garuda";
	  font-size: 18px

    }
	.border_l{
		border-right: solid;
		border-bottom: solid;
		border-width: 0.5px;
		position:relative;
	}.border_r{
		border-bottom: solid;
		border-width: 0.5px;
		position:relative;
	}

    .page {
        background: white;
            width: 29.7cm;
            height: 21cm;
            display: block;
           padding:0px;
		   margin:0px;
            margin-bottom: 0.5cm;
           
      }  

.txt_mini{
	font-size: 8px;
	margin-top:4px;
}
.text_p{
	margin-bottom:5px;
	font-size: 16px;
	border-bottom: 1px dotted black;
	text-align: left;
	
}.text_p>b{
	padding-bottom:5px;
}
h4{
 margin-bottom:10px;
}
.span_code{
	border-style: solid;
	border-width:medium;
	padding:0px;
	margin:0px;
}

.box_left{
	//float:left;
	//overflow: hidden;
	word-wrap:break-word;
	max-width:100%;
}
.box_rihth{
   position:absolute;
   right:0px;
    padding-left: 20px;
	margin-left:50%;
	//border-width: 2px;
	//overflow: hidden;
	max-width:100%;
	min-width:50%;
	//word-wrap:break-word;
}
@page :right {
	margin-top: 0cm;
	margin-bottom: 0cm;
	header: html_myHeader;
}



  </style>
</head>

<body>';

   foreach($newdata as $i => $page){

    $txt_html.='<div class="page">';
	if(count($page['d'])>1){
					$txt_html.=' <table border="0" width="100%">';
				}else{
					$txt_html.=' <table border="0" width="650px">';
				}
	  for($j=1;$j <= count($page['d']);$j++){
	  $td = $page['d'][$j];
		  $txt_html.='
		  <tr>
			<td class="border_l" width="50%">
							';
							//<img src="https://barcode.tec-it.com/barcode.ashx?data='.$td['mr_work_barcode'].'&code=Code39&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0" height="45" width="350" align="absmiddle">
							$txt_html.='<table width="650px" border="0">';
							$txt_html.='
							<tr>
								<td rowspan="4" width="100px" valign="top" align="left">
												<h3>
												BY HAND
												</h3>
											</td>
									<td align="center">
										<barcode code="'.$td['mr_work_barcode'].'" type="C128A" height="0.7"/>
									</td>
									<td rowspan="4" valign="bottom" align="center">
									</td>
								</tr>';
							$txt_html.='<tr><td><center>'.$td['mr_work_barcode'].'</center></td></tr>';
							//$txt_html.='<br>';
							$txt_html.='<tr><td><center><span class="span_code">'.$td['dep_code_send'].'</span></center></td></tr>';
							$txt_html.='<tr><td><center><p class="txt_mini">'.date('Y-m-d H:i:s').'</p></td></tr></center>';
				$txt_html.='</table>'; 

				$txt_html.=' 
				<table border="0" width="650px">
					<tr>
						<td>';
							$txt_html.='
							<table width="650px" border="0">
								<tr >
									<td align="left">
									<h3><b>จาก</b> : </h3>
									</td>
								</tr>
								<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อผู้ส่ง :   </b>'.$td['mr_emp_code'].' :  '.$td['send_name'].' '.$td['send_lastname'].'<b> โทร :  </b>'.$td['send_tel'].'</td></tr> '; 
								$txt_html.='
								<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['dep_code_send'].'</td></tr>
								<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['dep_send'].'</td></tr>';
								$txt_html.='
								<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่ออเอกสาร/จำนวน  :  </b>'.utf8_wordwrap( $td['mr_topic'],100,' / '.$td['quty'].' <br>',true).'</td></tr>
								<tr><td class="text_p"><b>&nbsp;&nbsp; หมายเหตุ : </b>'.utf8_wordwrap($td['mr_work_remark'],100,'<br>',true).'</td></tr>   	
							</table>
							
						</td>
					</tr>
					<tr>
						<td>';
						$tesiver = '<h3><b>กรุณาส่ง';
						$tesiver.='&nbsp;&nbsp; ('.$td['mr_work_byhand_name'].' )';
							
							$txt_html.='</b></h3>
							<table width="650px">
							<tr >
									
									<td align="left" width="100%">
									'.$tesiver.'
									</td>
								</tr>
									<tr><td class="text_p"><b>ชื่อผู้รับ :   </b>'.$td['mr_cus_name'].' <b>  โทร :   </b>'.$td['mr_cus_tel'].'</td></tr> '; 
									$txt_html.='
									<tr><td class="text_p"><b> ที่อยู่ :  </b>'.$td['mr_address'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; </b>'.$td['mr_sub_districts_name'].' '.$td['mr_districts_name'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; </b>'.$td['mr_provinces_name'].' '.$td['mr_post_code'].'</td></tr>
									';
								
								$txt_html.='
							</table>
						</td>
					</tr>
				</table>	
			</td>
		  ';
		$txt_html.='';
		$j++;
		if($j <= count($page['d'])){
		$td = $page['d'][$j];
		$txt_html.='
					<td class="border_l" width="50%">
					';
					//<img src="https://barcode.tec-it.com/barcode.ashx?data='.$td['mr_work_barcode'].'&code=Code39&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0" height="45" width="350" align="absmiddle">
					$txt_html.='<table width="650px" border="0">';
					$txt_html.='
					<tr>
						<td rowspan="4" width="100px" valign="top" align="left">
										<h3>
										BY HAND
										</h3>
									</td>
							<td align="center">
								<barcode code="'.$td['mr_work_barcode'].'" type="C128A" height="0.7"/>
							</td>
							<td rowspan="4" valign="bottom" align="center">
							</td>
						</tr>';
					$txt_html.='<tr><td><center>'.$td['mr_work_barcode'].'</center></td></tr>';
					//$txt_html.='<br>';
					$txt_html.='<tr><td><center><span class="span_code">'.$td['dep_code_send'].'</span></center></td></tr>';
					$txt_html.='<tr><td><center><p class="txt_mini">'.date('Y-m-d H:i:s').'</p></td></tr></center>';
			$txt_html.='</table>'; 

			$txt_html.=' 
			<table border="0" width="650px">
			<tr>
				<td>';
					$txt_html.='
					<table width="650px" border="0">
						<tr >
							<td align="left">
							<h3><b>จาก</b> : </h3>
							</td>
						</tr>
						<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อผู้ส่ง :   </b>'.$td['mr_emp_code'].' :  '.$td['send_name'].' '.$td['send_lastname'].'<b> โทร :  </b>'.$td['send_tel'].'</td></tr> '; 
						$txt_html.='
						<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['dep_code_send'].'</td></tr>
						<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['dep_send'].'</td></tr>';
						$txt_html.='
						<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่ออเอกสาร/จำนวน  :  </b>'.utf8_wordwrap( $td['mr_topic'],100,' / '.$td['quty'].' <br>',true).'</td></tr>
						<tr><td class="text_p"><b>&nbsp;&nbsp; หมายเหตุ : </b>'.utf8_wordwrap($td['mr_work_remark'],100,'<br>',true).'</td></tr>   	
					</table>
					
				</td>
			</tr>
			<tr>
				<td>';
				$tesiver = '<h3><b>กรุณาส่ง';
				$tesiver.='&nbsp;&nbsp; ('.$td['mr_work_byhand_name'].' )';
					
					$txt_html.='</b></h3>
					<table width="650px">
					<tr >
							
							<td align="left" width="100%">
							'.$tesiver.'
							</td>
						</tr>
							<tr><td class="text_p"><b>ชื่อผู้รับ :   </b>'.$td['mr_cus_name'].' <b>  โทร :   </b>'.$td['mr_cus_tel'].'</td></tr> '; 
							$txt_html.='
							<tr><td class="text_p"><b> ที่อยู่ :  </b>'.$td['mr_address'].'</td></tr>
							<tr><td class="text_p"><b>&nbsp;&nbsp; </b>'.$td['mr_sub_districts_name'].' '.$td['mr_districts_name'].'</td></tr>
							<tr><td class="text_p"><b>&nbsp;&nbsp; </b>'.$td['mr_provinces_name'].' '.$td['mr_post_code'].'</td></tr>
							';
						
						$txt_html.='
					</table>
				</td>
			</tr>
			</table>	
			</td>
		  ';
		  }
		$txt_html.='
		</tr>';
		 
	  }
	  $txt_html.='
	  
	  
</table>			
</div>
';





}
$txt_html.='</body>

</html>';

// echo $txt_html;
// exit;
//$fp = fopen('lidn.txt', 'w');
//fwrite($fp, $txt_html);
//fclose($fp);
////
//
//exit;
//ob_start();

//$html = ob_get_clean();

//echo $txt_html;
//exit;

function utf8_wordwrap($string, $width=75, $break="\n", $cut=false)
{
  if($cut) {
    // Match anything 1 to $width chars long followed by whitespace or EOS,
    // otherwise match anything $width chars long
    $search = '/(.{1,'.$width.'})(?:\s|$)|(.{'.$width.'})/uS';
    $replace = '$1$2'.$break;
  } else {
    // Anchor the beginning of the pattern with a lookahead
    // to avoid crazy backtracking when words are longer than $width
    $pattern = '/(?=\s)(.{1,'.$width.'})(?:\s|$)/uS';
    $replace = '$1'.$break;
  }
  return preg_replace($search, $replace, $string);
}


require_once 'ThaiPDF/thaipdf.php';
$left=3;
$right=3;
$top=5;
$bottom=5;
$header=0;
$footer=5;
$filename='file.pdf';
pdf_margin($left,$right,$top, $bottom,$header,$footer);
pdf_html($txt_html);
pdf_orientation('L');        
pdf_echo();

exit;


//require_once 'mpdf-development//thaipdf.php';
//require_once 'ThaiPDF/thaipdf.php';
require_once 'mpdf-development/vendor/autoload.php';

 $mpdf = new \Mpdf\Mpdf();
 $pdf_orientation = 'L';//L&P;
 $left=3;
 $right=3;
 $top=5;
 $bottom=5;
 $header=0;
 $footer=1;
$align = $__pdf_pgn_oalign;
// $border = "border-top-";
// $pgf_o = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fo", $pgf_o);
// $align = $__pdf_pgn_ealign;
// $pgf_e = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fe", $pgf_e);
$mpdf->AddPage($pdf_orientation,'','','','',$left,$right,$top,$bottom,$header,$footer,'','','html_fo', 'html_fe', 0, 0, 1, 1);

 //$mpdf->AddPage('L'); 
 $mpdf->WriteHTML($txt_html);
 //$mpdf->setFooter('{PAGENO}');
 $mpdf->Output();


 exit; 




$filename='file.pdf';
pdf_margin($left,$right,$top, $bottom,$header,$footer);
pdf_html($txt_html);
pdf_orientation('L');   
     
pdf_echo();

exit;
$template = Pivot_Template::factory('branch/printcoverpage.tpl');
$template->display(array(
	//'debug' => print_r($newdata,true),
	'data' => $newdata,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';
require_once 'Dao/Employee.php';
require_once 'nocsrf.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao		= new Dao_UserRole();
$statusDao 			= new Dao_Status();
$employeeDao 		= new Dao_Employee();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
$status_data = $statusDao->getStatusall();
$status_data2 = $statusDao->getStatusall2();

$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

// echo $user_id."<br>";
// echo print_r($user_data);

// exit();

$template = Pivot_Template::factory('employee/search_detail.tpl');
$template->display(array(
	//'debug' => print_r($status_data,true),
	'status_data' => $status_data,
	'status_data2' => $status_data2,
	//'userRoles' => $userRoles,
	//'success' => $success,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	'user_arr' => json_encode($user_data),
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'csrf' 					=>  NoCSRF::generate( 'csrf_token'),
	'serverPath' => $_CONFIG->site->serverPath
));
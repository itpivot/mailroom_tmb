<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Status.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_inout.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRole_Dao		= new Dao_UserRole();
$work_main_Dao      = new Dao_Work_main();
$branchDao			= new Dao_Branch();
$floorDao 			= new Dao_Floor();
$statusDao          = new Dao_Status();
$logDao             = new Dao_Work_log();
$work_inoutDao 		= new Dao_Work_inout(); 

$user_id  = $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$id = $req->get('id');
$data_search['mr_work_main_id'] = $id;
// $id = 9040;

$data           = $work_main_Dao->getWorkMainById($id);



//$branch_data    = $branchDao->getBranch();
//$floor_data     = $floorDao->fetchAll();
$status_data    = $statusDao->getStatusBranch();
// $log_data       = $logDao->getWorkLogById($id);
$user_data_show = $userDao->getEmpDataByuseridProfileBranch($user_id);
$report            = $work_inoutDao->searchBranch_manege( $data_search );


$log            = $logDao->getWorkLogById($id);
$log_data  = array();
foreach($log  as $key => $l){
    $st = $l['mr_status_id'];
    switch ($st) {
        case 1:  $l['mr_status_id']     = 1 ;   break;
        case 2:  $l['mr_status_id']     = 2 ;   break;
        case 3:  $l['mr_status_id']     = 3 ;   break;
        case 4:  $l['mr_status_id']     = 4 ;   break;
        case 5:  $l['mr_status_id']     = 5 ;   break;
        case 6:  $l['mr_status_id']     = 6 ;   break;
        case 7:  $l['mr_status_id']     = 7 ;   break;
        case 8:  $l['mr_status_id']     = 8 ;   break;
        case 9:  $l['mr_status_id']     = 9 ;   break;
        case 10: $l['mr_status_id']     = 3 ;   break;
        case 11: $l['mr_status_id']     = 11 ;  break;
        case 12: $l['mr_status_id']     = 5 ;   break;
        case 13: $l['mr_status_id']     = 13 ;  break;
        case 14: $l['mr_status_id']     = 14 ;  break;
        case 15: $l['mr_status_id']     = 15 ;  break;
        default:
        $l['mr_status_id']     = 15 ;
    }

    $log_data[$key] = $l; 
}




$log_status = array();
$new_status_arr = array();
$run_status_txt = '';
$run_status_arr = array();
foreach($status_data as $key => $val) {
	$new_status_arr[$val['mr_status_id']] = $val;
}




if($data['mr_type_work_id']==1){//  สนญ - สนญ
    $run_status_txt = '1,2,3,4,5';
    $run_status_arr = explode(',',$run_status_txt);
}elseif($data['mr_type_work_id']== 2 and $data['mr_user_role_id']== 2 ){ // สนญ - ส่งสาขา
//1,2,3,10,11,13,5,12
$run_status_txt = '1,2,3,11,13,5';
$run_status_arr = explode(',',$run_status_txt);
}elseif($data['mr_type_work_id']== 3 and $data['mr_user_role_id']==5){ // สาขา - สนญ
//7,8,9,14,10,3,4,'5,12'
$run_status_txt = '7,8,9,14,3,4,5';
$run_status_arr = explode(',',$run_status_txt);
}else{//สาขา - สาขา
//7,8,9,14,10,3,11,13,5,12
$run_status_txt = '7,8,9,14,3,11,13,5';
$run_status_arr = explode(',',$run_status_txt);
}

$date_start=0;
$date_end='';
$end_step = '';
$is12 = 0;
foreach($run_status_arr as $key => $val2) {
   $val=$new_status_arr[$val2];
   foreach($log_data as $kk => $vv) {
		if($vv['mr_status_id'] == 12){
			$is12 ++;
		}
   }
    foreach($log_data as $k => $v) {
        //$log_status[$key]['status_id'] = $val['mr_status_id'];
        //$log_status[$key]['name'] = $val['mr_status_name'];
		$date_end = $key;
        if($val['mr_status_id'] == $v['mr_status_id']) {
            $log_status[$key]['status_id'] = $val['mr_status_id'];
            $log_status[$key]['name'] = $val['mr_status_name'];
            $log_status[$key]['active'] = 'is-complete';
            $log_status[$key]['emp_code'] = $v['mr_emp_code'];
            $log_status[$key]['sys_timestamp'] = $v['sys_timestamp'];
            $log_status[$key]['icon'] = '<i class="material-icons">done</i>';
			$log_status[$key]['key_1'] = "";
			$log_status[$key]['key_2'] = "";
			
			
			if($key!=0){
				$dt1 = $log_status[$date_start]['sys_timestamp'];
				$dt2 = $log_status[$date_end]['sys_timestamp'];
				$log_status[$key]['key_1'] = $date_start; 
				$log_status[$key]['key_2'] =  $date_end;
				$arr = json_decode(json_encode(datetimeDiff($dt1, $dt2)), True);
				$d=($arr['day']!=0)?$arr['day'].' วัน  ':''; 
				$h=($arr['hour']!=0)?$arr['hour'].' ชั่วโมง  ':''; 
				$i=($arr['min']!=0)?$arr['min'].' นาที ':''; 
				$s=($arr['sec']!=0)?$arr['sec'].' วินาที  ':''; 
				$log_status[$key]['date'] =  $d.$h.$i.$s;
				
				$end_step =	$key+1;
			}
			$date_start=$date_end;	
			if($val['mr_status_id'] == $data['mr_status_id'] and $is12 == 0 and $data['mr_status_id']= 12){
				//$log_status[$key]['date'] = '';
				//$log_status[$key]['emp_code'] = '';
			}
			break;
        } else{
			$log_status[$key]['status_id'] = $val['mr_status_id'];
            $log_status[$key]['name'] = $val['mr_status_name'];
            $log_status[$key]['active'] = '';
            $log_status[$key]['emp_code'] = '';
            $log_status[$key]['sys_timestamp'] = $v['sys_timestamp'];
            $log_status[$key]['icon'] = '<i class="material-icons">done</i>';
            $log_status[$key]['icon'] = '';
            $log_status[$key]['key'] = '';
			//break;
		} 
    }
}

//echo '<pre>'.print_r($log_status,true).'</pre>';
//$end_step;
$received = "real";
if(!empty($data['real_receive_id'])) {
	if($data['receive_id'] == $data['real_receive_id']) {
		$received = "real";
	} else {
		$received = "replace";
	}
}

//echo '<pre>'.print_r($data['mr_user_role_id'],true).'</pre>';
//echo '<pre>'.print_r($data['mr_type_work_id'],true).'</pre>';
//echo '<pre>'.print_r($log_data,true).'</pre>';
//echo '<pre>'.print_r($status_data,true).'</pre>';



$dt1="2019-01-01 08:00:35";

$dt2="2019-01-02 08:00:45";



function datetimeDiff($dt1, $dt2){
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);

        $dtd = new stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2-$t1);
        $dtd->total_min = floor($dtd->total_sec/60);
        $dtd->total_hour = floor($dtd->total_min/60);
        $dtd->total_day = floor($dtd->total_hour/24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour -($dtd->total_day*24);
        $dtd->min = $dtd->total_min -($dtd->total_hour*60);
        $dtd->sec = $dtd->total_sec -($dtd->total_min*60);
        return $dtd;
    }
    
    

$report_custom = array();

if($report_custom[0]['mr_type_work_id']==2){ 
    $mr_branch_name_re  = $report[0]['mr_branch_code_re'].' : '.$report[0]['mr_branch_name_re'];
    $floor_re_re        = $report[0]['mr_branch_floor_re'];
}else{
    $mr_branch_name_re  = $report[0]['depart_code_receive'].' : '.$report[0]['depart_name_receive'];
    $floor_re_re        = $report[0]['depart_floor_receive'];
}  



if($report_custom[0]['mr_user_role_id']==5){ 
    $mr_branch_name_sen = $report[0]['mr_branch_code_sen'].' : '.$report[0]['mr_branch_name_sen'];
}else{
    $mr_branch_name_sen = $report[0]['mr_department_code'].' : '.$report[0]['mr_department_name'];
}


$report_custom['mr_branch_sen']    = $mr_branch_name_sen;
$report_custom['name_sen']              = $report[0]['mr_emp_code'].' : '.$report[0]['mr_emp_name'].' '.$report[0]['mr_emp_lastname'];
$report_custom['tel_sen']               = $report[0]['mr_emp_tel'];

$report_custom['name_re']               = $report[0]['mr_emp_code_re'].' : '.$report[0]['name_re'].' '.$report[0]['lastname_re'];
$report_custom['tel_re']                = $report[0]['tel_re'];
$report_custom['mr_branch_re']          = $mr_branch_name_re;
$report_custom['floor_re']              = $floor_re_re;
$report_custom['topic']                 = $report[0]['mr_topic']; 
$report_custom['mr_work_remark']        = $report[0]['mr_work_remark']; 
$report_custom['mr_type_work_name']        = $report[0]['mr_type_work_name']; 

//echo '<pre>'.print_r($report_custom,true).'</pre>'; 
//echo '<pre>'.print_r($report,true).'</pre>'; 

$template = Pivot_Template::factory('employee/work_info.tpl');
$template->display(array(
    'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles(),
    'report' => $report,
    'report_custom' => $report_custom,
    'user_data' => $user_data,
    'user_data_show' => $user_data_show,
    'serverPath' => $_CONFIG->site->serverPath,
    //'floor_data' => $floor_data,
    //'branch_data' => $branch_data,
    'end_step' => $end_step,
    'data' => $data,
    'status' => $log_status,
    'wid' => $id,
    'received' => $received
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'nocsrf.php';



/* Check authentication */
$auth = new Pivot_Auth();


if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}


$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$branchDao			= new Dao_Branch();

$sql="SELECT c.* ,
			p.mr_position_name
FROM mr_contact c
left join mr_emp e on(c.emp_code = e.mr_emp_code)
left join mr_position p on(p.mr_position_id = e.mr_position_id)
WHERE `acctive` = 1";
//$contactdata = $userDao->select($sql);
//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
//$floor_data = $floorDao->fetchAll();
$branch_data = $branchDao->getBranch();

$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$time = date("H:i:s");
$time_check = strtotime('4.00pm');
$time_16 = date("H:i:s",$time_check);

if( $time >= $time_16 ){
	$tomorrow = 1;
}else{
	$tomorrow = 0;
}	


$barcode 			= "TM".date("dmy");


//$emp_data = $employeeDao->getEmpDataSelect();

//echo "<pre>".print_r($tomorrow,true)."</pre>";


$template = Pivot_Template::factory('employee/work_out.tpl');
$template->display(array(
	//'debug' 				=> print_r($branch_data,true),
	//'contactdata' 			=> $contactdata,
	//'userRoles' 			=> $userRoles,
	'branch_data' 			=> $branch_data,
	//'success' 				=> $success,
	'barcode' 				=> $barcode,
	//'floor_data' 			=> $floor_data,
	//'userRoles' 			=> $userRoles,
	//'emp_data' 				=> $emp_data,
	//'users' 				=> $users,
	
	'csrf' 					=>  NoCSRF::generate( 'csrf_token'),
	'tomorrow' 				=> $tomorrow,
	'user_data' 			=> $user_data,
	'role_id' 				=> $auth->getRole(),
	'roles' 				=> Dao_UserRole::getAllRoles(),
	'serverPath' 			=> $_CONFIG->site->serverPath
));
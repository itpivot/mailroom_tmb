<?php
require_once 'Pivot/Dao.php';


class Dao_Department extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_department');
    }

    function getFloor()
    {
        $sql =
        '
            SELECT DISTINCT mr_department_floor
            FROM mr_department
        ';
        return $this->_db->fetchAll($sql);
    }
	 function getdepartmentall()
    {
        $sql =
        '
            SELECT 
				mr_department_id,
				mr_department_code
            FROM mr_department
        ';
        return $this->_db->fetchAll($sql);
    }
	function getFloorWorkInOut()
    {
        $sql =
        '
            SELECT DISTINCT mr_department_floor,
			mr_department_id
            FROM mr_department
        ';
        return $this->_db->fetchAll($sql);
    }
	
	function getIDByCode( $department_code )
    {
        $params = array();
        $sql =
        '
            SELECT 
				mr_department_id
            FROM mr_department
			WHERE mr_department_code LIKE ?
        ';

        array_push($params, (string)'%'.$department_code.'%');

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
    }
	
	
	function getDepartmentNotBranchTXT( $txt )
    {
        $params = array();
        $sql =
        '
            SELECT 
					* 
			FROM
				mr_department
			WHERE
				( mr_department_code like ? or 
					 mr_department_name like ? ) and
				mr_department_code NOT LIKE "00000%"
			Order By mr_department_code ASC
            limit 10 
        ';

        array_push($params, (string)'%'.$txt.'%');
        array_push($params, (string)'%'.$txt.'%');


        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
    }

	function select_department($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}

    function getdepartmentsaech($filter)
    {
        $params = array();
        $sql =
        '
            SELECT 
				mr_department_id,
				mr_department_code,
                mr_department_name,
                department_sysdate
            FROM mr_department
            where mr_department_id is not null
        ';
  
        if($filter['department_code'] !=''){
            $sql .=' and mr_department_code like ?'; 
            array_push($params, trim($filter['department_code']).'%');
        }
        if($filter['department_name'] !=''){
            $sql .=' and mr_department_name like ?'; 
            array_push($params, trim($filter['department_name']).'%');
        }
        
        
        //array_push($params, (string)'%'.$txt.'%');
        //array_push($params, (string)'%'.$txt.'%');


        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }

        return $stmt->fetchAll();
    }
    function CheckDepartmentDuplicate($mr_department_id,$mr_department_code)
    {
        $params = array();
        $sql =
        '
            SELECT 
				mr_department_id,
				mr_department_code,
                mr_department_name,
                department_sysdate
            FROM mr_department
            where mr_department_id is not null
        ';
  
        if($mr_department_code !=''){
            $sql .=' and mr_department_code like ?'; 
            array_push($params, trim($mr_department_code));
        }
        if($mr_department_id !=''){
            $sql .=' and mr_department_id != ? '; 
            array_push($params, $mr_department_id);
        }
        
        
        //array_push($params, (string)'%'.$txt.'%');
        //array_push($params, (string)'%'.$txt.'%');


        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }

        return $stmt->fetchAll();
    }
	
}
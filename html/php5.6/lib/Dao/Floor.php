<?php
require_once 'Pivot/Dao.php';


class Dao_Floor extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_floor');
    }
	
	function getFloor()
    {
        $sql =
        '
            SELECT DISTINCT name as mr_department_floor,mr_floor_id
            FROM mr_floor
        ';
        return $this->_db->fetchAll($sql);
    }
	function getFloorAll()
    {
        $sql =
        '
            SELECT name,mr_floor_id
            FROM mr_floor
        ';
        return $this->_db->fetchAll($sql);
    }
	
	
	function getFloorMess()
    {
        $sql =
        '
            SELECT f.* 
            FROM mr_zone z
			LEFT JOIN mr_floor f ON ( z.mr_floor_id = f.mr_floor_id )
			Order by f.mr_floor_id ASC
			
        ';
        return $this->_db->fetchAll($sql);
    }
	
	function getIDFloorByName( $floor_name )
    {
        $params = array();
        $sql =
        '
            SELECT mr_floor_id
            FROM mr_floor
			WHERE name = ?
			
        ';

		array_push($params, (string)$floor_name);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
    }


    function getFloors($floor)
    {
        $params = array();

        $sql = '
            SELECT 
                *
            FROM '.$this->_tableName.' 
            WHERE 
                name LIKE ?
        ';

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        array_push($params, trim($floor));

        $stmt->execute($params);
        return $stmt->fetchAll();
    }
    
    
    function pdo_fetch($sql, $params = array()) 
    {
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }

        return $stmt->fetch();
    }

    function pdo_fetchAll($sql, $params = array()) 
    {
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }

        return $stmt->fetchAll();
    }

    function select_floor($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	

    function getFloorsBymr_building_id($mr_building_id)
    {
        $params = array();

        $sql = '
            SELECT 
                *
            FROM '.$this->_tableName.' 
            WHERE 
            mr_building_id = ?
        ';

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        array_push($params, trim($mr_building_id));
        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }
        return $stmt->fetchAll();
    }
    function getFloorsAllmr_building_id()
    {
        $params = array();

        $sql = '
            SELECT 
                *
            FROM '.$this->_tableName.' f
            Left join mr_building b on(b.mr_building_id = f.mr_building_id)
            WHERE 
            mr_building_id = ?
        ';

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

       // array_push($params, trim($mr_building_id));
        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }
        return $stmt->fetchAll();
    }
    function getFloors_All_buildingJoin()
    {
        $params = array();

        $sql = '
            SELECT 
                f.mr_floor_id,
                f.name,
                b.mr_building_id,
                b.mr_building_name,
                e.mr_emp_code,
                e.mr_emp_name,
                e.mr_emp_lastname
            FROM '.$this->_tableName.' f
            Left join mr_building b on(b.mr_building_id = f.mr_building_id)
            Left join mr_zone z on(f.mr_floor_id = z.mr_floor_id)
            Left join mr_user u on(z.mr_user_id = u.mr_user_id)
            Left join mr_emp e on(u.mr_emp_id = e.mr_emp_id)
        ';

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

       // array_push($params, trim($mr_building_id));
        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }
        return $stmt->fetchAll();
    }
    function CheckFloorsDuplicate($floor_name,$mr_building_id)
    {
        $params = array();

        $sql = '
            SELECT 
                *
            FROM '.$this->_tableName.' f
            Left join mr_building b on(b.mr_building_id = f.mr_building_id)
            WHERE 
            f.mr_building_id = ?
            and f.name like ?
        ';

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        array_push($params, trim($mr_building_id));
        array_push($params, trim($floor_name));
        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }
        return $stmt->fetchAll();
    }
    function load_fioor_data($filter)
    {
        $params = array();

        $sql = '
            SELECT 
                f.mr_floor_id,
                f.sys_time,
                f.name as mr_floor_mame,
                f.floor_builder,
                f.floor_level,
                b.mr_branch_id,
                b.mr_branch_name,
                bd.mr_building_id,
                bd.mr_building_name
            FROM '.$this->_tableName.' f
            Left join mr_building bd on(bd.mr_building_id = f.mr_building_id)
            Left join mr_branch b on(b.mr_branch_id = bd.mr_branch_id)
            where f.mr_floor_id is not null
        ';
        
        if(isset($filter['floor_name'])){
            if($filter['floor_name'] !=''){
                $sql .=' and f.name like ?'; 
                array_push($params, trim($filter['floor_name']).'%');
            }
        }
        if(isset($filter['floor_level'])){
            if($filter['floor_level'] !=''){
                $sql .=' and f.floor_level like ?'; 
                array_push($params, trim($filter['floor_level']));
            }
        }
        if(isset($filter['mr_branch_id'])){
            if($filter['mr_branch_id'] !=''){
                $sql .=' and b.mr_branch_id = ?'; 
                array_push($params, trim($filter['mr_branch_id']));
            }
        }
        if(isset($filter['mr_building_id'])){
            if($filter['mr_building_id'] !=''){
                $sql .=' and bd.mr_building_id = ?'; 
                array_push($params, trim($filter['mr_building_id']));
            }
        }

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

       // array_push($params, trim($mr_building_id));
        if(!empty($params)) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }
        return $stmt->fetchAll();
    }
	
    
}
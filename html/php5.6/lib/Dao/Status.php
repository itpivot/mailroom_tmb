<?php
require_once 'Pivot/Dao.php';


class Dao_Status extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_status');
    }
	
	
	function getStatusBranch()
	{
		$sql =
		'
			SELECT 
				s.*
			FROM mr_status s
			WHERE
				s.mr_status_id is not null 
			order by mr_status_id asc
				
		';
		
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}function getStatusall()
	{
		$sql =
		'
			SELECT 
				s.*
			FROM mr_status s
			WHERE
				s.mr_type_work_id = 1 or
				s.mr_type_work_id is null
			order by mr_status_id asc
				
		';
		
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}function getStatusall2()
	{
		$sql =
		'
			SELECT 
				s.*
			FROM mr_status s
			WHERE
				s.mr_status_id >= 12 
			order by mr_status_id asc
				
		';
		
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}function getStatusall3()
	{
		$sql =
		'
			SELECT 
				s.*
			FROM mr_status s
			WHERE
				s.mr_type_work_id in(2)
			order by mr_status_id asc
				
		';
		
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	

	
	
}
<?php
require_once 'Pivot/Dao.php';


class Dao_Work_byhand extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_byhand');
    }
    function getReceiveBranchfromMailroom($status, $workType)
	{
		$params = array();
		$in_status_params = array();
		$in_workType_params = array();

		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}
		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
                bht.mr_work_byhand_name,
                wr.mr_round_name,
                bh.mr_cus_name,
                bh.mr_address,
                bh.mr_cus_tel,
                sd.mr_sub_districts_name,
                sd.zipcode,
                dt.mr_districts_name,
                pv.mr_provinces_name,
				st.mr_status_name
			from mr_work_byhand as bh
				left join mr_work_main as m on (m.mr_work_main_id = bh.mr_work_main_id)
				left join mr_status as st on (st.mr_status_id = m.mr_status_id)
				left join mr_work_byhand_type as bht on (bht.mr_work_byhand_type_id  = bh.mr_work_byhand_type_id )
				left join mr_round as wr on (wr.mr_round_id  = m.mr_round_id )
				left join mr_sub_districts as sd on (sd.mr_sub_districts_id  = bh.mr_sub_district_id )
				left join mr_districts as dt on (dt.mr_districts_id  = bh.mr_district_id )
				left join mr_provinces as pv on (pv.mr_provinces_id  = bh.mr_province_id )
			where bh.mr_work_main_id is not null
		';

		if(!empty($txt)) {
			$sql .= " and ( m.mr_work_barcode like ? or ";
			$sql .= " bh.mr_cus_name like ? or ";
			$sql .= " bh.mr_cus_tel like ? or ";
			$sql .= " bh.mr_address like ? ) ";

			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
		}

		if(!empty($hub_id)) {
			$sql .= " and b.mr_hub_id = ? and b.mr_hub_id is not null ";
			array_push($params, (int)$hub_id);
		}

		$sql .= " and m.mr_status_id in (".$in_status_condition.") ";

		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		$sql .= "order by m.mr_work_barcode asc";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_status_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
    function seach_Bytext($status, $workType,$txt)
	{
		$params = array();
		$in_status_params = array();
		$in_workType_params = array();

		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}
		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
                bht.mr_work_byhand_name,
                wr.mr_round_name,
                bh.mr_cus_name,
                bh.mr_address,
                bh.mr_cus_tel,
                sd.mr_sub_districts_name,
                sd.zipcode,
                dt.mr_districts_name,
                pv.mr_provinces_name,
				st.mr_status_name
			from mr_work_byhand as bh
				left join mr_work_main as m on (m.mr_work_main_id = bh.mr_work_main_id)
				left join mr_status as st on (st.mr_status_id = m.mr_status_id)
				left join mr_work_byhand_type as bht on (bht.mr_work_byhand_type_id  = bh.mr_work_byhand_type_id )
				left join mr_round as wr on (wr.mr_round_id  = m.mr_round_id )
				left join mr_sub_districts as sd on (sd.mr_sub_districts_id  = bh.mr_sub_district_id )
				left join mr_districts as dt on (dt.mr_districts_id  = bh.mr_district_id )
				left join mr_provinces as pv on (pv.mr_provinces_id  = bh.mr_province_id )
			where bh.mr_work_main_id is not null
		';

		if(!empty($txt)) {
			$sql .= " and ( m.mr_work_barcode like ? or ";
			$sql .= " bh.mr_cus_name like ? or ";
			$sql .= " bh.mr_cus_tel like ? or ";
			$sql .= " bh.mr_address like ? ) ";

			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
		}

		if(!empty($hub_id)) {
			$sql .= " and b.mr_hub_id = ? and b.mr_hub_id is not null ";
			array_push($params, (int)$hub_id);
		}

		$sql .= " and m.mr_status_id in (".$in_status_condition.") ";

		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		$sql .= "order by m.mr_work_barcode asc";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_status_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function updateWithMainId($data, $main_id)
	{
		$this->_db->beginTransaction();
		try {
            $id = $this->_db->update($this->_tableName, $data, 'mr_work_main_id = ' . $main_id);
            $this->_db->commit();
            return $id;
        } catch(Exception $e) {
            $this->_db->rollBack();
            return $e->getMessage();
        }
	}

	function seach_send_Bytext($user_id,$status, $workType,$txt)
	{
		$params = array();
		$in_status_params = array();
		$in_workType_params = array();

		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}
		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
                bht.mr_work_byhand_name,
                wr.mr_round_name,
                bh.mr_cus_name,
                bh.mr_address,
                bh.mr_cus_tel,
                sd.mr_sub_districts_name,
                sd.zipcode,
                dt.mr_districts_name,
                pv.mr_provinces_name,
				st.mr_status_name
			from mr_work_byhand as bh
				left join mr_work_main as m on (m.mr_work_main_id = bh.mr_work_main_id)
				left join mr_status as st on (st.mr_status_id = m.mr_status_id)
				left join mr_work_byhand_type as bht on (bht.mr_work_byhand_type_id  = bh.mr_work_byhand_type_id )
				left join mr_round as wr on (wr.mr_round_id  = m.mr_round_id )
				left join mr_sub_districts as sd on (sd.mr_sub_districts_id  = bh.mr_sub_district_id )
				left join mr_districts as dt on (dt.mr_districts_id  = bh.mr_district_id )
				left join mr_provinces as pv on (pv.mr_provinces_id  = bh.mr_province_id )
			where bh.mr_work_main_id is not null
		';

		if(!empty($txt)) {
			$sql .= " and ( m.mr_work_barcode like ? or ";
			$sql .= " bh.mr_cus_name like ? or ";
			$sql .= " bh.mr_cus_tel like ? or ";
			$sql .= " bh.mr_address like ? ) ";

			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
		}

		if(!empty($hub_id)) {
			$sql .= " and b.mr_hub_id = ? and b.mr_hub_id is not null ";
			array_push($params, (int)$hub_id);
		}
		if(!empty($user_id)) {
			$sql .= " and m.messenger_user_id = ? ";
			array_push($params, (int)$user_id);
		}

		$sql .= " and m.mr_status_id in (".$in_status_condition.") ";

		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		$sql .= "order by m.mr_work_barcode asc";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_status_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function getDatasend_byhand($user_id,$status, $workType)
	{
		$params = array();
		$in_status_params = array();
		$in_workType_params = array();

		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}
		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
                bht.mr_work_byhand_name,
                wr.mr_round_name,
                bh.mr_cus_name,
                bh.mr_address,
                bh.mr_cus_tel,
                sd.mr_sub_districts_name,
                sd.zipcode,
                dt.mr_districts_name,
                pv.mr_provinces_name,
				st.mr_status_name
			from mr_work_byhand as bh
				left join mr_work_main as m on (m.mr_work_main_id = bh.mr_work_main_id)
				left join mr_status as st on (st.mr_status_id = m.mr_status_id)
				left join mr_work_byhand_type as bht on (bht.mr_work_byhand_type_id  = bh.mr_work_byhand_type_id )
				left join mr_round as wr on (wr.mr_round_id  = m.mr_round_id )
				left join mr_sub_districts as sd on (sd.mr_sub_districts_id  = bh.mr_sub_district_id )
				left join mr_districts as dt on (dt.mr_districts_id  = bh.mr_district_id )
				left join mr_provinces as pv on (pv.mr_provinces_id  = bh.mr_province_id )
			where bh.mr_work_main_id is not null
		';
		
		if(!empty($txt)) {
			$sql .= " and ( m.mr_work_barcode like ? or ";
			$sql .= " bh.mr_cus_name like ? or ";
			$sql .= " bh.mr_cus_tel like ? or ";
			$sql .= " bh.mr_address like ? ) ";

			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
		}

		if(!empty($hub_id)) {
			$sql .= " and b.mr_hub_id = ? and b.mr_hub_id is not null ";
			array_push($params, (int)$hub_id);
		}
		if(!empty($user_id)) {
			$sql .= " and m.messenger_user_id = ? ";
			array_push($params, (int)$user_id);
		}
		$sql .= " and m.mr_status_id in (".$in_status_condition.") ";

		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		$sql .= "order by m.mr_work_barcode asc";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_status_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function getdatatoday()
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			w_m.sys_timestamp as d_send,
			DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send2,
			w_m.mr_work_barcode,
			w_m.mr_work_remark,
			w_bh.mr_cus_name, 
            w_bh.mr_cus_tel, 
            w_bh.mr_address,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_name,
			emp_send.mr_emp_lastname,
			concat(emp_send.mr_emp_code ,"  " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_send,
			r.mr_round_name,
			f.name as f_name,
			s.mr_status_id,
			s.mr_status_name,
			mess_send.mr_user_username as mess,
			type_bh.mr_work_byhand_name
 
		FROM
			mr_work_main w_m
            LEFT join mr_work_byhand w_bh on(w_bh.mr_work_main_id = w_m.mr_work_main_id)
			LEFT join  mr_user mess_send on(mess_send.mr_user_id = w_m.messenger_user_id)
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_bh.mr_send_emp_id)
            LEFT join  mr_work_byhand_type type_bh on(type_bh.mr_work_byhand_type_id = w_bh.mr_work_byhand_type_id)
            LEFT join  mr_department dep on(dep.mr_department_id = emp_send.mr_department_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
            LEFT join  mr_floor f on(f.mr_floor_id = emp_send.mr_floor_id)
		WHERE
			w_m.sys_timestamp LIKE DATE_FORMAT(CURDATE(), "%Y-%m-%d %")
			and w_m.mr_type_work_id = 4
			and w_m.mr_status_id != 6
		group by w_m.mr_work_main_id
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}

	function getDataWithTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			SELECT * FROM mr_work_byhand wbh
			where
					( wbh.mr_cus_name like ?
					 ) 
					 group by wbh.mr_cus_name,wbh.mr_address
					 ORDER BY `mr_messenger_id`  asc
					 limit 10
		';

		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getdataByuserID($user_id,$byVal=null)
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			w_m.sys_timestamp as d_send,
			w_m.mr_work_date_sent as d_send2,
			w_m.mr_work_barcode,
			w_m.mr_work_remark,
			w_m.mr_topic,
			w_bh.mr_cus_name, 
            w_bh.mr_cus_tel, 
            w_bh.mr_address,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_tel as send_tel,
			emp_send.mr_emp_name as send_name,
			emp_send.mr_emp_lastname  as send_lastname,
			emp_send.mr_emp_email  as send_email,
			concat(emp_send.mr_emp_code ,"  " , emp_send.mr_emp_name,"  
					" , emp_send.mr_emp_lastname,"  
					",dep.mr_department_name) as name_send,
			r.mr_round_name,
			f.name as f_name,
			s.mr_status_id,
			s.mr_status_name,
			mess_send.mr_user_username as mess,
			type_bh.mr_work_byhand_name
 
		FROM
			mr_work_main w_m
            LEFT join mr_work_byhand w_bh on(w_bh.mr_work_main_id = w_m.mr_work_main_id)
			LEFT join  mr_user mess_send on(mess_send.mr_user_id = w_m.messenger_user_id)
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_bh.mr_send_emp_id)
            LEFT join  mr_work_byhand_type type_bh on(type_bh.mr_work_byhand_type_id = w_bh.mr_work_byhand_type_id)
            LEFT join  mr_department dep on(dep.mr_department_id = emp_send.mr_department_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
            LEFT join  mr_floor f on(f.mr_floor_id = emp_send.mr_floor_id)
		WHERE
			w_m.mr_type_work_id = 4
			and w_m.mr_user_id = ?';
		array_push($params, (int)$user_id);
		if(isset($byVal['date_send'])){
				$sql .=' and (w_m.mr_work_date_sent BETWEEN  ? and ?
				or w_m.sys_timestamp BETWEEN ? and ?
				)';
				array_push($params, trim($byVal['date_send'])." 00:00:00");
				array_push($params, trim($byVal['date_send'])." 03:59:59");
				array_push($params, trim($byVal['date_send'])." 00:00:00");
				array_push($params, trim($byVal['date_send'])." 03:59:59");
			}
		$sql .='	
		group by w_m.mr_work_main_id
		limit 0,3000';
		

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		//  echo $sql;
		 
		//  echo  print_r($byVal);
        // exit;
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function getdataByval($byVal=array())
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			w_bh.mr_work_byhand_id ,
			w_m.sys_timestamp as d_send,
			w_m.mr_round_id,
			w_m.quty,
			w_m.mr_topic as topic,
			w_m.mr_work_remark as work_remark,
			DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send2,
			w_m.mr_work_barcode,
			w_m.mr_work_remark,
			w_bh.mr_cus_name, 
			w_bh.mr_cus_lname, 
            w_bh.mr_cus_tel, 
            w_bh.mr_work_byhand_type_id , 
            w_bh.mr_send_emp_detail, 
            w_bh.mr_address,
            w_bh.mr_post_code,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_tel as send_tel,
			emp_send.mr_emp_name as send_name,
			emp_send.mr_emp_lastname  as send_lastname,
			emp_send.mr_emp_email  as send_email,
			concat(emp_send.mr_emp_code ,"  " , emp_send.mr_emp_name,"  
					" , emp_send.mr_emp_lastname,"  
					",dep.mr_department_name) as name_send,
			r.mr_round_name,
			f.name as f_name,
			s.mr_status_id,
			s.mr_status_name,
			mess_send.mr_user_username as mess,
			type_bh.mr_work_byhand_name,
			sd.mr_sub_districts_name,
			sd.mr_sub_districts_code,
			dt.mr_districts_name,
			dt.mr_districts_code,
			pv.mr_provinces_name,
			pv.mr_provinces_code
		FROM
			mr_work_main w_m
            LEFT join mr_work_byhand w_bh on(w_bh.mr_work_main_id = w_m.mr_work_main_id)
			LEFT join  mr_user mess_send on(mess_send.mr_user_id = w_m.messenger_user_id)
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_bh.mr_send_emp_id)
            LEFT join  mr_work_byhand_type type_bh on(type_bh.mr_work_byhand_type_id = w_bh.mr_work_byhand_type_id)
            LEFT join  mr_department dep on(dep.mr_department_id = emp_send.mr_department_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
            LEFT join  mr_floor f on(f.mr_floor_id = emp_send.mr_floor_id)
			LEFT join  mr_sub_districts sd on(w_bh.mr_sub_district_id = sd.mr_sub_districts_id)
            LEFT join  mr_districts dt on(w_bh.mr_district_id = dt.mr_districts_id)
            LEFT join  mr_provinces pv on(w_bh.mr_province_id = pv.mr_provinces_id)

		WHERE
			w_m.mr_status_id != 6';
			if(!empty($byVal)){
				if(isset($byVal['mr_type_work_id'])){
						$sql .=' and w_m.mr_type_work_id  = ? ';
					array_push($params,intval($byVal['mr_type_work_id']) );
				}
	
				if(isset($byVal['round_printreper'])){
						$sql .=' and w_m.mr_round_id = ? ';
					array_push($params,$byVal['round_printreper'] );
				}
	
				if(isset($byVal['mr_work_barcode'])){
						$sql .=' and w_m.mr_work_barcode = ? ';
					array_push($params,strval($byVal['mr_work_barcode']));
				}
	
				if(isset($byVal['date_report'])){
					$sql .=' and (w_m.mr_work_date_sent BETWEEN  ? and ?
					or w_m.sys_timestamp BETWEEN ? and ?
					)';
					array_push($params, trim($byVal['date_report'])." 00:00:00");
					array_push($params, trim($byVal['date_report'])." 03:59:59");
					array_push($params, trim($byVal['date_report'])." 00:00:00");
					array_push($params, trim($byVal['date_report'])." 03:59:59");
				}
			}

		$sql .='
			group by w_m.mr_work_main_id
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function getreportByval($byVal=array())
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			w_bh.mr_work_byhand_id ,
			w_m.sys_timestamp as d_send,
			w_m.mr_round_id,
			w_m.quty,
			w_m.mr_topic as topic,
			w_m.mr_work_remark as work_remark,
			DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send2,
			w_m.mr_work_barcode,
			w_m.mr_work_remark,
			w_bh.mr_cus_name, 
			w_bh.mr_cus_lname, 
            w_bh.mr_cus_tel, 
            w_bh.mr_work_byhand_type_id , 
            w_bh.mr_send_emp_detail, 
            w_bh.mr_address,
            w_bh.mr_post_code,
			dep.mr_department_name as dep_send,
			dep.mr_department_code as dep_code_send,
			dep.mr_department_code_4 as dep_code_send4,
			cost.mr_cost_code as cost_code_send,
			cost.mr_cost_name as cost_name_send,
			branch_send.mr_branch_name as branch_name_send,
			branch_send.mr_branch_code as branch_code_send,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_tel as send_tel,
			emp_send.mr_emp_name as send_name,
			emp_send.mr_emp_lastname  as send_lastname,
			emp_send.mr_emp_email  as send_email,
			concat(emp_send.mr_emp_code ,"  " , emp_send.mr_emp_name,"  
					" , emp_send.mr_emp_lastname,"  
					",dep.mr_department_name) as name_send,
			r.mr_round_name,
			f.name as f_name,
			s.mr_status_id,
			s.mr_status_name,
			mess_send.mr_user_username as mess,
			type_bh.mr_work_byhand_name,
			sd.mr_sub_districts_name,
			sd.mr_sub_districts_code,
			dt.mr_districts_name,
			dt.mr_districts_code,
			pv.mr_provinces_name,
			pv.mr_provinces_code
		FROM
			mr_work_main w_m
            LEFT join mr_work_byhand w_bh on(w_bh.mr_work_main_id = w_m.mr_work_main_id)
			LEFT join  mr_user mess_send on(mess_send.mr_user_id = w_m.messenger_user_id)
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_bh.mr_send_emp_id)
            LEFT join  mr_branch branch_send on(emp_send.mr_branch_id = branch_send.mr_branch_id)
            LEFT join  mr_work_byhand_type type_bh on(type_bh.mr_work_byhand_type_id = w_bh.mr_work_byhand_type_id)
            LEFT join  mr_department dep on(dep.mr_department_id = emp_send.mr_department_id)
            LEFT join  mr_cost cost on(emp_send.mr_cost_id = cost.mr_cost_id )
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
            LEFT join  mr_floor f on(f.mr_floor_id = emp_send.mr_floor_id)
			LEFT join  mr_sub_districts sd on(w_bh.mr_sub_district_id = sd.mr_sub_districts_id)
            LEFT join  mr_districts dt on(w_bh.mr_district_id = dt.mr_districts_id)
            LEFT join  mr_provinces pv on(w_bh.mr_province_id = pv.mr_provinces_id)

		WHERE
			w_m.mr_status_id != 6';
			if(!empty($byVal)){
				if(isset($byVal['mr_type_work_id'])){
						$sql .=' and w_m.mr_type_work_id  = ? ';
					array_push($params,intval($byVal['mr_type_work_id']) );
				}
	
				if(isset($byVal['round_printreper'])){
						$sql .=' and w_m.mr_round_id = ? ';
					array_push($params,$byVal['round_printreper'] );
				}
	
				if(isset($byVal['mr_work_barcode'])){
						$sql .=' and w_m.mr_work_barcode = ? ';
					array_push($params,strval($byVal['mr_work_barcode']));
				}
	
				if(isset($byVal['date_report1'])){
					$sql .=' and ( w_m.sys_timestamp BETWEEN ? and ? )
					and w_m.sys_timestamp >= "2023-01-01 00:00:00"
					';
					array_push($params, trim($byVal['date_report1'])." 00:00:00");
					array_push($params, trim($byVal['date_report2'])." 23:59:59");
				}
			}

		$sql .='
			group by w_m.mr_work_main_id
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		//echo $sql;
        //exit;
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function updateworkByhandByMainId($data, $main_id)
	{
		$this->_db->beginTransaction();
		try {
            $id = $this->_db->update($this->_tableName, $data, 'mr_work_main_id = ' . $main_id);
            $this->_db->commit();
            return $id;
        } catch(Exception $e) {
            $this->_db->rollBack();
            return $e->getMessage();
        }
	}
	
}
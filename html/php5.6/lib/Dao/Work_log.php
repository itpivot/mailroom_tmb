<?php
require_once 'Pivot/Dao.php';


class Dao_Work_log extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_log');
    }
	
	function SLAlog( $id )
	{
		$params = array();
		$sql =
		'
			SELECT 
				l.*
			FROM mr_work_log l
			WHERE
				l.mr_work_main_id = ?
				and l.mr_status_id in(2,5)
		';
		
		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function getWorkLogById($id)
	{
		$params = array();
		$sql =
		'
			select 
				l.mr_work_log_id,
				l.mr_status_id,
				l.sys_timestamp,
				e.mr_emp_id,
				e.mr_emp_name,
				e.mr_emp_lastname,
				e.mr_emp_code
			from '.$this->_tableName.' as l
				left join mr_user u ON ( u.mr_user_id = l.mr_user_id )
				left join mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			where
				l.mr_work_main_id = ?
			order by l.mr_work_log_id asc
		';

		array_push($params, (int)$id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function getWorkLogstarff($id)
	{
		$params = array();
		$sql =
		'
			select 
				l.mr_work_log_id,
				l.mr_status_id,
				l.sys_timestamp,
				e.mr_emp_id,
				e.mr_emp_name,
				e.mr_emp_lastname,
				e.mr_emp_code
			from '.$this->_tableName.' as l
				left join mr_user u ON ( u.mr_user_id = l.mr_user_id )
				left join mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
			where
				l.mr_work_main_id = ?
				and (u.mr_user_role_id NOT IN (2,5) or u.mr_user_id = 984)
			order by l.mr_work_log_id asc
		';

		array_push($params, (int)$id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	
	
	
	
	
	
}
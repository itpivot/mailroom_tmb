<?php
require_once 'Pivot/Dao.php';


class Dao_Work_post extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_work_post');
    }

	function getdatatoday_post_in($date,$round=null)
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send,
			w_m.mr_work_barcode,
			w_m.mr_work_remark,
			w_p.mr_address,
			w_p.mr_cus_tel,
			w_p.sp_num_doc,
			w_p.num_doc,
			w_p.mr_cus_name as sendder_name,
			w_p.mr_cus_lname as sendder_lname,
			concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_send,
			w_p.mr_address as sendder_address,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_name,
			emp_send.mr_emp_lastname,
			concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_resive,
			r.mr_round_name,
			s.mr_status_id,
			s.mr_status_name,
			t_p.mr_type_post_name
		FROM
			mr_work_main w_m
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_work_inout w_io on(w_io.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
            LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_io.mr_emp_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
		WHERE
			w_m.mr_type_work_id = 5
			and w_m.mr_status_id != 6';
		if(!empty($date)){
			
				$sql .=' and (w_m.mr_work_date_sent like ? 
				or w_m.sys_timestamp LIKE ?
				)';
				array_push($params, trim($date)."%");
				array_push($params, trim($date)."%");
		}
		if(!empty($round)){
			$sql .=' and w_m.mr_round_id = ? ';
			array_push($params,$round );
		}
		$sql .=' group by w_m.mr_work_main_id';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();

	}

	function getdatatoday()
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send,
			w_m.mr_work_barcode,
			w_p.sp_num_doc,
			concat(w_m.mr_work_barcode,",",w_p.sp_num_doc) as mr_work_barcode_and_re,
			w_m.mr_work_remark,
			w_p.mr_address,
			w_p.mr_cus_tel,
			w_p.mr_cus_name as sendder_name,
			w_p.mr_cus_lname as sendder_lname,
			concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_resive,
			w_p.mr_address as sendder_address,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_name,
			emp_send.mr_emp_lastname,
			concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_send,
			r.mr_round_name,
			s.mr_status_id,
			s.mr_status_name,
			t_p.mr_type_post_name
		FROM
			mr_work_main w_m
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
            LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_p.mr_send_emp_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
		WHERE
			w_m.sys_timestamp LIKE DATE_FORMAT(CURDATE(), "%Y-%m-%d %")
			and w_m.mr_type_work_id = 6
			and w_m.mr_status_id != 6
		group by w_m.mr_work_main_id
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	function getdataPostinReport($data_search = null){
		$params = array();
		$sql ='
		SELECT
				w_m.mr_work_main_id,
				DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send,
				w_m.mr_work_barcode,
				w_m.mr_work_remark,
				w_p.mr_address,
				w_p.num_doc,
				w_p.mr_post_price,
				w_p.mr_post_totalprice,
				w_m.quty,
				pv.mr_provinces_name,
				w_p.mr_send_emp_detail,
				w_io.mr_floor_id,
				f.name as floor_name,
				w_p.mr_cus_tel,
				w_p.mr_send_emp_id,
				emp_re.mr_emp_id as mr_resive_emp_id,
				w_p.mr_cus_name as sendder_name,
				w_p.mr_cus_lname as sendder_lname,
				concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_send,
				w_p.mr_address as sendder_address,
				w_p.mr_send_department_id as mr_resive_department_id,
				dep.mr_department_name as dep_resive,
				dep.mr_department_code as dep_code_resive,
				emp_re.mr_emp_code ,
				emp_re.mr_emp_name,
				emp_re.mr_emp_lastname,
				concat(emp_re.mr_emp_code ," : " , emp_re.mr_emp_name,"  " , emp_re.mr_emp_lastname,"  ",dep.mr_department_name) as name_resive,
				r.mr_round_name,
				s.mr_status_id,
				s.mr_status_name,
				w_p.mr_type_post_id,
				t_p.mr_type_post_name,
				cost.mr_cost_name,
				cost.mr_cost_code,
				b.mr_branch_code,
				b.mr_branch_name
			FROM
				mr_work_main w_m
				LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
				LEFT join  mr_work_inout w_io on(w_io.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_floor f on(f.mr_floor_id = w_io.mr_floor_id)
				LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
				LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
				LEFT join  mr_emp emp_re on(emp_re.mr_emp_id = w_io.mr_emp_id)
				LEFT join  mr_branch b on(emp_re.mr_branch_id = b.mr_branch_id)
				LEFT join  mr_cost cost on(cost.mr_cost_id = emp_re.mr_cost_id)
				LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
				LEFT join  mr_provinces pv on(pv.mr_provinces_id = w_p.mr_province_id)
			WHERE 
			    w_m.mr_type_work_id = 5
				and w_m.mr_status_id != 6
			';

			if($data_search['start_date'] != ''){
				$sql.=" and	w_m.mr_work_date_sent >= ?";
				$sql.=" and	w_m.mr_work_date_sent <= ?";
				array_push($params, $data_search['start_date']." 00:00:00");
				array_push($params, $data_search['end_date']." 23:59:59");
			}
			
			$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
			// Execute Query
			//  echo $sql;
			// exit;
		   
			$stmt->execute($params);
		
			return $stmt->fetchAll();
	}

	function getdataByval($byVal = null)
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			DATE_FORMAT(w_m.mr_work_date_sent, "%Y-%m-%d") as d_send,
			w_m.mr_work_barcode,
			w_p.sp_num_doc,
			w_p.num_doc,
			co.mr_cost_name,
			co.mr_cost_code,
			concat(w_p.sp_num_doc,",",w_p.sp_num_doc) as mr_work_barcode_and_re,
			w_m.mr_work_remark,
			w_p.mr_address,
			w_p.mr_cus_tel,
			w_p.mr_cus_name as name_resive,
			w_p.mr_cus_lname as lname_resive,
			concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_resive,
			w_p.mr_address as sendder_address,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_name,
			emp_send.mr_emp_lastname,
			concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_send,
			r.mr_round_name,
			s.mr_status_id,
			s.mr_status_name,
			t_p.mr_type_post_name
		FROM
			mr_work_main w_m
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_cost co on(w_p.mr_cost_id = co.mr_cost_id)
            LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
            LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_p.mr_send_emp_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
		WHERE
			w_m.mr_type_work_id = 6
			and w_m.mr_status_id != 6';
		if(!empty($byVal)){
			if(!empty($byVal['round_printreper'])){
					$sql .=' and w_m.mr_round_id = ? ';
				array_push($params,$byVal['round_printreper'] );
			}

			if(isset($byVal['date_report'])){
				$sql .=' and (w_m.mr_work_date_sent BETWEEN  ? and ?
				or w_m.sys_timestamp BETWEEN ? and ?
				)';
				array_push($params, trim($byVal['date_report'])." 00:00:00");
				array_push($params, trim($byVal['date_report'])." 03:59:59");
				array_push($params, trim($byVal['date_report'])." 00:00:00");
				array_push($params, trim($byVal['date_report'])." 03:59:59");
			}
		}
		$sql .='	group by w_m.mr_work_main_id';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		//  echo $sql;
        // exit;
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}


	function getDataWithTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			SELECT 
				wp.*,
				sd.zipcode, 
				sd.mr_sub_districts_id,
				sd.mr_sub_districts_code,
                sd.mr_sub_districts_name,
				d.mr_districts_id,
				d.mr_districts_code,
                d.mr_districts_name,
                p.mr_provinces_id,
                p.mr_provinces_code,
                p.mr_provinces_name
			FROM mr_work_post wp
			LEFT JOIN mr_sub_districts as sd on(sd.mr_sub_districts_id = wp.mr_sub_district_id)
			LEFT JOIN mr_districts AS d ON (d.mr_districts_id = wp.mr_district_id)
            LEFT JOIN mr_provinces AS p ON (p.mr_provinces_id = wp.mr_province_id)

			where
					( 
						wp.mr_cus_name like ?
						or wp. mr_cus_lname like ?
					 ) 
					 group by wp.mr_cus_name,wp.mr_cus_lname
					 ORDER BY wp.mr_work_post_id
		';

		array_push($params, "%".strval($txt)."%");
		array_push($params, "%".strval($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}


	function ExportDataReportPrice($data_search)
	{
		$params = array();

		$sql='SELECT
				w_m.mr_work_main_id,
				w_m.sys_timestamp,
				DATE_FORMAT(w_m.mr_work_date_sent, "%Y-%m-%d") as d_send,
				w_m.mr_work_barcode,
				w_m.mr_work_remark,
				w_p.mr_address,
				w_p.mr_post_price,
				w_p.mr_post_totalprice,
				w_p.num_doc,
				w_p.sp_num_doc,
				w_m.quty,
				w_p.mr_post_weight,
				pv.mr_provinces_name,
				w_p.mr_send_emp_detail,
				w_p.mr_cus_tel,
				w_p.mr_send_emp_id,
				w_p.mr_cus_name as sresive_name,
				w_p.mr_cus_lname as resive_lname,
				concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_resive,
				w_p.mr_address as sendder_address,
				dep.mr_department_name as dep_send,
				dep.mr_department_code as dep_code_send,
				dep.mr_department_code_4 as dep_code_4,
				emp_send.mr_emp_code ,
				emp_send.mr_emp_name,
				emp_send.mr_emp_lastname,
				concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_send,
				r.mr_round_name,
				s.mr_status_id,
				s.mr_status_name,
				w_p.mr_type_post_id,
				t_p.mr_type_post_name,
				cost1.mr_cost_code as mr_cost_code1,
				cost2.mr_cost_code as mr_cost_code2
			FROM
				mr_work_main w_m
				LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
				LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
				LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
				LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_p.mr_send_emp_id)
				LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
				LEFT join  mr_provinces pv on(pv.mr_provinces_id = w_p.mr_province_id)
				LEFT join  mr_cost cost1 on(w_p.mr_cost_id = cost1.mr_cost_id)
				LEFT join  mr_cost cost2 on(emp_send.mr_cost_id = cost2.mr_cost_id)
			WHERE 
			    w_m.mr_type_work_id = 6
				and w_m.mr_status_id != 6
			';

			if($data_search['start_date'] != ''){
				$sql.=" and	w_m.mr_work_date_sent >= ?";
				$sql.=" and	w_m.mr_work_date_sent <= ?";
				array_push($params, $data_search['start_date']." 00:00:00");
				array_push($params, $data_search['end_date']." 23:59:59");
			}
					
			//echo $sql;
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getdataByuser($byVal = null,$user_id=null)
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			DATE_FORMAT(w_m.mr_work_date_sent, "%Y-%m-%d") as d_send,
			w_m.mr_work_barcode,
			w_p.sp_num_doc,
			w_p.num_doc,
			concat(w_m.mr_work_barcode,",",w_p.sp_num_doc) as mr_work_barcode_and_re,
			w_m.mr_work_remark,
			w_p.mr_address,
			w_p.mr_cus_tel,
			w_p.mr_cus_name as name_resive,
			w_p.mr_cus_lname as lname_resive,
			concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_resive,
			w_p.mr_address as sendder_address,
			dep.mr_department_name as dep_resive,
			dep.mr_department_code as dep_code_resive,
			emp_send.mr_emp_code ,
			emp_send.mr_emp_tel as send_tel,
			emp_send.mr_emp_name as send_name,
			emp_send.mr_emp_lastname  as send_lastname,
			emp_send.mr_emp_email  as send_email,
			concat(emp_send.mr_emp_code ,"  " , emp_send.mr_emp_name,"  
					" , emp_send.mr_emp_lastname,"  
					",dep.mr_department_name) as name_send,
			r.mr_round_name,
			s.mr_status_id,
			s.mr_status_name,
			t_p.mr_type_post_name
		FROM
			mr_work_main w_m
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
            LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_p.mr_send_emp_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
			
		WHERE
			w_m.mr_type_work_id = 6 ';

		if(!empty($user_id)){
				$sql .=' 
				and w_m.mr_user_id = ? ';
			array_push($params, $user_id);
		}
		if(!empty($byVal)){
			if(!empty($byVal['round_printreper'])){
					$sql .=' and w_m.mr_round_id = ? ';
				array_push($params,$byVal['round_printreper'] );
			}
			
			if(!empty($byVal['round_printreper'])){
					$sql .=' and w_m.mr_round_id = ? ';
				array_push($params,$byVal['round_printreper'] );
			}

			if(isset($byVal['date_report'])){
				$sql .=' and (w_m.mr_work_date_sent BETWEEN  ? and ?
				or w_m.sys_timestamp BETWEEN ? and ?
				)';
				array_push($params, $byVal['date_report']." 00:00:00");
				array_push($params, $byVal['date_report']." 23:59:59");
				array_push($params, $byVal['date_report']." 00:00:00");
				array_push($params, $byVal['date_report']." 23:59:59");
			}
		}
		$sql .='	group by w_m.mr_work_main_id
					order by w_m.mr_work_main_id desc		
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		//  echo $sql;
        // exit;
        // echo print_r($params);
		// exit;
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	
	function getdataPostOutByval($byVal = null)
	{
		$params = array();
		$sql =
		'
		SELECT
			w_m.mr_work_main_id,
			DATE_FORMAT(w_m.mr_work_date_sent, "%Y-%m-%d") as d_send,
			w_m.mr_work_barcode,
			w_p.sp_num_doc,
			concat(w_m.mr_work_barcode,",",w_p.sp_num_doc) as mr_work_barcode_and_re,
			w_m.mr_work_remark,
			w_m.mr_topic,
			w_p.mr_address,
			w_p.mr_cus_tel,
			w_p.mr_cus_name as name_resive,
			w_p.mr_cus_lname as lname_resive,
			w_p.mr_send_emp_id,
			w_p.mr_post_code,
			w_p.mr_work_post_id,
			w_p.num_doc,
			w_p.mr_send_department_id,
			w_p.mr_sub_district_id,
			w_p.mr_district_id,
			w_p.mr_province_id,
			w_p.mr_type_post_id,
			w_p.mr_post_weight,
			w_p.mr_post_price,
			w_p.mr_post_totalprice,
			w_p.mr_post_amount,
			w_p.mr_send_emp_detail,
			concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_resive,
			concat(dep.mr_department_code,": ",dep.mr_department_name) as full_name_dep,
			w_p.mr_address as sendder_address,
			dep.mr_department_name as dep_send_name,
			dep.mr_department_code as dep_send_code,
			emp_send.mr_emp_code ,
			emp_send.mr_cost_id ,
			emp_send.mr_emp_name as name_send,
			emp_send.mr_emp_lastname as mr_emp_lastname,
			emp_send.mr_emp_tel as mr_emp_tel,
			emp_send.mr_emp_email as mr_emp_email,
			concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name ,"  " , emp_send.mr_emp_lastname ,"  ",dep.mr_department_name) as name_send,
			concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name ,"  " , emp_send.mr_emp_lastname) as full_name_send,
			r.mr_round_name,
			s.mr_status_id,
			s.mr_status_name,
			t_p.mr_type_post_name,
			sd.mr_sub_districts_name,
			sd.mr_sub_districts_code,
			dt.mr_districts_name,
			dt.mr_districts_code,
			pv.mr_provinces_name,
			w_m.mr_round_id,
			pv.mr_provinces_code
		FROM
			mr_work_main w_m
            LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
            LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
            LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
            LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
            LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_p.mr_send_emp_id)
            LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
            LEFT join  mr_sub_districts sd on(w_p.mr_sub_district_id = sd.mr_sub_districts_id)
            LEFT join  mr_districts dt on(w_p.mr_district_id = dt.mr_districts_id)
            LEFT join  mr_provinces pv on(w_p.mr_province_id = pv.mr_provinces_id)
		WHERE
			 w_m.mr_status_id != 6';
		if(!empty($byVal)){
			if(isset($byVal['mr_type_work_id'])){
					$sql .=' and w_m.mr_type_work_id  = ? ';
				array_push($params,intval($byVal['mr_type_work_id']) );
			}

			if(isset($byVal['round_printreper'])){
					$sql .=' and w_m.mr_round_id = ? ';
				array_push($params,$byVal['round_printreper'] );
			}

			if(isset($byVal['mr_work_barcode'])){
					$sql .=' and w_m.mr_work_barcode = ? ';
				array_push($params,strval($byVal['mr_work_barcode']));
			}

			if(isset($byVal['date_report'])){
				$sql .=' and (w_m.mr_work_date_sent BETWEEN  ? and ?
				or w_m.sys_timestamp BETWEEN ? and ?
				)';
				array_push($params, trim($byVal['date_report'])." 00:00:00");
				array_push($params, trim($byVal['date_report'])." 03:59:59");
				array_push($params, trim($byVal['date_report'])." 00:00:00");
				array_push($params, trim($byVal['date_report'])." 03:59:59");
			}
		}
		$sql .='	group by w_m.mr_work_main_id';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		//  echo $sql;
        // exit;
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
}
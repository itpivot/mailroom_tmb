<?php
require_once 'Zend/Auth/Adapter/DbTable.php';
require_once 'Zend/Db.php';
require_once 'Zend/Session/Namespace.php';

class Pivot_Auth
{
    const SESSION_NAME      = 'sessionAuth';
    
    protected $_authAdapter = null;
    protected $_user        = null;

    private static $failCount = 0;
    private $counter = 0;


    
    

    function __construct()
    {
        global $_CONFIG;
        $db  = Zend_Db::factory($_CONFIG->database);
        $this->_authAdapter = new Zend_Auth_Adapter_DbTable($db, 'mr_user', 'mr_user_username', 'mr_user_password');
        $this->_encrypt_method = "AES-256-CBC";
        $this->_secret_key = 'pivotsecretkey';
        $this->_secret_iv = 'pivotsecretiv';

        $this->_key = hash('sha256', $this->_secret_key);
        $this->_iv = substr(hash('sha256', $this->_secret_iv), 0, 16);
        
        /* try to restore identity from session */    
        $session = $this->getSession();        
        if (!is_null($session) && isset($session->user)) {
            $this->_user  = $session->user;

        }

        $this->_db = $db;

        if($this->isAuth()) {

            if((time() - intval(strtotime($this->_user->sys_timestamp))) > 900) { // 900 sec = 15 minutes 
                // ถ้าค่ามากกว่า 900 sec แสดงว่าไม่มีการใช้งาน เกิน 15 นาที
                $userID = $this->getUser();
                
				
				if(isset($userID)){
					$datas = array();
					$datas['isLogin'] = 1;
					//$datas['is_first_login'] = 1;
					$datas['sys_timestamp'] = date("Y-m-d H:i:s");
					
					$this->_db->update('mr_user', $datas, ' mr_user_id = ' . intval($userID));
					header('Location: ../user/logout.php');
				}
            } else {
                 //echo time() - intval(strtotime($this->_user->sys_timestamp)); // แสดงผลลัพธ์ ระหว่าง เวลาปัจจุบัน - sys_timstamp  
                 $this->updateActivity(); // ถ้ามีการใช้งาน ให้ update sys_timestamp ทุกหน้าใน DB
                 $this->_user->sys_timestamp = date('Y-m-d H:i:s'); //  update sys_timestamp ให้ตัวแปล ใน class auth
            }
        }
		if($this->getUser()!=''){
			$tocent = $this->_db->fetchOne("SELECT tocent FROM mr_user where mr_user_id ='".$this->getUser()."'");
			$wewrole_id = $this->_db->fetchOne("SELECT mr_user_role_id FROM mr_user where mr_user_id ='".$this->getUser()."'");
			if($this->_user->mr_user_role_id != $wewrole_id){
				header('Location: ../user/logout.php');
				
			}if($tocent != $this->_user->tocent){
				$this->_user  = "";
				Zend_Session::namespaceUnset(self::SESSION_NAME);
				Zend_Session::destroy();
				header('Location: ../user/login.php');
			}
			//echo $this->_user->tocent;
		}
    }
    


    public function getSession()
    {
        if (!Zend_Session::sessionExists()) {
            return null;
        }
        return new Zend_Session_Namespace(self::SESSION_NAME);
    }

    public function hasAccess($access_roles = array())
    {
        return in_array($this->_user->mr_user_role_id, $access_roles);
    }

    public function isAuth()
    {
        return isset($this->_user);
    }
    
    public function getUser()
    {
        if($this->_user) {
            return $this->_user->mr_user_id;
        }
    }
    
    public function getActive() 
    {
        return $this->_user->active;
    }

    public function getUserName()
    {
        if($this->_user){
            return $this->_user->mr_user_username;
        }
    }

    public function getLoginStatus()
    {
        return $this->_user->isLogin;
    }


    public function getFirstLogin() 
    {
        if($this->_user) {
            return $this->_user->is_first_login;
        }
    }
    
    public function getRole()
    {
        if($this->_user) {
            return $this->_user->mr_user_role_id;
        }
    }

    public function authenticate($username, $password, $encrypted = false)
    {
        // if (!$encrypted) {
        //     $password = $this->encryptAES((string)$password);
        //     // $this->_authAdapter->setCredentialTreatment('md5(?)');
        //     // $this->_authAdapter->setCredentialTreatment('RGdEOFdHVURLcUJxckIrVnJBaDRtQT09');
        //     // $this->_authAdapter->setCredentialTreatment("base64_encode(openssl_encrypt(?, $this->_encrypt_method, $this->_key, 0, $this->_iv))");
        //     // $this->_authAdapter->setCredentialTreatment("12345");
        //     // echo $this->encryptAES((string)$password);
        // }
        $password = $this->encryptAES((string)$password);
        $this->_authAdapter->setIdentity($username);
        $this->_authAdapter->setCredential($password);
        
        // echo "<pre>".print_r($this->_authAdapter, true)."</pre>";
        
        $result = $this->_authAdapter->authenticate();

        

        if ($result->isValid()) {
            $user = $this->_authAdapter->getResultRowObject();
            $this->_user = $user;
			
			$this->updateActivity();
        }

        return $result;
    }
	
	function updateActivity()
    {
		$date_today = date("Y-m-d");
		$time_now	= date("H:i:s");
		$sys_timestamp	=	$date_today.' '.$time_now;
        $datas['sys_timestamp'] =	$sys_timestamp;
		$userID = $this->getUser();
		
		$this->_db->update('mr_user', $datas, ' mr_user_id = '. $userID);

    }
    
    public function saveSession()
    {
        // change in properties 
        $user = $this->_authAdapter->getResultRowObject();
        $user->isLogin = 0; // on production is 1
        //$user->is_first_login = 1;
        $user->sys_timestamp = date('Y-m-d H:i:s');
        $this->_user  = $user;
        $this->updateActivity();
        // change in DB
        $userID = intval($this->getUser());
		$ch_token = md5(uniqid(rand(), true));
        $datas['isLogin'] 	=	1; // on production is 1
        $datas['tocent'] 	=	$ch_token; // on production is 1
		$this->_user->tocent = $ch_token ;
        //$datas['is_first_login'] =	1;
        $datas['sys_timestamp'] = date("Y-m-d H:i:s");
        $this->_db->update('mr_user', $datas, ' mr_user_id = '. $userID);
        // save session 
        $session = new Zend_Session_Namespace(self::SESSION_NAME);
        $session->user  = $this->_user;
        return $this->_user;
    }

    public function remove()
    {
        $users = $this->_user;
        if(!empty($users)) {
            //change in DB
            $userID = intval($this->getUser());
            $datas['isLogin'] =	0;
            $datas['sys_timestamp'] = date("Y-m-d H:i:s");
            $this->_db->update('mr_user', $datas, ' mr_user_id = '. $userID);
            // change in properties 
            $ip = $_SERVER['REMOTE_ADDR'];
            $save_log['sys_timestamp'] = date('Y-m-d H:i:s');
            $save_log['activity'] = "Logout";
            $save_log['terminal'] = $ip;
            $save_log['status'] = "Success";
            $save_log['mr_user_id'] = $userID;
            $save_log['description'] = "Logout Success";
			if($userID!= ''){
				$this->_db->insert('mr_access_status_log', $save_log);
			}
            $this->_user  = "";
        }       
        // remove session 
        Zend_Session::namespaceUnset(self::SESSION_NAME);
        Zend_Session::destroy();
    }   


    public function encryptAES($password)
    {
        $password = openssl_encrypt($password, $this->_encrypt_method, $this->_key, 0, $this->_iv);
        $password = base64_encode($password);
        return $password;
    }


}
?>
<?php


/**
 * Request handler
 */
class Pivot_Request
{
    protected $_values = null;
    protected $_alternativeValues = null;

    function __construct()
    {
        if (count($_POST) > 0) {
            $this->_values = $_POST;
            $this->_alternativeValues = $_GET;
        } else {
            $this->_values = $_GET;
            $this->_alternativeValues = $_POST;
        }
    }

    /**
     * Check box is checked or not
     * @param  string    $varName    Variable name
     * @return  boolean  value
     */
    public function isChecked($varName)
    {
        return (isset($this->_values[$varName]));
    }

    /**
     * Get value
     * @param  string   $varName    Variable name
     * @param  mixed    $value      Value if url parameter is not set, default is empty string
     * @param  boolean  $trim       Trim space or not
     * @return  string  value
     */
    public function get($varName, $value = '', $trim = true)
    {
        // Get value from $_GET or $_POST, if not exists
        if (isset($this->_values[$varName])) {
            $rawValue = $this->_values[$varName];
        } elseif (isset($this->_alternativeValues[$varName])) {
            $rawValue = $this->_alternativeValues[$varName];
        }

        if (isset($rawValue)) {
            if (is_array($rawValue)) {
                if ($trim) {
                    $values = array();
                    foreach ($rawValue as $v) {
                        $v = trim($v);
                        array_push($values, $v);
                    }
                    return $values;
                }

                return $rawValue;
            }

            $value = ($trim) ? trim($rawValue) : $rawValue;
        }

        return $value;
    }
}

<?php

/*
 * This file is part of Twig.
 *
 * (c) 2009 Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Exception thrown when a security error occurs at runtime.
 *
 * @package    twig
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: SecurityError.php,v 1.3 2017/11/07 03:10:47 worapong Exp $
 */
class Twig_Sandbox_SecurityError extends Twig_Error
{
}

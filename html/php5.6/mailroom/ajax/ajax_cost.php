
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'nocsrf.php';
require_once 'Dao/Round.php';
require_once 'Dao/Zone.php';
require_once 'Dao/Building.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Cost.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
} else {

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();
$roundDao			= new Dao_Round();
$zoneDao			= new Dao_zone();
$buildingDao		= new Dao_Building();
$FloorDao			= new Dao_Floor();
$costDao			= new Dao_Cost();

$user_id			= $auth->getUser();






$page  				=  $req ->get('page');//save
$add_cost_code  	=  $req ->get('add_cost_code');
$add_cost_name  	=  $req ->get('add_cost_name');

if($page  == 'add'){
	if(!empty($add_cost_code)){
		$c_data = $costDao->checkCostByCostCode($add_cost_code);
	}else{
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'กรุณาระบุ รหัสค่าใช้จ่าย' 
				)
			);
		exit;
	}

	if(empty($c_data)){
		$cost_data = array();
		$cost_data['mr_cost_code'] = $add_cost_code;
		$cost_data['mr_cost_name'] = $add_cost_name;
		$c_data = $costDao->save($cost_data);
		echo json_encode(
			array(
				'status' => 200, 
				'data' => $c_data, 
				'message' => 'สำเร็จ' 
				)
			);

	}else{
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'รหัสค่าใช้จ่ายนี้มีในระบบแล้ว' 
				)
			);
		exit;
	}
}else{
	echo json_encode(
		array(
			'status' => 500, 
			'data' => '', 
			'message' => 'เกิดข้อผิดพลาด' 
			)
		);
}
}

 ?>
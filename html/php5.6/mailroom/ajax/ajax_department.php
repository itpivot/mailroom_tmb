
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'nocsrf.php';
require_once 'Dao/Round.php';
require_once 'Dao/Zone.php';
require_once 'Dao/Building.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Department.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
} else {

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();
$roundDao			= new Dao_Round();
$zoneDao			= new Dao_zone();
$buildingDao		= new Dao_Building();
$FloorDao			= new Dao_Floor();
$departmentDao		= new Dao_Department();


$user_id			= $auth->getUser();






$page  			=  $req ->get('page');//save
$mr_user_id  	=  $req ->get('mr_user_id');
$mr_floor_id  	=  $req ->get('mr_floor_id');
$re 			= array();
if($page == "save_add_department"){	

	$department_code 		=  $req ->get('department_code');
	$department_name 		=  $req ->get('department_name');
	$mr_department_id 		=  $req ->get('mr_department_id');

	if($department_code==""){
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'กรุณาระบุ รหัสหน่วยงาน !' 
				)
			);
		exit;
	}
	if($department_name==""){
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'กรุณาระบุ ชื่อหน่วยงาน !' 
				)
			);
		exit;
	}
	$ischeck = $departmentDao->CheckDepartmentDuplicate($mr_department_id,$department_code);
	if(empty($ischeck)){
		$insert_department 	= array();		
		$insert_department['mr_department_code'] = $department_code;
		$insert_department['mr_department_name'] = $department_name;
		
		$departmentDao_id = $departmentDao->save($insert_department,$mr_department_id);
	}else{
		echo json_encode(
			array(
				'status' => 500, 
				'data' => $mr_department_id, 
				'message' => 'รหัสหน่วยงาน นี้มีในระบบแล้ว' 
				)
			);
		exit;	
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $mr_department_id, 
			'message' => 'สำเร็จ' 
			)
		);

}elseif($page == "loadDepartment"){
	$filter['department_code']  		=  $req ->get('department_code');
	$filter['department_name']  		=  $req ->get('department_name');
	
	$data	= $departmentDao->getdepartmentsaech($filter);	
	foreach($data as $key=>$val){
		$data[$key]['no'] = $key+1;
		$data[$key]['action'] ='<button type="button" class="btn btn-warning" onclick="Edit_department('.$val['mr_department_id'].',
		\''.$val['mr_department_code'].'\',
		\''.$val['mr_department_name'].'\'
		)" title="แก้ไขข้อมูล"><i class="material-icons">edit</i></button>';
	
	}


	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);

}elseif($page == "getBuilding"){
	$mr_branch_id  	=  $req ->get('mr_branch_id');
	$data =array();
	$data = $buildingDao->getBuildingByBranchId($mr_branch_id);

	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'บันทึกสำเร็จ' 
			)
		);
exit;

}else if($page == "save"){
		$datasave['mr_user_id'] 	= $mr_user_id;
		$datasave['mr_floor_id'] 	= $mr_floor_id;
		
		if($mr_floor_id==""){
			echo json_encode(
				array(
					'status' => 401, 
					'data' => '', 
					'message' => 'กรุณาระบุ ชั้น' 
					)
				);
			exit;
		}
		
		if($mr_user_id==""){
			echo json_encode(
				array(
					'status' => 401, 
					'data' => '', 
					'message' => 'กรุณาระบุ พนักงาน' 
					)
				);
			exit;
		}
		
		$re = $zoneDao->save($datasave);
		
		$data	= $zoneDao->getZone_UserAll();		
		foreach($data as $key=>$val){
			$data[$key]['no'] = $key+1;
			$data[$key]['action'] = $key+1;
			$data[$key]['action'] ='<button type="button" class="btn btn-danger" onclick="remove_zone('.$val['mr_zone_id'].')">ลบ</button>';
		}
		echo json_encode(
				array(
					'status' => 200, 
					'data' => $data, 
					'message' => 'บันทึกสำเร็จ' 
					)
				);
		exit;
}else if($page == "remove"){
	$mr_zone_id  	=  $req ->get('id');
	$re = $zoneDao->remove($mr_zone_id);
	$data	= $zoneDao->getZone_UserAll();		
	foreach($data as $key=>$val){
		$data[$key]['no'] = $key+1;
		$data[$key]['action'] = $key+1;
		$data[$key]['action'] ='<button type="button" class="btn btn-danger" onclick="remove_zone('.$val['mr_zone_id'].')">ลบ</button>';
	}
	echo json_encode(
			array(
				'status' => 200, 
				'data' => $data, 
				'message' => 'ลบข้อมูลเรียบร้อย' 
				)
			);
	exit;
}else{
	$data	= $zoneDao->getZone_UserAll();	
	foreach($data as $key=>$val){
		$data[$key]['no'] = $key+1;
		$data[$key]['action'] ='<button type="button" class="btn btn-danger" onclick="remove_zone('.$val['mr_zone_id'].')">ลบ</button>';
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);
	}
}



 ?>
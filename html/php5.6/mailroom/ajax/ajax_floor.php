
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'nocsrf.php';
require_once 'Dao/Round.php';
require_once 'Dao/Zone.php';
require_once 'Dao/Building.php';
require_once 'Dao/Floor.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
} else {

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();
$roundDao			= new Dao_Round();
$zoneDao			= new Dao_zone();
$buildingDao		= new Dao_Building();
$FloorDao			= new Dao_Floor();

$user_id			= $auth->getUser();






$page  			=  $req ->get('page');//save
$mr_user_id  	=  $req ->get('mr_user_id');
$mr_floor_id  	=  $req ->get('mr_floor_id');
$re 			= array();
if($page == "save_add_floor"){	
	$floor_name  		=  $req ->get('floor_name');
	$floor_level  		=  $req ->get('floor_level');
	$mr_branch_id  		=  $req ->get('mr_branch_id');
	$mr_building_id  	=  $req ->get('mr_building_id');
	$mr_branch_name  	=  $req ->get('mr_branch_name');

	if($floor_name==""){
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'กรุณาระบุ ชื่อชั้น' 
				)
			);
		exit;
	}
	if($floor_level==""){
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'กรุณาระบุ ระดับชั้น' 
				)
			);
		exit;
	}
	if($mr_branch_id==""){
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'กรุณาระบุ สาขา' 
				)
			);
		exit;
	}
	if($mr_building_id==""){
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'กรุณาระบุ ตึก/อาคาร' 
				)
			);
		exit;
	}
	if($mr_building_id==0 and $mr_branch_name == ""){
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'เกิดข้อผิดพลาด ไม่พบข้อมูลชื่อสาขา กรุณาออกจากระบบแล้วเข้าใหม่' 
				)
			);
		exit;
	}
	$ischeck = $FloorDao->CheckFloorsDuplicate($floor_name,$mr_building_id);
	if(empty($ischeck)){
		$insert_floor 	= array();
		$insert_bd 		= array();
		if($mr_building_id==0){
			$insert_bd['mr_building_name'] = $mr_branch_name;
			$insert_bd['mr_branch_id'] = $mr_branch_id;
			$ch_building = $buildingDao->getBuildingByBranchId($mr_branch_id);
			if(empty($ch_building)){
				$mr_building_id = $buildingDao->save($insert_bd);
			}else{
				echo json_encode(
					array(
						'status' => 500, 
						'data' => '', 
						'message' => 'เกิดข้อผิดพลาด กรุณาระบุ ตึก/อาคาร' 
						)
					);
				exit;
			}
		}

		$insert_floor['mr_building_id'] = $mr_building_id;
		$insert_floor['name'] 			= $floor_name;
		$insert_floor['floor_level'] 	= $floor_level;
		$insert_floor['floor_builder'] 	= $mr_branch_name;
		$mr_floor_id = $FloorDao->save($insert_floor);
		
	}else{
		echo json_encode(
			array(
				'status' => 500, 
				'data' => '', 
				'message' => 'ชั้นนี้มีไนระบบแล้ว' 
				)
			);
		exit;	
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $ischeck, 
			'message' => 'สำเร็จ' 
			)
		);

}elseif($page == "loadFloor"){
	$filter['floor_name']  		=  $req ->get('floor_name');
	$filter['floor_level']  	=  $req ->get('floor_level');
	$filter['mr_branch_id']  	=  $req ->get('mr_branch_id');
	$filter['mr_building_id']  	=  $req ->get('mr_building_id');
	
	$data	= $FloorDao->load_fioor_data($filter);	
	foreach($data as $key=>$val){
		$data[$key]['no'] = $key+1;
		$data[$key]['action'] ='<button type="button" class="btn btn-warning" onclick="Edit_floor('.$val['mr_floor_id'].')" title="แก้ไขข้อมูล"><i class="material-icons">edit</i></button>';
		$data[$key]['action'] ='';
	}


	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);

}elseif($page == "getBuilding"){
	$mr_branch_id  	=  $req ->get('mr_branch_id');
	$data =array();
	$data = $buildingDao->getBuildingByBranchId($mr_branch_id);

	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'บันทึกสำเร็จ' 
			)
		);
exit;

}else if($page == "save"){
		$datasave['mr_user_id'] 	= $mr_user_id;
		$datasave['mr_floor_id'] 	= $mr_floor_id;
		
		if($mr_floor_id==""){
			echo json_encode(
				array(
					'status' => 401, 
					'data' => '', 
					'message' => 'กรุณาระบุ ชั้น' 
					)
				);
			exit;
		}
		
		if($mr_user_id==""){
			echo json_encode(
				array(
					'status' => 401, 
					'data' => '', 
					'message' => 'กรุณาระบุ พนักงาน' 
					)
				);
			exit;
		}
		
		$re = $zoneDao->save($datasave);
		
		$data	= $zoneDao->getZone_UserAll();		
		foreach($data as $key=>$val){
			$data[$key]['no'] = $key+1;
			$data[$key]['action'] = $key+1;
			$data[$key]['action'] ='<button type="button" class="btn btn-danger" onclick="remove_zone('.$val['mr_zone_id'].')">ลบ</button>';
		}
		echo json_encode(
				array(
					'status' => 200, 
					'data' => $data, 
					'message' => 'บันทึกสำเร็จ' 
					)
				);
		exit;
}else if($page == "remove"){
	$mr_zone_id  	=  $req ->get('id');
	$re = $zoneDao->remove($mr_zone_id);
	$data	= $zoneDao->getZone_UserAll();		
	foreach($data as $key=>$val){
		$data[$key]['no'] = $key+1;
		$data[$key]['action'] = $key+1;
		$data[$key]['action'] ='<button type="button" class="btn btn-danger" onclick="remove_zone('.$val['mr_zone_id'].')">ลบ</button>';
	}
	echo json_encode(
			array(
				'status' => 200, 
				'data' => $data, 
				'message' => 'ลบข้อมูลเรียบร้อย' 
				)
			);
	exit;
}else{
	$data	= $zoneDao->getZone_UserAll();	
	foreach($data as $key=>$val){
		$data[$key]['no'] = $key+1;
		$data[$key]['action'] ='<button type="button" class="btn btn-danger" onclick="remove_zone('.$val['mr_zone_id'].')">ลบ</button>';
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);
	}
}



 ?>
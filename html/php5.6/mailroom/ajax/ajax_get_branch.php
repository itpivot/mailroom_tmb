<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';

$auth 			    = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();   



$page 				= $req->get('page');
if(preg_match('/<\/?[^>]+(>|$)/', $page)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
	
if($page==2){
	$sql3 = 'SELECT * FROM mr_hub';
	$hub = $userDao ->select($sql3);
	$txt_hub='';
	foreach($hub as $key_j => $val_kj){
		$txt_hub.='<option value="'.$val_kj['mr_hub_id'].'">'.$val_kj['mr_hub_name'].'</option>';
	} 

	$mr_hub_id			= $req->get('mr_hub_id');
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_hub_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
   		exit();
	}
	$params 	= array();
	$emp 		= new Pivot_Dao('mr_emp');
	$emp_db 	= $emp->getDb();  
	$sql='SELECT 
				h.mr_hub_name,
				h.mr_hub_id,
				e.mr_emp_id,
				e.mr_emp_code,
				e.mr_emp_name,
				e.mr_emp_lastname,
				e.update_date

			FROM mr_user u
			left join mr_emp e on(e.mr_emp_id = u.mr_emp_id)
			LEFT JOIN mr_hub h ON ( h.mr_hub_id = e.mr_hub_id )
			WHERE mr_user_role_id = 8
			and e.mr_emp_code != "Resign"';
	
	
	if($mr_hub_id != ''){
		$sql.=' and e.mr_hub_id = ? ';
		array_push($params, $mr_hub_id);
	}
	$stmt 				= new Zend_Db_Statement_Pdo($emp_db, $sql);   
	
	$stmt->execute($params);
	$emp_data = $stmt->fetchAll();
			
			
	//$emp_data = $userDao ->select($sql);
	foreach( $emp_data as $num => $v ){
		$mr_hub_name = $v['mr_hub_name'];
		$v['no'] = $num +1 ; 
		$v['mr_hub_name'] = '<select onchange="mess_chang_hub(\''.$v['mr_emp_id'].'\', $(this).val());" class="select_chang_brabch">
					';	
			if($mr_hub_name  != ''){
				$v['mr_hub_name'] .= '<option disabled selected>'.$mr_hub_name .'</option>';
			}else{
				$v['mr_hub_name'] .= '<option disabled selected>--</option>';
			}
			$v['mr_hub_name'] .= '	'.$txt_hub.'
				</select>';
		
		$emp_data[$num] = $v;
		
	}

$arr['data'] = $emp_data;

echo json_encode($arr);
}else{
	$params 	= array();
	$emp 		= new Pivot_Dao('mr_branch');
	$emp_db 	= $emp->getDb();  
	$sql='SELECT 
		b.mr_branch_id, 
		b.mr_branch_name, 
		b.mr_user_username_old, 
		b.mr_branch_code, 
		b.mr_hub_id, 
		b.mr_branch_type as mr_branch_type_id, 
		b.sys_date,
		h.mr_hub_name,
		bt.branch_type_name
	FROM mr_branch b
	LEFT JOIN mr_branch_type bt ON ( bt.mr_branch_type_id = b.mr_branch_type)
	LEFT JOIN mr_hub h ON ( h.mr_hub_id = b.mr_hub_id )
	where b.mr_branch_id is not null
	and b.active = 1
	';

	$mr_branch_category_id		= $req->get('mr_branch_category_id');
	$mr_branch_id		= $req->get('mr_branch_id');
	$mr_hub_id			= $req->get('mr_hub_id');
	$mr_branch_type_id	= $req->get('mr_branch_type_id');
	$stmt 		 		= new Zend_Db_Statement_Pdo($emp_db, $sql);   


	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
   		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_hub_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
   		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_type_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
   		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_category_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
   		exit();
	}


	if($mr_branch_id != ''){
		$sql.=' and b.mr_branch_id = ? ';
		array_push($params, $mr_branch_id);
	}
	if($mr_branch_category_id != ''){
		$sql.=' and b.mr_branch_category_id = ? ';
		array_push($params, $mr_branch_category_id);
	}


	if($mr_branch_type_id != ''){
		$sql.=' and b.mr_branch_type = ? ';
		array_push($params, $mr_branch_type_id);
	}


	if($mr_hub_id != ''){
		$sql.=' and b.mr_hub_id = ? ';
		array_push($params, $mr_hub_id);
	}

	//$branchdata = $userDao ->select($sql);
	$stmt 		 		= new Zend_Db_Statement_Pdo($emp_db, $sql); 
	$stmt->execute($params);
	$branchdata 		= $stmt->fetchAll();


	$sql2 = 'SELECT * FROM mr_branch_type';
	$branc_type = $userDao ->select($sql2);
	$txt_branc_type='';
	foreach($branc_type as $key_i => $val_i){
		$txt_branc_type.='<option value="'.$val_i['mr_branch_type_id'].'">'.$val_i['branch_type_name'].'</option>';
	}      



	$sql3 = 'SELECT * FROM mr_hub';
	$hub = $userDao ->select($sql3);
	$txt_hub='';
	foreach($hub as $key_j => $val_kj){
		$txt_hub.='<option value="'.$val_kj['mr_hub_id'].'">'.$val_kj['mr_hub_name'].'</option>';
	}      
	//echo "<pre>".print_r($branchdata,true)."</pre>"; 
	//echo json_encode($branchdata);
	//exit();

	foreach( $branchdata as $num => $v ){
		$mr_branch_type_id = $v['mr_branch_type_id'];
		$mr_hub_name = $v['mr_hub_name'];
		$v['no'] = $num +1 ; 
		$v['branch_type_name'] = '<select  onchange="chang_type(\''.$v['mr_branch_id'].'\', $(this).val())" class="select_chang_brabch">
									<option vallue="" disabled selected>'.$v['branch_type_name'].'</option>
									'.$txt_branc_type.'
								</select>';

		if($mr_branch_type_id <= 2){
			$v['mr_hub_name'] = '<select onchange="chang_hub(\''.$v['mr_branch_id'].'\', $(this).val());" class="select_chang_brabch">
								';	
						if($mr_hub_name  != ''){
							$v['mr_hub_name'] .= '<option disabled selected>'.$mr_hub_name .'</option>';
						}else{
							$v['mr_hub_name'] .= '<option disabled selected>--</option>';
						}
						$v['mr_hub_name'] .= '	'.$txt_hub.'
								</select>';
		}
		$v['btn'] .= '<button onclick="remove_branch(\''.$v['mr_branch_id'].'\')" type="button" class="btn btn-danger">ลบ</button>';
		$v['mr_branch_name'] = '<button style="cursor: pointer;" onclick="edit_branch_name('
			.$v['mr_branch_id'].', '
			.'\''.$v['mr_branch_code'].'\', '
			.'\''.$v['mr_branch_name'].'\', '
			.'\''.$v['mr_branch_type_id'].'\', '
			.'\''.$v['mr_hub_id'].'\' '
			.')" type="button" class="btn btn-link">
		<span class="material-icons">edit</span>
		</button>'.$v['mr_branch_code']."  :  ".$v['mr_branch_name'];
		$emp_data[$num] = $v;
		
	}




	$arr['data'] = $emp_data;

	echo json_encode($arr);
}
 ?>

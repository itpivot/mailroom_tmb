<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';
require_once 'nocsrf.php';
require_once 'Dao/Work_post.php';

$auth 			    = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();   
$work_postDao 		= new Dao_Work_post();



$page 				= $req->get('page');
$barcode 				= $req->get('barcode');
if(preg_match('/<\/?[^>]+(>|$)/', $page)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
 
   exit();
}
$byVal = array();
$byVal['mr_type_work_id'] = 6 ;
$byVal['mr_work_barcode'] = $barcode ;
$work_postdata = $work_postDao->getdataPostOutByval($byVal);
$res = array();
$res['data'] = empty($work_postdata)?array():$work_postdata[count($work_postdata)-1];
$res['count'] = count($work_postdata);
$res['status'] = 200;
$res['message'] = '-------------------';
$res['token'] = NoCSRF::generate( 'csrf_token' );
echo json_encode($res);	
//echo "---------------------";
exit;

if($page=='getdataBybarcode'){

}
 ?>

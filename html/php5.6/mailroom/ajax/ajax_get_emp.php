<?php
ini_set('memory_limit', '-1');
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Department.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Position.php';



$auth 			    = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();   


$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();  
                               
$branchDao 			= new Dao_Branch();                                 
$departmentDao 		= new Dao_Department();                                 
$floorDao 			= new Dao_Floor();                                 
$positionDao 			= new Dao_Position();                                 


$branch 		= $branchDao->fetchAll();
$department 	= $departmentDao->fetchAll();
$floor 			= $floorDao->fetchAll();
$position 		= $positionDao->fetchAll();

$d_branch 		= array();
$d_department 	= array();
$d_floor 		= array();
$d_position 	= array();

foreach($branch as $i=>$val_i){
	$d_branch[$val_i['mr_branch_id']] = $val_i;
}                              
foreach($department as $j=>$val_j){
	$d_department[$val_j['mr_department_id']] = $val_j;
}                              
foreach($floor as $k=>$val_k){
	$d_floor[$val_k['mr_floor_id']] = $val_k;
}     
foreach($position as $k=>$val_l){
	$d_position[$val_l['mr_position_id']] = $val_l;
}                              

$emp_data 			= $empDao->getEmpDataSelect();

foreach( $emp_data as $num => $v ){
    $txt = '<div class="btn-group" role="group" aria-label="Basic example">';
	if(isset($d_branch[$v['mr_branch_id']])){
		$branchname = $d_branch[$v['mr_branch_id']]['mr_branch_code']." ".$d_branch[$v['mr_branch_id']]['mr_branch_name'];
	}else{
		$branchname = '';	
	}
	

    $txt .= '<a href="edit_emp_data.php?id='.urlencode(base64_encode($v['mr_emp_id'])).'" class="btn btn-sm btn-primary" target="_blank">
				<i class="material-icons">edit</i> 
			</a>';
	if($v['mr_emp_code']!= 'Resign'){
		$txt .= '<button type="button" onclick="resign_empdata(\''.$v['mr_emp_code'].'\');" class="btn btn-sm btn-danger"><i class="material-icons">highlight_off</i> </button>';
	}
	$txt .= '</div>';
	$v['branchname'] 		='';
	$v['floor_name'] 		='';
	$v['department_name'] 	='';
	$v['position_name'] 	='';
	if(!empty($v['mr_branch_id'])){
		$v['branchname'] = $d_branch[$v['mr_branch_id']]['mr_branch_code']." ".$d_branch[$v['mr_branch_id']]['mr_branch_name'];
	}if(!empty($v['mr_floor_id'])){
		$v['floor_name'] = $d_floor[$v['mr_floor_id']]['name'];
	}if(!empty($v['mr_department_id'])){
		$v['department_name'] = $d_department[$v['mr_department_id']]['mr_department_code']." ".$d_department[$v['mr_department_id']]['mr_department_name'];
	}if(!empty($v['mr_position_id'])){
		$v['position_name'] = $d_position[$v['mr_position_id']]['mr_position_code']." ".$d_position[$v['mr_position_id']]['mr_position_name'];
	}
    $v['edit'] 				= $txt;
	$v['no'] 				= $num +1;
	$emp_data[$num] 	= $v;
	//$emp_data 	= $v;
}

echo json_encode(array(
    'status' => 200,
    'data' => $emp_data
));


 ?>

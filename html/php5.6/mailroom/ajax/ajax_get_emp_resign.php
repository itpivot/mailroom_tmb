<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';

$auth 			    = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();                                 

$emp_data = $empDao->getEmpDataResign();

foreach( $emp_data as $num => $v ){
	$emp_data[$num]['no'] = $num +1;
}

$arr['data'] = $emp_data;

echo json_encode($arr);
 ?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Floor.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$department_Dao 		= new Dao_Department();
$work_inout_Dao 		= new Dao_Work_inout();
$floor_Dao 				= new Dao_Floor();

$start 					= $req->get('start_date');
$end 					= $req->get('end_date');

function changeFormatDate($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode('/', $date);
        $result = $new_date[2]."-".$new_date[1]."-".$new_date[0];
        return $result;
    }
}

$data = array();

$data['start_date'] = changeFormatDate($start); 
$data['end_date'] = changeFormatDate($end); 
$data['mr_status_id'] = $req->get('status');

if(!empty($data['start_date'])){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['start_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
}
if(!empty($data['end_date'])){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['end_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
}
if(preg_match('/<\/?[^>]+(>|$)/', $data['mr_status_id'])) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$dept = $floor_Dao->getFloor();

//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************

$work_inout1 = $work_inout_Dao->searchAutoSummary_branch_to_HO($data);
$newlydata = array();
// echo count($work_inout1);
// echo print_r($work_inout1,true);

$arr_sum = array();
for($i = 1;$i<=15;$i++){
	$arr_sum[$i] 					= 0;
}
$arr_sum['no'] = '';
$arr_sum['mr_branch_name'] = 'รวม';


foreach($work_inout1 as $i => $i_val){
	$mr_branch_id 								= $i_val['mr_branch_id'];
	$mr_status_id 								= $i_val['mr_status_id'];

	if($mr_status_id== 7){
		$mr_status_id = 1;
	}elseif($mr_status_id== 9 or $mr_status_id == 14){
		$mr_status_id = 2;
	}elseif($mr_status_id == 10){
		$mr_status_id = 3;
	}elseif($mr_status_id == 11){
		$mr_status_id = 4;
	}elseif($mr_status_id == 12){
		$mr_status_id = 5;
	}
	
	if(!isset($newlydata[$mr_branch_id])){
		for($i = 1;$i<=15;$i++){
			$newlydata[$mr_branch_id][$i] 	= 0;
		}
		$newlydata[$mr_branch_id]['mr_branch_name'] = $i_val['mr_branch_name'];
	}

	if(!isset($newlydata[$mr_branch_id][$mr_status_id])){
		$newlydata[$mr_branch_id][$mr_status_id] = 1;
	}else{
		$newlydata[$mr_branch_id][$mr_status_id] += 1;
	}

	if(!isset($newlydata[$mr_branch_id]['sum'])){
		$newlydata[$mr_branch_id]['sum'] = 1;
	}else{
		$newlydata[$mr_branch_id]['sum'] +=1;
	}
	$arr_sum[$mr_status_id] += 1;
	$arr_sum['sum'] += 1;
}

if(!empty($newlydata)){
	$newlydata[] = $arr_sum;
}

$index = 0;
foreach($newlydata as $i => $i_val){
	$i_val['no'] = ($index+1);
	$printdata[$index] = $i_val;
	$index++;
}
echo json_encode(array(
	'status' => 200, 
	'data' => $printdata,
));

?>
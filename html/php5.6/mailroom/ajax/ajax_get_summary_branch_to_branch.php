<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Floor.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$department_Dao 		= new Dao_Department();
$work_inout_Dao 		= new Dao_Work_inout();
$floor_Dao 				= new Dao_Floor();

$start 					= $req->get('start_date');
$end 					= $req->get('end_date');

function changeFormatDate($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode('/', $date);
        $result = $new_date[2]."-".$new_date[1]."-".$new_date[0];
        return $result;
    }
}

$data = array();

$data['start_date'] = changeFormatDate($start); 
$data['end_date'] = changeFormatDate($end); 
$data['mr_status_id'] = $req->get('status');

if(!empty($data['start_date'])){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['start_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
}
if(!empty($data['end_date'])){
	if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['end_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
}
if(preg_match('/<\/?[^>]+(>|$)/', $data['mr_status_id'])) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$dept = $floor_Dao->getFloor();


//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************
//*******************MOS UPDATE *****************


$work_inout1 = $work_inout_Dao->searchAutoSummary_branch_to_branch($data);
$newlydata = array();

foreach($work_inout1 as $i => $i_val){
	$mr_branch_id 								= $i_val['mr_branch_id'];
	$mr_status_id 								= $i_val['mr_status_id'];
	

	if(!isset($newlydata[$mr_branch_id])){

		$newlydata[$mr_branch_id]  				= 	$i_val;
		$newlydata[$mr_branch_id]['st1']		= 0;
		$newlydata[$mr_branch_id]['st2']		= 0;
		$newlydata[$mr_branch_id]['st3']		= 0;
		$newlydata[$mr_branch_id]['st4']		= 0;
		$newlydata[$mr_branch_id]['st5']		= 0;
		$newlydata[$mr_branch_id]['st6']		= 0;
		$newlydata[$mr_branch_id]['st7']		= 0;
		$newlydata[$mr_branch_id]['st8']		= 0;
		$newlydata[$mr_branch_id]['st9']		= 0;
		$newlydata[$mr_branch_id]['st10']		= 0;
		$newlydata[$mr_branch_id]['st11']		= 0;
		$newlydata[$mr_branch_id]['st12']		= 0;
		$newlydata[$mr_branch_id]['st13']		= 0;
		$newlydata[$mr_branch_id]['st14']		= 0;
		$newlydata[$mr_branch_id]['st15']		= 0;


	}
	if($mr_status_id == 1){
		$newlydata[$mr_branch_id]['st1']		+= 1;
	}else if($mr_status_id == 2){
		$newlydata[$mr_branch_id]['st2']		+= 1;
	}else if($mr_status_id == 3){
		$newlydata[$mr_branch_id]['st3']		+= 1;
	}else if($mr_status_id == 4){
		$newlydata[$mr_branch_id]['st4']		+= 1;
	}else if($mr_status_id == 5){
		$newlydata[$mr_branch_id]['st5']		+= 1;
	}else if($mr_status_id == 6){
		$newlydata[$mr_branch_id]['st6']		+= 1;
	}else if($mr_status_id == 7){
		$newlydata[$mr_branch_id]['st7']		+= 1;
	}else if($mr_status_id == 8){
		$newlydata[$mr_branch_id]['st8']		+= 1;
	}else if($mr_status_id == 9){
		$newlydata[$mr_branch_id]['st9']		+= 1;
	}else if($mr_status_id == 10){
		$newlydata[$mr_branch_id]['st10']		+= 1;
	}else if($mr_status_id == 11){
		$newlydata[$mr_branch_id]['st11']		+= 1;
	}else if($mr_status_id == 12){
		$newlydata[$mr_branch_id]['st12']		+= 1;
	}else if($mr_status_id == 13){
		$newlydata[$mr_branch_id]['st13']		+= 1;
	}else if($mr_status_id == 14){
		$newlydata[$mr_branch_id]['st14']		+= 1;
	}else if($mr_status_id == 15){
		$newlydata[$mr_branch_id]['st15']		+= 1;
	}

	
	$newlydata[$mr_branch_id]['no'] 	= 	count($newlydata);
	$newlydata[$mr_branch_id]['sum'] = 	$newlydata[$mr_branch_id]['st6']+
								$newlydata[$mr_branch_id]['st7']+
								$newlydata[$mr_branch_id]['st8']+
								$newlydata[$mr_branch_id]['st9']+
								$newlydata[$mr_branch_id]['st10']+
								$newlydata[$mr_branch_id]['st11']+
								$newlydata[$mr_branch_id]['st12']+
								$newlydata[$mr_branch_id]['st13']+
								$newlydata[$mr_branch_id]['st14']+
								$newlydata[$mr_branch_id]['st15'];
	
}

$index = 0;
foreach($newlydata as $i => $i_val){
	$printdata[$index] = $i_val;
	$index++;
}
//echo print_r($newlydata,true);
//exit;

echo json_encode(array(
	'status' => 200, 
	'data' => $printdata,
));

?>
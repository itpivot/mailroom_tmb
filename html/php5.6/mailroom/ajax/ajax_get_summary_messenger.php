<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req            = new Pivot_Request();
$userDao        = new Dao_User();
$userRoleDao    = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();

$start          = $req->get('start_date');
$end            = $req->get('end_date');




function changeFormatDate($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode('/', $date);
        $result = $new_date[2]."-".$new_date[1]."-".$new_date[0];
        return $result;
    }
}

$data = array();

$data['start_date'] = changeFormatDate($start); 
$data['end_date'] = changeFormatDate($end); 
$data['mr_status_id'] = $req->get('status');

if(!empty($data['start_date'])){
    if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['start_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
    }
}
if(!empty($data['end_date'])){
    if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['end_date'])) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
    }
}

if(preg_match('/<\/?[^>]+(>|$)/', $data['mr_status_id'])) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$receive_status = array(1,2,6);
$messenger = $userDao->getMessengers();



$work_inout1 = $work_inout_Dao->getSummaryMessenger_mosUpdate($data);
// echo json_encode($work_inout1);
// exit;
// echo count($work_inout1);
// exit;

$m_p['mr_emp_code']		= "อื่นๆ";
$m_p['mr_emp_lastname']	= "";
$m_p['mr_emp_name']		= "";
$m_p['mr_user_id']		= "none";
array_push($messenger, $m_p);
$m_p['mr_emp_code']		= "รวม";
$m_p['mr_emp_lastname']	= "";
$m_p['mr_emp_name']		= "";
$m_p['mr_user_id']		= "sum";
array_push($messenger, $m_p);


// echo json_encode($messenger);
// exit;
$messenger_new = array();
foreach($messenger as $k =>$val_k){
    $mr_user_id = $val_k['mr_user_id'];
    $resp['mr_user_id'] = $val_k['mr_user_id'];
    $resp['mr_emp_fullname'] = $val_k['mr_emp_code']." : ".$val_k['mr_emp_name']."  ".$val_k['mr_emp_lastname'];
    $messenger_new[$mr_user_id] =  $resp;
}




//$work_inout2 = $work_inout_Dao->searchAutoSummary_status_4_to_6($data);
$new_data=array();
$arr_sum = array();
for($i = 1;$i<=15;$i++){
	$arr_sum[$i] 					= 0;
}
$arr_sum['no'] = '';
$arr_sum['floor_name_main'] = 'รวม';


foreach($work_inout1 as $i =>$val_i){
		//array_push($new_date[$floor_main_id],$val_i);
		
		$mr_status_id 		= ($val_i['status']!='')?$val_i['status']:0;
		if($mr_status_id	<= 3	){
			$usr_main_id 	= 	($val_i['usr_main_id']!='')?$val_i['usr_main_id']:'none';
			if($usr_main_id=='none' or $usr_main_id==0){
				$usr_main_id 	= ($val_i['usr_inout_id']!='')?$val_i['usr_inout_id']:'none';
			}
		}else{
			$usr_main_id 	= 	($val_i['usr_inout_id']!='')?$val_i['usr_inout_id']:'none';
			if($usr_main_id=='none' or $usr_main_id==0){
				$usr_main_id 	= ($val_i['usr_main_id']!='')?$val_i['usr_main_id']:'none';
			}
		}
		



        if($mr_status_id== 1 or $mr_status_id== 7 or $mr_status_id== 8){
            $mr_status_id = 1;
        }elseif($mr_status_id== 2 or $mr_status_id== 9 or $mr_status_id == 14){
            $mr_status_id = 2;
        }elseif($mr_status_id== 3 or $mr_status_id == 10){
            $mr_status_id = 3;
        }elseif($mr_status_id== 4 or $mr_status_id == 11 or $mr_status_id== 13){
            $mr_status_id = 4;
        }elseif($mr_status_id== 5 or $mr_status_id == 12){
            $mr_status_id = 5;
        }elseif($mr_status_id== 6 or $mr_status_id == 15){
            $mr_status_id = 6;
        }



        if(!isset($new_data[$usr_main_id])){
            for($i = 1;$i<=15;$i++){
                $new_data[$usr_main_id][$i] 	= 0;
            }
            $new_data[$usr_main_id]['mr_emp_fullname']  = $messenger_new[$usr_main_id]['mr_emp_fullname'];//$i_val['floor_name_main'];
            $new_data[$usr_main_id]['mr_user_id']       = $messenger_new[$usr_main_id]['mr_user_id'];//$i_val['floor_name_main'];
        }
        if(!isset($new_data[$usr_main_id][$mr_status_id])){
            $new_data[$usr_main_id][$mr_status_id] = 1;
        }else{
            $new_data[$usr_main_id][$mr_status_id] += 1;
        }
    
        if(!isset($new_data[$usr_main_id]['sum'])){
            $new_data[$usr_main_id]['sum'] = 1;
        }else{
            $new_data[$usr_main_id]['sum'] +=1;
        }
    
    
        //$newlydata[$floor_main_id]['no'] 	= 	count($newlydata);
        $arr_sum[$mr_status_id] += 1;
        $arr_sum['sum'] += 1;

}

// echo json_encode($new_data);
// exit;
if(!empty($new_data)){
    //$new_data['sum'] = $arr_sum;
    $index = 0;
    $str = '';

//     echo json_encode($new_data);
// exit;
    foreach($new_data as $i => $i_val){
        $i_val['no'] = ($index+1);
        $printdata[$index] = $i_val;
        $index++;
        $newly              ='';
        $mess_receive       ='';
        $mailroom           ='';
        $mess_send          ='';
        $success            ='';
        $cancel             ='';
        $no                 ='';
            if($i_val['mr_user_id'] !== '') {
                $fn_getDetail = "'".urlencode(base64_encode($i_val['mr_user_id']))."'";
                
                $no                         = ($index+1);
                $newly                      = $i_val['1']                   == 0 ? '_' : '<a href="#" onclick="getDetail('.$fn_getDetail.',1'.');" >'.$i_val['1'].'</a>';
                $mess_receive               = $i_val['2']                   == 0 ? '_' : '<a href="#" onclick="getDetail('.$fn_getDetail.',2'.');" >'.$i_val['2'].'</a>';
                $mailroom                   = $i_val['3']                   == 0 ? '_' : '<a href="#" onclick="getDetail('.$fn_getDetail.',3'.');" >'.$i_val['3'].'</a>';
                $mess_send                  = $i_val['4']                   == 0 ? '_' : '<a href="#" onclick="getDetail('.$fn_getDetail.',4'.');" >'.$i_val['4'].'</a>';
                $success                    = $i_val['5']                   == 0 ? '_' : '<a href="#" onclick="getDetail('.$fn_getDetail.',5'.');" >'.$i_val['5'].'</a>';
                $cancel                     = $i_val['6']                   == 0 ? '_' : '<a href="#" onclick="getDetail('.$fn_getDetail.',6'.');" >'.$i_val['6'].'</a>';
                $sum                        = $i_val['sum']                 == 0 ? '_' :$i_val['sum'];
                $i_valmr_emp_fullname       = $i_val['mr_emp_fullname']    == '' ? '_' :$i_val['mr_emp_fullname'];
                // echo json_encode($i_val);
                // exit;

            } else {
                $no = "";
                $newly          = $i_val['newly'];
                $mess_receive   = $i_val['mess_receive'];
                $mailroom       = $i_val['mailroom'];
                $mess_send      = $i_val['mess_send'];
                $success        = $i_val['success'];
                $cancel         = $i_val['cancel'];
                
            }
            $str .= '<tr>';
            $str .= '<td>'.$i_val['no'].'</td>';
            $str .= '<td>'.$i_valmr_emp_fullname.'</td>';
            $str .= '<td>'.$newly.'</td>';
            $str .= '<td>'.$mess_receive.'</td>';
            $str .= '<td>'.$mailroom.'</td>';
            $str .= '<td>'.$mess_send.'</td>';
            $str .= '<td>'.$success.'</td>';
            $str .= '<td>'.$cancel.'</td>';
            $str .= '<td>'.$sum.'</td>';
            $str .= '</tr>';
            
    }

    $str .= '<tr>';
    $str .= '<td>Z</td>';
    $str .= '<td>รวม</td>';
    $str .= '<td>'.$arr_sum[1].'</td>';
    $str .= '<td>'.$arr_sum[2].'</td>';
    $str .= '<td>'.$arr_sum[3].'</td>';
    $str .= '<td>'.$arr_sum[4].'</td>';
    $str .= '<td>'.$arr_sum[5].'</td>';
    $str .= '<td>'.$arr_sum[6].'</td>';
    $str .= '<td>'.$arr_sum['sum'].'</td>';
$str .= '</tr>';
   
}
$arr_data = array();
$arr_data['MESS'] = $work_inout1;
$arr_data['html'] = $str;

echo json_encode($arr_data);
exit;


$end_data = array();
$sum = array();
$sum['newly'] 		               =0;
$sum['msg_receive']                =0;
$sum['mailroom'] 	               =0;
$sum['msg_send'] 	               =0;
$sum['success'] 	               =0;
$sum['cancel'] 		               =0;
$sum['total'] 	                   =0;
foreach($messenger as $k =>$val_k){
    if(!isset($end_data[$k])){
        $end_data[$k]['newly'] 			    =0;
        $end_data[$k]['mess_receive'] 	    =0;
        $end_data[$k]['mailroom'] 		    =0;
        $end_data[$k]['mess_send'] 		    =0;
        $end_data[$k]['success'] 		    =0;
        $end_data[$k]['cancel'] 		    =0;
        $end_data[$k]['total'] 	            =0;
    }
		
	//$sum['no'] 	   					   =$k+2;
	$sum['mr_emp_fullname'] 	   ='รวม';
	$sum['mr_user_id'] 	   ='';
	

	$mr_user_id = $val_k['mr_user_id'];
	$end_data[$k]['no'] 			= $k+1;
	$end_data[$k]['mr_emp_fullname'] 				= $val_k['mr_emp_code'].':'.$val_k['mr_emp_name'].'  '.$val_k['mr_emp_lastname'];
	$end_data[$k]['newly'] 							.= intval($new_data[$mr_user_id]['newly']);
	$end_data[$k]['mr_user_id'] 					.= $val_k['mr_user_id'];
	$end_data[$k]['mess_receive'] 					.= intval($new_data[$mr_user_id]['msg_receive']);
	$end_data[$k]['mailroom'] 						.= intval($new_data[$mr_user_id]['mailroom']);
	$end_data[$k]['mess_send'] 						.= intval($new_data[$mr_user_id]['msg_send']);
	$end_data[$k]['success'] 						.= intval($new_data[$mr_user_id]['success']);
	$end_data[$k]['cancel'] 						.= intval($new_data[$mr_user_id]['cancel']);
	$end_data[$k]['total'] 							.= $end_data[$k]['newly'] 		
														+$end_data[$k]['mess_receive']
														+$end_data[$k]['mailroom'] 	
														+$end_data[$k]['mess_send'] 	
														+$end_data[$k]['success'] 	
														+$end_data[$k]['cancel'] 	;	
	$sum['newly'] 		    .= $end_data[$k]['newly'] 			    ;
	$sum['mess_receive']    .= $end_data[$k]['mess_receive'] 	    ;														
	$sum['mailroom'] 	    .= $end_data[$k]['mailroom'] 		    ;														
	$sum['mess_send'] 	    .= $end_data[$k]['mess_send'] 		    ;														
	$sum['success'] 	    .= $end_data[$k]['success'] 		    ;														
	$sum['cancel'] 		    .= $end_data[$k]['cancel'] 		    ;														
	$sum['total'] 	        .= $end_data[$k]['total'] 	            ;														
															
}
array_push($end_data,$sum);
//array_push($end_data,$sum);





echo json_encode($end_data);
exit;





$inout = $work_inout_Dao->getSummaryMessenger($data);
$result = array();
$mes = array();
foreach($inout as $key => $value){
    if(in_array($value['status'], $receive_status)){
        $result[$value['usr_main_id']][] = $value;
    } else {
        $result[$value['usr_inout_id']][] = $value;
    }
}
ksort($result);
$index = 0;
foreach($result as $k => $v) {
           $mes[$index]['mr_user_id'] = $k;
           $status = $work_inout_Dao->array_column($v, 'status');
           $mes[$index]['status'] = $status;
           $index++;
}


    $resp = array();
    foreach($messenger as $keys => $vals) {
        $resp[$keys]['mr_user_id'] = $vals['mr_user_id'];
        $resp[$keys]['mr_emp_fullname'] = $vals['mr_emp_code']." : ".$vals['mr_emp_name']."  ".$vals['mr_emp_lastname'];
        $newly = 0;
        $mess_receive = 0;
        $mailroom = 0;
        $mess_send = 0;
        $success = 0;
        $cancel = 0;

        foreach($mes as $indexs => $datas) {
            if($datas['mr_user_id'] == $vals['mr_user_id']) {
                foreach($datas['status'] as $i => $d) {
                    switch($d) {
                        case 1:
                            $newly .= 1;
                            break;
                        case 2:
                            $mess_receive .= 1;
                            break;
                        case 3:
                            $mailroom .= 1;
                            break;
                        case 4:
                            $mess_send .= 1;
                            break;
                        case 5:
                            $success .= 1;
                            break;
                        case 6:
                            $cancel .= 1;
                            break;
                    }
                }
            }
        }
        $resp[$keys]['newly'] = $newly;
        $resp[$keys]['mess_receive'] = $mess_receive;
        $resp[$keys]['mailroom'] = $mailroom;
        $resp[$keys]['mess_send'] = $mess_send;
        $resp[$keys]['success'] = $success;
        $resp[$keys]['cancel'] = $cancel;
        $sum_total = array($newly, $mess_receive, $mailroom, $mess_send, $success, $cancel);
        $resp[$keys]['total'] = array_sum($sum_total);
    }

    $resp[count($resp)] = array(
        'mr_user_id' =>  '',
        'mr_emp_fullname' => 'รวม',
        'newly' => array_sum( $work_inout_Dao->array_column($resp, 'newly')),
        'mess_receive' => array_sum( $work_inout_Dao->array_column($resp, 'mess_receive')),
        'mailroom' => array_sum( $work_inout_Dao->array_column($resp, 'mailroom')),
        'mess_send' => array_sum( $work_inout_Dao->array_column($resp, 'mess_send')),
        'success' => array_sum( $work_inout_Dao->array_column($resp, 'success')),
        'cancel' => array_sum( $work_inout_Dao->array_column($resp, 'cancel')),
        'total' => array_sum( $work_inout_Dao->array_column($resp, 'total')),
    );

    echo json_encode($resp);







?>
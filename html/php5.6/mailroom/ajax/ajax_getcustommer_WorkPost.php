<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_byhand.php';
require_once 'Dao/Work_post.php';

$auth 			            = new Pivot_Auth();
$employeeDao 	            = new Dao_Employee();
$req 			            = new Pivot_Request();
$work_byhandDao 			= new Dao_Work_byhand();
$work_postDao 			    = new Dao_Work_post();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$txt = $req->get('search');
if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}



$emp_data = $work_postDao->getDataWithTXTsearch($txt);

// echo json_encode($emp_data);
// exit;

$json = array();
foreach ($emp_data as $key => $value) {
    $fullname = $value['mr_cus_name']." ".$value['mr_cus_lname'];
    $name = $value['mr_cus_name'];
    $lname = $value['mr_cus_lname'];
    
	$json[] = array(
		"value"                 => $value['mr_work_post_id'],
		"label"                 => $fullname,
		"name"                  => $name,
		"lname"                 => $lname,
        "mr_address"            => $value['mr_address'],
        "fullname"              => $fullname,
        "mr_cus_tel"            => $value['mr_cus_tel'],
        "zipcode"               => $value['zipcode'],
        "mr_sub_districts_code" => $value['mr_sub_districts_code'],
        "mr_sub_districts_name" => $value['mr_sub_districts_name'],
        "mr_districts_code"     => $value['mr_districts_code'],
        "mr_districts_name"     => $value['mr_districts_name'],
        "mr_provinces_code"     => $value['mr_provinces_code'],
        "mr_provinces_name"     => $value['mr_provinces_name'],
	);
}


echo json_encode($json);









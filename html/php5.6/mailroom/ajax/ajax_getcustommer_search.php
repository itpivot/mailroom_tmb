<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_byhand.php';

$auth 			= new Pivot_Auth();
$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();
$work_byhandDao 			= new Dao_Work_byhand();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$txt = $req->get('search');
if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}



$emp_data = $work_byhandDao->getDataWithTXTsearch($txt);



$json = array();
foreach ($emp_data as $key => $value) {
    $fullname = $value['mr_cus_name'];
    list($name,$lname) = explode(' ',$value['mr_cus_name']);
    //$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
	$json[] = array(
		"value" => $value['mr_work_byhand_id'],
		"label" => $fullname,
		"name" => $name,
		"lname" => $lname,
        "mr_address" => $value['mr_address'],
        "fullname" => $value['mr_cus_name'],
        "mr_cus_tel" => $value['mr_cus_tel'],
	);
}


echo json_encode($json);
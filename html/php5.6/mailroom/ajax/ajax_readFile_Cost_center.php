<?php 
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Position.php';

require_once 'nocsrf.php';
require_once 'PHPExcel.php';
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');
error_reporting(E_ALL & ~E_NOTICE);




$auth 					= new Pivot_Auth();
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();
$positionDao 		    = new Dao_Position();


if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

try{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*120, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}catch ( Exception $e ){
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
   exit();
}



$employee 				= $employeeDao->getEmpCode();
$userData 				= $userDao->get_Username();
$department_data 		= $departmentDao->getdepartmentall();
$floor_data 			= $floorDao->getFloorAll();
$branch_data 			= $branchDao->getBranch();
$position_data 			= $positionDao->getPosition();
$cost_data 				= $costDao->fetchAll();
$empall_data 				= $employeeDao->fetchAll();



$name 			= $_FILES['file']['name'];
$tmp_name 		= $_FILES['file']['tmp_name'];
$size 			= $_FILES['file']['size'];
$err 			= $_FILES['file']['error'];
$file_type 		= $_FILES['file']['type'];


// echo print_r($_FILES,true);
// exit;

$file_name = explode( ".", $name );
$extension = $file_name[count($file_name) - 1]; // xlsx|xls
try {
    $inputFileType = PHPExcel_IOFactory::identify($tmp_name);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($tmp_name);
} catch(Exception $e) {
	echo json_encode(array('status' => 505, 'message' => 'Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage() ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}


$emp = array();
foreach($empall_data  as $i_emp => $v_emp ){
	$emp[$v_emp['mr_emp_code']] = $v_emp['mr_emp_id'];
}

$cost = array();
foreach($cost_data as $i_cost => $v_cost ){
	$cost[$v_cost['mr_cost_code']] = $v_cost['mr_cost_id'];
}


$department = array();
foreach($department_data as $i_depart => $v_depart ){
	$department[$v_depart['mr_department_code']] = $v_depart['mr_department_id'];
}


$floor = array();
foreach($floor_data as $i_floor => $v_floor ){
	$floor[$v_floor['name']] = $v_floor['mr_floor_id'];
}

$branch = array();
foreach($branch_data as $i_branch => $v_branch_data ){
	$branch[$v_branch_data['mr_branch_code']] = $v_branch_data['mr_branch_id'];
}

$position = array();
foreach($position_data as $i_position => $v_position ){
	$position[$v_position['mr_position_code']] = $v_position['mr_position_id'];
}




$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$result = 0;
for ($row = 1; $row <= $highestRow; $row++){ //  Read a row of data into an array
    $type 				= $sheet->getCell('A'.$row)->getValue();
    $type_full			= $sheet->getCell('B'.$row)->getValue();

    $emp_code 			= $sheet->getCell('C'.$row)->getValue();

    $Dept_10 			= $sheet->getCell('H'.$row)->getValue();
    $Dept_4 			= $sheet->getCell('I'.$row)->getValue();
    $Dept_name 			= $sheet->getCell('J'.$row)->getValue();
    $Cost_update 		= $sheet->getCell('K'.$row)->getValue();
	
	

	if($row == 1){
		$arr_msg = '';
		$excollums = array('A','B','C','H','I','J','K');
		$collumsname = array('สังกัด',	'สังกัด (ชื่อเต็ม)',	'ID',	'Dept ID',	'DeptID(4)',	'Description TH',	'PC Cost Center');
		$collumsval  = array($type,		$type_full,			  $emp_code,    $Dept_10,	$Dept_4,	$Dept_name,	   $Cost_update);
		foreach($collumsname as $key => $c_val){
			if($collumsval[$key] != $c_val){
				$arr_msg .= '<div class="alert alert-danger" role="alert"> ชื่อ collum '.$excollums[$key]." ไม่ตรงกับระบบ >>[".$collumsval[$key]."] != [".$c_val."]</div>";
			}							
		}
		if(!empty($arr_msg)){
			echo json_encode(array('status' => 505, 'message' => $arr_msg ,'token'=>NoCSRF::generate('csrf_token')));
			exit();
		}
		
	}else{
		if($type == ''){
			break;  
		}
		if(isset($cost[$Cost_update])){
			//echo "update <br>";
			$mr_cost_id = $cost[$Cost_update];
		}else{
			$costupdate = array();
			$costupdate['mr_cost_name'] = $Dept_name;
			$costupdate['mr_cost_code'] = $Cost_update;
			$mr_cost_id = $costDao->save($costupdate);
			$cost[$Cost_update] = $mr_cost_id;
			//echo "--- <br>";
		}
		if(isset($department[$Dept_10])){
			$mr_department_id = $department[$Dept_10];
			$dep_update = array();
			$dep_update['mr_department_name'] = $Dept_name;
			$dep_update['mr_department_code_4'] = $Dept_4;
			if($mr_department_id!=''){
				$departmentDao->save($dep_update,$mr_department_id);
			}
		}elseif(isset($department[$Dept_4])){
			$dep_update = array();
			$mr_department_id = $department[$Dept_4];
			$dep_update = array();
			$dep_update['mr_department_name'] 	= $Dept_name;
			$dep_update['mr_department_code'] 	= $Dept_10;
			$dep_update['mr_department_code_4'] = $Dept_4;
			if($mr_department_id!=''){
				$departmentDao->save($dep_update,$mr_department_id);
			}
		
		}else{
			$dep_update = array();
			$dep_update['mr_department_name'] 	= $Dept_name;
			$dep_update['mr_department_code'] 	= $Dept_10;
			$dep_update['mr_department_code_4'] = $Dept_4;
			$mr_department_id = $departmentDao->save($dep_update);
			$department[$Dept_10] = $mr_department_id;
		}

		if(isset($emp[$emp_code])){
			$employee_id = $emp[$emp_code];
			$emp_update = array();
			$emp_update['mr_department_id'] = $mr_department_id;
			$emp_update['mr_cost_id'] 		= $mr_cost_id;
			$employeeDao->save($emp_update,$employee_id);
			$result++;
			//echo "update <br>";
		}else{
			//echo "--- <br>";
		}
	}
}
// exit;



 echo $result;
 exit;
 
?>
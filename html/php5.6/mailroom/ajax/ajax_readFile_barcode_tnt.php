<?php 
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Tnt_barcode_import.php';
require_once 'PHPExcel.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();
$tnt_barcode_importDao 	= new Dao_Tnt_barcode_import();

$sql = "SELECT mr_branch_id, mr_branch_name, mr_branch_code, mr_address_id, mr_group_id, mr_hub_id, mr_branch_type, sys_date FROM mr_branch";
$branch_data =  $branchDao->selectdata($sql);


$name 			= $_FILES['file']['name'];
$tmp_name 		= $_FILES['file']['tmp_name'];
$size 			= $_FILES['file']['size'];
$err 			= $_FILES['file']['error'];
$file_type 		= $_FILES['file']['type'];

$allowfile 		= array('xlsx','xls');
$surname_file = strtolower(pathinfo($name, PATHINFO_EXTENSION));

if(!in_array($surname_file,$allowfile)){
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$file_name = explode( ".", $name );
$extension = $file_name[count($file_name) - 1]; // xlsx|xls

try {
    $inputFileType = PHPExcel_IOFactory::identify($tmp_name);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($tmp_name);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$ch_num = 0;
//  Loop through each row of the worksheet in turn
$try_data['error'] 		= array();
$try_data['success'] 	= array();
$ch_error=0;
for ($row = 2; $row <= $highestRow; $row++){ 
	$ch_error=0;
    $no 				= $sheet->getCell('A'.$row)->getValue();
    $barcode			= $sheet->getCell('B'.$row)->getValue();
    $branch_code 		= $sheet->getCell('F'.$row)->getValue();
    $Branch_name 		= $sheet->getCell('G'.$row)->getValue();
	$date_	 			= $sheet->getCell('L'.$row)->getValue();
	
	$no 			=  	preg_quote($no, '/');
	$barcode 		=  	preg_quote($barcode, '/');
	$branch_code 	=  	preg_quote($branch_code, '/');
	$Branch_name 	=  	preg_quote($Branch_name, '/');
	$date_ 			=  	preg_quote($date_, '/');
	  

	$branch                   = array_filter($branch_data, function($el) use ($branch_code) {return $el['mr_branch_code'] == $branch_code; });



	if($no!=''){
			if($branch_code == ''){
				array_push($try_data['error'], 'ไม่พบ Barcode ลำดับที่ :  '.$no);
				$ch_error++;
			}if(empty($branch)){
				array_push($try_data['error'], 'ไม่พบรหัสสาขา ลำดับที่ :   '.$no);
				$ch_error++;
			}if($date_ == ''){
				array_push($try_data['error'], 'ไม่พบวันที่ ลำดับที่ :   '.$no);
				$ch_error++;
			}	
			if(empty($branch)){
				array_push($try_data['error'], 'ไม่พบสาขา ลำดับที่ :   '.$no);
				$ch_error++;
			} 
			if($ch_error<=0){
				$branch = array_values($branch);
				$branch_id 	= $branch[0]['mr_branch_id'];
				$date = date('Y-m-d',strtotime($date_));

				$save_barcode['date_send'] 		= $date;
				$save_barcode['mr_branch_id'] 	= $branch_id;
				$save_barcode['barcode_tnt'] 	= $barcode;
			
				$sql_checkbarcode = "SELECT 
						mr_tnt_barcode_import_id, 
						sys_date, date_send, 
						mr_branch_id, 
						barcode_tnt, 
						active
					FROM mr_tnt_barcode_import
					where barcode_tnt like'".$barcode."'
				";

				$sql_checkbranch = "SELECT 
						mr_tnt_barcode_import_id, 
						sys_date, date_send, 
						mr_branch_id, 
						barcode_tnt, 
						active
					FROM mr_tnt_barcode_import
					where mr_branch_id = ".$branch_id."
					and active = 0
				";
		//echo print_r($branch ,true);
		//echo '----------------------------------------------<br>';
			//$sql = "SELECT * FROM mr_floor WHERE name IN ('".$floor."')";
			$checkbarcode =  $branchDao->select($sql_checkbarcode);
			$checkbranch =  $branchDao->select($sql_checkbranch);

			if(!empty($checkbarcode)){
				array_push($try_data['error'], ' barcode ซ้ำ  '.$barcode);
			}elseif(!empty($checkbranch)){
				array_push($try_data['error'], ' สาขา ซ้ำ  '.$branch_code);
			}else{
				$successid = $tnt_barcode_importDao->save($save_barcode);
				array_push($try_data['success'],$successid);
			}
		}
		
 	}else{
		array_push($try_data['error'], 'ไม่มี no  '.$no);
	}
}
$try_data['count_error'] = count($try_data['error']);
$try_data['count_success'] = count($try_data['success']);

$txt_error = '';
foreach($try_data['error'] as $key => $val){
	$txt_error .=  '<span class="badge badge-pill badge-danger">'.$val.'</span><br>';
}

$try_data['txt_error'] = $txt_error;

echo json_encode(array(
	'status' => 200,
	'data' => $try_data
));
exit;
?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'nocsrf.php';
require_once 'Dao/Round.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
} else {

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();
$roundDao			= new Dao_Round();
$user_id			= $auth->getUser();





$page  			=  $req ->get('page');//save
$type_work  	=  $req ->get('type_work');
$round_name  	=  $req ->get('round_name');
$mr_round_id  	=  $req ->get('mr_round_id');
$re 			= array();
if($page == "save"){
		$dataround['mr_round_name'] 	= $round_name;
		$dataround['mr_type_work_id'] 	= $type_work;
		
		if($round_name==""){
			echo json_encode(
				array(
					'status' => 401, 
					'data' => '', 
					'message' => 'ข้อมูลซ้ำ' 
					)
				);
			exit;
		}
		
		if($type_work==""){
			echo json_encode(
				array(
					'status' => 401, 
					'data' => '', 
					'message' => 'ข้อมูลซ้ำ' 
					)
				);
			exit;
		}
		
		$re = $roundDao->save($dataround,$mr_round_id);
		
		$data	= $roundDao->getRoundAll();	
		foreach($data as $key=>$val){
			$data[$key]['no'] = $key+1;
		}
		echo json_encode(
				array(
					'status' => 200, 
					'data' => $data, 
					'message' => 'บันทึกสำเร็จ' 
					)
				);
		exit;
}else{
	$data	= $roundDao->getRoundAll();	
	foreach($data as $key=>$val){
		$data[$key]['no'] = $key+1;
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);
	}
}



 ?>
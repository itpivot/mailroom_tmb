<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'nocsrf.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
   echo "Failed";
} else {
try
{
	// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
	NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
	// form parsing, DB inserts, etc.
	// ...
	$result = 'CSRF check passed. Form parsed.';
}
catch ( Exception $e )
{
	// CSRF attack detected
  $result = $e->getMessage() . ' Form ignored.';
   echo json_encode(array(
		'status' => 505, 
		'getMessage' => $e->getMessage(), 
		'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,
		'token'=>NoCSRF::generate( 'csrf_token' )));
 
   exit();
}


$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();
$user_id			= $auth->getUser();


$barcodeok	 		= '';




	
try{
	$mr_work_main_id 		= $req->get('update_mr_work_main_id');
	$mr_work_byhand_id 		= $req->get('mr_work_byhand_id');
    $save_main['mr_status_id']				= 4;
	$save_main['messenger_user_id'] 		= $req->get('messenger_user_id');
    $save_main['mr_round_id']				= $req->get('round');
    $save_main['mr_work_date_sent']			= $req->get('date_send');
	$main_id 								= $work_mainDao->save($save_main,$mr_work_main_id);
	
	
////////////////////////////////////////////////////////////
    $save_byhand['mr_work_byhand_type_id']	= $req->get('type_send');

	$work_byhand_id	= $work_byhandDao->save($save_byhand,$mr_work_byhand_id);
	$save_log['mr_user_id'] 									= $user_id;
	$save_log['mr_work_main_id'] 								= $mr_work_main_id;
	$save_log['mr_status_id'] 									= 4;
	$save_log['remark'] 										= "mailroom_สร้างราายการนำส่ง";
	$work_logDao->save($save_log);



	echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
		

}
catch ( Exception $e )
{
	echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}	
		

}



 ?>
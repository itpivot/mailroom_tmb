<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();

$remark_success = $req->get('remark_success');
$work_id = $req->get('work_id');
$wo_status_id = $req->get('wo_status_id');


//$work_id = $_SESSION['all_work'];
$arr_work_id = explode(",", $work_id);

function decode_work_main_id($id)
{
	return base64_decode(urldecode($id));
	//urlencode(base64_encode($work_main_id));
}
$params = array_map('decode_work_main_id', $arr_work_id);
$impload_id = implode(',',$params);


if($wo_status_id == 5){
	$remark_success = 'สำเร็จนอกระบบ>> '.$remark_success;
}elseif($wo_status_id == 6){
	$remark_success = 'ยกเลิกรายการ>> '.$remark_success;
}else{
	echo json_encode(array('status' => 500, 'message' => 'บันทึกไม่สำเร็จ ไม่พบสถานะงาน'));
	exit;
}
$sql = "
		UPDATE mr_work_main 
		SET mr_work_remark = concat(mr_work_remark,' || ','".$remark_success."') ,
		mr_status_id = '".$wo_status_id."'
		WHERE `mr_work_main_id` in(".$impload_id.")";
		
$send = $work_inout_Dao->updatework_successWithMainId($sql);


foreach($params as $key => $id){
	$save_log['mr_user_id'] 									= $auth->getUser();
	$save_log['remark'] 									    = $remark_success;
	$save_log['mr_status_id'] 									= $wo_status_id;
	$save_log['mr_work_main_id'] 								= $id;
	$work_log_Dao->save($save_log);
}
//$work_log_Dao->save($save_log);
//$resp = $work_main_Dao->save($data,$mr_work_main_id);


if($send != "") {
    echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ'));
} else {
    echo json_encode(array('status' => 500, 'message' => 'บันทึกไม่สำเร็จ'));
}

?>
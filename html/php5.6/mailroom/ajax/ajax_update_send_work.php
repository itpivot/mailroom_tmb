<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$barcode 			= $req ->get('barcode');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
$params = array();
$sql_select = "
			SELECT 
				m.*,
				e.mr_emp_name as send_name,
				e.mr_emp_lastname  as send_lname,
				e2.mr_emp_name as re_name,
				e2.mr_emp_lastname  as re_lname,
				m.mr_status_id,
				d.mr_department_code as re_mr_department_code,
				d.mr_department_name as re_mr_department_name,
				b.mr_branch_code as re_mr_branch_code,
				sw.barcode as supper_barcode,
				tw.mr_type_work_name,
				st.mr_status_name
				
			FROM mr_work_main  m 
				LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
				LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
				LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
				left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
				LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
				Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
				Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
				Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
				where sw.barcode like ?
				and m.mr_status_id in(8)
				group by m.mr_work_main_id 
";

array_push($params, (string)$barcode);
$data = $work_mainDao->select_work_main($sql_select,$params);

 if(empty($data)){
	$sqlupdate = "UPDATE mr_send_work SET mr_receive_date = '".date('Y-m-d H:i:s')."' WHERE barcode like '".$barcode."'";
	$data 		= $send_workDao->query1($sqlupdate);
	echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ'));
 }else{
	echo json_encode(array('status' => 500, 'message' => 'ยังรับงานไม่ครบถ้วน'));
 }

 ?>
 
 
 

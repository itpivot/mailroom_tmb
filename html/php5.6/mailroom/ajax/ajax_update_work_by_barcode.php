<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 	= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();


$users 				= $auth->getUser();
$barcode 			= $req->get('barcode');
$full_barcode 		= $req->get('full_barcode');
$mr_round_id 		= $req ->get('mr_round_id');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $full_barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$status = 3;

if( $full_barcode == "true" ){
	$barcode_full = $barcode;
}else if( $full_barcode == "false" ){
	$barcode_full 	= "TM".date("dmy").$barcode;
}

$work_data = $work_mainDao->getIDbyBarcode($barcode_full);

$save_work_main['mr_status_id'] 	= $status;
$save_work_log['mr_status_id']		= $status;
$save_work_log['mr_user_id'] 		= $users;
$save_work_log['mr_work_main_id'] 	= $work_data['mr_work_main_id'];
$save_work_log['mr_round_id'] 		= $mr_round_id;
$save_work_log['remark'] 			= "Mailroom รับเอกสาร จากพนักงานเดิน";



if ( $work_data['mr_work_main_id'] == "" || $work_data['mr_status_id'] == 4 || $work_data['mr_status_id'] == 5 || $work_data['mr_status_id'] == 3 ){
	echo json_encode(
	array('status' => 500, 
			'message' => 'ไม่สามารถบันทึกได้ โปรดตรวจสอบความถูกต้อง',
			'work_data' => $work_data
			
			
			));
}else{
	$work_mainDao->save($save_work_main,$work_data['mr_work_main_id']);
	$work_logDao->save($save_work_log);

	echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ'));
	
}

?>

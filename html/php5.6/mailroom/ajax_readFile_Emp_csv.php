<?php 
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Position.php';
ini_set('max_execution_time', '0');
error_reporting(E_ALL & ~E_NOTICE);




$auth 					= new Pivot_Auth();
$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();
$positionDao 		    = new Dao_Position();


if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}



$employee 				= $employeeDao->getEmpCode();
$userData 				= $userDao->get_Username();
$department_data 		= $departmentDao->getdepartmentall();
$floor_data 			= $floorDao->getFloorAll();
$branch_data 			= $branchDao->getBranch();
$position_data 			= $positionDao->getPosition();




$department = array();
foreach($department_data as $i_depart => $v_depart ){
	$department[$v_depart['mr_department_code']] = $v_depart['mr_department_id'];
}

$floor = array();
foreach($floor_data as $i_floor => $v_floor ){
	$floor[$v_floor['name']] = $v_floor['mr_floor_id'];
}

$branch = array();
foreach($branch_data as $i_branch => $v_branch_data ){
	$branch[$v_branch_data['mr_branch_code']] = $v_branch_data['mr_branch_id'];
}

$position = array();
foreach($position_data as $i_position => $v_position ){
	$position[$v_position['mr_position_code']] = $v_position['mr_position_id'];
}

$employee_data = array();

function getCSV($file)
{
	$cols = array();
	$rows = array();
	
	$handle = fopen($file, 'r');
	$length = 1024;
	while (($cols = fgetcsv($handle, $length, ',')) !== FALSE) {
		foreach( $cols as $key => $val ) {
			$cols[$key] = trim( $cols[$key] );
			$tis = iconv('utf-8', 'tis-620', $cols[$key]) ;
			$cols[$key] = iconv('tis-620', 'utf-8', $tis) ;
			$cols[$key] = str_replace('""', '"', $cols[$key]);
		}
		$rows[] = $cols;
	}
	return $rows;
}


if (!function_exists('array_column')) {
    function array_column($input, $column_key, $index_key = null) {
        $arr = array_map(function($d) use ($column_key, $index_key) {
            if (!isset($d[$column_key])) {
                return null;
            }
            if ($index_key !== null) {
                return array($d[$index_key] => $d[$column_key]);
            }
            return $d[$column_key];
        }, $input);

        if ($index_key !== null) {
            $tmp = array();
            foreach ($arr as $ar) {
                $tmp[key($ar)] = current($ar);
            }
            $arr = $tmp;
        }
        return $arr;
    }
}



$user_data = array_column($userData, 'mr_user_username');
$employee_data = array_column($employee, 'mr_emp_code');
$file = $_FILES['file']; // File 
$commit_date = $_POST['commit_date'];

$days = explode('/', $commit_date); 
$cm_date = $days[0].'-'.$days[1].'-'.$days[2];


//echo '<pre>'.print_r( $position, true).'</pre>';
//exit();

$date_now  			= date('Y-m-d'); // Now Date
$results 			= array();
    $file_name 		= $_FILES['file']['name'];
	$tmp_name 		= $_FILES['file']['tmp_name'];
	$file_size 		= $_FILES['file']['size'];
	$err 			= $_FILES['file']['error'];
	$file_type 		= $_FILES['file']['type'];

$path = '../download/';
$file_names = explode('.', $file_name);
$surename = $file_names[count($file_names) - 1];
$new_name = 'TMBmailroom_data_emp_'.date('Ymdhis').'('.$date_now.').'.$surename;


if(!is_dir($path)) {
   @mkdir($path); // create path
   move_uploaded_file($tmp_name, $path.$new_name);
}else {
   move_uploaded_file($tmp_name, $path.$new_name);
}

$data = array();
$DATA_D = array();
$values = array();

if(file_exists($path.$new_name)) {
	if( $surename == 'csv' ){
				
			$path_file = $path.$new_name;
			//$file_csv = file($path_file);
			$i = 0;
			$nData = 0;
			$text = getCSV($path_file);
			unset($text[0]);
			$index_value = 0;
//echo '<pre>'.print_r($text,true).'<pre>';
//echo ">>>>";
//exit;			
			foreach($text as $value){
								
				//unset($DATA[0]);
				$DATA 				= $value;
			
					if ( $DATA['0'] != "D"  ){
						$values[ $index_value ]['type'] 		= $DATA['0'];
						$values[ $index_value ]['id'] 			= $DATA['1'];
						
							list( $name_1, $name_2, $l_name_1, $l_name_2, $l_name_3 ) = explode(" ",$DATA['2']);
							
							if( $l_name_1 == "" && $l_name_2 == "" && $l_name_3 == "" ){
								$values[ $index_value ]['name'] 		=  $name_1;
								$values[ $index_value ]['last_name'] 	=  $name_2;
								
							}else if ( $name_1 == "นาย" || $name_1 == "นส" || $name_1 == "นางสาว" || $name_1 == "นาง" || $name_1 == "น.ส." || $name_1 == "นส." || $name_1 == "Mr." || $name_1 == "Mrs." || $name_1 == "Miss" || $name_1 == "Ms."  || $name_1 == "Dr." ) {
								$values[ $index_value ]['name'] 	 =  $name_2;
								$values[ $index_value ]['last_name'] =  trim($l_name_1." ".$l_name_2." ".$l_name_3);
								
							}else {
								$values[ $index_value ]['name'] 	 =  $name_2;
								$values[ $index_value ]['last_name'] =  trim($l_name_1." ".$l_name_2." ".$l_name_3);
								
							}
							
						$values[ $index_value ]['email'] 				= $DATA['3'];
						$values[ $index_value ]['tel'] 					= $DATA['4'];
						
						
						list( $mr_workplace01, $mr_workplace02, ) = explode("|",$DATA['5']);
						$values[ $index_value ]['mr_workplace'] 		= $DATA['5'];
						$values[ $index_value ]['mr_workplace1'] 		= $mr_workplace01;
						$values[ $index_value ]['floor_name'] 			= $mr_workplace02;
						
						
						//$floor_ID = (isset($floor[$mr_workplace02]))?$floor[$mr_workplace02]:'';
					    //
						//if($floor_ID!=''){
						//	echo $DATA['5'].'>>>ว่าง';
						//}
						
						
						
						
						list( $mr_workarea1, $mr_workarea2, ) = explode("|",$DATA['6']);
							$values[ $index_value ]['mr_workarea1'] 		= $mr_workarea1;
							$values[ $index_value ]['mr_workarea2'] 		= $mr_workarea2;
							$values[ $index_value ]['mr_workarea'] 			= $DATA['6'];
							$values[ $index_value ]['department_code'] 		= $DATA['7'];
							$values[ $index_value ]['department_name'] 		= $DATA['8'];
							$values[ $index_value ]['cost_id'] 				= $DATA['9'];
							
							$values[ $index_value ]['mr_position_code'] 	= $DATA['10'];
							$values[ $index_value ]['mr_position_name'] 	= $DATA['11'];
							
							$values[ $index_value ]['mr_branch_code'] 	  	= $DATA['12'];
							$values[ $index_value ]['mr_branch_name'] 	  	= $DATA['13'];
				
							
						
					}



				$index_value++;	
				//echo print_r($values);	
				//exit();
			}	

			// echo print_r($values);	
			// exit();
			
//exit; 		
		//	foreach( $values as $key => $vale){
		//		if($key==51){
		//			exit();	
		//		}
		//		echo '<pre>'.print_r( $vale, true).'</pre>';
		//	}
			//echo '<pre>'.print_r( $value, true).'</pre>';
			
				
			//echo '<pre>'.print_r( $DATA[0], true).'</pre>';
			//exit();		
			$val = array();
			$sql = " ";
			$num = 0;
			$date_import = date('Y-m-d');
			//echo '<pre>'.print_r( $DATA['0'], true).'</pre>';
			
			if ( $DATA['0'] != "D"  ){
				foreach( $values as $key => $vale){

					foreach($vale as $key_data =>$val_data){
						//$vale[$key_data] 	=  	preg_quote($val_data, '/');
					}




					$depart_id = array_search($vale['department_code'], array_column($department_data, 'mr_department_code'));
						//$mr_department_id = $department_data[$depart_id]['mr_department_id'];
						//$mr_floor_id = $department_data[$depart_id]['mr_floor_id'];
						//echo '<pre>'.print_r( $vale['type'], true).'</pre>';
						$branch_ID='';
						$position_id='';
						$department_code='';
						$floor_ID='';
						$mr_position_code = substr($vale['mr_position_code'], 0, 6);
						$branch_ID = (isset($branch[$vale['mr_branch_code']]))?$branch[$vale['mr_branch_code']]:null;
						$position_id = (isset($position[$mr_position_code]))?$position[$mr_position_code]:null;
						
						$department_code = (isset($department[$vale['department_code']]))?$department[$vale['department_code']]:null;
						$floor_ID = (isset($floor[$vale['floor_name']]))?$floor[$vale['floor_name']]:null;
						
						if($branch_ID=='' and $vale['mr_branch_code']!='' and $vale['mr_branch_name']  != ''){
							$saveB['mr_branch_code']		=$vale['mr_branch_code'];
							$saveB['mr_branch_name']		=$vale['mr_branch_name'];
							$branch_ID = $branchDao ->save($saveB);
							$branch[$vale['mr_branch_code']] = $branch_ID;
						}
						
						if($position_id=='' and $vale['mr_position_code']!='' and $vale['mr_position_name']  != ''){
							$mr_position_code = substr($vale['mr_position_code'], 0, 6);
							$saveP['mr_position_code']		=$mr_position_code;
							$saveP['mr_position_name']		=$vale['mr_position_name'];
							$position_id = $positionDao->save($saveP);
							$position[$mr_position_code] = $position_id;
							
						}
					
						
						if ( $department_code == "" ){						
							$department_data_save['mr_department_name'] = $vale['department_name'];
							$department_data_save['mr_department_code'] = $vale['department_code'];
							$department_data_save['mr_department_floor'] = $vale['floor_name'];
							$department_data_save['mr_floor_id'] = $floor_ID;
							
							$department_code = $departmentDao->save($department_data_save);
							$department[$vale['department_code']] = $department_code;
							
						}
						
						//echo '<pre>'.print_r( $department_code, true).'</pre>';
						//echo '<pre>'.print_r( $floor_ID, true).'</pre>';
						//exit();
						
						
						if( trim($vale['type']) == "A" ){
							//echo '<pre>'.print_r( $vale, true).'</pre>';
							$history_data=array();
							$emp_data_save=array();
							if( !in_array($vale['id'], $employee_data) ){
								
								//$mr_cost_id = $costDao->fetchCostByCostCode( $vale['cost_id'] );
								//echo '<pre>'.print_r( $vale, true).'</pre>';
								//$sql = 'INSERT INTO `mr_emp`( `mr_emp_name`, `mr_emp_lastname`, `mr_emp_code`, `mr_emp_tel`, `mr_emp_email`, `mr_department_id`, `mr_floor_id` , `mr_cost_id` , `mr_date_import` ) VALUES ';
								//$sql .= "( '".$vale['name']."' , '".$vale['last_name'] ."','". $vale['id'] ."','". $vale['tel'] ."','". $vale['email'] ."',". $mr_department_id .",". $mr_floor_id .",". $mr_cost_id .",'". $date_import ."');";
								//$employeeDao->ImportEmp($sql);
								
								$emp_data_save['mr_emp_name'] 			= $vale['name'];
								$emp_data_save['mr_emp_lastname'] 		= $vale['last_name'];
								$emp_data_save['mr_emp_code'] 			= $vale['id'];
								$emp_data_save['mr_emp_tel'] 			= $vale['tel'];
								$emp_data_save['mr_emp_email'] 			= $vale['email'];
								$emp_data_save['mr_department_id'] 		= $department_code;
							if($floor_ID!=''){
								$emp_data_save['mr_floor_id'] 			= $floor_ID;
							}
							
							if($branch_ID!=''){
								$emp_data_save['mr_branch_id'] 			= $branch_ID;
							}
							if($position_id!=''){
								$emp_data_save['mr_position_id'] 			= $position_id;
							}
 								$emp_data_save['mr_date_import']		= $date_import;
								
								$emp_data_save['mr_workplace']			= $vale['mr_workplace']; 
								$emp_data_save['mr_workarea']			= $vale['mr_workarea']; 	
								
								$emp_id = $employeeDao->save($emp_data_save);
								
								
								$history_data['mr_department_id'] 		= $department_code;
								if($vale['cost_id']!=''){
									//$history_data['mr_cost_id'] 			= $vale['cost_id'];
								}
								$history_data['mr_emp_id'] 				= $emp_id;
								$history_data['action'] 				= $vale['type'];
								$historyDao->save($history_data);
								
								$num++;
							
							}else{
									$history_data=array();
									$data_update=array();
									//$mr_cost_id = $costDao->fetchCostByCostCode( $vale['cost_id'] );
									$data_update['mr_emp_name'] 		= $vale['name'];
									$data_update['mr_emp_lastname']		= $vale['last_name'];
									$data_update['mr_emp_tel']			= $vale['tel'];
									$data_update['mr_emp_email']		= $vale['email'];
									$data_update['mr_department_id']	= $department_code;
									//$data_update['mr_floor_id']		  	     = $mr_floor_id;
									
									if($branch_ID!=''){
										//$data_update['mr_branch_id'] 			= $branch_ID;
									}else{
										//$data_update['mr_branch_id'] 			= '';
									}
									if($position_id!=''){
										$data_update['mr_position_id'] 			= $position_id;
									}else{
										$data_update['mr_position_id'] 			= null;
									}
									
									if($vale['cost_id']!=''){
										//$data_update['mr_cost_id']			= $vale['cost_id'];
									}
									$data_update['mr_date_import']		= $date_import;
									$data_update['mr_workplace']		= $vale['mr_workplace']; 
									$data_update['mr_workarea']			= $vale['mr_workarea']; 	
									//echo '<pre>'.print_r( $data_update, true).'</pre>';
									
									
									
									if(!in_array($vale['id'], $user_data) ){
										$data_update['mr_floor_id']		  	     = $mr_floor_id;
										if($branch_ID!=''){
										    $data_update['mr_branch_id'] 			= $branch_ID;
										}
										
									}
									$emp_id_update = $employeeDao->updateEmpByID($data_update, $vale['id']);
									if($emp_id_update!=''){
										$history_data['mr_department_id'] 		= $department_code;
										if($vale['cost_id']!=''){
											//$history_data['mr_cost_id'] 			= $vale['cost_id'];
										}
										$history_data['mr_emp_id'] 				= $emp_id_update;
										$history_data['action'] 				= $vale['type'];
										$historyDao->save($history_data);
									}
									$num++;
								
							}
						}else if ( $vale['type'] == "C" ){
							$history_data=array();
							$data_update=array();
							//$mr_cost_id = $costDao->fetchCostByCostCode( $vale['cost_id'] );
							$data_update['mr_emp_name'] 		= $vale['name'];
							$data_update['mr_emp_lastname']		= $vale['last_name'];
							$data_update['mr_emp_tel']			= $vale['tel'];
							$data_update['mr_emp_email']		= $vale['email'];
							$data_update['mr_department_id']	= $department_code;
							//$data_update['mr_floor_id']		  	= $mr_floor_id;
							
							if($branch_ID!=''){
								//$data_update['mr_branch_id'] 			= $branch_ID;
							}else{
								//$data_update['mr_branch_id'] 			= '';
							}
							if($position_id!=''){
								$data_update['mr_position_id'] 			= $position_id;
							}else{
								$data_update['mr_position_id'] 			= null;
							}
							
							if($vale['cost_id']!=''){
								//$data_update['mr_cost_id']			= $vale['cost_id'];
							}
							$data_update['mr_date_import']		= $date_import;
							$data_update['mr_workplace']		= $vale['mr_workplace']; 
							$data_update['mr_workarea']			= $vale['mr_workarea']; 	
							//echo '<pre>'.print_r( $data_update, true).'</pre>';
							if(!in_array($vale['id'], $user_data) ){
								$data_update['mr_floor_id']		  	     = $mr_floor_id;
								if($branch_ID!=''){
								    $data_update['mr_branch_id'] 			= $branch_ID;
								}
								
							}
									
							$emp_id_update = $employeeDao->updateEmpByID($data_update, $vale['id']);
							if($emp_id_update!=''){
							$history_data['mr_department_id'] 		= $department_code;
							if($vale['cost_id']!=''){
							//	$history_data['mr_cost_id'] 			= $vale['cost_id'];
							}
							$history_data['mr_emp_id'] 				= $emp_id_update;
							$history_data['action'] 				= $vale['type'];
							$historyDao->save($history_data);
							}
							$num++;
													
						}
					//echo $sql;
					//echo '<pre>'.print_r( $sql, true).'</pre>';
				}
				
			}else if ( $DATA[0] == "D" ){
				//echo '<pre>'.print_r( $DATA, true).'</pre>';
				//exit();
				$index_valueD = 0;
				$valueD = array();
				
				foreach($text as $valueD){
					$DATA_D 				= $valueD;
					
					$resign = $employeeDao->checkEmpResign( $DATA_D['1'] );
					
					//echo '<pre>'.print_r( $resign, true).'</pre>';
					//exit();
					if ( $resign != "Resign" ) {
						$data_delete['mr_date_import'] 	= $date_import;
						$data_delete['mr_emp_email'] 	= "Resign";
						$data_delete['mr_emp_code'] 	= "Resign";
						$data_delete['old_emp_code'] 	= $DATA_D['1'];
						
						
						$emp_id_delete = $employeeDao->updateEmpByID($data_delete, $DATA_D['1']);
						// echo print_r($emp_id_delete,true) ;
						// exit;
						$userDao->Update_delete_User( $DATA_D['1'] );
						if($emp_id_delete!=0){
							$history_data['mr_emp_id'] 				= $emp_id_delete;
						}
							$history_data['action'] 				= $DATA_D['0']."::".$DATA_D['1'];
							$historyDao->save($history_data);
							$index_valueD++;
							$num++;
						
					}
					
				}
			}
		//echo '<pre>'.print_r( $sql, true).'</pre>';
		
		$result = $num;
	}
		
}

 echo $result;
 
?>
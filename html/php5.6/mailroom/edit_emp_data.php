<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Position.php';

/* Check authentication */
$auth = new Pivot_Auth();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}



$req 				= new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$branch_dao 			= new Dao_Branch();
$position_dao 			= new Dao_Position();

//$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$branch = $branch_dao->fetchAll();
$department_data = $departmentDao->fetchAll();
$floor_data = $floorDao->getFloorMess();
$position_data = $position_dao->fetchAll();

$work_inout_Dao = new Dao_Work_inout();

$emp_id = $req->get('id');

$emp_id = base64_decode(urldecode($emp_id));

$emp_data = $employeeDao->getEmpProfileBy_emp_id($emp_id);

$filter['mr_branch_id'] = $emp_data['mr_branch_id'];
$floor_data2 = $floorDao->load_fioor_data($filter);



//echo "---<pre>".print_r($emp_data,true)."</pre>---";
// echo "---<pre>".print_r($emp_id,true)."</pre>---";
// echo "---<pre>".print_r($emp_data,true)."</pre>---";

//exit;

$template = Pivot_Template::factory('mailroom/edit_emp_data.tpl');
$template->display(array(
	//'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	//'success' => $success,
	'department_data' => $department_data,
	'userRoles' => $userRoles,
	//'users' => $users,
	'empdata' => $emp_data,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	//'userID' => $user_id,
	//'empID' => $emp_id,
	'branch' => $branch,
	'floor_data' => $floor_data,
	'floor_data2' => $floor_data2,
	'position_data' => $position_data,
	//'alert' => $alert

));
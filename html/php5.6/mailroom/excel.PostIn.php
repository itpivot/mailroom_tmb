<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_post.php';
require_once 'PHPExcel.php';
require_once 'Dao/Send_work.php';
error_reporting(E_ALL & ~E_NOTICE);

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
$work_postDao 	= new Dao_Work_post();

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$alert 			= '';

$data  			= array();
$round 	= $req->get('round_printreper');
$date 	= $req->get('date_report');


	

//echo '<pre>'.print_r($date_report,true).'</pre>';

//exit;
if(preg_match('/<\/?[^>]+(>|$)/', $round_print)) {
	$alert = "
	$.confirm({
		title: 'Alert!',
		content: 'เกิดข้อผิดพลาด!',
		buttons: {
			OK: function () {
				location.href = 'create_work_post_in.php';
				}
			}
		});
	";
}else if(preg_match('/<\/?[^>]+(>|$)/', $date_report)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'create_work_post_in.php';
			}
		}
	});
		
	";
}else{
	$sql='SELECT
				w_m.mr_work_main_id,
				DATE_FORMAT(w_m.sys_timestamp, "%Y-%m-%d") as d_send,
				w_m.mr_work_barcode,
				w_m.mr_work_remark,
				w_p.mr_address,
				w_p.num_doc,
				w_p.mr_post_price,
				w_p.mr_post_totalprice,
				w_m.quty,
				pv.mr_provinces_name,
				w_p.mr_send_emp_detail,
				w_io.mr_floor_id,
				f.name as floor_name,
				w_p.mr_cus_tel,
				w_p.mr_send_emp_id,
				emp_re.mr_emp_id as mr_resive_emp_id,
				w_p.mr_cus_name as sendder_name,
				w_p.mr_cus_lname as sendder_lname,
				concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_send,
				w_p.mr_address as sendder_address,
				w_p.mr_send_department_id as mr_resive_department_id,
				dep.mr_department_name as dep_resive,
				dep.mr_department_code as dep_code_resive,
				emp_re.mr_emp_code ,
				emp_re.mr_emp_name,
				emp_re.mr_emp_lastname,
				concat(emp_re.mr_emp_code ," : " , emp_re.mr_emp_name,"  " , emp_re.mr_emp_lastname,"  ",dep.mr_department_name) as name_resive,
				r.mr_round_name,
				s.mr_status_id,
				s.mr_status_name,
				w_p.mr_type_post_id,
				t_p.mr_type_post_name
			FROM
				mr_work_main w_m
				LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
				LEFT join  mr_work_inout w_io on(w_io.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_floor f on(f.mr_floor_id = w_io.mr_floor_id)
				LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
				LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
				LEFT join  mr_emp emp_re on(emp_re.mr_emp_id = w_io.mr_emp_id)
				LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
				LEFT join  mr_provinces pv on(pv.mr_provinces_id = w_p.mr_province_id)
			WHERE
				w_m.sys_timestamp like"'.$date.'%"
				and w_m.mr_type_work_id = 5
				and w_m.mr_status_id != 6
			   ';

		if($round_print!='') {
			 $sql.='
				and w_m.mr_round_id = 	'.$round.' ';
				}

		$sql.='
		group by w_m.mr_work_main_id 
		order by w_m.mr_work_main_id asc';	
		


		
		
	
		$data_qury 			= $send_workDao->select($sql);
	//$data_qury	= $work_postDao->getdatatoday_post_in($date,$round);
		// echo count($data);
		$new_data = array();
		foreach($data_qury as $key=>$val){
			$new_data[$key][] = ($key+1);
			$new_data[$key][] = $val['d_send'];
			$new_data[$key][] = $val['mr_work_barcode'];
			$new_data[$key][] = $val['num_doc'];
			$new_data[$key][] = $val['dep_code_resive'].' : '.$val['dep_resive'];
			$new_data[$key][] = $val['floor_name'];
			$new_data[$key][] = $val['mr_round_name'];
			$new_data[$key][] = $val['sendder_name'].' '.$val['sendder_lname'];;
			$new_data[$key][] = $val['mr_work_remark'];
			$new_data[$key][] = $val['mr_emp_name']." ".$val['mr_emp_lastname'];
			$new_data[$key][] = $val['quty'];
		}
}









$arr_report1	= array();
$sheet1 		= 'PostInDetail';
$headers1  		= array(
	 'ลำดับ',                                                  
	 'วันที่',                                                  
	 'เลขที่เอกสาร',                                       
	 'เลขที่ ปณ.',                                       
	 'หน่วยงานผู้รับ',                                       
	 'ชั้นผู้รับ',                                       
	 'รอบ',                                       
	 'ผู้ส่ง',                                                               
	 'หมายเหตุ',  
	 'ผู้รับ',           
	 'จำนวน',           
);

$file_name = 'TMB-Report-Thai-PostIn'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($new_data as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
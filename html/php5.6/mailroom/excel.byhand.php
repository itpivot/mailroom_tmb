<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_post.php';
require_once 'PHPExcel.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Work_byhand.php';
error_reporting(E_ALL & ~E_NOTICE);

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
$work_postDao 	= new Dao_Work_post();
$work_byhandDao 	= new Dao_Work_byhand();

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$alert 			= '';

$data  			= array();
$round 	= $req->get('round_printreper');
$date 	= $req->get('date_report');


	

//echo '<pre>'.print_r($date_report,true).'</pre>';

//exit;
if(preg_match('/<\/?[^>]+(>|$)/', $round_print)) {
	$alert = "
	$.confirm({
		title: 'Alert!',
		content: 'เกิดข้อผิดพลาด!',
		buttons: {
			OK: function () {
				location.href = 'report.byhand.php';
				}
			}
		});
	";
}else if(preg_match('/<\/?[^>]+(>|$)/', $date_report)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'report.byhand.php';
			}
		}
	});
		
	";
}else{


	$form_data = array();
	$date_start 		= $req->get('date_1');
	$date_end 		= $req->get('date_2');
	$form_data['date_report1'] = $date_start ;
	$form_data['date_report2'] = $date_end ;
	$form_data['mr_type_work_id'] = 4 ;

	
$data_qury = $work_byhandDao->getreportByval($form_data);
// echo '<pre>---'.print_r($data_qury,true).'</pre>';
// exit;

	//$data_qury	= $work_postDao->getdatatoday_post_in($date,$round);
		// echo count($data);
		$new_data = array();
		foreach($data_qury as $key=>$val){
			$new_data[$key][] = ($key+1);
			$new_data[$key][] = $val['mr_work_barcode'];
			$new_data[$key][] = $val['mr_emp_code'].':'.$val['send_name'].' '.$val['send_lastname'];
			$new_data[$key][] = $val['branch_code_send'].':'.$val['branch_name_send'];
			$new_data[$key][] = "'".$val['cost_code_send'];
			$new_data[$key][] = "'".$val['dep_code_send4'];
			$new_data[$key][] = $val['dep_code_send'].' :'.$val['dep_send'];;
			$new_data[$key][] = $val['mr_cus_name'].' '.$val['mr_cus_lname'];
			$new_data[$key][] = $val['d_send'];
			$new_data[$key][] = $val['quty'];
			$new_data[$key][] = $val['mr_address'];
			$new_data[$key][] = $val['mr_sub_districts_name'];
			$new_data[$key][] = $val['mr_districts_name'];
			$new_data[$key][] = $val['mr_provinces_name'];
			$new_data[$key][] = $val['mr_post_code'];
		}
}









$arr_report1	= array();
$sheet1 		= 'Detail';
$headers1  		= array(
	 'ลำดับ',                                                    
	 'เลขที่เอกสาร',                                       
	 'ผู้ส่ง',                                       
	 'สาขาผู้ส่ง',                                       
	 'รหัสค่าใช้จ่าย',                                       
	 'รหัสหน่วยงาน4',                                       
	 'หน่วยงาน',                                       
	 'ชื่อผู้รับ',                                                                  
	 'วันที่สั่งงาน',  
	 'จำนวน',           
	 'ที่อยู่',           
	 'ตำบล',           
	 'อำเภอ',           
	 'จังหวัด',           
	 'ปณ.',           
);

$file_name = 'TMB-Report-By-Hand'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($new_data as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
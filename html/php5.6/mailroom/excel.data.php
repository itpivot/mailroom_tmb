<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Employee.php'; 
require_once 'Dao/Branch.php'; 
require_once 'Dao/Department.php'; 
require_once 'Dao/Floor.php'; 
require_once 'Dao/Round.php'; 

require_once 'PHPExcel.php';

include_once('xlsxwriter.class.php');
header('Content-Type: text/html; charset=utf-8');



ini_set("memory_limit", "-1");
ini_set('max_execution_time', 0);

$time_start = microtime(true);

// echo "---";
// exit;
/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();
$employeeDao 		= new Dao_Employee();
$branchDao 			= new Dao_Branch();
$departmentDao 		= new Dao_Department();
$floorDao 			= new Dao_Floor();
$roundDao 			= new Dao_Round();

 
$data_search		    		=  json_decode($req->get('params'),true);                                                  
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$employeedata 	= $employeeDao->getEmpDataUser_forExcel();
$branchdata 	= $branchDao->getBranchData_forExcel();
$departmentdata = $departmentDao->fetchAll();
$rounddata 		= $roundDao->fetchAll();
$floordata 		= $floorDao->getFloors_All_buildingJoin();


$employee_arr = array();
$user_arr = array();
foreach($employeedata as $i_e => $val_e){
	$employee_arr[$val_e['mr_emp_id']]  = $val_e; 
	$user_arr[$val_e['mr_user_id']]  = $val_e; 
}
$branch_arr = array();
foreach($branchdata as $i_b => $val_b){
	$branch_arr[$val_b['mr_branch_id']]  = $val_b; 
}
$department_arr = array();
foreach($departmentdata as $i_d => $val_d){
	$department_arr[$val_d['mr_department_id']]  = $val_d; 
}
$floor_arr = array();
foreach($floordata as $i_f => $val_f){
	$floor_arr[$val_f['mr_floor_id']]  = $val_f; 
}
$round_arr = array();
foreach($rounddata as $i_r => $val_r){
	$round_arr[$val_r['mr_round_id']]  = $val_r; 
}

function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}
//$data1 			= $work_inoutDao->searchMailroom_upQury2021_12_23($data_search);




// A function that will create the initial setup
// for the progress bar: You can modify this to
// your liking for visual purposes:
	function create_progress() {
		// First create our basic CSS that will control
		// the look of this bar:
		echo "
	  <style>
	  #text {
		position: absolute;
		top: 100px;
		left: 50%;
		margin: 0px 0px 0px -150px;
		font-size: 18px;
		text-align: center;
		width: 300px;
	  }
		#barbox_a {
		position: absolute;
		top: 130px;
		left: 50%;
		margin: 0px 0px 0px -160px;
		width: 304px;
		height: 24px;
		background-color: black;
	  }
	  .per {
		position: absolute;
		top: 130px;
		font-size: 18px;
		left: 50%;
		margin: 1px 0px 0px 150px;
		background-color: #FFFFFF;
	  }
	  
	  .bar {
		position: absolute;
		top: 132px;
		left: 50%;
		margin: 0px 0px 0px -158px;
		width: 0px;
		height: 20px;
		background-color: #0099FF;
	  }
	  
	  .blank {
		background-color: white;
		width: 300px;
	  }
	  </style>
	  ";
	  
		// Now output the basic, initial, XHTML that
		// will be overwritten later:
		echo "
	  <div id='text'>Script Progress</div>
	  <div id='barbox_a'></div>
	  <div class='bar blank'></div>
	  <div class='per'>0%</div>
	  ";
	  
		// Ensure that this gets to the screen
		// immediately:
		flush();
	  }
	  
	  // A function that you can pass a percentage as
	  // a whole number and it will generate the
	  // appropriate new div's to overlay the
	  // current ones:
	  
	  function update_progress($percent) {
		// First let's recreate the percent with
		// the new one:

		echo "<div class='per'>{$percent}
		  %</div>";
	  
		// Now, output a new 'bar', forcing its width
		// to 3 times the percent, since we have
		// defined the percent bar to be at
		// 300 pixels wide.
		echo "<div class='bar' style='width: ",
		  $percent * 3, "px'></div>";
	  
		// Now, again, force this to be
		// immediately displayed:
		flush();
	  }
	  
	  // Ok, now to use this, first create the
	  // initial bar info:
	  create_progress();
	  
	  // Now, let's simulate doing some various
	  // amounts of work, and updating the progress
	  // bar as we go. The usleep commands will
	  // simulate multiple lines of code
	  // being executed.
	
		//$count 			= $work_inoutDao->Mailroom_count_report($data_search);
$start = $data_search['start_date'];
$end = $data_search['end_date'];
$start2 = $start;
$end2 = date('Y-m-d',strtotime($start . "+15 days"));
$i = 0;
$ss_serch=array();

while ($end>$end2) {
	if($i==0){
		//echo $start2."<<<<<<<<<<<<>>>>>>>>>>>>>>>>>".$end2."<br>";
		
		$ss_serch[$i]['start_date']	 	= $start2;
		$ss_serch[$i]['end_date'] 		= $end2;

		$start2 = date('Y-m-d',strtotime($start2 . "+16 days"));
		$end2 = date('Y-m-d',strtotime($end2 . "+15 days"));

		
		$i++;
	}else{
		$start2 = date('Y-m-d',strtotime($start2 . "+15 days"));
		$end2 = date('Y-m-d',strtotime($end2 . "+15 days"));
	}
	
	if($end2>$end){
		$end2 = $end;
	}
		$ss_serch[$i]['start_date']	 	= $start2;
		$ss_serch[$i]['end_date'] 		= $end2;

	//echo $start2."<<<<<<<<<<<<>>>>>>>>>>>>>>>>>".$end2."<br>";
	//echo "<br>";
	//echo "---------------<br>";
	$i++;
}







	  
	  // Now that you are done, you could also
	  // choose to output whatever final text that
	  // you might wish to, and/or to redirect
	  // the user to another page.






//$data 			= $work_inoutDao->Mailroom_Quryreport($data_search);
//$log 			= $work_logDao->Mailroom_Qurylogsreport($data_search);



// echo '>>>>>>>>>>..<pre>'.print_r($data1[0],true);
// echo '>>>>>>>>>>..<pre>'.print_r($floor_arr,true);


$round_resiveName 	=array();//mr_work_main_id
$round_resiveDate 	=array();//mr_work_main_id
$round_resiveVal 	=array();//mr_work_main_id
if(!empty($data)){
	if(!function_exists('array_column')){
		$myfield_arr 	= array_column_($data, 'mr_work_main_id');
	}else{
		$myfield_arr 	= array_column($data, 'mr_work_main_id');
	}
	$all_main_id 	= implode(",",$myfield_arr);
	$round_resive   = $work_inoutDao->count_mr_round_resive_work($all_main_id);
	foreach($round_resive as $val){
		$round_resiveVal[$val['mr_work_main_id']] = $val['count_qty'];
		$round_resiveName[$val['mr_work_main_id']] = $val['mr_round_name'];
		$round_resiveDate[$val['mr_work_main_id']] = $val['sysdate'];
	}
}

// $time_end = microtime(true);
// $time = $time_end - $time_start;
// echo '>>>>>>>>>>..'.count($data)."<br>";
// echo '>>>>>>>>>>..'.$time;
// echo '>>>>>>>>>>..<pre>'.print_r($round_resiveDate,true);
// exit;


$arr_report1	= array();
$sheet1 		= 'Detail';
$headers1  		= array(
	 'NO',                                       
	 'Barcode',   
	 'ประเภทการส่ง',                          
	 'สถานะงาน',                                                       
	 'ชื่อผู้ส่ง',                                 
	 'สาขาผู้ส่ง',                              
	 'หน่วยงานผู้ส้ง',                              
	 'อาคารผู้ส่ง',                              
	 'ชั้นผู้ส่ง',                              
	 'ประเภทสาขาผู้ส่ง',    
	 'ชื่อผู้รับ',                                 
	 'สาขาผู้รับ',                              
	 'หน่วยงานผู้รับ',                              
	 'อาคารผู้รับ',                              
	 'ชั้นผู้รับ',                              
	 'ประเภทสาขาผู้รับ',
	 'ชื่อเอกสาร',
	 'วันที่ส่ง',                    
	 'วันที่ห้อง Mailroom รับ',                    
	 'วันที่ห้อง Mailroom ส่งออก',                    
	 'วันที่สำเร็จ',                                                       
	 'หมายเหตุ',            
	 'ความพึงพอใจ',
	 'หมายเหตุความพึงพอใจ',
	 'ส่งจาก',
	 'พนักงานรับ',
	 'พนักงานส่ง',
	 'รอบ',
	 'รอบ-วันที่',
	 'รอบการพิมพ์ใบคุมส่งออก',
	 'จำนวน'
);

ob_start();
  $filename = 'export';
  $delimiter = ';';
  $enclosure = '"';
  header("Content-disposition: attachment; filename=TMB_Report_Y.csv");
  // Tells to the browser that the content is a csv file
  header("Content-Type: text/csv");

  $df = fopen('../download/TMB_Report_Y.csv', 'w');
  fputs($df, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
  fputcsv($df, $headers1);
  //fputcsv($df, $columnNames);


$indexs = 2;		
$count_n = count($ss_serch);
if($count_n > 0){
	foreach($ss_serch as $end => $data){
		sleep(5);
		$progress = floor(($end*100)/$count_n);
		update_progress($progress);
		sleep(5);
		ob_flush(); 
		flush();

		$data 			= $work_inoutDao->Mailroom_Quryreport_payment($data);


		foreach($data as $keys => $vals) {
			$mr_type_work_id 			= $vals['mr_type_work_id'];
		
					$type_send = array(1,2,3);
					if (in_array($mr_type_work_id, $type_send)){
						$send_data 				= array();
						$dep_data 				= array();
						$emp_send_name 			='';
						$branch_type_send_name 	='';
						$dep_send_name 			='';
						$send_branch_id 			= $vals['send_branch_id'];
						$send_floor_id 			= $vals['send_floor_id'];
						if(isset($user_arr[$vals['send_user_id']])){
							$send_data 				= $user_arr[$vals['send_user_id']];
							$emp_send_name 			= $send_data['mr_emp_code'].': '.$send_data['mr_emp_name'].' '.$send_data['mr_emp_lastname'];
							
		
							if($send_floor_id  == ''){
								$send_floor_id  = $send_data['mr_floor_id'];
							}
							if($send_branch_id == ''){
								$send_branch_id = $send_data['mr_branch_id'];
							}
							if(isset($department_arr[$send_data['mr_department_id']])){
								$dep_data 		= $department_arr[$send_data['mr_department_id']];
								$dep_send_name 	= $dep_data['mr_department_code'].': '.$dep_data['mr_department_name'];
							}
						}
		
						$send_branch_data  = array();
						$send_branch_name = '';
						if(isset($branch_arr[$send_branch_id]) and $send_branch_id != ''){
							$send_branch_data = $branch_arr[$send_branch_id];
							$send_branch_name = $send_branch_data['mr_branch_code'].': '.$send_branch_data['mr_branch_name'];
							$branch_type_send_name 	= $send_branch_data['branch_type_name'];
						}
						$send_floor_data  = array();
						$send_floor_name = '';
						$send_building_name = '';
						$resive_mess_name = '';
						if(isset($floor_arr[$send_floor_id]) and $send_floor_id != ''){
							$send_floor_data 		= $floor_arr[$send_floor_id];
							$send_floor_name 		= $send_floor_data['name'];
							$send_building_name 	= $send_floor_data['mr_building_name'];
							if($send_floor_data['mr_emp_code']!=''){
								$resive_mess_name 		= $send_floor_data['mr_emp_code'].':  '.$send_floor_data['mr_emp_name'].' '.$send_floor_data['mr_emp_lastname'];
							}
						}
						
					}elseif($mr_type_work_id==4){
						$send_data 				= array();
						$dep_data 				= array();
						$emp_send_name 			='';
						$branch_type_send_name 	='';
						$dep_send_name 			='';
						$send_branch_id 		='';
						$send_floor_id 			='';
		
						if(isset($employee_arr[$vals['ht_send_emp_id']])){
							$send_data 				= $employee_arr[$vals['ht_send_emp_id']];
							$emp_send_name 			= $send_data['mr_emp_code'].': '.$send_data['mr_emp_name'].' '.$send_data['mr_emp_lastname'];
							
		
							if($send_floor_id  == ''){
								$send_floor_id  = $send_data['mr_floor_id'];
							}
							if($send_branch_id == ''){
								$send_branch_id = $send_data['mr_branch_id'];
							}
							if(isset($department_arr[$send_data['mr_department_id']])){
								$dep_data 		= $department_arr[$send_data['mr_department_id']];
								$dep_send_name 	= $dep_data['mr_department_code'].': '.$dep_data['mr_department_name'];
							}
						}
		
						$send_branch_data  = array();
						$send_branch_name = '';
						if(isset($branch_arr[$send_branch_id]) and $send_branch_id != ''){
							$send_branch_data = $branch_arr[$send_branch_id];
							$send_branch_name = $send_branch_data['mr_branch_code'].': '.$send_branch_data['mr_branch_name'];
							$branch_type_send_name 	= $send_branch_data['branch_type_name'];
						}
						$send_floor_data  = array();
						$send_floor_name = '';
						$send_building_name = '';
						$resive_mess_name = '';
						if(isset($floor_arr[$send_floor_id]) and $send_floor_id != ''){
							$send_floor_data 		= $floor_arr[$send_floor_id];
							$send_floor_name 		= $send_floor_data['name'];
							$send_building_name 	= $send_floor_data['mr_building_name'];
							if($send_floor_data['mr_emp_code']!=''){
								$resive_mess_name 		= $send_floor_data['mr_emp_code'].':  '.$send_floor_data['mr_emp_name'].' '.$send_floor_data['mr_emp_lastname'];
							}
						}
					}elseif($mr_type_work_id==5){
						$emp_send_name = $vals['mr_cus_name'].' '.$vals['mr_cus_lname'];
						$send_branch_name = $vals['mr_address']; 
						$dep_send_name = '';
						$send_floor_name = '';
						$branch_type_send_name = '';
					}elseif($mr_type_work_id==6){
						$send_data 				= array();
						$dep_data 				= array();
						$emp_send_name 			='';
						$branch_type_send_name 	='';
						$dep_send_name 			='';
						$send_branch_id 		='';
						$send_floor_id 			='';
		
						if(isset($employee_arr[$vals['tp_send_emp_id']])){
							$send_data 				= $employee_arr[$vals['tp_send_emp_id']];
							$emp_send_name 			= $send_data['mr_emp_code'].': '.$send_data['mr_emp_name'].' '.$send_data['mr_emp_lastname'];
							
		
							if($send_floor_id  == ''){
								$send_floor_id  = $send_data['mr_floor_id'];
							}
							if($send_branch_id == ''){
								$send_branch_id = $send_data['mr_branch_id'];
							}
							if(isset($department_arr[$send_data['mr_department_id']])){
								$dep_data 		= $department_arr[$send_data['mr_department_id']];
								$dep_send_name 	= $dep_data['mr_department_code'].': '.$dep_data['mr_department_name'];
							}
						}
		
						$send_branch_data  = array();
						$send_branch_name = '';
						if(isset($branch_arr[$send_branch_id]) and $send_branch_id != ''){
							$send_branch_data = $branch_arr[$send_branch_id];
							$send_branch_name = $send_branch_data['mr_branch_code'].': '.$send_branch_data['mr_branch_name'];
							$branch_type_send_name 	= $send_branch_data['branch_type_name'];
						}
						$send_floor_data  = array();
						$send_floor_name = '';
						$send_building_name = '';
						$resive_mess_name = '';
						if(isset($floor_arr[$send_floor_id]) and $send_floor_id != ''){
							$send_floor_data 		= $floor_arr[$send_floor_id];
							$send_floor_name 		= $send_floor_data['name'];
							$send_building_name 	= $send_floor_data['mr_building_name'];
							if($send_floor_data['mr_emp_code']!=''){
								$resive_mess_name 		= $send_floor_data['mr_emp_code'].':  '.$send_floor_data['mr_emp_name'].' '.$send_floor_data['mr_emp_lastname'];
							}
						}
					}
					
					
						/* ---------------------------------------------------------------------------- */
						/* ------------------------------------ END SEND DATA --------------------------*/
						/* ---------------------------------------------------------------------------- */
					$type_resive = array(1,2,3,5);
					if (in_array($mr_type_work_id, $type_resive)){
		
						$resive_data 				= array();
						$resive_dep_data 				= array();
						$emp_resive_name 			='';
						$branch_type_resive_name 	='';
						$resive_dep_name 			='';
						$resive_branch_id 			= $vals['resive_branch_id'];
						$resive_floor_id 			= $vals['resive_floor_id'];
						if(isset($employee_arr[$vals['resive_emp_id']])){
							$resive_data 				= $employee_arr[$vals['resive_emp_id']];
							$emp_resive_name 			= $resive_data['mr_emp_code'].': '.$resive_data['mr_emp_name'].' '.$resive_data['mr_emp_lastname'];
		
							if($resive_floor_id  == ''){
								$resive_floor_id  = $resive_data['mr_floor_id'];
							}
							if($resive_branch_id == ''){
								$resive_branch_id = $resive_data['mr_branch_id'];
							}
							if(isset($department_arr[$resive_data['mr_department_id']])){
								$resive_dep_data 		= $department_arr[$resive_data['mr_department_id']];
								$resive_dep_name 		= $resive_dep_data['mr_department_code'].': '.$resive_dep_data['mr_department_name'];
							}
						}
		
						$resive_branch_data  	= array();
						$resive_branch_name 	= '';
						if(isset($branch_arr[$resive_branch_id]) and $resive_branch_id != ''){
							$resive_branch_data 		= $branch_arr[$resive_branch_id];
							$resive_branch_name 		= $resive_branch_data['mr_branch_code'].': '.$resive_branch_data['mr_branch_name'];
							$branch_type_resive_name	= $resive_branch_data['branch_type_name'];
						}
						$resive_floor_data  = array();
						$resive_floor_name = '';
						$resive_building_name = '';
						$send_mess_name = '';
						if(isset($floor_arr[$resive_floor_id]) and $resive_floor_id != ''){
							$resive_floor_data 		= $floor_arr[$resive_floor_id];
							$resive_floor_name 		= $resive_floor_data['name'];
							$resive_building_name 		= $resive_floor_data['mr_building_name'];
							if($resive_floor_data['mr_emp_code']!=''){
								$send_mess_name 		= $resive_floor_data['mr_emp_code'].':  '.$resive_floor_data['mr_emp_name'].' '.$resive_floor_data['mr_emp_lastname'];
							}
						}
					}elseif($mr_type_work_id==4){
						$emp_resive_name 			= $vals['bh_mr_cus_name'].' '.$vals['bh_mr_cus_lname'];
						$resive_branch_name	 		= $vals['bh_mr_address']; 
						$resive_dep_name = '';
						$resive_floor_name = '';
						$branch_type_resive_name = '';
					}elseif($mr_type_work_id==6){
						$emp_resive_name 			= $vals['mr_cus_name'].' '.$vals['mr_cus_lname'];
						$resive_branch_name	 		= $vals['mr_address']; 
						$resive_dep_name 			= '';
						$resive_floor_name 			= '';
						$branch_type_resive_name 	= '';
		
		
					}
					/* ---------------------------------------------------------------------------- */
					/* ------------------------------------ END RESIVE DATA --------------------------*/
					/* ---------------------------------------------------------------------------- */
		
					//$sys_timestamp 		= ($vals['sys_timestamp']!='')?date('Y-m-d',strtotime($vals['sys_timestamp'])):'';
					//$time_mail 			= ($vals['time_mail']!='')?date('Y-m-d',strtotime($vals['time_mail'])):'';
					//$time_succ 			= ($vals['time_succ']!='')?date('Y-m-d',strtotime($vals['time_succ'])):'';
					$sys_timestamp 		= ($vals['sys_timestamp']!='')?$vals['sys_timestamp']:'';
					//$time_mail 			= ($vals['time_mail']!='')?$vals['time_mail']:'';
					$time_mail_send 			= ($vals['date_rrrsive_2']!='')?$vals['date_rrrsive_2']:'';
					//$time_succ 			= ($vals['time_succ']!='')?$vals['time_succ']:'';

					$sr = explode(',',$vals['log_st_id']);
					$time = explode(',',$vals['log_time']);
					$k_st1 = array_search(3, $sr);
					if($k_st1==''){
						$k_st1 = array_search(10, $sr);
					}
					
					$k_st2 = array_search(5, $sr);
					if($k_st2==''){
						$k_st2 = array_search(12, $sr);
					}


					$time_mail 			= ($k_st1 != '')?$time[$k_st1]:'';
					//$time_mail_send 	= ($vals['date_rrrsive_2']!='')?$vals['date_rrrsive_2']:'';
					$time_succ 			= ($k_st2 !='')?$time[$k_st2]:'';

					
					
					$rate_send = $vals['rate_send'];
					$rate_send_txt = '';;
					if($rate_send == ''){
						$rate_send_txt = '';
					}elseif($rate_send == 5){
						$rate_send_txt = 'พึ่งพอใจ';
					}else{
						$rate_send_txt = 'ไม่พึ่งพอใจ';
					}
		
					$type_send = array(1,2,3);
					$mr_round_id 	= '';
					$mr_round_name 	= '';
					if (in_array($mr_type_work_id, $type_send)){
						$mr_round_id = $vals['r_rrrsive_id'];
						$date_rrrsive 			= ($vals['date_rrrsive']!='')?date('Y-m-d',strtotime($vals['date_rrrsive'])):'';
						$date_rrrsive_2 			= $vals['date_rrrsive_2'];
					}else{
						$mr_round_id = $vals['mr_round_id'];
						$date_rrrsive 			= ($vals['sys_timestamp']!='')?date('Y-m-d',strtotime($vals['sys_timestamp'])):'';
						$date_rrrsive_2 			= $vals['sys_timestamp'];
		
					}
					if(isset($round_arr[$mr_round_id]) and $mr_round_id!= ''){
						$mr_round_name = $round_arr[$mr_round_id]['mr_round_name'];//r_rrrsive_id//mr_round_id
					}else{
						$date_rrrsive = '';
						$date_rrrsive_2 = '';
					}
		
		
					$emp_mess_resive = '';
					$emp_mess_send = '';
					
		
					if($mr_type_work_id == 1){
						$emp_mess_resive = $resive_mess_name;
						$emp_mess_send = $send_mess_name;
					}elseif($mr_type_work_id == 2){
						$mr_user_role_id = $resive_data['mr_user_role_id'];
						if($mr_user_role_id == 1 or $send_branch_id == 1383){
							$emp_mess_resive = $resive_mess_name;
						}else{
							if(isset($user_arr[$vals['resive_messenger_user_id']]) and $vals['resive_messenger_user_id'] != ''){
								$emp_mess_arr = $user_arr[$vals['resive_messenger_user_id']];
								$emp_mess_resive 		= $emp_mess_arr['mr_emp_code'].':  '.$emp_mess_arr['mr_emp_name'].' '.$emp_mess_arr['mr_emp_lastname'];
							}
						}
		//
						if(isset($user_arr[$vals['send_messenger_user_id']]) and $vals['send_messenger_user_id'] != ''){
							$emp_mess_arr = $user_arr[$vals['send_messenger_user_id']];
							$emp_mess_send 		= $emp_mess_arr['mr_emp_code'].':  '.$emp_mess_arr['mr_emp_name'].' '.$emp_mess_arr['mr_emp_lastname'];
						}
					
					}elseif($mr_type_work_id == 3){
						if(isset($user_arr[$vals['resive_messenger_user_id']]) and $vals['resive_messenger_user_id'] != ''){
							$emp_mess_arr = $user_arr[$vals['resive_messenger_user_id']];
							$emp_mess_resive 		= $emp_mess_arr['mr_emp_code'].':  '.$emp_mess_arr['mr_emp_name'].' '.$emp_mess_arr['mr_emp_lastname'];
						}
						$emp_mess_send = $send_mess_name;
					}elseif($mr_type_work_id == 4){
						$emp_mess_resive = $resive_mess_name;
						if(isset($user_arr[$vals['send_messenger_user_id']]) and $vals['send_messenger_user_id'] != ''){
							$emp_mess_arr = $user_arr[$vals['send_messenger_user_id']];
							$emp_mess_send 		= $emp_mess_arr['mr_emp_code'].':  '.$emp_mess_arr['mr_emp_name'].' '.$emp_mess_arr['mr_emp_lastname'];
						}
					}elseif($mr_type_work_id == 5){
						$emp_mess_send = $send_mess_name;
					}elseif($mr_type_work_id == 6){
						$emp_mess_resive = $resive_mess_name;
					}
		
					$mr_status_id = $vals['mr_status_id'];
					$status = array(3,10);
					$mr_status_name 	= '';
					if (in_array($mr_status_id, $status) and $vals['r_rrrsive_id']!= ''){
						$mr_status_name 	= 'เอกสารถูกส่งออกจากห้องสารบรรณกลางแล้ว'; 
					}else{
						$mr_status_name 	= $vals['mr_status_name'];
					}
					
					$arr_report1 = array();
					$arr_report1[]  	=	$keys+1;
					$arr_report1[]  	=	"'".$vals['mr_work_barcode'];
					$arr_report1[]  	=	''.$vals['mr_type_work_name'];//$vals['mr_topic'];
					$arr_report1[]  	=	''.$mr_status_name;//$vals['mr_status_name'];
					$arr_report1[]  	=	''.$emp_send_name;//$vals['emp_resive'];
					$arr_report1[]  	=	''.$send_branch_name;//$vals['b_resive'];
					$arr_report1[]  	=	''.$dep_send_name;//$vals['dep_resive'];
					$arr_report1[]  	=	''.$send_building_name;//$vals['f_resive'];
					$arr_report1[]  	=	''.$send_floor_name;//$vals['f_resive'];
					$arr_report1[]  	=	''.$branch_type_send_name;//$vals['bt_resive'];
					$arr_report1[]  	=	''.$emp_resive_name;//$vals['emp_send'];
					$arr_report1[]  	=	''.$resive_branch_name;//$vals['b_send'];
					$arr_report1[]  	=	''.$resive_dep_name;//$vals['dep_send'];
					$arr_report1[]  	=	''.$resive_building_name;//$vals['f_send'];
					$arr_report1[]  	=	''.$resive_floor_name;//$vals['f_send'];
					$arr_report1[]  	=	''.$branch_type_resive_name;//$vals['bt_send'];
					$arr_report1[]  	=	''.$vals['mr_topic'];//$vals['mr_work_date_sent'];
					$arr_report1[]  	=	"'".$sys_timestamp;//$vals['MailroomResive_date'];time_mail
					$arr_report1[]  	=	"'".$time_mail;//$vals['mr_work_date_success'];
					$arr_report1[]  	=	"'".$time_mail_send;//$vals['mr_work_date_success'];
					$arr_report1[]  	=	"'".$time_succ;//$vals['mr_type_work_name'];
					$arr_report1[] 	=	''.$vals['mr_work_remark'];//$vals['mr_work_remark'];
					$arr_report1[]  	=	''.$rate_send_txt;//$vals['rate_send'];//$rate_send;
					$arr_report1[]  	=	''.$vals['rate_remark'];//$vals['rate_remark'];//$rate_remark;
					$arr_report1[]  	=	'';//'';//$mr_user_role;
					$arr_report1[]  	=	''.$emp_mess_resive ;//$vals['emp_mess_resive'];
					$arr_report1[]  	=	''.$emp_mess_send ;//$vals['emp_mess_send'];
					$arr_report1[]  	=	''.$mr_round_name;//isset($round_resiveName[$vals['mr_work_main_id']])?$round_resiveName[$vals['mr_work_main_id']]:'';
					$arr_report1[]  	=	"'".$date_rrrsive;//isset($round_resiveDate[$vals['mr_work_main_id']])?$round_resiveDate[$vals['mr_work_main_id']]:'';
					$arr_report1[]  	=	"'".$date_rrrsive_2;//isset($round_resiveDate[$vals['mr_work_main_id']])?$round_resiveDate[$vals['mr_work_main_id']]:'';
					$arr_report1[]  	=	"'".$vals['quty'];//isset($round_resiveVal[$vals['mr_work_main_id']])?$round_resiveVal[$vals['mr_work_main_id']]:'';
					fputcsv($df, $arr_report1);
					//unset($data[$keys]);
		}



	}
	sleep(5);
	update_progress(100);
}



fclose($df);
ob_get_clean();
header("location:../download/TMB_Report_Y.csv");
exit;

//echo "ok";
//echo "<pre>".print_r($data[$keys],true)."</pre>";
$time_end = microtime(true);
$time = $time_end - $time_start;
// echo '>>>>>>>>>>..'.$time;
// echo "<pre>".print_r($arr_report1[$keys],true)."</pre>";
// exit;




$time_end = microtime_float();
$time = $time_end - $time_start;

//echo "<br> Did nothing in $time seconds\n";
//exit;





$file_name = 'TMB_Report'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($arr_report1 as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();


function array_column_($array,$colname,$Indexkey=''){
    $return_array = array();
    if(is_array($array) || is_object($array)){
      foreach($array as $arrayDATA){
        if(is_object($arrayDATA)){
          if(isset($arrayDATA->{$colname})){
            if(isset($Indexkey) && isset($arrayDATA->{$Indexkey}) ){
              $return_array[$arrayDATA->{$Indexkey}] = $arrayDATA->{$colname};
            } else {
              $return_array[] = $arrayDATA->{$colname};
            }
          }
        } else if(is_array($arrayDATA)) {
          if(isset($arrayDATA[$colname])){
            if(isset($Indexkey) && isset($arrayDATA[$Indexkey]) ){
              $return_array[$arrayDATA[$Indexkey]] = $arrayDATA[$colname];  
            } else {
              $return_array[] = $arrayDATA[$colname]; 
            } 
          } 
        }     
      }
    } 
    return $return_array;
  }
exit;


<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'PHPExcel.php';

include_once('xlsxwriter.class.php');
ini_set("memory_limit", "-1");
set_time_limit(0);

$time_start = microtime(true);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
 
$data_search		    		=  json_decode($req->get('params'),true);                                                  
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}
$data = $work_inoutDao->searchMailroom_upQury($data_search);
$time_end = microtime(true);
$time = $time_end - $time_start;
// echo '>>>>>>>>>>..'.count($data)."<br>";
// echo '>>>>>>>>>>..'.$time;
// exit;


$arr_report1	= array();
$sheet1 		= 'Detail';
$headers1  		= array(
	 'NO',                                       
	 'Barcode',                                       
	 'ชื่อผู้รับ',                                 
	 'สาขาผู้รับ',                              
	 'หน่วยงานผู้รับ',                              
	 'ชั้นผู้รับ',                              
	 'ประเภทสาขาผู้รับ',                     
	 'ชื่อผู้ส่ง',                                 
	 'สาขาผู้ส่ง',                              
	 'หน่วยงานผู้ส้ง',                              
	 'ชั้นผู้ส่ง',                              
	 'ประเภทสาขาผู้ส่ง',    
	 'ชื่อเอกสาร',
	 'วันที่ส่ง',                    
	 'วันที่ห้อง Mailroom รับ',                    
	 'วันที่สำเร็จ',                          
	 'สถานะงาน',                           
	 'ประเภทการส่ง',                           
	 'หมายเหตุ',            
	 'ความพึงพอใจ',
	 'หมายเหตุความพึงพอใจ',
	 'ส่งจาก',
	 'พนักงานรับ',
	 'พนักงานส่ง',
	 'รอบ',
	 'จำนวน'
);



$indexs = 2;
foreach($data as $keys => $vals) {
	
			$arr_report1[$keys][0]  	=	$keys+1;
			$arr_report1[$keys][1]  	=	"'".$vals['mr_work_barcode'];
			$arr_report1[$keys][2]  	=	$vals['emp_resive'];
			$arr_report1[$keys][3]  	=	$vals['b_resive'];
			$arr_report1[$keys][4]  	=	$vals['dep_resive'];
			$arr_report1[$keys][5]  	=	$vals['f_resive'];
			$arr_report1[$keys][6]  	=	$vals['bt_resive'];
			$arr_report1[$keys][7]  	=	$vals['emp_send'];
			$arr_report1[$keys][8]  	=	$vals['b_send'];
			$arr_report1[$keys][9]  	=	$vals['dep_send'];
			$arr_report1[$keys][10]  	=	$vals['f_send'];
			$arr_report1[$keys][11]  	=	$vals['bt_send'];
			$arr_report1[$keys][12]  	=	$vals['mr_topic'];
			$arr_report1[$keys][13]  	=	$vals['mr_work_date_sent'];
			$arr_report1[$keys][14]  	=	$vals['MailroomResive_date'];
			$arr_report1[$keys][15]  	=	$vals['mr_work_date_success'];
			$arr_report1[$keys][16]  	=	$vals['mr_status_name'];
			$arr_report1[$keys][17]  	=	$vals['mr_type_work_name'];
			$arr_report1[$keys][18] 	=	$vals['mr_work_remark'];
			$arr_report1[$keys][19]  	=	$vals['rate_send'];//$rate_send;
			$arr_report1[$keys][20]  	=	$vals['rate_remark'];//$rate_remark;
			$arr_report1[$keys][21]  	=	'';//$mr_user_role;
			$arr_report1[$keys][22]  	=	$vals['emp_mess_resive'];
			$arr_report1[$keys][23]  	=	$vals['emp_mess_send'];
			$arr_report1[$keys][24]  	=	$vals['mr_round_name'];
			$arr_report1[$keys][25]  	=	$vals['count_qty'];
	unset($data[$keys]);
}


//echo "ok";
//echo "<pre>".print_r($data[$keys],true)."</pre>";
$time_end = microtime(true);
$time = $time_end - $time_start;
echo '>>>>>>>>>>..'.$time;
echo "<pre>".print_r($arr_report1[$keys],true)."</pre>";
exit;




$time_end = microtime_float();
$time = $time_end - $time_start;

//echo "<br> Did nothing in $time seconds\n";
//exit;





$file_name = 'TMB_Report'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($arr_report1 as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
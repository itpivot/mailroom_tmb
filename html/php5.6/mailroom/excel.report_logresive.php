<?php
header('Content-Type: text/html; charset=utf-8');
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php'; 
require_once 'PHPExcel.php';
error_reporting(E_ALL & ~E_NOTICE);

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');

$time_start = microtime(true);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

// echo "---";
// exit;


$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                     
 
$data_search['start_date']		    		=  $req->get('date_1');                                                  
$data_search['end_date']		    		=  $req->get('date_2');                                                  

$params 	= array();
$log		= array();
$log_q		= array();
$main_id    = array();


	$params1 	= array();
	$sql_log = '
	SELECT
		l.mr_work_log_id,
		l.sys_timestamp,
		l.remark,
		m.mr_work_barcode,
		u.mr_user_username,
		e.mr_emp_name,
		e.mr_emp_lastname
	FROM
		mr_work_log l
	LEFT JOIN mr_work_main m on(m.mr_work_main_id=l.mr_work_main_id)
	LEFT JOIN mr_status s on(s.mr_status_id=m.mr_status_id) 
	LEFT JOIN mr_user u on(u.mr_user_id=l.mr_user_id)
	LEFT JOIN mr_emp e on(e.mr_emp_id = u.mr_emp_id)
	WHERE l.sys_timestamp BETWEEN ? and ? 
	and l.mr_status_id in(3,10)
	and l.mr_user_id != ""
	GROUP by l.mr_work_main_id
			';
		//$sql_log .= '   l.sys_timestamp  BETWEEN  ? and  ? ';
       // $sql_log .= '  group by mr_work_main_id ';
       // $sql_log .= '  UNION ALL ';
		//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
		array_push($params1, (string)$data_search['start_date'].' 00:00:00');
		array_push($params1, (string)$data_search['end_date'].' 23:59:59');
        $time_start = microtime(true);
		$log_q = $work_inoutDao->select_work_inout($sql_log,$params1);
        $time_end = microtime(true);
        $time = $time_end - $time_start;

        //echo '>>>>>>>>>>..'.$time;
       // echo '>>>>>>>>>>..<pre>'.print_r(count($log_q),true);
       //exit;

      //  exit;
//  echo '>>>>>>>>>>..<pre>'.print_r($log_q,true);
//  exit;


$arr_report1	= array();
$sheet1 		= 'Detail';
$headers1  		= array();
$indexs = 2;
foreach($log_q as $keys => $vals) {
		foreach($vals as $k => $v){
			if($keys == 0){
				$headers1[] = $k;
			}
			$arr_report1[$keys][]  	=	$v;
		}
			
}



//echo "<br> Did nothing in $time seconds\n";
//exit;



$file_name = 'TMB_Report'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($arr_report1 as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
// require_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';
require_once 'PHPExcel.php';
require_once 'Dao/Access_status_log.php';
set_time_limit(0);


$req = new Pivot_Request();
$access_status_log_Dao = new Dao_Access_status_log();

function PrettyDate($date)
{
    if ($date) {
        $old = explode('/', $date);
        return $old[2] . '-' . $old[1] . '-' . $old[0];
    } else {
        return null;
    }

}

$param = json_decode($req->get('data'), true);

$data = array();
$data['start_date'] = PrettyDate($param['start']);
$data['end_date'] = PrettyDate($param['end']);
$data['activity'] = $param['activity'];
$data['status'] = $param['status'];


$acc = $access_status_log_Dao->fetchAccess_logs($data);
$total = count($acc);


usort($acc, function ($x, $y) {
    return $y['sys_timestamp'] > $x['sys_timestamp'];
});


$indexs = 2;
$columnNames = array(
    0 => '#',
    1 => 'USER',
    2 => 'ชื่อ-สกุล',
    3 => 'Terminal',
    4 => 'กิจกรรม',
    5 => 'ผลการเข้าสู่ระบบ',
    6 => 'วันเวลา',
    7 => 'หมายเหตุ',
    8 => 'user-role',
    9 => 'รหัสหน่วยงาน(4)',
    10 => 'หน่วยงาน',
    11 => 'สาขา',
    12 => 'Cost-center',
);
  ob_start();
  $filename = 'export';
  $delimiter = ';';
  $enclosure = '"';
  header("Content-disposition: attachment; filename=Access_Logs.csv");
  // Tells to the browser that the content is a csv file
  header("Content-Type: text/csv");

  $df = fopen('../download/Access_Logs.csv', 'w');
  fputs($df, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
  fputcsv($df, $columnNames);
  //fputcsv($df, $columnNames);

foreach($acc as $k => $v) {
    $data= array();
    $data[] = $indexs;
    $data[] = $v['mr_user_username'];
    $data[] = $v['mr_emp_code'].' '.$v['mr_emp_name'].' '.$v['mr_emp_lastname'];
    $data[] = $v['terminal'];
    $data[] = $v['activity'];
    $data[] = $v['status'];
    $data[] = $v['sys_timestamp'];
    $data[] = $v['description'];
    $data[] = $v['mr_user_role_name'];
    $data[] = "'".$v['mr_department_code_4'];
    $data[] = $v['mr_department_code'].' '.$v['mr_department_name'];
    $data[] = $v['mr_branch_code'].' '.$v['mr_branch_name'];
    $data[] = "'".$v['mr_cost_code'];
    // fputcsv($df, $data);
    fputcsv($df, $data);
     $indexs++;
}

  fclose($df);
  ob_get_clean();
  header("location:../download/Access_Logs.csv");
exit;



?>
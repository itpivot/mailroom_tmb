<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
// require_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';
require_once 'PHPExcel.php';
require_once 'Dao/Access_status_log.php';
set_time_limit(0);


$req = new Pivot_Request();
$access_status_log_Dao = new Dao_Access_status_log();

function PrettyDate($date)
{
    if ($date) {
        $old = explode('/', $date);
        return $old[2] . '-' . $old[1] . '-' . $old[0];
    } else {
        return null;
    }

}

$param = json_decode($req->get('data'), true);

$data = array();
$data['start_date'] = PrettyDate($param['start']);
$data['end_date'] = PrettyDate($param['end']);
$data['activity'] = $param['activity'];
$data['status'] = $param['status'];


$acc = $access_status_log_Dao->fetchAccess_logs($data);
$total = count($acc);



// echo count($acc);
// exit;

usort($acc, function ($x, $y) {
    return $y['sys_timestamp'] > $x['sys_timestamp'];
});


// $objPHPExcel = new PHPExcel();
// $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma')->setSize(10);
// $objPHPExcel->getProperties()->setCreator("Pivot Co.,Ltd")->setLastModifiedBy("Pivot")->setTitle("Report Agent")->setSubject("Report")->setDescription("Report document for Excel, generated using PHP classes.");

// $columnIndexs = range('A', 'I');
// $columnNames[0] = array(
//     0 => '#',
//     1 => 'รหัส',
//     2 => 'ชื่อ-สกุล',
//     3 => 'Terminal',
//     4 => 'กิจกรรม',
//     5 => 'ผลการเข้าสู่ระบบ',
//     6 => 'วันเวลา',
//     7 => 'หมายเหตุ',
//     8 => 'user-role',
// );

// // Set column widths
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(10);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(20);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(30);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(20);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setWidth(20);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(20);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(20);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(30);
// $objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setWidth(30);

// // Header
// $row = 1;
// foreach ($columnNames as $key => $value) {
//     for ($index = 0; $index < count($columnIndexs); $index++) {
//         $leadColumns[$key][$index]['index'] = $columnIndexs[$index] . $row;
//         $leadColumns[$key][$index]['name'] = $columnNames[$key][$index];
//     }
//     $row++;
// }

// foreach ($leadColumns as $head) {
//     foreach ($head as $headColumn) {
//         $objPHPExcel->setActiveSheetIndex(0)->setCellValue($headColumn['index'], $headColumn['name'])->getStyle($headColumn['index']);
//         $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->setBold(true);
//         $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLACK);
//         $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
//         $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->getStartColor()->setARGB('FFDCDCDC');
//         $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//     }
// }


// Push Data

$indexs = 2;
$columnNames = array(
    0 => '#',
    1 => 'รหัส',
    2 => 'ชื่อ-สกุล',
    3 => 'Terminal',
    4 => 'กิจกรรม',
    5 => 'ผลการเข้าสู่ระบบ',
    6 => 'วันเวลา',
    7 => 'หมายเหตุ',
    8 => 'user-role',
);
  ob_start();
  $df = fopen('../download/Access_Logs.csv', 'w');
  fputcsv($df, $columnNames);

foreach($acc as $k => $v) {
    $data= array();
    $data[] = $indexs;
    $data[] = $v['mr_user_username'];
    $data[] = $v['mr_emp_lastname'];
    $data[] = $v['mr_emp_name'];
    $data[] = $v['terminal'];
    $data[] = $v['activity'];
    $data[] = $v['status'];
    $data[] = $v['sys_timestamp'];
    $data[] = $v['description'];
    $data[] = $v['mr_user_role_name'];
    fputcsv($df, $data);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $indexs, ($k + 1))->getStyle('A' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $indexs, strtoupper($v['mr_user_username']))->getStyle('B' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $indexs, $v['mr_emp_name']." ".$v['mr_emp_lastname'])->getStyle('C' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $indexs, $v['terminal'])->getStyle('D' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $indexs, strtoupper($v['activity']))->getStyle('E' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $indexs, strtoupper($v['status']))->getStyle('F' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $indexs, date('d/m/y H:i', strtotime($v['sys_timestamp'])))->getStyle('G' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $indexs, $v['description'])->getStyle('H' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $indexs, $v['mr_user_role_name'])->getStyle('H' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
   // $objPHPExcel->getActiveSheet(0)->getStyle('A'.$indexs.':I'.$indexs)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $indexs++;
}
$file_data = file_get_contents('../download/Access_Logs.csv');
$utf8_file_data = utf8_encode($file_data);
$new_file_name = '../download/Access_Logs.csv';
file_put_contents($new_file_name , $utf8_file_data );
fclose($df);
ob_get_clean();
header("location:../download/Access_Logs.csv");
exit;






$nickname = "Access_Logs_".date('dmY');
$filename = $nickname . '.xls';

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Access Logs');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
unlink('../download/Access_Logs.xls');    
$objWriter->save("../download/Access_Logs.xls");
header("location:../download/Access_Logs.xls");
exit;

$objPHPExcel = PHPExcel_IOFactory::load("../download/polimer_uzex.xlsx");	
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: public');
ob_end_clean();
$objWriter->save('php://output');
exit();

?>
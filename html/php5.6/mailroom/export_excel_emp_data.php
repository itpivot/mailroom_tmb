<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Work_inout.php';
require_once 'ajax/check_sla.php';

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$userDao 			= new Dao_User();   
$work_inoutDao 		= new Dao_Work_inout();
$class_check_sla 	= new Class_check_sla();

$sql = "SELECT 
				u.mr_user_role_id,
				r.mr_user_role_name,
				e.mr_emp_name, 
				e.mr_emp_lastname, 
				concat(' ',e.mr_emp_code) as mr_emp_code, 
				e.mr_floor_id, 
				e.mr_workplace, 
				e.mr_workarea, 
				e.mr_position_id, 
				e.mr_department_id, 
				e.old_emp_code,
				b.mr_branch_id,
				b.mr_branch_name,
				concat(' ',b.mr_branch_code) as mr_branch_code, 
				d.mr_department_name,
				concat(' ',d.mr_department_code) as mr_department_code
			FROM mr_emp e
			left join mr_user u on(u.mr_emp_id = e.mr_emp_id) 
			left join mr_user_role r on(r.mr_user_role_id = u.mr_user_role_id) 
			left join mr_branch b on(b.mr_branch_id = e.mr_branch_id) 
			left join mr_department d on(d.mr_department_id = e.mr_department_id) 
			where e.mr_emp_code not like 'Resign'
			";
$sql_department 	= "SELECT * FROM mr_department ";
$sql_branch 		= "SELECT * FROM mr_branch ";
$sql_floor 			= "SELECT * FROM mr_floor ";


$emp_data__all 			= $userDao ->select($sql);
//$department_data__all 	= $userDao ->select($sql_department);
//$branch_data__all 		= $userDao ->select($sql_branch);
//$sql_floor_data__all 	= $userDao ->select($sql_floor);
$new_emp	=	array();
foreach($emp_data__all  as $index_i => $val_i){
	$mr_department_id 	= $val_i['mr_department_id'];
	$mr_branch_id 		= $val_i['mr_branch_id'];
	$mr_floor_id 		= $val_i['mr_floor_id'];
	$mr_user_role_id 	= $val_i['mr_user_role_id'];
	$mr_emp_code 	= $val_i['mr_emp_code'];
	
	if($mr_user_role_id == '2'){
		$emptype = 'พนักงาน TMB สำนักงานใหญ่';//mr_user_role_name
	}else if($mr_user_role_id == '1'){
		$emptype = 'ADMIN สำนักงานใหญ่';
	}else if($mr_user_role_id == '3'){
		$emptype = 'พนักงานเดินเอกสาร สำนักงานใหญ่';
	}else if($mr_user_role_id == '4'){
		$emptype = 'พนักงานห้อง Mailroom';
	}else if($mr_user_role_id == '5'){
		$emptype = 'พนักงาน TMB สาขา';
	}else if($mr_user_role_id != ''){
		$emptype =  $val_i['mr_user_role_name'];
	}else{
		$emptype = 'ยังไม่ลงทะเบียนเข้าใช้งาน';
	}
	$is_tmb = '';
	$len_emp_code =  strlen($mr_emp_code); 
	if (is_numeric($mr_emp_code)) {
		if($len_emp_code == 6){
			$is_tmb = 'TMB';
		}else{
			$is_tmb = 'outsource';
		}
	}else{
		$is_tmb = 'outsource';
	}
	$new_emp[] = array(
		($index_i+1),
		$val_i['mr_emp_code'],
		$val_i['mr_emp_name'],
		$val_i['mr_emp_lastname'],
		$val_i['mr_department_code'],
		$val_i['mr_department_name'],
		$val_i['mr_branch_code'],
		$val_i['mr_branch_name'],
		$val_i['mr_workarea'],
		$val_i['mr_workplace'],
		$is_tmb,
		$emptype
		
	);
}
//echo "<pre>".print_r($new_emp ,true)."</pre>";
//exit;

$sheet = 'emp_data';
$headers = array(
    'ลำดับ.',
    'รหัสพนักงาน',
    'ชื่อ	',
    'นามสกุล',
    'รหัสหน่วยงาน',
    'ชื่อหน่วยงาน',
    'รหัสาขา',
    'ชื่อสาขา',
    'mr_workarea',
    'workplace',
    'Is TMB',
    'สิทธ์การใช้งาน'
);

$file_name = 'TMB_Report.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet,$headers,$styleHead);
foreach ($new_emp as $v) {
    $writer->writeSheetRow($sheet,$v,$styleRow);
 }
//exit;

$writer->writeToStdOut();

exit();
//echo print_r($work_type2,true);
//echo 'end <br>';

echo json_encode($resout);
//echo print_r($work_type2,true);
//echo 'end <br>';
?>

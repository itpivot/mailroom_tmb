
function validateForm(){
  var lst_sender_building = $("#lst_sender_building").val();
  var lst_sender_floor = $("#lst_sender_floor").val();
  var sender_name = $("#txt_sender_name").val();
  var lst_receiver_building = $("#lst_receiver_building").val();
  var lst_receiver_floor = $("#lst_receiver_floor").val();
  var receiver_name = $("#txt_receiver_name").val();
  var barcode = $("#txt_barcode").val();
  var remark = $("#txt_remark").val();
  var status = true;


  if(lst_sender_building == "" || lst_sender_building == null){
    $('#sender_building').html("กรุณาเลือกข้อมูล").css('color','red');
    status = false;
  }else{
    $('#sender_building').html("");
    status = true;
  }

  if(lst_sender_floor == "" || lst_sender_floor == null){
    $('#sender_floor').html("กรุณาเลือกข้อมูล").css('color','red');
    status = false;
  }else{
    $('#sender_floor').html("");
    status = true;
  }

  if(sender_name == "" || sender_name == null){
    $('#sender_name').html('จำเป็นต้องกรอก').css('color','red');
    status = false;
  }else{
    $('#sender_name').html("");
    status = true;
  }

  if(lst_receiver_building == "" || lst_receiver_building == null){
      $('#receiver_building').html('กรุณาเลือกข้อมูล').css('color','red');
      status = false;
  }else{
    $('#receiver_building').html("");
    status = true;
  }

  if(lst_receiver_floor == "" || lst_receiver_floor == null){
    $('#receiver_floor').html('กรุณาเลือกข้อมูล').css('color','red');
    status = false;
  }else{
    $('#receiver_floor').html("");
    status = true;
  }

  if(receiver_name == "" || receiver_name == null){
    $("#receiver_name").html('จำเป็นต้องกรอก').css('color','red');
    status = false;
  }else{
      $("#receiver_name").html("");
    status = true;
  }

  if(barcode == "" || barcode == null){
    $('#barcode').html('จำเป็นต้องกรอก').css('color','red');
    status = false;
  }else{
    $('#barcode').html("");
    status = true;
  }

  if(status == true){
    saveData();
  }

}

function saveData(){
  var lst_sender_building = $("#lst_sender_building").val();
  var lst_sender_floor = $("#lst_sender_floor").val();
  var sender_name = $("#txt_sender_name").val();
  var lst_receiver_building = $("#lst_receiver_building").val();
  var lst_receiver_floor = $("#lst_receiver_floor").val();
  var receiver_name = $("#txt_receiver_name").val();
  var barcode = $("#txt_barcode").val();
  var remark = $("#txt_remark").val();
  $.ajax({
    type: 'post',
    url: 'ajax/ajax_save_sender_entry_data.php',
    data:{
        'sender_building': lst_sender_building,
        'sender_floor': lst_sender_floor,
        'sender_name': sender_name,
        'receiver_building': lst_receiver_building,
        'receiver_floor': lst_receiver_floor,
        'receiver_name': receiver_name,
        'barcode': barcode,
        'remark': remark
    },
    dataType: 'json',
    success: function(res){
      	$("#tb_work_order").dataTable().ajax.reload();
    }
  });
}

<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$alert 			= '';

$data  			= array();
$round_print 	= $req->get('round_printreper');
$date_report 	= $req->get('date_report');

	

//echo '<pre>'.print_r($date_report,true).'</pre>';

//exit;
if(preg_match('/<\/?[^>]+(>|$)/', $round_print)) {
	$alert = "
	$.confirm({
		title: 'Alert!',
		content: 'เกิดข้อผิดพลาด!',
		buttons: {
			OK: function () {
				location.href = 'create_work_byHand_out.php';
				}
			}
		});
	";
}else if(preg_match('/<\/?[^>]+(>|$)/', $date_report)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{

//echo "<pre>".print_r($req,true)."</pre>";
// echo $date_report;
// echo ">>".$round_print;
// 	exit;

	$sql='SELECT
				w_m.mr_work_main_id,
				DATE_FORMAT(w_m.mr_work_date_sent, "%Y-%m-%d") as d_send,
				w_m.mr_work_barcode,
				w_m.mr_work_remark,
				w_p.mr_address,
				w_p.mr_post_price,
				w_p.mr_post_totalprice,
				w_p.sp_num_doc,
				w_p.num_doc,
				w_p.mr_post_weight,
				w_m.quty,
				pv.mr_provinces_name,
				w_p.mr_send_emp_detail,
				w_p.mr_cus_tel,
				w_p.mr_send_emp_id,
				w_p.mr_cus_name as sresive_name,
				w_p.mr_cus_lname as resive_lname,
				concat(w_p.mr_cus_name," ",w_p.mr_cus_lname) as name_resive,
				w_p.mr_address as sendder_address,
				dep.mr_department_name as dep_resive,
				dep.mr_department_code as dep_code_resive,
				emp_send.mr_emp_code ,
				emp_send.mr_emp_name,
				emp_send.mr_emp_lastname,
				concat(emp_send.mr_emp_code ," : " , emp_send.mr_emp_name,"  " , emp_send.mr_emp_lastname,"  ",dep.mr_department_name) as name_send,
				r.mr_round_name,
				s.mr_status_id,
				s.mr_status_name,
				w_p.mr_type_post_id,
				t_p.mr_type_post_name
			FROM
				mr_work_main w_m
				LEFT join mr_status s on(s.mr_status_id = w_m.mr_status_id)
				LEFT join  mr_work_post w_p on(w_p.mr_work_main_id = w_m.mr_work_main_id)
				LEFT join  mr_type_post t_p on(t_p.mr_type_post_id = w_p.mr_type_post_id)
				LEFT join  mr_department dep on(dep.mr_department_id = w_p.mr_send_department_id)
				LEFT join  mr_emp emp_send on(emp_send.mr_emp_id = w_p.mr_send_emp_id)
				LEFT join  mr_round r on(r.mr_round_id = w_m.mr_round_id)
				LEFT join  mr_provinces pv on(pv.mr_provinces_id = w_p.mr_province_id)
			WHERE
				w_m.mr_work_date_sent like"'.$date_report.'%"
				and w_m.mr_type_work_id = 6
				and w_m.mr_status_id != 6
			   ';

		if($round_print!='') {
			 $sql.='
				and w_m.mr_round_id = 	'.$round_print.' ';
				}

		$sql.='
		group by w_m.mr_work_main_id 
		order by w_m.mr_work_main_id asc';	
		


		
		
	
	$data 			= $send_workDao->select($sql);
	$datasorce 		= array();
	$arr_sum_price 	= array();
	$arr_total_price 	= array();
	foreach($data as $j => $data_j){
		if($data_j['mr_post_totalprice']<=0){
			$data_j['mr_post_totalprice'] = $data_j['quty']*$data_j['mr_post_price'];
		}
		$mr_send_emp_id 										= $data_j['mr_send_emp_id'];
		$mr_type_post_id 										= $data_j['mr_type_post_id'];

		$datasorce[$mr_type_post_id][$mr_send_emp_id][$j] 								=  $data_j;

		if(isset($arr_total_price[$mr_type_post_id]['s_quty'])){
			$arr_total_price[$mr_type_post_id]['s_quty'] 						+= $data_j['quty'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_price'] 				+= $data_j['mr_post_price'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_totalprice'] 			+= $data_j['mr_post_totalprice'];
		}else{
			$arr_total_price[$mr_type_post_id]['s_quty'] 						= $data_j['quty'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_price'] 				= $data_j['mr_post_price'];	
			$arr_total_price[$mr_type_post_id]['s_mr_post_totalprice'] 			= $data_j['mr_post_totalprice'];
		}
			

		if(isset($arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_quty'])){
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_quty'] 						+= $data_j['quty'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_price'] 				+= $data_j['mr_post_price'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_totalprice'] 			+= $data_j['mr_post_totalprice'];	
		}else{
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_quty'] 						= $data_j['quty'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_price'] 				= $data_j['mr_post_price'];	
			$arr_sum_price[$mr_type_post_id][$mr_send_emp_id]['s_mr_post_totalprice'] 			= $data_j['mr_post_totalprice'];	

		}

		

		
	}
	
//$sumcount = array_sum ( $  ) ;
//$price = array_sum ( $arr_price ) ;
//$sum_price = array_sum ( $arr_sum_price ) ;
// echo "<pre>".print_r($datasorce,true)."</pre>";
// exit;
//echo $date_report;


	$newdata=array();
	$page = 1;
	foreach($datasorce as $m => $data_l){
		$no=1;
		$index 	= 1;
		$total  = $arr_total_price[$m]['s_quty'];
		foreach($data_l as $k => $data_k){
		
			$count  = $arr_sum_price[$m][$k]['s_quty'];
			foreach($data_k as $l 	=> $data_){
				$data_['no'] 							= $no;
				$data_['count'] 						= $count;
				$data_['pr'] 							= $arr_sum_price[$m][$k]['s_mr_post_price'];
				$data_['sumpr'] 						= $arr_sum_price[$m][$k]['s_mr_post_totalprice'];

				$newdata['data'][$page]['data'][$l] 	=  $data_;
				$newdata['data'][$page]['head']['data'] =  $data_;
				$newdata['data'][$page]['head']['page'] =  $page;
				$newdata['allpage'] =  $page;
				$no++;
				if($index >= 39){
					if($count>0){
						$index =2;
					}else{
						$index =1;
					}
					$page++;
				}else{
					if($count>0){
						$index +=2;
					}else{
						$index +=1;
					}
					
				}
				$count = 0;
			}
		}
		$data_['no'] 							= $no;
		$data_['total'] 						= $total;
		$data_['total_pr'] 						= $arr_total_price[$m]['s_mr_post_price'];
		$data_['total_sumpr'] 					= $arr_total_price[$m]['s_mr_post_totalprice'];

		$newdata['data'][$page]['data'][$l+1] 	=  $data_;
		$newdata['data'][$page]['head']['data'] =  $data_;
		$newdata['data'][$page]['head']['page'] =  $page;
		$newdata['allpage'] =  $page;
		$page++;
	}
}





//echo "<pre>".print_r($newdata,true)."</pre>";
// echo $date_report;
// // echo ">>".$round_print;
///exit;















$txt_html='
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Times New Roman", Times, serif;
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 16px
 }
 tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 12px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

    @media print {
	#print_p{
		  display:none;
		}
      html,
      body {
        width: 210mm;
        height: 297mm;
		 font-family: "Times New Roman", Times, serif;
		 font-size:12px;
      }
      .page {
		width: 210mm;
		height: 297mm;
		min-height: 297mm;
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: unset;
      }
    }
  </style>
</head>

<body>
  <div class="book">
   
   ';
foreach( $newdata['data'] as $i => $page){
//echo"kkkk<br>";
   $txt_html.='  <div class="page">
      <div class="subpage ">
        <div class="header mb-3">
          
          
	
		</div>
		<table width="100%">
			<tr>
				<td width="70%" align="center">
					<h5 class="">สารบรรณกลางและโลจิสติกส์  บริหารทรัพยากรอาคาร</h5>
					<h6 class="">ใบสรุปรายงาน-ส่ง ปณ.</h6>
				</td>
				
				<td  align="right">
					หน้า  :&nbsp;&nbsp;&nbsp;<b><u>'.$page['head']['page'].'/'.$newdata['allpage'].'&nbsp;&nbsp;&nbsp;</u></b><br>
					วันที่  <b><u> '.$date_report.' รอบ '.$page['head']['data']['mr_round_name'].'</u></b></p>
					<p class="card-text">เวลาพิมพ์  <b><u>'.date('Y-m-d').'   &nbsp;&nbsp;&nbsp;'.date('H:i:s').'&nbsp;&nbsp;&nbsp;</u></b> </p>
				</td>
				
            </tr>
            <tr>
				<td colspan="2" align="left">
				ประเภทการส่ง : '.$page['head']['data']['mr_type_post_name'].'
				</td>
            </tr>
		 </table>
		 
		 
		 

        <div class="table-responsive">
          <table   class="table table-sm table-bordered">
            <thead class="thead-light">
              <tr>
				';
				$type_re = array(3,5);
				if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
				{
					$txt_html.='
					<th width="50%" class="text-center">ผู้ส่ง/ผู้รับ</th>';
				}else{

					$txt_html.='
					<th width="60%" class="text-center">ผู้ส่ง/ผู้รับ</th>';
				}

				$txt_html.='
                <th class="text-center" width="20%">ปลายทาง</th>
                <th class="text-center" width="15%">ทะเบียน</th> 
				';
				$type_re = array(3,5);
				if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
				{
					$txt_html.='<th class="text-center" width="15%">เลขตอบรับ</th> ';
				}

				$txt_html.='
                <th class="text-center" width="5%">ราคา</th>
                <th class="text-center" width="5%" >จำนวน</th>
                <th class="text-center" width="5%">ราคารวม</th>
                <th>หมายเหตุ</th>
              </tr>
            </thead>';
           $txt_html.='<tbody>';
			$arr_total = array();
		   foreach( $page['data'] as $subd){
				//foreach( $emp as $subd){
					
					if($subd['no'] == "all"){
						$txt_html.='
						<tr>
							<th class="text-right"colspan="3"><u>รวมจำนวนซองทั้งสิ้น</u></th>
							<th class="text-center" ><u>{{subd.qty}}</u></th>
							<th></th>
						</tr>';

					}else{ 
						if($subd['total']!=0){
							$arr_total = $subd;
							
						}else{
							if($subd['count']!=0){
								$type_re = array(3,5);
								if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
									{
								$txt_html.=' 
								<tr>
									<th class="text-left" colspan="4">'.$subd['mr_send_emp_detail'].'  		</th>
									<th class="text-center" colspan="0"></th>
									<th class="text-center" colspan="0">'.$subd['count'].'</th>
									<th class="text-center" colspan="0">'.$subd['sumpr'].'</th>
								</tr>';
							}else{
								$txt_html.=' 
								
								<tr>
									<th class="text-left" colspan="3">'.$subd['mr_send_emp_detail'].'  		</th>
									<th class="text-center" colspan="0"></th>
									<th class="text-center" colspan="0">'.$subd['count'].'</th>
									<th class="text-center" colspan="0">'.$subd['sumpr'].'</th>
								</tr>';
							}


								$txt_html.=' 
								<tr>
									<td class="text-right">'.$subd['sresive_name'].' '.$subd['sresive_lname'].'		</td>
									<td class="text-center">'.$subd['mr_address'].'</td>
									<td class="text-center">'.$subd['num_doc'].'</td>
									';
									$type_re = array(3,5);
									if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
									{
										$txt_html.='<td>'.$subd['sp_num_doc'].'</td> ';
									}

									$txt_html.='
									<td class="text-center">'.$subd['mr_post_price'].'</td>
									<td class="text-center">'.$subd['quty'].'</td>
									<td class="text-center">'.$subd['mr_post_totalprice'].'</td>
									<td>'.$subd['mr_work_remark'].'</td>
								</tr>';

							}else{
								$txt_html.=' 
								<tr>
									<td class="text-right">'.$subd['sresive_name'].' '.$subd['sresive_lname'].'	</td>
									<td class="text-center">'.$subd['mr_address'].'</td>
									<td class="text-center">'.$subd['num_doc'].'</td>
									';
									$type_re = array(3,5);
									if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
									{
										$txt_html.='<td>'.$subd['sp_num_doc'].'</td> ';
									}

									$txt_html.='
									<td class="text-center">'.$subd['mr_post_price'].'</td>
									<td class="text-center">'.$subd['quty'].'</td>
									<td class="text-center">'.$subd['mr_post_totalprice'].'</td>
									<td>'.$subd['mr_work_remark'].'</td>
								</tr>';

							}
						}
						
					}
				//}
			}
			
			if($arr_total['total']!=0){
				$subd = $arr_total;
				$type_re = array(3,5);
				if(in_array($page['head']['data']['mr_type_post_id'],$type_re))
					{
						$txt_html.=' 
						<tr>
							<th class="text-left" colspan="4">รวมทั้งหมด  		</th>
							<th class="text-center" colspan="0">'.$subd['total_pr'].'</th>
							<th class="text-center" colspan="0">'.$subd['total'].'</th>
							<th class="text-center" colspan="0">'.$subd['total_sumpr'].'</th>
						</tr>';
					}else{
						$txt_html.=' 
						<tr>
							<th class="text-left" colspan="3">รวมทั้งหมด  		</th>
							<th class="text-center" colspan="0">'.$subd['total_pr'].'</th>
							<th class="text-center" colspan="0">'.$subd['total'].'</th>
							<th class="text-center" colspan="0">'.$subd['total_sumpr'].'</th>
						</tr>';
					}
			}

            $txt_html.='</tbody> ';
				if($subd['total']!=0){
					$txt_html.='
				  </table>
				</div>
				<br>
						<br>
						<br>';


				$txt_html.='<center>
				
				
				<table border="0" width="100%"   class="center">
				<tr>
					<td align="center">
						<p>ลงชื่อ...........................................</p>
						<p>(..............................................)</p>
						<p>วันที่................................</p>
						<p>เจ้าหน้าที่ บ.ไพวอท</p>
					</td>
					<td align="center">
						<p></p>
						<p></p>
						<p></p>
						<p>ประทับตราไปรษณีย์</p>
					</td>
					<td align="center">
						<p>ลงชื่อ...........................................</p>
						<p>(..............................................)</p>
						<p>วันที่................................</p>
						<p>เจ้าหน้าที่ไปรษณีย์</p>
					</td>
				</tr>

				
			</table>
			</center>';
				}else{
						$txt_html.='</tbody> ';
				
						$txt_html.='
					</table>
					</div>
					<br>
							<br>
							<br>';
				}
			$txt_html.='
			
      </div>
    </div>';

}
$txt_html.='  
  

 
</body>

</html>';




// echo '<pre>'.print_r($newdata,true).'</pre>';
// echo $txt_html;
// exit;
function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}


require_once 'ThaiPDF/thaipdf.php';
$left=3;
$right=3;
$top=5;
$bottom=5;
$header=0;
$footer=5;
$filename='file.pdf';
pdf_margin($left,$right,$top, $bottom,$header,$footer);
pdf_html($txt_html);
pdf_orientation('P');        
pdf_echo();

exit;
//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//exit;
$template = Pivot_Template::factory('mailroom/print_sort_branch_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
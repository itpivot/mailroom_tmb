<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '64M');
ini_set('memory_limit', '-1');

require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_mainDao = new Dao_Work_main();
$send_workDao = new Dao_Send_work();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuserid($user_id);
$alertt = '';

//$data = $work_mainDao->BranchgetDataAll_send($user_data['mr_branch_id']);

$id = $send_workDao->fetchAll('SELECT max(mr_send_work_id) FROM mr_send_work');
if(!empty($id)){
	$s_id = $id[count($id)-1]['mr_send_work_id'];
}else{
	$s_id = '';
}
$template = Pivot_Template::factory('mailroom/receive_work.tpl');
$template->display(array(
	'debug' => print_r($data,true),
	'alertt' => $alertt,
	'id' => $s_id,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$statusDao = new Dao_Status();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
$today = date('Y-m-d H:i:s');

$status_data = $statusDao->getStatusall();
$status_data2 = $statusDao->getStatusall3();
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$template = Pivot_Template::factory('mailroom/report.emp_resivework.tpl');
$template->display(array(
	//'debug' => print_r($status_data,true),
	'status_data' => $status_data,
	'status_data2' => $status_data2,
	'today' => $today,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
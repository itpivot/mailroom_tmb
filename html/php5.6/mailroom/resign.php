<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';

require_once 'Dao/UserRole.php';

/* Check authentication */
//$auth = new Pivot_Auth();
//if (!$auth->isAuth()) {
//    Atapy_Site::toLoginPage();
//}

$req = new Pivot_Request();


$template = Pivot_Template::factory('mailroom/resign.tpl');
$template->display(array(
    'error' => print_r($errors, true),
    'debug' => $debug,
));

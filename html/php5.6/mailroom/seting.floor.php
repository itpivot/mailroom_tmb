<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Round.php';
require_once 'nocsrf.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//
$req = new Pivot_Request();
$userDao = new Dao_User();

$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$userRoleDao		= new Dao_UserRole();
$branchDao			= new Dao_Branch();
$roundDao			= new Dao_Round();
$send_workDao		= new Dao_Send_work();



$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);
$user_data_show = $userDao->getEmpDataByuseridProfile($user_id);
$messenger = $userDao->getMessengers();
//$type_work = $roundDao->gettype_work();
$floor_data		= $floorDao->fetchAll();
//echo "<pre>".print_r($messenger,true)."</pre>";//mr_emp_code

$sql='SELECT 
	b.mr_branch_id, 
	b.mr_branch_name, 
	b.mr_branch_code, 
	b.mr_hub_id, 
	b.mr_branch_type as mr_branch_type_id, 
	b.sys_date
FROM mr_branch b ';
$branchdata = $userDao ->select($sql);


$template = Pivot_Template::factory('mailroom/seting.floor.tpl');
$template->display(array(
	//'debug' => print_r($contactdata,true),
	'contactdata' => $contactdata,
	//'debug' => print_r($path_pdf,true),
	'userRoles' => (isset($userRoles) ? $userRoles : null),
	'success' => (isset($success) ? $success : null),
	'branchdata' => $branchdata,
	'floor_data' => $floor_data,
	'messenger' => $messenger,
	'user_data' => $user_data,
	'user_data_show' => $user_data_show,
	//'select' => '0',
	'today' => date('Y-m-d'),
	'role_id' => $auth->getRole(),
	'csrf_token' =>  NoCSRF::generate( 'csrf_token'),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
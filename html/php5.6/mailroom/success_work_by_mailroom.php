<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Work_inout.php';

/* Check authentication */
$auth = new Pivot_Auth();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}



$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$department_data = $departmentDao->fetchAll();
$floor_data = $floorDao->getFloorMess();
$work_inout_Dao = new Dao_Work_inout();

$work_id = $req->get('id');
$work_id = base64_decode(urldecode($work_id));
$alert = '';
if(preg_match('/<\/?[^>]+(>|$)/', $work_id)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'search.php';
			}
		}
	});
		
	";
}

$user_data = $work_inout_Dao->getCancleWorkByMailroom($work_id);


//echo "<pre>".print_r($user_data,true)."</pre>";


$template = Pivot_Template::factory('mailroom/success_work_by_mailroom.tpl');
$template->display(array(
	//'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'department_data' => $department_data,
	'userRoles' => $userRoles,
	'users' => $users,
	'user_data' => $user_data,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'userID' => $user_id,
	'empID' => $emp_id,
	'floor_data' => $floor_data,
	'alert' => $alert
));
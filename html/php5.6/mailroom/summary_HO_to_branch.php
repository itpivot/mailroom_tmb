<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$status_Dao = new Dao_Status();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$mess_data = $userDao->getMessSend();

$status = $status_Dao->fetchAll();


$template = Pivot_Template::factory('mailroom/summary_HO_to_branch.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	//'userRoles' => $userRoles,
	'success' => $success,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	'mess_data' => $mess_data,
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'status' => $status
));
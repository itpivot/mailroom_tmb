
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'Dao/Work_post.php';
require_once 'Dao/Type_post.php';
require_once 'Dao/Department.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Employee.php';
require_once 'nocsrf.php';
require_once 'Dao/Cost.php';
require_once 'PHPExcel.php';


ini_set('memory_limit', '-1');
set_time_limit(-1);
$costDao			= new Dao_Cost();

	try {
		$inputFileName = "import.xlsx"; 
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); 
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); 
        $objReader->setReadDataOnly(true); 
        $objPHPExcel = $objReader->load($inputFileName); 
	} catch(Exception $e) {
		echo json_encode(array('status' => 505, 'message' => 'Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage() ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}

	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = $sheet->getHighestColumn();
	$ch_num = 0;
	//  Loop through each row of the worksheet in turn
	$store_barcode = array();
	$store_barcode_re = array();
	$data_dave = array();
	for ($row = 1; $row <= $highestRow; $row++){ //  Read a row of data into an array
		$id 				= $sheet->getCell('A'.$row)->getValue();
		$name			= $sheet->getCell('B'.$row)->getValue();
		$code 			= $sheet->getCell('C'.$row)->getValue();
        $update = array();

		$update['mr_cost_name'] = $name;
		$update['mr_cost_code'] = $code;
        if($id!=''){
            $costDao->save($update,$id);
            echo $no."<br>";
        }else{
            echo "null <br>";
        }

		//exit;
		
		
	}
	
	exit();
	try{
		foreach($data_dave as $key_save => $val_save){
			//date_import
			//import_round
			$save_main['mr_work_date_sent']				=$date_import; //วันนำส่ง
			$save_main['mr_work_barcode']				= $val_save['mr_work_barcode'];
			$save_main['mr_work_remark']				= $val_save['mr_work_remark'];
			$save_main['mr_type_work_id']				= 6;
			$save_main['mr_status_id']					= 3;
			$save_main['mr_user_id']					= $val_save['mr_user_id'];
			$save_main['mr_round_id']					= $val_save['mr_round_id'];
			$save_main['quty']							= $val_save['quty'];
			$mr_work_main_id 							= $work_mainDao->save($save_main);


			$save_post['mr_work_main_id']				= $mr_work_main_id;
			$save_post['sys_timestamp']					= date("Y-m-d H:i:s");
			if($val_save['mr_send_emp_id'] != ''){
				$save_post['mr_send_emp_id']			= $val_save['mr_send_emp_id'];
			}
			$save_post['sp_num_doc']			        = $val_save['sp_num_doc'];
			$save_post['mr_send_department_id']			= $val_save['mr_send_department_id'];
			$save_post['mr_send_emp_detail']			= $val_save['mr_send_emp_detail'];
			$save_post['mr_cus_name']					= $val_save['mr_cus_name'];
			$save_post['mr_cus_lname']					= '';
			$save_post['mr_address']					= $val_save['mr_address'];
			$save_post['mr_type_post_id']				= $val_save['mr_type_post_id'];
			$save_post['mr_post_weight']				= $val_save['mr_post_weight'];
			$save_post['mr_post_price']					= $val_save['mr_post_price'];
			$save_post['mr_post_totalprice']			= $val_save['mr_post_totalprice'];
			$save_post['mr_post_amount']				= $val_save['mr_post_amount'];
			$work_post_id								= $work_postDao->save($save_post);
		
			//exit();	
			$save_log['mr_user_id'] 									= $user_id;
			$save_log['mr_status_id'] 									= 3;
			$save_log['mr_work_main_id'] 								= $mr_work_main_id;
			$save_log['remark'] 										= "mailroom_สร้างราายการนำส่ง ปณ. ส่งออก import Excel";
			$log_id = $work_logDao->save($save_log);


		}
		echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
			

	}catch ( Exception $e){
		if(!empty($mr_work_main_id)){
			$work_mainDao->remove($mr_work_main_id);
		}
		if(!empty($work_post_id)){
			$work_postDao->remove($work_post_id);
		}
		if(!empty($log_id)){
			$work_logDao->remove($log_id);
		}
		echo json_encode(array('status' => 500, 'message' => 'เกิดข้อผิดพลาดในการบันทึกข้อมูล' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}	

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Floor.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$department_Dao 		= new Dao_Department();
$work_inout_Dao 		= new Dao_Work_inout();
$floor_Dao 				= new Dao_Floor();

$start = $req->get('start_date');
$end = $req->get('end_date');

function changeFormatDate($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode('/', $date);
        $result = $new_date[2]."-".$new_date[1]."-".$new_date[0];
        return $result;
    }
}

function changeFormatDateTime($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode(' ', $date);
        $new_datedate = explode('-', $new_date[0]);
        $result = $new_datedate[2]."-".$new_datedate[1]."-".$new_datedate[0]." ".$new_date[1];
        return $result;
    }
}

function changeFormatTime($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode(' ', $date);
        $result = $new_date[1];
        return $result;
    }
}


 function DateTimeDiff($strDateTime1,$strDateTime2)
	 {
		return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
	 }

$data = array();

$data['start_date'] = changeFormatDate($start); 
$data['end_date'] = changeFormatDate($end); 

$dept = $floor_Dao->getFloor();
$work_inout = $work_inout_Dao->searchAutoSLA0($data);



foreach($work_inout as $key => $val) {
	$work_inout[$key]['no'] = $key + 1;
	$work_inout[$key]['order_time_show'] = changeFormatDateTime($val['order_time']);
	$work_inout[$key]['mess_re_show'] = changeFormatDateTime($val['mess_re']);
	$work_inout[$key]['success_show'] = changeFormatDateTime($val['success']);
	
	//$mess_re = changeFormatTime($val['mess_re']);
	//$success = changeFormatTime($val['success']);
	$diff = DateTimeDiff($val['order_time'],$val['success']);
	if( $diff > 1 ){
		$work_inout[$key]['SLA']	= "SLA";	
	}
	
}
//echo "<pre>".print_r($work_inout,true)."</pre>"; 
//exit();	

// echo print_r($resp);
echo json_encode($work_inout);
// exit();
?>
<?php

require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

include_once('xlsxwriter.class.php');

ini_set("memory_limit","1536M");
	
/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
 
$date			=	$req->get('date');
                                             
   
$data = $work_inoutDao->reportAllMonthlySend($date);

                           
//echo "<pre>".print_r($data,true)."</pre>"; 
//exit();	


function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = explode("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}

		
		
		
	

//echo "<pre>".print_r( $data , true )."</pre>";

$nickname = "report_send_daily_".$date;
$filename = $nickname.'.xlsx';
$sheet = 'report';


$head = array(                                                         
		'No.'				,//	=>'Integer',                           
		'Barcode'			,//	=>'string',                            
		'ชื่อผู้รับ'			,//	=>'string',                            
		'ที่อยู่'        	,//	=>'string',                            
		'ชื่อผู้ส่ง'        	,//	=>'string',                            
		'ที่อยู่'      	,//	=>'string',    
		'ชื่อเอกสาร'      	,//	=>'string',
		'วันที่ส่ง'         	,//	=>'string',                            
		'วันที่สำเร็จ'            	,//	=>'string',                            
		'สถานะงาน'            	,//	=>'string',                            
		'ประเภทการส่ง'            	,//	=>'string',                            
		'หมายเหตุ'            	,//	=>'string',                            
	
	);
	
	
	for($i = 0; $i < count($data); $i++){
				
			
	
			if( $data[$i]['mr_work_date_sent'] ){
			list( $y, $m, $d ) = explode("-", $data[$i]['mr_work_date_sent']);
			$date_order[$i]['mr_work_date_sent'] = $d."/".$m."/".$y;
			}	
			
			if( $data[$i]['mr_work_date_success'] ){
			list( $y, $m, $d ) = explode("-", $data[$i]['mr_work_date_success']);
			$date_meet[$i]['mr_work_date_success'] = $d."/".$m."/".$y;
			}	
			
			if(  $data[$i]['name_re'] ){
				$name_receive[$i]['name_re'] 				= $data[$i]['name_re']." ".$data[$i]['lastname_re'];
				$name_receive[$i]['depart_receive'] 		= $data[$i]['depart_code_receive']." - ".$data[$i]['depart_name_receive']." ชั้น ".$data[$i]['depart_floor_receive'];
				
			}			
			
			if(  $data[$i]['mr_emp_name'] ){
				$name_send[$i]['send_name'] 				= $data[$i]['mr_emp_name']." ".$data[$i]['mr_emp_lastname'];
				$name_send[$i]['depart_send'] 				= $data[$i]['mr_department_code']." - ".$data[$i]['mr_department_name']." ชั้น ".$data[$i]['depart_floor_send'];
				
			}		
	
	
	
	
	$datas[$i] = array(
			$i+1,
				$data[$i]['mr_work_barcode'],
				$name_receive[$i]['name_re'], 	
				$name_receive[$i]['depart_receive'],	
				$name_send[$i]['send_name'],
				$name_send[$i]['depart_send'],
				$data[$i]['mr_topic'],
				$date_order[$i]['mr_work_date_sent'],
				$date_meet[$i]['mr_work_date_success'],
				$data[$i]['mr_status_name'],
				$data[$i]['mr_type_work_name'],
				$data[$i]['mr_work_remark']
				
		);
	    
	}

//echo "<pre>".print_r( $data , true )."</pre>";
//exit();








header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$filename.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
// header('Cache-Control: max-age=0');

$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
ini_set("memory_limit",'-1');
//$writer->writeSheetHeader($sheet,$header);
$writer->writeSheetRow($sheet,$head,$styleHead);
foreach ($datas as $v) 
	$writer->writeSheetRow($sheet,$v,$styleRow);



$writer->writeToStdOut();
exit(0);

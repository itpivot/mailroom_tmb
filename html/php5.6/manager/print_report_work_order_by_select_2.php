<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$userDao 			= new Dao_User();

$mess_id = $req->get('mess_id');


$mess_id = $userDao->getMessPrint( $mess_id );

//echo "<pre>".print_r($mess_id,true)."</pre>";
//exit();	


$index = 1;
foreach ($mess_id as $k => $v) {
	$work_order = $work_inoutDao->printMessSendWork($v['mr_user_id']);
	
	$ddd = count( $work_order );
	
	//echo "<pre>".print_r($work_order,true)."</pre>";
	//exit();
	foreach ($work_order as $key => $value) {
		
		if(($index * 1) == $k){
			$index+=1;
		}
		
		
		$report[$index]['mr_emp_name'] = $v['mr_emp_name'];
		$report[$index]['mr_emp_lastname'] = $v['mr_emp_lastname'];
		$report[1]['countindex'] = $index;
		
		
		$report[$index]['page'] = $index;
		$report[$index]['data'][$key] = $value;
		$report[$index]['data'][$key]['number'] = $key+1;
		
		
		
		//echo "<pre>".print_r($report,true)."</pre>";
		
		//if($value['scg_delivery_round'] == ""){
		//$maxRound = $work_order_dao->getMaxRound();
		//}else{
		//$maxRound = $value['scg_delivery_round'];
		//}
	}
}


//echo "<pre>".print_r($work_order,true)."</pre>";




$template = Pivot_Template::factory('mailroom/print_report_work_order_selected_2.tpl');
$template->display(array(
	 //'debug' => print_r($report ,true),
	'date' => date("d-m-Y"),
	'time' => date("H:i:s"),
  'report' => $report,
  'round' => $maxRound,
  'user' => $getUser
));


 ?>

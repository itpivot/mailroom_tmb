<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();

$work_id = $req->get('work_id');
$user_pass = $auth->encryptAES($req->get('user_pass'));
$emp_code = $req->get('emp_id');

$sendData = $userDao->getempByEmpid( $emp_code );

 
 
//echo "<pre>".print_r($user_pass,true)."</pre>"; 
//exit();


if ( ( $emp_code == $sendData['mr_emp_code'] ) && ( $user_pass == $sendData['mr_user_password'] ) ) {
	
	$result = 1;
	
	// Save User ที่รับงานแทน
	$data_inout_save['mr_user_id'] = $sendData['mr_user_id'];
	$all_id = explode(",", $work_id);
	foreach($all_id as $kee => $v_data){
		$data = $work_inout_Dao->getWorkByMainIDAll($v_data);
		$work_id  = $data['mr_work_inout_id'];	
		//echo print_r($all_id,true);
		if($work_id!=''){
			$work_inout_Dao->save($data_inout_save,$work_id);
		}
	}
}else{
	$result = 0;
}


echo json_encode($result);
?>
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();

$user_id = $auth->getUser();

$sendData = $work_main_Dao->getReceiveWorkPostOut(intval($user_id));

foreach ($sendData as $key => $value) {
	$sendData[$key]['mr_work_date_sent'] = $work_inout_Dao->setDateFormat($value['mr_work_date_sent'], 2) ;
	$sendData[$key]['diff_time'] = $work_inout_Dao->spliceDateTime($value['sys_timestamp']) ;
}

/* echo "<pre>".print_r($sendData,true)."</pre>"; 
exit(); */

// echo "The time is " . date("h:i:sa");


echo json_encode($sendData);
?>
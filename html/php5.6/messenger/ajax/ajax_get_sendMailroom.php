<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();

$user_id = $auth->getUser();
$txt  =  $req->get('txt');



$sendData = $work_inout_Dao->getSendToReciever(intval($user_id),$txt);
foreach($sendData as $in_k => $val_k){
	//$sendData[$in_k]['mr_work_main_id'] = $val_k['mr_work_main_id'];
	$sendData[$in_k]['mr_work_main_id'] = urlencode(base64_encode($val_k['mr_work_main_id']));
	$sendData[$in_k]['work_main_id'] 	= $val_k['mr_work_main_id'];
}
//echo "<pre>".print_r($sendData,true)."</pre>"; 
//exit();

echo json_encode($sendData);
?>
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_byhand.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req                    = new Pivot_Request();
$userDao                = new Dao_User();
$userRoleDao            = new Dao_UserRole();
$work_main_Dao          = new Dao_Work_main();
$work_inout_Dao         = new Dao_Work_inout();
$employee_Dao           = new Dao_Employee();
$work_order_logs_Dao    = new Dao_Work_log();
$work_byhand_Dao        = new Dao_Work_byhand();

$userId     = $auth->getUser();
$empUser    = $userDao->getempByuserid($userId);
$empData    = $employee_Dao->getEmpDataById($empUser['mr_emp_id']);
$work_mainAll = json_decode($req->get('wId'), true);
try{
    foreach($work_mainAll as $k => $v) {
        $main_all[$k]['mr_status_id'] = 5;
        $main_all[$k]['messenger_user_id'] = $userId;
        //$main_all[$k]['mr_work_main_id'] = $v;
        //$inout_all[$k]['mr_status_receive'] = intval($action);
        //$inout_all[$k]['mr_work_main_id'] = $v;
        
        $dataByhand['send_remark']                      = $req->get('remark');
        $dataByhand['mr_work_byhand_status_send_id']    = $req->get('sender_status');
        $work_byhand_Dao->updateworkByhandByMainId($dataByhand, $v);

        $log_all[$k]['sys_timestamp']   = date('Y-m-d H:i:s');
        $log_all[$k]['mr_work_main_id'] = $v;
        $log_all[$k]['mr_user_id']      = $userId;
        $log_all[$k]['mr_status_id']    = 5;
        

        $resive_name    = $req->get('resive_name');
        $resive_lname   = $req->get('resive_lname');
        if($req->get('sender_status') == 1){
            if($resive_name!=''){
                $log_all[$k]['remark']          = "ส่งเอกสารถึงปลายทาง ผู้รับ :". $resive_name.'  '.$resive_lname;
            }else{
                echo json_encode(array(
                    'status' => 500,
                    'error' => 'กรุณาระชุ ชื่อ-ผู้รับ'
                ));
                exit;
            }
        }else{
            if( $req->get('remark') ==''){
                echo json_encode(array(
                    'status' => 500,
                    'error' => 'กรุณาระบุหมายเหตุ'
                ));
                exit; 
            }
        }

        $m_multi[] = $work_main_Dao->save($main_all[$k], $v);
       // $inout_multi[] = $work_byhand_Dao->updateWithMainId($inout_all[$k], $v);
        $logs_multi[] = $logs_Id = $work_order_logs_Dao->save($log_all[$k]);
        $signaturePath = '../../signatures';
        if(!is_dir($signaturePath)) {
            mkdir($signaturePath);
        }
        $signaturePath = '../../signatures/'.date("Y-m");
        if(!is_dir($signaturePath)) {
            mkdir($signaturePath);
        }
        $signature = $req->get('signature');
        $sign_data = base64_decode($signature);
    
        $filename = "${signaturePath}/${v}/${v}.jpg";
    
        if(!is_dir($signaturePath)) {
            mkdir($signaturePath);
        }
    
        if(!is_dir("${signaturePath}/${v}/")) {
            mkdir("${signaturePath}/${v}/");
        }
            
        $sender_status = $req->get('sender_status');
        if( $sender_status == 1){
            file_put_contents($filename, $sign_data);
        }else{
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $filename)) {

            }else {
                echo json_encode(array(
                    'status' => 500,
                    'error' => 'Sorry, there was an error uploading your file.'
                ));
            }
        }
        
    }
    echo json_encode(array(
        'status' => 200,
        'image' => str_replace('../../../', '../../', $filename),
        'message' => 'upload signature succss'
    ));
} catch(Exception $e) {
    echo json_encode(array(
        'status' => 500,
        'error' => $e->getMessage()
    ));
}







?>
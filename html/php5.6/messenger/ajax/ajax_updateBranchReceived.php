<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Confirm_Log.php';
require_once 'PHPMailer.php';


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();
$confirm_log_Dao = new Dao_Confirm_Log();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$user_id = $auth->getUser();

$type = $req->get('type');
$action = $req->get('action');
$wId = $req->get('wId');

$params = json_decode($req->get('data'), true);
$ins = array();
$workId = array();


$sendmail = new SendMail();


foreach($params as $key => $val) {
    if($val['name'] == "arrId[]") {
        array_push($workId, $val['value']);
    } else {
        $ins[$val['name']] = $val['value'];
    }
}


$getEmpId = $userDao->getempByEmpid($ins['username']);

if($ins['main_id'] != "") {
    $main['mr_status_id'] = 12;
    $main['mr_work_date_success'] = date('Y-m-d');
    $work_main_Dao->save($main, intval($ins['main_id']));

    $inout['mr_status_send'] = 1;
    $work_inout_Dao->updateInOutWithMainId($inout, $ins['main_id']);

    $logs['sys_timestamp'] = date('Y-m-d H:i:s');
    $logs['mr_work_main_id'] = $ins['main_id'];
    $logs['mr_user_id'] = $user_id;
    $logs['mr_status_id'] = 12;
    $resp = $work_log_Dao->save($logs);

    $datasByEmail = $work_main_Dao->getDatasByEmail(intval($ins['main_id']));

    $barcode			=  $datasByEmail['barcode'];
    $send_name			=  $datasByEmail['send_name'];
    $send_lastname		=  $datasByEmail['send_lastname'];
    $send_email			=  $datasByEmail['send_email'];
    $reciever_name		=  $datasByEmail['reciever_name'];
    $reciever_lastname  =  $datasByEmail['reciever_lastname'];

    $Subjectmail = "รายการส่งเอกสารเลขที่ $barcode ผู้รับได้รับเอกสารเรียบร้อยแล้ว ";
    $body = "";
    $body .= "เรียน คุณ <b>$send_name $send_lastname </b><br><br>";
    $body .= "รายการส่งเอกสารเลขที่  <b> $barcode  </b> ผู้รับ <b>$reciever_name $reciever_lastname </b> $result_recheck<br>";
    $body .= "ได้รับเอกสารเรียบร้อยแล้วค่ะ<br>";
    $body .= "<br>";
    $body .= "Pivot MailRoom Auto-Response Message <br>";
    $body .= "(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";
    //$result_mail = $sendmail->mailNotice($body,$Subjectmail, $send_email);

    // $result_mail = "success";
    //if(($resp != "") && $result_mail == "success") {
    if($resp != "") {
        $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
        $cfLogs['mr_work_main_id'] = intval($ins['main_id']);
        $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
        $cfLogs['mr_status'] = 'Success';
        $cfLogs['descriptions'] = "Receive Success";
        
        $success = $confirm_log_Dao->save($cfLogs);

    
    } else {
        $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
        $cfLogs['mr_work_main_id'] = intval($ins['main_id']);
        $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
        $cfLogs['mr_status'] = 'Failed';
        $cfLogs['descriptions'] = "Receive Failed";
        
        $failed = $confirm_log_Dao->save($cfLogs);
        
    }

    if(isset($success)) {
        $send_data = 'success';
    } else {
        $send_data = 'failed';
    }
} else {

    foreach($workId as $k => $v) {
        $main_all['mr_status_id'] = 12;
        $main_all['mr_work_date_success'] = date('Y-m-d');
        $work_main_Dao->save($main_all, intval($v));

        $inout_all['mr_status_send'] = 1;
        $work_inout_Dao->updateInOutWithMainId($inout_all, $v);

        $logs['sys_timestamp'] = date('Y-m-d H:i:s');
        $logs['mr_work_main_id'] = $v;
        $logs['mr_user_id'] = $user_id;
        $logs['mr_status_id'] = 12;
        $resp = $work_log_Dao->save($logs);

        $datasByEmail = $work_main_Dao->getDatasByEmail(intval($v));

        $barcode			=  $datasByEmail['barcode'];
        $send_name			=  $datasByEmail['send_name'];
        $send_lastname		=  $datasByEmail['send_lastname'];
        $send_email			=  $datasByEmail['send_email'];
        $reciever_name		=  $datasByEmail['reciever_name'];
        $reciever_lastname  =  $datasByEmail['reciever_lastname'];


        $Subjectmail = "รายการส่งเอกสารเลขที่ $barcode ผู้รับได้รับเอกสารเรียบร้อยแล้ว ";
        $body = "";
        $body .= "เรียน คุณ <b>$send_name $send_lastname </b><br><br>";
        $body .= "รายการส่งเอกสารเลขที่  <b> $barcode  </b> ผู้รับ <b>$reciever_name $reciever_lastname </b> $result_recheck<br>";
        $body .= "ได้รับเอกสารเรียบร้อยแล้วค่ะ<br>";
        $body .= "<br>";
        $body .= "Pivot MailRoom Auto-Response Message <br>";
        $body .= "(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";
       // $result_mail = $sendmail->mailNotice($body,$Subjectmail, $send_email);
       
        //if(($resp != "") && $result_mail == "success") {
        if($resp != "") {
            $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
            $cfLogs['mr_work_main_id'] = intval($v);
            $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
            $cfLogs['mr_status'] = 'Success';
            $cfLogs['descriptions'] = "Receive Success";
            
            $success[] = $confirm_log_Dao->save($cfLogs);

        
        } else {
            $cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
            $cfLogs['mr_work_main_id'] = intval($v);
            $cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
            $cfLogs['mr_status'] = 'Failed';
            $cfLogs['descriptions'] = "Receive Failed";
            
            $failed[] = $confirm_log_Dao->save($cfLogs);
            
        }

    }

}

if(isset($success)) {
    $send_data = 'success';
} else {
    $send_data = 'failed';
}

echo $send_data;
// echo "<pre>".print_r($ins, true)."</pre>";
// echo "<pre>".print_r($workId, true)."</pre>";
?>
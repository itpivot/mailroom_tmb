<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
    exit;
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();

$id = $req->get('id');

//$data_inout         = $work_inout_Dao->getWorkByMainID($id);
$mr_work_main_id    = $id;

// echo "<pre>".print_r($data_inout,true)."</pre>"; 
// exit();


$data['mr_status_id']          = 16;
$resp = $work_main_Dao->save($data,$mr_work_main_id);
if($resp != ""){
    $logs['sys_timestamp']      = date('Y-m-d H:i:s');
    $logs['mr_work_main_id']    = $mr_work_main_id;
    $logs['mr_user_id']         = $user_id;
    $logs['mr_status_id']       = 16;
    $logs['remark']             = 'พนักงานเข้ารับเอกสาร(ไม่พบเอกสาร)';
    $resp = $work_log_Dao->save($logs);
}




if($resp != "") {
    echo "success";
} else {
    echo "false";
}

?>
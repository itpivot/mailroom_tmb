<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();

$id = $req->get('id');
$work_status = $req->get('work_status');

//echo print_r($work_status);
//exit();

if( $work_status == 4 ){
    // Messenger รับงานจากห้อง MailRoom
    $mr_status_id = 4;
}elseif ( $work_status == 1 ) {
    // Messenger ยกเลิกงานจาก User
    $mr_status_id = 6;
}else{
    // Messenger รับงานจาก User
    $mr_status_id = 2;
}


$data['mr_status_id'] = $mr_status_id;
//$data['mr_work_date_success'] = date('Y-m-d');

$w_save_main = $work_main_Dao->save($data, intval($id));



$data_inout = $work_inout_Dao->getWorkByMainID($id);
$data_save_inout['mr_status_receive'] = 1;
$work_inout_Dao->save($data_save_inout,$data_inout);



if($w_save_main != "") {
    $logs['sys_timestamp'] = date('Y-m-d H:i:s');
    $logs['mr_work_main_id'] = $w_save_main;
    $logs['mr_user_id'] = $user_id;
    $logs['mr_status_id'] = $mr_status_id;
    $resp = $work_log_Dao->save($logs);
	
	
	
	
	
}

if($resp != "") {
    echo "success";
} else {
    echo "false";
}

?>
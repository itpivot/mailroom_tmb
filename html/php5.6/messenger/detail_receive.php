<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_inout.php';


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$work_inout_Dao = new Dao_Work_inout();


$barcode = $req->get('barcode');

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$work_inout_data = $work_inout_Dao->getUserReceive( $barcode );

$user_data = $userDao->getEmpSignByUserID( $work_inout_data['mr_user_id'] );



//echo "<pre>".print_r($work_inout_data,true)."</pre>";


$template = Pivot_Template::factory('messenger/detail_receive.tpl');
$template->display(array(
	//'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'barcode' => $barcode,
	'work_inout_data' => $work_inout_data,
	'userRoles' => $userRoles,
	'users' => $users,
	'user_data' => $user_data,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
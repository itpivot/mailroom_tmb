<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$date_s = date('Y-m-d',strtotime("-1 days"));
$date_e = date('Y-m-d');
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);


//echo $date_s;
$template = Pivot_Template::factory('messenger/mailroom_receive_list_Thaipost_IN.tpl');
$template->display(array(
	//'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	//'success' => $success,
	'userRoles' => $userRoles,
	'users' => $users,
	'user_data' => $user_data,
	'date_s' => $date_s,
	'date_e' => $date_e,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));

?>
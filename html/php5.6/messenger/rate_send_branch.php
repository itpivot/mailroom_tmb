<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();


$id = $req->get('id');
$w_main = $work_main_Dao->getBarcodeByid(intval($id));
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$template = Pivot_Template::factory('messenger/rate_send_branch.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'user_data' => $user_data,
	'userRoles' => $userRoles,
	'users' => $users,
	'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles(),
    'send_id' => $id,
	'barcode' => $w_main['mr_work_barcode'],
	'serverPath' => $_CONFIG->site->serverPath
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Status.php';
require_once 'Dao/Building.php';
require_once 'Dao/Messenger.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_order.php';
require_once 'Dao/Work_log.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$statusDao 		= new Dao_Status();
$buildingDao 	= new Dao_Building();
$messengerDao 	= new Dao_Messenger();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$workOrderDao 	= new Dao_Work_order();
$workLogDao 	= new Dao_Work_log();


$scg_work_order_id	=	$req->get('scg_work_order_id');


$search['scg_messenger_id']			=	$messengerDao->getMessengerByUserID( $auth->getUser() ) ;
$search['barcode']			=	$req->get('barcode');
$search['scg_status_id']	=	$req->get('scg_status_id');
$search['search_date']		=	$workOrderDao->setDateFormat( $req->get('search_date'), 1 ) ;



if( $scg_work_order_id != '' )
{
	$workLogData['scg_work_order_id'] = $scg_work_order_id ;
	$workLogData['scg_status_id'] = 3 ;
	$workLogData['scg_messenger_id'] = $messengerDao->getMessengerByUserID( $auth->getUser() ) ;
	$workLogDao->save($workLogData) ;
}

$buildingDatas		=	$workOrderDao->getWorkOrderAll($search);

$today 				= date('d/m/Y');

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$status	=	$statusDao->fetchAll();
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$template = Pivot_Template::factory('messenger/report.tpl');
$template->display(array(
	'debug' => print_r($search,true),
	'buildingDatas' => $buildingDatas,
	'today' => $today,
	'users' => $users,
	'user_data' => $user_data,
	'status' => $status,
	'userRoles' => $userRoles,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
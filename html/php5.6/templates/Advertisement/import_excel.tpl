{% extends "base.tpl" %}

{% block title %}{% endblock %} 
{% block scriptImport %}
{% endblock %}

{% block styleReady %}
{% endblock %}

{% block domReady %}

	$('#btn_upload').on('click',function() {
		var data = $('#txt_upload').prop("files");
		var form_val = $('#txt_upload').prop("files")[0];
		// console.log(data);
		if($('#txt_upload').val() == ""){
			alert('Please Upload File');
		}else{
			var file_type = data[0]['type'];
			var form_data = new FormData();
			form_data.append('file', form_val);
			if(data[0]['type'] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || data[0]['type'] == "application/vnd.ms-excel"){
					$.ajax({
					url: 'read_excel.php',
               		cache: false,
                	contentType: false,
                	processData: false,
                	data: form_data,                         
                	type: 'post',
					
			}).done(function(res){
				console.log(res);
			});
			}else{
				alert("Not Support");
			}
		}
		
	});
{% endblock %}

{% block javaScript %}
{% endblock %}
	
{% block content %}
	<div class="row">
		<div class="page-header">
			<h1>Import Excel</h1>
		</div>

		<form action="" method="POST" id="formUpload" enctype="multipart/form-data" accept-charset="utf-8" >
			<div class="form-group">
				<label for="txt_upload">File Uplaod</label>
				<input type="file" name="txt_upload" id="txt_upload">
			</div>
			<button type="button" class="btn btn-primary" name="btn_upload" id="btn_upload"><i class="glyphicon glyphicon-open"></i> UPLOAD</button>
		</form>
	</div>
{% endblock %}


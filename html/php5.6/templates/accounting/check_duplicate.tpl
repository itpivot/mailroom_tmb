{% extends "base.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_ac2 %} active {% endblock %}

{% block javaScript %}
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	function updateStatus( scg_work_order_id )
	{
		 window.location.href = 'work.php?scg_work_order_id=' + scg_work_order_id ;
	}

	function scanBarcode()
	{
		 window.location.href = 'work_scan_barcode.php';
	}



{% endblock %}
{% block styleReady %}
{% endblock %}
{% block Content %}
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-center">เลขที่ EMS ซ้ำกัน</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="table-responsive">
                <table class="table table-bordered table-hover" id="tb_route">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-left">Barcode</th>
                            <th class="text-center">Count</th>
                            <th class="text-center">ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% for d in duplicate %}
                        <tr>
                            <td class="text-center">{{ loop.index }}</td>
                            <td class="text-left">{{ d.ems_barcode }}</td>
                            <td class="text-center">{{ d.count }}</td>
                            <td class="text-center">{{ d.id }}</td>
                        </tr>
                        {% endfor %}
                    </tbody>
                </table>
            </div>
            <a href="sender_post_report.php">
                <button type="button" class="btn btn-default btn-block"> Back </button>
            </a>
		</div>

	</div>
{% endblock %}
{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

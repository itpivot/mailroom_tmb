{% extends "base.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_ac1 %} active {% endblock %}

{% block javaScript %}
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	function updateStatus( scg_work_order_id )
	{
		 window.location.href = 'work.php?scg_work_order_id=' + scg_work_order_id ;
	}

	function scanBarcode()
	{
		 window.location.href = 'work_scan_barcode.php';
	}



{% endblock %}
{% block styleReady %}
{% endblock %}
{% block Content %}
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-center">Menu</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<a href="../mailroom/report_sender_entry_data.php" ><button type="button" class="btn btn-default btn-lg btn-block">Report Work Order</button></a>
			<a href="accounting_report.php" ><button type="button" class="btn btn-default btn-lg btn-block">Report Total Cost</button></a>
			<a href="report_kerry.php" ><button type="button" class="btn btn-default btn-lg btn-block">Report Kerry</button></a>
			<a href="report_dhl.php" ><button type="button" class="btn btn-default btn-lg btn-block">Report DHL</button></a>
		</div>
	</div>
{% endblock %}
{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base.tpl" %}
{% block title %}{% parent %} - List{% endblock %}
{% block menu_ac2 %} active {% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="css/chosen.css">

<script src="../themes/bootstrap/js/jquery-1.12.4.js"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>
{% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .txtCode {
        width: 100px;
        height: 24px;
        font-size: 15px;
        padding-left: 10px;
        border: 1px solid #aaa;
        border-radius: 5px;
      }
      .txtInputs {
        width: 300px;
        height: 24px;
        font-size: 15px;
        padding-left: 10px;
        border: 1px solid #aaa;
        border-radius: 5px;
      }
      .fieldset {
        border: 1px solid #ccc;
        padding: 10px;
        padding-top : 0px;
      }
      .panel{
        background-color:#eee;
        padding:5px;
        margin-bottom: 5px;
      }
      .tb_Inputs {
        margin-left: 120px;
      }
      .tb_Inputs tr > td{
        padding: 5px 5px 5px 5px;
      }
      .txtReadOnly {
        width: 300px;
        height: 24px;
        font-size: 15px;
        padding-left: 10px;
        border: 1px solid #aaa;
        border-radius: 5px;
        background-color: #eee;
        cursor: not-allowed;
      }

      .btns {
        width: auto;
        height: 23px;
        border: 1px solid #b6b4b4;
        padding-left: 10px;
        padding-right: 10px;
        font-size: 15px;
      }
      .btn_save {
        width: auto;
        height: 33px;
        border: 1px solid #b6b4b4;
        position: absolute;
        right: 10px;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      input[type=text]:focus {
            background-color: #ffffbf;
      }

      #tb_work_order {
        font-size: 13px;
      }



      @media only screen and (orientation:portrait){
            .txtInputs {
              width: 200px;
            }
      }

	  .panel {
		margin-bottom : 5px;
	  }


{% endblock %}
{% block domReady %}
<!-- ON LOAD ACTIVITY -->
 $("#txt_barcode").focus();
$('#lst_status').chosen();
$('#lst_edit_file_type').chosen({width: "196px"});

$('.input-daterange').datepicker({
  autoclose: true,
  clearBtn: true
});

$("#btn_reset").click(function(){
  table.rows('.selected').remove().draw( false );
   $("#txt_barcode").val("");
  $("#txt_code").val("");
  $("#lst_building").val("").trigger("chosen:updated");
  $("#lst_floor").val("").trigger("chosen:updated");
  $("#txt_receiver_name").val("");
  $("#txt_sender_name").val("");
  $("#start_date").val("");
  $("#end_date").val("");
  $("#lst_status").val("").trigger("chosen:updated");
  $("#txt_round").val("");
  table.destroy();
  table = $("#tb_work_order").DataTable({
    "ajax": {
        "url": "ajax/ajax_load_work_order_search.php",
        "type": "POST"
    },
    'columns': [
          { 'data':'scg_work_order_id' },
          { 'data':'system_date'},
          { 'data':'barcode' },
          { 'data':'scg_sender_name' },
          { 'data':'scg_sender_building_name' },
          { 'data':'scg_sender_building_floor_name' },
          { 'data':'scg_receiver_name' },
          { 'data':'scg_receiver_building_name' },
          { 'data':'scg_receiver_building_floor_name' },
          { 'data':'scg_delivery_round' },
          { 'data':'remark' },
          { 'data':'scg_file_type' },
          { 'data':'scg_status_name' },
          { 'data':'fullnames' }
    
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
  		 if (aData['scg_status_name'] == "Newly") {
  				 $(nRow).addClass('hilight');
  		 }
       else{
         $(nRow).removeClass('hilight');
       }
  		},
    'order': [[ 0, "desc" ]],
    'scrollCollapse': true
  });

});
<!-- ON LOAD ACTIVITY -->
<!-- DATA TABLES -->
var table = $("#tb_work_order").DataTable({
  "ajax": {
      "url": "ajax/ajax_load_work_order_search.php",
      "type": "POST"
  },
  'columns': [
        { 'data':'scg_work_order_id' },
          { 'data':'system_date'},
          { 'data':'barcode' },
          { 'data':'scg_sender_name' },
          { 'data':'scg_sender_building_name' },
          { 'data':'scg_sender_building_floor_name' },
          { 'data':'scg_receiver_name' },
          { 'data':'scg_receiver_building_name' },
          { 'data':'scg_receiver_building_floor_name' },
          { 'data':'scg_delivery_round' },
          { 'data':'remark' },
          { 'data':'scg_file_type' },
          { 'data':'scg_status_name' },
          { 'data':'fullnames' }
    
  ],
  "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		 if (aData['scg_status_name'] == "Newly") {
				 $(nRow).addClass('hilight');
		 }
     else{
       $(nRow).removeClass('hilight');
     }
		},
    'order': [[ 0, "desc" ]],
    'scrollCollapse': true
});
  <!-- DATA TABLES -->
<!-- select rows -->
$('#tb_work_order tbody').on("click","tr",function() {
  $(this).toggleClass('selected');
  var row_data = table.row(this).data();
});
<!-- select rows -->

<!-- DATA TABLES -->
<!-- List Building  and List Floor -->
$("#lst_building").chosen().change(function() {
  $.ajax({
    url: "ajax/ajax_select_building_floor.php",
    type: "post",
    data: {
        'building': this.value
    },
    dataType: "json",
    success: function(res){
      console.log(res);
      $('#lst_floor').empty();
        for(var i = 0; i < res.length;i++){

          $('#lst_floor').append($('<option></option>',
          {
            value: res[i]['scg_building_floor_id'],
            text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
          }))
        }
        <!-- $('#lst_floor').trigger("chosen:updated"); -->
        $('#lst_floor').val(0).trigger("chosen:updated");
    }
  })
});
$("#lst_floor").chosen();
<!-- List Building  and List Floor -->
<!-- Automatic Code -->
$('#txt_code').keydown(function(event){
    var keycode = event.keyCode || event.which;
    if(keycode == 9 || keycode == 13){
      var txt = $('#txt_code').val();
      var count_txt = txt.length;
      if(count_txt <= 1){
        $('#alert_building').html("รหัสไม่ถูกต้อง").css('color','red');
        $('#txt_code').focus();
      }
      if(count_txt == 2){
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_building.php',
          data: {
            'building': txt,
          },
          dataType: 'json',
          success: function(res){
              <!-- console.log(res.length); -->
              if(res.length != 0){
                var office = res[0]['scg_building_office_id'];
                $('#lst_building').val(office).trigger("chosen:updated");
                getFloor(office);
                $("#lst_floor").focus();
              }else{
                $('#alert_building').html("รหัสไม่ถูกต้อง").css('color','red');
                $('#txt_code').focus();
              }
          }
        });
      }
      if(count_txt == 3){
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_building.php',
          data: {
            'building': txt,
          },
          dataType: 'json',
          success: function(res){
              <!-- console.log(res.length); -->
              if(res.length != 0){
                var office = res[0]['scg_building_office_id'];
                $('#lst_building').val(office).trigger("chosen:updated");
                getFloor(office);
                $("#lst_floor").focus();
              }else{
                $('#alert_building').html("รหัสไม่ถูกต้อง").css('color','red');
                $('#txt_code').focus();
              }
          }
        });
      }
      if(count_txt == 5){
        var b_code = txt.substring(0,3);
        var f_code = txt.substring(3);

        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_location.php',
          data: {
            'building': b_code,
            'floor': f_code
          },
          dataType: 'json',
          success: function(res){
            if(res['floor'].length != 0 && res['building'].length != 0){
                var office = res['building'][0]['scg_building_office_id'];
                var floor = res['floor'][0]['scg_building_floor_id'];
                $('#lst_building').val(office).trigger("chosen:updated");
                getFloor(office, floor);
              $('#alert_building').html("");
              $("#txt_receiver_name").focus();
            }else{
                $('#alert_building').html("รหัสไม่ถูกต้อง").css('color','red');
                $('#txt_code').focus();
            }

          }
        });
      }
      if(count_txt == 4){
        var b_code = txt.substring(0,2);
        var f_code = txt.substring(2);

        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_location.php',
          data: {
            'building': b_code,
            'floor': f_code
          },
          dataType: 'json',
          success: function(res){
            if(res['floor'].length != 0 && res['building'].length != 0){
                  var office = res['building'][0]['scg_building_office_id'];
                  var floor = res['floor'][0]['scg_building_floor_id'];
                  $('#lst_building').val(office).trigger("chosen:updated");
                  getFloor(office, floor);
                  $('#alert_building').html("");
                  $("#txt_receiver_name").focus();
            }else{
                  $('#alert_building').html("รหัสไม่ถูกต้อง").css('color','red');
                  $('#txt_code').focus();
            }
          }
        });
      }
    }
});
<!-- Automatic Code -->
$('#btn_print_selected').click(function() {
  var print_data = new Array();
  for (var i = 0; i < table.rows('.selected').data().length; i++) {
     <!-- console.log( table.rows('.selected').data()[i]); -->
     print_data.push(table.rows('.selected').data()[i]['scg_work_order_id']);
  }
  if(print_data.length <= 0){
    alert("ท่านยังไม่ได้เลือกรายการ");
  }else{
    var order = print_data;
    console.log(order);
    var orders = JSON.stringify(order);
    window.open('print_report_work_order_by_select.php?orders='+orders);
  }
});
<!-- PRINT -->
$("#btn_close_job").click(function() {

  var print_data = new Array();
  for (var i = 0; i < table.rows('.selected').data().length; i++) {
     <!-- console.log( table.rows('.selected').data()[i]); -->
     print_data.push(table.rows('.selected').data()[i]['scg_work_order_id']);
  }
  <!-- table.rows().data().length // Count Row In Datatable -->
  if(print_data.length <= 0){
    alert("ท่านยังไม่ได้เลือกรายการ");
  }else{
    var order = print_data;
    <!-- var order = JSON.stringify(print_data); -->
    <!-- console.log(order); -->
    $.post('ajax/ajax_update_work_order_status_by_select.php',{ order : order }, function(res){
             table.ajax.reload();
    });
    <!-- Update Round -->
  }
  <!-- Send Data to php -->

});

$("#btn_print").click(function() {
      var data = new Object();
      data['barcode'] = $("#txt_barcode").val();
      data['building'] = $("#lst_building").val();
      data['floor'] = $("#lst_floor").val();
      data['receiver'] =  $("#txt_receiver_name").val();
      data['sender'] = $("#txt_sender_name").val();
      data['start_date'] = $("#start_date").val();
      data['end_date'] = $("#end_date").val();
      data['status'] = $("#lst_status").val();
      data['round'] = $('#txt_round').val();
      data['user_id'] = $("#user_id").text();
      var param = JSON.stringify(data);

      window.open('print_report_work_order_rount.php?params='+param);

});
<!-- PRINT -->
<!-- Search -->
$("#btn_search").click(function(){
  var barcode = $("#txt_barcode").val();
  var building = $("#lst_building").val();
  var floor = $("#lst_floor").val();
  var receiver =  $("#txt_receiver_name").val();
  var sender = $("#txt_sender_name").val();
  var start_date = $("#start_date").val();
  var end_date = $("#end_date").val();
  var status = $("#lst_status").val();
  var round = $("#txt_round").val();
  table.destroy();
  table = $("#tb_work_order").DataTable({
    "ajax": {
        "url": "ajax/ajax_load_work_order_search.php",
        "type": "POST",
        "data": {
          'barcode': barcode,
          'building': building,
          'floor': floor,
          'receiver': receiver,
          'sender': sender,
          'start_date': start_date,
          'end_date': end_date,
          'status': status,
          'round': round
        }
    },

    'columns': [
          { 'data':'scg_work_order_id' },
          { 'data':'system_date'},
          { 'data':'barcode' },
          { 'data':'scg_sender_name' },
          { 'data':'scg_sender_building_name' },
          { 'data':'scg_sender_building_floor_name' },
          { 'data':'scg_receiver_name' },
          { 'data':'scg_receiver_building_name' },
          { 'data':'scg_receiver_building_floor_name' },
          { 'data':'scg_delivery_round' },
          { 'data':'remark' },
          { 'data':'scg_file_type' },
          { 'data':'scg_status_name' },
          { 'data':'fullnames' }
    
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
  		 if (aData['scg_status_name'] == "Newly") {
  				 $(nRow).addClass('hilight');
  		 }
       else{
         $(nRow).removeClass('hilight');
       }
  		},
      'order': [[ 0, "desc" ]],
      'scrollCollapse': true
  });
});
<!-- end Search -->
<!-- test -->
$("#test_search").click(function(){
  var barcode = $("#txt_barcode").val();
  var building = $("#lst_building").val();
  var floor = $("#lst_floor").val();
  var receiver =  $("#txt_receiver_name").val();
  var sender = $("#txt_sender_name").val();
  var start_date = $("#start_date").val();
  var end_date = $("#end_date").val();
  var status = $("#lst_status").val();
  var round = $("#txt_round").val();
  $.ajax({
    url: 'ajax/ajax_load_work_order_search.php',
    type: 'post',
    data: {
      'barcode': barcode,
      'building': building,
      'floor': floor,
      'receiver': receiver,
      'sender': sender,
      'start_date': start_date,
      'end_date': end_date,
      'status': status,
      'round': round
    },
    dataType: 'json',
    success: function(res){
      console.log(res);
    }
  })
});

$("#btn_reprint_mixed").click(function() {
  var building_id = new Array();
  for(var i = 0; i < table.rows('.selected').data().length; i++){
    building_id.push(table.rows('.selected').data()[i]['scg_work_order_id']);
  }
  if(building_id.length <= 0){
    alert("ท่านยังไม่ได้เลือกรายการ");
  }
  else{
    console.log(building_id);
    var order = building_id;
    var orders = JSON.stringify(order);
    window.open('print_report_work_order_by_select.php?orders='+orders);
  }
});

$('#btn_reprint_separate').click(function() {
  var building_id = new Array();
  for(var i = 0; i < table.rows('.selected').data().length; i++){
    building_id.push(table.rows('.selected').data()[i]['scg_work_order_id']);
  }
  if(building_id.length <= 0){
    alert("ท่านยังไม่ได้เลือกรายการ");
  }
  else{
    console.log(building_id);
    var order = building_id;
    var orders = JSON.stringify(order);
    window.open('print_report_work_order_by_select_2.php?orders='+orders);
  }
});



<!-- lst Edit Sender -->
$('#lst_edit_sender_building').chosen({width: "196px"}).change(function() {
  $.ajax({
    url: "ajax/ajax_select_building_floor.php",
    type: "post",
    data: {
        'building': this.value
    },
    dataType: "json",
    success: function(res){

      $('#lst_edit_sender_floor').empty();
        for(var i = 0; i < res.length;i++){

          $('#lst_edit_sender_floor').append($('<option></option>',
          {
            value: res[i]['scg_building_floor_id'],
            text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
          }))
        }
        $('#lst_edit_sender_floor').trigger("chosen:updated");
    }
  })
});
$('#lst_edit_sender_floor').chosen({width: "196px"});
<!-- lst Edit Sender -->
<!-- lst Edit Receiver -->
$('#lst_edit_receiver_building').chosen({width: "196px"}).change(function() {
  $.ajax({
    url: "ajax/ajax_select_building_floor.php",
    type: "post",
    data: {
        'building': this.value
    },
    dataType: "json",
    success: function(res){

      $('#lst_edit_receiver_floor').empty();
        for(var i = 0; i < res.length;i++){

          $('#lst_edit_receiver_floor').append($('<option></option>',
          {
            value: res[i]['scg_building_floor_id'],
            text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
          }))
        }
        $('#lst_edit_receiver_floor').trigger("chosen:updated");
    }
  })
});
$('#lst_edit_receiver_floor').chosen({width: "196px"});
<!-- lst Edit Receiver -->
<!-- lst Edit sender Code -->
$('#txt_edit_sender_code').keydown(function(event){
    var keycode = event.keyCode || event.which;
    if(keycode == 9 || keycode == 13){
      var txt = $('#txt_edit_sender_code').val();
      var count_txt = txt.length;
      if(count_txt <= 1){
        $('#alert_sender_building').html("รหัสไม่ถูกต้อง").css('color','red');
        $('#txt_edit_sender_code').focus();
      }
      if(count_txt == 2){
        <!-- console.log(txt); -->
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_building.php',
          data: {
            'building': txt,
          },
          dataType: 'json',
          success: function(res){
              <!-- console.log(res.length); -->
              if(res.length != 0){
                $("#lst_edit_sender_floor").focus();
                var office = res[0]['scg_building_office_id'];
                $('#lst_edit_sender_building').val(office).trigger("chosen:updated");
                 getEditSenderFloor(office);
              }else{
                $('#alert_sender_building').html("รหัสไม่ถูกต้อง").css('color','red');
                $('#txt_edit_sender_code').focus();
              }
          }
        });
      }
      if(count_txt == 3){
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_building.php',
          data: {
            'building': txt,
          },
          dataType: 'json',
          success: function(res){
              <!-- console.log(res.length); -->
              if(res.length != 0){
                var office = res[0]['scg_building_office_id'];
                $('#lst_edit_sender_building').val(office).trigger("chosen:updated");
                getEditSenderFloor(office);
                $("#lst_edit_sender_floor").focus();
              }else{
                $('#alert_sender_building').html("รหัสไม่ถูกต้อง").css('color','red');
                $('#txt_edit_sender_code').focus();
              }
          }
        });
      }
      if(count_txt == 5){
        var b_code = txt.substring(0,3);
        var f_code = txt.substring(3);
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_location.php',
          data: {
            'building': b_code,
            'floor': f_code
          },
          dataType: 'json',
          success: function(res){
            if(res['floor'].length != 0 && res['building'].length != 0){
            var office = res['building'][0]['scg_building_office_id'];
            var floor = res['floor'][0]['scg_building_floor_id'];
            $('#lst_edit_sender_building').val(office).trigger("chosen:updated");
            getEditSenderFloor(office, floor);
              $('#alert_sender_building').html("");
               $("#txt_edit_sender_name").focus();
            }else{
                  $('#alert_sender_building').html("รหัสไม่ถูกต้อง").css('color','red');
                  $('#txt_edit_sender_code').focus();
            }

          }
        });
      }
      if(count_txt == 4){
        var b_code = txt.substring(0,2);
        var f_code = txt.substring(2);
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_location.php',
          data: {
            'building': b_code,
            'floor': f_code
          },
          dataType: 'json',
          success: function(res){
            if(res['floor'].length != 0 && res['building'].length != 0){
                  var office = res['building'][0]['scg_building_office_id'];
                  var floor = res['floor'][0]['scg_building_floor_id'];
                  $('#lst_edit_sender_building').val(office).trigger("chosen:updated");
                  getEditSenderFloor(office, floor);
                  $('#alert_sender_building').html("");
                  $("#txt_edit_sender_name").focus();
            }else{
                  $('#alert_sender_building').html("รหัสไม่ถูกต้อง").css('color','red');
                  $('#txt_edit_sender_code').focus();
            }

          }
        });
      }
    }
});
<!-- lst Edit sender Code -->

<!-- lst Edit Receiver Code -->
$('#txt_edit_receiver_code').keydown(function(event){
    var keycode = event.keyCode || event.which;
    if(keycode == 9 || keycode == 13){
      var txt = $('#txt_edit_receiver_code').val();
      var count_txt = txt.length;
      if(count_txt <= 1){
        $('#alert_receiver_building').html("รหัสไม่ถูกต้อง").css('color','red');
        $('#txt_edit_receiver_code').focus();
      }
      if(count_txt == 2){
        <!-- console.log(txt); -->
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_building.php',
          data: {
            'building': txt,
          },
          dataType: 'json',
          success: function(res){
              <!-- console.log(res.length); -->
              if(res.length != 0){
                $("#lst_edit_receiver_floor").focus();
                var office = res[0]['scg_building_office_id'];
                $('#lst_edit_receiver_building').val(office).trigger("chosen:updated");
                 getEditReceiverFloor(office);
              }else{
                $('#alert_receiver_building').html("รหัสไม่ถูกต้อง").css('color','red');
                $('#txt_edit_receiver_code').focus();
              }
          }
        });
      }
      if(count_txt == 3){
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_building.php',
          data: {
            'building': txt,
          },
          dataType: 'json',
          success: function(res){
              <!-- console.log(res.length); -->
              if(res.length != 0){
                var office = res[0]['scg_building_office_id'];
                $('#lst_edit_receiver_building').val(office).trigger("chosen:updated");
                getEditReceiverFloor(office);
                $("#lst_edit_receiver_floor").focus();
              }else{
                $('#alert_receiver_building').html("รหัสไม่ถูกต้อง").css('color','red');
                $('#txt_edit_receiver_code').focus();
              }
          }
        });
      }
      if(count_txt == 5){
        var b_code = txt.substring(0,3);
        var f_code = txt.substring(3);
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_location.php',
          data: {
            'building': b_code,
            'floor': f_code
          },
          dataType: 'json',
          success: function(res){
            if(res['floor'].length != 0 && res['building'].length != 0){
            var office = res['building'][0]['scg_building_office_id'];
            var floor = res['floor'][0]['scg_building_floor_id'];
            $('#lst_edit_receiver_building').val(office).trigger("chosen:updated");
            getEditReceiverFloor(office, floor);
              $('#alert_receiver_building').html("");
               $("#txt_edit_receiver_name").focus();
            }else{
                  $('#alert_receiver_building').html("รหัสไม่ถูกต้อง").css('color','red');
                  $('#txt_edit_receiver_code').focus();
            }

          }
        });
      }
      if(count_txt == 4){
        var b_code = txt.substring(0,2);
        var f_code = txt.substring(2);
        $.ajax({
          type: 'post',
          url: 'ajax/ajax_select_location.php',
          data: {
            'building': b_code,
            'floor': f_code
          },
          dataType: 'json',
          success: function(res){
            if(res['floor'].length != 0 && res['building'].length != 0){
                  var office = res['building'][0]['scg_building_office_id'];
                  var floor = res['floor'][0]['scg_building_floor_id'];
                  $('#lst_edit_receiver_building').val(office).trigger("chosen:updated");
                  getEditReceiverFloor(office, floor);
                  $('#alert_receiver_building').html("");
                  $("#txt_edit_receiver_name").focus();
            }else{
                  $('#alert_receiver_building').html("รหัสไม่ถูกต้อง").css('color','red');
                  $('#txt_edit_receiver_code').focus();
            }

          }
        });
      }
    }
});
<!-- lst Edit receiver Code -->
$("#btn_select_all").click(function() {
  table.rows().iterator( 'row', function ( context, index ) {
      $( this.row( index ).node() ).toggleClass( 'selected' );
      console.log(this.row( index ).node() );
  } );
  if ($(this).text() == "Deselect All")
       $(this).text("Select All");
    else
       $(this).text("Deselect All");

});
{% endblock %}
{% block domReady2 %}
$('#btn_edit_data').click(function() {
  var work_id = $('#lb_order_id').text();
  var barcode = $('#txt_edit_barcode').val();
  var sender_building = $('#lst_edit_sender_building').val();
  var sender_floor = $('#lst_edit_sender_floor').val();
  var sender_name = $('#txt_edit_sender_name').val();
  var receiver_building = $('#lst_edit_receiver_building').val();
  var receiver_floor = $('#lst_edit_receiver_floor').val();
  var receiver_name = $('#txt_edit_receiver_name').val();
  var remark = $('#txt_edit_remark').val();
  var lst_file_type = $('#lst_edit_file_type').val();
  var status = true;

  if(sender_building == "" || sender_building == null){
    $('#alert_sender_building').html('กรุณาเลือกข้อมูล').css('color','red');
    status = false;
  }else{
    $('#alert_sender_building').html("");
    status = true;
  }

  if(sender_floor == "" || sender_floor == null){
    $('#alert_sender_floor').html('กรุณาเลือกข้อมูล').css('color','red');
    status = false;
  }else{
    $('#alert_sender_floor').html("");
    status = true;
  }

  if(sender_name == "" || sender_name == null){
    $('#alert_sender_name').html('จำเป็นต้องกรอก').css('color','red');
    status = false;
  }else{
    $('#alert_sender_name').html("");
    status = true;
  }

  if(receiver_building == "" || receiver_building == null){
    $('#alert_receiver_building').html('กรุณาเลือกข้อมูล').css('color','red');
    status = false;
  }else{
    $('#alert_receiver_building').html("");
    status = true;
  }

  if(receiver_floor == "" || receiver_floor == null){
    $('#alert_receiver_floor').html('กรุณาเลือกข้อมูล').css('color','red');
    status = false;
  }else{
    $('#alert_receiver_floor').html("");
    status = true;
  }

  if(receiver_name == "" || receiver_name == null){
    $('#alert_receiver_name').html('จำเป็นต้องกรอก').css('color','red');
    status = false;
  }else{
    $('#alert_receiver_name').html("");
    status = true;
  }

   if(lst_file_type == "" || lst_file_type == null){
    $('#alert_file_type').html('กรุณาเลือกข้อมูล').css('color','red');
    status = false;
  }else{
    $('#alert_file_type').html("");
    status = true;
  }


  if(status == true){
    $.ajax({
      url: 'ajax/ajax_update_sender_entry_data.php',
      type: 'POST',
      data: {
        'work_order_id': work_id,
        'sender_name': sender_name,
        'sender_office': sender_building,
        'sender_floor': sender_floor,
        'receiver_name': receiver_name,
        'receiver_office': receiver_building,
        'receiver_floor': receiver_floor,
        'barcode': barcode,
        'remark': remark,
        'file_type': lst_file_type
      },
      success: function(res){
        $('#myModal').modal('hide');
        table.ajax.reload();
      }
    });
  }
});

{% endblock %}
	{% block javaScript %}

  <!-- FUNCTION GETFLOOR -->
  function getFloor(building, floor){
      $.ajax({
        type: 'post',
        url: 'ajax/ajax_select_floor.php',
        data: {
            'building': building,
        },
        dataType: 'json',
        success: function(res){
          if(res.length != 0){
            $('#lst_floor').empty();
            for(var i = 0; i < res.length;i++){
              $('#lst_floor').append($('<option></option>',
              {
                value: res[i]['scg_building_floor_id'],
                text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
              }))
            }
            $('#lst_floor').val(floor).trigger("chosen:updated");
            $('#alert_building').html("");
          }
          else{
            $('#alert_building').html("รหัสไม่ถูกต้อง").css('color','red');
            $('#txt_code').focus();
          }
        }
      });
  }
    <!-- FUNCTION GETFLOOR -->
  <!-- Function getEditSenderFloor -->
  function getEditSenderFloor(building, floor){
      $.ajax({
        type: 'post',
        url: 'ajax/ajax_select_floor.php',
        data: {
            'building': building,
        },
        dataType: 'json',
        success: function(res){
          if(res.length != 0){
            $('#lst_edit_sender_floor').empty();
            for(var i = 0; i < res.length;i++){
              $('#lst_edit_sender_floor').append($('<option></option>',
              {
                value: res[i]['scg_building_floor_id'],
                text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
              }))
            }
            $('#lst_edit_sender_floor').val(floor).trigger("chosen:updated");
            $('#alert_sender_building').html("");
          }
          else{
            $('#alert_sender_building').html("รหัสไม่ถูกต้อง").css('color','red');
            $('#txt_edit_sender_code').focus();
          }
        }
      });
  }
  <!-- Function getEditSenderFloor -->
  <!-- Function getEditReceiverFloor -->
  function getEditReceiverFloor(building, floor){
      $.ajax({
        type: 'post',
        url: 'ajax/ajax_select_floor.php',
        data: {
            'building': building,
        },
        dataType: 'json',
        success: function(res){
          if(res.length != 0){
            $('#lst_edit_receiver_floor').empty();
            for(var i = 0; i < res.length;i++){
              $('#lst_edit_receiver_floor').append($('<option></option>',
              {
                value: res[i]['scg_building_floor_id'],
                text: res[i]['scg_building_floor_code']+" : "+res[i]['scg_building_floor_name']
              }))
            }
            $('#lst_edit_receiver_floor').val(floor).trigger("chosen:updated");
            $('#alert_receiver_building').html("");
          }
          else{
            $('#alert_receiver_building').html("รหัสไม่ถูกต้อง").css('color','red');
            $('#txt_edit_receiver_code').focus();
          }
        }
      });
  }
  <!-- Function getEditReceiverFloor -->
    function editWorkOrder(id){
      console.log(id);
      $.ajax({
        url: 'ajax/ajax_load_data_edit_work_order.php',
        type: 'post',
        data: {
          work_id: id
        },
        dataType: 'json',
        success: function(res){
          console.log(res);
          $('#myModal').modal('show');
          for(var i = 0; i < res.length; i++){
            $('#txt_edit_barcode').val(res[i]['barcode']);
            $('#lb_order_id').text(res[i]['scg_work_order_id']);
            $('#lst_edit_sender_building').val(res[i]['scg_sender_building_id']).trigger("chosen:updated");
            getEditSenderFloor(res[i]['scg_sender_building_id'], res[i]['scg_sender_building_floor_id']);

            $('#lst_edit_receiver_building').val(res[i]['scg_receiver_building_office_id']).trigger("chosen:updated");
            getEditReceiverFloor(res[i]['scg_receiver_building_office_id'], res[i]['scg_receiver_building_floor_id']);

            $('#txt_edit_sender_name').val(res[i]['scg_sender_name']);
            $('#txt_edit_receiver_name').val(res[i]['scg_receiver_name']);
            $('#txt_edit_remark').val(res[i]['remark']);
            $('#txt_edit_round').val(res[i]['scg_delivery_round']);
            $('#lst_edit_file_type').val(res[i]['scg_file_type']).trigger("chosen:updated");
          }

        }
      })
    }
	{% endblock %}
		{% block Content2 %}
        <div class="row">
          <div class="col-md-12">
            <fieldset class="fieldset panel">
              <lagend><h4><b><center>Entry Data Report :  ( {{ date }} ) </center></b></h4></lagend>
              <hr>
              <div class="col-md-6">
              <table class="tb_Inputs">
                <tr>
                  <td><b>Barcode</b></td>
                  <td>
                    <input type="text" class="txtInputs" id="txt_barcode" name="txt_barcode" placeholder="Barcode" >
                  </td>
                </tr>
                <tr>
                  <td><b>Building</b></td>
                  <td>
                    <input type="text" name="txt_code" id="txt_code" class="txtCode" maxlength="5" placeholder="Search...">
                    <select class="my_select_box" id="lst_building" style="width:196px;" >
                         <option value="" disabled selected>Input Building</option>
                          {% for offices in office %}
                          <option value="{{ offices.scg_building_office_id }}">{{ offices.scg_building_code }} : {{ offices.scg_building_name }}</option>
                          {% endfor %}
                    </select>
                    <span id="alert_building"></span>
                  </td>
                </tr>
                <tr>
                  <td><b>Floor</b></td>
                  <td>
                    <select class="my_select_box" id="lst_floor" style="width:300px;" >
                         <option value="" disabled selected>Input Floor</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td><b>Receiver</b></td>
                  <td>
                    <input type="text" class="txtInputs" id="txt_receiver_name" name="txt_receiver_name" placeholder="Receiver Name" >
                  </td>
                </tr>
                <tr>
                  <td><b>Sender</b></td>
                  <td>
                      <input type="text" class="txtInputs" id="txt_sender_name" name="txt_sender_name" placeholder="Sender Name" >
                  </td>
                </tr>
              </table>
                </div>
                <div class="col-md-6">
                  <table class="tb_Inputs">
                    <tr>
                      <td><b>Date</b></td>
                      <td>
                        <div class="input-daterange input-group" id="datepicker" data-date-format="yyyy-mm-dd">
                              <input type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From date"/>
                              <label class="input-group-addon" style="border:0px;">to</label>
                              <input type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="To date"/>
                        </div>
                        <span id="alert_date"></span>
                      </td>
                    </tr>
                    <tr>
                      <td><b>Status</b></td>
                      <td>
                        <select class="my_select_box" name="lst_status" id="lst_status" style="width:300px;">
                          <option value="" disabled selected>Choose Status</option>
                          <option value="">All</option>
                          {% for status in status_data %}
                          <option value="{{status.scg_status_id}}">{{status.name}}</option>
                          {% endfor %}
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td><b>Round</b></td>
                      <td>
                          <input type="number" class="txtInputs" id="txt_round" name="txt_round" placeholder="Round">
                      </td>
                    </tr>
                    <tr>
                      <td><b>Batch Number</b></td>
                      <td>
                        <h3><label id="lb_batch_number">{{ maxRound }} </label> - <label id="user_id">{{ user_id }}</label></h3>
                      </td>
                    </tr>

                  </table>
                </div>
            </fieldset>
            <div class="col-md-4">
              <!-- <button type="button" name="button" id="test_search">test_search</button> -->
              <button type="button" id="btn_select_all" class="btn btn-default">Select All</button>
              <button type="button" id="btn_reprint_separate" class="btn btn-default">Print แยกแผ่น</button>
              <button type="button" id="btn_reprint_mixed" name="btn_reset" class="btn btn-default">Print รวมแผ่น</button>
            </div>
            <div class="col-md-4 col-md-offset-4" align="right">
              <button type="button" id="btn_search" class="btn btn-primary ">Search</button>
              <button type="button" id="btn_reset" name="btn_reset" class="btn btn-default ">Clear</button>
              <button type="button" id="btn_print" class="btn btn-default ">Print</button>
            </div>
            <br><br>
                <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover" id="tb_work_order">
                        <thead>
                          <tr>
                            <th>Order Id</th>
                            <th>Sent : Date/Time</th>
                            <th>Bacode</th>
                            <th>Sender</th>
                            <th>Building</th>
                            <th>Floor</th>
                            <th>Receiver</th>
                            <th>Building</th>
                            <th>Floor</th>
                            <th>Round</th>
                            <th>Remark</th>
                            <th>File Type</th>
                            <th>Status</th>
                            <th>Fullname</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Order Id: <label id="lb_order_id"></label></h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                          <div class="row">
                            <div class="col-md-6">
                              <form action="" class="form-horizontal">
                                <div class="form-group">
                                  <label for="txt_edit_barcode" class="col-sm-2 control-label">Barcode</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="txtInputs" id="txt_edit_barcode" name="txt_edit_barcode" placeholder="Barcode" >
                                  </div>
                                  <span id="alert_barcode"></span>
                                </div>
                            <div class="form-group">
                                  <label for="lst_edit_sender_building" class="col-sm-2 control-label">Building</label>
                                <div class="col-sm-10">
                                  <input type="text" name="txt_edit_sender_code" id="txt_edit_sender_code" class="txtCode" maxlength="5" placeholder="Search...">
                                  <select class="my_select_box" id="lst_edit_sender_building" style="width:196px;" >
                                       <option value="" disabled selected>Input Building</option>
                                        {% for offices in office %}
                                        <option value="{{ offices.scg_building_office_id }}">{{ offices.scg_building_code }} : {{ offices.scg_building_name }}</option>
                                        {% endfor %}
                                  </select>
                                  <span id="alert_sender_building"></span>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="lst_edit_sender_floor" class="col-sm-2 control-label">Floor</label>
                              <div class="col-sm-10">
                                <select class="my_select_box" id="lst_edit_sender_floor" style="width:300px;" >
                                     <option value="" disabled selected>Input Floor</option>
                                </select>
                                <span id="alert_sender_floor"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="txt_edit_sender_name" class="col-sm-2 control-label">Sender</label>
                              <div class="col-sm-10">
                                <input type="text" class="txtInputs" id="txt_edit_sender_name" name="txt_sender_name" placeholder="Sender Name" >
                                <span id="alert_sender_name"></span>
                              </div>
                            </div>
                              </form>
                            </div>
                            <div class="col-md-6">
                              <form action="" class="form-horizontal">
                                <div class="form-group">
                                      <label for="lst_edit_receiver_building" class="col-sm-2 control-label">Building</label>
                                    <div class="col-sm-10">
                                      <input type="text" name="txt_edit_receiver_code" id="txt_edit_receiver_code" class="txtCode" maxlength="5" placeholder="Search...">
                                      <select class="my_select_box" id="lst_edit_receiver_building" style="width:196px;" >
                                           <option value="" disabled selected>Input Building</option>
                                            {% for offices in office %}
                                            <option value="{{ offices.scg_building_office_id }}">{{ offices.scg_building_code }} : {{ offices.scg_building_name }}</option>
                                            {% endfor %}
                                      </select>
                                      <span id="alert_receiver_building"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label for="lst_edit_receiver_floor" class="col-sm-2 control-label">Floor</label>
                                  <div class="col-sm-10">
                                    <select class="my_select_box" id="lst_edit_receiver_floor" style="width:300px;" >
                                         <option value="" disabled selected>Input Floor</option>
                                    </select>
                                    <span id="alert_receiver_floor"></span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="txt_edit_receiver_name" class="col-sm-2 control-label">Receiver</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="txtInputs" id="txt_edit_receiver_name" name="txt_edit_receiver_name" placeholder="Receiver Name" >
                                    <span id="alert_receiver_name"></span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="txt_edit_remark" class="col-sm-2 control-label">Remark</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="txtInputs" id="txt_edit_remark" name="txt_edit_remark" placeholder="Remark" >
                                    <span id="alert_remark"></span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="txt_edit_round" class="col-sm-2 control-label">Round</label>
                                  <div class="col-sm-10">
                                      <input type="number" class="txtInputs" id="txt_edit_round" name="txt_edit_round" placeholder="Round" readonly="true">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="lst_edit_file_type" class="col-sm-2 control-label">FileType</label>
                                  <div class="col-sm-10">
                                    <select name="" id="lst_edit_file_type">
                                       <option value="" disabled selected>Input File Type</option>
                                       <option value="1">01 : เอกสาร</option>
                                       <option value="2">02 : พัสดุ</option>
                                       <option value="3">03 : ไปรษณีย์</option>
                                       <option value="4">04 : อื่นๆ</option> 
                                    </select>
                                    <span id="alert_file_type"></span>
                                  </div>
                                </div>
                                <div align='right'>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary" id="btn_edit_data">Save</button>
                                </div>

                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>
              </div>
            </div>
		{% endblock %}

{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

<!DOCTYPE html>

<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" >
<head>
  {% block head %}
		<title>{% block title %}The Digital PEON Book System{% endblock %}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pivot Mailroom Services">
		<meta name="author" content="Pivot">
		<link rel="icon" href="../themes/bootstrap/css/favicon.ico" />
		
		
		   <!-- Bootstrap core CSS -->
    <link href="../themes/bootstrap_emp/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	
	<link rel="stylesheet" href="../themes/alertifyjs/css/alertify.css" id="alertifyCSS">
	<link rel="stylesheet" href="../themes/alertifyjs/css/themes/default.css" >
	  <!-- Datepicker -->
	
    <!-- Custom styles for this template -->
    <link href="../themes/bootstrap_emp/bootstrap/css/simple-sidebar.css" rel="stylesheet">
		
	<link href="../themes/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet">
			 
	 <link rel="stylesheet" type="text/css" href="../themes/material_icon/material-icons.min.css">
	 
	 <link rel="stylesheet" type="text/css" href="../themes/fancybox/jquery.fancybox.min.css">
	 
	<!-- <script src="../themes/bootstrap_emp/jquery/jquery.min.js"></script> -->
	<script src="../scripts/jquery-1.12.4.js"></script>
	
	{% block scriptImport %}{% endblock %}
	<script src="../themes/bootstrap/js/ie-emulation-modes-warning.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js"></script>

	<link rel="stylesheet" href="../themes/bootstrap_emp/dist/sweetalert2.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	
	
	
	<script src="../themes/bootstrap_emp/dist/slideout.min.js"></script>
	
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  {% endblock %}
  <style type="text/css">
  {% block styleReady %}{% endblock %}
  nav#menu {
	background-image: -ms-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
	background-image: -moz-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
	background-image: -o-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
	background-image: -webkit-linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
	background-image: linear-gradient(top, #e6e9f0 0%, #eef1f5 100%);
  }
  .logout{
	border-top: 1px solid #0266bb;
    margin-left:20px;
    margin-right:50px;
  }
  
  .list-group-item {
    padding: .25rem 1.25rem; */
    margin-bottom: -1px;
	border: 0px solid rgba(0,0,0,.125);
	}
	
	.material-icons {
		vertical-align: middle;
		display: inline; 
	}
	
	.btn-success span {
		vertical-align: middle;
	}
	
	.status {
		border: 1px solid #e0e0e0;
		padding: 5px;
		padding-left: 10px;
		padding-right: 10px;
		border-radius: 15px;
	}
	
	.card {
		box-shadow: 2px 2px 1px 1px rgba(50,50,50,.4);
	}
	
	.btn{
	//	box-shadow: 2px 2px 1px 1px rgba(50,50,50,.4);
	}

	.slideout-menu {
		position: fixed;
		top: 0;
		bottom: 0;
		width: 290px;
		min-height: 100vh;
		overflow-y: scroll;
		-webkit-overflow-scrolling: touch;
		z-index: 0;
		display: none;
	}
	
	.slideout-menu-left {
		left: 0;
	}
	
	.slideout-menu-right {
		right: 0;
	}
	
	.slideout-panel {
		position: relative;
		z-index: 1;
		will-change: transform;
		background-color: #FFF; /* A background-color is required */
		min-height: 100vh;
	}
	
	.slideout-open,
	.slideout-open body,
	.slideout-open .slideout-panel {
		overflow: hidden;
	}
	
	.slideout-open .slideout-menu {
		display: block;
	}
	
	.panel-header{
		height: 50px;
		margin-bottom: 20px;
		color:	white;
		text-align: center;
		//box-shadow: 2px 2px 1px 1px rgba(50,50,50,.4);
	}
	
	
	.js-slideout-toggle{
		margin: 13px 5px;
		position: absolute;
		left: 10px;
	}
	 
	
	.header-text {
		margin-top: 8px;
		font-size: 25px;
		font-weight:bold;
		color: #696969;
		text-shadow: white 0.1em 0.1em 0.1em;
	}

	
	@media screen and (max-width: 1080px) {
		.header-text {
			margin-top: 13px;
			font-size: 18px;
			font-weight:bold;
			color: #696969;
			text-shadow: white 0.1em 0.1em 0.1em;
		}
	}
  </style>
</head>
<body>
<!--Start Header-->




<nav id="menu">  <!-- style="background-color: #0266bb;" -->
				
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
						{% if role_id == roles.Employee %}
							<a href="../data/profile.php"><span class="white-text name">{{ user_data.mr_user_username }}</span></a>
						{% elseif role_id == roles.Messenger %}	
							<a href="../data/profile.php"><span class="white-text name">{{ user_data.mr_user_username }}</span></a>
						{% elseif role_id == roles.MessengerBranch %}	
							<a href="../messenger/profile.php"><span class="white-text name">{{ user_data.mr_user_username }}</span></a>
						{% elseif role_id == roles.MessengerHO %}	
							<a href="../messenger/profile.php"><span class="white-text name">{{ user_data.mr_user_username }}</span></a>
						{% else %}
							<span class="white-text name">{{ user_data.mr_user_username }}</span>
						{% endif %}
                    </a>
                </li>
                {% if role_id == roles.Administrator %}
					
					<li><a href="../mailroom/search.php"><i class="material-icons" style="padding-right:5px;">search</i>ค้นหา </a></li>
					<li><a href="../mailroom/user.php"><i class="material-icons" style="padding-right:5px;">group</i>แก้ปัญหา User </a></li>
					<li>
						<a data-toggle='collapse' href='#collapse_mailroom'><i class="material-icons" style="padding-right:5px;">assessment</i>ประเมิณผล<i class="material-icons">arrow_drop_down</i></a>
						<div id='collapse_mailroom' class='panel-collapse collapse'>
							<ul class='list-group'>
								<li class='list-group-item'><a href="../mailroom/summary.php"><i class="material-icons">keyboard_arrow_right</i> ผู้รับ</a></li>
								<li class='list-group-item'><a href="../mailroom/summary_sender.php"><i class="material-icons">keyboard_arrow_right</i> ผู้ส่ง</a></li>
							</ul>
						</div>
					</li>

					<li><a data-toggle="collapse" href="#collapse3" ><i class="material-icons" style="padding-right:5px;">description</i>รายงาน<i class="material-icons">arrow_drop_down</i> </a></li>
						<div id="collapse3" class="panel-collapse collapse">
							<ul class="list-group" >
								<li class="list-group-item" ><a href="../manager/report_send.php"> รายงานรับส่งเอกสาร(รายเดือน)</a></li>
								<li class="list-group-item" ><a href="../manager/report_send_daily.php">รายงานรับส่งเอกสาร(รายวัน)</a></li>
							</ul>
						</div>
					<li><a href="../manager/report_sla.php"><i class="material-icons" style="padding-right:5px;">assignment_turned_in</i>รายงาน SLA </a></li>
					<li class="logout"></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				
				{% elseif role_id == roles.Employee %}
					<li><a data-toggle="collapse" href="#collapse1" ><i class="material-icons" style="padding-right:5px;">content_paste</i>การสั่งงาน <i class="material-icons">arrow_drop_down</i></a></li>
						<div id="collapse1" class="panel-collapse collapse">
							<ul class="list-group" >
								<li class="list-group-item" ><a href="../employee/profile_check.php?type=1">รับส่งภายในสำนักงานใหญ่</a></li>
								<li class="list-group-item" ><a href="../employee/profile_check.php?type=2">รับส่งที่สาขา</a></li>
								<li class="list-group-item" ><a href="../employee/create_work_import_excel.php">Import Excel</a></li>
								<!-- <li class="list-group-item" ><a href="work_out.php">ส่งภายนอก</a></li>
								<li class="list-group-item" ><a href="work_byhand.php">By Hand(เอกสารถึงลูกค้า)</a></li>
								<li class="list-group-item" ><a href="work_post.php">ส่งไปรษณีย์</a></li> -->
							</ul>
						</div>
						
					<!-- <li><a href="#"><i class="material-icons" style="padding-right:5px;">inbox</i>  รับเอกสาร (Inbox) </a></li> -->
					<li><a href="../employee/receive_work.php"><i class="material-icons" style="padding-right:5px;">assignment_return</i> รับเอกสาร </a></li>
					<li><a href="../employee/search_follow.php"><i class="material-icons" style="padding-right:5px;">inbox</i>  ติดตามเอกสาร </a></li>
					<li><a href="../employee/search.php"><i class="material-icons" style="padding-right:5px;">search</i>ค้นหา </a></li>
					<li><a href="../employee/search_detail.php"><i class="material-icons" style="padding-right:5px;">assessment</i>ค้นหาแบบละเอียด </a></li>
					<li><a data-toggle="collapse" href="#collapse2" ><i class="material-icons" style="padding-right:5px;">description</i>รายงาน<i class="material-icons">arrow_drop_down</i> </a></li>
						<div id="collapse2" class="panel-collapse collapse">
							<ul class="list-group" >
								<li class="list-group-item" ><a href="../employee/report_send.php">รายงานการส่งออก</a></li>
								<li class="list-group-item" ><a href="../employee/report_receive.php">รายงานการรับเข้า</a></li>
							<!-- 	<li class="list-group-item" ><a href="work_out.php">ส่งภายนอก</a></li>
								<li class="list-group-item" ><a href="work_byhand.php">By Hand(เอกสารถึงลูกค้า)</a></li>
								<li class="list-group-item" ><a href="work_post.php">ส่งไปรษณีย์</a></li> -->
							</ul>
						</div>
					<span class="permition_branch_menage" style="display: none;">
						<li class="logout"></li>
						<li ><a href="../mailroom/report.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">print</i>รายงาน</a></li>
						<li ><a href="../employee/search_branch_menage.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">donut_small</i>รายงาน(บริหารสาขา)</a></li>
					</span>
					<li class="logout"></li>
					<li ><a href="../data/profile.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">perm_contact_calendar</i>แก้ไขข้อมูลส่วนตัว</a></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				{% elseif role_id == roles.MessengerBranch %}
						<li><a href="../messenger/news.php"><i class="material-icons" style="padding-right:5px;">home</i>หน้าแรก</a></li>
						<li>
							<a data-toggle='collapse' href='#receive_branch'><i class="material-icons" style="padding-right:5px;">directions_bike</i>เข้ารับเอกสารจากสาขา<i
								 class="material-icons">arrow_drop_down</i></a>
							<div id='receive_branch' class='panel-collapse collapse'>
								<ul class='list-group'>
									<li><a href="../messenger/receive_branch_list.php" style="padding-left: 30px;"><i class="material-icons" style="padding-right:5px;">transfer_within_a_station</i>รับเอกสาร</a></li>
									<li><a href="../messenger/receive_branch_mixed.php" style="padding-left: 30px;"><i class="material-icons" style="padding-right:5px;">library_add</i>รวมเอกสารส่ง
											Mailroom</a></li>
								</ul>
							</div>
						</li>

						<li><a href="../messenger/receive_mailroom_to_branch.php"><i class="material-icons" style="padding-right:5px;">domain</i>รับเอกสารจาก
								Mailroom</a></li>
						<li><a href="../messenger/receive_hub.php"><i class="material-icons" style="padding-right:5px;">store_mall_directory</i>รับเอกสารจาก
								Hub</a></li>
						<li><a href="../messenger/send_branch_list.php"><i class="material-icons" style="padding-right:5px;">send</i>ส่งเอกสารที่สาขา</a></li>

						<!-- <li><a href="../messenger/search_follow.php"><i class="material-icons" style="padding-right:5px;">search</i>  ติดตามเอกสาร </a></li> -->
						<li class="logout"></li>
						<li><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				
				{% elseif role_id == roles.Messenger %}
					<li><a href="../messenger/news.php"><i class="material-icons" style="padding-right:5px;">home</i>หน้าแรก</a></li>
					<li><a href="../messenger/receive_list.php"><i class="material-icons" style="padding-right:5px;">chevron_left</i>รับเอกสาร</a></li>
					<li><a href="../messenger/mailroom_receive_list.php"><i class="material-icons" style="padding-right:5px;">compare_arrows</i>รับเอกสาร Mailroom</a></li>
					<li><a href="../messenger/send_list.php"><i class="material-icons" style="padding-right:5px;">chevron_right</i>ส่งเอกสาร</a></li>
					<li><a href="../messenger/search_follow.php"><i class="material-icons" style="padding-right:5px;">search</i>  ติดตามเอกสาร </a></li>
					<li class="logout"></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				{% elseif role_id == roles.MessengerHO %}	
					<li><a href="../messenger/news.php"><i class="material-icons" style="padding-right:5px;">home</i>หน้าแรก</a></li>
					<li><a href="../messenger/receive_list.php"><i class="material-icons" style="padding-right:5px;">chevron_left</i>รับเอกสาร</a></li>
					<li><a href="../messenger/mailroom_receive_list.php"><i class="material-icons" style="padding-right:5px;">compare_arrows</i>รับเอกสาร Mailroom</a></li>
					<li><a href="../messenger/send_list.php"><i class="material-icons" style="padding-right:5px;">chevron_right</i>ส่งเอกสาร</a></li>
					<li><a href="../messenger/search_follow.php"><i class="material-icons" style="padding-right:5px;">search</i>  ติดตามเอกสาร </a></li>
					
					<li class="logout"></li>
						<li>
							<a data-toggle='collapse' href='#receive_branch'><i class="material-icons" style="padding-right:5px;">directions_bike</i>เข้ารับเอกสารจากสาขา<i
								 class="material-icons">arrow_drop_down</i></a>
							<div id='receive_branch' class='panel-collapse collapse'>
								<ul class='list-group'>
									<li><a href="../messenger/receive_branch_list.php" style="padding-left: 30px;"><i class="material-icons" style="padding-right:5px;">transfer_within_a_station</i>รับเอกสาร</a></li>
									<li><a href="../messenger/receive_branch_mixed.php" style="padding-left: 30px;"><i class="material-icons" style="padding-right:5px;">library_add</i>รวมเอกสารส่ง
											Mailroom</a></li>
								</ul>
							</div>
						</li>

						<li><a href="../messenger/receive_mailroom_to_branch.php"><i class="material-icons" style="padding-right:5px;">domain</i>รับเอกสารจาก
								Mailroom</a></li>
						<li><a href="../messenger/receive_hub.php"><i class="material-icons" style="padding-right:5px;">store_mall_directory</i>รับเอกสารจาก
								Hub</a></li>
						<li><a href="../messenger/send_branch_list.php"><i class="material-icons" style="padding-right:5px;">send</i>ส่งเอกสารที่สาขา</a></li>

						<!-- <li><a href="../messenger/search_follow.php"><i class="material-icons" style="padding-right:5px;">search</i>  ติดตามเอกสาร </a></li> -->
						

					<li class="logout"></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>

				{% elseif role_id == roles.Mailroom %}
						
					<!-- <li><a href="#"><i class="material-icons" style="padding-right:5px;">inbox</i>  รับเอกสาร (Inbox) </a></li> -->
					<li><a href="../mailroom/reciever.php"><i class="material-icons" style="padding-right:5px;">inbox</i> รับเข้าเอกสาร </a></li>
					<li><a href="../mailroom/reciever_branch.php"><i class="material-icons" style="padding-right:5px;">flight_land</i> รับเอกสารจากสาขา </a></li>
					<li><a href="../mailroom/sender.php"><i class="material-icons" style="padding-right:5px;">unarchive</i> ส่งออกเอกสาร </a></li>
					<li><a href="../mailroom/mailroom_import_barcode_TNT.php"><i class="material-icons" style="padding-right:5px;">flight_takeoff</i> import TNT Barcode </a></li>
					<li><a href="../mailroom/mailroom_sendWork.php"><i class="material-icons" style="padding-right:5px;">local_printshop</i> พิมพ์ใบนำส่งเอกสาร </a></li>
					<li><a href="../mailroom/mailroom_sendWorkTNT.php"><i class="material-icons" style="padding-right:5px;">flight_takeoff</i> ส่งออกเอกสารไปสาขา </a></li>
					<!-- <li><a href="search_follow.php"><i class="material-icons" style="padding-right:5px;">inbox</i>  ติดตามเอกสาร </a></li> -->
					<li><a href="../mailroom/search.php"><i class="material-icons" style="padding-right:5px;">search</i>ค้นหา </a></li>
					<li><a href="../mailroom/user.php"><i class="material-icons" style="padding-right:5px;">group</i>แก้ปัญหา User </a></li>
					<li>
						<a data-toggle='collapse' href='#collapse_mailroom'><i class="material-icons" style="padding-right:5px;">assessment</i>ประเมิณผล<i class="material-icons">arrow_drop_down</i></a>
						<div id='collapse_mailroom' class='panel-collapse collapse'>
							<ul class='list-group'>
								<li class='list-group-item'><a href="../mailroom/summary_sender.php"><i class="material-icons">keyboard_arrow_right</i> รับส่งเอกสาร(HO)</a></li>
								<li class='list-group-item'><a href="../mailroom/summary_messenger.php"><i class="material-icons">keyboard_arrow_right</i> พนักงานเดินเอกสาร</a></li>
								<li class='list-group-item'><a href="../mailroom/summary_HO_to_branch.php"><i class="material-icons">keyboard_arrow_right</i>สำนักงานใหญ่ถึงสาขา</a></li>
								<li class='list-group-item'><a href="../mailroom/summary_branch_to_HO.php"><i class="material-icons">keyboard_arrow_right</i>สาขาถึงสำนักงานใหญ่</a></li>
								<li class='list-group-item'><a href="../mailroom/summary_branch_to_branch.php"><i class="material-icons">keyboard_arrow_right</i> สาขาถึงสาขา</a></li>
								<li class='list-group-item'><a href="../mailroom/summary_branch.php"><i class="material-icons">keyboard_arrow_right</i> งานสาขา</a></li>
								<!-- <li class='list-group-item'><a href="../mailroom/summary.php"><i class="material-icons">keyboard_arrow_right</i> นำส่งเอกสาร</a></li> -->
								
							</ul>
						</div>
					</li>
					<li><a href="../mailroom/receive_work.php"><i class="material-icons">gesture</i>ลงชื่อรับเอกสาร</a></li>
					<li><a href="../mailroom/access_log.php"><i class="material-icons" style="padding-right:5px;">group</i>การเข้าใช้งาน</a></li>
					<li><a href="../mailroom/import_excel.php"><i class="material-icons">contacts</i>เพิ่มข้อมูลพนักงาน</a></li>
					<li><a href="../mailroom/update_branch.php"><i class="material-icons">local_library</i>อัปเดทข้อมูลสาขา</a></li>
					<li class="logout"></li>
						<li ><a href="../mailroom/report.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">print</i>รายงาน</a></li>
						<li ><a href="../employee/search_branch_menage.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">donut_small</i>รายงาน(บริหารสาขา)</a></li>
					<li class="logout"></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				
				{% elseif role_id == roles.Branch %}
					<li><a data-toggle="collapse" href="#collapse1" ><i class="material-icons" style="padding-right:5px;">content_paste</i>การสั่งงาน <i class="material-icons">arrow_drop_down</i></a></li>
						<div id="collapse1" class="panel-collapse collapse">
							<ul class="list-group" >
								<li class="list-group-item" ><a href="work_in.php">ส่งสำนักงานใหญ่</a></li>
								<li class="list-group-item" ><a href="work_out.php">ส่งระหว่างสาขา</a></li>
							</ul>
						</div>
					<li><a href="search_follow.php"><i class="material-icons" style="padding-right:5px;">search</i>ค้นหาและติดตามเอกสาร </a></li>
					<li class="logout"></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
				{% elseif role_id == roles.Manager %}
					<li><a href="search.php"><i class="material-icons" style="padding-right:5px;">search</i>ค้นหา </a></li>
					<li><a href="../mailroom/user.php"><i class="material-icons" style="padding-right:5px;">group</i>แก้ปัญหา User </a></li>
					<li>
						<a data-toggle='collapse' href='#collapse_summary'><i class="material-icons" style="padding-right:5px;">assessment</i>ประเมิณผล<i class="material-icons">arrow_drop_down</i></a>
						<div id='collapse_summary' class='panel-collapse collapse'>
							<ul class='list-group'>
								<li class='list-group-item'><a href="../mailroom/summary_sender.php"><i class="material-icons">keyboard_arrow_right</i> รับเอกสาร</a></li>
								<li class='list-group-item'><a href="../mailroom/summary_messenger.php"><i class="material-icons">keyboard_arrow_right</i> พนักงานเดินเอกสาร</a></li>
							</ul>
						</div>
					</li>
					
					<li><a data-toggle="collapse" href="#collapse3" ><i class="material-icons" style="padding-right:5px;">description</i>รายงาน<i class="material-icons">arrow_drop_down</i> </a></li>
						<div id="collapse3" class="panel-collapse collapse">
							<ul class="list-group" >
								<li class="list-group-item" ><a href="../manager/report_send.php"> รายงานรับส่งเอกสาร(รายเดือน)</a></li>
								<li class="list-group-item" ><a href="../manager/report_send_daily.php">รายงานรับส่งเอกสาร(รายวัน)</a></li>
							</ul>
						</div>
					<li><a href="../manager/report_sla.php"><i class="material-icons" style="padding-right:5px;">assignment_turned_in</i>รายงาน SLA </a></li>
					<li><a href="../manager/access_log.php"><i class="material-icons" style="padding-right:5px;">group</i>การเข้าใช้งาน</a></li>
					<li ><a href="../user/logout.php" style="margin-left:0px;"> <i class="material-icons" style="padding-right:5px;">power_settings_new</i>Logout</a></li>
					
				{% endif %}
            </ul>
</nav>    
        <!-- /#sidebar-wrapper -->

<!--End Header-->
<!--Start Container-->
<div id="panel" >
 <header class="panel-header" style="background-color: #DCD9D4; 
 background-image: linear-gradient(to bottom, rgba(255,255,255,0.50) 0%, rgba(0,0,0,0.50) 100%), radial-gradient(at 50% 0%, rgba(255,255,255,0.10) 0%, rgba(0,0,0,0.50) 50%); 
 background-blend-mode: soft-light,screen;">
        <span class="js-slideout-toggle"><i class="material-icons">menu</i> <b>MENU</b> </span>
		<span class="header-nav">
			
				<label class="header-text" >The Digital PEON Book System</label>
			<!-- <img class="img-responsive" style="position:absolute;right:0px;padding:10px;" src="../themes/images/logo-tmb_mini.png" title="TMB" />  	 -->
		</span>
</header>
	<div  class="container">{% block Content %}{% endblock %}</div>
	<div class="container-fluid">{% block Content2 %}{% endblock %}</div>

</div>

<!--End Container-->

<!--Start Footer-->
<footer class="footer">
  <div class="container">
    <p class="text-muted">Copyright © 2017 Pivot Co., Ltd.</p>
  </div>
</footer>
<!--End Footer-->

<!--Start Debug-->

	{% block debug %}{% endblock %}
<!--End Debug-->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="../themes/bootstrap/js/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="../themes/bootstrap/js/jquery.vendor.min.js"><\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../themes/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	
<!-- 	<link href="../themes/jquery/jquery-ui.css" rel="stylesheet"> -->
	  
	<script src="../themes/alertifyjs/alertify.js"></script> 
	<script src="../themes/bootstrap_emp/dist/sweetalert2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
	
    <script src="../themes/bootstrap_emp/popper/popper.min.js"></script>
    <script src="../themes/bootstrap_emp/bootstrap/js/bootstrap.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script src="../themes/fancybox/jquery.fancybox.min.js"></script>
	<!-- <script src="../themes/jquery/jquery-ui.js"></script> -->
	<!-- money and currency formatting http://openexchangerates.github.io/accounting.js/ -->
	<!-- <script src="../themes/bootstrap/js/accounting.min.js"></script> -->
	<script type="text/javascript">
		$(document).ready(function(){
		
		 
		
		 var slideout = new Slideout({
			'panel': document.getElementById('panel'),
			'menu': document.getElementById('menu'),
			'padding': 256,
			'tolerance': 70
		});
						
		document.querySelector('.js-slideout-toggle').addEventListener('click', function() {
          slideout.toggle();
        });		
				
		
		
		
		$('.dropdown-toggle').dropdown()
		{% if select == '0' %}

		{% else %}
			$('select').select2();
		{% endif %}

		var usrID = '{{ userID }}';
		$.ajax({
			url: '../data/ajax/ajax_check_password_Date.php',
			method: 'GET',
			data: { userID: usrID },
			dataType: 'json',
			success: function (res) {
			//console.log(res);
				if(parseInt(res['diffdate']) >= 45 ) {
					//alert('');
					alertify.alert('Alert!!', 'กรุณาเปลี่ยนรหัสผ่าน เนื่องจากรหัสผ่านของคุณมีอายุการใช้งานเกิน 45 วันแล้ว!', function(){ 
						//alertify.success('Ok'); 
						window.location.href='../user/change_password.php?usr='+res['usrID'];
					});
				}
			}
		});
		$.ajax({
			url: '../user/ajax/ajax_permission_branch_menage.php',
			method: 'GET',
			data: { userID: usrID },
			dataType: 'json',
			success: function (res) {
				//console.log('>>>222');
				if(res.length > 0){
					$('.permition_branch_menage').show();
				}else{
					$('.permition_branch_menage').hide();
				}
				
			}
		});
		
			{% block domReady %}{% endblock %}
     	    {% block domReady2 %}{% endblock %} 
		});
	   {% block javaScript %}
	   {% endblock %}

	    //window.onbeforeunload = function (e) {
        //            e = e || window.event;
        //            // For IE and Firefox prior to version 4
        //            if (e) {
        //              // e.returnValue = 'ท่านต้องการปิดหน้านี้หรือไม่';
        //              if(confirm("ท่านต้องการปิดหน้านี้หรือไม่")) {
        //                 location.href = '../user/logout.php';
        //              }
        //            }
        //            // For Safari
        //        //return 'ท่านต้องการปิดหน้านี้หรือไม่';
        //       // if(confirm("ท่านต้องการปิดหน้านี้หรือไม่")) {
        //              // location.href = '../user/logout.php';
        //       // }
        //};
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116752839-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-116752839-1');
	</script>

</body>
</html>

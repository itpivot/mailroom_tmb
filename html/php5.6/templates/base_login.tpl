<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<!DOCTYPE html>
<!-- <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"> -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     
	<meta name="author" content="Pivot">
	<title>Pivot Mailroom Services</title>
	<link rel="icon" href="../themes/bootstrap/css/favicon.ico" />

   <!--  <link rel="icon" href="../themes/bootstrap/css/favicon.ico" /> -->
	 <!-- Bootstrap core CSS -->
    <link href="../themes/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../themes/bootstrap/css/signin.css" rel="stylesheet" />
   <link rel="stylesheet" href="../themes/alertifyjs/css/alertify.css" id="alertifyCSS">
	<link rel="stylesheet" href="../themes/alertifyjs/css/themes/default.css" >
  <style type='text/css'>
    {% block styleReady %}{% endblock %}
    html, body {
      height: 100%;
    }
    #content {
      min-height: 85%;
    }

    .footer,
    .push {
      height: 10px;
      text-align: center;
    }

    @media (min-width: 481px) and (max-width: 767px) {
      font-size:12px;
    }



    @media (min-width: 320px) and (max-width: 480px) {
      font-size: 12px;
    }
  </style>
</head>
<body>
    <div class="container" id='content'>
    {% block body %}{% endblock %}
    
    <div class="push"></div>
    </div>
    <!--Start Footer-->
    <footer class="footer">
      <div class="container">
        <p class="text-muted"><strong>ติดต่อห้อง Mailroom:</strong>02-299-1111 ต่อ 5694 </p>
        <p class=""><b>ปัญหาการใช้งานระบบติดต่อที่:</b></p>
        <p class="text-muted"><strong></strong>คุณจุฑารัตน์ เนียม ศรี เบอร์โทร 061-823-4346 อีเมล์ jutarat_ni@pivot.co.th</p>
        <p class="text-muted"><strong></strong>คุณณิธิวัชรา อยู่ถิ่น เบอร์โทร 02-299-5694 (ห้องสารบรรณกลาง) อีเมล์ Mailroom@pivot.co.th</p>

        <p class="text-muted"></p>
      </div>
    </footer>
    <!--End Footer-->
    <!-- <script src='../themes/bootstrap/js/jquery-3.1.1.min.js'></script> -->
    <script src="../themes/bootstrap/js/jquery-1.12.4.js"></script>
    <!-- <script src='../themes/bootstrap_emp/dist/sweetalert2.min.js'></script> -->
    <script src="../themes/bootstrap/js/ie-emulation-modes-warning.js"></script>
   <script src="../themes/alertifyjs/alertify.js"></script> 
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../themes/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    <script type='text/javascript'>
        $(document).ready(function() {
          {% block domReady %}{% endblock %}
        });
      
        {% block javascript %}{% endblock %}
    </script> 
        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116752839-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-116752839-1');
    </script>

  </body>
</html>
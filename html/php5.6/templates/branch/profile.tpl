{% extends "base_emp_branch.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block styleReady %}
.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}
{% endblock %}

{% block domReady %}
		var usrID = '{{ userID }}';

		$("#mr_role_id").change(function(){
		    var role_id = $(this).val();
			//alert(role_id);
			alertify.confirm('แก้ไขสถานที่ปฏิบัติงาน :','ยืนยันการเปลี่ยนสถานที่ปฏิบัติงาน', function(){ 
				//alertify.alert("ok");
				$.post('./ajax/ajax_UpdateUserRole.php',{role_id:role_id},
					function(res) {	
						if ( res.status == 200 ){
							alertify.alert('',"บันทึกสำเร็จ",function(){window.location.reload();});
						}else{
							alertify.alert('error',"บันทึกไม่สำเร็จ  "+res.message,function(){window.location.reload();});
						}
					},'json');
			},function(){ 
				alertify.error('Cancel')
			});
		});

		$("#btn_save").click(function(){
			$('.myErrorClass').removeClass('myErrorClass');

			var branch_floor = $('#mr_branch_floor').select2('data')
			var mr_position_id 		= $("#mr_position_id").val();
			var mr_branch_floor 	= branch_floor[0].text;
			var floor_id 			= branch_floor[0].id;
			var pass_emp 			= $("#pass_emp").val();
			var name 				= $("#name").val();
			var last_name 			= $("#last_name").val();
			var tel 				= $("#tel").val();
			var tel_mobile 			= $("#tel_mobile").val();
			var email 				= $("#email").val();
			var emp_id 				= $("#emp_id").val();
			var user_id 			= $("#user_id").val();
			var mr_branch_id 		= $("#mr_branch_id").val();
			var department 		= $("#department").val();
			var	status 				= true;
			
			if( mr_branch_id == "" || mr_branch_id == null){
				//$('#mr_branch_error').css({'border-bottom':' 1px solid red'});
				$('#select2-mr_branch_id-container').addClass('myErrorClass');
				status = false;
				alert('กรุณากรอกข้อมูล สาขา');
				return;
			}
			if( pass_emp == "" || pass_emp == null){
				//$('#pass_emp').css({'color':'red','border-style':'solid','border-color':'red'});
				$('#pass_emp').addClass('myErrorClass');
				status = false;
				return;
			}
			
			if(	name == "" || name == null){
				status = false;
				//$('#name').css({'color':'red','border-style':'solid','border-color':'red'});
				$('#name').addClass('myErrorClass');
				status = false;
				alert('กรุณากรอกข้อมูล ชื่อ');
				return;
			}
			
			if( last_name == "" || last_name == null){
				status = false;
				//$('#last_name ').css({'color':'red','border-style':'solid','border-color':'red'});
				$('#last_name ').addClass('myErrorClass');
				alert('กรุณากรอกข้อมูล นามสกุล');
				return;
			}
			
			if( email == "" || email == null){
				status = false;
				//$('#email ').css({'color':'red','border-style':'solid','border-color':'red'});
				$('#email ').addClass('myErrorClass');
				alert('กรุณากรอกข้อมูล E-mail');
				return;
			}
			if( tel == "" || tel == null){
				status = false;
				//$('#tel ').css({'color':'red','border-style':'solid','border-color':'red'});
				$('#tel ').addClass('myErrorClass');
				alert('กรุณากรอกข้อมูล เบอร์โทรศัพท์');
				return;
			}
			
			if( status === true ){
				alertify.confirm('บันทึก','ยืนยันการแก้ไขข้อมูล', 
				function(){
				$.post(
					'./ajax/ajax_update_emp.php',
					{
						mr_position_id	 : mr_position_id,
						floor_id	 : floor_id,
						mr_branch_floor	 : mr_branch_floor,
						pass_emp	 : pass_emp,
						user_id		 : user_id,
						emp_id		 : emp_id,
						name		 : name,		
						last_name 	 : last_name,
						tel 		 : tel,		
						tel_mobile 	 : tel_mobile,
						email 		 : email,
						mr_branch_id : mr_branch_id,
						department : department,
					},
					function(res) {
						if ( res.status == 200 ){
							alertify.alert('บันทึก',res.error.message);
						}else{
							alertify.alert('บันทึก',res.error.message);
						}

					},'json');
				},function(){ });
			}else{
				alert('กรุณากรอกข้อมูลให้ครบถ้วน');
			}	
		});

	$('#mr_branch_id').select2({
		theme: "bootstrap4",
		placeholder: "เลือกสาขา",
		ajax: {
			url: "ajax/ajax_getdata_branch_select.php",
			dataType: "json",
			delay: 250,
			processResults: function (data) {
				return {
					results : data
				};
			},
			cache: true
		}
	}).on('select2:select', function (e) {
			console.log($('#mr_branch_id').val());
			
			

			var branch_id = $('#mr_branch_id').val();
			$.ajax({
				//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
				url: 'ajax/ajax_autocompress_chang_branch.php',
				type: 'POST',
				data: {
					branch_id: branch_id
				},
				dataType: 'json',
				success: function(res) {
					if(res.status == 200) {
						$('#mr_branch_floor').html(res.data);
					}
					
				}
			})

		});

		
{% endblock %}				
{% block javaScript %}
				
{% endblock %}					
				
{% block Content %}

	<br>
	<br>
	<div class="container">
			<div class="form-group" style="text-align: center;">
				 <label><h4> ข้อมูลผู้สั่งงาน </h4></label>
			</div>	
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="{{ user_data.mr_emp_id }}">
	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน" value="{{ user_data.mr_emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name" placeholder="ชื่อ" value="{{ user_data.mr_emp_name }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="last_name" placeholder="นามสกุล" value="{{ user_data.mr_emp_lastname }}" >
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input maxlength="10" type="text" class="form-control form-control-sm" id="tel" placeholder="เบอร์โทรศัพท์" value="{{ user_data.mr_emp_tel }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input maxlength="10" type="text" class="form-control form-control-sm" id="tel_mobile" placeholder="เบอร์มือถือ" value="{{ user_data.mr_emp_mobile }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						Email :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="email" placeholder="email" value="{{ user_data.mr_emp_email }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
					ตำแหน่ง					:
					</div>	
					<div class="col-8" style="padding-left:0px">
					<input type="text" readonly class="form-control form-control-sm" id="mr_position_id" placeholder="" value="{{ user_data.mr_position_name }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
					สาขา :
					</div>	
					<div id="mr_branch_error"class="col-8" style="padding-left:0px">
					<select class="form-control-lg" id="mr_branch_id" style="width:100%;">
						<option selected value="{{ user_data.mr_branch_id }}">{{ user_data.mr_branch_name }}</option>
					</select>
					
							
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อหน่วยงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="department" style="width:100%;">
							{% for s in department_data %}
								<option value="{{ s.mr_department_id }}" {% if s.mr_department_id == user_data.mr_department_id %} selected="selected" {% endif %} >{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
					ชั้น :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="mr_branch_floor" style="width:100%;" >
							<option value=""> ไม่มีข้อมูล</option>
								<option selected value="{{ user_data.mr_branch_floor }}">{{ user_data.mr_branch_floor }}</option>			
								{{floor}}
						</select>
					</div>		
				</div>			
			</div>
			
			
			
			
			
			<!-- <div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสหน่วยงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_depart" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" value="{{ user_data.mr_department_code }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้น :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="floor" placeholder="ชั้น" value="{{ user_data.mr_department_floor }}">
					</div>		
				</div>			
			</div>
			
				 -->
	
			{% if role_id == 2 or role_id == 5%}
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px;color:red;">
					***สถานที่ปฏิบัติงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="mr_role_id" style="width:100%;">
							<option value="2" {% if role_id == 2 %}selected {% endif %}> สำนักงาน(พหลโยธิน)</option>				
							<option value="5" {% if role_id == 5 %}selected {% endif %}> สาขา/สาขาและอาคารอื่นๆ</option>				
						</select>
					</div>		
				</div>			
			</div>
			{% endif %}
			
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึกการแก้ไข</button>
				<br>
				<center>
					<a href = "../user/change_password.php?usr={{ userID }}"><b> เปลี่ยนรหัสผ่าน คลิกที่นี่</b></a>
				</center>
			</div>
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp_branch.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
	  }


{% endblock %}
{% block scriptImport %}

    

		<link rel="stylesheet" href="../themes/jquery/jquery-ui.css">
		<script src="../themes/jquery/jquery-ui.js"></script>
		<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
		<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

			


{% endblock %}

{% block domReady %}
		$("#barcode").focus();
		$("#btn_save").click(function(){
			load_data();
		});	
	var table = $('#tb_work_order').DataTable({
			'responsive': true,
			'pageLength': '100',
			'columns': [
				{ 'data':'no' },
				{ 'data':'sys_timestamp'},
				{ 'data':'supper_barcode' },
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_type_work_name' },
				{ 'data':'mr_status_name' },
				{ 'data':'acctiom' }
			],
			
			'scrollCollapse': true 
		});
	var tb_keyin = $('#tb_keyin').DataTable({
			'responsive': true,
			'pageLength': '100',
			'columns': [
				{ 'data':'no' },
				{ 'data':'sys_timestamp'},
				{ 'data':'supper_barcode' },
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_type_work_name' },
				{ 'data':'mr_status_name' },
				{ 'data':'acctiom' },
				{ 'data':'check_print' }
			],
			
			'scrollCollapse': true 
		});

		$('#select-all').on('click', function(){
			// Check/uncheck all checkboxes in the table
			var rows = tb_keyin.rows({ 'search': 'applied' }).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		 });		
			
		
	//load_all_data();
	load_data_resived();
	$('#date_report').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
	});	
{% endblock %}
{% block javaScript %}
		function cancle_work(main_id,remark){						
			var r = confirm("ยืนยันการยกเลิกการส่งเอกสาร!");
			if (r == true) {
				$.post(
					'./ajax/ajax_cancle_by_mailroom.php',
					{
						mr_work_main_id 		 : main_id,
						remark_can 				 : 'ยกเลิกการจัดส่ง(ไม่พบเอกสารมาจากสาขา)',
						remark 					 : remark,
					},
					function(res) {
						alert(res.message);
						var barcode 			= $("#barcode").val();
						var status 				= true;
						
						if(barcode == "" || barcode == null){
							//load_all_data();
							load_data();
						}else{
							load_data();
						}
					},'json'
				);
			}				
		}

		function update_send_work(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			
				if(barcode == "" || barcode == null){
					status = false;
					$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});

				}
			if( status === true ){
					$.ajax({
						url: "ajax/ajax_update_send_work.php",
						type: "post",
						dataType:'json',
						data: {
							'barcode': barcode
						},
					beforeSend: function( xhr ) {
						$('#bg_loader').show();
					}
					}).done(function( msg ) {

						if(msg.status == 200){
							$("#barcode").val('');
							load_data();
							load_data_resived()
						}else{
							alertify.alert(msg.message);
						}
					});
					
				}
		}
		
	function load_data(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			
			if(barcode == "" || barcode == null){
				status = false;
				$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
			}
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_load_data_reciever_branch.php",
					type: "post",
					dataType:'json',
					data: {
						'barcode': barcode
					},
				   beforeSend: function( xhr ) {
						$('#bg_loader').show();		
					}
				}).done(function( msg ) {
					
					if(msg.status == 200){
						$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
						$('#tb_work_order').DataTable().clear().draw();
						$('#tb_work_order').DataTable().rows.add(msg.data).draw();
						//alert('22222');
					}else{
						$('#barcode').val('');
						$('#barcode').focus();
						$('#tb_work_order').DataTable().clear().draw();
						//alertify.alert(msg.message);
					}
				});
			}
		}

		function load_all_data(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_load_data_reciever_branch_all.php",
					type: "post",
					dataType:'json',
					data: {
						'barcode': barcode
					},
				   beforeSend: function( xhr ) {
						$('#bg_loader').show();
					}
				}).done(function( msg ) {
					if(msg.status == 200){
						$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
						//$('#bg_loader').hide();
						$('#tb_work_order').DataTable().clear().draw();
						$('#tb_work_order').DataTable().rows.add(msg.data).draw();
					}else{
						$('#tb_work_order').DataTable().clear().draw();
						alertify.alert(msg.message);
					}
				});
			}
		}
		
		function update_barcode() {
			var barcode 			= $("#barcode").val();
			var sub_barcode 		= $("#sub_barcode").val();
			var mr_round_id 		= $("#mr_round_id").val();
			var status 				= true;
			
			if(sub_barcode == "" || sub_barcode == null){
				$('#sub_barcode').css({'color':'red','border-style':'solid','border-color':'red'});
				status = false;
			}else{
				status = true;
			}
		
			//console.log(status);
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_update_work_branch_by_barcode.php",
					type: "post",
					dataType: "json",
					data: {
						'mr_round_id': mr_round_id,
						'barcode': barcode,
						'sub_barcode': sub_barcode
					},
					success: function(res){
						var str = '';
						var str0 = '';
						var str1 = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
						var str2 = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
							str0+='<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
							str0+='<span aria-hidden="true">&times;</span>';
							str0+='</button>'
							str0+='<strong>  '+res.title+'  </strong>';
							str0+= res.message;
							str0+='</div>';
							if(res.status == 200){	
								$("#sub_barcode").val('');
								str = str1+str0;
								load_data();
							}else{
								str = str2+str0;
								load_data();
							}
							load_data();
							$('#alert_').html(str);	
						// if(!!res){
						// 	var str = '';
						// 	var str0 = '';
						// 	var str1 = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
						// 	var str2 = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
						// 		str0+='<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
						// 		str0+='<span aria-hidden="true">&times;</span>';
						// 		str0+='</button>'
						// 		str0+='<strong>  '+res['st']+'  </strong>';
						// 		str0+= res['msg'];
						// 		str0+='</div>';
						// 		if(res['st']=='success'){	
						// 			$("#sub_barcode").val('');
						// 			str = str1+str0;
						// 			load_data();
						// 		}else{
						// 			str = str2+str0;
						// 		}
						// 		$('#alert_').html(str);	
						// }
						$('#sub_barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
					}								
				});	
			}
		}

		function keyEvent( evt, barcode )
		{
			if(window.event)
				var key = evt.keyCode;
			else if(evt.which)
				var key = evt.which;
				
			if( key == 13 ){
				load_data();
				$("#sub_barcode").focus();
			}
		}
		function keyEvent_subbarcode( evt )
		{
			if(window.event)
				var key = evt.keyCode;
			else if(evt.which)
				var key = evt.which;
				
			if( key == 13 ){
				update_barcode();
			}
		}



function load_data_resived() {
	var date_report 		= $('#date_report').val();		
	var round_printreper  	= $('#round_printreper').val() ;	
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_load_data_show_reciever.php",
		data: {
						'date_report'		: date_report,
						'round_printreper'	: round_printreper
					},
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();

		}else{
			$('#tb_keyin').DataTable().clear().draw();
		}
	  });
}


function print_option(type){
	var dataall = [];
	var tbl_data = $('#tb_keyin').DataTable();
	tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		dataall.push(this.value);
	});

	if(dataall.length < 1){
		alertify.alert("ผิดพลาด","ท่านยังไม่เลือกรายการเอกสาร"); 
		return;
	}

	var round = $('#round_printreper').select2('data')
	$('#round_name').val(round[0].text);
	$('#date').val($('#date_report').val());

	var newdataall = dataall.join(",");
	//console .log(dataall);
	
	$('#data_option').val(newdataall);
	
	if(type==0){
		$("#print_option").attr('action', 'print_peper_all_data.php');
	}

	$('#print_option').submit();

}


{% endblock %}

{% block Content %}
		<div class="row" border="1">
			<div class="col">
			</div>
			<div class="col-12">
				<div class="card">
					<h4 class="card-header">รับเอกสารจากสาขา</h4>
					<div class="card-body">
							<div class="row">
								<div class="col-sm-3">
								<label>วันที่</label>
								<input type="text" value="{{today}}" class="form-control mb-sm-0" id="today" placeholder="To day" readonly>	
								</div>
								<div class="col-sm-3">
									<label>รอบเอกสาร</label>
									 <select class="form-control" id="mr_round_id">
										 {% for r in round %}
										 <option value="{{r.mr_round_id}}">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
										 {% endfor %}
										</select>
								</div>
							</div>
						<hr>
							<div class="row">
								<div class="col-sm-4">
									<label>รหัสบาร์โค้ดใบปะหน้าซองใหญ่</label>
									<div class="input-group">
										<input type="text" class="form-control mb-sm-0" id="barcode" placeholder="Barcode" onkeyup="keyEvent(event,this.value);">									  
										<div class="input-group-append">
										<button type="button" class="btn btn-outline-primary" id="btn_save">ค้นหา</button>
									  </div>
									</div>
								</div>
							</div>
						
						
						<div class="row justify-content-center">
							<div class="col-sm-4">
								<label>เลขที่เอกสาร</label>
								<div class="input-group">
									<input type="text" class="form-control mb-5 mb-sm-0" id="sub_barcode" placeholder="Barcode" onkeyup="keyEvent_subbarcode(event);"><br>
									<div class="input-group-append">
										<button type="button" class="btn btn-outline-primary" onclick="update_barcode();">บันทึก</button>
									</div>
								</div>
								
							</div>
						</div>
						<hr>
						<div class="col" id="alert_">
						</div>
						<div class="table-responsive">
						  <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
							<thead>
							  <tr>
								<th>#</th>
								<th>Date/Time</th>
								<th>ใบคุม</th>
								<th>Bacode</th>
								<th>Sender</th>
								<th>Receiver</th>
								<th>Type Work</th>
								<th>Status</th>
								<th>Actiom</th>
							  </tr>
							</thead>
							<tbody>
							</tbody>
						  </table>
						</div>
						<hr>
						<div class="row justify-content-center">
						{#<div class="col-sm-12 text-center">
								<button type="button" class="btn btn-outline-primary" id="" onclick="update_send_work();">บันทึก</button>
								<button type="button" class="btn btn-outline-secondary" id="">ยกเลิก</button>
							</div>#}
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	<br>

		























	<form id="print_option" action="print_option.php" method="เำะ" target="_blank">
		<input type="hidden" id="data_option" name="data_option">
		<input type="hidden" id="round_name" name="round_name">
		<input type="hidden" id="date" name="date">
	</form>
		
<div class="row px-5">
	<div class="col-md-12 text-right">
	<form id="form_print_post_in" action="#" method="post" target="_blank">
		<table>
			<tr>
				<td>
					<span class="box_error" id="err_round"></span>
					<select onchange="load_data_resived();"  data-error="#err_round" class="form-control form-control-sm" id="round_printreper" name="round_printreper">
						<option value="0">กรุณาเลือกรอบ</option>
						{% for r in round %}
						<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
						{% endfor %}
					</select>
				</td>
				<td>
					<span class="box_error" id="err_date_report"></span>
					<input data-error="#err_date_report" onchange="load_data_resived();" name="date_report" id="date_report" class="form-control" type="text" value="{{today}}" placeholder="{{today}}">
				</td>
				<td>
					<button onclick="load_data_resived();" type="button" class="btn btn-outline-secondary" id="">ค้นหา</button>
					{# <button onclick="print_option(1);" type="button" class="btn btn-sm btn-outline-secondary" id="">พิมพ์ใบงาน</button> #}
				</td>
			</tr>
		</table>
	</form>
	</div>	
</div>





					<div class="row px-5">
						<div class="col-md-12">
							<hr>
							<h5 class="card-title">รายการรับเข้่า</h5>
							<table class="table" id="tb_keyin">
								<thead class="thead-light">
									<tr>
									<th>#</th>
									<th>Date/Time</th>
									<th>ใบคุม</th>
									<th>Bacode</th>
									<th>Sender</th>
									<th>Receiver</th>
									<th>Type Work</th>
									<th>Status</th>
									<th>Actiom</th>
									<th>

										<label class="custom-control custom-checkbox">
											<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">เลือกทั้งหมด</span>
										</label>
										<button type="button" class="btn btn-link" onclick="print_option(0);">พิมพ์ใบคุม</button>
									</th>
								  </tr>
								</thead>
								<tbody>
							
								</tbody>
							  </table>


						</div>
					</div>
					

				</div>
			  </div>
		
		</div>
	</div>

</div>
 
	
	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp_branch.tpl" %}

{% block title %}{% parent %} - รายงาน {% endblock %}

{% block menu_5 %} active {% endblock %}

{% block styleReady %}
    #btn_exports:hover{
        color: #FFFFFF;
        background-color: #055d97;
    }

    #btn_exports{
        border-color: #0074c0;
        color: #FFFFFF;
        background-color: #0074c0;
    }

    #detail_sender_head h4{
        text-align:left;
        color: #006cb7;
        border-bottom: 3px solid #006cb7;
        display: inline;
    }

    #detail_sender_head {
        border-bottom: 3px solid #eee;
        margin-bottom: 20px;
        margin-top: 20px;
    }

    #detail_receiver_head h4{
        text-align:left;
        color: #006cb7;
        border-bottom: 3px solid #006cb7;
        display: inline;
    }

    #detail_receiver_head {
        border-bottom: 3px solid #eee;
        margin-bottom: 20px;
        margin-top: 40px;
    }

    .input-group-addon{
        background-color: #FFFFFF;
        border: 1px solid #055d97;
    }

{% endblock %}

{% block domReady %}
    // $('#month_success').select2({
    //     data: [
    //         { id: 1, text: 'text' }
    //     ]
    // });

    $('#btn_exports').click(function() {
        var months = $('#month_success').val();
        var years = $('#year_success').val();
        var status = true;

        if(months == "" || months == null) {
            status = false;
        }

        if (years == "" || years == null) {
            status = false;
        }

        if(!status) {
             alertify.alert('ยังไม่เลือกเงื่อนไข');
        } else {
            window.open('export_dateSuccess.php?month='+months+"&year="+years, "_blank")
        }
        
    });


    $('#year_success').on('select2:select', function (e) {
        $('#month_success').empty().trigger('change')
        $.ajax({
            url: "./ajax/ajax_getMonthSuccess.php",
            type: "POST",
            data: {
                year: e.params.data.text
            },
            dataType: 'json',
            success: function(res) {
                $('#month_success').select2(res).trigger('change');
            }
        })
    });

{% endblock %}

{% block javaScript %}

{% endblock %}

{% block Content %}
<div class="container">
    <div style="text-align:center; color:#0074c0; margin-top:20px;">
        <label>
            <h3><b>รายงานรอบนำส่งเอกสาร</b></h3>
        </label>
    </div>

    <div class="form-group" style="" id="detail_sender_head">
        <h4>รายงานรอบนำส่งเอกสาร (ประวัติการนำส่ง) </h4>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label for="date_success" class="col-sm-2 font-weight-bold">ปี:</label>
                <div class="col-sm-10">
                    <select class="form-control form-control-sm" name="yearSuccess" id="year_success">
                        <option value="">--- เลือกปี ---</option>
                        {% if years|length > 0 %}
                            {% for year in years %}
                                <option value="{{ year }}">{{ year }}</option>
                            {% endfor %}
                        {% endif %}
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label for="date_success" class="col-sm-2 font-weight-bold">เดือน:</label>
                <div class="col-sm-10">
                    <select class="form-control form-control-sm" name="monthSuccess" id="month_success">
                        <option value="">--- เลือกเดือน ---</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
     <div class="float-right">
            <button type="button" class="btn btn-outline-primary" id="btn_exports">ออกรายงาน</button>
    </div>
    </div>
    


{% endblock %}


{% block debug %}

{% if debug != '' %}
<pre>{{ debug }}</pre>
{% endif %}

{% endblock %}

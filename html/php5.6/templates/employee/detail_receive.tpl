{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
		




		
{% endblock %}				
{% block javaScript %}
	
				
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
			<div class="form-group" style="text-align: center;">
				{% if work_inout_data.mr_emp_id == user_data.mr_emp_id %}
					<label><h4> ข้อมูลผู้เซ็นรับเอกสาร </h4></label>
				{% else %}
					<label><h4> ข้อมูลผู้เซ็นรับเอกสาร(แทน) </h4></label>
				{% endif %}
			</div>	
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="{{ user_data.mr_emp_id }}">
	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน" value="{{ user_data.mr_emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name" placeholder="ชื่อ" value="{{ user_data.mr_emp_name }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="last_name" placeholder="นามสกุล" value="{{ user_data.mr_emp_lastname }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel" placeholder="เบอร์โทรศัพท์" value="{{ user_data.mr_emp_tel }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel_mobile" placeholder="เบอร์มือถือ" value="{{ user_data.mr_emp_mobile }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						แผนก :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="department" value="{{ user_data.department_name }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้นของผู้รับเอกสาร :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="floor" value="{{ user_data.floor }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อเอกสาร :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="topic" value="{{ user_data.mr_topic }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						หมายเหตุ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="topic" value="{{ user_data.mr_work_remark }}" readonly>
					</div>		
				</div>			
			</div>
			
			
			
			
			
			<!-- <div class="form-group">
				<a href="search.php"><button type="button" class="btn btn-outline-primary btn-block" >ย้อนกลับ</button></a>
			</div>
			 -->
			
	
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

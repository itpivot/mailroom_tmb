{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block Content %}
	<div class="row" >
		<div class="col-1">
		</div>
		<div class="col-10">
			<div class="card" style="width:100%">
				<div class="card-body">
					<h4 class="card-title">รายงานสรุปการส่งเอกสาร</h4>
				{% for m in month%}
					<a href="excel.report_send.php?&month={{m.month_number}}&year={{m.year}}" target="_blank">{{ m.month }}</a>
					<br />
				{% endfor %}
				</div>
			</div>	
		</div>	
	</div>	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

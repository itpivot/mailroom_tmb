{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
	  }

	  #loader{
		width:100px;
		height:100px;
		display:table;
		margin: auto;
		border-radius:50%;
	  }#bg_loader{
		  position:absolute;
		  top:0px;
		  background-color:rgba(255,255,255,0.7);
		  height:100%;
		  width:100%;
		  z-index : 999;
	  }
{% endblock %}
{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}
	  $('#bg_loader').hide();
		//var users_login = JSON.parse('{{ user_arr }}');
	
		$('.input-daterange').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		});	
		
		
		
var table = $('#tb_work_order').DataTable({ 
    "responsive": true,
	"searching": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
	"pageLength": 100, 
    'columns': [
		{ 'data':'no' },
		{ 'data':'time_test'},
		{ 'data':'mr_work_barcode' },
		{ 'data':'name_receive' },
		{ 'data':'name_send' },
		{ 'data':'mr_type_work_name' },
		{ 'data':'mr_status_name' },
		{ 'data':'mr_work_remark' },
		{ 'data':'ch_data' }
    ]
});

		$('#select-all').on('click', function(){
		   // Check/uncheck all checkboxes in the table
		   var rows = table.rows({ 'search': 'applied' }).nodes();
		   $('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
		
		<!-- Search 'csrf_token' : $('#csrf_token').val() -->
		$("#btn_search").click(function(){
			var barcode 		= $("#barcode").val();
			var pass_emp_send 	= $("#pass_emp_send").val();
			var pass_emp_re 	= $("#pass_emp_re").val();
			var start_date 		= $("#start_date").val();
			var end_date 		= $("#end_date").val();
			var status 			= $("#status").val();
			
			$.ajax({
				dataType: "json",
				method: "POST",
				url: "ajax/ajax_search_work_mailroom.php",
				data: { 	
					'barcode': barcode,
						'receiver': pass_emp_re,
						'sender': pass_emp_send,
						'start_date': start_date,
						'end_date': end_date,
						'csrf_token' : $('#csrf_token').val(),
						'status': status
				  },
				 beforeSend: function( xhr ) {
					  $('#bg_loader').show();
				  }
			  }).done(function( msg ) {
				$('#csrf_token').val(msg.token);
				$('#bg_loader').hide();
				  if(msg.status == 200){
					  $('#bg_loader').hide();
					  $('#tb_work_order').DataTable().clear().draw();
					  $('#tb_work_order').DataTable().rows.add(msg.data).draw();
				  }else{
					  alertify.alert('เกิดข้อผิดพลาด',msg.message);
					  return;
				  }
			  });

			});
			<!-- end Search -->
		
		
		
		$("#pass_emp_send").keyup(function(){
			var pass_emp = $("#pass_emp_send").val();
			
			$.ajax({
				url: "ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200) {
						if(res.data != false) {
							$("#name_sender").val(res['data']['mr_emp_name']);
							$("#pass_depart_send").val(res['data']['mr_department_code']);
							$("#floor_send").val(res['data']['mr_department_floor']);
							$("#depart_send").val(res['data']['mr_department_name']);
							$("#emp_id_send").val(res['data']['mr_emp_id']);
						}
					}
					
					
				}
												
			});
									
		});
		
		$("#pass_emp_re").keyup(function(){
			var pass_emp = $("#pass_emp_re").val();
			$.ajax({
				url: "ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200) {
						if(res.data != false) {
							$("#name_receiver").val(res['data']['mr_emp_name']);
							$("#pass_depart_re").val(res['data']['mr_department_code']);
							$("#floor_re").val(res['data']['mr_department_floor']);
							$("#depart_re").val(res['data']['mr_department_name']);
							$("#emp_id_re").val(res['data']['mr_emp_id']);
						}
					}
				}
											
			});
									
		});
		
		
		$('#name_receiver_select').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select_search.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setForm(e.params.data);
			//setUsersFrom(users_login, 'send');
		});

		function setForm(data) {
				var emp_id = parseInt(data.id);
				var fullname = data.text;
				$.ajax({
					url: './ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						//console.log(res);
						if(res.status == 200) {
							if(res.data != false) {
								$("#pass_depart_re").val(res['data']['mr_department_code']);
								$("#floor_re").val(res['data']['mr_department_floor']);
								//$("#floor_id").val(res['data']['mr_floor_id']);
								$("#depart_re").val(res['data']['mr_department_name']);
								//$("#emp_id").val(res['data']['mr_emp_id']);
								$("#pass_emp_re").val(res['data']['mr_emp_code']);
							}
						}
					
					}
				})
		}
		
		$('#name_send_select').select2({
				placeholder: "ค้นหาผู้ส่ง",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select_search.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setFormSend(e.params.data);
			//setUsersFrom(users_login, 're');
		});

		function setFormSend(data) {
				var emp_id = parseInt(data.id);
				var fullname = data.text;
				$.ajax({
					url: './ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						//console.log(res);
						if(res.status == 200) {
							if(res.data != false) {
								$("#pass_depart_send").val(res['data']['mr_department_code']);
								$("#floor_send").val(res['data']['mr_department_floor']);
								//$("#floor_id").val(res['data']['mr_floor_id']);
								$("#depart_send").val(res['data']['mr_department_name']);
								//$("#emp_id").val(res['data']['mr_emp_id']);
								$("#pass_emp_send").val(res['data']['mr_emp_code']);
						
							}
						}
						
						
						
					}
				})
		}
		
		
		
		
		
		
		$("#btn_excel").click(function() {
			var data = new Object();
			data['barcode'] 			= $("#barcode").val();
			data['sender'] 				= $("#pass_emp_send").val();
			data['receiver'] 			= $("#pass_emp_re").val();
			data['start_date'] 			= $("#start_date").val();
			data['end_date'] 			= $("#end_date").val();
			data['status'] 				= $("#status").val();
			var param = JSON.stringify(data);
		
			window.open('excel.report.php?params='+param);
		
		});
		
		function setUsersFrom(users, type) {
			$("#pass_depart_" + type).val("");
			$("#floor_" + type).val("");
			$("#depart_" + type).val("");
			$("#pass_emp_" + type).val("");

			$.ajax({
					url: './ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: parseInt(users.mr_emp_id)
					},
					dataType: 'json',
					success: function (res) {
						if(res.status == 200) {
							if(res.data != false) {
								var users = {
									id: parseInt(res.data.mr_emp_id),
									text: res.data.mr_emp_code+" - "+res.data.mr_emp_name+" "+res.data.mr_emp_lastname
								};
		
								var option = new Option(users.text, users.id, true, true);
								if(type == "send") {
									$('#name_send_select').append(option).trigger('change');
								} else {
									$('#name_receiver_select').append(option).trigger('change');
								}
								$("#pass_depart_" + type).val(res.data.mr_department_code);
								$("#floor_" + type).val(res.data.mr_department_floor);
								$("#depart_" + type).val(res.data.mr_department_name);
								$("#pass_emp_" + type).val(res.data.mr_emp_code);
							}
						}
					

					}
			});
		}
		
		
		
{% endblock %}


{% block javaScript %}
	
	
	function print_click() {
				var dataall = [];
	

				var tel_receiver = $('#tel_receiver').val();
				var tbl_data = $('#tb_work_order').DataTable();
				 tbl_data.$('input[type="checkbox"]:checked').each(function(){
					 //console.log(this.value);
					//  dataall.push(this.value);
				
					var token = encodeURIComponent(window.btoa(this.value));
					
					dataall.push(token);
				  });
				  //console.log(tel_receiver);
				  if(dataall.length < 1){
					 alertify.alert("ตรวจสอบข้อมูล","ท่านยังไม่เลือกงาน"); 
					 return;
				  }

				
				 // return;
				var newdataall = dataall.join(",");

				window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
			}	

{% endblock %}

{% block Content2 %}

		<div class="row" border="1">
			<div class="col">
				
			</div>
			<div class="col-10">
				<div class="card">
					<h4 class="card-header">ค้นหา</h4>
					<div class="card-body">
						<form>
							<div class="row justify-content-between">
								<div class="card col-5" style="padding:0px;margin-left:20px">
									<h5 style="padding:5px 10px;">ผู้ส่ง</h5>
									<div class="card-body">
										<form>
											<div class="row ">
												<div class="col-12">
													<select class="form-control-lg" id="name_send_select" style="width:100%;" >
														{# < option value = "0" > ค้นหาผู้ส่ง < /option>
														{% for e in emp_data %}
															<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
														{% endfor %} #}
													</select>
													<input type="hidden" id="pass_emp_send">
												</div>		
											
											
											
											
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_send" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_sender" placeholder="ชื่อผู้ส่ง">
												</div> -->
											</div>
											<br>
											<div class="row ">
												<div class="col-8">
														<input type="text" class="form-control mb-5 mb-sm-2" id="pass_depart_send" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" disabled>
												</div>
												
												<div class="col-4">
														<input type="text" class="form-control mb-5 mb-sm-0" id="floor_send" placeholder="ชั้น" disabled>
												</div>
											</div>
											
											<div class="row ">
												<div class="col">
														<input type="text" class="form-control mb-5 mb-sm-2" id="depart_send" placeholder="ชื่อหน่วยงาน" disabled>
												</div>
												
											</div>
			
											
												
										</form>
									</div>
								</div>
								
								<div class="card col-5" style="padding:0px;margin-right:20px">
									<h5 style="padding:5px 10px;">ผู้รับ</h5>
									<div class="card-body">
										<form>
											<div class="row ">
											
											
												<div class="col-12">
													<select class="form-control-lg" id="name_receiver_select" style="width:100%;" >
														{# < option value = "0" > ค้นหาผู้รับ< /option>
														{% for e in emp_data %}
															<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
														{% endfor %} #}
													</select>
													<input type="hidden" id="pass_emp_re">
												</div>		
											
												
											
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_re" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_receiver" placeholder="ชื่อผู้รับ">
												</div> -->
											</div>
											<br>
											<div class="row ">
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_depart_re" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" disabled>
												</div>
												
												<div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-0" id="floor_re" placeholder="ชั้น" disabled>
												</div>
											</div>
											
											<div class="row ">
												<div class="col">
													<input type="text" class="form-control mb-5 mb-sm-2" id="depart_re" placeholder="ชื่อหน่วยงาน" disabled>
												</div>
											</div>
			
											
												
										</form>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-top:40px;">
								<div class="col-1">
								</div>
								<div class="col-10">
								<input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf}}"></input>
									<div class="input-daterange input-group" id="datepicker" data-date-format="yyyy-mm-dd">
										<input type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From date"/>
										<label class="input-group-addon" style="border:0px;">to</label>
										<input type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="To date"/>
									</div>
								</div>
								
							</div>
							
							<div class="row" style="margin-top:10px;">
								<div class="col-1">
								</div>
								<div class="col-10">
									<div class="row">
										<div class="col-6">
											<input type="text" class="form-control mb-5 mb-sm-2" id="barcode" placeholder="Tracking number">
										</div>
										<div class="col-6">
											<select class="form-control-lg" id="status" style="width:100%;">
												<option value="">สถานะงานทั้งหมด</option>
												<optgroup label="รับส่งภายไนสำนักงานใหญ่">
												{% for s in status_data %}
													<option value="{{ s.mr_status_id }}">{{ s.mr_status_name }}</option>
												{% endfor %}
												</optgroup>
												<optgroup label="รับส่งที่สาขา">
												{% for s in status_data2 %}
													<option value="{{ s.mr_status_id }}">{{ s.mr_status_name }}</option>
												{% endfor %}
												</optgroup>
												
											</select>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-top:10px;">
								<div class="col-3">
								</div>
								<div class="col-6">
									<div class="row">
										<div class="col-6">
											<button type="button" class="btn btn-outline-primary btn-block" id="btn_search">ค้นหา</button>
										</div>
										<div class="col-6">
											<button type="button" class="btn btn-outline-dark btn-block" id="btn_excel">Export Excel</button>
										</div>
									</div>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	
		 <div class="panel panel-default" style="margin-top:50px;">
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date/Time</th>
                            <th>Bacode</th>
                            <th>Receiver</th>
                            <th>Sender</th>
                            <th>Type Work</th>
                            <th>Status</th>
                            <th>Remark</th>
                            <th>
							<label class="custom-control custom-checkbox">
								<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description">ทั้งหมด</span>
							</label>
							<button type="button" onclick="print_click();" class="btn btn-outline-dark btn-sm">&nbsp;&nbsp;&nbsp; print &nbsp;&nbsp;&nbsp;&nbsp;</button>
							</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
	
	
				<div id="bg_loader" class="card">
				<img id = 'loader'src="../themes/images/spinner.gif">
			</div>	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

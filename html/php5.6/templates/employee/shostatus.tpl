{% extends "base.tpl" %}
{% block domReady %}

{% endblock %} 
	 $("#flip").click(function(){
        $("#panel").slideToggle("slow");
    });
{% block javaScript %}

function actionbuttom(data) {
			$("#data"+data).slideToggle("slow");
	//console.log(data);
}
{% endblock %}
{% block css %}
	.detel {
   
    text-align: center;
    background-color: #CCCCFF;
    border: solid 1px #c3c3c3;
	}

	.detel {
		display: none;
	}
	#panel, #flip {
    padding: 5px;
    text-align: center;
    background-color: #e5eecc;
    border: solid 1px #c3c3c3;
	}

	#panel {
		padding: 50px;
		display: none;
	}
{% endblock %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e5 %} active {% endblock %}


{% block Content %}

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2">


			<center><b>Report</b></center><br>
		
		<table class="table">
			<thead>
			
			  <tr class="active">
				<th class="text-center">NO</th>
				<th>วัน/เวลา ที่สั่งงาน</th>
				<th>status</th>
			  </tr>
			  
			</thead>
			<tbody>
			{% for w in workStatus %}
			  <tr class="warning">
				<td class="text-center">{{loop.index}}</td>
				<td>{{ w.system_date}}</td>
				<td>{{ w.status_name}}</td>
			  </tr>
			{% endfor %}
			</tbody>
		</table>
		
		
		
		</div>
	</div>
	
{% endblock %}
{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp2.tpl" %}

{% block title %}แก้ไขข้อมูลพนักงาน{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block styleReady %}
.box_error{
	font-size:12px;
	color:red;
}
{% endblock %}

{% block scriptImport %}

<script src="../themes/jquery/jquery.validate.min.js"></script>

{% endblock %}

{% block domReady %}
		
		{{alert}}
		
		$("#mr_type_work_id").change(function(){
			var type  = $(this).val();
			if(type == 2){
				$('#div_branch_id').show();
				$('#floor_branch').show();
				$('#floor_ho').hide();
			}else{
				$('#div_branch_id').hide();
				$('#floor_branch').hide();
				$('#floor_ho').show();
			}
		});

		$("#mr_branch_id").change(function(){
			var type  = $("#mr_type_work_id").val()
			var branch_id  = $(this).val();
			$.ajax({
				url: 'ajax/ajax_autocompress_chang_branch.php',
				type: 'POST',
				data: {
					branch_id: branch_id
				},
				dataType: 'json',
				success: function(res) {
					if(res.status == 200) {
						$('#floor').html(res.data);
					}
					
				}
			})
			console.log(type);
		});


		$("#btn_save").click(function(){
			if($('#myform_data_emp').valid()) {
				var form = $('#myform_data_emp');
				var serializeData = form.serializeArray();
				var data 							= $('#floor').select2('data');
				if($('#floor').val()){
					var floor2_text 					= data[0].text;
				}
				serializeData.push({ name: "floor_name", value: floor2_text });
				serializeData.push({ name: "page", value: "update" });

				$.ajax({
					method: "POST",
					dataType:'json',
					url: "ajax/ajax_update_emp_data.php",
					data: serializeData,
					beforeSend: function() {
						// setting a timeout
						$("#bg_loader").show();
					},
					error: function (error) {
					  alert('error; ' + eval(error));
					  $("#bg_loader").hide();
					 //location.reload();
					}
				  })
				  .done(function( res ) {
					alert(res['msg']);
					location.reload();
				  });
			}
		});
		




		
$('#myform_data_emp').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'mr_branch_id': {
			required: true
		},
		'floor': {
			required: true
		},
		'emp_code': {
			required: true
		},
		'emp_name': {
			required: true
		},
		'last_name': {
			required: true
		},
		'tel': {
			required: true
		},
		'tel_mobile': {
			required: true
		},
		'email': {
			required: true
		},
		'department': {
			required: true
		},
		'position': {
			required: true
		},
	  
	},
	messages: {
		'add_cost_code': {
		  required: 'กรุณาระบุ รหัสค่าใช้จ่าย.'
		},
		'add_cost_name': {
		  required: 'กรุณาระบุ ชื่อ'
		}
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });

		
{% endblock %}				
{% block javaScript %}
				
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
		<form id="myform_data_emp">
			<div class="form-group" style="text-align: center;">
				 <label><h4> แก้ไขข้อมูลพนักงาน </h4></label>
			</div>	
				<input name="mr_emp_id" id="mr_emp_id" type="hidden" value="{{empdata.mr_emp_id}}">
				<div class="form-group" id="div_branch_id">
					<div class="row">
						<div class="col-4" style="padding-right:0px">
							สาขา :
						</div>	
						<div class="col-8" style="padding-left:0px">
						<span class="box_error" id="err_mr_branch_id"></span><br>
							<select data-error="#err_mr_branch_id" class="form-control-lg" id="mr_branch_id" name="mr_branch_id" style="width:100%;">
								{% if empdata.mr_branch_id == '' %}
										<option value="0" selected="selected">ไม่มี</option>
								{% endif %}		
								{% for b in branch %}
									<option value="{{ b.mr_branch_id }}" {% if b.mr_branch_id == empdata.mr_branch_id %} selected="selected" {% endif %}>{{ b.mr_branch_code }}:{{ b.mr_branch_name }}</option>
								{% endfor %}					
							</select>
						</div>		
					</div>			
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-4" style="padding-right:0px ;">
							ชั้น :
						</div>	
						<div id="floor_ho" class="col-8" style="padding-left:0px;">
						<span class="box_error" id="err_floor"></span><br>
							<select data-error="#err_floor" class="form-control-lg" id="floor" name="floor" style="width:100%;">
								{% if empdata.mr_floor_id == '' %}
											<option value="" selected="selected">ไม่มี</option>
								{% endif %}		
								{% for f in floor_data2 %}
									<option value="{{ f.mr_floor_id }}" {% if f.mr_floor_id == empdata.mr_floor_id %} selected="selected" {% endif %}>{{ f.mr_floor_mame }}</option>
								{% endfor %}					
							</select>
						</div>	
						
					</div>			
				</div>
	
	
	
	
	
	
	
	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_emp_code"></span><br>
						<input type="text" data-error="#err_emp_code" class="form-control form-control-sm" id="emp_code" name="emp_code" placeholder="รหัสพนักงาน" value="{{ empdata.mr_emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_emp_name"></span><br>
						<input type="text" data-error="#err_emp_name" class="form-control form-control-sm" id="emp_name"  name="emp_name" placeholder="ชื่อ" value="{{ empdata.mr_emp_name }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_last_name"></span><br>
						<input type="text" data-error="#err_last_name" class="form-control form-control-sm" id="last_name" name="last_name" placeholder="นามสกุล" value="{{ empdata.mr_emp_lastname }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_tel"></span><br>
						<input type="text" data-error="#err_tel" class="form-control form-control-sm" id="tel" name="tel" placeholder="เบอร์โทรศัพท์" value="{{ empdata.mr_emp_tel }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_tel_mobile"></span><br>
						<input type="text" data-error="#err_tel_mobile" class="form-control form-control-sm" id="tel_mobile" name="tel_mobile" placeholder="เบอร์มือถือ" value="{{ empdata.mr_emp_mobile }}">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						Email :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_email"></span><br>
						<input type="text" data-error="#err_email" class="form-control form-control-sm" id="email" name="email" placeholder="email" value="{{ empdata.mr_emp_email }}">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						แผนก :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_department"></span><br>
						<select data-error="#err_department" class="form-control-lg" id="department" name="department" style="width:100%;">
							{% for s in department_data %}
								<option value="{{ s.mr_department_id }}" {% if s.mr_department_id == empdata.mr_department_id %} selected="selected" {% endif %}>{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			
		
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ตำแหน่ง :
					</div>	
					<div class="col-8" style="padding-left:0px">
					<span class="box_error" id="err_position"></span><br>
						<select data-error="#err_position" class="form-control-lg" id="position" name="position" style="width:100%;">
							{% for p in position_data %}
								<option value="{{ p.mr_position_id }}" {% if p.mr_department_id == empdata.mr_department_id %} selected="selected" {% endif %}>{{ p.mr_position_code }} - {{ p.mr_position_name}}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึกการแก้ไขชั้น</button>
				<br>
				
			</div>
		</form>
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

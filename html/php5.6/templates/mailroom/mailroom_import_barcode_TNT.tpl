{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}
      #tb_work_order {
        font-size: 13px;
      }
	  .panel {
		margin-bottom : 5px;
	  }
#loader{
	  width:5%;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:absolute;
		top:0px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">

<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>

{% endblock %}

{% block domReady %}
$('#bg_loader').hide()
		load_data();
	var table = $('#tb_work_order').DataTable({
			'responsive': true,
			'searching': false,
			'lengthChange': false,
			'columns': [
				{ 'data':'no' },
				{ 'data':'sys_date'},
				{ 'data':'date_send' },
				{ 'data':'barcode_tnt' },
				{ 'data':'mr_branch_code' },
				{ 'data':'status' }
			],
			
			'scrollCollapse': true 
		});
$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});
			
		
		
{% endblock %}


{% block javaScript %}
		
	function remove_barcode_TNT() {
		alertify.confirm("ลบข้อมูล","ยืนยันการลบข้อมูล.",
		function(){
			ok_remove_barcode_TNT();
		},
		function(){
		  alertify.error('Cancel');
		});
	}
	function ok_remove_barcode_TNT() {
		var dataall 		= [];
		var tbl_data 		= $('#tb_work_order').DataTable();
		tbl_data.$('input[type="checkbox"]:checked').each(function(){
			dataall.push(this.value);
		});
			
		if(dataall.length < 1){
			alertify.alert("เกิดข้อผิดพลาด","ท่านยังไม่เลือกงาน"); 
			return;
		}
	
		var newdataall = dataall.join(",");
	
		$.ajax({
			url: "ajax/ajax_remove_barcode_tnt.php",
			type: "post",
			dataType:'json',
			data: {
				'mr_tnt_barcode_import_id'	: newdataall
			},
			beforeSend: function( xhr ) {
				$('#bg_loader').show();	
			}
		}).done(function( msg ) {
			$('#bg_loader').hide();
			load_data();
		});
	}
	
	function load_data(){

		var mr_branch_id = $('#mr_branch_id').val();
		var hub_id = $('#hub_id').val();
		var data_all = $("#data_all").prop("checked") ? '1' : '0';
		if(hub_id == 'all_branch'){
			$('#div_mr_user_id').hide();
			$('#div_tnt_tracking').show();
		}else{
			$('#div_mr_user_id').show();
			$('#div_tnt_tracking').hide();
		}	

		$.ajax({
			url: "ajax/ajax_load_barcode_TNT.php",
			type: "post",
			dataType:'json',
			data: {
				'mr_branch_id'	: mr_branch_id,
				'data_all'		: data_all,
				'hub_id'		: hub_id
			},
			beforeSend: function( xhr ) {
				$('#bg_loader').show();
			}
		}).done(function( msg ) {
			if(msg.status == 500 || msg.status == 401){
				alertify.alert(msg.message);
			}else{
				$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
				$('#bg_loader').hide();
				$('#tb_work_order').DataTable().clear().draw();
				$('#tb_work_order').DataTable().rows.add(msg.data).draw();
			}
		});
	}



		function import_excel() {
			$('#div_error').hide();	
			var formData = new FormData();
			formData.append('file', $('#file')[0].files[0]);
			if($('#file').val() == ''){
				$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
				$('#div_error').show();
				return;
			}else{
			 var extension = $('#file').val().replace(/^.*\./, '');
			 if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
				 $('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
				$('#div_error').show();
				return;
			 }
			}
			$.ajax({
				   url : 'ajax/ajax_readFile_barcode_tnt.php',
				   dataType : 'json',
				   type : 'POST',
				   data : formData,
				   processData: false,  // tell jQuery not to process the data
				   contentType: false,  // tell jQuery not to set contentType
				   success : function(data) {

					if(data.status == 200){
						if(data.data.count_error > 0){
							$('#div_error').html(data.data.txt_error)
							$('#div_error').show();
						}

						$('#bg_loader').hide();
						$('#file').val('');
						load_data();
					}

					//    if(data['count_error'] > 0){
					// 	$('#div_error').html(data['txt_error'])
					// 	$('#div_error').show();
					//    }else{

					//    }
					//    $('#bg_loader').hide();
					//    $('#file').val('');
					//    load_data();
					   
				}, beforeSend: function( xhr ) {
					$('#bg_loader').show();
				}
			});
		
		}


{% endblock %}

{% block Content2 %}
<form id="print_1" action="print_sort_branch_all.php" method="post" target="_blank">
  <input type="hidden" id="data1" name="data1">
</form>
<form id="print_2" action="print_hab_all.php" method="post" target="_blank">
  <input type="hidden" id="data2" name="data2">
</form>
		<div class="row" border="1">
			<div class="col-12">
				<div class="card">
					<h4 class="card-header">TNT Barcode Import</h4>
					<div class="card-body">
							
							
						<form id="form_import_excel">
								<label for="file">
									</label>

							<div class="col-md-3 col-lg-4">	  
							<div class="form-group">
							<a onclick="$('#modal_showdata').modal({ backdrop: false});" data-toggle="tooltip" data-placement="top" title="เลือกไฟล์ Excel ของท่าน"></a>
								<input type="file" class="form-control-file" id="file">
							</div>
							<br>
							 
							<button onclick="import_excel();" id="btn_fileUpload" type="button" class="btn btn-success"> 	
								<i class="material-icons">
									vertical_align_top
								</i>
								<br> 
								Upload
							</button>
							<a  href="template_tnt.xlsx" type="button" class="btn btn-light" download> 	
								<i class="material-icons">
									vertical_align_bottom
								</i>
								<br> 
								download templates
							</a>
							<br>
							<hr>
							<br>
							</div>
							</div>
						</form>
						<div id="div_error" class="alert alert-danger" role="alert" style="display: none;">
							
						  </div>




							<div class="row">
							<div class="col-sm-12"><br>
								{#
								<button onclick="print_byHub();"type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบคุมใหญ่</button>
								<button onclick="print_all();" type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบคุมย่อย</button>
								#}
							</div>
						</div>
						<hr>
						
						<div class="table-responsive">
						  <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
							<thead>
							  <tr>
								<th>#</th>
								<th>วันที่/เวลา Import</th>
								<th>วันที่จัดส่ง</th>
								<th>TNT Barcode</th>
								<th>สาขาปลายทาง</th>
								<th class="text-center">
									<label class="custom-control custom-checkbox">
										<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">เลือกทั้งหมด</span>
									</label>
									<button type="button" class="btn btn-outline-primary" onclick="remove_barcode_TNT();">ลบข้อมูลที่เลือก</button> 
								</th>
							  </tr>
							</thead>
							<tbody>
							</tbody>
						  </table>
						</div>
						
						<div class="row justify-content-center" style="margin-top:20px;">
								<div class="col-sm-12 text-center">
									
								</div>
							</div>
						<hr>
						
					</div>
				</div>
			</div>
		</div>
	<br>
		 
	
	
	
 <div id="bg_loader" class="card">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

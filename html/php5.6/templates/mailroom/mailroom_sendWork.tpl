{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }

      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}
      #tb_work_order {
        font-size: 13px;
      }
	  .panel {
		margin-bottom : 5px;
	  }
#loader{
	  width:5%;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:absolute;
		top:0px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">

<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap v4.6.1.css">

{% endblock %}

{% block domReady %}
$('#bg_loader').hide()
		//load_data();
		load_data_Byround_resive();
	var table = $('#tb_work_order').DataTable({
			'responsive': true,
			'searching': true,
			'lengthChange': false,
			'columns': [
				{ 'data':'no' },
				{ 'data':'remove'},
				{ 'data':'sys_timestamp'},
				{ 'data':'st_name'},
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_type_work_name' },
				{ 'data':'branch' },
				{ 'data':'send_round' },
				{ 'data':'mr_status_name' }
			],
			
			'scrollCollapse': true 
		});
$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});
			
	$('#mr_branch_id').select2({
		theme: 'bootstrap4',
	});
	$('#hub_id').select2({
		theme: 'bootstrap4',
	});

	$('#scan_round_id').select2({
		theme: 'bootstrap4',
	});

	$('#date').datepicker({
		todayHighlight	: true,
		format			: "yyyy-mm-dd",
		autoclose		: true,
		setDate			: new Date(),
		});

	$('#date_send').datepicker({
		todayHighlight	: true,
		format			: "yyyy-mm-dd",
		autoclose		: true,
		setDate			: new Date(),
		});



$('#mr_round_id').select2(
	{ width: '100%' }
);
		
{% endblock %}


{% block javaScript %}
		
	function load_data_Byround_resive() {
		var scan_round_id = $('#scan_round_id').val();
		var barcose_scan = $('#barcose_scan').val();

		$.ajax({
			url: "ajax/ajax_load_data_mailroom_send_round.php",
			type: "post",
			dataType:'json',
			data: {
				'scan_round_id'	: scan_round_id,
				'barcose_scan'		: barcose_scan,
				'page'		: 'load_data',
			},
		   beforeSend: function( xhr ) {
				$('#bg_loader').show();
			}
		}).done(function( msg ) {
				$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
				$('#bg_loader').hide();
			if(msg['status']==200){
				
				$('#tb_work_order').DataTable().clear().draw();
				$('#tb_work_order').DataTable().rows.add(msg.data).draw();
			}else{
				$('#tb_work_order').DataTable().clear().draw();
			}
		});
	}
	function save_barcode() {
		alertify.confirm('บันทึกข้อมูล','กดปุ่ม "OK" เพื่อยืนยัน', function(){
			if ($('#is_status_update').is(':checked')) {
				var is_status_update = 1;
			}else{
				var is_status_update = 0;
			}
			var scan_round_id = $('#scan_round_id').val();
			var barcose_scan = $('#barcose_scan').val();
			var date_send = $('#date_send').val();
			$('#barcose_scan').val('');

			$.ajax({
				url: "ajax/ajax_load_data_mailroom_send_round.php",
				type: "post",
				dataType:'json',
				data: {
					'scan_round_id'			: scan_round_id,
					'barcose_scan'			: barcose_scan,
					'date_send'				: date_send,
					'is_status_update'		: is_status_update,
					'page'					: 'save',
				},
			beforeSend: function( xhr ) {
					$('#bg_loader').show();
				}
			}).done(function( msg ) {
				setTimeout(function(){ $('.alert ').hide(); }, 3000);
				$('#bg_loader').hide();
				load_data_Byround_resive();
				if(msg['status']==200){
					$('#success').html(msg['msg']);
					$('#success').show();
				}else{
					$('#error').html(msg['msg']);
					$('#error').show();
				}
			});
		}, function(){ alertify.error('Cancel')});
	}
	function keyEvent_subbarcode( evt )
		{
			if(window.event)
				var key = evt.keyCode;
			else if(evt.which)
				var key = evt.which;
				
			if( key == 13 ){
				save_barcode();
			}
		}
		function remove_mr_round_resive_work(id){
			console.log(id);
			$.ajax({
				url: "ajax/ajax_load_data_mailroom_send_round.php",
				type: "post",
				dataType:'json',
				data: {
					'id'				: id,
					'page'				: 'remove',
				},
			   beforeSend: function( xhr ) {
					$('#bg_loader').show();
				}
			}).done(function( msg ) {
				setTimeout(function(){ $('.alert ').hide(); }, 3000);

				if(msg['status']==200){
					$('#success').html(msg['msg']);
					$('#success').show();
				}else{
					$('#error').html(msg['msg']);
					$('#error').show();
				}
				$('#bg_loader').hide();
				load_data_Byround_resive();
			});
		}

	function saveround() {
		var dataall = [];
		var tbl_data = $('#tb_work_order').DataTable();
		 tbl_data.$('input[type="checkbox"]:checked').each(function(){
			 //console.log(this.value);
			 dataall.push(this.value);
		  });
		  //console.log(tbl_data);
		  if(dataall.length < 1){
			 alertify.alert("alert","ท่านยังไม่เลือกงาน"); 
			 return;
		  }
		var newdataall = dataall.join(",");
		$.ajax({
			url: "ajax/ajax_saveround.php",
			type: "post",
			dataType:'json',
			data: {
				'main_id': newdataall
			},
		   beforeSend: function( xhr ) {
			$('#bg_loader').show();	
			}
		}).done(function( msg ) {
			if(msg.status == 200){
				$('#bg_loader').hide();
				load_data();
			}else{
				alertify.alert("แจ้งเตือน",msg.message); 
			}
			
		});
	}
	
	function print_all() {
		var dataall = [];
		var tbl_data = $('#tb_work_order').DataTable();
		tbl_data.$('input[type="checkbox"]:checked').each(function(){
			dataall.push(this.value);
		});

		if(dataall.length < 1){
			alertify.alert("alert","ท่านยังไม่เลือกงาน"); 
			return;
		}
		var newdataall = dataall.join(",");
		$('#data1').val(newdataall);
		$('#print_1').submit();

	}
	
	function print_byHub() {
		var dataall = [];
		var tbl_data = $('#tb_work_order').DataTable();
		tbl_data.$('input[type="checkbox"]:checked').each(function(){
			 //console.log(this.value);
			dataall.push(this.value);
		});

		if(dataall.length < 1){
			alertify.alert("alert","ท่านยังไม่เลือกงาน"); 
			return;
		}
		var newdataall = dataall.join(",");
		$('#data2').val(newdataall);
		$('#print_2').submit();
	}	
	function changHub(){

		var hub_id = $('#hub_id').val();
		$.ajax({
			url: "ajax/ajax_load_branch_Byhub.php",
			type: "post",
			dataType:'json',
			data: {
				'hub_id': hub_id
			},
		beforeSend: function( xhr ) {
			//$('#bg_loader').show();
		}
		}).done(function( msg ) {
			if(msg.status == 200){
				$('#mr_branch_id').html(msg.data);
				load_data();
			}else{
				alertify.alert("แจ้งเตือน",msg.message); 
			}
		});
	}

	function load_data(){
		var mr_branch_id = $('#mr_branch_id').val();
		var hub_id = $('#hub_id').val();
		var data_all = $("#data_all").prop("checked") ? '1' : '0';

		$.ajax({
			url: "ajax/ajax_load_data_send_branch.php",
			type: "post",
			dataType:'json',
			data: {
				'mr_branch_id'	: mr_branch_id,
				'data_all'		: data_all,
				'hub_id'		: hub_id
			},
		   beforeSend: function( xhr ) {
				$('#bg_loader').show();
			}
		}).done(function( msg ) {
			$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
			$('#bg_loader').hide();
			$('#tb_work_order').DataTable().clear().draw();
			$('#tb_work_order').DataTable().rows.add(msg.data).draw();
		});
	}
	
	function click_load_data(){
		var date = $('#date').val();
		var mr_round_id = $('#mr_round_id').val();
		// var data_all = $("#data_all").prop("checked") ? '1' : '0';

		$.ajax({
			url: "ajax/ajax_load_data_send_branch.php",
			type: "post",
			dataType:'json',
			data: {
				'date'	: date,
				'mr_round_id'		: mr_round_id
			},
		   beforeSend: function( xhr ) {
				$('#bg_loader').show();
			}
		}).done(function( msg ) {
			$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
			$('#bg_loader').hide();
			$('#tb_work_order').DataTable().clear().draw();
			$('#tb_work_order').DataTable().rows.add(msg.data).draw();
		});
	}

	function new_load() {
		var date 				= $('#date').val();
		var mr_branch_id 		= $('#mr_branch_id').val();
		var hub_id 				= $('#hub_id').val();
		var mr_round_id 		= ( JSON.stringify($('#mr_round_id').select2('val')) );

		//var mr_round_id = $('#mr_round_id').val();
		// var data_all = $("#data_all").prop("checked") ? '1' : '0';

		$.ajax({
			url: "ajax/ajax_load_data_print_branch.php",
			type: "post",
			dataType:'json',
			data: {
				'date'				: date,
				'hub_id'			: hub_id,
				'mr_branch_id'		: mr_branch_id,
				'mr_round_id'		: mr_round_id
			},
		   beforeSend: function( xhr ) {
				$('#bg_loader').show();
			}
		}).done(function( msg ) {
			$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
			$('#bg_loader').hide();
			$('#tb_work_order').DataTable().clear().draw();
			$('#tb_work_order').DataTable().rows.add(msg.data).draw();
		});
	}


	function print_option(type){
			var dataall = [];
			var tbl_data = $('#tb_work_order').DataTable();
			tbl_data.$('input[type="checkbox"]:checked').each(function(){
				 //console.log(this.value);
				dataall.push(this.value);
			});
	
			if(dataall.length < 1){
				alertify.alert("alert","ท่านยังไม่เลือกงาน"); 
				return;
			}

			var round = $('#mr_round_id').select2('data')
			//console.log($('#mr_round_id').val());
            if($('#mr_round_id').val()){
				$('#round_name').val(round[0].text);
			}

			var newdataall = dataall.join(",");
			$('#data_option').val(newdataall);
			
			if(type==0){
				$("#print_option").attr('action', 'print_sort_branch_all.php');
			}else if(type==1){
				$("#print_option").attr('action', 'print_sort_resive.php');
			}else if(type==2){
				$("#print_option").attr('action', 'print_peper_all_data.php');
			}else if(type==3){
				$("#print_option").attr('action', 'print_hub_all.php');
			}

			$('#print_option').submit();
		
	}
{% endblock %}

{% block Content2 %}
<form id="print_1" action="print_sort_branch_all.php" method="post" target="_blank">
  <input type="hidden" id="data1" name="data1">
</form>
<form id="print_2" action="print_hab_all.php" method="post" target="_blank">
  <input type="hidden" id="data2" name="data2">
</form>
		<div class="row" border="1">
			<div class="col-12">
				<div class="card">
					<h4 class="card-header">พิมพ์ใบคุมเอกสาร</h4>
					<div class="card-body">
						<form id="print_option" action="print_option.php" method="post" target="_blank">
						<input type="hidden" value="" id="round_name" name="round_name">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
							  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Scan Barcode</a>
							</li>
							<li class="nav-item">
							  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">ค้นหาใบคุม</a>
							</li>
						  </ul>
						  <div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
							<br>
								<div class="row">
									<div class="col-md-4">	
									<label class="" for="">วันที่ส่งออก : </label>
									<br>
									  <input type="text" id="date_send" name="date_send" class="form-control" autocomplete="off" value="{{today}}">
									  
									</div>
								</div>	
								
								  <div class="row">
									<div class="col-md-4">	
										<br>
										<label class="" for="">รอบ :  </label>
										<br>
										<select class="form-control" id="scan_round_id" onchange="load_data_Byround_resive();">
											{% for r in round %}
											<option value="{{r.mr_round_id}}">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
											{% endfor %}
										</select>
									</div>
								</div>

								<div class="row">
									<div class="col-md-4">	
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">	
										<div class="form-group">
											<label class="" for="">Scan Barcode :  </label>
											<input onkeyup="keyEvent_subbarcode(event);" type="text" class="form-control" id="barcose_scan" placeholder="Scan Barcode">
											
											<div class="custom-control custom-switch switch-certificate">
												<input type="checkbox" class="custom-control-input" id="is_status_update" name="is_status_update" value="1">
												<label class="custom-control-label" for="is_status_update">อัปดทสถานะงานหลังจากยิงบาร์โค้ด</label>
											</div>
											
										</div>
									</div>
								</div>


							</div>
							<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
								<br>
								<br>
								<br>
								<input type="hidden" id="data_option" name="data_option">
								<div class="row">
									<div class="col-md-4">	
									<label class="" for="">วันที่ : </label>
									<br>
									  <input type="text" id="date" name="date" class="form-control" autocomplete="off" value="{{today}}">
									  
									</div>
								</div>	
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleFormControlSelect1">ศูนย์</label>
											<select{# onchange="changHub();"#} name="hub_id" class="form-control" id="hub_id" style="width: 100%;">
											<option value="">ทั้งหมด</option>
											<option value="all_hub">ทุก Hub</option>
											<option value="all_branch">สาขาต่างจังหวัดทั้งหมด</option>
											{% for b in hub %}
												<option value="{{b.mr_hub_id}}">{{b.mr_hub_name}}</option>
											{% endfor %}
											</select>
										</div>									
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="exampleFormControlSelect1">สาขา</label>
											<select {#  onchange="load_data();" #} name="mr_branch_id" class="form-control" id="mr_branch_id" style="width: 100%;">
											<option value="">ทุกสาขา</option>
											{% for b in branch %}
												<option value="{{b.mr_branch_id}}">{{b.mr_branch_code}} : {{b.mr_branch_name}}</option>
											{% endfor %}
											</select>
										</div>									
									</div>
									
								</div>
								
								<div class="row">
									<div class="col-md-4">	
										<label class="" for="">รอบการยิงรับ :  </label>
										<br>
										<select class="form-control" id="mr_round_id" multiple="multiple"  name="states[]">
											{% for r in round %}
											<option value="{{r.mr_round_id}}">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
											{% endfor %}
										</select>
									</div>
								</div>	
								{#
								<div class="row">
									<div class="col-md-4">
											 <label class="custom-control custom-checkbox">
												<input onclick="load_data();"id="data_all" name="data_all" type="checkbox" class="custom-control-input" value="1">
												<span class="custom-control-indicator"></span>
												<span class="custom-control-description">แสดงงานที่สั่งวันนี้เท่านั้น</span>
											</label>								
										</div>
								</div>#}
								<div class="row">
									<div class="col-md-12">	
									<hr>
									<button onclick="new_load();" type="button" class="btn btn-info">ค้นหา</button>
									</div>
								</div>

								<div class="row justify-content-center">
									<div class="col-sm-12 text-center">
										<hr>
										
										<button onclick="print_option(3);"type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบคุมใหญ่</button>
										<button onclick="print_option(0);" type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบคุมย่อย</button>
										<button onclick="print_option(1);" type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบนำส่ง(แยกตามผู้รับ)</button>
										<button onclick="print_option(2);" type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบนำส่ง(รวมรายการ)</button>
		
										{#
										<button onclick="saveround();"type="button" class="btn btn-outline-primary" id="" onclick="">บันทึกข้อมูลรอบ</button> 
										#}
									</div>
								</div>


							</div>
						  </div>




							
						  
						<div class="collapse" id="collapseExample">
						  <div class="card card-body">
								
							
							<div class="row">
								<div class="col-md-12">	
								<br>
								<hr>
								{#

								<label class="" for="">พิมพ์ใบคุมแยกตาม : </label>
								<br>
								  <input type="radio" id="" name="customRadio" class="">  หน่วยงาน/ชั้น
								  <br>
								  <input type="radio" id="" name="customRadio" class="">  ชื่อผู้รับ
								  
								  #}
								</div>
								
							
							</div>
							


								
						  
						  </div>
						</div>
						<br>
						
						<hr>
						
						<div class="table-responsive">
							<div class="alert alert-success" role="alert" id="success" style="display: none;">
								....
							  </div>
							  <div class="alert alert-danger" role="alert" id="error"  style="display: none;">
								''''
							  </div>
						  <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
							<thead>
							  <tr>
								<th>#</th>
								<th>Action</th>
								<th>Date/Time</th>
								<th>Status</th>
								<th>Bacode</th>
								<th>Sender</th>
								<th>Receiver</th>
								<th>Type Work</th>
								<th>Branch</th>
								<th>SendRound</th>
								<th class="text-center">
									<label class="custom-control custom-checkbox">
										<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">เลือกทั้งหมด</span>
									</label>
								</th>
							  </tr>
							</thead>
							<tbody>
							</tbody>
						  </table>
						</div>
						
						<div class="row justify-content-center" style="margin-top:20px;">
								<div class="col-sm-12 text-center">
									
								</div>
							</div>
						<hr>
					</form>
					</div>
				</div>
			</div>
		</div>
	<br>
		 
	
	
	
 <div id="bg_loader" class="card">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

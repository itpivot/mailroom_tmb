{% extends "base_emp2.tpl" %}
{% block title %}{% parent %} - List{% endblock %}
{% block menu_6 %} active {% endblock %}

{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}


{% block styleReady %}
#btn_save:hover{
	color: #FFFFFF;
	background-color: #055d97;

}

#btn_save{
	border-color: #0074c0;
	color: #FFFFFF;
	background-color: #0074c0;
}

#detail_sender_head h4{
	text-align:left;
	color: #006cb7;
	border-bottom: 3px solid #006cb7;
	display: inline;
}

#detail_sender_head {
	border-bottom: 3px solid #eee;
	margin-bottom: 20px;
	margin-top: 20px;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:absolute;
		top:0px;
		left:0px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}
{% endblock %}


{% block domReady %}
{{alertt}}
$('#barcode').keydown(function (e){
    if(e.keyCode == 13){
        save_click();
    }
})
var tbl_data = $('#tableData').DataTable({ 
    "responsive": true,
	//"searching": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
	"pageLength": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'btn'},
        {'data': 'send_work_no'},
        {'data': 'send_name'},
        {'data': 'mr_status_name'},
        {'data': 'tnt_tracking_no'},
        {'data': 'res_name'},
        {'data': 'cre_date'},
		{'data': 'update_date'},
        {'data': 'action'}
    ]
});


$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});
loaddata();
{% endblock %}
{% block javaScript %}
function print_click() {
	var dataall = [];
	
	var tbl_data = $('#tableData').DataTable();
	 tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		 dataall.push(this.value);
		 
	  });
	  //console.log(tel_receiver);
	  if(dataall.length < 1){
		 alertify.alert("ตรวจสอบข้อมูล","ท่านยังไม่เลือกงาน"); 
		 return;
	  }
	 // return;
	var newdataall = dataall.join(",");
	alertify.confirm("โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง "+(dataall.length)+"  รายการ ", function() {
			//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
			//return;
			$.ajax({
				dataType: "json",
				method: "POST",
				url: "ajax/ajax_save_receive_multiple.php",
				data: { 
						maim_id: newdataall
				  },
				 beforeSend: function( xhr ) {
					$('#bg_loader').show();
							  
				},
				error: function(xhr, error){
					alert('เกิดข้อผิดพลาด !');
					location.reload();
			   	}
			}).done(function( msg ) {
				if(msg.status == 200){
					location.href = "rate_send.php?id="+newdataall;
				}else{
					alertify.alert(msg.title,msg.message); 
				}
				$('#bg_loader').hide();
			});

	}).setHeader('<h5> ยืนยันการรับเอกสาร  </h5> ');
}	



function click_resave(barcode) {
	//console.log(bc);
	var dataall = [];
	var tel_receiver = $('#tel_receiver').val();
	//var barcode = $('#barcode').val();
	var tbl_data = $('#tableData').DataTable();
	// tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		// dataall.push(this.value);
		 
    //  });
	  //console.log(tel_receiver);
	  if(barcode == '' ){
		 alertify.alert("ข้อมูลไม่ครบถ้วน","ท่านยังไม่เลือกงาน"); 
		 return;
	  }
	 // return;
	//var newdataall = dataall.join(",");
	//console.log(newdataall);
	//console.log(tel_receiver);
	$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_save_receive.php",
	  data: { 
			barcode: barcode, 
			tel: tel_receiver 
		},
	   beforeSend: function( xhr ) {
			$('#bg_loader').show();
		},
		error: function(xhr, error){
			alert('เกิดข้อผิดพลาด !');
			location.reload();
	 }
	}).done(function( msg ) {
		if(msg['st'] == 'success'){
			//loaddata();
			//alertify.alert(msg['st'],msg['msg']);
			location.href = "rate_send.php?id="+msg['mr_work_main_id'];
		}else{
			alertify.alert(msg['st'],msg['msg']); 
		}
		  $('#bg_loader').hide();
	  });
	
}
function save_click() {
	var dataall = [];
	var tel_receiver = $('#tel_receiver').val();
	var barcode = $('#barcode').val();
	var tbl_data = $('#tableData').DataTable();
	// tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		// dataall.push(this.value);
		 
    //  });
	  //console.log(tel_receiver);
	  if(barcode == '' ){
		 alertify.alert("ข้อมูลไม่ครบถ้วน","ท่านยังไม่เลือกงาน"); 
		 return;
	  }
	 // return;
	//var newdataall = dataall.join(",");
	//console.log(newdataall);
	//console.log(tel_receiver);
	$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_save_receive.php",
	  data: { 
				barcode: barcode, 
				tel: tel_receiver 
		},
	   beforeSend: function( xhr ) {
			$('#bg_loader').show();
					
		},
		error: function(xhr, error){
			alert('เกิดข้อผิดพลาด !');
			location.reload();
	 }
	}).done(function( msg ) {
		
		if(msg['st'] == 'success'){
			$('#barcode').val('');
			loaddata();

			$( "#succ" ).show();
		setTimeout(function(){ 
			$( "#succ" ).hide();
		}, 300);
			
			//lertify.alert(msg['st'],msg['msg']);
		}else{
			$( "#err" ).show();
			setTimeout(function(){ 
				$( "#err" ).hide();
			}, 3000);
		}
		  $('#bg_loader').hide();
	  });
}
function onseart() {
 var barcode = $('#barcode').val();
 var ch_b = $('#b_'+barcode).val();
 if(ch_b){
	$('#b_'+barcode).prop("checked", true);
		$( "#succ" ).show();
		setTimeout(function(){ 
			$( "#succ" ).hide();
		}, 3000);
 }else{
		$( "#err" ).show();
		setTimeout(function(){ 
			$( "#err" ).hide();
		}, 3000);
 }

}
function loaddata() {
	$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_load_receive_work.php",
	  data: { 	id: '',
				status: '' 
		},
	   beforeSend: function( xhr ) {
			$('#bg_loader').show();
		},
		error: function(xhr, error){
			alert('เกิดข้อผิดพลาด !');
			location.reload();
	 }
	}).done(function( msg ) {
		$('#bg_loader').hide();
		if(msg.status == 200){
			$('#tableData').DataTable().clear().draw();
			$('#tableData').DataTable().rows.add(msg.data).draw();
		}else{
			alertify.alert('ผิดพลาด',msg.message,function(){window.location.reload();});
		}
	});
}
{% endblock %}	
{% block Content2 %}

	
			<div class="container-fluid">
			<div class="" style="text-align: center;color:#0074c0;margin-top:20px;">
				 <label><h3><b>รายการเอกสาร</b></h3></label>
			</div>	
			<div class="form-group" style="" id="detail_sender_head">
				 <H4> รับเอกสาร </H4>
			</div>
			<div class="form-row align-items-center">
				<div class="col-auto">
				  <input type="text" class="form-control mb-2" id="barcode" placeholder="Barcode">
				</div>
				<div class="col-auto">
				{#
				  <button onclick="save_click();" type="button" class="btn btn-primary mb-2">ค้นหา</button>
				  #}<button onclick="save_click();" type="button" class="btn btn-primary mb-2">บันทึกรับ</button>
				  
				</div>
				<div id="err" class="col-md-12 alert alert-danger" style="display:none;"role="alert">
				 ไม่พบข้อมูลงานที่กำลังนำส่ง
				</div>
				<div id="succ"class="col-md-12 alert alert-success " style="display:none;" role="alert">
				 ok!!
				</div>
			 </div>

			<div class="table-responsive">
				<table class="table table-bordered table-hover display responsive no-wrap" id="tableData">
					<thead class="">
						<tr>
						<th>ลำดับ</th>
						<th>Action</th>
						<th>เลขที่เอกสาร</th>
						<th>ชื่อผู้ส่ง</th>
						<th>สถานะงาน</th>
						<th>TNT</th>
						<th>ชื่อผู้รับ</th>
						<th>วันที่สร้างรายการ</th>
						<th>วันที่แก้ไขล่าสุด</th>
						<th>
						<div class="btn-group mr-2" role="group" aria-label="First group">
							<button onclick="$('#select-all').click();" type="button" class="btn btn-secondary">
							<input id="select-all" type="checkbox">
							</button>
							<button type="button" class="btn btn-primary" onclick="print_click();">รับเอกสารที่เลือก</button>
						</div>					
						</th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
			</div>	
			
			
		
			
			
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
					
					</div>	
					
					
					<div class="col-4" style="padding-right:0px">
					</div>	
						
				</div>			
				
			</div>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
		</div>
		
		<div id="bg_loader" class="card">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>
		<div id="bog_link">
		</div>		
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

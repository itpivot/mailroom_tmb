{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
    body {
        height:100%;
    }
    .table-hover tbody tr.hilight:hover {
        background-color: #555;
        color: #fff;
    }

    table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
    }

    table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

    #tb_work_order {
    	font-size: 13px;
    }

	.panel {
		margin-bottom : 5px;
	}
	.loading {
		position: fixed;
		top: 40%;
		left: 50%;
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
		z-index: 1000;
	}
	
	.img_loading {
		width: 350px;
		height: auto;
	}

{% endblock %}
{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">

<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}
$('[data-toggle="tooltip"]').tooltip()
		$('.input-daterange').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		});	
		
		var table = $('#tb_work_order').DataTable({ 
			"scrollY": "800px",
			"scrollCollapse": true,
			"responsive": true,
			//"searching": false,
			"language": {
				"emptyTable": "ไม่มีข้อมูล!"
			},
			"pageLength": 10, 
			'columns': [
				{ 'data':'no' },
						{ 'data':'cancle' },
						{ 'data':'ch_data'},
						{ 'data':'time_re'},
						{ 'data':'mr_work_barcode' },
						{ 'data':'name_receive' },
						{ 'data':'con_log' },
						{ 'data':'depart_floor_receive' },
						{ 'data':'name_send' },
						{ 'data':'depart_floor_send' },
						{ 'data':'mr_status_name' },
						{ 'data':'mr_type_work_name' },
						{ 'data':'mr_work_remark' },
						{ 'data':'mr_topic' },
						{ 'data':'con_name' }
			]
		});
		$("#btn_search").click(function(){
			getData();
		});	
		
		$("#pass_emp_send").keyup(function(){
			var pass_emp = $("#pass_emp_send").val();
			$.ajax({
				url: "ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200 ){
						$("#name_sender").val(res.data.mr_emp_name);
						$("#pass_depart_send").val(res.data.mr_department_code);
						$("#floor_send").val(res.data.mr_department_floor);
						$("#depart_send").val(res.data.mr_department_name);
						$("#emp_id_send").val(res.data.mr_emp_id);
					}else{
						alertify.alert('ผิดพลาด',"บันทึกไม่สำเร็จ  "+res.message,function(){window.location.reload();});
					}
				}							
			});
		});
		
		
		$('#select-all').on('click', function(){
		   // Check/uncheck all checkboxes in the table
		   var rows = table.rows({ 'search': 'applied' }).nodes();
		   $('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
		
		
		
		$("#pass_emp_re").keyup(function(){
			var pass_emp = $("#pass_emp_re").val();
			$.ajax({
				url: "ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200 ){
						$("#name_sender").val(res.data.mr_emp_name);
						$("#pass_depart_send").val(res.data.mr_department_code);
						$("#floor_send").val(res.data.mr_department_floor);
						$("#depart_send").val(res.data.mr_department_name);
						$("#emp_id_send").val(res.data.mr_emp_id);
					}else{
						alertify.alert('ผิดพลาด',"บันทึกไม่สำเร็จ  "+res.message,function(){window.location.reload();});
					}
				}							
			});				
		});
		
		
		$("#btn_excel").click(function() {
			var data = new Object();
			data['barcode'] 			= $("#barcode").val();
			data['sender'] 				= $("#pass_emp_send").val();
			data['receiver'] 			= $("#pass_emp_re").val();
			data['start_date'] 			= $("#start_date").val();
			data['end_date'] 			= $("#end_date").val();
			data['status'] 				= $("#status").val();
			var param = JSON.stringify(data);
		
			location.href='excel.report.php?params='+param;
			// window.open('excel.report.php?params='+param);
		
		});
		
		
		
		$("#btn_clear").click(function() {
			location.reload();
			
		});
		
		
		
		$('#name_receiver_select').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select_search.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setForm(e.params.data);
			
		});

		function setForm(data) {
			var emp_id = parseInt(data.id);
			var fullname = data.text;
			$.ajax({
				url: './ajax/ajax_autocompress_name.php',
				type: 'POST',
				data: {
					name_receiver_select: emp_id
				},
				dataType: 'json',
				success: function(res) {
					if(res.status == 200){
						$("#pass_depart_re").val(res.data.mr_department_code);
						$("#floor_re").val(res.data.mr_department_floor);
						$("#depart_re").val(res.data.mr_department_name);
						$("#pass_emp_re").val(res.data.mr_emp_code);
					}else{
						alertify.alert('ผิดพลาด',"บันทึกไม่สำเร็จ  "+res.message,function(){window.location.reload();});
					}
				}
			})
		}
		
		$('#name_send_select').select2({
				placeholder: "ค้นหาผู้ส่ง",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select_search.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setFormSend(e.params.data);
		});

		function setFormSend(data) {
			var emp_id = parseInt(data.id);
			var fullname = data.text;
			$.ajax({
				url: './ajax/ajax_autocompress_name.php',
				type: 'POST',
				data: {
					name_receiver_select: emp_id
				},
				dataType: 'json',
				success: function(res) {
					
					if(res.status == 200){
						$("#pass_depart_send").val(res.data.mr_department_code);
						$("#floor_send").val(res.data.mr_department_floor);
						$("#depart_send").val(res.data.mr_department_name);
						$("#pass_emp_send").val(res.data.mr_emp_code);
					}else{
						alertify.alert('ผิดพลาด',"บันทึกไม่สำเร็จ  "+res.message,function(){window.location.reload();});
					}
				}
			})
		}

{% endblock %}


{% block javaScript %}

function successAll_click() {
				var dataall = [];
	

				var tel_receiver = $('#tel_receiver').val();
				var tbl_data = $('#tb_work_order').DataTable();
				 tbl_data.$('input[type="checkbox"]:checked').each(function(){
					 //console.log(this.value);
					//  dataall.push(this.value);
				
					var token = encodeURIComponent(window.btoa(this.value));
					
					dataall.push(token);
				  });
				  //console.log(tel_receiver);
				  if(dataall.length < 1){
					 alertify.alert("ตรวจสอบข้อมูล","ท่านยังไม่เลือกงาน"); 
					 return;
				  }

				
				 // return;
				var newdataall = dataall.join(",");
				$('#wo_status_id').val('5');
				$('#work_all_id').val(newdataall);
				if($('#work_all_id').val()!= ''){
					$('#form_success_work').submit();
				}
				//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
			}
function cancleAll_click() {
				var dataall = [];
				var tel_receiver = $('#tel_receiver').val();
				var tbl_data = $('#tb_work_order').DataTable();
				 tbl_data.$('input[type="checkbox"]:checked').each(function(){
					 //console.log(this.value);
					//  dataall.push(this.value);
				
					var token = encodeURIComponent(window.btoa(this.value));
					
					dataall.push(token);
				  });
				  //console.log(tel_receiver);
				  if(dataall.length < 1){
					 alertify.alert("ตรวจสอบข้อมูล","ท่านยังไม่เลือกงาน"); 
					 return;
				  }

				
				 // return;
				var newdataall = dataall.join(",");
				$('#work_all_id').val(newdataall);
				$('#wo_status_id').val('6');
				if($('#work_all_id').val()!= ''){
					$('#form_success_work').submit();
				}
				//window.open('../branch/printcoverpage.php?maim_id='+newdataall+'');
			}
   function getData(){
			var barcode 						= $("#barcode").val();
			var pass_emp_send 					= $("#pass_emp_send").val();
			var pass_emp_re 					= $("#pass_emp_re").val();
			var start_date 						= $("#start_date").val();
			var end_date 						= $("#end_date").val();
			var status 							= $("#status").val();
			var select_mr_contact_id 			= $("#select_mr_contact_id").val();

			$.ajax({
			method: "POST",
			dataType: "json",
			url: "ajax/ajax_search_work_mailroom.php",
			data: {
				'barcode': barcode,
				'receiver': pass_emp_re,
				'sender': pass_emp_send,
				'start_date': start_date,
				'end_date': end_date,
				'select_mr_contact_id': select_mr_contact_id,
				'status': status
				},
			beforeSend: function( xhr ) {
				$('.loading').show();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกินไป!');
				//alertify.alert("กิดข้อผิดผลาด !");
				//location.reload();

			}
		})
		.done(function( msg ) {
			if(msg.status == 200){
				$('.loading').hide();
				$('#tb_work_order').DataTable().clear().draw();
				$('#tb_work_order').DataTable().rows.add(msg.data.data).draw(); 
			}else{
				alert(msg.message);
				location.reload();
			}
		});
	};

{% endblock %}

{% block Content2 %}

		<div class="row" border="1">
			<div class="col">
				
			</div>
			<div class="col-10">
				<div class="card">
					<h4 class="card-header">ค้นหา</h4>
					<div class="card-body">
						<form id="form_success_work" action="success_work_all_mailroom.php" method="post">
							<input id="wo_status_id" name="wo_status_id" type="hidden" value="">
							<input id="work_all_id" name="work_all_id" type="hidden" value="">
						</form>
						<form>
							<div class="row justify-content-between">
								<div class="card col-5" style="padding:0px;margin-left:20px">
									<h5 style="padding:5px 10px;">ผู้ส่ง</h5>
									<div class="card-body">
										<form>
											<div class="row ">
											
													<div class="col-12">
													<select class="form-control-lg" id="name_send_select" style="width:100%;" >
														{# < option value = "0" > ค้นหาผู้ส่ง < /option>
														{% for e in emp_data %}
															<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
														{% endfor %} #}
													</select>
													<input type="hidden" id="pass_emp_send">
												</div>
												
											
											
											
											
											
											
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_send" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_sender" placeholder="ชื่อผู้ส่ง">
												</div> -->
											</div>
											<br>

											<div class="row ">
												<div class="col-8">
														<input type="text" class="form-control mb-5 mb-sm-2" id="pass_depart_send" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" disabled>
												</div>
												
												<div class="col-4">
														<input type="text" class="form-control mb-5 mb-sm-0" id="floor_send" placeholder="ชั้น" disabled>
												</div>
											</div>
											
											<div class="row ">
												<div class="col">
														<input type="text" class="form-control mb-5 mb-sm-2" id="depart_send" placeholder="ชื่อหน่วยงาน" disabled>
												</div>
												
											</div>
											
											
											
											
											
											
												
										</form>
									</div>
								</div>
								
								<div class="card col-5" style="padding:0px;margin-right:20px">
									<h5 style="padding:5px 10px;">ผู้รับ</h5>
									<div class="card-body">
										<form>
											<div class="row ">
											
														
											
												<div class="col-12">
													<select class="form-control-lg" id="name_receiver_select" style="width:100%;" >
														{# < option value = "0" > ค้นหาผู้รับ< /option>
														{% for e in emp_data %}
															<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
														{% endfor %} #}
													</select>
													<input type="hidden" id="pass_emp_re">
												</div>		
											
											
											
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_re" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_receiver" placeholder="ชื่อผู้รับ">
												</div> -->
											</div>
											
											<br>
											<div class="row ">
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_depart_re" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" disabled>
												</div>
												
												<div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-0" id="floor_re" placeholder="ชั้น" disabled>
												</div>
											</div>
											
											<div class="row ">
												<div class="col">
													<input type="text" class="form-control mb-5 mb-sm-2" id="depart_re" placeholder="ชื่อหน่วยงาน" disabled>
												</div>
											</div>
											
											
											
											<div class="row ">
												<div class="col">
														<select class="form-control-lg" id="select_mr_contact_id" style="width:100%;">
															 <option value = "" selected> รายชื่อติดต่อ </option>
															{% for con in contactdata %}
																<option value="{{ con.mr_contact_id }}" > {{ con.department_name }} ** {{ con.remark }} : {{ con.QuantityText }} **</option>
															{% endfor %}
														</select>
												</div>
												
											</div>
											
												
										</form>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-top:40px;">
								<div class="col-1">
								</div>
								<div class="col-10">
									<div class="input-daterange input-group" id="datepicker" data-date-format="yyyy-mm-dd">
										<input  autocomplete="off" type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From date"/>
										<label class="input-group-addon" style="border:0px;">to</label>
										<input autocomplete="off" type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="To date"/>
									</div>
								</div>
								
							</div>
							
							<div class="row" style="margin-top:10px;">
								<div class="col-1">
								</div>
								<div class="col-10">
									<div class="row">
										<div class="col-6">
											<input type="text" class="form-control mb-5 mb-sm-2" id="barcode" placeholder="Barcode">
										</div>
										<div class="col-6">
											<select class="form-control-lg" id="status" style="width:100%;">
												<option value="">สถานะงานทั้งหมด</option>
												<optgroup label="รับส่งภายไนสำนักงานใหญ่">
												{% for s in status_data %}
													<option value="{{ s.mr_status_id }}">{{ s.mr_status_name }}</option>
												{% endfor %}
												</optgroup>
												<optgroup label="รับส่งที่สาขา">
												{% for s in status_data2 %}
													<option value="{{ s.mr_status_id }}">{{ s.mr_status_name }}</option>
												{% endfor %}							
													<option value="Pending">เอกสารคงค้าง</option>
												</optgroup>
												
											</select>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-top:10px;">
								<div class="col-md-12 text-center">
							
									<button type="button" class="btn btn-outline-primary" id="btn_search">
									<i class="material-icons">
									find_in_page
									</i>
									ค้นหา
									</button>
									<button type="button" class="btn btn-outline-dark" id="btn_excel">
									<i class="material-icons">
										play_for_work
									</i>
										Export Excel
									</button>
									<button type="button" class="btn btn-outline-warning" id="btn_clear">
									<i class="material-icons">
										undo
									</i>
										Clear</button>
							
								</div>
							</div>
							<div class="row" style="margin-top:10px;">
								<div class="col-3">
								</div>
								<div class="col-6">
									<div class="row">
										<div class="col-4">
											
										</div>
										<div class="col-4">
											
										</div>
										<div class="col-4">
											
										</div>
									</div>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	
		 <div class="panel panel-default" style="margin-top:50px;">
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>#</th><div class="btn-group" role="group" aria-label="Basic example">
                      <th width="250"><label class="custom-control custom-checkbox">
								<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description">ทั้งหมด</span>
							</label>
							
							<button type="button" onclick="successAll_click();" class="btn btn-outline-success btn-sm"
							data-toggle="tooltip" data-placement="top" title="ปิดงานสำเร็จเอกสารถึงผู้รับเรียบร้อยแล้ว"
							>
								<span class="material-icons">
									done
									</span>
								</button>
							<button type="button" onclick="cancleAll_click();" class="btn btn-outline-danger btn-sm"
							data-toggle="tooltip" data-placement="top" title="ยกเลิกรายการ"
							>
								<span class="material-icons">
									clear
									</span>
							</button>
							</div>
						</th>
                      <th>วันที่/เวลาสั่งงาน</th>
                      <th>Bacode</th>
                      <th>ผู้รับ</th>
                      <th>วันที่/เวลา</th>
                      <th>ชั้นของผู้รับ</th>
                      <th>ผู้ส่ง</th>
                      <th>ชั้นของผู้ส่ง</th>
                      <th>สถานะเอกสาร</th>
                      <th>type work</th>
                      <th>หมายเหตุ</th>
                      <th>ชื่อเอกสาร</th>
                      <th>contact</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
	
<div class='loading' style="display: none;">
                <img src="../themes/images/loading.gif" class='img_loading'>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

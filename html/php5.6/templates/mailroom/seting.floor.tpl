{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}	

var tbl_data = $('#tb_keyin').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'name'},
		{'data': 'g_mr_user_username'},
		{'data': 'sys_time'},
		{'data': 'action'}
    ]
});
var tbl_floor = $('#tb_floor').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_floor_mame'},
		{'data': 'mr_building_name'},
		{'data': 'mr_branch_name'},
		{'data': 'sys_time'},
		{'data': 'action'}
    ]
});
 $('#tb_keyin').on('click', 'tr', function () {
        var data = tbl_data.row( this ).data();
        //alert( 'You clicked on '+data['mr_round_name']+'\'s row' );
		//$('#round_name').val(data['mr_round_name']);
		//$('#type_work').val(data['mr_type_work_id']);
		//$('#mr_round_id').val(data['mr_round_id']);
    } );


$('#myTab a').on('click', function (e) {
	e.preventDefault()
	var id = $(this).attr('id');
	if(id=="profile-tab"){
		load_data_emp_floor();
	}else{
		load_data_floor();
	}
	console.log(id);
  })

  $('#mr_floor_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
});
  $('#mr_user_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
});
  $('#mr_building_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
});
  $('#mr_branch_id').select2({ 
	theme: 'bootstrap4',
	width: '100%' 
}).on('select2:select', function (e) {
  set_option_building();
  var data = e.params.data;
	$('#mr_branch_name').val(data['text']);
});

load_data_floor();
{% endblock %}
{% block javaScript %}


function save_add_floor() {
	var data =  $('#myform_data_floor').serialize()+"&page=save_add_floor";
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_floor.php",
		data:data,
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		   //alert('error; ' + eval(error));

		  alertify.alert('error',eval(error)+'!');
		  $("#bg_loader").hide();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			set_option_building() 

		}else{
			 alertify.alert('เกิดข้อผิดพลาด',res['message']+'!');
		  	$("#bg_loader").hide();
		}
	  });
}
function set_option_building() {
	var mr_branch_id = $('#mr_branch_id').val();
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_floor.php",
		data:{
			page						:	'getBuilding',
			mr_branch_id				:	mr_branch_id
		},
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		}
	  })
	  .done(function( res ) {
		$('#mr_building_id').html('');
		var newOption = new Option('กรุณาระบุอาคาร', '', false, false);
		$('#mr_building_id').append(newOption).trigger('change');

		$("#bg_loader").hide();
		if(res['status'] == 200){
			if(res['data'].length  >= 1 && res['data'] != undefined){
				
				res['data'].forEach(async function(rating) {
				 //console.log(rating.mr_building_id);
				 	newOption = new Option(rating.mr_building_name, rating.mr_building_id, false, false);
					$('#mr_building_id').append(newOption).trigger('change');
				
				})
			}else{
					$('#mr_building_id').html('');
					newOption = new Option('ไม่มีข้อมูลตึก/อาคาร', 0, false, false);
					$('#mr_building_id').append(newOption).trigger('change');
			}
		}else{
 			alert('error; ' + res['message']);
		  	$("#bg_loader").hide();
		}
	  });
}

function load_data_emp_floor() {
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_floor.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();

		}
	  });
}

function load_data_floor() {
	var data =  $('#myform_data_floor').serialize()+"&page=loadFloor";
$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_floor.php",
		data:data,
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_floor').DataTable().clear().draw();
			$('#tb_floor').DataTable().rows.add(res.data).draw();

		}
	  });
}
function remove_zone(id) {
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_floor.php",
		data:{
			page			:	'remove',
			id				:	id
		},
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();
			$('#round_name').val('');
			$('#type_work').val('');
			$('#mr_round_id').val('');
			alertify.alert('ลบข้อมูลสำเร็จ',res['message']+'!');
		}else{
			alertify.alert('ตรวจสอบข้อมูล',res['message']+'!');
		}
	  });
}
function save_zone() {
var mr_floor_id = $('#mr_floor_id').val();
var mr_user_id = $('#mr_user_id').val();

	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_floor.php",
		data:{
			page			:	'save',
			mr_user_id		:	mr_user_id,
			mr_floor_id		:	mr_floor_id
		},
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();
			$('#round_name').val('');
			$('#type_work').val('');
			$('#mr_round_id').val('');
			alertify.alert('บันทึกสำเร็จ',res['message']+'!');
		}else{
			alertify.alert('ตรวจสอบข้อมูล',res['message']+'!');
		}
	  });
}


{% endblock %}
{% block Content2 %}

<div  class="container-fluid">




	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
		  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">ข้อมูลชั้น</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">พนักงานประจำชั้น</a>
		</li>
	  </ul>
	  <div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">


			<form id="myform_data_floor">
				<br>
				<div class="row">
					<div class="col">
						<div class="">
							<label><h3><b>ข้อมูลชั้น</b></h3></label><br>
							<label>ชั้น > จัดการชั้น</label>
					   </div>	
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-12">
										<hr>
										<h5 class="card-title">รายการชั้น</h5>
										<table class="table table-hover" id="tb_floor">
											<thead class="thead-light">
											  <tr>
												
												<th width="10%" scope="col" colspan="2">
													<div class="form-group">
														<label for="exampleInputEmail1">ชื่อชั้น</label>
														<input type="text" class="form-control" id="floor_name" name="floor_name"  placeholder="ชื่อชั้น 1A,1B,วงศ์สว่างชั้น 1,ตึกรัชดาชั้น 2-B">
													  </div>
												</th>
												<th width="10%" scope="col">
													<div class="form-group">
														<label for="exampleInputEmail1">ละดับชั้น</label>
														<input type="number" class="form-control" id="floor_level" name="floor_level" placeholder="1,2,3,4,5....">
													  </div>
												</th>
												<th width="10%" scope="col">													
													<div class="form-group">
													  <label for="mr_branch_id">สาขา</label>
													  <input type="hidden" value="" id="mr_branch_name" name="mr_branch_name">
													  <select class="form-control" id="mr_branch_id" name="mr_branch_id">
														  <option value="" disabled selected>สาขา</option>
															  {% for b in branchdata %}
															  <option value="{{b.mr_branch_id}}">{{b.mr_branch_code}} {{b.mr_branch_name}}</option>
															  {% endfor %}
													  </select>
											  </th>
											  <th width="10%" scope="col">													
													<div class="form-group">
													<label for="mr_building_id">ตึก/อาคาร</label>
													<select class="form-control" id="mr_building_id" name="mr_building_id">
														<option value="" disabled selected>ตึก</option>
															{#
															{% for f in floor_data %}
															<option value="{{f.mr_floor_id}}">{{f.name}}</option>
															{% endfor %}
															#}
													</select>
												</th>
												<th width="10%" scope="col">
												<div class="form-group">
												<button type="button" class="btn btn-info" onclick="load_data_floor()">
												<i class="material-icons" style="padding-right:5px;">search</i>
												</button>
												</div>
												</th>
											  </tr>
											  <tr>
												<th width="10%" colspan="6" scope="col"><button type="button" class="btn btn-success" onclick="save_add_floor()">บันทึก(เพิ่มข้อมูลสาขา)</button></th>
											</tr>
											  <tr>
												<th width="5%" scope="col">#</th>
												<th width="10%" scope="col">ชั้น</th>
												<th width="10%" scope="col">ตึก/อาคาร</th>
												<th width="10%" scope="col">สาขา/สถานที่</th>
												<th width="10%" scope="col">วันที่นำเข้า</th>
												<th width="10%" scope="col">จัดการ</th>
											  </tr>
											</thead>
											<tbody>
										
											</tbody>
										  </table>
			
			
									</div>
								</div>
								
			
							</div>
						  </div>
					
					</div>
				</div>
			</form>
			
		</div>
		<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
			<form id="myform_data_senderandresive">
				<br>
				<div class="row">
					<div class="col">
						<div class="">
							<label><h3><b>ชั้นที่รับผิดชอบ ของพนักงาน</b></h3></label><br>
							<label>ชั้น > จัดการชั้น</label>
					   </div>	
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-12">
										<hr>
										<h5 class="card-title">รายการชั้น</h5>
										<table class="table table-hover" id="tb_keyin">
											<thead class="thead-light">
											  <tr>
												<th width="10%" scope="col" colspan="2">
													<select class="form-control type_work" id="mr_floor_id">
													  <option value="" disabled selected>ชั้น</option>
													  {% for f in floor_data %}
													  <option value="{{f.mr_floor_id}}">{{f.name}}</option>
													  {% endfor %}
													</select>
												</th>
												<th width="10%" scope="col">
													<select class="form-control type_work" id="mr_user_id">
													  <option value="" disabled selected>พนักงาน</option>
													  {% for emp in messenger %}
													  {% if emp.mr_emp_code != "Resign" %}
													  <option value="{{emp.mr_user_id}}">{{ emp.mr_emp_code }} :{{ emp.mr_emp_name }} {{ emp.mr_emp_lastname }}</option>
													  {% endif %}
													  {% endfor %}
													</select>
												</th>
												<th width="10%" scope="col"><button type="button" class="btn btn-success" onclick="save_zone()">เพิม</button></th>
												<th width="10%" scope="col"></th>
											  </tr>
											  <tr>
												<th width="5%" scope="col">#</th>
												<th width="10%" scope="col">ชั้น</th>
												<th width="10%" scope="col">พนักงาน</th>
												<th width="10%" scope="col">วันที่ Update</th>
												<th width="10%" scope="col">จัดการ</th>
											  </tr>
											</thead>
											<tbody>
										
											</tbody>
										  </table>
			
			
									</div>
								</div>
								
			
							</div>
						  </div>
					
					</div>
				</div>
			</form>
		</div>
	  </div>




</div>







<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

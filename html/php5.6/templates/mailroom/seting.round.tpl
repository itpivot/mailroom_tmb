{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

.type_work, ::after, ::before {
     box-sizing: revert; 
}

{% endblock %}

{% block domReady %}	

var tbl_data = $('#tb_keyin').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'no'},
        {'data': 'mr_round_name'},
		{'data': 'mr_type_work_name'},
		{'data': 'sys_date'}
    ]
});
 $('#tb_keyin').on('click', 'tr', function () {
        var data = tbl_data.row( this ).data();
        //alert( 'You clicked on '+data['mr_round_name']+'\'s row' );
		$('#round_name').val(data['mr_round_name']);
		$('#type_work').val(data['mr_type_work_id']);
		$('#mr_round_id').val(data['mr_round_id']);
    } );

load_data_bydate();
function load_data_bydate() {
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_round.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();

		}
	  });
}



{% endblock %}
{% block javaScript %}


function save_round() {
var type_work = $('#type_work').val();
var round_name = $('#round_name').val();
var mr_round_id = $('#mr_round_id').val();

	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_round.php",
		data:{
			page			:	'save',
			type_work		:	type_work,
			round_name		:	round_name,
			mr_round_id		:	mr_round_id
		},
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();
			$('#round_name').val('');
			$('#type_work').val('');
			$('#mr_round_id').val('');
		}
	  });
}


{% endblock %}
{% block Content2 %}

<div  class="container-fluid">
<form id="myform_data_senderandresive">
	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>รอบ รับ-ส่ง เอกสาร</b></h3></label><br>
				<label>การสั่งงาน > รับส่งเอกสาร BY HAND</label>
		   </div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<hr>
							<h5 class="card-title">รายการรอบ รับ-ส่ง เอกสารทั้งหมด</h5>
							<table class="table table-hover" id="tb_keyin">
								<thead class="thead-light">
								  <tr>
									<th width="10%" scope="col" colspan="2">
										<input id="round_name" class="form-control" type="text" placeholder="ชื่อรอบ">
										<input id="mr_round_id" type="hidden">
									</th>
									<th width="10%" scope="col">
										<select class="form-control type_work" id="type_work">
										  <option value="" disabled selected>ประเภท</option>
										  {% for type in type_work %}
										  <option value="{{type.mr_type_work_id}}">{{type.mr_type_work_name}}</option>
										  {% endfor %}
										</select>
									</th>
									<th width="10%" scope="col"><button type="button" class="btn btn-success" onclick="save_round()">บันทึก</button></th>
								  </tr>
								  <tr>
									<th width="5%" scope="col">#</th>
									<th width="10%" scope="col">รอบ</th>
									<th width="10%" scope="col">ประเภท</th>
									<th width="10%" scope="col">วันที่ Update</th>
								  </tr>
								</thead>
								<tbody>
							
								</tbody>
							  </table>


						</div>
					</div>
					

				</div>
			  </div>
		
		</div>
	</div>
</form>
</div>







<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
		{{alert}}
		
		$("#btn_save").click(function(){
		
			var remark_can 						= $("#remark_can").val();
			var remark 							= $("#remark").val();
			var mr_work_main_id 				= $("#mr_work_main_id").val();

				if( remark_can != "" ){
				//console.log(floor);
					$.post(
						'./ajax/ajax_success_by_mailroom.php',
						{
							mr_work_main_id 		 : mr_work_main_id,
							remark_can 				 : remark_can,
							remark 					 : remark,
						},
						function(res) {
							if(res.status == 200){
								location.href='search.php';
							}
							alert(res.message);
							
						},'json');
						
				}else{
					alert("โปรดกรอกหมายเหตุที่สำเร็จการส่งเอกสาร");
				}
		});
		
		





		
{% endblock %}				
{% block javaScript %}
				
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
			<div class="form-group" style="text-align: center;">
				 <label><h4> ข้อมูลผู้รับเอกสาร </h4></label>
			</div>	
			 <input type="hidden" id="mr_work_main_id" value="{{ user_data.mr_work_main_id }}">
	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน" value="{{ user_data.emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name" placeholder="ชื่อ" value="{{ user_data.name_re }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="last_name" placeholder="นามสกุล" value="{{ user_data.lastname_re }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel" placeholder="เบอร์โทรศัพท์" value="{{ user_data.tel_re }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel_mobile" placeholder="เบอร์มือถือ" value="{{ user_data.mobile_re }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						Email :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="email" placeholder="email" value="{{ user_data.mr_emp_email }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						แผนก :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="department" style="width:100%;" disabled>
							{% for s in department_data %}
								<option value="{{ s.mr_department_id }}" {% if s.mr_department_id == user_data.mr_department_id %} selected="selected" {% endif %}>{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้น :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="floor" style="width:100%;" disabled>
							
							{% for f in floor_data %}
								<option value="{{ f.mr_floor_id }}" {% if f.mr_floor_id == user_data.mr_floor_id %} selected="selected" {% endif %}>{{ f.name }}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อเอกสาร :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="topic"  value="{{ user_data.mr_topic }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						หมายเหตุ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="remark" value="{{ user_data.mr_work_remark }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						หมายเหตุที่สำเร็จ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="remark_can" placeholder="หมายเหตุที่สำเร็จ" value="">
					</div>		
				</div>			
			</div>
			
			
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึกการสำเร็จการส่งเอกสาร</button>
				<br>
				
			</div>
			
			
	
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

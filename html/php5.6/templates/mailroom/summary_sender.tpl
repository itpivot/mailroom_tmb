{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
       width: 100%;
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
      }
    
      .input-daterange {
          width: 450px;
      }

      #tb_summary,
      #tb_summary th{
          text-align:center;
          font-weight: bold;
      }

    .loading {
          position: fixed;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          z-index: 1000;
      }


      .img_loading {
            width: 350px;
            height: auto;
            
      }
      
{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">
<link rel="stylesheet" href="../themes/datepicker/bootstrap-datepicker3.min.css">

<script type='text/javascript' src="../themes/datepicker/bootstrap-datepicker.min.js"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>
{% endblock %}

{% block domReady %}
	$('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
	});

    var start = '';
    var end = '';
    getSummary(start, end);

    $('#btn_search').on('click', function() {
        var start = $('#date_start').val();
        var end = $('#date_end').val();
        getSummary(start, end);
    }); 

    $('#btn_clear').on('click', function() {
        $('#date_start').val("");
        $('#date_end').val("");
        var start = '';
        var end = '';
         getSummary(start, end);
    });

{% endblock %}


{% block javaScript %}
    var getSummary = function(start, end) {
        $.ajax({
            url: './ajax/ajax_get_summary_sender.php',
            method: 'POST',
            data: { start_date: start, end_date: end },
            dataType: 'json',
            beforeSend: function() {
                 $('.loading').show();
                $('#show_data').hide();
            },
            success: function(res) {
                $('.loading').hide();
                if(res.status == 500){
                    alertify.alert('ผิดพลาด',res.message,function(){window.location.reload();});
                }else{
                    var str = '';
                    $('#show_data').show();
                    for(var i = 0; i < res.length; i++){
                        var newly;
                        var msg_receive;
                        var mailroom;
                        var msg_send;
                        var success;
                        var cancel;
                        var total;
                        if(res[i]['no'] === "") {
                            newly = parseInt(res[i]['newly']) === 0 ? "-" : res[i]['newly'];
                            msg_receive = parseInt(res[i]['msg_receive']) === 0 ? "-" : res[i]['msg_receive'];
                            mailroom = parseInt(res[i]['mailroom']) === 0 ? "-" : res[i]['mailroom'];
                            msg_send = parseInt(res[i]['msg_send']) === 0 ? "-" : res[i]['msg_send'];
                            success = parseInt(res[i]['success']) === 0 ? "-" : res[i]['success'];
                            cancel = parseInt(res[i]['cancel']) === 0 ? "-" : res[i]['cancel'];
                            total = parseInt(res[i]['total']) === 0 ? "-" : res[i]['total'];
                        } else {
                            newly = parseInt(res[i]['newly']) === 0 ? "-" : '<a href="#" onclick="getDetail('+"'"+res[i]['mr_floor_id']+"'"+', 1);">'+res[i]['newly']+'</a>';
                            msg_receive = parseInt(res[i]['msg_receive']) === 0 ? "-" : '<a href="#" onclick="getDetail('+"'"+res[i]['mr_floor_id']+"'"+', 2);">'+res[i]['msg_receive']+'</a>';
                            mailroom = parseInt(res[i]['mailroom']) === 0 ? "-" : '<a href="#" onclick="getDetail('+"'"+res[i]['mr_floor_id']+"'"+', 3);">'+res[i]['mailroom']+'</a>';
                            msg_send = parseInt(res[i]['msg_send']) === 0 ? "-" : '<a href="#" onclick="getDetail('+"'"+res[i]['mr_floor_id']+"'"+', 4);">'+res[i]['msg_send']+'</a>';
                            success = parseInt(res[i]['success']) === 0 ? "-" : '<a href="#" onclick="getDetail('+"'"+res[i]['mr_floor_id']+"'"+', 5);">'+res[i]['success']+'</a>';
                            cancel = parseInt(res[i]['cancel']) === 0 ? "-" : '<a href="#" onclick="getDetail('+"'"+res[i]['mr_floor_id']+"'"+', 6);">'+res[i]['cancel']+'</a>';
                            total = parseInt(res[i]['total']) === 0 ? "-" : '<a href="#" >'+res[i]['total']+'</a>';
                        }
                        str += "<tr>";
                            str += "<td>"+res[i]['no']+"</td>";
                            str += "<td>"+res[i]['mr_department_floor']+"</td>";
                            str += "<td>"+newly+"</td>";
                            str += "<td>"+msg_receive+"</td>";
                            str += "<td>"+mailroom+"</td>";
                            str += "<td>"+msg_send+"</td>";
                            str += "<td>"+success+"</td>";
                            str += "<td>"+cancel+"</td>";
                            str += "<td>"+total+"</td>";
                        str += "</tr>";
                    }
                    $('#show_data').html(str);
                }
            }
        });
    }

    var getDetail = function(floor, status) {
        var obj = {};
        var start_date = $('#date_start').val();
        var end_date = $('#date_end').val();
        const uri = 'detail_work_sender_inout.php?param=';
        obj.mr_department_floor = floor;
        obj.status_id = status;
        obj.start_date = start_date;
        obj.end_date = end_date;
        param = JSON.stringify(obj); // encode parameter base64
        //window.open(uri+param, '_blank', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1000,height=350');
        window.open(uri+param, '_blank');
        loca
    }
	
{% endblock %}

{% block Content2 %}
      <div class="card">
            <div class='card-header'>
               <b>ติดตามงาน</b>
            </div>
               
            <div class="card-body">
               <form class='form-inline'>
                   <label><b>ช่วงวันที่ : </b>&emsp;</label>
                   <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group input-daterange">
                            <input type="text" class="form-control" id='date_start' placeholder='วันที่เริ่มต้น'>
                            <div class="input-group-addon" style='background-color: #fff; border-color: #fff; padding: 0px 10px;'><b>ถึง</b></div>
                            <input type="text" class="form-control" id='date_end' placeholder='วันที่สิ้นสุด'>
                        </div>
                   </div>
                   <button type='button' class='btn btn-primary' id='btn_search'><b>ค้นหา</b></button>&nbsp;&nbsp;
                   <button type='button' class='btn btn-secondary' id='btn_clear'><b>เคลียร์</b></button>
               </form>
            </div>
      </div>

      <div class='card' style='margin-top: 20px;'>
        <div class='card-header'>
            <b>สถานะงาน</b>
        </div>        
        <div class='card-body'>
            <table id='tb_summary' class="table table-responsive table-bordered table-striped table-sm" cellspacing="0">
                <thead class="thead-inverse">
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชั้น</th>
                        <th>รอพนักงานเดินเอกสาร</th>
                        <th>พนักงานเดินเอกสารรับเอกสารแล้ว</th>
                        <th>เอกสารถึงห้อง Mailroom</th>
                        <th>พนักงานเดินเอกสารอยู่ในระหว่างนำส่ง</th>
                        <th>เอกสารถึงผู้รับเรียบร้อยแล้ว</th>
                        <th>ยกเลิกการจัดส่ง</th>
                        <th>รวม</th>
                    </tr>
                </thead>
                <tbody id='show_data'></tbody>
            </table>
           <div class='loading'>
                <img src="../themes/images/loading.gif" class='img_loading'>
            </div>
        </div>
      </div>
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

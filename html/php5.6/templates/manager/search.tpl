{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
	  }


{% endblock %}
{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">

<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}
		$('.input-daterange').datepicker({
			autoclose: true,
			clearBtn: true
		});	
		
		var table = $("#tb_work_order").DataTable();
		<!-- Search -->
			$("#btn_search").click(function(){
			var barcode 		= $("#barcode").val();
			var pass_emp_send 	= $("#pass_emp_send").val();
			var pass_emp_re 	= $("#pass_emp_re").val();
			var start_date 		= $("#start_date").val();
			var end_date 		= $("#end_date").val();
			var status 			= $("#status").val();
			
				table.destroy();
				table = $("#tb_work_order").DataTable({
					"ajax": {
						"url": "ajax/ajax_search_work_mailroom.php",
						"type": "POST",
						"data": {
						'barcode': barcode,
						'receiver': pass_emp_re,
						'sender': pass_emp_send,
						'start_date': start_date,
						'end_date': end_date,
						'status': status
						}
					},
				
					'columns': [
						{ 'data':'no' },
						{ 'data':'time_test'},
						{ 'data':'mr_work_barcode' },
						{ 'data':'name_receive' },
						{ 'data':'name_send' },
						{ 'data':'mr_type_work_name' },
						{ 'data':'mr_status_name' },
						{ 'data':'mr_work_remark' }
					],
					/* "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						if (aData['scg_status_name'] == "Newly") {
								$(nRow).addClass('hilight');
							}
						else{
							$(nRow).removeClass('hilight');
							}
						}, */
					'scrollCollapse': true
				});
			});
			<!-- end Search -->
		
		
		$("#btn_clear").click(function() {
			location.reload();
			
		});
		
		
		
		$('#name_receiver_select').select2({
				placeholder: "ค้นหาผู้ส่ง",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select_search.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setForm(e.params.data);
		});

		function setForm(data) {
				var emp_id = parseInt(data.id);
				var fullname = data.text;
				$.ajax({
					url: './ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						//console.log(res);
						$("#pass_depart_re").val(res['mr_department_code']);
						$("#floor_re").val(res['mr_department_floor']);
						//$("#floor_id").val(res['mr_floor_id']);
						$("#depart_re").val(res['mr_department_name']);
						//$("#emp_id").val(res['mr_emp_id']);
						$("#pass_emp_re").val(res['mr_emp_code']);
						
						
						
					}
				})
		}
		
		$('#name_send_select').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select_search.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setFormSend(e.params.data);
		});

		function setFormSend(data) {
				var emp_id = parseInt(data.id);
				var fullname = data.text;
				$.ajax({
					url: './ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						//console.log(res);
						$("#pass_depart_send").val(res['mr_department_code']);
						$("#floor_send").val(res['mr_department_floor']);
						//$("#floor_id").val(res['mr_floor_id']);
						$("#depart_send").val(res['mr_department_name']);
						//$("#emp_id").val(res['mr_emp_id']);
						$("#pass_emp_send").val(res['mr_emp_code']);
						
						
						
					}
				})
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		$("#pass_emp_send").keyup(function(){
			var pass_emp = $("#pass_emp_send").val();
			$.ajax({
				url: "ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
				console.log(res);
					$("#name_sender").val(res['mr_emp_name']);
					$("#pass_depart_send").val(res['mr_department_code']);
					$("#floor_send").val(res['mr_department_floor']);
					$("#depart_send").val(res['mr_department_name']);
					$("#emp_id_send").val(res['mr_emp_id']);
					
				}
												
			});
									
		});
		
		$("#pass_emp_re").keyup(function(){
			var pass_emp = $("#pass_emp_re").val();
			$.ajax({
				url: "ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
				console.log(res);
					$("#name_receiver").val(res['mr_emp_name']);
					$("#pass_depart_re").val(res['mr_department_code']);
					$("#floor_re").val(res['mr_department_floor']);
					$("#depart_re").val(res['mr_department_name']);
					$("#emp_id_re").val(res['mr_emp_id']);
					
				}
												
			});
									
		});
		
		
		$("#btn_excel").click(function() {
			var data = new Object();
			data['barcode'] 			= $("#barcode").val();
			data['sender'] 				= $("#pass_emp_send").val();
			data['receiver'] 			= $("#pass_emp_re").val();
			data['start_date'] 			= $("#start_date").val();
			data['end_date'] 			= $("#end_date").val();
			data['status'] 				= $("#status").val();
			var param = JSON.stringify(data);
		
			window.open('excel.report.php?params='+param);
		
		});
		
		
		
		
{% endblock %}


{% block javaScript %}
    /* function getData(){
			table.rows('.selected').remove().draw( false );
			$("#barcode").val("");
			table.destroy();
			var table = $("#tb_work_order").DataTable({
			"ajax": {
				"url": "ajax/ajax_load_work_mailroom.php",
				"type": "POST"
			},
			'columns': [
				{ 'data':'mr_work_main_id' },
				{ 'data':'time_test '},
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_re' },
				{ 'data':'lastname_re' },
				{ 'data':'mr_emp_name' },
				{ 'data':'mr_emp_lastname' }
			],
			'order': [[ 0, "desc" ]],
			'scrollCollapse': true
		});
			
	};
 */
{% endblock %}

{% block Content %}

		<div class="row" border="1">
			<div class="col">
				
			</div>
			<div class="col-10">
				<div class="card">
					<h4 class="card-header">ค้นหา</h4>
					<div class="card-body">
						<form>
							<div class="row justify-content-between">
								<div class="card col-5" style="padding:0px;margin-left:20px">
									<h5 style="padding:5px 10px;">ผู้ส่ง</h5>
									<div class="card-body">
										<form>
											<div class="row ">
												<div class="col-12">
													<select class="form-control-lg" id="name_receiver_select" style="width:100%;" >
														{# < option value = "0" > ค้นหาผู้รับ < /option>
														{% for e in emp_data %}
															<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
														{% endfor %} #}
													</select>
													<input type="hidden" id="pass_emp_re">
												</div>		
											
											
											
											
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_re" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_receiver" placeholder="ชื่อผู้รับ">
												</div> -->
											</div>
											<br>
											<div class="row ">
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_depart_re" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" disabled>
												</div>
												
												<div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-0" id="floor_re" placeholder="ชั้น" disabled>
												</div>
											</div>
											
											<div class="row ">
												<div class="col">
													<input type="text" class="form-control mb-5 mb-sm-2" id="depart_re" placeholder="ชื่อหน่วยงาน" disabled>
												</div>
											</div>
										</form>
									</div>
								</div>
								
								<div class="card col-5" style="padding:0px;margin-right:20px">
									<h5 style="padding:5px 10px;">ผู้รับ</h5>
									<div class="card-body">
										<form>
											<div class="row ">
												<div class="col-12">
													<select class="form-control-lg" id="name_send_select" style="width:100%;" >
														{# < option value = "0" > ค้นหาผู้ส่ง < /option>
														{% for e in emp_data %}
															<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
														{% endfor %} #}
													</select>
													<input type="hidden" id="pass_emp_send">
												</div>		
											
											
											
											
											
											
											
											
											
											
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_send" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_sender" placeholder="ชื่อผู้ส่ง">
												</div> -->
											</div>
											<br>
											<div class="row ">
												<div class="col-8">
														<input type="text" class="form-control mb-5 mb-sm-2" id="pass_depart_send" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" disabled>
												</div>
												
												<div class="col-4">
														<input type="text" class="form-control mb-5 mb-sm-0" id="floor_send" placeholder="ชั้น" disabled>
												</div>
											</div>
											
											<div class="row ">
												<div class="col">
														<input type="text" class="form-control mb-5 mb-sm-2" id="depart_send" placeholder="ชื่อหน่วยงาน" disabled>
												</div>
												
											</div>
			
											
												
										</form>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-top:40px;">
								<div class="col-1">
								</div>
								<div class="col-10">
									<div class="input-daterange input-group" id="datepicker" data-date-format="yyyy-mm-dd">
										<input type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From date"/>
										<label class="input-group-addon" style="border:0px;">to</label>
										<input type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="To date"/>
									</div>
								</div>
								
							</div>
							
							<div class="row" style="margin-top:10px;">
								<div class="col-1">
								</div>
								<div class="col-10">
									<div class="row">
										<div class="col-6">
											<input type="text" class="form-control mb-5 mb-sm-2" id="barcode" placeholder="Barcode">
										</div>
										<div class="col-6">
											<select class="form-control-lg" id="status" style="width:100%;">
												<option value="">สถานะงาน</option>
												{% for s in status_data %}
													<option value="{{ s.mr_status_id }}">{{ s.mr_status_name }}</option>
												{% endfor %}
												
											</select>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-top:10px;">
								<div class="col-3">
								</div>
								<div class="col-6">
									<div class="row">
										<div class="col-4">
											<button type="button" class="btn btn-outline-primary btn-block" id="btn_search">ค้นหา</button>
										</div>
										<div class="col-4">
											<button type="button" class="btn btn-outline-dark btn-block" id="btn_excel">Export Excel</button>
										</div>
										<div class="col-4">
											<button type="button" class="btn btn-outline-warning btn-block" id="btn_clear">Clear</button>
										</div>
									</div>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	
		 <div class="panel panel-default" style="margin-top:50px;">
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date/Time</th>
                            <th>Bacode</th>
                            <th>Receiver</th>
                            <th>Sender</th>
                            <th>Type Work</th>
                            <th>Status</th>
                            <th>Remark</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
	
	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

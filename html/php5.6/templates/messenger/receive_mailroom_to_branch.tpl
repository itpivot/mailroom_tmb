{% extends "base_emp2.tpl" %}

{% block title %}{% parent %} Send List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
	
	.fixed-bottom {
		position: sticky;
		bottom: 0;
		//top: 250px;
		z-index: 1075;

	}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
	#loading{
			display: block;
			margin-left: auto;
			margin-right: auto;
			width: 50%;
		
	}

{% endblock %}

{% block domReady %}
    $('.block_btn').hide();

    var receive_list = {};
    getReceiveBranch();

    $('#txt_search').keyup(function(e) {
        var txt = e.target.value;

        $.ajax({
            url: './ajax/ajax_getReceiveBranch.php',
            type: 'POST',
            cache: false,
            data: {
                type: 'get_mailroom_search',
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function(resp) {
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                // console.log(resp['counter'])
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function() {
                // loading hide
				$('#loading').hide();
            }
        })

    });

{% endblock %}

{% block javaScript %}
    var selectChoice = [];
    
    function branch_chang(b_name){
        var txt = b_name;

        $.ajax({
            url: './ajax/ajax_getReceiveBranch.php',
            type: 'POST',
            cache: false,
            data: {
                type: 'get_mailroom_search',
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function(resp) {
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                // console.log(resp['counter'])
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function() {
                // loading hide
				$('#loading').hide();
            }
        })
    }
    function getReceiveBranch()
    {
        $.ajax({
            url: "./ajax/ajax_getReceiveBranch.php",
            type: "GET",
            cache: false,
            data: {
                type: 'get_mailroom'
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function() {
                // loading hide
				$('#loading').hide();
            } 
        });
    }


    function updateStatus(wId, actions)
    {
        $.ajax({
            url: './ajax/ajax_getReceiveBranch.php',
            type: 'POST',
            cache: false,
            data: {
                wId: wId,
                action: actions,
                type: 'update_mailroom'
            },
            dataType: 'json',
            beforeSend: function () {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function (resp) {
                // result
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function () {
                // loading hide
				$('#loading').hide();
            } 
        });
    }

     function selectedCard(wId, elm) {
         var foundArr = selectChoice.indexOf(wId);
         if ($('#' + elm.id).is(':checked')) {
             if (foundArr == -1) {
                 selectChoice.push(wId)
             }
         } else {
             if (foundArr != -1) {
                 selectChoice.splice(foundArr, 1);
             }
         }


         if (selectChoice.length > 0) {
             $('.block_btn').fadeIn();
             $('#btn_checked').html('ยกเลิก');
         } else {
             $('.block_btn').fadeOut();
             $('#btn_checked').html('เลือกทั้งหมด');
         }
         console.log(selectChoice)
     }

     function checkChoice() {

         $('.check_all').each(function (i, elm) {
             var wId = parseInt($('#' + elm.id).attr('data-value'));
             var foundArr = selectChoice.indexOf(wId);

             $(elm).prop('checked', !elm.checked);
             if (elm.checked) {
                 if (foundArr == -1) {
                     selectChoice.push(wId)
                 } else {

                 }
             } else {
                 if (foundArr != -1) {
                     selectChoice.splice(foundArr, 1);
                 }
             }
         });

         if (selectChoice.length > 0) {
             $('.block_btn').fadeIn();
             $('#btn_checked').html('ยกเลิก');
         } else {
             $('.block_btn').fadeOut();
             $('#btn_checked').html('เลือกทั้งหมด');
         }

     }

     function updateStatusAll(actions) {
         $.ajax({
             url: './ajax/ajax_getReceiveBranch.php',
             type: 'POST',
             cache: false,
             data: {
                 wId: JSON.stringify(selectChoice),
                 action: actions,
                 type: 'update_all_mailroom'
             },
             dataType: 'json',
             beforeSend: function () {
                 // loading show
				$('#loading').show();
				$('.result_bch').html('');
             },
             success: function (resp) {
                 // result
                 $('.result_bch').empty();
                 $('p span#counter_works').text(resp['counter']);
                 $('.result_bch').html(resp['data']);

                 selectChoice.length = 0;
                 $('#btn_checked').html('เลือกทั้งหมด');
                 $('.block_btn').hide();
             },
             complete: function () {
                 // loading hide
				 $('#loading').hide();
             }
         });
     }

{% endblock %}

{% block Content %}
<div class="content_bch">
    <div class="header_bch">
        <div class="form-group">
            <p class="font-weight-bold text-muted">ทั้งหมด <span id="counter_works">0</span> งาน </p>
            <select name="lst_branch" id="lst_branch" class="form-control" onchange="$('#txt_search').val(this.value);branch_chang(this.value)">
                <option value="">-- เลือกสาขา --</option>
                {% for b in branch %}
                    <option value="{{ b.mr_branch_name }}">{{ b.mr_branch_name }}</option>
                {% endfor %}
            </select><br>
            <br>
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหาจากหมายเลขงาน, สาขา, และชื่อ-สกุลผู้ส่ง" />
        </div>
    </div>
    <button type="button" class="btn btn-secondary btn-block my-2" id="btn_checked" onclick="checkChoice();">เลือกทั้งหมด</button>
    <div class="block_btn my-2">
        <button type="button" id="btn_receive" class="btn btn-primary btn-block" onclick="updateStatusAll(1);">รับเอกสารทั้งหมด</button>
        <button type="button" id="btn_not_found" class="btn btn-warning btn-block" onclick="updateStatusAll(2);">ไม่พบเอกสาร</button>
    </div>
	<img id="loading" src="../themes/images/loading.gif">
    <div class="result_bch">
        
    </div>
</div>
<div class="footer_bch">
    
</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


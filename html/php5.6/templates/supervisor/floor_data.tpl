{% extends "base.tpl" %}

{% block title %}{% parent %} - Building Data{% endblock %}

{% block menu_sp1 %} active {% endblock %}
{% block scriptImport %}
	
		<script src="../themes/bootstrap/js/jquery-11.1.js"></script>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="css/chosen.css">
		<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<script src="js/chosen.jquery.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
	#content {
		margin-top: 10px
	}

	.fieldset {
		border: 1px solid #ccc;
		padding: 10px;
		padding-top : 0px;
	}
	td{
		padding: 3px;
	}
	input[type=text] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		text-align: center;
		width: 300px;
	}input[type=number] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		//background-color:yellow;
	}
	input:focus{ 
		background-color:#ffffbf;
	}
	input[type=number] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
	}
	textarea{
		border: 1px solid #ccc;
		border-radius: 4px;
	}
	.panel{
		background-color:#eee;
		padding:5px;
		margin-bottom: 5px;
	}
	legend{
		width:15%;
	}
	
	.setClass
	{
		margin-bottom: 5px;
	}

	.label_txt{
		width: 100px;
		text-align: center;
	}

	table.dataTable tbody tr {
				cursor: pointer;
	}

	table.dataTable tbody tr.selected {
		background-color: #555;
		color: #fff;
		cursor:pointer;
	}

	.table-hover tbody tr:hover > td {
        background-color: #555;
        color: #fff;
     }


{% endblock %}

{% block domReady %}
	var fsave = $('#btn_save');
	var fdelete = $('#btn_delete');
	var code = $("#txt_fcode");
	
	$('#btn_delete').hide();
	
	var tfloor = $('#tb_floor_show').DataTable({
		ajax: {
			url: "ajax/ajax_load_floor_data.php",
			type: "POST"
		},
		columns: [
			{ 'data': null,
				'defaultContent': "",
				'searchable': false,
				'orderable': false
			},
			{ "data": "scg_building_floor_id" },
			{ "data": "scg_building_office_id" },
			{ "data": "scg_building_floor_code" },
			{ "data": "scg_building_floor_name"}
		],
		order: [[ 1, "desc"]]
	});

	$('#tb_floor_show tbody').on("click","tr",function() {
			if ( $(this).hasClass('selected') ) {
				  $('#btn_delete').fadeOut();
    			  $(this).removeClass('selected');
    	  	}
      	  	else {
      	  		$('#btn_delete').fadeIn();
                tfloor.$('tr.selected').removeClass('selected');
          		$(this).addClass('selected');
        	}
	});
	
	$('#tb_floor_show tbody').on("click","tr",function() { 

		var rows = tfloor.row(this).data();
		var f_id = rows['scg_building_floor_id'];
		$.ajax({
			url: "ajax/ajax_get_floor_edit.php",
			type: "POST",
			data: {
				fid: f_id 
			},
			dataType: 'json',
			success: function(res){
				console.log(res);
				if(res.length > 0){
					$("#txt_fcode").val(res[0]['scg_building_floor_code']);
				    $("#txt_fname").val(res[0]['scg_building_floor_name']);
					$("#lst_building").val(parseInt(res[0]['scg_building_office_id'])).trigger("chosen:updated");
					$('#old_id').val(res[0]['scg_building_floor_id']);
				}
			}
		});
		
	});

		tfloor.on( 'order.dt search.dt', function () {
		tfloor.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			cell.innerHTML = i+1;
		} );
	} ).draw();

	$('#lst_building').chosen({width: "300px"}).change(function(){
		var office_id = parseInt($(this).val());
		tfloor.destroy();
		tfloor = $('#tb_floor_show').DataTable({
				ajax: {
					url: "ajax/ajax_load_floor_data_by_bid.php",
					type: "POST",
					data: {
						bid : office_id
					}
				},
				columns: [
					{ 'data': null,
						'defaultContent': "",
						'searchable': false,
						'orderable': false
					},
					{ "data": "scg_building_floor_id" },
					{ "data": "scg_building_office_id" },
					{ "data": "scg_building_floor_code" },
					{ "data": "scg_building_floor_name"}
				],
				order: [[ 1, "desc"]]	
		});
	});

	code.keyup(function(){
		<!-- console.log($(this).val()); -->
		var bid = $('#lst_building').val();
		var length_txt = $(this).val().length;
		if(bid == "" || bid == null){
			$('#alert_building').html('เลือก Building ก่อน').css('color','red');
		}else{
			$('#alert_building').fadeOut();
			if( length_txt < 2){
				$('#alert_fcode').html('ใช้ 2 ตัวอักษร').css('color','red');
			}else{
				$.ajax({
					url: "ajax/ajax_check_duplicate_code_floor.php",
					type: "POST",
					data: {
						fcode: $(this).val(),
						bid: bid
					},
					dataType: 'json',
					success: function(res){
						if(parseInt(res[0]['fcode']) > 0){
							$('#alert_fcode').html('code ซ้ำ').css('color','red');
						}else{
							$('#alert_fcode').html('สามารถใช้ code นี้ได้').css('color','green');
						}
					}
				});
			}
		}
		
		
	});



	fsave.click(function(){
		var chkForm = Validated();
		var code = $("#txt_fcode").val();
		var name = $("#txt_fname").val();
		var ftype = $("#lst_building").val();
		var oid = $('#old_id').val();
		if(chkForm == true){
			var oid = $('#old_id').val();
			if(oid != ""){
				$.ajax({
					url: "ajax/ajax_save_floor_data.php",
					type: "POST",
					data: {
						oid: oid,
						code: code,
						name: name,
						ftype: ftype
					},
					success: function(res){
						if(res == "Update Success"){
							ClearFrom();
							tfloor.ajax.reload();
						}
					}
				});
			}else{
				$.ajax({
					url: "ajax/ajax_save_floor_data.php",
					type: "POST",
					data: {
						oid: oid,
						code: code,
						name: name,
						ftype: ftype
					},
					success: function(res){
						if(res == "Save Success"){
							ClearFrom();
							tfloor.ajax.reload();
						}
					}
				});
			}
		}
	});

	fdelete.click(function() {
		var oid = $('#old_id').val();
		if(confirm("ท่านต้องการลบรายการนี้หรือไม่")){
			$.ajax({
					url: "ajax/ajax_delete_floor_data.php",
					type: "POST",
					data: {
							oid: oid
						},
					success: function(res){
							if(res == "Delete Success"){
								ClearFrom();
								tfloor.ajax.reload();
							}
						}
			});
		}
		
	});
{% endblock %}

{% block javaScript %}
	function Validated(){
		var code = $("#txt_fcode").val();
		var name = $("#txt_fname").val();
		var building = $("#lst_building").val();
		var status = true;
		
		if(code == "" || code == null){
			$('#alert_fcode').html('จำเป็นต้องกรอก').css('color','red');
			status = false;
		}else{
			$('#alert_fcode').html('')
			status = true;
		}

		if(name == "" || name == null){
			$('#alert_fname').html('จำเป็นต้องกรอก').css('color','red');
			status = false;
		}else{
			$('#alert_fname').html('')
			status = true;
		}

		if(building == "" || building == null){
			$('#alert_building').html('กรุณาเลือกข้อมูล').css('color','red');
			status = false;
		}else{
			$('#alert_building').html('')
			status = true;
		}

		return status;
	}

	function ClearFrom(){
		$('#btn_delete').fadeOut();
		$('#old_id').val("");
		$("#txt_fcode").val("");
		$("#txt_fname").val("");
		$("#lst_building").val(0).trigger("chosen:updated");
	}
{% endblock %}

{% block Content %}
	<div class="container" id="content">
		<div class="row">
			<form action="" class="form-horizontal" id="frm_building" >
				<div class="col-md-12">
					<fieldset class="fieldset panel">
						<legend><b>Floor</b></legend>
						 <div class="col-md-6 col-md-offset-3">
							<table id="tb_frm_floor" >
								<input type="hidden" id="old_id">
								<tr>
									<td class="label_txt"><b>Building<b></td>
									<td>
										<select name="lst_building" id="lst_building">
											 <option value="" disabled selected>Choose Building</option>
											 {% for b in building %}
												<option value="{{ b.scg_building_office_id }}">{{ b.scg_building_code }} : {{ b.scg_building_name }}</option>
											 {% endfor %}
										</select>
										<span id="alert_building"></span>
									</td>
									
								</tr>
								<tr>
									<td class="label_txt"><b>Code</b></td>
									<td>
										<input type="text" id="txt_fcode" name="txt_fcode" placeholder="Code" maxlength="2">
										<span id="alert_fcode"></span>
									</td>
								</tr>
								<tr>
									<td class="label_txt"><b>Name</b></td>
									<td>
										<input type="text" id="txt_fname" name="txt_fname" placeholder="Floor Name" >
										<span id="alert_fname"></span>
									</td>
								</tr>
								
								<tr>
									<td class="label_txt"></td>
									<td>
										<button type="button" id="btn_save" name="btn_save" class="btn btn-primary">Save</button>
										<button type="button" id="btn_delete" name="btn_delete" class="btn btn-danger">Delete</button>
										<button type="button" class="btn btn-default" onClick='window.location.reload();'>Clear</button>
									</td>
								</tr>
							</table>
						 </div>
					<fieldset>
				</div>
			</form>

			<div class="col-md-12">
				<fieldset class="fieldset panel">
					<table class="table table-striped table-bordered" id="tb_floor_show">
						<thead>
							<tr>
								<th>#</th>
								<th>ID</th>
								<th>Building Name</th>
								<th>Code</th>
								<th>NAME</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
{% endblock %}
{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


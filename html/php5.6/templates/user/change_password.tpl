{% extends "base_login.tpl" %}

{% block title %}{% parent %} - Change Password{% endblock %} 

{% block styleReady %}
input[type='text'],
input[type='password'] {
    border-radius: 0px;
}

.btn {
    border-radius: 0px;
}

{% endblock %}


{% block domReady %}
    $('#btn_save').on('click', function() {
        var username = $('#username').val();
        var old_password = $('#old_password').val();
        var new_password = $('#new_password').val();
        var confirm_password = $('#confirm_password').val();
        var pattern = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{10,}$/;
        var status = true;
        var data = {};
        
        if(old_password == "") {
            // alert("กรุณากรอกรหัสผ่านเดิมของท่าน");
            alertify.alert("แจ้งเตือน","กรุณากรอกรหัสผ่านเดิมของท่าน");
            $('#old_password').focus();
            status = false;
            return;
        }

        if(new_password == "") {
            // alert("กรุณาสร้างรหัสผ่านใหม่ของท่าน");
            alertify.alert("แจ้งเตือน","กรุณาสร้างรหัสผ่านใหม่ของท่าน");
            $('#new_password').focus();
            status = false;
            return;
        } else {
            var chkPass = pattern.test(new_password);
            if(new_password.length < 10) {
                // alert("รหัสผ่านต้องมีความยาวอย่างน้อย 10 หลัก");
                alertify.alert("แจ้งเตือน","รหัสผ่านต้องมีความยาวอย่างน้อย 10 หลัก");
                $('#new_password').focus();
                status = false;
                return;
            }
            if(!chkPass) {
                // alert("รูปแบบรหัสผ่านของท่านไม่ถูกต้อง");
                 alertify.alert("แจ้งเตือน","รูปแบบรหัสผ่านของท่านไม่ถูกต้อง");
                $('#new_password').focus();
                status = false;
                return;
            }

            
        }

        if(confirm_password == "") {
            // alert("กรุณายืนยันรหัสผ่าน");
             alertify.alert("แจ้งเตือน","กรุณายืนยันรหัสผ่าน");
            $('#confirm_password').focus();
            status = false;
            return;
        } 
        
        

        if(new_password != confirm_password) {
            // alert("'รหัสผ่านไม่ตรงกัน กรุณากรอกรหัสผ่านให้ตรงกัน");
            alertify.alert("แจ้งเตือน","รหัสผ่านไม่ตรงกัน กรุณากรอกรหัสผ่านให้ตรงกัน");
            $('#confirm_password').focus();
            status = false;
            return;
        }
        

        if(status) {
            data.username = username;
            data.old_password = old_password;
            data.new_password = new_password;
            $.ajax({
                url: "./ajax/ajax_change_password.php",
                method: 'POST',
                data: data,
                success: function(res) {
                    if(res == "success") {
                        alertify.alert("สำเร็จ","การเปลี่ยนรหัสผ่านสำเร็จ", function(){
                            location.href = 'logout.php';
                        });
                    } else if(res == "duplicate") {
                         alertify.alert("แจ้งเตือน","ท่านเคยใช้งานรหัสผ่านนี้มาแล้ว กรุณาอย่าตั้งซ้ำ");
                    } else {
                         alertify.alert("แจ้งเตือน","รหัสผ่านเดิมของท่านไม่ถูกต้อง");
                    }
                }
            })
        } 
    });
{% endblock %}

{% block body %}
        <div class='panel panel-default'>
            <div class='panel-body'>
                <h3>เปลี่ยนรหัสผ่าน</h3>
                <hr>
                <div class='row'>
                    <div class='col-md-6'>
                        <form id='frm_change_password' class='form-horizontal'>
                            <div class='form-group'>
                                <label class='col-sm-4 control-label'><b>username :</b></label>
                                <div class='col-sm-8'>
                                    <input type='text' class='form-control' readonly value='{{ username }}' id='username'>
                                    <input type='hidden' value='{{ userID }}'>
                                </div>
                            </div>
                            <hr>
                            <div class='form-group'>
                                <label class='col-sm-4 control-label'><b>Old password :</b></label>
                                <div class='col-sm-8'>
                                    <input type='password' class='form-control' id='old_password' value="{{atp}}">
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-sm-4 control-label'><b>Create new password :</b></label>
                                <div class='col-sm-8'>
                                    <input type='password' class='form-control' id='new_password' >
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class='col-sm-4 control-label'><b>Confirm password :</b></label>
                                <div class='col-sm-8'>
                                    <input type='password' class='form-control' id='confirm_password'>
                                </div>
                            </div>
                            <div class='form-group'>
                                <div class="col-sm-offset-4 col-sm-8">
                                    <a class='btn btn-danger' href="../user/logout.php"><b>ออกจากระบบ</b></a>
                                    <button type='button' class='btn btn-default' id='btn_save'><b>เปลี่ยนรหัสผ่าน</b></button>
                                </div>
                            </div>  
                        </form>
                    </div>
                    <div class='col-md-6'>
                       <div class="well">
                           <h4><u>คำเตือน</u></h4>
                           <ul>
                               <li>รหัสผ่านต้องมีทั้งตัวเลขและตัวหนังสือ</li>
                               <li>รหัสผ่านต้องมีทั้งตัวหนังสือพิมพ์เล็กและพิมพ์ใหญ่และตัวเลขอย่างน้อย 1 ตัว</li>
                               <li>มีอักขระพิเศษอย่างหน้อย 1 ตัว</li>
                               <li>ต้องมีความยาวอย่างน้อย 10 ตัว</li>
                               <li>รหัสผ่านของท่านต้องประกอบไปด้วย ตัวอักษรภาษาอังกฤษพิมพ์เล็ก ,พิมพ์ใหญ่, ตัวเลขและอักขระพิเศษประกอบด้วย ยกเว้น / (slash) \ (backslash)</li>
                           </ul>
                       </div>
                    </div>
                </div>
                
            </div>
        </div>
 

{% endblock %}
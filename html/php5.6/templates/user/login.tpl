{% extends "base_login.tpl" %}

{% block title %}{% parent %} - User Login{% endblock %} 

{% block styleReady %}
 #centered {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

 #img_logo {
   width: 100%;
   height: auto;
 }

 .btn, .form-control {
   border-radius: 0px;
   margin: 5px 0px;
 }

 .form-control {
   background-color: #f3f4f9;
   font-weight: bold;
   width: auto;
 }

 #btn-login{
   border-radius: 0px;
 }

#tste_id{
   color:red;
   font-size: 70px;
   position: absolute;
   top: -30px;
   left: -50px;
   -ms-transform: rotate(50deg); /* IE 9 */
  -webkit-transform: rotate(20deg); /* Safari 3-8 */
  transform: rotate(-30deg);
  
}

.txt-red{
  color:red;
  font-size: 30px;
}
@media screen and(max-width: 1080) {
 .form-control { 
    width: auto;
  }

}
{% endblock %}



{% block domReady %}

	$("#re_password").click(function(){
    
    // var password = prompt("กรุณากรอกรหัสพนักงานของท่าน","");

    // if (password != null || password != "") {
    //           $.ajax({
    //                 url: "./ajax/ajax_forget_password.php",
    //                 type: 'POST',
    //                 data: {
    //                       emp_code: password
    //                 },
    //                 success: function (res) {
    //                         if (res == "success") {
    //                           alert("กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ");
    //                         } else {
    //                           alert("รหัสพนักงานของท่านไม่ถูกต้องค่ะ");
    //                         }
    //                       }
    //                  });
    //   }

        alertify
              .prompt("ลืมรหัสผ่าน","กรุณากรอกรหัสพนักงานของท่าน","", 
                function(ev,val) {
					console.log(val);
                          //ev.preventDefault();
                        if(val != null || val != "") {
                                $.ajax({
                                      url: "./ajax/ajax_forget_password.php",
                                      type: 'POST',
                                      data: {
                                            emp_code: val
                                      },
                                      success: function(res) {
                                        if (res == "success") {
                                                // alert("กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ");
                                                // alertify.alert("กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ");
                                                alertify.alert('success',"กรุณาตรวจสอบรหัสผ่านใหม่ของท่านที่ email ของท่านค่ะ");
                                        } else {
                                                // alert("รหัสพนักงานของท่านไม่ถูกต้องค่ะ");
                                                // alertify.alert("รหัสพนักงานของท่านไม่ถูกต้องค่ะ");
                                                alertify.alert("เกิดข้อผิดพลาด","กรุณาติดต่อห้องสารบัญกลาง");
                                        }
                                      }
                                })
                        }
                      
                }, function() {
                       alertify.error('Cancel');
                });
		});
    

 
    
{% endblock %}

{% block javascript %}


function logout_All(id){
	alertify.confirm('ยืนยันการออกจากระบบ', 'รหัสพนักงานนี้ได้ทำการเข้าใช้งานระบบอยู่แล้ว  ท่านต้องการออกจากระบบ', 
		function(){ 
			//alertify.success('Ok:'+id) ;
			$.ajax({
			  method: "POST",
			  dataType: "json",
			  url: "ajax/ajax_logout.php",
			  data: { 'user_id': id}
			})
			  .done(function( msg ) {
				 // window.location.reload();
				 if(msg['st']=='success'){
					 alertify.alert('สำเร็จ', 'กรุณาเข้าสู่ระบบ',function(){ 
						window.location.href='login.php';
					 });
				 }else{
				   alertify.alert('เกิดข้อผิดพลาด', 'กรุณาลองใหม่อีกครั้ง!', function(){
					   window.location.reload();
					});
				//console.log(msg);
				 }
			
			
			
			  })
		},function(){ 
			alertify.error('Cancel'+id);
	 }).set('labels', {ok:'ออกจากระบบ!', cancel:'ยกเลิก'}); ;
}


{% endblock %}

{% block body %}
<div id='centered'>
  <!-- <div class='panel panel-default' id='login_tab'>
    <div class='panel-body'> -->
      <form class="form-signin" action="login.php" id="login" method="post" name="login" autocomplete="off">
        <center>
          <img src="../themes/images/logo-pv-ttb.png" alt="logo" id='img_logo'>
        </center>
        <br>
       

        <center>
         {#  <p class="txt-red">ทดสอบระบบ</p> #}
          <b>เข้าระบบสมาชิก</b>
		  <input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf}}">

          <label for="inputUsername" class="sr-only">รหัสพนักงาน</label>
          <input name="username" type="username" id="inputUsername" class="form-control" placeholder="รหัสพนักงาน"  autocomplete="off" required autofocus>
          <label for="inputPassword" class="sr-only">รหัสผ่าน</label>
          <input name="password" type="password" id="inputPassword" class="form-control" placeholder="รหัสผ่าน" required autocomplete="off"></center>
          <div class="checkbox">
            <label>
              <input type="checkbox" value="remember-me"> Remember me
            </label>
          </div>
        <label>
            <b style='color:red; width:auto;'>{{ error }}</b>
        </label>
        <input name="returnUrl" type="hidden" value="{{ returnUrl }}" />
        <input name="action" type="hidden" value="save" />
        <button class="btn btn-lg btn-primary btn-block btn-sm" type="submit" id='btn-login'><b>เข้าสู่ระบบ</b></button>
		<br>
		<center>
		 
		  <a href='register.php' ><span class='glyphicon glyphicon-user' ></span>&nbsp;ลงทะเบียน</a>&emsp; 
		  <a href='#' id="re_password"><span class='glyphicon glyphicon-repeat'></span>&nbsp;ลืมรหัสผ่าน กดที่นี่</a><br><br>
		  <a href='./Doc.pdf' target="_blank" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลดใบปะหน้าซอง</a><br><br>
		  <a href='./Doc2.pdf' target="_blank" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลดคู่มือการใช้งาน</a>
    </center>
		
		
		
      </form>
     
			

    </div>
  <!-- </div> -->

  
<!-- </div> -->
 

{% endblock %}
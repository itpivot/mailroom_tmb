<?php
require_once 'Pivot/Dao.php';
require_once 'Dao/UserRole.php';


class Dao_User extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_user');
    }
    
    function isExistsUser($username, $id = null)
    {
        $sql = 'SELECT count(*) FROM ' . $this->_tableName . ' WHERE mr_user_username = ?';
        
        if (is_numeric($id)) {
            $sql .= ' AND mr_user_id != ' . $this->_db->quote($id);
        }
        $n = $this->_db->fetchOne($sql, $username);
        return ($n > 0);
    }
    

    public function save($datas, $id = null)
    {
        if ($datas['password'] != '') {
           // $datas['password'] = md5($datas['password']);
        } else {
            unset($datas['password']);
        }
        return parent:: save($datas,$id);
    }
	
		
	
	function getempByuserid($user_id)
	{
	$sql =
		'
			SELECT 
				*
			FROM mr_user 
			WHERE mr_user_id = '.$user_id.'
			
		';
	
		//echo $sql;
		return $this->_db->fetchRow($sql);
	}
	
	function getEmpDataByuserid($user_id)
	{
	$sql =
		'
			SELECT 
				u.*,
				e.*,
				d.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			WHERE u.mr_user_id = '.$user_id.'
			
		';
	
		//echo $sql;
		return $this->_db->fetchRow($sql);
	}
	
	
	function getempByEmpid( $emp_code ) 
	{
	$sql =
		'
			SELECT  e.mr_emp_code,
					u.mr_user_password,
					e.mr_emp_name,
					e.mr_emp_lastname,
					u.mr_user_id,
					e.mr_emp_id
			FROM mr_user u 
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id ) 
			WHERE e.mr_emp_code LIKE "%'.$emp_code.'%"
			
			
		';
	
		//echo $sql;
		return $this->_db->fetchRow($sql);
	}
	
	function getMessSend()
	{
	$sql =
		'
			SELECT 
				u.*,
				e.*
			FROM mr_user u
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id ) 
			WHERE u.mr_user_role_id = 3
			AND u.mr_emp_id IS NOT NULL
			
		';
	
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	
	function getMessPrint( $user_id )
	{
	$sql =
		'
			SELECT 
				distinct (u.mr_user_id),
				e.*
			FROM mr_user u
			Left join mr_department d on ( d.mr_user_id = u.mr_user_id )
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			WHERE u.mr_user_id <> 0
			AND u.mr_user_role_id = 3 
					
		';
	
		if( $user_id != 0 ){
			$sql .= ' AND u.mr_user_id = "'.$user_id.'"';
		}	
	
	
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	function getUsersByEmpId($emp_id) 
	{
		$sql =
		'
			SELECT 
				u.*,
				u.mr_user_id as user_id,
				e.*,
				d.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( e.mr_department_id = e.mr_department_id )
			WHERE u.mr_emp_id = '.$emp_id.'
		';
		return $this->_db->fetchRow($sql);
	}
	
	function getEmpSignByUserID($user_id , $barcode) 
	{
		$sql =
		'
			SELECT 
				e.*,
				d.*,
				f.name as floor,
				m.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_work_inout i on ( i.mr_user_id = u.mr_user_id )
			Left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id )
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			WHERE u.mr_user_id = '.$user_id.' AND  m.mr_work_barcode = "'.$barcode.'"
		';
		//echo $sql;
		return $this->_db->fetchRow($sql);
	}



	//  ================================== MARCH ==========================

	function getUserIdFromUserName($user) 
	{
		$sql =
		'
			SELECT 
				*
			FROM mr_user as u
			WHERE u.mr_user_username = "'.$user['username'].'" AND u.mr_user_password = "'.$user['password'].'"
		';
		return $this->_db->fetchRow($sql);

	}

	function getDiffDatePassword($usr)
	{
		$nows = date('Y-m-d');
		$sql =
		'
			SELECT 
				DATEDIFF(NOW(), DATE_FORMAT(usr.change_password_date, "%Y-%m-%d")) as diffdate
			FROM mr_user as usr
			WHERE usr.mr_user_id = '.$usr.'
		';
		return $this->_db->fetchRow($sql);
	}
	
	function getEmpDataByuseridProfile($user_id)
	{
	$sql =
		'
			SELECT 
				u.*,
				e.*,
				d.*,
				f.mr_floor_id as floor_emp,
				f.mr_floor_id as mr_floor_id_emp,
				f.name
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( e.mr_floor_id = f.mr_floor_id )
			WHERE u.mr_user_id = '.$user_id.'
			
		';
	
		//echo $sql;
		return $this->_db->fetchRow($sql);
	}

	function getFailedByUser($username)
	{
		$sql =
		'
			SELECT 
				u.mr_user_id,
				u.isFailed,
				u.mr_user_username,
				u.mr_user_password
			FROM mr_user as u
			WHERE u.mr_user_username = "'.$username.'"
		';
		return $this->_db->fetchRow($sql);
		// return $sql;
	}
	
	function getEmpDataForEdit()
	{
	$sql =
		'
			SELECT 
				u.*,
				e.*,
				r.mr_user_role_name
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			left join mr_user_role r on ( r.mr_user_role_id = u.mr_user_role_id )
			WHERE u.mr_user_username != "delete"
		';
	
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	function updateStatus( )
	{
		//$sql = 'Update mr_user set isLogin = 0 WHERE isLogin = 1';
		$data['isLogin'] = 0 ;
		$this->_db->update($this->_tableName, $data,'isLogin = 1');
		$sql = ' SELECT mr_user FROM mr_user_id WHERE isLogin = 1 ';
		return $this->_db->fetchOne($sql);
	}
	

	// ================== NEW MARCH ==============================
	function getMessengers()
	{
		$sql =
		'
			select 
				usr.mr_user_id,
				emp.mr_emp_name, 
				emp.mr_emp_lastname,
				emp.mr_emp_code
			from mr_user as usr 
				left join mr_emp as emp on (emp.mr_emp_id = usr.mr_emp_id)
			where 
				usr.mr_user_role_id = 3
		';
		return $this->_db->fetchAll($sql);
	}
	
	
	function Update_delete_User( $code )
	{
		$data['sys_timestamp'] = date("Y-m-d H:i:s");
		$data['mr_user_username'] = 'delete' ;
		$data['active'] = 0 ;
		$this->_db->update($this->_tableName, $data, 'mr_user_username = "' .$code.'"');
		//$sql = ' SELECT mr_user FROM mr_user_id WHERE mr_user_username = "delete"';
		//return $this->_db->fetchOne($sql);
	}
	
	
	
}
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Log_change_password.php';
require_once 'PHPMailer.php';

$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao 	    = new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();
$sendmail           = new SendMail();
$log_change_pass_Dao = new Dao_Log_change_password();

$username = $req->get('username');
$old_password = $req->get('old_password');
$new_password = $req->get('new_password');

$usr = array();
$usrUpdate = array();
$usr['username'] = trim($username);
$usr['password'] = trim($auth->encryptAES($old_password));

$result = $users_Dao->getUserIdFromUserName($usr);


if(!$result) {
    echo "password not true";
} else {
    $usrUpdate['sys_timestamp'] = date('Y-m-d H:i:s');
    $usrUpdate['active'] = 1;
    $usrUpdate['is_first_login'] = 1;
    $usrUpdate['mr_user_password'] = $auth->encryptAES((string)$new_password); 
    $usrUpdate['change_password_date'] = date('Y-m-d H:i:s');

    $saveChange['sys_timestamp'] = date('Y-m-d H:i:s');
    $saveChange['password'] = $auth->encryptAES($new_password);
    $saveChange['mr_user_id'] = $result['mr_user_id'];
    
    $getUsrPassword = $log_change_pass_Dao->getPasswordByUser(intval($result['mr_user_id']));



    if(count($getUsrPassword) > 0) {  //นับจำนวนการเปลี่ยนรหัสผ่าน
       foreach ($getUsrPassword as $key => $value) {
           if( $value['password'] == $saveChange['password'] ) {
               $duplicate_password = $value['password'];
               break;
           } 
       }  
    
       if($duplicate_password == "") {
         $log_change_pass_Dao->save($saveChange);
         $result_update = $users_Dao->save($usrUpdate, intval($result['mr_user_id']));
       } else {
           echo "duplicate";
       }
    } else {
        // $log_change_pass_Dao->removeByUsrID(intval($result['mr_user_id']));
    
        $log_change_pass_Dao->save($saveChange);
        $result_update = $users_Dao->save($usrUpdate, intval($result['mr_user_id']));
    }
    
    if($result_update) {
        echo "success";
    }
}



?>
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'PHPMailer.php';

$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao 	    = new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();
$sendmail           = new SendMail();


$empCode = $req->get('emp_code');

$updateUser = array();
$usr = array();

function random_password( $length ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}

$resp = $employee_Dao->getEmp_code($empCode);

$user_data = $users_Dao->getempByEmpid($empCode);
//echo "<pre>".print_r( $resp , true )."</pre>";

// echo print_r($resp);

if($resp['mr_emp_id'] != "") {
    $result_search = $users_Dao->getempByEmpid_ch_Pass_2(intval($resp['mr_emp_id']));
    $usr['username'] = $resp['mr_emp_code'];
    $usr['password'] = 'PivotTTB@'.rand(10000,99999).'!';//random_password(10);
    //$usr['password'] = rand(1000,9999);

    $updateUser['mr_user_password'] = $auth->encryptAES((string)$usr['password']);
    $updateUser['sys_timestamp'] = date('Y-m-d H:i:s');
    $updateUser['isLogin'] 	= 0;
    $updateUser['isFailed'] = 0;
    $updateUser['active'] 	= 1;
    $updateUser['is_first_login'] = 0;

//echo '<1>'.print_r($resp,true);
//echo '<2>'.print_r($result_search,true);
//exit;
    $result = $users_Dao->save($updateUser, $result_search['mr_user_id'] );
	
	
	
    if($result != "") {
            $Subjectmail = "ขอแจ้งข้อมูลการเปลี่ยนรหัสผ่านใหม่";
            $body="";
            $body.="เรียน คุณ <b>".$resp['mr_emp_name']." ".$resp['mr_emp_lastname']."</b><br><br>";
            $body.= "ขอแจ้งข้อมูลการเปลี่ยนรหัสผ่านใหม่ของท่านค่ะ<br><br>";
            $body.= "password : "."<b>".$usr['password']."</b><br>";
            $body.="<br>";
            $body.="Pivot MailRoom Auto-Response Message <br>";
            $body.="(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";

            $result_mail = $sendmail->mailforget_password($body,$Subjectmail, $resp['mr_emp_email']);
            if($result_mail == 'success') {
                echo 'success';
            }
    }
} else {
    echo "false";

}
?>
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/User.php';


$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();
$userDao 			= new Dao_User();
$auth               = new Pivot_Auth();

$params             = array();

$user_id            = $auth->getUser();
$userName           = $auth->getUserName();

$user_permission 		= new Pivot_Dao('mr_permission_branch_menage');
$user_permission_db 	= $user_permission->getDb();

$sql= "SELECT * FROM mr_permission_branch_menage WHERE mr_user_username LIKE ? ";
// $user_data = $userDao->select($sql);

$stmt = new Zend_Db_Statement_Pdo($user_permission_db, $sql);

array_push($params, trim($userName));

$stmt->execute($params);
$user_data = $stmt->fetchAll();
//echo "<pre>".print_r($emp_data,true)."</pre>";
//exit();

echo json_encode($user_data);



//C:\xampp5_6/htdocs/mailroom_tmb_test/web/user/ajax/ajax_permission_branch_menage.php

?>
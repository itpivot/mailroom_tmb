<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';

$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao   	= new Dao_UserRole();
$users_Dao			= new Dao_User();
$employee_Dao       = new Dao_Employee();

$data = array();

$empCode = $req->get('empCode');
$password = $req->get('password');

$addUser = array();

$resp = $employee_Dao->getEmp_code($empCode);

$addUser['mr_emp_id'] = $resp['mr_emp_id'];
$addUser['mr_user_username'] = $empCode;
$addUser['mr_user_password'] = $auth->encryptAES((string)$password);
$addUser['active'] = 1;
$addUser['is_first_login'] = NULL;
$addUser['date_create'] = date('Y-m-d H:i:s');
$addUser['sys_timestamp'] = date('Y-m-d H:i:s');
$addUser['mr_user_role_id'] = 2;


$result_save = $users_Dao->save($addUser);
echo $result_save;



// 1 : ไม่มี Users
// 2 : มี Users แล้ว
// 3 : สำเร็จ

?>
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Dao/User.php';
require_once 'Pivot/Request.php';


$logedIn 		= array();

$auth           = new Pivot_Auth();
$user_Dao 		= new Dao_User();
$req = new Pivot_Request();

$auth->remove();
unset($_COOKIE);
header('Location: ../user/login.php');

?>

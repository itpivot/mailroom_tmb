<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);
$req 			= new Pivot_Request();
$employeeDao 	= new Dao_Employee();
$auth 			= new Pivot_Auth();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$pass_emp		=  $req->get('pass_emp');

if(preg_match('/<\/?[^>]+(>|$)/', $pass_emp)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$emp_data = $employeeDao->getEmp_code( $pass_emp );

//echo "<pre>".print_r($emp_data,true)."</pre>";
//exit();

echo json_encode($emp_data);
?>

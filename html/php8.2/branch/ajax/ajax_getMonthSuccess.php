<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';

require_once 'Dao/Work_main.php';
error_reporting(E_ALL & ~E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

$req            = new Pivot_Request();
$user_Dao       = new Dao_User();
$userRole_Dao	= new Dao_UserRole();
$work_main_Dao  = new Dao_Work_main();

$user_id        = $auth->getUser();

$view_month     = $_CONFIG->view->toArray();
$months		    = array_values($view_month['months']);

$years          = $req->get('year');

if(preg_match('/<\/?[^>]+(>|$)/', $years)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$month_data = $work_main_Dao->getMonthBranchSuccess($user_id, $years);
$monthSuccess = array();
$resp = array();

if(count($month_data) > 0) {
    foreach($month_data as $k => $v) {
        $monthSuccess[$v['month_success']]['id'] = $v['month_success'] ;
        $monthSuccess[$v['month_success']]['text'] = $months[$v['month_success'] - 1];
    } 

    $resp['data'] = array_values($monthSuccess);
} else {
    $resp['data'] = array();
}



echo json_encode($resp);


?>

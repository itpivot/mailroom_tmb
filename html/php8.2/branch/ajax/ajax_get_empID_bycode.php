<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_branch.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);
$req 			= new Pivot_Request();
$send_workDao	= new Dao_Send_work();
$auth 			= new Pivot_Auth();
$employee_dao 	= new Dao_Employee();
$_secret = $employee_dao->getEncryptKey();


if (!$auth->isAuth()) {
	
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	
	exit();
}

$emcode = $req->get('emp_code');

if(preg_match('/<\/?[^>]+(>|$)/', $emcode)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
}

$sql = "SELECT 
				e.mr_emp_id,
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_department_id,
				e.mr_position_id,
				e.mr_floor_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, '".$_secret->database->key."') as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, '".$_secret->database->key."') as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, '".$_secret->database->key."') as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_email, '".$_secret->database->key."') as mr_emp_email
		 FROM mr_emp  e
		 WHERE e.mr_emp_code LIKE ? ";
		 
if(!empty($emcode)){
	$params_emp_data = array();
	array_push($params_emp_data, (string)$emcode);
	$contactdata = $employee_dao->select_employee($sql,$params_emp_data);
}

$result['data'] = $contactdata;
// $result['params_emp_data'] = $params_emp_data;
// $result['sql'] = $sql;
echo json_encode($result);

 ?>

<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
ini_set('max_execution_time', 300);
error_reporting(E_ALL & ~E_NOTICE);


$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}

//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();
$user_id		= $auth->getUser();
//$user_data 		= $userDao->getEmpDataByuserid($user_id);
$data 			= $work_mainDao->usergetDataAll_receive($user_id);

$newdata = array();
$no=0;
foreach($data as $i => $val_i){
	$txt_disabled='disabled';
	switch($val_i['mr_status_id']) {
		case 3:
			// code block
			$txt_disabled='';
			break;
		case 4:
			// code block
			$txt_disabled='';
			break;
		case 11:
			// code block
			$txt_disabled='';
			break;
		case 10:
			// code block
			$txt_disabled='';
			break;
		  case 12:
			// code block
			$txt_disabled='';
			break;
		  case 13:
			// code block
			$txt_disabled='';
			break;
		  default:
			// code block
			//$txt_disabled='disabled';
			$txt_disabled='';
		}
		
	$newdata[$i]['no'] = ($no+1);
	$newdata[$i]['tnt_tracking_no'] 		= $val_i['tnt_tracking_no'];
	$newdata[$i]['send_work_no'] 		= '<a href="work_info.php?id='.urlencode(base64_encode($val_i['mr_work_main_id'])).'">'.$val_i['mr_work_barcode'].'</a>';
	$newdata[$i]['send_name'] 			= $val_i['send_name'].' '.$val_i['send_lname'];
	if($val_i['mr_status_id']==6){
		$newdata[$i]['mr_status_name'] 			= '<p class="text-danger">'.$val_i['mr_status_name'].'</p>';
	}elseif($val_i['mr_status_id']==11){
		$newdata[$i]['mr_status_name'] 			= '<p class="text-success">'.$val_i['mr_status_name'].'</p>';
	}else{
		$newdata[$i]['mr_status_name'] 			= '<p class="text-secondary">'.$val_i['mr_status_name'].'</p>';
	}
	$newdata[$i]['res_name'] 			= $val_i['re_name'].'  '.$val_i['re_lname'];
	$newdata[$i]['cre_date'] 			= $val_i['sys_timestamp'];
	$newdata[$i]['update_date'] 		= (isset($val_i['log_sys_timestamp']))?$val_i['log_sys_timestamp']:$val_i['sys_timestamp'];
	if($val_i['mr_status_id'] == 5 or $val_i['mr_status_id'] == 14 ){
		//$newdata[$i]['action'] 				= '<i class="text-success material-icons">check_circle_outline</i>';
		$newdata[$i]['action'] 				= '';
	}else{
		$newdata[$i]['action'] 				= '';
	}
	if($txt_disabled == ''){
				$newdata[$i]['btn'] = '<button type="button" '.$txt_disabled.'  class="btn btn-success" onclick="click_resave(\''.urlencode(base64_encode($val_i['mr_work_barcode'])).'\');">  รับ </button>';
	}else{
		$newdata[$i]['btn'] = '<button type="button"  class="btn btn-secondary" disabled>  รับ </button>';
	}
	$no++;
}

echo json_encode(array(
    'status' => 200,
	'data' => array_values($newdata),
	));
 ?>


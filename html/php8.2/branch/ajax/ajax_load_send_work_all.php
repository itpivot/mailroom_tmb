<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
error_reporting(E_ALL & ~E_NOTICE);


$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
    exit();
}
$time_start = microtime(true);
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();
$user_id		= $auth->getUser();
//$user_data 		= $userDao->getEmpDataByuserid($user_id);
$data 			= $work_mainDao->BranchgetDataAll_send($user_id);
$newdata = array();
$no=0;
foreach($data as $i => $val_i){
	$newdata[$i]['no'] = ($no+1);
	$newdata[$i]['send_work_no'] 		= $val_i['mr_work_barcode'];
	$newdata[$i]['mr_status_name'] 		= $val_i['mr_status_name'];
	$newdata[$i]['send_name'] 			= $val_i['send_name'].' '.$val_i['send_lname'];
	if($val_i['mr_type_work_id'] != 2){
		$newdata[$i]['res_depart'] 		= $val_i['re_mr_department_code'].'-'.$val_i['re_mr_department_name'];
	}else{
		$newdata[$i]['res_depart'] 		= $val_i['re_mr_branch_code'].'-'.$val_i['re_mr_branch_name'];
	}
	$newdata[$i]['res_name'] 			= $val_i['re_name'].'  '.$val_i['re_lname'];
	$newdata[$i]['cre_date'] 			= $val_i['sys_timestamp'];
	$newdata[$i]['update_date'] 		= isset($val_i['log_sys_timestamp'])?$val_i['log_sys_timestamp']:$val_i['sys_timestamp'];
	$newdata[$i]['action'] 				= '
		<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
			<input name="ch_bog[]" value="'.urlencode(base64_encode($val_i['mr_work_main_id'])).'" type="checkbox" class="custom-control-input">
			<span class="custom-control-indicator"></span>
		</label>
	';
	$no++;
}
$time_end = microtime(true);
$time = $time_end - $time_start;

echo json_encode(array(
    'status' => 200,
    'microtime' => $time,
    'data' => array_values($newdata)
));
 ?>

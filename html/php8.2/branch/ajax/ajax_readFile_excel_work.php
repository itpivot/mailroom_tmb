<?php 
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/History_import_emp.php';
require_once 'Dao/Cost.php';
require_once 'Dao/Branch.php';
require_once('PhpSpreadsheet-master/vendor/autoload.php');
//error_reporting(E_ALL & ~E_NOTICE);
ini_set('memory_limit', '-1');
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRole_Dao 			= new Dao_UserRole();
$departmentDao			= new Dao_Department();
$employeeDao 			= new Dao_Employee();
$floorDao 				= new Dao_Floor();
$historyDao 			= new Dao_History_import_emp();
$costDao 				= new Dao_Cost();
$branchDao 				= new Dao_Branch();
$reader 				= new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();


$_secret 	= $userDao->getEncryptKey();
$sql 		= "SELECT * FROM `mr_floor` ORDER BY `mr_floor`.`mr_floor_id` ASC";
$floor 		= $branchDao->selectdata($sql);
$stroption	= '';
foreach($floor as $f_key => $f_val){
	$stroption.='<option value="'.$f_val['mr_floor_id'].'">'.$f_val['name'].'</option>';
}
// var_dump(extension_loaded('zip'));
// exit;

$name 			= $_FILES['file']['name'];
$tmp_name 		= $_FILES['file']['tmp_name'];
$size 			= $_FILES['file']['size'];
$err 			= $_FILES['file']['error'];
$file_type 		= $_FILES['file']['type'];

$allowfile = array('xlsx','xls');
$surname_file = strtolower(pathinfo($name, PATHINFO_EXTENSION));

if(!in_array($surname_file,$allowfile)){
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$file_name = explode( ".", $name );
// exit;
$extension = $file_name[count($file_name) - 1]; // xlsx|xls


//$data = $sheet->toArray();

// output the data to the console, so you can see what there is.
try {
	$spreadsheet = $reader->load($tmp_name);
	$sheet = $spreadsheet->getSheet($spreadsheet->getFirstSheetIndex());
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($name,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//echo "ok--01 <br>";
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$ch_num = 0;
//  Loop through each row of the worksheet in turn
$emp_clode = array();
$str_tble = array();
$row_no=0;
$datatosave = array();
for ($row = 6; $row <= $highestRow; $row++){ 
 $error_type = 0; 
 $error_emp = 0; 
$stroption_ed='<option value="">ไม่พบข้อมูล</option>';
$stroption_emp='<option value="">ไม่พบข้อมูล</option>';
$strtel='ไม่พบข้อมูล';
$strdep='ไม่พบข้อมูล';

// exit;
    //  Read a row of data into an array
	
	
    $no 				= $sheet->getCell('A'.$row)->getValue();
    $w_type				= $sheet->getCell('B'.$row)->getValue();
    $emp_clode 			= $sheet->getCell('C'.$row)->getValue();
    $floor 				= $sheet->getCell('D'.$row)->getValue();
    $doc_title	 		= $sheet->getCell('E'.$row)->getValue();
    $remark				= $sheet->getCell('F'.$row)->getValue();
    $b_code				= $sheet->getCell('G'.$row)->getValue();
	$doc_type			= $sheet->getCell('H'.$row)->getValue();
	 
	//exit;
	//echo "ok--02 <br>";	  
if($no != ''){	 
		 
	$rowdata[$row]['no']      				=     ($no	== '' ? null : $no);
	$rowdata[$row]['w_type'] 		 		=     ($w_type 	== '' ? null : $w_type);
	$rowdata[$row]['emp_clode'] 		    =     ($emp_clode == '' ? null : $emp_clode);
	$rowdata[$row]['floor']       			=     ($floor == '' ? null : $floor);
	$rowdata[$row]['doc_title']      		=     ($doc_title == '' ? null : $doc_title);
	$rowdata[$row]['remark'] 		     	=     ($remark == '' ? null : $remark);
   
	
	if($floor != ''){
		   // $sql = "SELECT * FROM `mr_floor` WHERE `name` like '".$floor."'";
		   // $datafor =  $branchDao->select($sql);
		   // $datafor = $floorDao->getFloors($floor);

		   //if(!empty($datafor)){
		   //	$rowdata[$row]['ch_floor'] = $datafor;
		   //	$stroption_ed.='<option value="'.$datafor['mr_floor_id'].'" selected>'.$datafor['name'].'</option>';
			   
		   //}else{
			   $rowdata[$row]['ch_floor'] = 'nodata';
		   //}
	}else{
		$rowdata[$row]['ch_floor'] = 'nodata';
	}
	
	


	
	// echo "---------";
	// continue;
	if($emp_clode != ''){
		
		   if (is_numeric($emp_clode)) {
			   $emp_clode = ($emp_clode == '' ? null : $emp_clode);
		   } else {
			   $emp_clode = ($emp_clode == '' ? null : $emp_clode);
		   }

		   $params 	= array();
		   $emp 		= new Pivot_Dao('mr_emp');
		   $emp_db 	= $emp->getDb();
		   
		   $sql = "
				SELECT 
				   em.mr_emp_id ,
				   em.mr_emp_code,
				   em.mr_floor_id,
				   AES_DECRYPT(em.mr_emp_name, '".$_secret->database->key."') as mr_emp_name,
				   AES_DECRYPT(em.mr_emp_lastname, '".$_secret->database->key."') as mr_emp_lastname,
				   AES_DECRYPT(em.mr_emp_tel, '".$_secret->database->key."') as mr_emp_tel,
				   AES_DECRYPT(em.mr_emp_email, '".$_secret->database->key."') as mr_emp_email,

				   d.mr_department_id,
				   d.mr_department_name,
				   d.mr_department_code,
				   u.mr_user_role_id,
				   b.mr_branch_id,
				   b.mr_branch_code,
				   b.mr_branch_name,
				   f.name as f_name,
				   p.mr_position_name
			   FROM mr_emp em
				   left join mr_floor f on (f.mr_floor_id = em.mr_floor_id)
				   left join mr_department d on (d.mr_department_id = em.mr_department_id)
				   left join mr_user u on (u.mr_emp_id = em.mr_emp_id)
				   left join mr_branch b on (b.mr_branch_id = em.mr_branch_id)
				   left join mr_position p on (p.mr_position_id = em.mr_position_id)
			   where mr_emp_code like ?
		   ";

		   $stmt = new Zend_Db_Statement_Pdo($emp_db, $sql);
		   array_push($params, $emp_clode);
		   $stmt->execute($params);
		   $data = $stmt->fetchAll();
		//    echo print_r($sql,true);
		//    echo print_r($params,true);
		//    echo print_r($data,true);
		//    exit;		   
		   if(!empty($data)){

			   $datatosave = $data[0];
			   $stroption_emp .= '<option value="'.$datatosave['mr_emp_id'].'" selected>'.$datatosave['mr_emp_code'].':'.$datatosave['mr_emp_name']  .'  '.$datatosave['mr_emp_lastname'] .'</option>';
			   
			   $rowdata[$row]['ch_empcode'] = $datatosave;
			   $strtel = ($datatosave['mr_position_name'] != '')?$datatosave['mr_position_name']:'ไม่พบข้อมูล';
			   
			   if($datatosave['mr_user_role_id'] == 5 ){
				   $strdep = ($datatosave['mr_branch_name'] != '')?$datatosave['mr_branch_name']:'ไม่พบข้อมูล';
			   } else {
				   $strdep = ($datatosave['mr_department_name'] != '')?$datatosave['mr_department_name']:'ไม่พบข้อมูล';
				   
				   if($rowdata[$row]['ch_floor'] == 'nodata' and $datatosave['f_name'] != ''){
					   $stroption_ed .= '<option value="'.$datatosave['mr_floor_id'].'" selected>'.$datatosave['f_name'].'</option>';
				   }
			   }
			   
		   } else {
			   $rowdata[$row]['ch_empcode'] = 'nodata';
		   }
	   // บัตรเดบิต - - - > ผู้รับเอกสาร   เจ้าหน้าที่บริการลูกค้าอาวุโส (Job_Code =000391)
	   //รหัสบัตร -- - > ผู้รับเอกสาร      ผู้ช่วยผู้จัดการ-งานปฏิบัติการและบริการ (Job_Code = 000239)
   }elseif ($b_code != ''){
	//echo "ok--02.2 <br>";
			   if (is_numeric($b_code)) {
				   $b_code = ($b_code == '' ? null : $b_code);
			   } else {
				   $b_code = ($b_code == '' ? null : "'".$b_code."'");
			   }

			   if(rtrim($doc_title) == 'cheque'){
				   $set_job = " ,
							   CASE 
								   WHEN p.mr_position_code = 002736 THEN '1'
								   WHEN p.mr_position_code = 002737 THEN '2'
								   WHEN p.mr_position_code = 002735 THEN '3'
								   WHEN p.mr_position_code = 002738 THEN '4'
								   WHEN p.mr_position_code = 002739 THEN '5'
								   ELSE 999
							   END AS sost_by 
				   ";
				   
				   $position_code=" 002736,
									002737,
									002735,
									002738,
									002739 ";

				   // No.	Jobcode	Jobcode Descr THA	Jobcode Descr ENG
				   // 1	001925	ผู้ช่วยผู้จัดการสาขา	Assistant Branch Manager
				   // 2	001926	ผู้ช่วยผู้จัดการสาขาต้นแบบ	Flagship - Assistant Branch Manager
				   // 3	000391	เจ้าหน้าที่บริการลูกค้าอาวุโส	Senior Customer Service Officer
				   // 4	001812	เจ้าหน้าที่บริการลูกค้าอาวุโส สาขาต้นแบบ	Flagship - Senior Customer Service Officer
				   // 5	000008	ผู้จัดการสาขา	Branch Manager
				   // 6	001810	ผู้จัดการสาขาต้นแบบ	Flagship - Branch Manager

				   

			   } elseif (rtrim($doc_title) == 'marketing'){
				   $set_job = " ,
							   CASE 
								   WHEN p.mr_position_code = 001925 THEN '1'
								   WHEN p.mr_position_code = 000008 THEN '2'
								   WHEN p.mr_position_code = 001926 THEN '3'
								   WHEN p.mr_position_code = 001810 THEN '4'
								   WHEN p.mr_position_code = 000391 THEN '5'
								   WHEN p.mr_position_code = 001812 THEN '6'
								   ELSE 999
							   END AS sost_by 
				   ";
			   
				   $position_code=" 001925,
									000008,
									001926,
									001810,
									000391,
									001812 ";
				   //	Seq	Jobcode	Jobcode Descr THA	Jobcode Descr ENG
				   //	1	001925	ผู้ช่วยผู้จัดการสาขา	Assistant Branch Manager
				   //	2	000008	ผู้จัดการสาขา	Branch Manager
				   //	3	001926	ผู้ช่วยผู้จัดการสาขาต้นแบบ	Flagship - Assistant Branch Manager
				   //	4	001810	ผู้จัดการสาขาต้นแบบ	Flagship - Branch Manager
				   //	5	000391	เจ้าหน้าที่บริการลูกค้าอาวุโส	Senior Customer Service Officer
				   //	6	001812	เจ้าหน้าที่บริการลูกค้าอาวุโส สาขาต้นแบบ	Flagship - Senior Customer Service Officer





			   }else{
				   $set_job = " ,
							   CASE 
								   WHEN p.mr_position_code = 001925 THEN '1'
								   WHEN p.mr_position_code = 001926 THEN '2'
								   WHEN p.mr_position_code = 000391 THEN '3'
								   WHEN p.mr_position_code = 001812 THEN '4'
								   WHEN p.mr_position_code = 000008 THEN '5'
								   WHEN p.mr_position_code = 001810 THEN '6'
								   ELSE 999
							   END AS sost_by 
				   ";
				   ////เจ้าหน้าที่บริการลูกค้าอาวุโส (Job_Code =000391)
				   $position_code=" 001925,
								   001926,
								   000391,
								   001812,
								   000008,
								   001810 ";



			   }

				$sql = "
					SELECT 
						   em.mr_emp_id ,
						   em.mr_emp_lastname,
						   em.mr_emp_name,
						   em.mr_emp_code,
						   	AES_DECRYPT(em.mr_emp_name, '".$_secret->database->key."') as mr_emp_name,
							AES_DECRYPT(em.mr_emp_lastname, '".$_secret->database->key."') as mr_emp_lastname,
							AES_DECRYPT(em.mr_emp_tel, '".$_secret->database->key."') as mr_emp_tel,
							AES_DECRYPT(em.mr_emp_email, '".$_secret->database->key."') as mr_emp_email,
						   em.mr_floor_id,
						   d.mr_department_id,
						   d.mr_department_name,
						   d.mr_department_code,
						   u.mr_user_role_id,
						   b.mr_branch_id,
						   b.mr_branch_code,
						   b.mr_branch_name,
						   f.name as f_name,
						   p.mr_position_name,
						   em.mr_emp_tel ".$set_job."
				   FROM mr_emp em
					   left join mr_floor f on (f.mr_floor_id = em.mr_floor_id)
					   left join mr_department d on (d.mr_department_id = em.mr_department_id)
					   left join mr_user u on (u.mr_emp_id = em.mr_emp_id)
					   left join mr_branch b on (b.mr_branch_id = em.mr_branch_id)
					   left join mr_position p on (p.mr_position_id = em.mr_position_id)
				   WHERE b.mr_branch_code like ?
				   and p.mr_position_code in (".$position_code.")
				   and em.mr_emp_code NOT LIKE 'Resign'
				   ORDER BY sost_by asc
			   ";

			   $params		= array();
			   $emp 		= new Pivot_Dao('mr_emp');
			   $emp_db 	= $emp->getDb();

			   array_push($params, $b_code);
			   //array_push($params, $position_code);

			   $stmt = new Zend_Db_Statement_Pdo($emp_db, $sql);

			   $stmt->execute($params);
			   $data = $stmt->fetchAll();

			   //$sql = "SELECT * FROM `mr_floor` WHERE `name` IN ('".$floor."')";
			   //echo $sql;
			   //echo json_encode($datatosave);
			   //exit; 		
			   // $datatosave =  $branchDao->select($sql);
			
			   if(!empty($data)){
				   $datatosave = $data[0];
				   $stroption_emp.='<option value="'.$datatosave['mr_emp_id'].'" selected>'.$datatosave['mr_emp_code'].':'.$datatosave['mr_emp_name']  .'  '.$datatosave['mr_emp_lastname'] .'</option>';
				   
				   $rowdata[$row]['ch_empcode'] = $datatosave;
				   $strtel = ($datatosave['mr_position_name'] != '')?$datatosave['mr_position_name']:'ไม่พบข้อมูล';
				   
				   if($datatosave['mr_user_role_id'] == 5 ){
					   $strdep = ($datatosave['mr_branch_name'] != '')?$datatosave['mr_branch_name']:'ไม่พบข้อมูล';
				   }else{
					   $strdep = ($datatosave['mr_department_name'] != '')?$datatosave['mr_department_name']:'ไม่พบข้อมูล';
					   
					   if($rowdata[$row]['ch_floor'] == 'nodata' and $datatosave['f_name'] != ''){
						   $stroption_ed.='<option value="'.$datatosave['mr_floor_id'].'" selected>'.$datatosave['f_name'].'</option>';
					   }
				   }
				   
			   }else{
				   $rowdata[$row]['ch_empcode'] = 'nodata';
				   $datatosave = array();
			   }
	   }else{
		   $rowdata[$row]['ch_empcode'] = 'nodata';
	   }
	
	

	
	   //echo "ok--04.004 <br>";
	
	
	
   $str_tble[$row_no]['error_emp'] 		= $error_emp;
   //$str_tble[$row_no]['sql'] 			= $sql;
   //$str_tble[$row_no]['data'] 		= $data;
   $str_tble[$row_no]['error_type'] = $error_type;
   $str_tble[$row_no]['in'] = $row_no;
   $str_tble[$row_no]['no'] = $no;
   $str_tble[$row_no]['type'] = '<div id="type_error_'.$row_no.'" class="form-group">
									   <select  id ="val_type_'.$row_no.'" name="val_type_'.$row_no.'" class="valit_error select_type" style="width:100%">
										 <option value="">ไม่พบข้อมูล</option>
										 <option value="1" '.(( $w_type	==1)?'selected':'').'>ส่งที่สำนักงานใหญ่</option>
										 <option value="2" '.(( $w_type	==2)?'selected':'').'>ส่งที่สาขา</option>
									   </select>
								   </div>
								   ';
   $str_tble[$row_no]['name'] = '<div id="name_error_'.$row_no.'" class="form-group">
								   <select id="emp_'.$row_no.'" name="emp_'.$row_no.'"   class="select_name" style="width:100%" >'.$stroption_emp.'</select>
								 </div>';
   if($w_type	==1){							  
   $str_tble[$row_no]['floor'] = '<div id="floor_error_'.$row_no.'" class="form-group">
								   <select id="floor_'.$row_no.'" name="floor_'.$row_no.'" class="select_floor"   style="width:100%" >'.$stroption_ed.$stroption.'</select>
							   </div>	
							   ';
   }else{
   $str_tble[$row_no]['floor'] = '<div id="floor_error_'.$row_no.'" class="form-group">
								   <select id="floor_'.$row_no.'" name="floor_'.$row_no.'" class="select_floor"   style="width:100%" >'
								   .' 	 
										 <option value="1">ชั่น 1</option>
										 <option value="2">ชั่น 2</option>
										 <option value="3">ชั่น 3</option>'.
								   '</select>
							   </div>';
   }							
							   
   $str_tble[$row_no]['doc_title'] = '<input onkeyup="ch_fromdata();" type="text" id="title_'.$row_no.'" name="title_'.$row_no.'" value="'.$doc_title.'" >';
   $str_tble[$row_no]['remark'] = '<input  type="text" name="remark_'.$row_no.'" value="'.$remark.'" >';
   $str_tble[$row_no]['tel'] = '<input type="text" id="tel_'.$row_no.'" name="tel_'.$row_no.'" value="'.$strtel.'" readonly>';

$strdep = array();
 if(empty($datatosave)){
	$strdep = '';
 }else{
	 if($w_type	==1){
		$strdep = ' <option selected value="'.$datatosave['mr_department_id'].'">'.$datatosave['mr_department_code'].': '.$datatosave['mr_department_name'].'</option>';	
		// $strdep[] = ' <option selected value="'.isset($datatosave['mr_department_id'])?$datatosave['mr_department_id']:''.'">'.$datatosave['mr_department_code'].':'.$datatosave['mr_department_name'].'</option>';
   }elseif($w_type	==2){
	   $strdep = ' <option selected value="'.$datatosave['mr_branch_id'].'">'.$datatosave['mr_branch_code'].': '.$datatosave['mr_branch_name'].'</option>';	
	//    $strdep[] = ' <option selected value="'.isset($datatosave['mr_branch_id'])?$datatosave['mr_branch_id']:''.'">hh'.$datatosave['mr_branch_code'].':'.$datatosave['mr_branch_name'].'</option>';
   }else{
		$strdep = '';
   }
   	//echo print_r($strdep); 
	//exit;
 }
   //$str_tble[$row_no]['strdep'] = $strdep;
   
   $str_tble[$row_no]['strdep'] = '<div id="dep_error_'.$row_no.'">
									   <select '.(($w_type == 1) ? ' readonly ':'').'class="strdep" name="strdep_'.$row_no.'" id="strdep_'.$row_no.'" style="width:100%">'.$strdep.'</select>
								   </div>';
   $row_no++;


   }	
   //echo "ok--03 <br>";
}
//echo "ok--04 <br>";
//echo print_r($str_tble,true);
$data_send=array();
$data_send['status'] = "200";
$data_send['data'] = $str_tble;
//$newString = mb_convert_encoding($data_send, "UTF-8", "auto");
//echo print_r($data_send,true);

//echo print_r($data_send,true);
echo json_encode($data_send);
// //echo "<pre>".print_r($rowdata, true)."</pre>";
exit;
?>


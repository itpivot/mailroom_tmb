<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Work_order.php';
require_once 'Dao/Receiver.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Building_external.php';
error_reporting(E_ALL & ~E_NOTICE);

$auth 					= new Pivot_Auth();
$userdao 				= new Dao_User();
$req 					= new Pivot_Request();
$employeedao 			= new Dao_Employee();
$work_orderdao 			= new Dao_Work_order();
$receiverdao 			= new Dao_Receiver();
$work_logdao 			= new Dao_Work_log();
$building_externaldao 	= new Dao_Building_external();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$page = $req ->get('page');

if(preg_match('/<\/?[^>]+(>|$)/', $page)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$users = $auth->getUser();

if($page =='in'){

	$employee_id 	= $req->get('employee_id');
	$inresevre_id 	= $req->get('inresevre_id');
	$inremark 		= $req->get('inremark');
	$radio 			= $req->get('radio');

	if(preg_match('/<\/?[^>]+(>|$)/', $employee_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $inresevre_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $inremark)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $radio)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}

	$insavedataorder['scg_employee_id'] 				= $employeedao->getEmployeeid($employee_id);	
	$insavedataorder['scg_receiver_internal_id'] 		= $employeedao->getEmployeeid($inresevre_id);
	if($radio == 2){
		$insavedataorder['barcode'] 						= genBarcode("SI".date('m'));	
	}
	else{
		$insavedataorder['barcode'] 						= genBarcode("PI".date('m'));
	}
	$insavedataorder['remark'] 							= $inremark;
	
	$work_log['scg_work_order_id']						= $work_orderdao->save($insavedataorder);
	$work_log['scg_status_id']							= 1;
	$save=$work_logdao->save($work_log);
	
	//echo print_r($insavedataorder,true);
	echo $work_log['scg_work_order_id']	;
	
}else if ($page =='ex'){
	$companyname 		= $req->get('companyname');
	$exname 			= $req->get('exname');
	$exlname 			= $req->get('exlname');
	$exaddress_no 		= $req->get('exaddress_no');
	$exaddress_tumbon 	= $req->get('exaddress_tumbon');
	$exaddress_district = $req->get('exaddress_district');
	$exaddress_province = $req->get('exaddress_province');
	$extel 				= $req->get('extel');
	$employee_id 		= $req->get('employee_id');
	$extime 			= $req->get('extime');
	$exdate 			= $req->get('exdate');
	$exremark 			= $req->get('exremark');
	$radio 				= $req->get('radio');

	if(preg_match('/<\/?[^>]+(>|$)/', $companyname)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exname)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exlname)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exaddress_no)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exaddress_tumbon)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exaddress_district)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exaddress_province)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $extel)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $employee_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $extime)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exdate)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $exremark)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $radio)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}

	$arr_companyname=(explode("   ",$companyname));
	$dd=$building_externaldao->getBuildidBYnam($arr_companyname);
	
	$exsavedataresever['scg_receiver_id'] 				= $receiverID;
	$exsavedataresever['name'] 							= $exname;
	$exsavedataresever['surname'] 						= $exlname;
	$exsavedataresever['scg_receiver_type_id'] 			= 1;
	$exsavedataresever['address'] 						= 'บริษัท :'.$companyname.'  บ้านเลขที่ :'.$exaddress_no.'  แขวง/ตำบล :'.$exaddress_tumbon.'  เขต/อำเภอ :'.	$exaddress_district.'  จังหวัด :'.$exaddress_province;
	$exsavedataresever['telephone_number'] 				= $extel;
	$exsavedataresever['active'] 						= 1;
	$exsavedataresever['scg_building_external_id'] 		= $building_externaldao->getBuildidBYnam($arr_companyname);
	
	$exsavedataorder['scg_employee_id'] 				= $employeedao->getEmployeeid($employee_id);	
	$exsavedataorder['scg_receiver_id'] 				= $receiverdao->save($exsavedataresever);	
	$exsavedataorder['system_date'] 					= $exdate." ".$extime;
	if($radio == 2){
		$exsavedataorder['barcode'] 						= genBarcode("SE".date('m'));	
	}
	else{
		$exsavedataorder['barcode'] 						= genBarcode("PE".date('m'));
	}
	$exsavedataorder['remark'] 							= $exremark;
	
	$work_log['scg_work_order_id']						= $work_orderdao->save($exsavedataorder);
	$work_log['scg_status_id']							= 1;
	$save=$work_logdao->save($work_log);

	echo $work_log['scg_work_order_id']	;
		
}else if ($page =='post'){

	$postname 				= $req->get('postname');
	$postlname 				= $req->get('postlname');
	$postaddress_no 		= $req->get('postaddress_no');
	$postaddress_moo 		= $req->get('postaddress_moo');
	$postaddress_tumbon 	= $req->get('postaddress_tumbon');
	$postaddress_district 	= $req->get('postaddress_district');
	$postaddress_province 	= $req->get('postaddress_province');
	$postaddress_clod 		= $req->get('postaddress_clod');
	$posttel 				= $req->get('posttel');
	$employee_id 			= $req->get('employee_id');
	$postdate 				= $req->get('postdate');
	$posttime 				= $req->get('posttime');
	$postremark 			= $req->get('postremark');
	$radio 					= $req->get('radio');

	if(preg_match('/<\/?[^>]+(>|$)/', $postname)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postlname)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postaddress_no)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postaddress_moo)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postaddress_tumbon)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postaddress_district)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postaddress_province)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postaddress_clod)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $posttel)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $employee_id)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postdate)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $posttime)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $postremark)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	if(preg_match('/<\/?[^>]+(>|$)/', $radio)) {
		echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
		exit();
	}
	
	$postsavedataresever['scg_receiver_id'] 			= $receiverID;
	$postsavedataresever['name'] 						= $postname;
	$postsavedataresever['surname'] 					= $postlname;
	$postsavedataresever['scg_receiver_type_id'] 		= 2;
	$postsavedataresever['address'] 					= ' บ้านเลขที่ :'.$postaddress_no.'  หมู่ :'.$req->get('postaddress_moo').'  แขวง/ตำบล :'.$postaddress_moo.'  เขต/อำเภอ :'.	$postaddress_district.'  จังหวัด :'.$postaddress_province.'  รหัสไปรษณีย์ :'.$postaddress_clod;
	$postsavedataresever['telephone_number'] 			= $posttel;
	$postsavedataresever['active'] 						= 1;
	
	$postsavedataorder['scg_employee_id'] 				= $employeedao->getEmployeeid($employee_id);	
	$postsavedataorder['scg_receiver_id'] 				= $receiverdao->save($postsavedataresever);	
	$postsavedataorder['system_date'] 					= $postdate." ".$posttime;
	if($radio == 2){
		$postsavedataorder['barcode'] 						= genBarcode("SP".date('m'));	
	}
	else{
		$postsavedataorder['barcode'] 						= genBarcode("PP".date('m'));
	}
	$postsavedataorder['remark'] 						= $postremark;
	
	$work_log['scg_work_order_id']						= $work_orderdao->save($postsavedataorder);
	$work_log['scg_status_id']							= 1;
	$save=$work_logdao->save($work_log);

	echo $work_log['scg_work_order_id']	;
}

function genBarcode($barcode){
	$work_orderdao 	= new Dao_Work_order();
	$last_barcode 	= $work_orderdao->work_orderdao($barcode);

	$num_run 		= substr( $last_barcode, -4 );
	$num_run++;
	$num = "0001";
	if( strlen($num_run) == 1 ){
		$num = "000".$num_run;
	}else if( strlen($num_run) == 2 ){
		$num = "00".$num_run;		
	}else if( strlen($num_run) == 3 ){
		$num = "0".$num_run;		
	}else if( strlen($num_run) == 4 ){
		$num = $num_run;
	}

	$barcode	 		= $barcode."".$num;
	return  $barcode;
}
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Work_branch.php';
ini_set('max_execution_time', 300);
error_reporting(E_ALL & ~E_NOTICE);

$req 				= new Pivot_Request();
$auth 				= new Pivot_Auth();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$branchDao			= new Dao_Branch();
$workbranchDao		= new Dao_Work_branch();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$user_id 		= $auth->getUser();
$emp_data_owner = $userDao->getEmpDataByuserid( $user_id ); 

$emp_id 		= $req->get('emp_id');
$emp_type 		= $req->get('emp_type');

if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $emp_type)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

$data_search['emp_type'] = $emp_type;
if ($emp_id){
	$user_data = $userDao->getUsersByEmpIdBranch( $emp_id ); 
	
	if ( $emp_type == "sender" ){
		if( !$user_data ){
			$data_search['sender']   	= 0;
			$data_search['receiver'] 	= $emp_data_owner['mr_emp_id'];
			
		}else{
			$data_search['sender']   	= $user_data['user_id'];
			$data_search['receiver'] 	= $emp_data_owner['mr_emp_id'];
		}
	}else if ( $emp_type == "receiver" ){
		
		$data_search['sender'] 		= $user_id;
		$data_search['receiver'] 	= $emp_id;
	}
}else{
	
	$data_search['receiver'] 	= $emp_data_owner['mr_emp_id'];
	$data_search['sender'] 		= $user_id;
	$data_search['check_emp'] 	= "empty";
}

$mr_type_work_id 	=  $req->get('mr_type_work_id'); 
$barcode 			=  $req->get('barcode'); 
$start_date 		=  $req->get('start_date');                     
$end_date 			=  $req->get('end_date');                                 
$status 			=  $req->get('status'); 
$department_type 	=  $req->get('department_type');
$department_id 		=  $req->get('department_id');
$branch_type 		=  $req->get('branch_type');
$branch_id 			=  $req->get('branch_id');

if(preg_match('/<\/?[^>]+(>|$)/', $mr_type_work_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $start_date)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $end_date)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $status)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $department_type)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $department_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $branch_type)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $branch_id)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}




$data_search['receiver'] 			= $emp_data_owner['mr_emp_id'];
$data_search['sender'] 				= $user_id;
$data_search['mr_type_work_id']  	=  $mr_type_work_id; 
$data_search['barcode']  			=  $barcode; 
$data_search['start_date']  		=  $start_date;                     
$data_search['end_date']  			=  $end_date;                                 
$data_search['status']  			=  $status; 
$data_search['department_type']  	=  $department_type;
$data_search['department_id']  		=  $department_id;
$data_search['branch_type']  		=  $branch_type;
$data_search['branch_id']  			=  $branch_id;
$data_search['user_id']  			=  $user_id;  

$count_data = 0;
$arrkey = array('barcode','status','mr_type_work_id','start_date','end_date','department_id','branch_id');	
foreach($arrkey as $val){
	if($data_search[$val]!=''){
		$count_data ++;
	}
}				
if($count_data<1){
	$start_date  						=  date("Y-m-d 00:00:00");
	$end_date  							=  date("Y-m-d H:i:s");
	$data_search['start_date']  		=  $start_date;                     
	$data_search['end_date']  			=  $end_date;   
}




function setDBToDate($date){
	$result = "";
	if( $date ){
		list( $y, $m, $d ) = explode("-", $date);
		$result = $d."/".$m."/".$y;
	}
	return $result;
}

$report = $workbranchDao->searchUserBranch( $data_search );

foreach( $report as $num => $value ){
	$status = '';
	$action = '';
	$report[$num]['no'] = $num +1;
	$report[$num]['mr_type_work_name'] = $value['mr_type_work_name'];
	$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
	// $report[$num]['name_receive2'] = $value['name_re2']." ".$value['lastname_re2'];
	$report[$num]['name_send'] = $value['name_se']." ".$value['lastname_se'];
	
	$report[$num]['barcode_show'] = "<b><a href='work_info.php?id=".urlencode(base64_encode($value['mr_work_main_id']))."'>".$value['mr_work_barcode']."</a><b>";
	if( empty($value['d_name_se'])){
		$report[$num]['add_sender'] = $value['branch_code_se']."-".$value['branch_name_se'];
	}else{
		
		$report[$num]['add_sender'] = $value['d_code_se']."-".$value['d_name_se'];
	}

	if(empty($value['d_name_re'])){
		$report[$num]['add_receiver'] = $value['branch_code_re']."-".$value['branch_name_re'];
	}else{
		$report[$num]['add_receiver'] = $value['d_code_re']."-".$value['d_name_re'];
	}	
	if($value['mr_status_id'] != 5 and $value['mr_status_id'] != 14 ){
		$report[$num]['name_receive2'] = "-";
		$report[$num]['add_receiver2'] = "-";
	}else{
		if((int)$value['b_mr_user_id'] == (int)$value['user_id_re2']) {
			$report[$num]['name_receive2'] = "-";
			$report[$num]['add_receiver2'] = "-";
		}else {
			$report[$num]['name_receive2'] = $value['name_re2']." ".$value['lastname_re2'];
			if(!empty($report[$num]['name_receive2'])) {
				if(empty($value['branch_name_re2']) || $value['branch_name_re2'] == "-"){
					$report[$num]['add_receiver2'] = $value['branch_name_re']."-".$value['branch_code_re'];
				}else{
					$report[$num]['add_receiver2'] = $value['branch_code_re2']."-".$value['branch_name_re2'];
				}	
			} else {
				$report[$num]['add_receiver2'] = "";
			}
		}
	}

	$action = '<button type="button"  class="btn btn-danger btn-sm" disabled>ยกเลิกการจัดส่ง</button>';
	switch(intval($value['mr_status_id'])) {
		// รอพนักงานเดินเอกสาร และ งานใหม่
		case 1:
			$status =  '<center><p class="pt-primary font-weight-bold">'.$value['mr_status_name'].'</p></center>';
			break;
		case 8:
			$action = '<button type="button" onclick="cancel_order(\''.urlencode(base64_encode($value['mr_work_main_id'])).'\');" class="btn btn-danger btn-sm">ยกเลิกการจัดส่ง</button>';
			$status =  '<center><p class="pt-primary font-weight-bold">'.$value['mr_status_name'].'</p></center>';
			break;
		case 7:
			$action = '<button type="button" onclick="cancel_order(\''.urlencode(base64_encode($value['mr_work_main_id'])).'\');" class="btn btn-danger btn-sm">ยกเลิกการจัดส่ง</button>';
			$status = '<center><p class="pt-indigo font-weight-bold">'.$value['mr_status_name'].'</p></center>';
			break;
		// เอกสารถึงมือผู้รับแล้ว
		case 5:
			$status = '<center><p class="pt-succes font-weight-bolds">'.$value['mr_status_name'].'</p></center>';
			break;
		case 12:
			$status = '<center><p class="pt-succes font-weight-bolds">'.$value['mr_status_name'].'</p></center>';
			break;
		// อยู่ระหว่างนำส่ง
		case 4:
			$status = '<center><p class="text-warning font-weight-bolds">'.$value['mr_status_name'].'</p></center>';
			break;
		case 11: 
			$status = '<center><p class="pt-teal font-weight-bold">'.$value['mr_status_name'].'</p></center>';
			break;
		// เอกสารถึงห้องเมลรูม
		case 3:
			$status = '<center><p class="text-warning font-weight-bolds">'.$value['mr_status_name'].'</p></center>';
			break;
		case 10: 
			$status = '<center><p class="pt-amber font-weight-bold">'.$value['mr_status_name'].'</p></center>';
			break;
		default:
			$status =  '<center><p class="pt-primary font-weight-bold">'.$value['mr_status_name'].'</p></center>';
			break;
	}
	$report[$num]['status'] = $status;
	$report[$num]['action'] = $action;
}


echo json_encode(array(
	'status' => 200,
	'data' => $report
));

?>

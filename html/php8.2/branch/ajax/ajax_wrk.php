<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_log.php';
error_reporting(E_ALL & ~E_NOTICE);


$auth 		= new Pivot_Auth();
$req 		= new Pivot_Request();
$workLogDao = new Dao_Work_log();

if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
	exit();
}

$mr_work_main_id    = $req->get('mr_work_main_id');
$mr_user_id         = $req->get('mr_user_id');
$mr_status          = $req->get('mr_status');

$data = array();
$data['mr_work_main_id'] 	= $mr_work_main_id;
$data['mr_user_id'] 		= $mr_user_id;
$data['mr_status'] 			= $mr_status;

?>

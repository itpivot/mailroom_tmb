<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Send_work.php';
require_once 'nocsrf.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//
$req = new Pivot_Request();
$userDao = new Dao_User();

$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$userRoleDao		= new Dao_UserRole();
$branchDao			= new Dao_Branch();
$send_workDao		= new Dao_Send_work();

$user_id			= $auth->getUser();
$user_data_show 	= $userDao->getEmpDataByuseridProfileBranch($user_id);
$user_data 			= $userDao->getEmpDataByuserid($user_id);
$csrf 				= NoCSRF::generate( 'csrf_token' );

if((!isset($user_data['mr_branch_id'])) || $user_data['mr_branch_id'] == ''){
	$alertt = "
			alertify.alert('alert','ไม่พบข้อมูลสาขากรุณาอัปเดทข้อมูล  Profile', function (e) {
				//window.open('profile.php');
			});
	";
}
$floor_data  = $send_workDao->select('SELECT f.* FROM `mr_floor` f
left join `mr_building` as b on(b.mr_building_id = f.mr_building_id)
where b.mr_branch_id = 1 ORDER BY cast(f.floor_level as unsigned),name ASC');
$branch_data = $send_workDao->select('SELECT * FROM mr_branch ORDER BY cast(mr_branch_code as unsigned) ASC');
//$mr_contact = $send_workDao->select('SELECT * FROM mr_contact');
//echo "INSERT INTO `mr_contact` (`department_code`, `department_name`, `floor`, `mr_contact_name`, `emp_code`, `emp_tel`, `remark`) VALUES <br>";
//foreach($mr_contact as $i => $v_){
//	echo "('".trim($v_['department_code'])."','".rtrim(ltrim($v_['department_name']))."','".rtrim(ltrim($v_['floor']))."','".rtrim(ltrim($v_['mr_contact_name']))."','".trim($v_['emp_code'])."','".rtrim(ltrim($v_['emp_tel']))."','".rtrim(ltrim($v_['remark']))."'),<br>";
//}
////echo '<pre>'.print_r($mr_contact,true).'</pre>';
//exit;

$time = date("H:i:s");
$time_check = strtotime('4.00pm');
$time_16 = date("H:i:s",$time_check);

if( $time >= $time_16 ){
	$tomorrow = 1;
}else{
	$tomorrow = 0;
}	

foreach ($branch_data as $key => $value) {
        $branch_data[$key]['branch'] = $value['mr_branch_code']." - " .$value['mr_branch_name'];
        //$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
		
}

$barcode  = "TM".date("dmy");



//$emp_data = $employeeDao->getEmpDataSelect();
//echo "<pre>".print_r($user_data_show,true)."</pre>";
//$contactdata = $send_workDao->select($sql);
$template = Pivot_Template::factory('branch/create_work.tpl');
$template->display(array(
	//'debug' => print_r($user_data,true),
	//'contactdata' => $contactdata,
	//'debug' => print_r($path_pdf,true),
	'userRoles' => (isset($userRoles) ? $userRoles : null),
	'success' => (isset($success) ? $success : null),
	'barcode' => $barcode,
	'floor_data' => $floor_data,
	'branch_data' => $branch_data,
	'emp_data' => (isset($emp_data) ? $emp_data : null),
	'users' => (isset($users) ? $users : null),
	'tomorrow' => $tomorrow,
	'user_data' => $user_data,
	'user_data_show' => $user_data_show,
	'csrf' => $csrf,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once('PhpSpreadsheet-master/vendor/autoload.php');



/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
//



$req 				= new Pivot_Request();
$user_Dao 			= new Dao_User();
$userRole_Dao		= new Dao_UserRole();
$work_main_Dao      = new Dao_Work_main();

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Table;
use PhpOffice\PhpSpreadsheet\Worksheet\Table\TableStyle;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();
// Set document properties

 


$user_id  = $auth->getUser();
$month = $req->get('month');
$year = $req->get('year');

$month_data = $work_main_Dao->getDataBranchSuccess($user_id, $month, $year);
$col 		= range('A', 'Z');
$columnNames = array(
		'No.',                           
		'Barcode',                            
		'ชื่อผู้รับ',                            
		'ที่อยู่',                            
		'ผู้ลงชื่อรับ',                            
		'ที่อยู่ ผู้ลงชื่อรับ',                            
		'ชื่อผู้ส่ง',                            
		'ที่อยู่',    
		'ชื่อเอกสาร',
		'วันที่ส่ง',                            
		 'วันที่สำเร็จ',                            
		 'สถานะงาน',                            
		 'ประเภทการส่ง',                            
		 'หมายเหตุ'     
);

// Add some data columnNames
$row=1;
$sheet       = $spreadsheet->getActiveSheet(0);
foreach($columnNames as $in_x=>$txt_X){
	$sheet->setCellValue($col[$in_x].$row,$txt_X)->getStyle($col[$in_x].$row)
	->getAlignment()
	->setHorizontal(PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
	$sheet->getStyle($col[$in_x].$row)->getFont()->setBold(true);
	$sheet->getStyle($col[$in_x].$row)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $sheet->getStyle($col[$in_x].$row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
    $sheet->getStyle($col[$in_x].$row)->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK);
    $sheet->getStyle($col[$in_x].$row)->getBorders()->getAllBorders()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
}
// Add Data
foreach($month_data as $keys => $vals) {
			if( $vals['mr_work_date_sent'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_sent']);
				$date_order[$keys]['mr_work_date_sent'] = $d."/".$m."/".$y;
			}	
			
			if( $vals['mr_work_date_success'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_success']);
				$date_meet[$keys]['mr_work_date_success'] = $d."/".$m."/".$y;
			}	
			
			if(  $vals['receive_name'] ){
				$name_receive[$keys]['name_re'] 				= $vals['receive_name'];
				$name_receive[$keys]['depart_receive'] 		= $vals['receive_branch_code']." - ".$vals['receive_branch_name']." ชั้น ".$vals['receive_floor_name'];
					if($vals['receive_branch_code']==''){
						$name_receive[$keys]['depart_receive'] 		= $vals['re_department_code']." - ".$vals['re_department_name']." ชั้น ".$vals['receive_floor2'];
					}
			}
			
			if(  $vals['real_receive_name'] ){
				$name_receive[$keys]['real_name_re'] 				= $vals['real_receive_name'];
				$name_receive[$keys]['depart_real_receive'] 		= $vals['receive_branch_code']." - ".$vals['receive_branch_name'];
			}	
			
			if(  $vals['sender_name'] ){
				$name_send[$keys]['send_name'] 				= $vals['sender_name'];
				$name_send[$keys]['depart_send'] 				= $vals['sender_branch_code']." - ".$vals['sender_branch_name'];
				
			}	
			
				
			$ex_data[$keys] = array(
			 ($keys+1),
			 isset( $vals['mr_work_barcode'])?"'".$vals['mr_work_barcode']:'-',
			 isset( $name_receive[$keys]['name_re'])? $name_receive[$keys]['name_re']:'-',
			 isset( $name_receive[$keys]['depart_receive'])? $name_receive[$keys]['depart_receive']:'-',
			 isset( $name_receive[$keys]['real_name_re'])? $name_receive[$keys]['real_name_re']:'-',
			 isset( $name_receive[$keys]['depart_real_receive'])? $name_receive[$keys]['depart_real_receive']:'-',
			 isset( $name_send[$keys]['send_name'])? $name_send[$keys]['send_name']:'-',
			 isset( $name_send[$keys]['depart_send'])? $name_send[$keys]['depart_send']:'-',
			 isset( $vals['mr_topic'])? $vals['mr_topic']:'-',
			 isset( $date_order[$keys]['mr_work_date_sent'])? $date_order[$keys]['mr_work_date_sent']:'-',
			 isset( $date_meet[$keys]['mr_work_date_success'])? $date_meet[$keys]['mr_work_date_success']:'-',
			 isset( $vals['mr_status_name'])? $vals['mr_status_name']:'-',
			 isset( $vals['mr_type_work_name'])? $vals['mr_type_work_name']:'-',
			 isset( $vals['mr_work_remark'])? $vals['mr_work_remark']:'-',
			);

}
//Create Table Data
$row++;
foreach($ex_data as $in_i=>$txt_i){
	foreach($txt_i as $in_j=>$txt_j){
		$sheet->setCellValue($col[$in_j].$row,$txt_j)->getStyle($col[$in_j].$row)->getAlignment()->setHorizontal(PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle($col[$in_j].$row,$txt_j)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle($col[$in_j].$row,$txt_j)->getBorders()->getAllBorders()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

	}
	$row++;
}



// echo "<pre>".print_r($ex_data,true);
// exit;




// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('report_success');
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);
 
// Redirect output to a client’s web browser (Xlsx)
$nickname = "report_success";
$filename = $nickname.'.xlsx';
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
 
// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
 
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

exit;

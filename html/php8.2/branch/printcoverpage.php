<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
ini_set('max_execution_time', 0);
error_reporting(E_ALL & ~E_WARNING | E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id	= $auth->getUser();
$user_data 	= $userDao->getEmpDataByuserid($user_id);
$alert 	= '';
$data  		= array();

$_secret = $userDao->getEncryptKey();


$work_main_id = $req->get('maim_id');
$chm = $work_main_id;
if($work_main_id == 'sesstion' ){
	$work_main_id = $_SESSION['sesstion_all_id'][$user_id];
}

if($work_main_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'create_work.php';
			}
		}
	});
		
	";
}else{

		function decode_work_main_id($id)
		{
			return base64_decode(urldecode($id));
			//urlencode(base64_encode($work_main_id));
		}

		$params 				= array();
		$in_work_main_id_params = array(); // [1, 2, 3]
		$in_work_main_id_params = explode(',',$work_main_id);
		$params_work_main_id 	= array(); // condition: generate ?,?,?
		$params_work_main_id 	= str_repeat('?,', count($in_work_main_id_params) - 1) . '?'; // example: ?,?,?

		$sql="
			SELECT 
						m.*,
						con.department_name as detail,
						con.emp_code as con_empcode,
						concat(con.department_name,' | ',m.mr_topic) as new_topic,
						sw.sender_tel as sw_sender_tel,
						sw.barcode as sw_barcode,
						wb.mr_branch_floor ,
						floor.name as re_floor ,
						AES_DECRYPT(e.mr_emp_tel, '".$_secret->database->key."') as send_tel,
						AES_DECRYPT(e.mr_emp_name, '".$_secret->database->key."') as send_name,
						AES_DECRYPT(e.mr_emp_lastname, '".$_secret->database->key."')  as send_lname,
						AES_DECRYPT(e2.mr_emp_tel, '".$_secret->database->key."') as re_tel,
						AES_DECRYPT(e2.mr_emp_name, '".$_secret->database->key."') as re_name,
						AES_DECRYPT(e2.mr_emp_lastname, '".$_secret->database->key."')  as re_lname,
						d.mr_department_code as re_mr_department_code,
						d.mr_department_name as re_mr_department_name,
						d2.mr_department_code as send_mr_department_code,
						d2.mr_department_name as send_mr_department_name,
						b.mr_branch_code as re_mr_branch_code,
						b2.mr_branch_code as send_mr_branch_code,
						b.mr_branch_name as re_mr_branch_name,
						b2.mr_branch_name as send_mr_branch_name,
						u.mr_user_role_id,
						con.department_name
				FROM mr_work_main m
				left join mr_send_work sw using(mr_send_work_id)
				LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
						LEFT JOIN mr_floor floor ON ( floor.mr_floor_id = wb.mr_floor_id )
						LEFT JOIN mr_contact con ON ( con.mr_contact_id = wb.mr_contact_id )
						LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
						left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
						LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
						Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
						Left join mr_department d2 on ( d2.mr_department_id = e.mr_department_id )
						Left join mr_branch b on ( b.mr_branch_id = wb.mr_branch_id )
						Left join mr_branch b2 on ( b2.mr_branch_id = e.mr_branch_id )
						where m.mr_work_main_id in(".$params_work_main_id.")
						group by m.mr_work_main_id
				";	

		if($chm == 'sesstion' ){
		//	$arr_id = array();
		//	foreach($in_work_main_id_params as $v){
		//		array_push($arr_id, urlencode(base64_encode($v)) );
		//	}
		//	$params = array_map('decode_work_main_id', $arr_id);
			$params = array_map('decode_work_main_id', $in_work_main_id_params);
		}else{
			
			$params = array_map('decode_work_main_id', $in_work_main_id_params);
		}
		$data = $send_workDao->select_send_work($sql,$params);


// echo "<pre>".print_r($data,true)."</pre>";
// exit;

		$page 	= 1;
		$index 	= 1;
		$no=1;
		$newdata = array();
		foreach($data as $i => $val_i){
			if($val_i['con_empcode']=='TMA003'){
				$val_i['mr_topic'] = $val_i['new_topic'];
			}
			
			if(count($data)-1 == $i){
				//$val_i=$data[($i-1)];
				$val_i['no']='all';
				$val_i['qty']=count($data)-1;
			}else{
				//$val_i=$data[($i-1)];
				$val_i['no']=$no;
				$val_i['qty']=1;
			}
			$newdata[$page]['h']['p'] = $page;
			$newdata[$page]['h']['all'] = ceil(count($data)/4);  
			$newdata[$page]['d'][$index] = $val_i;
			if($index == 4){
				$index = 1;
				$page++;
			}else{
				$index++;
			}
			$no++;	
		}
}
// exit;


$txt_html ='<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
     
      font-family: "Garuda";
	  font-size: 18px

    }
	.border_l{
		border-right: solid;
		border-bottom: solid;
		border-width: 0.5px;
		position:relative;
	}.border_r{
		border-bottom: solid;
		border-width: 0.5px;
		position:relative;
	}

    .page {
        background: white;
            width: 29.7cm;
            height: 21cm;
            display: block;
           padding:0px;
		   margin:0px;
            margin-bottom: 0.5cm;
           
      }  

.txt_mini{
	font-size: 8px;
	margin-top:4px;
}
.text_p{
	margin-bottom:5px;
	font-size: 16px;
	border-bottom: 1px dotted black;
	text-align: left;
	
}.text_p>b{
	padding-bottom:5px;
}
h4{
 margin-bottom:10px;
}
.span_code{
	border-style: solid;
	border-width:medium;
	padding:0px;
	margin:0px;
}

.box_left{
	//float:left;
	//overflow: hidden;
	word-wrap:break-word;
	max-width:100%;
}
.box_rihth{
   position:absolute;
   right:0px;
    padding-left: 20px;
	margin-left:50%;
	//border-width: 2px;
	//overflow: hidden;
	max-width:100%;
	min-width:50%;
	//word-wrap:break-word;
}
@page :right {
	margin-top: 0cm;
	margin-bottom: 0cm;
	header: html_myHeader;
}



  </style>
</head>

<body>';

   foreach($newdata as $i => $page){

    $txt_html.='<div class="page">';
	if(count($page['d'])>1){
					$txt_html.=' <table border="0" width="100%">';
				}else{
					$txt_html.=' <table border="0" width="650px">';
				}
	  for($j=1;$j <= count($page['d']);$j++){
	  $td = $page['d'][$j];
		  $txt_html.='
		  <tr>
			<td class="border_l" width="50%">
							<center>';
							//<img src="https://barcode.tec-it.com/barcode.ashx?data='.$td['mr_work_barcode'].'&code=Code39&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0" height="45" width="350" align="absmiddle">
							$txt_html.='<table width="650px" border="0">';
							$txt_html.='
							<tr>
								<td>
									<barcode code="'.$td['mr_work_barcode'].'" type="C128A" height="0.7"/>
								</td>
								<td rowspan="4">
								<barcode code="QR-'.$td['mr_work_barcode'].'" type="QR" size="0.6" error="M" disableborder = "1"/>
								</td>
							</tr>';
							$txt_html.='<tr><td><center>'.$td['mr_work_barcode'].'</center></td></tr>';
							//$txt_html.='<br>';
							if($td['re_mr_branch_code'] != '' or $td['re_mr_department_code'] != ''){
								if($td['mr_type_work_id'] == 2){
									$txt_html.='<tr><td><center><span class="span_code">'.$td['re_mr_branch_code'].'</span></center></td></tr>';
								}else{
									$txt_html.='<tr><td><center><span class="span_code">'.$td['re_mr_department_code'].'</span></center></td></tr>';
								}
							}
							$txt_html.='<tr><td><center><p class="txt_mini">'.date('Y-m-d H:i:s').'</p></td></tr></center>';
				$txt_html.='</table>'; 

				$txt_html.='</center> 
				<table border="0" width="650px">
					<tr>
						<td>';
							$txt_html.='
							<table width="650px" border="0">
								<tr >
									<td align="left">
									<h3><b>จาก</b> : </h3>
									</td>
								</tr>
								<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อผู้ส่ง :   </b>'.$td['send_name'].'  '.$td['send_lname'].'<b> โทร :  </b>'.$td['send_tel'].'</td></tr> '; 
								if($td['mr_user_role_id'] == 5){
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสสาขา :  </b>'.$td['send_mr_branch_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อสาขา  :  </b>'.$td['send_mr_branch_name'].'</td></tr>';
								}else{
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['send_mr_department_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['send_mr_department_name'].'</td></tr>';
								}
								$txt_html.='
								<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่ออเอกสาร/จำนวน  :  </b>'.utf8_wordwrap( $td['mr_topic'],100,' / '.$td['quty'].' <br>',true).'</td></tr>
								<tr><td class="text_p"><b>&nbsp;&nbsp; หมายเหตุ : </b>'.utf8_wordwrap($td['mr_work_remark'],100,'<br>',true).'</td></tr>   	
							</table>
							
						</td>
					</tr>
					<tr>
						<td>';
						$tesiver = '<h3><b>กรุณาส่ง';
							if($td['mr_type_work_id'] == 2){
								$tesiver.='(ส่งที่สาขา)';
							}elseif($td['mr_type_work_id'] == 3){
								$tesiver.='(ส่งที่สำนักงานใหญ่)';
							}else{
								$tesiver.='(รับส่งภายในสำนักงานใหญ่)';
							}
							$txt_html.='</b></h3>
							<table width="650px">
							<tr >
									<td width="20%" rowspan="6">
									</td>
									<td align="left" width="80%">
									'.$tesiver.'
									</td>
								</tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อผู้รับ :   </b>'.$td['re_name'].'   '.$td['re_lname'].' <b>  โทร :   </b>'.$td['re_tel'].'</td></tr> '; 
								if($td['mr_type_work_id'] == 2){
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อสาขา  :  </b><span style="font-size:20px;"> '.$td['re_mr_branch_code'].' - '.$td['re_mr_branch_name'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>'.$td['mr_branch_floor'].'</td></tr>';
								}elseif($td['mr_type_work_id'] == 3){
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['re_mr_department_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['re_mr_department_name'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>'.$td['re_floor'].'</td></tr>';
								}else{
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['re_mr_department_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['re_mr_department_name'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>'.$td['re_floor'].'</td></tr>';
								}
								$txt_html.='
							</table>
						</td>
					</tr>
				</table>	
			</td>
		  ';
		$txt_html.='';
		$j++;
		if($j <= count($page['d'])){
		$td = $page['d'][$j];
		$txt_html.='
			<td class="border_r" width="50%">
							<center>';
							//<img src="https://barcode.tec-it.com/barcode.ashx?data='.$td['mr_work_barcode'].'&code=Code39&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=96&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0" height="45" width="350" align="absmiddle">
							$txt_html.='<table width="650px" border="0">';
							$txt_html.='
							<tr>
								<td>
									<barcode code="'.$td['mr_work_barcode'].'" type="C128A" height="0.7"/>
								</td>
								<td rowspan="4">
								<barcode code="QR-'.$td['mr_work_barcode'].'" type="QR" size="0.6" error="M" disableborder = "1"/>
								</td>
							</tr>';
							$txt_html.='<tr><td><center>'.$td['mr_work_barcode'].'</center></td></tr>';
							//$txt_html.='<br>';
							if($td['re_mr_branch_code'] != '' or $td['re_mr_department_code'] != ''){
								if($td['mr_type_work_id'] == 2){
									$txt_html.='<tr><td><center><span class="span_code">'.$td['re_mr_branch_code'].'</span></center></td></tr>';
								}else{
									$txt_html.='<tr><td><center><span class="span_code">'.$td['re_mr_department_code'].'</span></center></td></tr>';
								}
							}
							$txt_html.='<tr><td><center><p class="txt_mini">'.date('Y-m-d H:i:s').'</p></td></tr></center>';
				$txt_html.='</table>'; 
				$txt_html.='</center> 
				<table border="0" width="650px">
					<tr>
						<td>';
							$txt_html.='
							<table width="650px" border="0">
							<tr >
									<td align="left">
									<h3><b>จาก</b> :</h3>
									</td>
									
								</tr>
								<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อผู้ส่ง : </b>'.$td['send_name'].'  '.$td['send_lname'].'   โทร :   </b>'.$td['send_tel'].' </td></tr> '; 
								if($td['mr_user_role_id'] == 5){
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสสาขา :  </b>'.$td['send_mr_branch_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อสาขา  :  </b>'.$td['send_mr_branch_name'].'</td></tr>';
								}else{
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['send_mr_department_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['send_mr_department_name'].'</td></tr>';
								}
								$txt_html.='
								<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่ออเอกสาร/จำนวน  :  </b>'.utf8_wordwrap( $td['mr_topic'],100,' / '.$td['quty'].' <br>',true).'</td></tr>
								<tr><td class="text_p"><b>&nbsp;&nbsp; หมายเหตุ : </b>'.utf8_wordwrap($td['mr_work_remark'],100,'<br>',true).'</td></tr>   	
							</table>
							 
						</td>
					</tr>
					<tr>
						<td>';
						$tesiver = '<h3><b>กรุณาส่ง';
							if($td['mr_type_work_id'] == 2){
								$tesiver.='(ส่งที่สาขา)';
							}elseif($td['mr_type_work_id'] == 3){
								$tesiver.='(ส่งที่สำนักงานใหญ่)';
							}else{
								$tesiver.='(รับส่งภายในสำนักงานใหญ่)';
							}
							$txt_html.='</b></h3>
							<table width="650px" border="0">
									<tr >
									<td width="20%" rowspan="6">
									</td>
									<td align="left" width="80%">
									'.$tesiver.'
									</td>
								</tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อผู้รับ : </b>'.$td['re_name'].'   '.$td['re_lname'].'  โทร :   </b>'.$td['re_tel'].'</td></tr> '; 
								if($td['mr_type_work_id'] == 2){
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสสาขา :  </b>'.$td['re_mr_branch_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อสาขา  :  </b>'.$td['re_mr_branch_name'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>'.$td['mr_branch_floor'].'</td></tr>';
								}elseif($td['mr_type_work_id'] == 3){
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['re_mr_department_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['re_mr_department_name'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>'.$td['re_floor'].'</td></tr>';
								}else{
									$txt_html.='
									<tr><td class="text_p"><b>&nbsp;&nbsp; รหัสหน่วยงาน :  </b>'.$td['re_mr_department_code'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชื่อหน่วยงาน  :  </b>'.$td['re_mr_department_name'].'</td></tr>
									<tr><td class="text_p"><b>&nbsp;&nbsp; ชั้น  :  </b>'.$td['re_floor'].'</td></tr>
									';
								}
								$txt_html.='
								
							</table>
						</td>
					</tr>
				</table>	
				</td>
		  ';
		  }
		$txt_html.='
		</tr>';
		 
	  }
	  $txt_html.='
	  
	  
</table>			
</div>
';





}
$txt_html.='</body>

</html>';

// echo $txt_html;
// exit;
//$fp = fopen('lidn.txt', 'w');
//fwrite($fp, $txt_html);
//fclose($fp);
////
//
//exit;
//ob_start();

//$html = ob_get_clean();

//echo $txt_html;
//exit;

function utf8_wordwrap($string, $width=75, $break="\n", $cut=false)
{
  if($cut) {
    // Match anything 1 to $width chars long followed by whitespace or EOS,
    // otherwise match anything $width chars long
    $search = '/(.{1,'.$width.'})(?:\s|$)|(.{'.$width.'})/uS';
    $replace = '$1$2'.$break;
  } else {
    // Anchor the beginning of the pattern with a lookahead
    // to avoid crazy backtracking when words are longer than $width
    $pattern = '/(?=\s)(.{1,'.$width.'})(?:\s|$)/uS';
    $replace = '$1'.$break;
  }
  return preg_replace($search, $replace, $string);
}

//require_once 'mpdf-development//thaipdf.php';
//require_once 'ThaiPDF/thaipdf.php';
require_once 'mpdf-development/vendor/autoload.php';

 $mpdf = new \Mpdf\Mpdf();
 $pdf_orientation = 'L';//L&P;
 $left=3;
 $right=3;
 $top=5;
 $bottom=5;
 $header=0;
 $footer=1;
$align = $__pdf_pgn_oalign;
$border = "border-top-";
$pgf_o = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
$mpdf->DefHTMLFooterByName("fo", $pgf_o);
// $align = $__pdf_pgn_ealign;
// $pgf_e = "$dv1$align$dv2$border$dv3$border$dv4$border$dv5$dv6";
// $mpdf->DefHTMLFooterByName("fe", $pgf_e);
$mpdf->AddPage($pdf_orientation,'','','','',$left,$right,$top,$bottom,$header,$footer,'','','html_fo', 'html_fe', 0, 0, 1, 1);

 //$mpdf->AddPage('L'); 
 $mpdf->WriteHTML($txt_html);
 //$mpdf->setFooter('{PAGENO}');
 $mpdf->Output();


 exit; 




$filename='file.pdf';
pdf_margin($left,$right,$top, $bottom,$header,$footer);
pdf_html($txt_html);
pdf_orientation('L');   
     
pdf_echo();

exit;
$template = Pivot_Template::factory('branch/printcoverpage.tpl');
$template->display(array(
	//'debug' => print_r($newdata,true),
	'data' => $newdata,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
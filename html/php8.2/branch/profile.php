<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$departmentDao	= new Dao_Department();
$employeeDao 	= new Dao_Employee();
$floorDao 		= new Dao_Floor();
$branchDao 		= new Dao_Branch();

$position = $userDao->select('SELECT * FROM `mr_position` WHERE  `active` =1');
//$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$department_data = $departmentDao->fetchAll();
//$floor_data = $floorDao->fetchAll();
$branchata = $branchDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuseridProfile($user_id);

$emp_id 	= $user_data['mr_emp_id'];
$branch_id = $user_data['mr_branch_id']; 

$table_name = 'mr_contact';
		$sql = "
			SELECT f.* FROM `mr_floor` f
			left join `mr_building` as b on(b.mr_building_id = f.mr_building_id)
				where b.mr_branch_id = '".$branch_id."'
			order by b.mr_branch_id asc
			";
	if(!empty($sql)){
		$dao 			= new Pivot_Dao($table_name);
		$dao_db 		= $dao->getDb();
	
		$stmt = new Zend_Db_Statement_Pdo($dao_db, $sql);
		$stmt->execute();
		
		$contactdata = $stmt->fetchAll();
	}
		
    //$emp_data = $employeeDao->getEmpByID( $name_receiver_select );
	$floor = array();
		$floor[0]['id'] = '0';
		$floor[1]['id'] = '0';
		$floor[2]['id'] = '0';	
		$floor[3]['id'] = '0';	

		$floor[0]['name'] = 'ชั้น 1';
		$floor[1]['name'] = 'ชั้น 2';	
		$floor[2]['name'] = 'ชั้น 3';	
		$floor[3]['name'] = 'ชั้น 4';	
	if(empty($contactdata)){
		$floor = array();
		$floor[0]['id'] = '0';
		$floor[1]['id'] = '0';
		$floor[2]['id'] = '0';	
		$floor[3]['id'] = '0';	
		$floor[4]['id'] = '0';	

		$floor[0]['name'] = 'ชั้น G';
		$floor[1]['name'] = 'ชั้น 1';	
		$floor[2]['name'] = 'ชั้น 2';	
		$floor[3]['name'] = 'ชั้น 3';	
		$floor[4]['name'] = 'ชั้น 4';	

	}else{
		$floor = array();
		foreach($contactdata as $key=> $val){
			//$floor .='<option value="'.$val['name'].'" >'.$val['name'].'</option>';
			//$floor .='<option value="'.$val['mr_floor_id'].'" >'.$val['name'].'</option>';
			
			$floor[$key]['id'] 		= $val['mr_floor_id'];
			$floor[$key]['name'] 	= $val['name'];
		}
	}
	// echo "<pre>".print_r($user_data,true)."</pre>";
	// echo "<pre>".print_r($contactdata,true)."</pre>";
//echo "<pre>".print_r($user_data,true)."</pre>";
$template = Pivot_Template::factory('branch/profile.tpl');
$template->display(array(
	//'debug' => print_r($user_data,true),
	'position' => $position,
	'branchata' => $branchata,
	//'userRoles' => $userRoles,
	//'success' => $success,
	'department_data' => $department_data,
	//'userRoles' => $userRoles,
	//'users' => $users,
	'user_data' => $user_data,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'userID' => urlencode(base64_encode($user_id)),
	'empID' => $emp_id,
	'floor' => $floor,
	//'floor_data' => $floor_data
));
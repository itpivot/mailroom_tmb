<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
error_reporting(E_ALL & ~E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$user_Dao = new Dao_User();
$userRole_Dao		= new Dao_UserRole();
$work_main_Dao      = new Dao_Work_main();
$user_id  = $auth->getUser();
$user_data = $user_Dao->getempByuserid($user_id);
$user_data_show = $user_Dao->getEmpDataByuseridProfileBranch($user_id);





$view_month =	$_CONFIG->view->toArray();
$months		=	array_values($view_month['months']);

$month_data = $work_main_Dao->getMonthBranchSuccess($user_id);

$yearSuccess = array();

if(count($month_data) > 0) {
    foreach($month_data as $k => $v) {
        $yearSuccess[$v['year_success']] = $v['year_success'];
    } 
}

$template = Pivot_Template::factory('branch/reports.tpl');
$template->display(array(
   // 'debug' => print_r($yearSuccess,true),
    'role_id' => $auth->getRole(),
    'user_data' => $user_data,
    'roles' => Dao_UserRole::getAllRoles(),
    'years' => (count($yearSuccess) > 0 ? array_values($yearSuccess) : null ),
    'serverPath' => $_CONFIG->site->serverPath
));
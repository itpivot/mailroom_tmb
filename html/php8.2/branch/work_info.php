<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Status.php';
require_once 'Dao/Work_log.php';
error_reporting(E_ALL & ~E_NOTICE);

$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRole_Dao		= new Dao_UserRole();
$work_main_Dao      = new Dao_Work_main();
$branchDao			= new Dao_Branch();
$floorDao 			= new Dao_Floor();
$statusDao          = new Dao_Status();
$logDao             = new Dao_Work_log();

$user_id            = $auth->getUser();
$user_data          = $userDao->getempByuserid($user_id);
$user_data_show     = $userDao->getEmpDataByuseridProfileBranch($user_id);

$id = $req->get('id');
$id = base64_decode(urldecode($id));

// $id = 400666;
if(preg_match('/<\/?[^>]+(>|$)/', $id)) {
	$alertt = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'search_work.php';
			}
		}
	});
		
	";
}else{
    $data = $work_main_Dao->getWorkMainById($id);

    //mr_user_role_id  == ผุ้ส่ง
    //mr_type_work_id  ==  ประเภทการส่ง

    $branch_data = $branchDao->getBranch();
    $floor_data = $floorDao->fetchAll();
    $status_data = $statusDao->getStatusBranch();
    
    $log = $logDao->getWorkLogById($id);
    
    foreach ($branch_data as $key => $value) {
        $branch_data[$key]['branch'] = $value['mr_branch_code']." - " .$value['mr_branch_name'];
    }
    $log_data  = array();
    foreach($log  as $key => $l){
        $st = $l['mr_status_id'];
        switch ($st) {
            case 1:  $l['mr_status_id']     = 1 ;   break;
            case 2:  $l['mr_status_id']     = 2 ;   break;
            case 3:  $l['mr_status_id']     = 3 ;   break;
            case 4:  $l['mr_status_id']     = 4 ;   break;
            case 5:  $l['mr_status_id']     = 5 ;   break;
            case 6:  $l['mr_status_id']     = 6 ;   break;
            case 7:  $l['mr_status_id']     = 7 ;   break;
            case 8:  $l['mr_status_id']     = 8 ;   break;
            case 9:  $l['mr_status_id']     = 9 ;   break;
            case 10: $l['mr_status_id']     = 3 ;   break;
            case 11: $l['mr_status_id']     = 11 ;  break;
            case 12: $l['mr_status_id']     = 5 ;   break;
            case 13: $l['mr_status_id']     = 13 ;  break;
            case 14: $l['mr_status_id']     = 14 ;  break;
            case 15: $l['mr_status_id']     = 15 ;  break;
            default:
            $l['mr_status_id']     = 15 ;
        }
    
        $log_data[$key] = $l; 
    }
    $log_status = array();
    $new_status_arr = array();
    $run_status_txt = '';
    $run_status_arr = array();
    foreach($status_data as $key => $val) {
        $new_status_arr[$val['mr_status_id']] = $val;
    }
    
    if($data['mr_type_work_id']==1){//  สนญ - สนญ
            $run_status_txt = '1,2,3,4,5';
            $run_status_arr = explode(',',$run_status_txt);
    }elseif($data['mr_type_work_id']== 2 and $data['mr_user_role_id']== 2 ){ // สนญ - ส่งสาขา
        //1,2,3,10,11,13,5,12
        $run_status_txt = '1,2,3,11,13,5';
        $run_status_arr = explode(',',$run_status_txt);
    }elseif($data['mr_type_work_id']== 3 and $data['mr_user_role_id']==5){ // สาขา - สนญ
        //7,8,9,14,10,3,4,'5,12'
        $run_status_txt = '7,8,9,14,3,4,5';
        $run_status_arr = explode(',',$run_status_txt);
    }else{//สาขา - สาขา
        //7,8,9,14,10,3,11,13,5,12
        $run_status_txt = '7,8,9,14,3,11,13,5';
        $run_status_arr = explode(',',$run_status_txt);
    }
    
    $date_start=0;
    $date_end='';
    $end_step = '';
    foreach($run_status_arr as $key => $val2) {
       $val=$new_status_arr[$val2];
        foreach($log_data as $k => $v) {
            //$log_status[$key]['status_id'] = $val['mr_status_id'];
            //$log_status[$key]['name'] = $val['mr_status_name'];
            $date_end = $key;
            if($val['mr_status_id'] == $v['mr_status_id']) {
                $log_status[$key]['status_id'] = $val['mr_status_id'];
                $log_status[$key]['name'] = $val['mr_status_name'];
                $log_status[$key]['active'] = 'is-complete';
                $log_status[$key]['emp_code'] = $v['mr_emp_code'];
                $log_status[$key]['sys_timestamp'] = $v['sys_timestamp'];
                $log_status[$key]['icon'] = '<i class="material-icons">done</i>';
                $log_status[$key]['key_1'] = "";
                $log_status[$key]['key_2'] = "";
                
                
                if($key!=0){
                    $dt1 = $log_status[$date_start]['sys_timestamp'];
                    $dt2 = $log_status[$date_end]['sys_timestamp'];
                    $log_status[$key]['key_1'] = $date_start; 
                    $log_status[$key]['key_2'] =  $date_end;
                    $arr = json_decode(json_encode(datetimeDiff($dt1, $dt2)), True);
                    $d=($arr['day']!=0)?$arr['day'].' วัน  ':''; 
                    $h=($arr['hour']!=0)?$arr['hour'].' ชั่วโมง  ':''; 
                    $i=($arr['min']!=0)?$arr['min'].' นาที ':''; 
                    $s=($arr['sec']!=0)?$arr['sec'].' วินาที  ':''; 
                    $log_status[$key]['date'] =  $d.$h.$i.$s;
                    
                    $end_step =	$key+1;
                }
                $date_start=$date_end;	
                break;
            } else{
                $log_status[$key]['status_id'] = $val['mr_status_id'];
                $log_status[$key]['name'] = $val['mr_status_name'];
                $log_status[$key]['active'] = '';
                $log_status[$key]['emp_code'] = '';
                $log_status[$key]['sys_timestamp'] = $v['sys_timestamp'];
                $log_status[$key]['icon'] = '<i class="material-icons">done</i>';
                $log_status[$key]['icon'] = '';
                $log_status[$key]['key'] = '';
                //break;
            } 
        }
    }
    //$end_step;
    $received = "real";
    if(!empty($data['real_receive_id'])) {
        if($data['receive_id'] == $data['real_receive_id']) {
            $received = "real";
        } else {
            $received = "replace";
        }
    }
}

// $dt1="2019-01-01 08:00:35";

// $dt2="2019-01-02 08:00:45";

function datetimeDiff($dt1, $dt2){
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);

        $dtd = new stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2-$t1);
        $dtd->total_min = floor($dtd->total_sec/60);
        $dtd->total_hour = floor($dtd->total_min/60);
        $dtd->total_day = floor($dtd->total_hour/24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour -($dtd->total_day*24);
        $dtd->min = $dtd->total_min -($dtd->total_hour*60);
        $dtd->sec = $dtd->total_sec -($dtd->total_min*60);
        return $dtd;
}
	
$template = Pivot_Template::factory('branch/work_info.tpl');
$template->display(array(
    'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles(),
    'user_data' => $user_data,
    'user_data_show' => $user_data_show,
    'serverPath' => $_CONFIG->site->serverPath,
    'floor_data' => $floor_data,
    'branch_data' => $branch_data,
    'end_step' => $end_step,
    'data' => $data,
    'status' => $log_status,
    'wid' => $id,
    'received' => $received,
    'alertt' => $alertt
));
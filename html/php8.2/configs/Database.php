<?php

return [
    'adapter' => 'pdo_mysql',
    'params'  => [
        'host'     => $_ENV['MYSQL_HOST'],
        'dbname'   => $_ENV['MYSQL_DATABASE'],
        'username' => $_ENV['MYSQL_USERNAME'] ,
        'password' => $_ENV['MYSQL_PASSWORD'],
        'port'     => $_ENV['MYSQL_PORT'],
        'charset'  => 'utf8',
    ],
];

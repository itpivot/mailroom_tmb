<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();


if( $iPod || $iPhone || $Android ){
	$template = Pivot_Template::factory('mobile/data/list.tpl');
	//$template = Pivot_Template::factory('data/list.tpl');
}else{
	//$template = Pivot_Template::factory('mobile/data/list.tpl');
	$template = Pivot_Template::factory('data/list.tpl');
}

$template = Pivot_Template::factory('data/list.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'userRoles' => $userRoles,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);

$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();

$employeedata   = array();

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array());
   exit();
} else {
    $emp_id = $req->get('employee_id');

    if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
        echo json_encode(array());
        exit();
    }

    $employeedata = $employeeDao->getemployeeByuserid($emp_id);
    echo json_encode($employeedata);
}


// echo "";
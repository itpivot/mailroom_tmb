<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);


$req 				= new Pivot_Request();
$employeeDao 		= new Dao_Employee();




/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    // Pivot_Site::toLoginPage();
    echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
    exit();
    // return error 
} else {
    $pass_emp		=  $req->get('pass_emp');
    $emp_data           = array();

    try {
        if(preg_match('/<\/?[^>]+(>|$)/', $pass_emp)) {
            throw new Exception('Internal Server Error.');
        }
        
        $emp_data = $employeeDao->getEmp_code($pass_emp);

        echo json_encode(array(
            'status' => 200,
            'data' =>  $emp_data
        ));
    } catch (Exception $e) {
        echo json_encode(array('status' => 500, 'message' => 'Internal Server Error.'));
    }

    // echo "<pre>".print_r($emp_data, true)."</pre>";
    // echo json_encode(array(
    //     'status' => 200,
    //     'data' =>  $emp_data
    // ));
    
}


?>
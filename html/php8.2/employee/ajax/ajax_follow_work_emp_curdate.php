<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
error_reporting(E_ALL & ~E_NOTICE);


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array());
    exit();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();

$txt            = $req->get('txt');
$sendData       = array();

$user_id        = $auth->getUser();

if(!empty($txt)) {
    if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
        echo json_encode(array());
        exit();
    }
}

$sendData = $work_inout_Dao->searchFollowCurdate($user_id ,$txt);

//echo print_r($sendData);
//echo print_r($sendData);
//exit();
echo json_encode($sendData);
?>
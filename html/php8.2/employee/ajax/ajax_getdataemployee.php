<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);

$employeeDao 	= new Dao_Employee();
$req 			= new Pivot_Request();

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    // Pivot_Site::toLoginPage();
    echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
    exit();
    // return error 
}

$employeedata   = array();
$emp_id = $req->get('employee_id');
 
try {
    if(preg_match('/<\/?[^>]+(>|$)/', $emp_id)) {
        throw new Exception('Internal Server Error.');
    }

    $employeedata = $employeeDao->getemployeeByuserid($emp_id);

    echo json_encode(array(
        'status' => 200,
        'data' =>  $employeedata
    ));
} catch (Exception $e) {
    echo json_encode(array('status' => 500, 'message' => 'Internal Server Error.'));
}
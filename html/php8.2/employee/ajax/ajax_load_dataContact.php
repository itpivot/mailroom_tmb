<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
ini_set('max_execution_time', 300);
error_reporting(E_ALL & ~E_NOTICE);



$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
	exit();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();


$type = $req->get('type');
//echo $type;
$table_name = '';
$_secret = $userDao->getEncryptKey();
// echo print_r($_secret,true);
//exit;
try {
	if(preg_match('/<\/?[^>]+(>|$)/', $type)) {
        throw new Exception('Internal Server Error.');
    }
	
	
	if($type == 1){
		$table_name = 'mr_contact';
	
		$sql = "
			SELECT 
				c.* ,
				AES_DECRYPT(e.mr_emp_tel, '".$_secret->database->key."') as emp_telemp_tel,
				p.mr_position_name
			FROM mr_contact c
				left join mr_emp e on(c.emp_code = e.mr_emp_code)
				left join mr_position p on(p.mr_position_id = e.mr_position_id)
			WHERE 
			acctive = 1
			and e.mr_emp_code != 'Resign'
			and e.mr_emp_code !=''
			and e.mr_emp_code is not null
			and c.type = 1
		";
	
	}elseif($type == 3){//รายชื่อตามหน่อยงงาน
		$table_name = 'mr_emp';
		$sql = "
			SELECT 
				e.mr_emp_code as emp_code,
				p.mr_position_name,
				d.mr_department_name as department_name,
				d.mr_department_code as department_code,
				f.name  as floor,
				AES_DECRYPT(e.mr_emp_tel, '".$_secret->database->key."') as emp_telemp_tel,
				CONCAT(AES_DECRYPT(e.mr_emp_name, '".$_secret->database->key."'),'  ',AES_DECRYPT(e.mr_emp_lastname, '".$_secret->database->key."')) as mr_contact_name
			FROM mr_emp e
				left join mr_floor f on(f.mr_floor_id = e.mr_floor_id)
				left join mr_department d on(d.mr_department_id = e.mr_department_id)
				left join mr_position p on(p.mr_position_id = e.mr_position_id)
			WHERE
				e.mr_department_id is not null
				and d.mr_department_id != ''
				and e.mr_department_id != 0
				and e.mr_emp_code != 'Resign'
				and e.mr_emp_code !=''
				and e.mr_emp_code is not null
				and e.mr_branch_id is not null 
				and e.mr_branch_id = 1
			";
	}elseif($type == 2){// ค้นหาตามสาขา
		$table_name = 'mr_emp';
	
		$sql = "
			SELECT 
				e.mr_emp_code as emp_code,
				p.mr_position_name,
				b.mr_branch_name as department_name,
				b.mr_branch_code as department_code,
				e.mr_branch_floor  as floor,
				AES_DECRYPT(e.mr_emp_tel, '".$_secret->database->key."') as emp_telemp_tel,
				CONCAT(AES_DECRYPT(e.mr_emp_name, '".$_secret->database->key."'),'  ',AES_DECRYPT(e.mr_emp_lastname, '".$_secret->database->key."')) as mr_contact_name
			FROM mr_emp e
				left join mr_floor f on(f.mr_floor_id = e.mr_floor_id)
				left join mr_branch b on(b.mr_branch_id = e.mr_branch_id)
				left join mr_department d on(d.mr_department_id = e.mr_department_id)
				left join mr_position p on(p.mr_position_id = e.mr_position_id)
			WHERE 
				e.mr_branch_id is not null
				and b.mr_branch_id != ''
				and e.mr_branch_id != 0
				and e.mr_branch_id != 1
				and e.mr_emp_code != 'Resign'
		";
	
	}elseif($type == 4){
		$table_name = 'mr_contact';
		$sql = "
			SELECT c.* ,
				p.mr_position_name,
				AES_DECRYPT(e.mr_emp_tel, '".$_secret->database->key."') as emp_telemp_tel
			FROM mr_contact c
				left join mr_emp e on(c.emp_code = e.mr_emp_code)
				left join mr_position p on(p.mr_position_id = e.mr_position_id)
			WHERE `acctive` = 1
				and e.mr_emp_code != 'Resign'
				and e.mr_emp_code !=''
				and e.mr_emp_code is not null
				and c.type = 2
		";
	}
	
	//echo $sql;;
	//exit;
	$contactdata = array();
	$datatable = array();
	
	if(!empty($sql)){
		$dao 			= new Pivot_Dao($table_name);
		$dao_db 		= $dao->getDb();
	
		$stmt = new Zend_Db_Statement_Pdo($dao_db, $sql);
		$stmt->execute();
		
		$contactdata = $stmt->fetchAll();
	}
	//$users = $userDao->fetchAll();
	//echo json_encode(array('status' => 500, 'message' => 'Internal Server Error.','data'=>$contactdata));
	//exit;
	if(count($contactdata) > 0) {
		foreach($contactdata as $i => $val){
			
			if(isset($val['mr_contact_id'])){
				if($val['mr_contact_id']!=''){
					$mr_contact_id = $val['mr_contact_id'];
				}else{
					$mr_contact_id = null;
				}
			}else{
				$mr_contact_id = null;
			}
			
			$datatable[$i]['no']					= '<button type="button" class="btn btn-link" onclick="getemp(\''.$val['emp_code'].'\')">เลือก</button>'      ;
			$datatable[$i]['no2']					= '<button type="button" class="btn btn-link" onclick="getemp(\''.$val['emp_code'].'\'); $(\'#mr_contact_id\').val('.$mr_contact_id.');">เลือก</button>'      ;
			$datatable[$i]['department']			= $val['department_code'].' : '.$val['department_name']                                                ;
			$datatable[$i]['floor'] 				= $val['floor']                                                                                        ;
			$datatable[$i]['mr_contact_name']       = $val['mr_contact_name']                                                                              ;
			$datatable[$i]['emp_code']              = $val['emp_code']                                                                                     ;
			$datatable[$i]['emp_tel']               = $val['emp_telemp_tel']                                                                               ;
			$datatable[$i]['mr_position_name']      = $val['mr_position_name']                                                                             ;
			$datatable[$i]['remark']                = isset($val['remark'])?$val['remark']:''                                                              ;
		}
	}
	
	echo json_encode(array(
        'status' => 200,
        'data' =>  $datatable
    ));
} catch(Exception $e) {
	echo json_encode(array('status' => 500, 'message' => 'Internal Server Error.'));
}


 ?>
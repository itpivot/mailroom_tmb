<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');
error_reporting(E_ALL & ~E_NOTICE);

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'Access Denied.'));
	exit();
}

$req 				= new Pivot_Request();                             
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
   
$user_id= 	$auth->getUser();
$role_id = 	$auth->getRole();
$emp_data = $userDao->getEmpDataByuserid( $user_id );    

  
$data_search['name_send_select']  		=  $req->get('name_send_select');                           
$data_search['send_branch_id']  		=  $req->get('send_branch_id');                           
$data_search['name_receiver_select']  	=  $req->get('name_receiver_select');     
$data_search['receiver_branch_id']  	=  $req->get('receiver_branch_id');                     
$data_search['barcode']  				=  $req->get('barcode');                                 
$data_search['start_date']  			=  $req->get('start_date'); 
$data_search['end_date']  				=  $req->get('end_date'); 

$check =  0;
foreach($data_search as $key => $val){
	if($val != ''){
		$check += 1;
	}
}

if($check == 0){
	$data_search['start_date'] 	= date('Y-m-d 00:00:00');
	$data_search['end_date'] 	= date('Y-m-d 23:59:59');
}

 
// echo print_r($data_search);
// exit();
//echo "<pre>".print_r($data_search,true)."</pre>"; 

// echo json_encode($data_search);
// exit();		
//
$report = $work_inoutDao->searchBranch_manege( $data_search );


foreach( $report as $num => $val ){

	$mr_user_role_id 	= $val['mr_user_role_id'];
	$mr_type_work_id 	= $val['mr_type_work_id'];
	$mr_user_id_get 	= $val['mr_user_id_get'];
// ที่อยู่ผู้รับ
	if($mr_type_work_id == 2){
		$address_re  = $val['mr_branch_code_re'].' : '.$val['mr_branch_name_re']."  ชั้น ".$val['mr_branch_floor_re'];
	}else{
		$address_re  = $val['depart_code_receive'].' : '.$val['depart_name_receive']."  ชั้น ".$val['depart_floor_receive'];
	}
// ที่อยู่ผู้ส่ง
	if($mr_user_role_id == 5 ){
		$address_send  = $val['mr_branch_code_sen'].' : '.$val['mr_branch_name_sen'];
	}else{
		$address_send  = $val['mr_department_code'].' : '.$val['mr_department_name']."  ชั้น ".$val['depart_floor_send'];
	}
//ผู้ลงชื่อรับ
	if($mr_user_id_get == 0 and ($val['mr_status_id'] == 5 or $val['mr_status_id'] == 12 )){
		$sql = "
			SELECT 
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname
				FROM mr_confirm_log  log
				left join mr_emp e on ( e.mr_emp_id = log.mr_emp_id )
				WHERE mr_work_main_id = ".$val['mr_work_main_id']."

		"; 
		//$mr_emp = $userDao->select($sql);
		if(!empty($mr_emp)){
			$emp_getwork =  $mr_emp[0]['mr_emp_code'].' : '.$val[0]['mr_emp_name']." ".$val[0]['mr_emp_lastname'];
		}else{
			$emp_getwork = '';
		}
		
	}else{
		$emp_getwork =  $val['emp_code_get'].' : '.$val['emp_name_get'].' '.$val['emp_lastname_get'];
	}

//วันที่สำเร็จ
	if($val['mr_work_date_success'] == ''){
		if($val['time_log'] != ''){
			$date_succ = date("Y-m-d",strtotime($val['time_log']));
		}else{
			$date_succ = '';	
		}
	}else{
		$date_succ = $val['mr_work_date_success'];
	}

	if($val['mr_user_role_id'] == 5){
		if($val['branch_type_name'] != ''){
			$branch_type_name = $val['branch_type_name'];
		}else{
			$branch_type_name = "สำนักงานใหญ่";
		}
	}else{
		$branch_type_name = "สำนักงานใหญ่";
	}

// barcode setting
	if($role_id == 4){
		$mr_work_barcode = $val['link_click'];
	}else{
		$mr_work_barcode = $val['mr_work_barcode'];
	}


	$report[$num]['no'] = $num +1;
	$report[$num]['name_receive'] 			= $val['name_re']." ".$val['lastname_re'];
	$report[$num]['name_send'] 				= $val['mr_emp_name']." ".$val['mr_emp_lastname'];
	$report[$num]['emp_getwork'] 			= $val['emp_getwork'];
	$report[$num]['mr_work_barcode'] 	= $mr_work_barcode;
	$report[$num]['mr_work_date_success'] 	= $date_succ;
	$report[$num]['ch_data'] = '';
}


$arr['data'] = $report;

echo json_encode($arr);
 ?>

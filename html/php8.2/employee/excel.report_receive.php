<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
//require_once 'PHPExcel.php';
// include_once('xlsxwriter.class.php');
require_once('PhpSpreadsheet-master/vendor/autoload.php');
// error_reporting(E_ALL & ~E_NOTICE);
// ob_start();

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout(); 
$userDao 			= new Dao_User();                                
 


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Table;
use PhpOffice\PhpSpreadsheet\Worksheet\Table\TableStyle;
$spreadsheet = new Spreadsheet();


$month				=	$req->get('month');
$year				=	$req->get('year');

$month				= base64_decode(urldecode($month));
$year 				= base64_decode(urldecode($year));

$user_id			= 	$auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$date 				= $year.'-'.$month;                                                 
   
$data = $work_inoutDao->reportEmpMonthReceive( $date , $user_data['mr_emp_id'] );

function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = explode("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}


$ex_data = array();
$col 		= range('A', 'Z');
$columnNames = array(
		'No.',                           
		'Barcode',                            
		'ชื่อผู้รับ',                            
		'ที่อยู่',                            
		'ชื่อผู้ส่ง',                            
		'ที่อยู่',    
		'ชื่อเอกสาร',
		'วันที่ส่ง',                            
		'วันที่สำเร็จ',                            
		'สถานะงาน',                            
		'ประเภทการส่ง',                            
		'หมายเหตุ'     
);

// Add some data columnNames
$row=1;
$sheet       = $spreadsheet->getActiveSheet(0);
foreach($columnNames as $in_x=>$txt_X){
	$sheet->setCellValue($col[$in_x].$row,$txt_X)->getStyle($col[$in_x].$row)
	->getAlignment()
	->setHorizontal(PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
	$sheet->getStyle($col[$in_x].$row)->getFont()->setBold(true);
	$sheet->getStyle($col[$in_x].$row)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $sheet->getStyle($col[$in_x].$row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
    $sheet->getStyle($col[$in_x].$row)->getFill()->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK);
    $sheet->getStyle($col[$in_x].$row)->getBorders()->getAllBorders()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

	
}

foreach($data as $keys => $vals) {
			if( $vals['mr_work_date_sent'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_sent']);
				$date_order[$keys]['mr_work_date_sent'] = $d."/".$m."/".$y;
			}	
			
			if( $vals['mr_work_date_success'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_success']);
				$date_meet[$keys]['mr_work_date_success'] = $d."/".$m."/".$y;
			}else{
				$date_meet[$keys]['mr_work_date_success'] = null;
			}	
			
			if(  $vals['name_re'] ){
				$name_receive[$keys]['name_re'] 				= $vals['name_re']." ".$vals['lastname_re'];
				$name_receive[$keys]['depart_receive'] 		= $vals['depart_code_receive']." - ".$vals['depart_name_receive']." ชั้น ".$vals['depart_floor_receive'];
			}			
			
			if(  $vals['mr_emp_name'] ){
				$name_send[$keys]['send_name'] 				= $vals['mr_emp_name']." ".$vals['mr_emp_lastname'];
				$name_send[$keys]['depart_send'] 				= $vals['mr_department_code']." - ".$vals['mr_department_name']." ชั้น ".$vals['depart_floor_send'];
				
			}		
			$ex_data[$keys] = array(
				($keys+1),
				isset($vals['mr_work_barcode'])?$vals['mr_work_barcode']:'-',
				isset($name_receive[$keys]['name_re'])?$name_receive[$keys]['name_re']:'-',
				isset($name_receive[$keys]['depart_receive'])?$name_receive[$keys]['depart_receive']:'-',
				isset($name_send[$keys]['send_name'])?$name_send[$keys]['send_name']:'-',
				isset($name_send[$keys]['depart_send'])?$name_send[$keys]['depart_send']:'-',
				isset($vals['mr_topic'])?$vals['mr_topic']:'-',
				isset($date_order[$keys]['mr_work_date_sent'])?$date_order[$keys]['mr_work_date_sent']:'-',
				isset($date_meet[$keys]['mr_work_date_success'])?$date_meet[$keys]['mr_work_date_success']:'-',
				isset($vals['mr_status_name'])?$vals['mr_status_name']:'-',
				isset($vals['mr_type_work_name'])?$vals['mr_type_work_name']:'-',
				isset($vals['mr_work_remark'])?$vals['mr_work_remark']:'-',
			);
}

$nickname = "report_receive";
$filename = $nickname.'.xlsx';
// echo "<pre>".print_r($ex_data,true);
// exit;

$row++;
foreach($ex_data as $in_i=>$txt_i){
	foreach($txt_i as $in_j=>$txt_j){
		$sheet->setCellValue($col[$in_j].$row,$txt_j)->getStyle($col[$in_j].$row)->getAlignment()->setHorizontal(PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle($col[$in_j].$row,$txt_j)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle($col[$in_j].$row,$txt_j)->getBorders()->getAllBorders()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
	}
	$row++;
}



$nickname = "report_send";
$filename = $nickname.'.xlsx';
// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('report_send');
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);
 

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
 
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
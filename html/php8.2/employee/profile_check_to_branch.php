<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
error_reporting(E_ALL & ~E_NOTICE);



/* Check authentication */
$auth = new Pivot_Auth();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();




//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
$department_data = $departmentDao->fetchAll();
$floor_data = $floorDao->fetchAll();
$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuseridProfile($user_id);


//echo "<pre>".print_r($user_data,true)."</pre>";


$template = Pivot_Template::factory('employee/profile_check.tpl');
$template->display(array(
	//'debug' => print_r($path_pdf,true),
	//'userRoles' => $userRoles,
	'success' => $success,
	'department_data' => $department_data,
	//'userRoles' => $userRoles,
	//'users' => $users,
	'user_data' => $user_data,
	'role_id' => $auth->getRole(),
	'floor_data' => $floor_data,
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
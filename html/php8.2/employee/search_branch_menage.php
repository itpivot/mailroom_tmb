<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao		= new Dao_UserRole();
$statusDao 			= new Dao_Status();
$employeeDao 		= new Dao_Employee();

$status_data = $statusDao->getStatusall();
$status_data2 = $statusDao->getStatusall2();
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);


$user_id= $auth->getUser();
$sql = "SELECT * FROM `mr_branch` ORDER BY `mr_branch`.`mr_branch_name` ASC"; 
$mr_branch = $userDao->select($sql);

// echo $user_id."<br>";

//echo "<pre>".print_r($mr_branch)."</pre>";

// exit();

$template = Pivot_Template::factory('employee/search_branch_menage.tpl');
$template->display(array(
	//'debug' => print_r($status_data,true),
	'mr_branch' => $mr_branch,
	'status_data' => $status_data,
	'status_data2' => $status_data2,
	//'userRoles' => $userRoles,
	'success' => $success,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	'user_arr' => json_encode($user_data),
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Floor.php';
require_once 'nocsrf.php';
require_once 'Dao/Send_work.php';
error_reporting(E_ALL & ~E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();

if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}


$req 				= new Pivot_Request();
$userDao 			= new Dao_User();
$userRoleDao 		= new Dao_UserRole();
$departmentDao		= new Dao_Department();
$employeeDao 		= new Dao_Employee();
$floorDao 			= new Dao_Floor();
$send_workDao		= new Dao_Send_work();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
//$floor_data = $floorDao->fetchAll();
$floor_data  = $send_workDao->select('SELECT f.* FROM `mr_floor` f
left join `mr_building` as b on(b.mr_building_id = f.mr_building_id)
where b.mr_branch_id = 1 ORDER BY cast(f.floor_level as unsigned),name ASC');

$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$time = date("H:i:s");
$time_check = strtotime('4.00pm');
$time_16 = date("H:i:s",$time_check);

if( $time >= $time_16 ){
	$tomorrow = 1;
}else{
	$tomorrow = 0;
}	


$barcode 			= "TM".date("dmy");


//$emp_data = $employeeDao->getEmpDataSelect();

//echo "<pre>".print_r($tomorrow,true)."</pre>";


$template = Pivot_Template::factory('employee/work_in.tpl');
$template->display(array(
	//'debug' => print_r($path_pdf,true),
	//'userRoles' => $userRoles,
	//'success' => $success,
	'barcode' => $barcode,
	'floor_data' => $floor_data,
	//'userRoles' => $userRoles,
	//'emp_data' => $emp_data,
	//'users' => $users,
	'tomorrow' => $tomorrow,
	'user_data' => $user_data,
	'role_id' => $auth->getRole(),
	'csrf' 				=>  NoCSRF::generate( 'csrf_token'),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));
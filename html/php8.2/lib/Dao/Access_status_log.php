<?php
require_once 'Pivot/Dao.php';


class Dao_Access_status_log extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_access_status_log');
    }
	
	function fetchAccess_logSucc(  $user_id  )
	{
		$params = array();
		$sql =
		'
			SELECT 
				sys_timestamp
			FROM mr_access_status_log
			
			WHERE
				mr_user_id = ? AND status = "Success" AND description = "Login Success"
			Order By sys_timestamp DESC
			Limit 1
		';
		if(!empty($user_id)) {
			array_push($params, (int)$user_id);
		}
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}
	
	
	function fetchAccess_logFail(  $user_id  )
	{
		$params = array();
		$sql =
		'
			SELECT 
				sys_timestamp
			FROM mr_access_status_log
			
			WHERE
				mr_user_id = ? AND status = "Failed" AND description = "Duplicate Login Account"
			Order By sys_timestamp DESC
			Limit 1
		';
		if(!empty($user_id)) {
			array_push($params, (int)$user_id);
		}
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}


	function fetch_logSucc(  $user_id  )
	{
		$params = array();
		$sql =
		'
			SELECT 
				sys_timestamp
			FROM mr_access_status_log
			
			WHERE
				mr_user_id = ?
				and status like "%Success%"
			Order By sys_timestamp DESC
			Limit 1
		';

		if(!empty($user_id)) {
			array_push($params, (int)$user_id);
		}
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}
	
	function fetchAccess_logs($data)
	{
		$params = array();
		$now = date('Y-m-d');
		$sql =
			'
			select
				lg.mr_access_status_log_id,
				usr.mr_user_id,
				usr.mr_user_username,
				emp.mr_emp_name,
				emp.mr_emp_lastname,
				lg.sys_timestamp,
				lg.activity,
				lg.terminal,
				lg.status,
				lg.description
			from mr_access_status_log as lg
			left join mr_user as usr on (usr.mr_user_id = lg.mr_user_id)
			left join mr_emp as emp on (emp.mr_emp_id = usr.mr_emp_id)
			where
				usr.mr_user_id is not null
		';
		if ($data['status'] != "0") {
			$sql .= ' and lg.status = ?';
			array_push($params, (string)$data['status']);
		}

		if($data['activity'] != "0") {
			$sql .= ' and lg.activity = ?';
			array_push($params, (string)$data['activity']);
		}

		if($data['start_date'] != "") {
			$sql .= ' and ( lg.sys_timestamp between ? and ?  )';
			array_push($params, (string)$data['start_date'].'00:00:00');
			array_push($params, (string)$data['end_date'].'23:59:59');
		} else {
			$sql .= ' and lg.sys_timestamp like ?';
			array_push($params, (string)$now.'%');
		}
		
		//$sql .= "  group by lg.mr_user_id order by lg.mr_user_id desc";
		$sql .= "  limit 0,10000";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}
	
	

}
<?php
require_once 'Pivot/Dao.php';


class Dao_Contact extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_contact');
    }
	function getcontactByID($id)
    {
		$params = array();
		
        $sql = '
			SELECT * 
			FROM ' . $this->_tableName . ' 
			WHERE 
				mr_contact_id = ?
		';        

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

		array_push($params, (int)$id);

        $stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
	}
	
	function select($sql)
    {
		if( $sql!= '' ){
			return $this->_db->fetchAll($sql);
		}else{
			return 'NO SQL';
		}
	}
	
}
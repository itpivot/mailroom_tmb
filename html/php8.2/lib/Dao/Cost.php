<?php
require_once 'Pivot/Dao.php';


class Dao_Cost extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_cost');
    }

	
	
	function fetchCostByCostCode( $cost_code )
	{
		$params = array();
		$sql =
		'
			SELECT 
				mr_cost_id
			FROM mr_cost
			WHERE
				mr_cost_code = ? 
		';

		array_push($params, (string)$cost_code);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}
   	
}
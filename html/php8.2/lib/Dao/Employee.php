<?php
require_once 'Pivot/Dao.php';


class Dao_Employee extends Pivot_Dao
{   
	private  $_secret;
    function __construct()
    {
        parent::__construct('mr_emp');
		$this->_secret = $this->getEncryptKey();
    }
	
	function getEmployeeid($code)
    {
		$params = array();
		
        $sql = '
			SELECT 
				mr_emp_id as id 
			FROM ' . $this->_tableName . ' 
			WHERE code = ?
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

		array_push($params, trim($code));
        //echo  "<br><br><br><br><br><br>".$sql ;
        $id = $stmt->fetchAll();
        return $id[0]['id'];
	}	
	
	
	
	function getEmp_code($mr_emp_code)
    {
		$params = array();

        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_code,
					e.mr_branch_id,
					AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
					AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
					AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
					AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,

					d.mr_department_code ,
					fl.name as mr_department_floor,
					e.mr_floor_id,
					b.mr_branch_name,
					b.mr_branch_code,
					d.mr_department_name,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				left join mr_floor fl on ( fl.mr_floor_id = e.mr_floor_id)
				WHERE e.mr_emp_code = ?
		';

			// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
		array_push($params, trim($mr_emp_code));
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetch();
		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	
	function getEmpByID($mr_emp_id)
    {
		$params = array();
		
        $sql = 'SELECT 
					e.mr_emp_id,
					e.sys_time,
					e.update_date,
					e.mr_emp_code,
					e.old_emp_code,
					e.mr_department_id,
					e.mr_position_id,
					e.mr_floor_id,
					e.mr_cost_id,
					e.mr_branch_id,
					e.mr_branch_floor,
					e.mr_date_import,
					e.mr_workplace,
					e.mr_workarea,
					e.mr_hub_id,
					e.emp_type,
					AES_DECRYPT(mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
					AES_DECRYPT(mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
					AES_DECRYPT(mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
					AES_DECRYPT(mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
					d.mr_department_id ,
					d.mr_department_code ,
					f.name as mr_department_floor,
					d.mr_department_name,
					b.mr_branch_name,
					b.mr_branch_code,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_user u on ( e.mr_emp_id = e.mr_emp_id)
				WHERE e.mr_emp_id = ?
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		 
		array_push($params, (int)$mr_emp_id);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();


		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	
	function getEmpcontact($mr_emp_id)
    {
		$params = array();
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_tel,
					e.mr_branch_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_name,
					b.mr_branch_name,
					b.mr_branch_code,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				WHERE e.mr_emp_id = ? 
		';

		array_push($params, (int)$mr_emp_id);
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	// MARCH MARCH MARCH 
	function getEmpData($data)
    {
		$params = array();
        $sql = 'SELECT 
					e.*,
					d.*,
					u.mr_user_id as u_id
				FROM mr_emp e
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				WHERE e.mr_emp_code = ?
					AND e.mr_emp_name = ?
					AND e.mr_emp_lastname = ?
		';

		array_push($params, (string)$data['mr_emp_code']);
		array_push($params, (string)$data['mr_emp_name']);
		array_push($params, (string)$data['mr_emp_lastname']);

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	
	function getEmpDataSelect( )
    {
        $sql = 'SELECT *
				FROM mr_emp e
				WHERE e.mr_emp_code IS NOT NULL 
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}


	function getEmpDataWithTXT($txt)
	{
		$explode_txt =	explode(" ", $txt);
		$arr_txt =	array();
		foreach($explode_txt as $v){
			if(trim($v) != ''){
				$arr_txt[] = $v;
			}
		}
		$params = array();
		if($txt == ''){
			$sql =
			"SELECT 
				mr_emp_id,
				sys_time,
				update_date,
				mr_emp_code,
				old_emp_code,
				mr_department_id,
				mr_position_id,
				mr_floor_id,
				mr_cost_id,
				mr_branch_id,
				mr_branch_floor,
				mr_date_import,
				mr_workplace,
				mr_workarea,
				mr_hub_id,
				emp_type,
				AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') as mr_emp_name,
				AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') as mr_emp_lastname,
				AES_DECRYPT(mr_emp_tel, '".$this->_secret->database->key."') as mr_emp_tel,
				AES_DECRYPT(mr_emp_email, '".$this->_secret->database->key."') as mr_emp_email
			 FROM mr_emp 
			WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
			ORDER BY mr_emp_id DESC limit 10 ";
		}else{
				$sql =
				"
				SELECT 
					mr_emp_id,
					sys_time,
					update_date,
					mr_emp_code,
					old_emp_code,
					mr_department_id,
					mr_position_id,
					mr_floor_id,
					mr_cost_id,
					mr_branch_id,
					mr_branch_floor,
					mr_date_import,
					mr_workplace,
					mr_workarea,
					mr_hub_id,
					emp_type,
					AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') as mr_emp_name,
					AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') as mr_emp_lastname,
					AES_DECRYPT(mr_emp_tel, '".$this->_secret->database->key."') as mr_emp_tel,
					AES_DECRYPT(mr_emp_email, '".$this->_secret->database->key."') as mr_emp_email
				FROM mr_emp
				WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
				";
				if(count($arr_txt)>1){
					if(count($arr_txt)==2){
						$sql .= " and(
							AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') like ? and
							AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') like ?
						)";
						array_push($params, "%".trim($arr_txt[0])."%");
						array_push($params, "%".trim($arr_txt[1])."%");
					}else{
						$sql .= " and(
							mr_emp_code like ? and
							AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') like ? and
							AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') like ?
						)";
						array_push($params, "%".trim($arr_txt[0])."%");
						array_push($params, "%".trim($arr_txt[1])."%");
						array_push($params, "%".trim($arr_txt[2])."%");
					}
				}else{
					$sql .= " and(
						mr_emp_code like ? or
						AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') like ? or
						AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') like ?
					)";
					array_push($params, "%".trim($txt)."%");
					array_push($params, "%".trim($txt)."%");
					array_push($params, "%".trim($txt)."%");
				}
		
				$sql .= " limit 10 ";
			}

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();

		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getEmpDataWithTXT_fileImport($txt)
	{
		$explode_txt =	explode(" ", $txt);
		$arr_txt =	array();
		foreach($explode_txt as $v){
			if(trim($v) != ''){
				$arr_txt[] = $v;
			}
		}
		$params = array();
		if($txt == ''){
		
		$sql =
			'
			select 
					e.mr_emp_id,
					e.sys_time,
					e.update_date,
					e.mr_emp_code,
					e.old_emp_code,
					e.mr_department_id,
					e.mr_position_id,
					e.mr_floor_id,
					e.mr_cost_id,
					e.mr_branch_id,
					e.mr_branch_floor,
					e.mr_date_import,
					e.mr_workplace,
					e.mr_workarea,
					e.mr_hub_id,
					e.emp_type,
					AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
					AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
					AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
					AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
					AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'") as mr_emp_mobile,
					p.*,b.*
			from mr_emp e
			left join mr_branch b on(b.mr_branch_id = e.mr_branch_id)
			left join mr_position p on(p.mr_position_id = e.mr_position_id)
			where
			e.mr_emp_code NOT LIKE "%Resign%" and 
			e.mr_emp_code is not null limit 1000 
		';

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();

		//echo $sql;
		// return $this->_db->fetchAll($sql);
		}else{
			$sql =
			'
			select 
			e.mr_emp_id,
					e.sys_time,
					e.update_date,
					e.mr_emp_code,
					e.old_emp_code,
					e.mr_department_id,
					e.mr_position_id,
					e.mr_floor_id,
					e.mr_cost_id,
					e.mr_branch_id,
					e.mr_branch_floor,
					e.mr_date_import,
					e.mr_workplace,
					e.mr_workarea,
					e.mr_hub_id,
					e.emp_type,
					AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
					AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
					AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
					AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
					AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'") as mr_emp_mobile,
					p.*,b.*
			from mr_emp e
			left join mr_branch b on(b.mr_branch_id = e.mr_branch_id)
			left join mr_position p on(p.mr_position_id = e.mr_position_id)
			WHERE 
			mr_emp_code NOT LIKE "%Resign%" and 
			mr_emp_code is not null
		';

		if(count($arr_txt)>1){
			if(count($arr_txt)==2){
				$sql .= " and(
					AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') like ? and
					AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') like ?
				)";
				array_push($params, "%".trim($arr_txt[0])."%");
				array_push($params, "%".trim($arr_txt[1])."%");
			}else{
				$sql .= " and(
					e.mr_emp_code like ? and
					AES_DECRYPT(e.mr_emp_name, '".$this->_secret->database->key."') like ? and
					AES_DECRYPT(e.mr_emp_lastname, '".$this->_secret->database->key."') like ?
				)";
				array_push($params, "%".trim($arr_txt[0])."%");
				array_push($params, "%".trim($arr_txt[1])."%");
				array_push($params, "%".trim($arr_txt[2])."%");
			}
		}else{
			$sql .= " and(
				b.mr_branch_code  like ? or
				b.mr_branch_name  like ? or
				e.mr_emp_code like ? or
				AES_DECRYPT(e.mr_emp_name, '".$this->_secret->database->key."') like ? or
				AES_DECRYPT(e.mr_emp_lastname, '".$this->_secret->database->key."') like ?
			)";
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
		}

		$sql .= " limit 100 ";
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();

		}
	}
	
	
	function getEmpCode()
	{
		$sql =
			'
			select mr_emp_code
			from mr_emp e
			
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	function getEmpDataWithTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or 
					 e.mr_emp_code like ?
					 ) and
					 e.mr_department_id != 0 and 
					 e.mr_floor_id != 0 and 
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 10 
		';

		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function byhandgetEmpDataWithTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or 
					 e.mr_emp_code like ?
					 ) and
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 10 
		';

		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getMessDataWithTXTsearch($txt){
		$params = array();

		$sql =
			'
			select * 
			from mr_emp e
			left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or 
					 e.mr_emp_code like ?
					 ) and
					 u.mr_user_role_id = 8 and
					 u.mr_user_username is not null and 
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 10 
		';
		



		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getMessDataFullTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			SELECT 
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_department_id,
				e.mr_position_id,
				e.mr_floor_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'") as mr_emp_mobile ,
				u.mr_user_id
				';
		$sql .="FROM mr_emp e
			left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
			WHERE 
				(AES_DECRYPT(e.mr_emp_name, '".$this->_secret->database->key."') like ? or
				AES_DECRYPT(e.mr_emp_lastname, '".$this->_secret->database->key."') like ? or
				e.mr_emp_code like ?
				)
				and
				u.mr_user_role_id = 8 and
				u.mr_user_username is not null  and
				e.mr_emp_code NOT LIKE '%Resign%'
			
		";
		//echo $sql;
		//return $this->_db->fetchAll($sql);
		//exit;
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	
	
	function ImportEmp( $sql )
	{
		return $this->_db->query($sql);
		//echo $sql;
		
	}
	
	function getEmpDataResign( )
    {
        $sql = 'SELECT *
				FROM mr_emp e
				LEFT JOIN mr_user m on ( e.mr_emp_id = m.mr_emp_id )
				WHERE e.mr_emp_code IS NOT NULL AND
				m.active = 0
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}
	
	
	function updateEmpByID(  $datas, $mr_emp_code  )
	{	
		$sql = ' SELECT mr_emp_id FROM mr_emp WHERE mr_emp_code = "'.$mr_emp_code.'" ';
		$id = $this->_db->fetchOne($sql);
		if(empty($id)){
        	$this->_db->update($this->_tableName, $datas,  'mr_emp_code = "' .$mr_emp_code.'"');
		}else{
			$id = 0;
		}
		//echo $sql;
		return $id;
	}
	
	
	function checkEmpResign( $mr_emp_code )
    {
        $sql = 
		'
			SELECT 
				mr_emp_email
			FROM 
				mr_emp
			WHERE 
				mr_emp_code = "'.$mr_emp_code.'" 

		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchOne($sql);
	}
	
	
	function getEmpDataWithTXTBranch($txt)
	{

		//echo $this->_secret->database->key;
		// exit;

		$explode_txt =	explode(" ", $txt);
		$arr_txt =	array();
		foreach($explode_txt as $v){
			if(trim($v) != ''){
				$arr_txt[] = $v;
			}
		}
		$params = array();
		if($txt == ''){
			$sql =
			"SELECT 
				mr_emp_id,
				sys_time,
				update_date,
				mr_emp_code,
				old_emp_code,
				mr_department_id,
				mr_position_id,
				mr_floor_id,
				mr_cost_id,
				mr_branch_id,
				mr_branch_floor,
				mr_date_import,
				mr_workplace,
				mr_workarea,
				mr_hub_id,
				emp_type,
				AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') as mr_emp_name,
				AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') as mr_emp_lastname,
				AES_DECRYPT(mr_emp_tel, '".$this->_secret->database->key."') as mr_emp_tel,
				AES_DECRYPT(mr_emp_email, '".$this->_secret->database->key."') as mr_emp_email
			 FROM mr_emp 
			WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
			ORDER BY mr_emp_id DESC limit 10 ";
		}else{
				$sql =
				"
				SELECT 
					mr_emp_id,
					sys_time,
					update_date,
					mr_emp_code,
					old_emp_code,
					mr_department_id,
					mr_position_id,
					mr_floor_id,
					mr_cost_id,
					mr_branch_id,
					mr_branch_floor,
					mr_date_import,
					mr_workplace,
					mr_workarea,
					mr_hub_id,
					emp_type,
					AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') as mr_emp_name,
					AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') as mr_emp_lastname,
					AES_DECRYPT(mr_emp_tel, '".$this->_secret->database->key."') as mr_emp_tel,
					AES_DECRYPT(mr_emp_email, '".$this->_secret->database->key."') as mr_emp_email
				FROM mr_emp
				WHERE 
					mr_emp_code NOT LIKE '%Resign%' and 
					mr_emp_code is not null
				";
				if(count($arr_txt)>1){
					if(count($arr_txt)==2){
						$sql .= " and(
							AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') like ? and
							AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') like ?
						)";
						array_push($params, "%".trim($arr_txt[0])."%");
						array_push($params, "%".trim($arr_txt[1])."%");
					}else{
						$sql .= " and(
							mr_emp_code like ? and
							AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') like ? and
							AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') like ?
						)";
						array_push($params, "%".trim($arr_txt[0])."%");
						array_push($params, "%".trim($arr_txt[1])."%");
						array_push($params, "%".trim($arr_txt[2])."%");
					}
				}else{
					$sql .= " and(
						mr_emp_code like ? or
						AES_DECRYPT(mr_emp_name, '".$this->_secret->database->key."') like ? or
						AES_DECRYPT(mr_emp_lastname, '".$this->_secret->database->key."') like ?
					)";
					array_push($params, "%".trim($txt)."%");
					array_push($params, "%".trim($txt)."%");
					array_push($params, "%".trim($txt)."%");
				}
		
				$sql .= " limit 10 ";
		}


		//echo $sql;
		

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	
	
	
	
	function getEmpByIDBranch($mr_emp_id)
    {
		$params = array();
		
        $sql = 'SELECT 
					e.mr_emp_id,
					e.sys_time,
					e.update_date,
					e.mr_emp_code,
					e.old_emp_code,
					e.mr_department_id,
					e.mr_position_id,
					e.mr_floor_id,
					e.mr_cost_id,
					e.mr_branch_id,
					e.mr_branch_floor,
					e.mr_date_import,
					e.mr_workplace,
					e.mr_workarea,
					e.mr_hub_id,
					e.emp_type,
					AES_DECRYPT(mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
					AES_DECRYPT(mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
					AES_DECRYPT(mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
					AES_DECRYPT(mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
					d.mr_department_code ,
					f.name as mr_department_floor,
					d.mr_department_name,
					b.*
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				WHERE e.mr_emp_id = ?
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
				
		array_push($params, (int)$mr_emp_id);
		// Execute Query
		$stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
		
		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	//function getEmp_name( $name )
    //{
    //    $sql = 'SELECT e.mr_emp_id,
	//				e.mr_emp_code,
	//				e.mr_emp_name,
	//				e.mr_emp_lastname,
	//				e.mr_emp_email,
	//				d.mr_department_code ,
	//				d.mr_department_floor,
	//				d.mr_department_name
	//			FROM mr_emp e
	//			left join mr_department d on ( d.mr_department_id = e.mr_department_id)
	//			left join mr_floor f on ( f.mr_floor_id = d.mr_department_floor)
	//			WHERE e.mr_emp_name LIKE "%'.$name.'%" OR e.mr_emp_lastname LIKE "%'.$name.'%"
	//	';
	//	//echo  "<br><br><br><br><br><br>".$sql ;
	//	return $this->_db->fetchAll($sql);
	//}


	function getEmpDataById($empId)
	{
		$params = array();
		$sql =
		'
			select 	* from '.$this->_tableName.' where mr_emp_id = ?
		';

		array_push($params, (int)$empId);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}
	public function insertEmployeeEncrypt($data)
    {
        if (!empty($data)) {
            $params = [];

            $sql         = ' INSERT INTO ' . $this->_tableName;
            $data_column = array_keys($data);
            $cols        = implode(", ", $data_column);

            $temp_val = array_reduce($data_column, function ($temp, $col) {
                switch (trim($col)) {
                    case "mr_emp_name":
                    case "mr_emp_lastname":
                    case "mr_emp_tel":
                    case "mr_emp_mobile":
                    case "mr_emp_email":
                        if (!empty($temp)) {
                            $temp .= " AES_ENCRYPT(?, ?), ";
                        } else {
                            $temp = " AES_ENCRYPT(?, ?), ";
                        }
                        break;
                    default:
                        $temp .= "?, ";
                        break;
                }

                return $temp;
            }, "");
            $sql .= " (" . trim($cols) . ") VALUES ";
            $sql .= " (" . rtrim(trim($temp_val), ",") . ") ";

            foreach ($data as $key => $val) {
                switch (trim($key)) {
					case "mr_emp_name":
					case "mr_emp_lastname":
					case "mr_emp_tel":
					case "mr_emp_mobile":
					case "mr_emp_email":
                        // case "cus_address":
                        // case "cus_ref_address":
                        array_push($params, $val);
                        array_push($params, $this->_secret->database->key);
                        break;
                    default:
                        array_push($params, $val);
                        break;
                }
            }
            // prepare statement SQL
            $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
            // Execute Query
            $stmt->execute($params);

            return $this->_db->lastInsertId();
        } else {
            return '';
        }
    }
	public function updateEmployeeEncrypt($data, $id)
    {
        if (!empty($data)) {
            $params  = [];
            $encrypt = $this->_secret->database->key;

            $sql = 'UPDATE ' . $this->_tableName . ' SET ';

            $data_update = array_reduce(
                array_chunk($data, 1, true),
                function ($combine, $item) use ($encrypt) {
                    $key = key($item);
                    switch (strtolower(trim($key))) {
						case "mr_emp_name":
						case "mr_emp_lastname":
						case "mr_emp_tel":
						case "mr_emp_mobile":
						case "mr_emp_email":
                            if (!empty($item[$key])) {
                                $value = "AES_ENCRYPT('" . trim($item[$key]) . "', '" . $encrypt . "')";
                            } else {
                                $value = 'NULL';
                            }
                            break;
                        default:
                            if (!empty($item[$key])) {
                                if (preg_match('/^\d+$/', $item[$key])) {
                                    $value = $item[$key];
                                } else {
                                    $value = "'" . $item[$key] . "'";
                                }

                            } else {
                                $value = 'NULL';
                            }
                            break;
                    }
                    $combine[] = $key . "=" . $value;

                    return $combine;
                },
                []
            );

            $update_str = implode(',', $data_update);
            $sql .= $update_str . " WHERE " . $this->_tableName . '_id = ' . $id;

            //prepare statement SQL
            $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
            // Execute Query
            $stmt->execute($params);

            return $id;
        } else {
            return '';
        }
    }
	function select_employee($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
    
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	function select_employee_row($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
    
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetch();
	}
}
<?php
require_once 'Pivot/Dao.php';


class Dao_Employee extends Pivot_Dao
{   
    function __construct()
    {
        parent::__construct('mr_emp');
    }
	
	function getEmployeeid($code)
    {
		$params = array();
		
        $sql = '
			SELECT 
				mr_emp_id as id 
			FROM ' . $this->_tableName . ' 
			WHERE code = ?
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

		array_push($params, trim($code));
        //echo  "<br><br><br><br><br><br>".$sql ;
        $id = $stmt->fetchAll();
        return $id[0]['id'];
	}	
	
	
	
	function getEmp_code($mr_emp_code)
    {
		$params = array();

        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_code,
					e.mr_branch_id,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					fl.name as mr_department_floor,
					e.mr_floor_id,
					b.mr_branch_name,
					b.mr_branch_code,
					d.mr_department_name,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				left join mr_floor fl on ( fl.mr_floor_id = e.mr_floor_id)
				WHERE e.mr_emp_code = ?
		';

			// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
		array_push($params, trim($mr_emp_code));
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetch();
		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	
	function getEmpByID($mr_emp_id)
    {
		$params = array();
		
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_tel,
					e.mr_branch_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_name,
					b.mr_branch_name,
					b.mr_branch_code,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				WHERE e.mr_emp_id = ?
				AND e.mr_floor_id <> 0 
				AND e.mr_department_id <> 0
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		 
		array_push($params, (int)$mr_emp_id);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();


		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	
	function getEmpcontact($mr_emp_id)
    {
		$params = array();
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_tel,
					e.mr_branch_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_name,
					b.mr_branch_name,
					b.mr_branch_code,
					u.mr_user_id
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				WHERE e.mr_emp_id = ? 
		';

		array_push($params, (int)$mr_emp_id);
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	// MARCH MARCH MARCH 
	function getEmpData($data)
    {
		$params = array();
        $sql = 'SELECT 
					e.*,
					d.*,
					u.mr_user_id as u_id
				FROM mr_emp e
				left join mr_user u on ( u.mr_emp_id = e.mr_emp_id)
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				WHERE e.mr_emp_code = ?
					AND e.mr_emp_name = ?
					AND e.mr_emp_lastname = ?
		';

		array_push($params, (string)$data['mr_emp_code']);
		array_push($params, (string)$data['mr_emp_name']);
		array_push($params, (string)$data['mr_emp_lastname']);

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	}
	
	
	function getEmpDataSelect( )
    {
        $sql = 'SELECT *
				FROM mr_emp e
				WHERE e.mr_emp_code IS NOT NULL 
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}


	function getEmpDataWithTXT($txt)
	{
		$params = array();
		
		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or
					 e.mr_emp_code like ? 
					 ) and
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 100 
		';

		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();

		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	function getEmpDataWithTXT_fileImport($txt)
	{
		if($txt != ''){
		
		$params = array();
		
		$sql =
			'
			select * 
			from mr_emp e
			left join mr_branch b on(b.mr_branch_id = e.mr_branch_id)
			left join mr_position p on(p.mr_position_id = e.mr_position_id)
			where
					( 						
						b.mr_branch_name like ? or 
						e.mr_emp_lastname like ? or 
						e.mr_emp_lastname like ?
					) and
					e.mr_emp_code NOT LIKE "%Resign%" and 
					e.mr_emp_code is not null limit 100 
		';
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();

		//echo $sql;
		// return $this->_db->fetchAll($sql);
		}else{
			$sql =
			'
			select * 
			from mr_emp e
			left join mr_branch b on(b.mr_branch_id = e.mr_branch_id)
			left join mr_position p on(p.mr_position_id = e.mr_position_id)
			where
					e.mr_emp_code NOT LIKE "%Resign%" and 
					e.mr_emp_code is not null limit 100 
			
		';
		return $this->_db->fetchAll($sql);
		}
	}
	
	
	function getEmpCode()
	{
		$sql =
			'
			select mr_emp_code
			from mr_emp e
			
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	
	function getEmpDataWithTXTsearch($txt)
	{
		$params = array();

		$sql =
			'
			select * 
			from mr_emp e
			where
					( e.mr_emp_name like ? or 
					 e.mr_emp_lastname like ? or 
					 e.mr_emp_code like ?
					 ) and
					 e.mr_department_id != 0 and 
					 e.mr_floor_id != 0 and 
					 e.mr_emp_code NOT LIKE "%Resign%" and 
					 e.mr_emp_code is not null limit 10 
		';

		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");
		array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	
	
	function ImportEmp( $sql )
	{
		return $this->_db->query($sql);
		//echo $sql;
		
	}
	
	function getEmpDataResign( )
    {
        $sql = 'SELECT *
				FROM mr_emp e
				LEFT JOIN mr_user m on ( e.mr_emp_id = m.mr_emp_id )
				WHERE e.mr_emp_code IS NOT NULL AND
				m.active = 0
		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchAll($sql);
	}
	
	
	function updateEmpByID(  $datas, $mr_emp_code  )
	{	
		$sql = ' SELECT mr_emp_id FROM mr_emp WHERE mr_emp_code = "'.$mr_emp_code.'" ';
		$id = $this->_db->fetchOne($sql);
        $this->_db->update($this->_tableName, $datas,  'mr_emp_code = "' .$mr_emp_code.'"');
		//echo $sql;
		return $id;
	}
	
	
	function checkEmpResign( $mr_emp_code )
    {
        $sql = 
		'
			SELECT 
				mr_emp_email
			FROM 
				mr_emp
			WHERE 
				mr_emp_code = "'.$mr_emp_code.'" 

		';
		//echo  "<br><br><br><br><br><br>".$sql ;
		return $this->_db->fetchOne($sql);
	}
	
	
	function getEmpDataWithTXTBranch($txt)
	{
		$sql =
			'
			select e.* 
			from mr_emp e
			where
					( e.mr_emp_name like "%'.$txt. '%" or 
					 e.mr_emp_lastname like "%'.$txt.'%" or e.mr_emp_code like "%'.$txt.'%") and
					 e.mr_emp_code != "Resign" and 
					 e.mr_emp_code is not null
					 limit 10 
		';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	
	
	function getEmpByIDBranch($mr_emp_id)
    {
		$params = array();
		
        $sql = 'SELECT e.mr_emp_id,
					e.mr_emp_code,
					e.mr_emp_name,
					e.mr_emp_lastname,
					e.mr_emp_email,
					e.mr_emp_tel,
					d.mr_department_code ,
					f.name as mr_department_floor,
					e.mr_floor_id,
					d.mr_department_id,
					d.mr_department_name,
					e.mr_workplace,
					e.mr_workarea,
					b.*
				FROM mr_emp e
				left join mr_department d on ( d.mr_department_id = e.mr_department_id)
				left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id)
				left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id)
				WHERE e.mr_emp_id = ?
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
				
		array_push($params, (int)$mr_emp_id);
		// Execute Query
		$stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
		
		//echo  "<br><br><br><br><br><br>".$sql ;
		// return $this->_db->fetchRow($sql);
	}
	//function getEmp_name( $name )
    //{
    //    $sql = 'SELECT e.mr_emp_id,
	//				e.mr_emp_code,
	//				e.mr_emp_name,
	//				e.mr_emp_lastname,
	//				e.mr_emp_email,
	//				d.mr_department_code ,
	//				d.mr_department_floor,
	//				d.mr_department_name
	//			FROM mr_emp e
	//			left join mr_department d on ( d.mr_department_id = e.mr_department_id)
	//			left join mr_floor f on ( f.mr_floor_id = d.mr_department_floor)
	//			WHERE e.mr_emp_name LIKE "%'.$name.'%" OR e.mr_emp_lastname LIKE "%'.$name.'%"
	//	';
	//	//echo  "<br><br><br><br><br><br>".$sql ;
	//	return $this->_db->fetchAll($sql);
	//}


	function getEmpDataById($empId)
	{
		$params = array();
		$sql =
		'
			select 	* from '.$this->_tableName.' where mr_emp_id = ?
		';

		array_push($params, (int)$empId);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}

	function select_employee($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
    
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	function select_employee_row($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
    
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetch();
	}
}
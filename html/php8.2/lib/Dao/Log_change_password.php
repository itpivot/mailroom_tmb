<?php
require_once 'Pivot/Dao.php';


class Dao_Log_change_password extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_log_change_password');
    }

    function getPasswordByUser($usrID) 
    {
        $params = array();
        $sql =
        '
            SELECT 
                logs.password
            FROM mr_log_change_password as logs
            WHERE logs.mr_user_id = ?
            ORDER BY logs.sys_timestamp DESC LIMIT 6
        ';

        array_push($params, (int)$usrID);

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();

    }

    function removeByUsrID($usrID) 
    {
        $sql =
        '
            DELETE FROM mr_log_change_password 
            WHERE mr_user_id = '.$usrID.'
        ';

        return $this->_db->query($sql);
    }
	
}
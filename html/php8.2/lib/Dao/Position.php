<?php
require_once 'Pivot/Dao.php';


class Dao_Position extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_position');
    }
	function getPosition()
	{
		$sql =
			' SELECT * FROM `mr_position` ORDER BY `mr_position`.`mr_position_code` ASC ';
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}

	function select_position($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
		

	}
	
}
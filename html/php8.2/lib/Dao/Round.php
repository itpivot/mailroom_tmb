<?php
require_once 'Pivot/Dao.php';


class Dao_Round extends Pivot_Dao
{

    function __construct()
    {
        parent::__construct('mr_round');
    }
	function getRoundAll()
	{
		$params = array(); 
		$sql =
		'
			SELECT
					*
				FROM
					mr_round r
				left join mr_type_work t using(mr_type_work_id)
			where r.active = 1
				ORDER BY
					r.mr_type_work_id ,r.mr_round_name ASC
					
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getRoundByhand()
	{
		$params = array(); 
		$sql =
		'
			SELECT
					*
				FROM
					`mr_round`
				WHERE
					`mr_type_work_id` = 4
					and active = 1
				ORDER BY
					`mr_round_id` ASC
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getRoundBranch()
	{
		$params = array(); 
		$sql =
		'
			SELECT
					*
				FROM
					mr_round r
				left join mr_type_work t using(mr_type_work_id)
				WHERE
					r.mr_type_work_id = 3
					and r.active = 1
				ORDER BY
					r.mr_round_id ASC
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getRoundBranch_tbank()
	{
		$params = array(); 
		$sql =
		'
		SELECT * 
		FROM mr_round r
		WHERE mr_round_name LIKE "%สำนักเพชรบุรี%" 
			   and r.active = 1
		ORDER BY
			r.mr_round_id ASC
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getRoundworkHO()
	{
		$params = array(); 
		$sql =
		'
			SELECT
					*
				FROM
					mr_round r
				left join mr_type_work t using(mr_type_work_id)
				WHERE
					r.mr_type_work_id = 1
					and active = 1
				ORDER BY
					r.mr_round_id ASC
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getRoundPost_in()
	{
		$params = array(); 
		$sql =
		'
			SELECT
					*
				FROM
					`mr_round`
				WHERE
					`mr_type_work_id` = 5
					and active = 1
				ORDER BY
					`mr_round_id` ASC
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getRoundPost_out()
	{
		$params = array(); 
		$sql =
		'
			SELECT
					*
				FROM
					`mr_round`
				WHERE
					`mr_type_work_id` = 6
					and active = 1
				ORDER BY
					`mr_round_id` ASC
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function gettype_work()
	{
		$params = array(); 
		$sql =
		'
			SELECT * FROM mr_type_work
		';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
}
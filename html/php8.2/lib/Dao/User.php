<?php
require_once 'Pivot/Dao.php';
require_once 'Dao/UserRole.php';


class Dao_User extends Pivot_Dao
{
	private  $_secret;
    function __construct()
    {
        parent::__construct('mr_user');
		//$this->_tableName = 'mr_user';
		$this->_secret = $this->getEncryptKey();
    }

    function select($sql)
	{
		if($sql != ''){
			return $this->_db->fetchAll($sql);
		}else{
			return 'No SQL';
		}
		
	}
	function get_Username()
	{
		$sql =
			"
			SELECT * FROM mr_user WHERE mr_user_username IS NOT NULL and mr_user_username != ''
			
		";
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
    function isExistsUser($username, $id = null)
    {
        $sql = 'SELECT count(*) FROM ' . $this->_tableName . ' WHERE mr_user_username = ?';
        
        if (is_numeric($id)) {
            $sql .= ' AND mr_user_id != ' . $this->_db->quote($id);
        }
        $n = $this->_db->fetchOne($sql, $username);
        return ($n > 0);
    }
    

    public function save($datas, $id = null)
    {
		if(isset($datas['password'])){
			if ($datas['password'] != '') {
			// $datas['password'] = md5($datas['password']);
			} else {
				unset($datas['password']);
			}
		}else{
			unset($datas['password']);	
		}
        return parent:: save($datas,$id);
    }
	
		
	
	function getempByuserid($user_id = null)
	{
		$params = array();

		$sql =
		'
			SELECT 
				*
			FROM mr_user 
			WHERE 
				mr_user_id = ?
				
		';

		array_push($params, (int)$user_id);
    
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
        // Execute Query
        $stmt->execute($params);    

        return $stmt->fetch();
	}
	
	function getEmpDataByuserid($user_id)
	{

		$params = array();

		$sql =
		"
			SELECT 
				u.mr_user_id,
				u.mr_user_username,
				u.mr_user_password,
				u.is_first_login,
				u.date_create,
				u.sys_timestamp,
				u.change_password_date,
				u.active,
				u.mr_emp_id,
				u.mr_user_role_id,
				u.isLogin,
				u.isFailed,
				u.tocent,
				e.mr_emp_id,
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_department_id,
				e.mr_position_id,
				e.mr_floor_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, '".$this->_secret->database->key."') as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, '".$this->_secret->database->key."') as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, '".$this->_secret->database->key."') as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_email, '".$this->_secret->database->key."') as mr_emp_email,
				d.mr_department_id,
				d.mr_department_name,
				d.mr_department_code,
				d.mr_department_floor
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			WHERE u.mr_user_id = ?
			
		";

		array_push($params, (int)$user_id);
    
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
        // Execute Query
        $stmt->execute($params);    

        return $stmt->fetch();
	
		//echo $sql;
		return $this->_db->fetchRow($sql);
	}
	
	
	function getempByEmpid( $emp_code ) 
	{
		$params = array();
		$sql =
			'
			SELECT  e.mr_emp_code,
					u.mr_user_password,
					e.mr_emp_name,
					e.mr_emp_lastname,
					u.mr_user_id,
					e.mr_emp_id
			FROM mr_user u 
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id ) 
			WHERE e.mr_emp_code LIKE ?
		';
	
		array_push($params, (string)$emp_code);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}

	function getempByEmpid_ch_Pass( $id ) 
	{
		$params = array();
		$sql =
			'
			SELECT  e.mr_emp_code,
					u.mr_user_password,
					e.mr_emp_name,
					e.mr_emp_lastname,
					u.mr_user_id,
					e.mr_emp_id
			FROM mr_user u 
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id ) 
			WHERE e.mr_emp_id LIKE ?
			
			
		';
		array_push($params, (string)'%'.$id.'%');

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}
	function getempByEmpid_ch_Pass_2( $id ) 
	{
		$params = array();
		$sql =
			'
			SELECT  e.mr_emp_code,
					u.mr_user_password,
					e.mr_emp_name,
					e.mr_emp_lastname,
					u.mr_user_id,
					e.mr_emp_id
			FROM mr_user u 
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id ) 
			WHERE e.mr_emp_id LIKE ?
			
			
		';
	
		array_push($params, (string)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetch();
	}
	function getMessSend()
	{
	$sql =
		'
			SELECT 
				u.*,
				e.*
			FROM mr_user u
			LEFT JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id ) 
			WHERE u.mr_user_role_id = 3
			AND u.mr_emp_id IS NOT NULL
			
		';
	
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	
	function getMessPrint( $user_id )
	{
		$params = array();
		$sql =
			'
			SELECT 
				distinct (u.mr_user_id),
				e.*
			FROM mr_user u
			Left join mr_department d on ( d.mr_user_id = u.mr_user_id )
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			WHERE u.mr_user_id <> 0
			AND u.mr_user_role_id = 3 
					
		';
	
		if( $user_id != 0 ){
			$sql .= ' AND u.mr_user_id = ? ';
			array_push($params, (int)$user_id);
		}	

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

        return $stmt->fetchAll();
	}
	
	function getUsersByEmpId($emp_id) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				u.*,
				u.mr_user_id as user_id,
				e.*,
				d.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			WHERE u.mr_user_id = ?
		';

		array_push($params, (int)$emp_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}

	function getUsersByEmpIdch_user($emp_id) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				u.*,
				u.mr_user_id as user_id,
				e.*,
				d.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			WHERE u.mr_emp_id = ?
		';

		array_push($params, (int)$emp_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
	
	function getEmpSignByUserID($user_id , $barcode) 
	{
		$params = array();
		
		$sql =
		'
			SELECT 
				e.*,
				d.*,
				f.name as floor,
				m.*
			FROM mr_user u
				left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
				Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
				Left join mr_work_inout i on ( i.mr_user_id = u.mr_user_id )
				Left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id )
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			WHERE 
				u.mr_user_id = ? AND 
				m.mr_work_barcode = ?
		';

		array_push($params, (int)$user_id);
		array_push($params, trim($barcode));

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
	}
	
	function getEmpSignByUserID_2($mr_user_id , $barcode) 
	{
		$params = array();
		
		$sql =
		'
			SELECT 
				e.*,
				d.*,
				f.name as floor,
				m.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			left join mr_confirm_log cl on ( cl.mr_emp_id = e.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = e.mr_floor_id )
			left join mr_work_main m on ( m.mr_work_main_id = cl.mr_work_main_id )
			WHERE 
				u.mr_user_id = ? AND 
				m.mr_work_barcode = ?
		';
		//echo $sql;
		array_push($params, (int)$mr_user_id);
		array_push($params, trim($barcode));

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
	}



	//  ================================== MARCH ==========================

	function getUserIdFromUserName($user) 
	{	
		$params = array();

		$sql =
		'
			SELECT 
				*
			FROM mr_user as u
			WHERE u.mr_user_username = ? AND
			u.mr_user_password = ?
		';

		array_push($params, (string)trim($user['username']));
		array_push($params, (string)trim($user['password']));

		//return $this->_db->fetchRow($sql);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();

	}

	function getDiffDatePassword($usr)
	{	
		$params = array();
		$sql =
		'
			SELECT 
				DATEDIFF(NOW(), 
				DATE_FORMAT(usr.change_password_date, "%Y-%m-%d")) as diffdate
			FROM mr_user as usr
			WHERE usr.mr_user_id = ?
		';

		array_push($params, (int)$usr);

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
	}
	
	function getEmpDataByuseridProfile($user_id)
	{
		$params = array();
		
		$sql =
		'
			SELECT 
				u.*,
				e.mr_emp_id,
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_department_id,
				e.mr_position_id,
				e.mr_floor_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				u.mr_user_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'") as mr_emp_mobile,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
				d.mr_department_id ,
				d.mr_department_name  ,
				d.mr_department_code  ,
				d.mr_department_floor ,
				f.mr_floor_id as floor_emp,
				f.mr_floor_id as mr_floor_id_emp,
				concat(b.mr_branch_code," : ",b.mr_branch_name) as mr_branch_name,
				concat(p.mr_position_code," : ",p.mr_position_name) as mr_position_name,
				f.name
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( e.mr_floor_id = f.mr_floor_id )
			Left join mr_branch b on ( e.mr_branch_id = b.mr_branch_id )
			Left join mr_position p on ( e.mr_position_id = p.mr_position_id )
			WHERE u.mr_user_id = ?
			
		';

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		 
		array_push($params, (int)$user_id);
        // Execute Query
        $stmt->execute($params);
		
		return $stmt->fetch();
	
	}

	function getFailedByUser($username)
	{
		$params = array();
		$sql =
		'
			SELECT 
				u.mr_user_id,
				u.isFailed,
				u.mr_user_username,
				u.mr_user_password
			FROM mr_user as u
			WHERE u.mr_user_username = ?
		';

		array_push($params, (string)$username);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
	
	function getEmpDataForEdit()
	{
	$sql =
		'
			SELECT 
				u.*,
				e.*,
				r.mr_user_role_name
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			left join mr_user_role r on ( r.mr_user_role_id = u.mr_user_role_id )
			WHERE u.mr_user_username != "delete"
		';
	
		//echo $sql;
		return $this->_db->fetchAll($sql);
	}
	
	
	function updateStatus( )
	{
		//$sql = 'Update mr_user set isLogin = 0 WHERE isLogin = 1';
		$data['isLogin'] = 0 ;
		$this->_db->update($this->_tableName, $data,'isLogin = 1');
		$sql = ' SELECT mr_user FROM mr_user_id WHERE isLogin = 1 ';
		return $this->_db->fetchOne($sql);
	}
	

	// ================== NEW MARCH ==============================
	function getMessengers()
	{
		$sql =
		'
			select 
				usr.mr_user_id,
				emp.mr_emp_name, 
				emp.mr_emp_lastname,
				emp.mr_emp_code
			from mr_user as usr 
				left join mr_emp as emp on (emp.mr_emp_id = usr.mr_emp_id)
			where 
				usr.mr_user_role_id in(3,9)
				and mr_emp_code != "Resign"
		';
		return $this->_db->fetchAll($sql);
	}
	
	
	function Update_delete_User( $code )
	{
		$data['sys_timestamp'] = date("Y-m-d H:i:s");
		$data['mr_user_username'] = 'delete' ;
		$data['active'] = 0 ;
		$this->_db->update($this->_tableName, $data, 'mr_user_username = "' .$code.'"');
		//$sql = ' SELECT mr_user FROM mr_user_id WHERE mr_user_username = "delete"';
		//return $this->_db->fetchOne($sql);
	}
	
	
	
	function getEmpDataByuseridProfileBranch($user_id)
	{
		$params = array();

		$sql =
		"
			SELECT 
				u.*,
				e.mr_emp_id,
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_department_id,
				e.mr_position_id,
				e.mr_floor_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, '".$this->_secret->database->key."') as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, '".$this->_secret->database->key."') as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, '".$this->_secret->database->key."') as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_email, '".$this->_secret->database->key."') as mr_emp_email,
				b.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id )
			WHERE u.mr_user_id = ?
			
		";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		 
		array_push($params, (int)$user_id);
        // Execute Query
        $stmt->execute($params);

		//echo $sql;
		return $stmt->fetch();
	
		//echo $sql;
		// return $this->_db->fetchRow($sql);
	}
	
	
	function getUsersByEmpIdBranch( $emp_id ) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				u.*,
				u.mr_user_id as user_id,
				e.mr_emp_id,
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_department_id,
				e.mr_position_id,
				e.mr_floor_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
				d.*
			FROM mr_user u
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_branch b on ( b.mr_branch_id = e.mr_branch_id )
			WHERE e.mr_emp_id = ?
		';


		array_push($params, (int)$emp_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
	
	
	
}
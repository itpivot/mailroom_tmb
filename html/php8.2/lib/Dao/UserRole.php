<?php
require_once 'Pivot/Dao.php';


class Dao_UserRole extends Pivot_Dao
{
    private static $_roles = array(
        'Administrator' => 1,
        'Employee' => 2,
        'Messenger' => 3,
        'Mailroom' => 4,
        'Branch' => 5,
        'Manager' => 6,
        'AIACapital' => 7,
        'MessengerBranch' => 8,
        'MessengerHO' => 9,
    );
    
    function __construct()
    {
        parent::__construct('mr_user_role');
    }

    public static function getAllRoles()
    {
        return self::$_roles;
    }
    
    public static function role($name) {
        return self::$_roles[$name];
    }
}
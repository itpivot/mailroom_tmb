<?php
require_once 'Pivot/Dao.php';


class Dao_Work_inout extends Pivot_Dao
{
	private  $_secret;
    function __construct()
    {
        parent::__construct('mr_work_inout');
		$this->_secret = $this->getEncryptKey();
    }
	
	function searchFollow( $txt = '', $userId = null  )
	{
		$params = array();
		
		$sql =
		'
			SELECT 
				m.*,
				i.*,
				e.*,
				d.*,
				s.mr_status_name
			FROM mr_work_inout i
			Left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			WHERE
				m.mr_user_id = ?
				AND m.mr_status_id <> 6 
				AND m.mr_work_date_sent = CURDATE()  
		';

		array_push($params, $userId);

		if(!empty($txt)){
			$sql .= ' AND (
					m.mr_work_barcode LIKE ? OR
					e.mr_emp_name LIKE ? OR
					e.mr_emp_lastname LIKE ? OR
					e.mr_emp_mobile	LIKE ? OR
					m.mr_work_date_sent	LIKE ?
				) 
			';

			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
		}

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
		//return $sql;
		// return $this->_db->fetchAll($sql);
	}
	
	
	function searchMessFollow( $txt = '', $userId = ''  )
	{
		$params = array(); 
		$sql =
		'
			f.name as mr_department_floor,
				m.*,
				i.*,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				e.mr_emp_tel as tel_re,
				e.mr_emp_mobile as mobile_re,
				u.*,
				x.*,
				d.mr_department_name,
				d.mr_department_code,,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			WHERE
				 m.mr_status_id <> 6 AND 
				 ( d.mr_user_id = ? OR y.mr_user_id = ? )
				 
				AND m.mr_work_date_sent = CURDATE()  
		';

		array_push($params, (int)$userId);
		array_push($params, (int)$userId);

		if(!empty($txt)){
			$sql .= ' AND (m.mr_work_barcode LIKE ? OR
				e.mr_emp_name LIKE ? OR
				e.mr_emp_lastname LIKE ? OR
				e.mr_emp_mobile	LIKE ? OR
				m.mr_work_date_sent	LIKE ? ) ';

				array_push($params, (string)'%'.$txt.'%');
				array_push($params, (string)'%'.$txt.'%');
				array_push($params, (string)'%'.$txt.'%');
				array_push($params, (string)'%'.$txt.'%');
				array_push($params, (string)'%'.$txt.'%');
		}

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function searchFollowCurdate( $userId, $text = null  )
	{
		$params = array();

		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_type_work_id,
				m.mr_status_id,
				m.mr_user_id,
				m.mr_floor_id,
				m.mr_round_id,
				m.mr_work_date_success,
				m.mr_topic,
				m.mr_branch_id as send_branch_id,
				m.mr_send_work_id,
				m.messenger_user_id,
				m.quty,
				i.mr_work_inout_id,
				i.mr_emp_id,
				i.mr_branch_floor,
				i.mr_status_send,
				i.mr_status_receive,
				i.rate_send,
				i.rate_remark,
				i.mr_contact_id,
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_department_id,
				e.mr_position_id,
				e.mr_floor_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'") as mr_emp_mobile,

				b.mr_branch_name,
				b.mr_user_username_old,
				b.mr_branch_code,
				b.mr_address_id,
				b.mr_group_id,
				b.mr_branch_type,
				b.mr_branch_category_id,
				b.sys_date,
				b.active,
				d.mr_department_name,
				d.mr_department_code,
				d.mr_department_floor,
				f.name as floor_name,
				s.mr_status_name
			FROM mr_work_inout i
			Left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_branch b on ( b.mr_branch_id = i.mr_branch_id )
			WHERE m.mr_work_main_id is not null 
				and m.mr_user_id = ?
				AND m.mr_status_id <> 6 
				AND m.mr_work_date_sent = CURDATE()  
		';

		array_push($params, $userId);

		if(!empty($text)) {
			$sql .= "
				and (m.mr_work_barcode like ? 
				or e.mr_emp_name like ? 
				or e.mr_emp_lastname like ? 
				or e.mr_emp_tel like ? 
				or e.mr_emp_mobile like ? 
				)
			";

			array_push($params, "%".trim($text)."%");
			array_push($params, "%".trim($text)."%");
			array_push($params, "%".trim($text)."%");
			array_push($params, "%".trim($text)."%");
			array_push($params, "%".trim($text)."%");
		}
	
		$sql .= " Order By i.sys_timestamp DESC ";
		

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	
	function searchMessFollowCurdate( $userId  )
	{
		$params = array(); 
		$sql =
		'
			SELECT 
				f.name as mr_department_floor,
				m.*,
				i.*,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				e.mr_emp_tel as tel_re,
				e.mr_emp_mobile as mobile_re,
				u.*,
				x.*,
				d.mr_department_name,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			WHERE
				 m.mr_status_id <> 6 AND 
				 ( d.mr_user_id = ? OR y.mr_user_id = ? )
				 
				AND m.mr_work_date_sent = CURDATE()  
			Order By i.sys_timestamp DESC
		';

		array_push($params, (int)$userId);
		array_push($params, (int)$userId);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function countFollowCurdate( $userId  )
	{
		$params = array();
		$sql =
		'
			SELECT 
				COUNT(m.mr_work_main_id) as count_curday
			FROM mr_work_inout i
			Left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			WHERE
				m.mr_user_id = ? 
				AND m.mr_status_id <> 6 
				AND m.mr_work_date_sent = CURDATE()  
		';

		array_push($params, (int)$userId);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
	
	function countMessFollowCurdate( $userId  )
	{
		$params = array();
		$sql =
		'
			SELECT 
				COUNT(m.mr_work_main_id) as count_curday
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			WHERE
				 m.mr_status_id <> 6 AND 
				 ( d.mr_user_id = ? OR y.mr_user_id = ? )
				 
				AND m.mr_work_date_sent = CURDATE()   
		';
		
		array_push($params, (int)$userId);
		array_push($params, (int)$userId);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
	
	function searchWorkEmp($txt, $user_id) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.*,
				i.*,
				i.sys_timestamp as time_test,
				e.mr_emp_id ,
				e.sys_time,
				e.update_date,
				e.mr_emp_code,
				e.old_emp_code,
				e.mr_position_id,
				e.mr_cost_id,
				e.mr_branch_id,
				e.mr_branch_floor,
				e.mr_date_import,
				e.mr_workplace,
				e.mr_workarea,
				e.mr_hub_id,
				e.emp_type,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'") as mr_emp_email,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'") as mr_emp_mobile,
				d.*,
				f.name as floor_name,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = d.mr_floor_id )
		WHERE
			m.mr_user_id = ? AND 
		';

		array_push($params, $user_id);

		if(!empty($txt)) {
			$sql .= '
				(m.mr_work_barcode LIKE ? OR
				e.mr_emp_name LIKE ? OR
				e.mr_emp_lastname LIKE ? OR
				e.mr_emp_tel LIKE ? OR
				e.mr_emp_mobile	 LIKE ? OR
				m.mr_work_date_sent	 LIKE ?) 
			';

			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
		}

		

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        $stmt->execute($params);
        return $stmt->fetchAll();


		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}
	
	function countSearchWorkEmp($txt, $user_id) 
	{
		$params = array();

		$sql =
		'
			SELECT 
				COUNT(m.mr_work_main_id) as count_data_search
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
		WHERE
			m.mr_user_id = ? and 
			
		';

		array_push($params, $user_id);

		if(!empty($txt)) {
			$sql .= '
				(m.mr_work_barcode LIKE ? OR
				e.mr_emp_name LIKE ? OR
				e.mr_emp_lastname LIKE ? OR
				e.mr_emp_tel LIKE ? OR
				e.mr_emp_mobile	 LIKE ? OR
				m.mr_work_date_sent	 LIKE ?) 
			';

			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
			array_push($params, "%".trim($txt)."%");
		}

		$sql .= " LIMIT 0 , 30 ";

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

        $stmt->execute($params);
        return $stmt->fetch();
		//echo $sql;
		// return $this->_db->fetchOne($sql);
	}
	
	function searchSendMailroom( $mess_id )
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.*,
				m.sys_timestamp as date_send,
				i.*,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				u.*,
				x.*,
				t.*,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			join(
					select
						*
					from mr_emp
				)x on ( x.mr_emp_id = u.mr_emp_id )
			WHERE
				m.mr_status_id = 3

		';
		
		if ( $mess_id != 0 ){
			$sql .= ' AND d.mr_user_id = ? ';
			array_push($params, (int)$mess_id);
		}

		$sql .= ' ORDER BY m.sys_timestamp DESC ';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);

		$stmt->execute($params);
		
        return $stmt->fetchAll();
	}
	
	
	
	function searchMailroom( $data_search )
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.sys_timestamp as time_re,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_type_work_id,
				m.mr_status_id,
				m.mr_user_id as m_user_id,
				m.mr_floor_id as sen_floor_id,
				m.mr_round_id,
				m.mr_work_date_success,
				m.mr_topic,
				m.mr_branch_id as sen_branch_id,
				m.mr_send_work_id,
				m.messenger_user_id,
				l.sys_timestamp as con_log,
				i.sys_timestamp as time_test,
				i.mr_work_inout_id,
				m.sys_timestamp as time_re,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'")  as mr_emp_lastname,
				u.mr_user_id,
				u.mr_user_username, 
				u.mr_emp_id, 
				u.mr_user_role_id,
				x.mr_emp_code,
				t.mr_type_work_id,
				t.mr_type_work_name,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				y.mr_department_code,
				y.mr_department_name,
				y.mr_department_code,
				z.name as depart_floor_send,
				s.mr_status_name,
				b2.mr_branch_code as re_branch_code,
				b2.mr_branch_name as re_branch_name,

				b3.mr_branch_code as send_branch_code2,
				b3.mr_branch_name as send_branch_name2,
				b3.mr_branch_type as mr_branch_type2,

				b4.mr_branch_code as re_branch_code2,
				b4.mr_branch_name as re_branch_name2,
				b1.mr_branch_code as send_branch_code,
				b1.mr_branch_type as mr_branch_type1,
				b1.mr_branch_name as send_branch_name,
				cont.department_name as con_name,
				
				AES_DECRYPT(ebh.mr_emp_name, "'.$this->_secret->database->key.'") as name_send_bh,
				AES_DECRYPT(ebh.mr_emp_lastname, "'.$this->_secret->database->key.'")  as lastname_send_bh,
				wbh.mr_cus_name as bh_resive,
				febh.name as floor_send_bh
			FROM mr_work_main m  

			left join mr_work_byhand wbh on ( wbh.mr_work_main_id = m.mr_work_main_id )
			left join mr_emp ebh on ( ebh.mr_emp_id = wbh.mr_send_emp_id )
			Left join mr_floor febh on ( febh.mr_floor_id = ebh.mr_floor_id )

			left join mr_work_inout i on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_contact cont on ( cont.mr_contact_id = i.mr_contact_id )
			left join mr_confirm_log l on ( l.mr_work_main_id = m.mr_work_main_id )
			
			left join mr_branch b1 on ( b1.mr_branch_id = m.mr_branch_id )
			left join mr_branch b2 on ( b2.mr_branch_id = i.mr_branch_id )

			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			left join mr_branch b4 on ( b4.mr_branch_id = e.mr_branch_id )

			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )

			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			left join mr_branch b3 on ( b3.mr_branch_id = x.mr_branch_id )

			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )			
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			
			
			
			
			
			
			WHERE
				m.mr_status_id <> 0
			
		';
		
		if( $data_search['select_mr_contact_id'] != "" ){
			$sql .= ' AND i.mr_contact_id = ? ';
			array_push($params, (int)$data_search['select_mr_contact_id']);
		}
		if( $data_search['barcode'] != "" ){
			$sql .= ' AND m.mr_work_barcode like ? ';
			array_push($params, (string)$data_search['barcode'].'%');
		}
		
		if( $data_search['sender'] != "" ){
			$sql .= ' AND x.mr_emp_code = ? ';
			array_push($params, (string)$data_search['sender']);
		}
		
		if( $data_search['receiver'] != "" ){
			$sql .= ' AND e.mr_emp_code = ? ';
			array_push($params, (string)$data_search['receiver']);
		}
		
		if( $data_search['status'] != "" ){
			if( $data_search['status'] != "Pending" ){
				$sql .= ' AND m.mr_status_id = ? ';
				array_push($params, (int)$data_search['status']);
			}else{
				$sql .= ' AND DATE_FORMAT(m.sys_timestamp, "%H : %I : %S") >= "16:00:00"';
			}
		}
		
		
		if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
			//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
			$sql .= '  AND ( m.mr_work_date_sent BETWEEN ? AND ? )';
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
			array_push($params, (string)$data_search['start_date']);
			array_push($params, (string)$data_search['end_date']);
		}
		if(empty($params)){
			$sql .= '  AND ( m.mr_work_date_sent BETWEEN ? AND ? )';
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
			$start_date = date("Y-m-d 00:00:00");
			$end_date   = date("Y-m-d H:i:s");
			array_push($params, (string)$start_date);
			array_push($params, (string)$end_date);
		}
		
		$sql.=" group by m.mr_work_main_id";
	
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function searchMailroom_upQury( $data_search )
	{
		$params = array();
		$sql =
		'
		SELECT 
				m.mr_work_main_id,
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_type_work_id,
				m.mr_status_id,
				m.mr_user_id as m_user_id,
				m.mr_floor_id as sen_floor_id,
				m.mr_round_id,
				m.mr_work_date_success,
				m.mr_topic,
				log.sys_timestamp as MailroomResive_date,
				log.log_new,
				log.log_mailroom,
				log.log_success,
				m.mr_branch_id as sen_branch_id,
				m.mr_send_work_id,
				m.messenger_user_id,
				type.mr_type_work_name,
				status.mr_status_name,
				i.rate_remark,
				rs.mr_round_name,
				CASE
					WHEN rsw.mr_round_resive_work_id is null  THEN m.quty
					ELSE count(rsw.mr_round_resive_work_id)
				END as count_qty,
				CASE
					WHEN i.rate_send is null  THEN "" 
					WHEN i.rate_send = 5 THEN "พึ่งพอใจ" 
					ELSE "ไม่พึ่งพอใจ"
				END as rate_send,
				
				
				';
			
		//sender data
		$sql .= ' 
		
				concat(dep_send.mr_department_code,":",dep_send.mr_department_name) as dep_send,
				CASE
					WHEN b_send1.mr_hub_id != "" 
					THEN b_send1.mr_hub_id
					ELSE b_send2.mr_hub_id
				END as send_hub_id,
				CASE
					WHEN b_send1.mr_branch_code != "" 
					THEN concat(b_send1.mr_branch_code,":",b_send1.mr_branch_name) 
					ELSE concat(b_send2.mr_branch_code,":",b_send2.mr_branch_name) 
				END as b_send,
				CASE
					WHEN u_send.mr_user_role_id = 2 THEN "สำนักงานใหญ่" 
					WHEN u_send.mr_user_role_id = 5 THEN "สาขา" 
					ELSE "admin"
				END as role_send,
				CASE
					WHEN bt_send.branch_type_name =  "" or bt_send.branch_type_name is null  THEN "สำนักงานใหญ่" 
					ELSE bt_send.branch_type_name
				END as bt_send,
				concat(emp_send.mr_emp_code,":",emp_send.mr_emp_name," ",emp_send.mr_emp_lastname) as emp_send,
				f_send.name as f_send,
				';
			
		// resive data
			$sql .= ' 
			
			
				concat(dep_resive.mr_department_code,":",dep_resive.mr_department_name) as dep_resive,
				CASE
					WHEN b_resive1.mr_hub_id != "" 
					THEN b_resive1.mr_hub_id
					ELSE b_resive2.mr_hub_id
				END as resive_hub_id,
				CASE
					WHEN b_resive1.mr_branch_code != "" 
					THEN concat(b_resive1.mr_branch_code,":",b_resive1.mr_branch_name) 
					ELSE concat(b_resive2.mr_branch_code,":",b_resive2.mr_branch_name) 
				END as b_resive,
				CASE
					WHEN u_resive.mr_user_role_id = 2 THEN "สำนักงานใหญ่" 
					WHEN u_resive.mr_user_role_id = 5 THEN "สาขา" 
					ELSE "admin"
				END as role_resive,
				CASE
					WHEN bt_resive.branch_type_name =  "" or bt_resive.branch_type_name is null THEN "สำนักงานใหญ่" 
					ELSE bt_resive.branch_type_name
				END as bt_resive,
				concat(emp_resive.mr_emp_code,":",emp_resive.mr_emp_name," ",emp_resive.mr_emp_lastname) as emp_resive,
				f_resive.name as f_resive,
			';
			
			// mess send data
			$sql .= ' 
				concat(emp_mess_send.mr_emp_code,":",emp_mess_send.mr_emp_name," ",emp_mess_send.mr_emp_lastname) as emp_mess_send,
			';
			
			// mess send data
			$sql .= ' 
				concat(emp_mess_resive.mr_emp_code,":",emp_mess_resive.mr_emp_name," ",emp_mess_resive.mr_emp_lastname) as emp_mess_resive
			';
				
				
			$sql .= ' 	
			FROM mr_work_main m
			left join mr_work_inout i on(i.mr_work_main_id = m.mr_work_main_id)
			Left join mr_type_work type on( type.mr_type_work_id = m.mr_type_work_id )
			Left join mr_status status on (status.mr_status_id = m.mr_status_id)
			';
			
			
			
		//mr_round_resive_work
		$sql .= '
			left join mr_round_resive_work rsw on(rsw.mr_work_main_id = m.mr_work_main_id)
			left join mr_round rs on(rs.mr_round_id = rsw.mr_round_id)
		';

		//sender data
		$sql .= ' 
			Left join mr_user u_send on ( u_send.mr_user_id = m.mr_user_id )
			Left join mr_emp emp_send on ( emp_send.mr_emp_id = u_send.mr_emp_id )
			Left join mr_department dep_send on ( dep_send.mr_department_id = emp_send.mr_department_id )
				left join mr_branch b_send1 on ( b_send1.mr_branch_id = m.mr_branch_id )
				left join mr_branch b_send2 on ( b_send2.mr_branch_id = emp_send.mr_branch_id )
				Left join mr_floor f_send on ( f_send.mr_floor_id = m.mr_floor_id )	
				Left join mr_branch_type bt_send on(bt_send.mr_branch_type_id = (
						CASE  WHEN b_send1.mr_branch_type != ""
								THEN b_send1.mr_branch_type
								ELSE b_send2.mr_branch_type
							END
						)
					)
			
			';
			
		// resive data
			$sql .= ' 
			Left join mr_emp emp_resive on ( emp_resive.mr_emp_id = i.mr_emp_id )
			Left join mr_user u_resive on ( u_resive.mr_emp_id = emp_resive.mr_emp_id )
			Left join mr_department dep_resive on ( dep_resive.mr_department_id = emp_resive.mr_department_id )
				left join mr_branch b_resive1 on ( b_resive1.mr_branch_id = i.mr_branch_id )
				left join mr_branch b_resive2 on ( b_resive2.mr_branch_id = emp_resive.mr_branch_id )
				Left join mr_floor f_resive on ( f_resive.mr_floor_id = i.mr_floor_id )	
				Left join mr_branch_type bt_resive on(bt_resive.mr_branch_type_id = (
						CASE  WHEN b_resive1.mr_branch_type != ""
								THEN b_resive1.mr_branch_type
								ELSE b_resive2.mr_branch_type
							END
						)
					)
			';
			
			// mess send data
			$sql .= ' 
			Left join mr_zone zone_send on ( zone_send.mr_floor_id = i.mr_floor_id)
			Left join mr_user u_mess_send on ( u_mess_send.mr_user_id = zone_send.mr_user_id )
			Left join mr_emp emp_mess_send on ( emp_mess_send.mr_emp_id = u_mess_send.mr_emp_id )
			
			';
			
			// mess resive data

			$sql .= ' 
			Left join mr_zone zone_resive on ( zone_resive.mr_floor_id = m.mr_floor_id)
			Left join mr_user u_mess_resive on ( u_mess_resive.mr_user_id = zone_resive.mr_user_id )
			Left join mr_emp emp_mess_resive on ( emp_mess_resive.mr_emp_id = u_mess_resive.mr_emp_id )
			
			';



	


			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==
			//++++++++++++++joinlog+++++++++++++++++==




			$sql .= 'Left join (
				';
		
		
				$sql .='
				SELECT	lg.sys_timestamp ,
						lg.mr_work_main_id ,
						CASE
							WHEN lg.mr_status_id in(1,7)
								THEN lg.sys_timestamp
								ELSE "" 
							END as log_new,
						CASE
							WHEN lg.mr_status_id in(3,10)
								THEN lg.sys_timestamp
								ELSE "" 
							END as log_mailroom,
						CASE
							WHEN lg.mr_status_id in(5,12)
								THEN lg.sys_timestamp
								ELSE "" 
							END as log_success

				FROM mr_work_log as lg';
							$sql .= ' 	
							left join mr_work_main m on(m.mr_work_main_id = lg.mr_work_main_id)
							left join mr_work_inout i on(i.mr_work_main_id = m.mr_work_main_id)
						';
					
			
		
				//sender data
				$sql .= ' 
					Left join mr_user u_send on ( u_send.mr_user_id = m.mr_user_id )
					Left join mr_emp emp_send on ( emp_send.mr_emp_id = u_send.mr_emp_id )
					
					
					';
					
				// resive data
					$sql .= ' 
					Left join mr_emp emp_resive on ( emp_resive.mr_emp_id = i.mr_emp_id )
					
					';
				//mr_round_resive_work
				$sql .= '
				left join mr_round_resive_work rsw on(rsw.mr_work_main_id = m.mr_work_main_id)
				left join mr_round rs on(rs.mr_round_id = rsw.mr_round_id)
			';
		
					$sql .= ' 
					WHERE
						m.mr_status_id <> 0
						and lg.mr_status_id IN (1,3,5,7,10,12) 
				';
				
				
				if( $data_search['select_mr_contact_id'] != "" ){
					$sql .= ' AND i.mr_contact_id = ? ';
					array_push($params, (int)$data_search['select_mr_contact_id']);
				}
				if( $data_search['barcode'] != "" ){
					$sql .= ' AND m.mr_work_barcode = ? ';
					array_push($params, (string)$data_search['barcode']);
				}
				
				
				
				
				
				if( $data_search['sender'] != "" ){
					$sql .= ' AND emp_send.mr_emp_code = ? ';
					array_push($params, (string)$data_search['sender']);
				}
				
				if( $data_search['receiver'] != "" ){
					$sql .= ' AND emp_resive.mr_emp_code = ? ';
					array_push($params, (string)$data_search['receiver']);
				}
				
				
				
				
				if( $data_search['status'] != "" ){
					if( $data_search['status'] != "Pending" ){
						$sql .= ' AND m.mr_status_id = ? ';
						array_push($params, (int)$data_search['status']);
					}else{
						$sql .= ' AND DATE_FORMAT(m.sys_timestamp, "%H : %I : %S") >= "16:00:00"';
					}
				}
				
				
				if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
					//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
					$sql .= '  AND 
								(
									( m.mr_work_date_sent  BETWEEN  ? AND ? )
									or
									( rsw.sysdate  BETWEEN  ? AND ? )
									
								)';
					//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
					array_push($params, (string)$data_search['start_date']);
					array_push($params, (string)$data_search['end_date']);
					array_push($params, (string)$data_search['start_date']." 00:00:00");
					array_push($params, (string)$data_search['end_date']." 23:59:59");
				}
		
		
				$sql .= ' GROUP by m.mr_work_main_id) log on(log.mr_work_main_id = m.mr_work_main_id)';
		
		
				
		
		
		
		
		
		
					
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
					//++++++++++++++joinlog+++++++++++++++++==
			
			$sql .= ' 
			WHERE
				m.mr_status_id <> 0
		';
		
		
		if( $data_search['select_mr_contact_id'] != "" ){
			$sql .= ' AND i.mr_contact_id = ? ';
			array_push($params, (int)$data_search['select_mr_contact_id']);
		}
		if( $data_search['barcode'] != "" ){
			$sql .= ' AND m.mr_work_barcode = ? ';
			array_push($params, (string)$data_search['barcode']);
		}
		
		
		
		
		
		if( $data_search['sender'] != "" ){
			$sql .= ' AND emp_send.mr_emp_code = ? ';
			array_push($params, (string)$data_search['sender']);
		}
		
		if( $data_search['receiver'] != "" ){
			$sql .= ' AND emp_resive.mr_emp_code = ? ';
			array_push($params, (string)$data_search['receiver']);
		}
		
		
		
		
		if( $data_search['status'] != "" ){
			if( $data_search['status'] != "Pending" ){
				$sql .= ' AND m.mr_status_id = ? ';
				array_push($params, (int)$data_search['status']);
			}else{
				$sql .= ' AND DATE_FORMAT(m.sys_timestamp, "%H : %I : %S") >= "16:00:00"';
			}
		}
		
		
		if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
			//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
			$sql .= '  AND 
						(
							( m.mr_work_date_sent  BETWEEN  ? AND ? )
							or
							( rsw.sysdate  BETWEEN  ? AND ? )
						
						)';
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
			array_push($params, (string)$data_search['start_date']);
			array_push($params, (string)$data_search['end_date']);
			array_push($params, (string)$data_search['start_date']." 00:00:00");
			array_push($params, (string)$data_search['end_date']." 23:59:59");
		}

			//	if( $data_search['sender'] != "" ){
			//		$sql .= ' AND e.mr_emp_code = ? ';
			//		array_push($params, (string)$data_search['sender']);
			//	}
			//	
			//	if( $data_search['receiver'] != "" ){
			//		$sql .= ' AND x.mr_emp_code = ? ';
			//		array_push($params, (string)$data_search['receiver']);
			//	}



		
		$sql.=" 
		group by m.mr_work_main_id 
		order by m.mr_work_main_id asc
		";
		//echo $sql;
		//exit;
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function searchMailroom_upQury2021_11_29( $data_search )
	{
		$params = array();
		$sql=" 
		SELECT 
			m.mr_work_main_id,
			m.sys_timestamp,
			m.mr_work_date_sent,
			m.mr_work_barcode,
			m.mr_work_remark,
			m.mr_type_work_id,
			m.mr_status_id,
			m.mr_user_id as m_user_id,
			m.mr_floor_id as sen_floor_id,
			m.mr_round_id,
			m.mr_work_date_success,
			m.mr_topic,
			m.mr_branch_id as sen_branch_id,
			m.mr_send_work_id,
			m.messenger_user_id,
			type_work.mr_type_work_name,
			status.mr_status_name,
			i.rate_remark,
			rs.mr_round_name,
			CASE
					WHEN rsw.mr_round_resive_work_id is null  THEN m.quty
					ELSE count(rsw.mr_round_resive_work_id)
				END as count_qty,
				CASE
					WHEN i.rate_send is null  THEN \"\" 
					WHEN i.rate_send = 5 THEN \"พึ่งพอใจ\" 
					ELSE \"ไม่พึ่งพอใจ\"
				END as rate_send
		FROM mr_work_main m
		";
	$sql .= ' 	
		left join mr_work_inout i on(i.mr_work_main_id = m.mr_work_main_id)
		Left join mr_type_work type_work on( type_work.mr_type_work_id = m.mr_type_work_id )
		Left join mr_status status on (status.mr_status_id = m.mr_status_id)
		';
	//mr_round_resive_work
	$sql .= '
		left join mr_round_resive_work rsw on(rsw.mr_work_main_id = m.mr_work_main_id)
		left join mr_round rs on(rs.mr_round_id = rsw.mr_round_id)
	';
	

		//sender data
		$sql .= ' 
			Left join mr_user u_send on ( u_send.mr_user_id = m.mr_user_id )
			Left join mr_emp emp_send on ( emp_send.mr_emp_id = u_send.mr_emp_id )
			Left join mr_department dep_send on ( dep_send.mr_department_id = emp_send.mr_department_id )
				left join mr_branch b_send1 on ( b_send1.mr_branch_id = m.mr_branch_id )
				left join mr_branch b_send2 on ( b_send2.mr_branch_id = emp_send.mr_branch_id )
				Left join mr_floor f_send on ( f_send.mr_floor_id = m.mr_floor_id )	
				Left join mr_branch_type bt_send on(bt_send.mr_branch_type_id = (
						CASE  WHEN b_send1.mr_branch_type != ""
								THEN b_send1.mr_branch_type
								ELSE b_send2.mr_branch_type
							END
						)
					)
			
			';
			
		// // resive data
		// 	$sql .= ' 
		// 	Left join mr_emp emp_resive on ( emp_resive.mr_emp_id = i.mr_emp_id )
		// 	Left join mr_user u_resive on ( u_resive.mr_emp_id = emp_resive.mr_emp_id )
		// 	Left join mr_department dep_resive on ( dep_resive.mr_department_id = emp_resive.mr_department_id )
		// 		left join mr_branch b_resive1 on ( b_resive1.mr_branch_id = i.mr_branch_id )
		// 		left join mr_branch b_resive2 on ( b_resive2.mr_branch_id = emp_resive.mr_branch_id )
		// 		Left join mr_floor f_resive on ( f_resive.mr_floor_id = i.mr_floor_id )	
		// 		Left join mr_branch_type bt_resive on(bt_resive.mr_branch_type_id = (
		// 				CASE  WHEN b_resive1.mr_branch_type != ""
		// 						THEN b_resive1.mr_branch_type
		// 						ELSE b_resive2.mr_branch_type
		// 					END
		// 				)
		// 			)
		// 	';
			
		// 	// mess send data
		// 	$sql .= ' 
		// 	Left join mr_zone zone_send on ( zone_send.mr_floor_id = i.mr_floor_id)
		// 	Left join mr_user u_mess_send on ( u_mess_send.mr_user_id = zone_send.mr_user_id )
		// 	Left join mr_emp emp_mess_send on ( emp_mess_send.mr_emp_id = u_mess_send.mr_emp_id )
			
		// 	';
			
		// 	// mess resive data
		// 	$sql .= ' 
		// 	Left join mr_zone zone_resive on ( zone_resive.mr_floor_id = m.mr_floor_id)
		// 	Left join mr_user u_mess_resive on ( u_mess_resive.mr_user_id = zone_resive.mr_user_id )
		// 	Left join mr_emp emp_mess_resive on ( emp_mess_resive.mr_emp_id = u_mess_resive.mr_emp_id )
			
		// 	';

		$sql.=" group by m.mr_work_main_id 
		
		limit 0,30000";

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function searchUser( $data_search )
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				m.mr_work_date_sent,
				m.mr_work_date_success,
				m.mr_type_work_id,
				i.sys_timestamp as time_test,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'")  as mr_emp_lastname,
				t.mr_type_work_name,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				y.mr_department_name,
				y.mr_department_code,
				z.name as depart_floor_send,
				s.mr_status_name,
				x.mr_emp_id as emp_re,
				e.mr_emp_id as emp_send,
				i.mr_user_id as mr_user_id_re,
				e.mr_emp_id,
				u.mr_user_id,
				u.mr_user_role_id as mr_user_role_send,
				m.mr_work_main_id,
				concat(br.mr_branch_code,"-",br.mr_branch_name) as branch_re,
				concat(bs.mr_branch_code,"-",bs.mr_branch_name) as branch_sen
				
				
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			
			
			
			left join mr_emp 		e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status 	s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user 		u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work 	t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_branch 	br on ( i.mr_branch_id = br.mr_branch_id )
			Left join mr_floor 		f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp 		x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_branch 	bs on ( x.mr_branch_id = bs.mr_branch_id )
			Left join mr_floor 		z on ( z.mr_floor_id = m.mr_floor_id )
			
			WHERE
				m.mr_status_id <> 0
			
		';


		$sql .= ' AND ( u.mr_user_id = ? OR e.mr_emp_id = ? ) ';

		array_push($params, $data_search['user_id']);
		array_push($params, $data_search['mr_emp_id']);
		
		if( $data_search['barcode'] != "" ){
			$sql .= ' AND m.mr_work_barcode LIKE ? ';
			array_push($params, "%".trim($data_search['barcode'])."%");
		}
		if( $data_search['status'] != "" ){
			$sql .= ' AND m.mr_status_id = ? ';
			array_push($params, $data_search['status']);
		}
		
		
		if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
			//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
			
			$sql .= '  AND ( m.mr_work_date_sent BETWEEN ? AND ? )';
			
			array_push($params, $data_search['start_date']);
			array_push($params, $data_search['end_date']);
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
		}
		
		
		// if( $data_search['sender'] == "" && $data_search['receiver'] == ""  ){
			
		// } else {
			if( !empty($data_search['sender'])){
				$sql .= ' AND x.mr_emp_code = ? ';
				array_push($params, $data_search['sender']);
			}if( !empty($data_search['receiver'])){
				$sql .= ' AND e.mr_emp_code = ? ';
				array_push($params, $data_search['receiver']);
			}
		// }
		
		
		// if( $data_search['sender'] != ""  ){
		// 	$sql .= ' AND x.mr_emp_code = "'.$data_search['sender'].'" AND e.mr_emp_id ="'.$data_search['mr_emp_id'].'"';
		// }
		
		// if( $data_search['receiver'] != ""  ){
		// 	$sql .= ' AND e.mr_emp_code = "'.$data_search['receiver'].'" AND u.mr_user_id ="'.$data_search['user_id'].'"';
		// }
		
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
		// Execute Query
		if(!empty($params)) {
			$stmt->execute($params);  
		} else {
			$stmt->execute();  
		}
          

        return $stmt->fetchAll();
		
		
		
		//echo $sql;
        // return $this->_db->fetchAll($sql);
	}
	function searchBranch_manege( $data_search )
	{
		$params = array();

		$sql =
		'
			SELECT 
				br.mr_branch_code as mr_branch_code_re,
				br.mr_branch_name as mr_branch_name_re,
				br.mr_branch_type as mr_branch_type_id_re,
				bs.mr_branch_code as mr_branch_code_sen,
				bs.mr_branch_name as mr_branch_name_sen,
				m.sys_timestamp as main_sys_time,
				m.mr_type_work_id,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				m.mr_work_date_sent,
				m.mr_work_date_success,
				i.sys_timestamp as time_test,
				i.mr_branch_floor as mr_branch_floor_re,
				e.mr_emp_code as mr_emp_code_re,
				e.mr_emp_name as name_re,
				e.mr_emp_tel as tel_re,
				e.mr_emp_lastname as lastname_re,
				u.mr_user_id,
				x.mr_emp_code,
				x.mr_emp_name,
				x.mr_emp_lastname,
				x.mr_emp_tel,
				t.mr_type_work_name,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				y.mr_department_name,
				y.mr_department_code,
				z.name as depart_floor_send,
				s.mr_status_name,
				x.mr_emp_id as emp_send,
				e.mr_emp_id as emp_re,
				i.mr_user_id as mr_user_id_get,
				e.mr_emp_id,
				u.mr_user_role_id,
				m.mr_work_main_id,
				m.mr_status_id,
				er.mr_emp_code as emp_code_get,
				er.mr_emp_name as emp_name_get,
				er.mr_emp_lastname as emp_lastname_get,
				log.sys_timestamp as time_log,
				mai_re.sys_timestamp as time_mail_re,
				bts.mr_branch_type_id,
				bts.branch_type_name,
				concat("<a href=\'../employee/work_info.php?id=",m.mr_work_main_id,"\' target=\'_blank\'>",m.mr_work_barcode,"</a>") as link_click
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )

			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )

			Left join mr_branch bs on ( bs.mr_branch_id = x.mr_branch_id )
			Left join mr_branch_type bts on ( bts.mr_branch_type_id = bs.mr_branch_type )
			Left join mr_branch br on ( br.mr_branch_id = i.mr_branch_id )

			Left join mr_user ur on ( ur.mr_user_id = i.mr_user_id )
			Left join mr_emp er on ( er.mr_emp_id = ur.mr_emp_id )
			Left join (SELECT * FROM mr_work_log WHERE mr_status_id IN (5,12) ';
			if($data_search['start_date']!= ''){
				$sql .= ' and sys_timestamp >= ? '; 
				array_push($params, trim($data_search['start_date'])." 00:00:00");
			}
			if( $data_search['mr_work_main_id'] != "" ){
				$sql .= ' AND mr_work_main_id = ? ';
				array_push($params, $data_search['mr_work_main_id']);
			}
			$sql .= ' GROUP by mr_work_main_id) log on(log.mr_work_main_id = m.mr_work_main_id)';


			$sql .= 'Left join (SELECT * FROM mr_work_log WHERE mr_status_id IN (3,10) ';
			if($data_search['start_date']!= ''){
				$sql .= ' and sys_timestamp >= ? ';
				array_push($params, trim($data_search['start_date'])." 00:00:00");
			}
			if( $data_search['mr_work_main_id'] != "" ){
				$sql .= ' AND mr_work_main_id = ? ';
				array_push($params, $data_search['mr_work_main_id']);
			}
			$sql .= ' GROUP by mr_work_main_id) mai_re on(mai_re.mr_work_main_id = m.mr_work_main_id) ';

			$sql .= 'WHERE
			 m.mr_type_work_id != 1 and
				(i.mr_branch_id  is not null or u.mr_user_role_id = 5)
			
		';
		if($data_search['name_send_select'] != ''){
			$sql .= ' AND  x.mr_emp_id = ? ';
			array_push($params, $data_search['name_send_select']);
		}

		if($data_search['name_receiver_select'] != ''){
			$sql .= ' AND  e.mr_emp_id = ? ';
			array_push($params, $data_search['name_receiver_select']);
		}
		
		if($data_search['receiver_branch_id'] != ''){
			$sql .= ' AND i.mr_branch_id = ? ';
			array_push($params, $data_search['receiver_branch_id']);
		}

		if($data_search['send_branch_id'] != ''){
			$sql .= ' AND x.mr_branch_id = ? ';
			array_push($params, $data_search['send_branch_id']);
		}

		
		
		if( $data_search['barcode'] != "" ){
			$sql .= ' AND m.mr_work_barcode LIKE ? ';
			array_push($params, "%".trim($data_search['barcode'])."%");
		}
		if( $data_search['mr_work_main_id'] != "" ){
			$sql .= ' AND m.mr_work_main_id = ? ';
			array_push($params, $data_search['mr_work_main_id']);
		}
		
		
		
		if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
			//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
			
			$sql .= '  AND ( m.mr_work_date_sent BETWEEN ? AND ? )';
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
			array_push($params, trim($data_search['start_date'])." 00:00:00");
			array_push($params, trim($data_search['end_date'])." 23:59:59");
		}
		$sql .= ' group by m.mr_work_main_id';


		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
        // Execute Query
        $stmt->execute($params);    

        return $stmt->fetchAll();
		//echo $sql;
        // return $this->_db->fetchAll($sql);
	}
	
	

	
	function mailroom_getreport_level1( $data_search )
	{
		$params = array();
		$sql =
		'
			SELECT 
				br.mr_branch_code as mr_branch_code_re,
				br.mr_branch_name as mr_branch_name_re,
				br.mr_branch_type as mr_branch_type_id_re,
				bs.mr_branch_code as mr_branch_code_sen,
				bs.mr_branch_name as mr_branch_name_sen,
				bs.mr_branch_type as mr_branch_type_id_send,
				m.sys_timestamp as main_sys_time,
				m.mr_type_work_id,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				m.mr_work_date_sent,
				m.mr_work_date_success,
				i.sys_timestamp as time_test,
				i.mr_branch_floor as mr_branch_floor_re,
				e.mr_emp_code as mr_emp_code_re,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				u.mr_user_id,
				x.mr_emp_code,
				x.mr_emp_name,
				x.mr_emp_lastname,
				x.mr_emp_tel,
				t.mr_type_work_name,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				y.mr_department_name,
				y.mr_department_code,
				z.name as depart_floor_send,
				s.mr_status_name,
				x.mr_emp_id as emp_send,
				e.mr_emp_id as emp_re,
				i.mr_user_id as mr_user_id_get,
				e.mr_emp_id,
				u.mr_user_role_id,
				m.mr_work_main_id,
				m.mr_status_id,
				er.mr_emp_code as emp_code_get,
				er.mr_emp_name as emp_name_get,
				er.mr_emp_lastname as emp_lastname_get,
				bts.mr_branch_type_id,
				bts.branch_type_name,
				log.sys_timestamp as time_log,
				mai_re.sys_timestamp as time_mail_re,
				concat("<a href=\'../employee/work_info.php?id=",m.mr_work_main_id,"\' target=\'_blank\'>",m.mr_work_barcode,"</a>") as link_click
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			Left join mr_branch bs on ( bs.mr_branch_id = x.mr_branch_id )
			Left join mr_branch_type bts on ( bts.mr_branch_type_id = bs.mr_branch_type )
			Left join mr_branch br on ( br.mr_branch_id = i.mr_branch_id )
			Left join mr_user ur on ( ur.mr_user_id = i.mr_user_id )
			Left join mr_emp er on ( er.mr_emp_id = ur.mr_emp_id )
			Left join (SELECT * FROM mr_work_log WHERE mr_status_id IN (5,12) ';
			if($data_search['start_date']!= ''){
				$sql .= ' and sys_timestamp >= ? '; 
				array_push($params, (string)$data_search['start_date'].' 00:00:00');
			}
			if( $data_search['mr_work_main_id'] != "" ){
				$sql .= ' AND mr_work_main_id = ? ';
				array_push($params, (int)$data_search['mr_work_main_id']);
			}
			$sql .= ' GROUP by mr_work_main_id) log on(log.mr_work_main_id = m.mr_work_main_id)';


			$sql .= 'Left join (SELECT * FROM mr_work_log WHERE mr_status_id IN (3,11) ';
			if($data_search['start_date']!= ''){
				$sql .= ' and sys_timestamp >= ? ';
				array_push($params, (string)$data_search['start_date'].' 00:00:00');
			}
			if( $data_search['mr_work_main_id'] != "" ){
				$sql .= ' AND mr_work_main_id = ? ';
				array_push($params, (int)$data_search['mr_work_main_id']);
			}
			$sql .= ' GROUP by mr_work_main_id) mai_re on(mai_re.mr_work_main_id = m.mr_work_main_id) ';

			$sql .= 'WHERE
			 m.mr_type_work_id >= 1 
			
		';
		if( $data_search['type'] == "ho"){
			$sql .= ' and u.mr_user_role_id = 2';
		}else{
			$sql .= ' and u.mr_user_role_id != 2';
		}


		if( $data_search['start_date'] != "" || $data_search['end_date'] != "" ){
			//$sql .= ' AND m.mr_status_id = "'.$data_search['status'].'"';
			
			$sql .= '  AND ( m.sys_timestamp BETWEEN ? AND ? )';
			//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
			array_push($params, (string)$data_search['start_date']);
			array_push($params, (string)$data_search['end_date']);
		}
		$sql .= ' group by m.mr_work_main_id';


		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}



	function getRecievMailroom( )
	{
		$sql =
		'
			SELECT 
				m.*,
				m.sys_timestamp as time_send,
				i.*,
				i.sys_timestamp as time_test,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				u.*,
				x.*,
				t.mr_type_work_name,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				d.mr_department_floor as depart_floor_receive,
				y.*,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			
			WHERE
				m.mr_status_id in( 1,2 ) 
				AND ( i.mr_status_receive = 1 or i.mr_status_receive IS NULL)
		';
		
		
		//echo $sql;
        return $this->_db->fetchAll($sql);
	}
	
	
	
	function getWorkPrintFrom( $depart_id )
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.*,
				i.*,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				e.mr_emp_code as mr_emp_code,
				u.*,
				x.*,
				t.*,
				d.mr_department_id,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				d.mr_department_floor as depart_floor_receive,
				y.*,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			
			WHERE
				m.mr_status_id = 4
		';
		
		
		if( $depart_id ){
			$sql .= ' AND d.mr_department_id = ? ';
			array_push($params, (int)$depart_id);
		}
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	
	function getWorkPrintDepart( )
	{
		$sql =
		'
			SELECT 
				distinct(d.mr_department_id)
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			WHERE
				m.mr_status_id = 4
			
		';
		//echo $sql;
        return $this->_db->fetchAll($sql);
	}
	
	
	
	
	
	/////////////////////////////////// Mess ///////////////////////////////////
	
	
	function getWorkByMainID( $id )
	{
		$params = array();
		$sql =
		'
			SELECT 
				mr_work_inout_id
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			WHERE
				m.mr_work_main_id = ?
		';

		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}

	function getWorkByMainIDAll( $id )
	{
		$params = array();
		$sql =
		'
			SELECT 
				i.mr_work_inout_id,
				i.mr_status_send,
				i.mr_status_receive,
				m.mr_status_id
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			WHERE
				m.mr_work_main_id = ?
		';

		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}
	
	
	// ===================================== mailroom =============================
	
	function printMessSendWork( $user_id ) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.*,
				i.*,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				e.mr_emp_code as mr_emp_code_re,
				u.*,
				x.*,
				d.mr_department_id,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				d.mr_department_floor as depart_floor_receive,
				y.*
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			
			
		WHERE
			m.mr_status_id = 3
		';
			
		if( $user_id != 0 ){
			$sql .= ' AND d.mr_user_id = ? ';
			array_push($params, (int)$user_id);
		}	
				
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();

	}
	
	// ======================== excel emp ========================
	//======================= Send =========================
	
	function reportEmpMonthSend( $month ,$user_id )
	{
		$params = array();

		$sql =
		'
			SELECT 
				m.*,
				i.*,
				u.*,
				t.*,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				z.name as depart_floor_send,
				y.*,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'")  as mr_emp_lastname,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			
			
			WHERE
				m.sys_timestamp LIKE ? AND
				m.mr_user_id = ?
		';

		array_push($params, trim($month)."%");
		array_push($params, $user_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
        // Execute Query
        $stmt->execute($params);    

        return $stmt->fetchAll();
		
		
		//echo $sql;
        // return $this->_db->fetchAll($sql);
	}
	
	//======================= Receive =========================
	
	function reportEmpMonthReceive( $month ,$user_id )
	{
		$params = array();

		$sql =
		'
			SELECT 
				m.*,
				i.*,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'")  as mr_emp_lastname,
				u.*,
				t.*,
				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				z.name as depart_floor_send,
				y.*,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			
			
			WHERE
				m.sys_timestamp LIKE ? AND
				i.mr_emp_id = ?
		';

		array_push($params, trim($month)."%");
		array_push($params, $user_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
        // Execute Query
        $stmt->execute($params);    

        return $stmt->fetchAll();
		


		//echo $sql;
        // return $this->_db->fetchAll($sql);
	}
	
		
	
	
	// =============== march =========================

	function getSendMailroom($user_id) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				d.mr_department_name,
				f.name as mr_department_floor,
				m.*,
				i.*,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(x.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(x.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				u.*,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			
		
			WHERE
				m.mr_status_id in (2,3,4) AND 
				d.mr_user_id = ?
			ORDER BY i.mr_status_send ASC
		';

		array_push($params, (int)$user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	} 
	
	function getSendMailroom2($user_id) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				d.mr_department_name,
				f.name as mr_department_floor,
				m.*,
				i.*,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(x.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(x.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				u.*,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			Left join mr_department w on ( w.mr_floor_id = i.mr_floor_id )
			
			WHERE
				m.mr_status_id in (2,3,4) AND 
				( d.mr_user_id = ? OR y.mr_user_id = ? OR w.mr_user_id = ? )
			group by m.mr_work_main_id
			ORDER BY i.mr_status_send ASC
		';

		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	} 
	
	
	
	
	function searchWorkdataSend($txt, $user_id) 
	{

		$params = array();
		$sql =
		'
			SELECT 
			m.*,
			i.*,
			
			AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
			AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
			AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
			AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

			AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
			AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
			AES_DECRYPT(x.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
			AES_DECRYPT(x.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

			u.*,
			t.*,
			d.mr_department_name,
			d.mr_department_floor,
			s.mr_status_name
		FROM mr_work_inout i
		left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
		left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
		Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
		Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
		Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
		Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
		Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
		Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
		
		
		WHERE
			(m.mr_work_barcode LIKE ? OR
			e.mr_emp_name LIKE ? OR
			e.mr_emp_lastname LIKE ? OR
			d.mr_department_floor LIKE ? ) AND
			m.mr_status_id in (2,4) AND 
			y.mr_user_id = ?
		ORDER BY i.mr_status_send ASC
		';

		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (int)$user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function getReceiveMailroom($user_id)
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_topic,
				i.sys_timestamp,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				d.mr_department_name,
				f.name as mr_department_floor,
				s.mr_status_name,
				i.mr_status_receive
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = m.mr_floor_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = m.mr_floor_id )
			WHERE
				m.mr_status_id = 1 AND 
				z.mr_user_id = ?
			ORDER BY i.mr_status_receive ASC, m.mr_work_date_sent DESC
		';

		array_push($params, (int)$user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	

	function receiveMailroomForMessenger($user_id)
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.*,
				i.*,
				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				e.mr_emp_tel as tel_re,
				e.mr_emp_mobile as mobile_re,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				t.*,
				d.mr_department_name,
				f.name as mr_department_floor,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			
			WHERE
				m.mr_status_id = 3 AND 
				( d.mr_user_id = ? OR y.mr_user_id = ?)
				
			ORDER BY i.mr_status_receive ASC, m.mr_work_date_sent DESC
		';

		array_push($params, (int)$user_id);
		array_push($params, (int)$user_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function searchWorkdataReceive($txt, $user_id) 
	{
		$params = array();
		$sql =
		'
		SELECT 
		m.mr_work_main_id,
		m.mr_work_date_sent,
		m.mr_work_barcode,
		m.mr_work_remark,
		m.mr_topic,
		i.sys_timestamp,

		e.mr_emp_name,
		e.mr_emp_lastname,
		e.mr_emp_tel ,
		e.mr_emp_mobile,

		AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
		AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
		AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
		AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

		d.mr_department_name,
		f.name as mr_department_floor,
		s.mr_status_name,
		i.mr_status_receive,
		z.*
	FROM mr_work_main m
	left join mr_work_inout i on ( i.mr_work_main_id = m.mr_work_main_id )
	Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
	left join mr_emp e on ( e.mr_emp_id = u.mr_emp_id )
	Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
	Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
	Left join mr_floor f on ( f.mr_floor_id = m.mr_floor_id )
	LEFT Join mr_zone z on ( z.mr_floor_id = m.mr_floor_id )
	WHERE
		m.mr_status_id = 1 AND 
		z.mr_user_id = ?
		and (m.mr_work_barcode LIKE ? OR
		e.mr_emp_name LIKE ? OR
		e.mr_emp_lastname LIKE ? OR
		d.mr_department_floor LIKE ? )
	ORDER BY i.mr_status_receive ASC, m.mr_work_date_sent DESC
	';

		array_push($params, (int)$user_id);
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	// MARCH MARCH MARCH
	function searchAutoSummary($data)
	{	
		$params = array();
		$today = date('Y-m-d');
		// $sql =
		// '
		// 	SELECT 
		// 		m.*,
		// 		i.*,
		// 		e.mr_emp_name as name_re,
		// 		e.mr_emp_lastname as lastname_re,
		// 		e.mr_emp_tel as tel_re,
		// 		e.mr_emp_mobile as mobile_re,
		// 		u.*,
		// 		x.*,
		// 		t.*,
		// 		d.mr_department_name,
		// 		f.name as mr_department_floor,
		// 		fs.name as mr_department_sender_floor,
		// 		s.mr_status_name
		// 	FROM mr_work_inout i
		// 	left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
		// 	left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
		// 	Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
		// 	Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
		// 	Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
		// 	Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
		// 	Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
		// 	Left join mr_floor fs on ( fs.mr_floor_id = m.mr_floor_id )
		// 	join(
		// 			select
		// 				*
		// 			from mr_emp
		// 		)x on ( x.mr_emp_id = u.mr_emp_id )
		// 	join(
		// 			select
		// 				*
		// 			from mr_department
		// 	)y on ( x.mr_department_id = y.mr_department_id )
		// 	join(
		// 			select
		// 				*
		// 			from mr_floor
		// 	)z on ( z.mr_floor_id = m.mr_floor_id )
		// 	join(
		// 			select
		// 				*
		// 			from mr_floor
		// 	)z2 on ( z2.mr_floor_id = i.mr_floor_id )
			
		// ';
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id 
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)

				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
			where
				(usr.mr_user_role_id = 3 OR usr2.mr_user_role_id = 3)
		';
		if($data['start_date'] != ''){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';

			array_push($params, (string)$today.'%');
		}
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function searchAutoSummary_status_1_to_3($data)
	{	
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				m.mr_user_id as main_user_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id,
				m.sys_timestamp
				
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)

				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
			where  m.mr_type_work_id = 1';
			
		
		if($data['start_date'] != ''){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';

			array_push($params, (string)$today.'%');
		}
		$sql .= ' group BY m.mr_work_main_id';
		

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function searchAutoSummary_status_4_to_6($data)
	{	
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id,
				sum(case WHEN m.mr_status_id = 4  THEN 1 ELSE 0 END ) as msg_send ,
				sum(case WHEN m.mr_status_id = 5  THEN 1 ELSE 0 END ) as success 
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)

				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
			where
				usr.mr_user_role_id = 3 OR usr2.mr_user_role_id = 3
		';
		if($data['start_date'] != ''){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		$sql .= ' group BY i.mr_floor_id';
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function searchAutoSummary_branch_to_branch($data)
	{	
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				m.mr_type_work_id,
				concat(b.mr_branch_code," : ",b.mr_branch_name) as mr_branch_name,
				b.mr_branch_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id
				
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				left join mr_user usr3 on (usr3.mr_user_id = m.mr_user_id)
				left join mr_emp e on (e.mr_emp_id = usr3.mr_emp_id)
				left join mr_branch b on (b.mr_branch_id = e.mr_branch_id)
				
				
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)

				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
				
				
			where
				m.mr_branch_id != ""
				and m.mr_branch_id is not null
				and m.mr_type_work_id = 2
				and m.mr_status_id in(6,7,8,9,10,11,12,13,14,15)
		';
		if($data['start_date'] != ''){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		$sql .= ' group BY m.mr_work_main_id';
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function searchAutoSummary_branch_to_HO($data)
	{	
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				m.mr_type_work_id,
				concat(b.mr_branch_code," : ",b.mr_branch_name) as mr_branch_name,
				b.mr_branch_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id
				
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				left join mr_user usr3 on (usr3.mr_user_id = m.mr_user_id)
				left join mr_emp e on (e.mr_emp_id = usr3.mr_emp_id)
				left join mr_branch b on (b.mr_branch_id = e.mr_branch_id)
				
				
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)

				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
				
				
			where
				m.mr_branch_id != ""
				and m.mr_branch_id is not null
				and usr3.mr_user_role_id = 5
		';
		if($data['start_date'] != ''){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		$sql .= ' group BY m.mr_work_main_id';
		

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();

	}
	
	function searchAutoSummary_HO_to_branch($data)
	{	
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				m.mr_type_work_id,
				concat(b.mr_branch_code," : ",b.mr_branch_name) as mr_branch_name,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id
				
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				left join mr_user usr3 on (usr3.mr_user_id = m.mr_user_id)
				left join mr_emp e on (e.mr_emp_id = usr3.mr_emp_id)
				left join mr_branch b on (b.mr_branch_id = e.mr_branch_id)
				
				
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)

				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
				
				
			where
				usr3.mr_user_role_id = 2
				and m.mr_type_work_id = 2
		';
		if($data['start_date'] != ''){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		$sql .= ' group BY m.mr_work_main_id';
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function detailSummary($data)
	{
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			SELECT 
				m.*,
				i.*,
				i.sys_timestamp as time_true,
		

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(x.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(x.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				u.*,
				t.*,
				d.mr_department_name,
				f.name as mr_department_floor,
				fs.name as mr_department_sender_floor,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_floor fs on ( fs.mr_floor_id = m.mr_floor_id )
			join(
					select
						*
					from mr_emp
				)x on ( x.mr_emp_id = u.mr_emp_id )
			join(
					select
						*
					from mr_department
			)y on ( x.mr_department_id = y.mr_department_id )
			join(
					select
						*
					from mr_floor
			)z on ( z.mr_floor_id = m.mr_floor_id )
		';
		if( $data['start_date'] != '--' ){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		
		if(intval($data['status_id']) != 0 || $data['status_id'] != '') {
			$sql .= ' AND m.mr_status_id = ? ';
			array_push($params, (int)$data['status_id'] );
		}

		if($data['mr_department_floor'] != '') {
			$sql .= ' AND f.name = ?  ';
			array_push($params, (string)$data['mr_department_floor'] );
		} else {
			$sql .= ' AND d.name = ""';
		}
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	
	}

	function detailSummarySender($data)
	{
		$params = array();
		$today = date('Y-m-d');
		// $sql =
		// '
		// 	SELECT 
		// 		m.*,
		// 		i.*,
		// 		i.sys_timestamp as time_true,
		// 		e.mr_emp_name as name_re,
		// 		e.mr_emp_lastname as lastname_re,
		// 		e.mr_emp_tel as tel_re,
		// 		e.mr_emp_mobile as mobile_re,
		// 		u.*,
		// 		x.*,
		// 		t.*,
		// 		d.mr_department_name,
		// 		f.name as mr_department_floor,
		// 		fs.name as mr_department_sender_floor,
		// 		s.mr_status_name
		// 	FROM mr_work_inout i
		// 	left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
		// 	left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
		// 	Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
		// 	Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
		// 	Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
		// 	Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
		// 	Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
		// 	Left join mr_floor fs on ( fs.mr_floor_id = m.mr_floor_id )
		// 	join(
		// 			select
		// 				*
		// 			from mr_emp
		// 		)x on ( x.mr_emp_id = u.mr_emp_id )
		// 	join(
		// 			select
		// 				*
		// 			from mr_department
		// 	)y on ( x.mr_department_id = y.mr_department_id )
		// 	join(
		// 			select
		// 				*
		// 			from mr_floor
		// 	)z on ( z.mr_floor_id = m.mr_floor_id )
		// ';
		$sql =
		'
			select 
				m.mr_round_id,
				m.mr_work_remark,
				i.sys_timestamp as time_true,
				m.mr_work_barcode,
				f2.name as mr_department_floor ,
				f.name as mr_department_sender_floor,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(e2.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e2.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e2.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e2.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				usr.*,
				t.*,
				i.*,
				s.*
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
				left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
				left join mr_department d on ( d.mr_department_id = e.mr_department_id )
				left join mr_user usr on (usr.mr_user_id = m.mr_user_id)
				left join mr_emp e2 on ( e2.mr_emp_id = usr.mr_emp_id )
				left join mr_status s on ( m.mr_status_id = s.mr_status_id )
				left join mr_floor f2 on ( f2.mr_floor_id = i.mr_floor_id )
			 	left join mr_floor f on ( f.mr_floor_id = m.mr_floor_id )
			where
				i.mr_work_inout_id is not null
				and m.mr_type_work_id = 1
		';
		if( $data['start_date'] != '--' ){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		
		if($data['status_id'] == 0) {
			$sql .= ' AND m.mr_status_id in(1,2,3,4,5,6)';
		}else if($data['status_id'] != '') {
			$sql .= ' AND m.mr_status_id = ? ';
			array_push($params, (int)$data['status_id']);
		}
		
		
		if($data['mr_department_floor'] == 'no'){ 
			$sql .= ' AND (m.mr_floor_id is null and  i.mr_floor_id is null )  ';
		}elseif($data['mr_department_floor'] != '') {
			if(intval($data['status_id']) <= 3){
				$sql .= ' AND (m.mr_floor_id = ? )  ';
				array_push($params, (int)$data['mr_department_floor']);

			}else if(intval($data['status_id']) == 3){
				$sql .= ' AND ( m.mr_floor_id = ? or (i.mr_floor_id = ? and m.mr_floor_id is null )) ';
				array_push($params, (int)$data['mr_department_floor']);
				array_push($params, (int)$data['mr_department_floor']);
			}else{
				$sql .= ' AND (i.mr_floor_id = ? or (m.mr_floor_id = ? and i.mr_floor_id is null ))  ';
				array_push($params, (int)$data['mr_department_floor']);
				array_push($params, (int)$data['mr_department_floor']);
			}
			
		}else {
			$sql .= ' AND m.mr_floor_id = ""';
		}
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	
	}
	
	function detailSummarySender_vMos($data)
	{
		$params = array();
		$today = date('Y-m-d');
		
		$sql =
		'
			select 
				m.mr_round_id,
				m.mr_work_remark,
				i.sys_timestamp as time_true,
				m.mr_work_barcode,
				f.name as mr_department_floor ,
				f2.name as mr_department_sender_floor,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(e2.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e2.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e2.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e2.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,
				usr.*,
				t.*,
				i.*,
				s.*
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
				left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
				left join mr_department d on ( d.mr_department_id = e.mr_department_id )
				left join mr_user usr on (usr.mr_user_id = m.mr_user_id)
				left join mr_emp e2 on ( e2.mr_emp_id = usr.mr_emp_id )
				left join mr_status s on ( m.mr_status_id = s.mr_status_id )
				left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			 	left join mr_floor f2 on ( f2.mr_floor_id = m.mr_floor_id )
			where
				i.mr_work_inout_id is not null
		';
		if( $data['start_date'] != '--' ){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		
		if(intval($data['status_id']) != 0 and $data['status_id'] != '') {
			$sql .= ' AND m.mr_status_id = ? ';
			array_push($params, (int)$data['status_id']);
		}

		if($data['mr_department_floor'] != '' and $data['mr_department_floor'] != 'รวม') {
			if(intval($data['status_id']) <= 0){
				$sql .= ' AND (f.name is not null and f2.name is not null )';
			}elseif(intval($data['status_id']) <= 3){
				$sql .= ' AND f2.name like ? ';
				array_push($params, (string)$data['mr_department_floor']);

			}elseif(intval($data['status_id']) == 6){
				$sql .= ' AND (f.name like ?  OR f2.name like ? ) ';
				array_push($params, (string)$data['mr_department_floor']);
				array_push($params, (string)$data['mr_department_floor']);
			}else{
				$sql .= ' AND f.name like ? ';
				array_push($params, (string)$data['mr_department_floor']);
			}
		} 
		if($data['mr_department_floor'] == 'รวม') {
			if(intval($data['status_id']) <= 0){
				$sql .= ' AND ((m.mr_status_id <= 3 and f2.name is not null) or (m.mr_status_id in(4,5,6) and f.name is not null))';
				//$sql .= ' AND (f.name is not null or f2.name is not null )';
			}elseif(intval($data['status_id']) <= 3){
				$sql .= ' AND f2.name is not null';
			}else{
				$sql .= ' AND f.name is not null';
			}
		}
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	
	}
	
	
	
	function getCheckWorkByMainID( $id )
	{
		$params = array();
		$sql =
		'
			SELECT 
				e.mr_emp_code
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			WHERE
				m.mr_work_main_id = ?
		';

		array_push($params, (int)$id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	}

	function getUserReceive( $id )
	{
		$params = array();

		$sql =
		'
			SELECT
				i.*,

				AES_DECRYPT(e2.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(e2.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(e2.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(e2.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				e2.mr_emp_code,
				u2.mr_user_id as mr_user_id2
			FROM mr_work_inout i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				Left join mr_user u on ( u.mr_user_id = i.mr_user_id )
				left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
				left join mr_confirm_log cl on ( cl.mr_work_main_id = m.mr_work_main_id )
				Left join mr_emp e2 on ( e2.mr_emp_id = cl.mr_emp_id )
				Left join mr_user u2 on ( u2.mr_emp_id = e2.mr_emp_id )
			WHERE
				m.mr_work_barcode = ?
			
		';
		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		
		array_push($params, $id);
		
        // Execute Query
		$stmt->execute($params);
		
		//echo $sql;
        return $stmt->fetch();
	}

	// ============================== MARCH =====================================

	function reportAllMonthlySend($month)
	{
		$params = array();
		$sql =
		'
			SELECT 
				m.mr_work_date_success,
				m.mr_work_main_id,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
				m.mr_work_date_sent,
				t.mr_type_work_name,
				m.sys_timestamp as time_order,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(x.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(x.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				d.mr_department_name as depart_name_receive,
				d.mr_department_code as depart_code_receive,
				f.name as depart_floor_receive,
				z.name as depart_floor_send,
				s.mr_status_name,
				y.mr_department_code,
				y.mr_department_name,
				
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_type_work t on ( t.mr_type_work_id = m.mr_type_work_id )
			Left join mr_department d on ( e.mr_department_id = d.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			Left join mr_floor z on ( z.mr_floor_id = m.mr_floor_id )
			
			WHERE
				m.sys_timestamp LIKE ?
		';

		array_push($params, (string)$month.'%');
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	
	
	function searchAutoSLA0($data)
	{	
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			SELECT 
				m.mr_work_main_id,
				m.sys_timestamp as order_time,
				m.mr_work_barcode,
				r.sys_timestamp as mess_re,
				l.sys_timestamp as success
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Join(
					select
						*
					from  mr_work_log
					where mr_status_id = 2
				)r on ( r.mr_work_main_id = m.mr_work_main_id )
			Join(
					select
						*
					from  mr_work_log
					where mr_status_id = 5
				)l on ( l.mr_work_main_id = m.mr_work_main_id )
			
			
			
		';
		if($data['start_date'] != ''){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	function getMainID_SendMailroomFinal($date=array()) 
	{
		$params = array();
		$sql =
		'SELECT 
				GROUP_CONCAT(mr_work_main_id) as id 
			FROM mr_work_log WHERE sys_timestamp 
			BETWEEN ? AND ? 
			AND mr_status_id = 3';		
		array_push($params, (string)$date['date_s'].' 00:00:00');
		array_push($params, (string)$date['date_e'].' 23:59:59');
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetch();
	} 
	
	function getSendMailroomFinal($user_id,$txt=null,$date=array(),$main_id=null) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				d.mr_department_name,
				f.name as mr_department_floor,
				m.mr_work_main_id,
				m.mr_work_barcode,
				m.mr_work_date_sent,
				m.mr_topic,
				m.mr_work_remark,
				i.mr_status_receive,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(x.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(x.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_round_resive_work lr on ( m.mr_work_main_id = lr.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = i.mr_floor_id )
			WHERE
				m.mr_status_id in (3) 
				AND m.mr_type_work_id NOT IN (4,5,6) 
				AND z.mr_user_id = ? ';
				array_push($params, (int)$user_id);

			if(!empty ($date)){
				if($main_id != ''){
					$sql .= "
					and(
						m.mr_work_main_id in('.$main_id.')
						or(
							m.sys_timestamp BETWEEN ? AND ?
							and m.mr_status_id in (3)
						)
					)";
					array_push($params, (string)$date['date_s'].' 00:00:00');
					array_push($params, (string)$date['date_e'].' 23:59:59');
				}else{
					$sql .= "
					and(
						m.sys_timestamp BETWEEN ? AND ?
						and m.mr_status_id in (3)	
					)";
					array_push($params, (string)$date['date_s'].' 00:00:00');
					array_push($params, (string)$date['date_e'].' 23:59:59');

				}
			}

			if($txt != ''){
				$sql .= ' and (m.mr_work_barcode like ? or e.mr_emp_name like ? or e.mr_emp_lastname like ? or f.name like ? )	';
				array_push($params, (string)'%'.$txt.'%');
				array_push($params, (string)'%'.$txt.'%');
				array_push($params, (string)'%'.$txt.'%');
				array_push($params, (string)'%'.$txt.'%');
			}
			$sql .= '
			
			group by m.mr_work_main_id
			ORDER BY m.mr_work_barcode ASC	
			
			';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        
		// Execute Query
		$stmt->execute($params);

		return $stmt->fetchAll();
	} 
	
	function getSendToReciever($user_id,$txt=null) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				d.mr_department_name,
				f.name as mr_department_floor,
				m.*,
				i.*,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,

				AES_DECRYPT(x.mr_emp_name, "'.$this->_secret->database->key.'") as mr_emp_name,
				AES_DECRYPT(x.mr_emp_lastname, "'.$this->_secret->database->key.'") as mr_emp_lastname,
				AES_DECRYPT(x.mr_emp_tel, "'.$this->_secret->database->key.'") as mr_emp_tel,
				AES_DECRYPT(x.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mr_emp_mobile,

				e.mr_emp_name as name_re,
				e.mr_emp_lastname as lastname_re,
				e.mr_emp_tel as tel_re,
				e.mr_emp_mobile as mobile_re,
				u.*,
				s.mr_status_name
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_status s on ( m.mr_status_id = s.mr_status_id )
			Left join mr_user u on ( u.mr_user_id = m.mr_user_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			Left join mr_emp x on ( x.mr_emp_id = u.mr_emp_id )
			Left join mr_department y on ( x.mr_department_id = y.mr_department_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = i.mr_floor_id )
			WHERE
				m.mr_status_id in (4) AND 
				z.mr_user_id = ?
				and (m.mr_work_barcode like ?
				or e.mr_emp_name like ?
				or e.mr_emp_lastname like ? )
			GROUP by m.mr_work_main_id
			ORDER BY m.mr_work_barcode ASC
			
		';

		array_push($params, (int)$user_id);
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');
		array_push($params, (string)'%'.$txt.'%');
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	} 
	
	function getEditFloorSend($work_inout_id) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				d.mr_department_name,
				f.name as mr_department_floor,
				i.*,
			
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'")  as mr_emp_email,

				e.mr_emp_code as emp_code,
				d.mr_department_id,
				m.mr_type_work_id,
				m.mr_status_id,
				m.mr_status_id
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = i.mr_floor_id )
			WHERE
				i.mr_work_inout_id = ?
		';

		array_push($params, (int)$work_inout_id);

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	} 
	

	// =================== MARCH 27032018 ==============================

	function getSummaryMessenger($data)
	{
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id as status
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)

				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
			where
				usr.mr_user_role_id = 3 OR usr2.mr_user_role_id = 3
		';
		
		if( $data['start_date'] != '' ){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
		
	}
	function getSummaryMessenger_mosUpdate($data)
	{
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
			select 
				m.mr_floor_id as floor_main_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id as status
			from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)
				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
			where
				m.mr_status_id is not null
				and m.mr_type_work_id = 1
		
		';

		if( $data['start_date'] != '' ){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
			
		}
		
		$sql .= ' group by m.mr_work_main_id ';

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function detailSummaryMessenger($data)
	{
		$params = array();
		$today = date('Y-m-d');
		$sql =
		'
		select 
				m.sys_timestamp,
				m.mr_work_barcode,
				CONCAT(AES_DECRYPT(emp.mr_emp_name, "'.$this->_secret->database->key.'"),"  ", AES_DECRYPT(emp.mr_emp_lastname, "'.$this->_secret->database->key.'")) as sender,
				CONCAT(AES_DECRYPT(emp2.mr_emp_name, "'.$this->_secret->database->key.'"),"  ", AES_DECRYPT(emp2.mr_emp_lastname, "'.$this->_secret->database->key.'")) as receiver,
				m.mr_floor_id as floor_main_id,
				z.mr_user_id as usr_main_id,
				f.name as floor_name_main,
				f3.name as floor_name_sender,
				f4.name as floor_name_receiver,
				usr.mr_user_username as username_main,
				i.mr_floor_id as floor_inout_id,
				z2.mr_user_id as usr_inout_id,
				f2.name as floor_name_inout,
				usr2.mr_user_username as username_inout,
				m.mr_status_id as status,
				sts.mr_status_name,
				m.mr_topic,
				i.rate_remark,
				m.mr_round_id,
				tw.mr_type_work_name
		from mr_work_inout as i
				left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
				left join mr_zone z on (z.mr_floor_id = m.mr_floor_id)
				left join mr_floor f on (f.mr_floor_id = z.mr_floor_id)
				left join mr_zone z2 on (z2.mr_floor_id = i.mr_floor_id)
				left join mr_floor f2 on (f2.mr_floor_id = z2.mr_floor_id)
				left join mr_floor f3 on (f3.mr_floor_id = m.mr_floor_id)
				left join mr_floor f4 on (f4.mr_floor_id = i.mr_floor_id)
				left join mr_user usr on (usr.mr_user_id = z.mr_user_id)
				left join mr_user usr2 on (usr2.mr_user_id = z2.mr_user_id)
				left join mr_user usr3 on (usr3.mr_user_id = m.mr_user_id)
				left join mr_user usr4 on (usr4.mr_user_id = i.mr_user_id)
				left join mr_emp emp on (emp.mr_emp_id = usr3.mr_emp_id)
				left join mr_emp emp2 on (emp2.mr_emp_id = i.mr_emp_id)
				left join mr_status sts on (sts.mr_status_id = m.mr_status_id)
				left join mr_type_work tw on (tw.mr_type_work_id = m.mr_type_work_id)
				
		where
		m.mr_status_id is not null
		and m.mr_type_work_id = 1
		';

		if( $data['start_date'] != '' ){
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$sql .= ' AND (m.sys_timestamp BETWEEN ? AND ? )';
			array_push($params, (string)$data['start_date'].' 00:00:00');
			array_push($params, (string)$data['end_date'].' 23:59:59');
		}
		else{
			$sql .= ' AND m.sys_timestamp LIKE ? ';
			array_push($params, (string)$today.'%');
		}

		if($data['mr_status_id'] != 0) {
			$sql .= ' AND m.mr_status_id = ? ';
			array_push($params, (int)$data['mr_status_id']);
			
		}
		//$sql .= ' AND (z.mr_user_id ='.$data['mr_user_id'].' or z2.mr_user_id ='.$data['mr_user_id'].') ';
		//$sql .= ' AND (z.mr_user_id ='.$data['mr_user_id'].') ';
		//$sql .= ' AND (z2.mr_user_id ='.$data['mr_user_id'].') ';
		
		if($data['mr_status_id'] <= 3){
			$sql .= ' AND (z.mr_user_id = ? or (z2.mr_user_id = ? and (z.mr_user_id = 0 or z.mr_user_id is null))) ';
			array_push($params, (int)$data['mr_user_id']);
			array_push($params, (int)$data['mr_user_id']);
		}else{
			$sql .= ' AND (z2.mr_user_id = ? or (z.mr_user_id = ? and (z2.mr_user_id = 0 or z2.mr_user_id is null) )) ';
			array_push($params, (int)$data['mr_user_id']);
			array_push($params, (int)$data['mr_user_id']);
		}
		
		$sql.=" group by m.mr_work_main_id ";

		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	
	
	function getCancleWorkByMailroom($work_inout_id) 
	{
		$params = array();
		$sql =
		'
			SELECT 
				d.mr_department_name,
				f.name as mr_department_floor,
				i.*,
				m.mr_topic,
				m.mr_work_remark,
				e.mr_emp_code as emp_code,
				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as name_re,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as lastname_re,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as tel_re,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as mobile_re,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'")  as mr_emp_email,

				d.mr_department_id
			FROM mr_work_inout i
			left join mr_work_main m on ( m.mr_work_main_id = i.mr_work_main_id )
			left join mr_emp e on ( e.mr_emp_id = i.mr_emp_id )
			Left join mr_department d on ( d.mr_department_id = e.mr_department_id )
			Left join mr_floor f on ( f.mr_floor_id = i.mr_floor_id )
			LEFT Join mr_zone z on ( z.mr_floor_id = i.mr_floor_id )
			WHERE
				m.mr_work_main_id = ?
		';

		array_push($params, (int)$work_inout_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetch();
	} 
	
	
	// March

	function updateInOutWithMainId($data, $main_id)
	{
		$this->_db->beginTransaction();
		try {
            $id = $this->_db->update($this->_tableName, $data, 'mr_work_main_id = ' . $main_id);
            $this->_db->commit();
            return $id;
        } catch(Exception $e) {
            $this->_db->rollBack();
            return $e->getMessage();
        }
	}
	

	function getReceiveBranchfromMailroom($status, $workType, $hub_id = null, $txt = null)
	{
		$params = array();
		$in_status_params = array();
		$in_workType_params = array();

		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}
		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,

				e.mr_workplace as receiver_workplace,
				e.mr_workarea as receiver_workarea,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as receiver_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as receiver_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as receiver_tel,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as receiver_mobile,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'")  as receiver_email,

				AES_DECRYPT(ep.mr_emp_name, "'.$this->_secret->database->key.'") as replace_name,
				AES_DECRYPT(ep.mr_emp_lastname, "'.$this->_secret->database->key.'") as replace_lastname,
				AES_DECRYPT(ep.mr_emp_tel, "'.$this->_secret->database->key.'") as replace_tel,
				AES_DECRYPT(ep.mr_emp_mobile, "'.$this->_secret->database->key.'")  as replace_mobile,
				AES_DECRYPT(ep.mr_emp_email, "'.$this->_secret->database->key.'")  as replace_email,

				ep.mr_emp_code as replace_code,
				ep.mr_workplace as replace_workplace,
				ep.mr_workarea as replace_workarea,

				io.mr_status_send,
				io.mr_status_receive,
				b.mr_branch_id,
				b.mr_branch_name,
				b.mr_branch_code,
				b.mr_branch_type,
				s.mr_status_id,
				s.mr_status_name,
				s.mr_type_work_id,
				h.mr_hub_id,
				h.mr_hub_name,
				ss.mr_status_action_name as status_send,
				sr.mr_status_action_name as status_receive
			from '.$this->_tableName.' as io
				left join mr_work_main as m on (m.mr_work_main_id = io.mr_work_main_id)
				left join mr_emp as e on (e.mr_emp_id = io.mr_emp_id)
				left join mr_department as d on (d.mr_department_id = e.mr_department_id)
				left join mr_branch as b on (b.mr_branch_id = io.mr_branch_id)
				left join mr_hub as  h on (h.mr_hub_id = b.mr_hub_id)
				left join mr_status as s on ( s.mr_status_id = m.mr_status_id )
				left join mr_floor as f on (f.mr_floor_id = io.mr_floor_id) 

				left join mr_user as up on (up.mr_user_id = io.mr_user_id)
				left join mr_emp as ep on (ep.mr_emp_id = up.mr_emp_id)

				left join mr_status_action as ss on (ss.mr_status_action_id = io.mr_status_send)
				left join mr_status_action as sr on (sr.mr_status_action_id = io.mr_status_receive)
			where io.mr_work_inout_id is not null
		';

		if(!empty($txt)) {
			$sql .= " and ( m.mr_work_barcode like ? or ";
			$sql .= " e.mr_emp_name like ? or ";
			$sql .= " e.mr_emp_lastname like ? or ";
			$sql .= " b.mr_branch_name like ? ) ";

			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
		}

		if(!empty($hub_id)) {
			$sql .= " and b.mr_hub_id = ? and b.mr_hub_id is not null ";
			array_push($params, (int)$hub_id);
		}

		$sql .= " and m.mr_status_id in (".$in_status_condition.") ";

		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		$sql .= "order by m.mr_work_barcode asc";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_status_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}
	
	function getSendBranchfromMailroom($status, $workType, $hub_id = null, $txt = null)
	{
		$params = array();
		$in_status_params = array();
		$in_workType_params = array();

		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}

		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,

				e.mr_emp_code as receiver_code,
				e.mr_workplace as receiver_workplace,
				e.mr_workarea as receiver_workarea,
				ep.mr_emp_code as replace_code,
				ep.mr_workplace as replace_workplace,
				ep.mr_workarea as replace_workarea,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as receiver_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as receiver_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as receiver_tel,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as receiver_mobile,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'")  as receiver_email,

				AES_DECRYPT(ep.mr_emp_name, "'.$this->_secret->database->key.'") as replace_name,
				AES_DECRYPT(ep.mr_emp_lastname, "'.$this->_secret->database->key.'") as replace_lastname,
				AES_DECRYPT(ep.mr_emp_tel, "'.$this->_secret->database->key.'") as replace_tel,
				AES_DECRYPT(ep.mr_emp_mobile, "'.$this->_secret->database->key.'")  as replace_mobile,
				AES_DECRYPT(ep.mr_emp_email, "'.$this->_secret->database->key.'")  as replace_email,

				io.mr_status_send,
				io.mr_status_receive,
				b.mr_branch_id,
				b.mr_branch_name,
				b.mr_branch_code,
				b.mr_branch_type,
				s.mr_status_id,
				s.mr_status_name,
				s.mr_type_work_id,
				h.mr_hub_id,
				h.mr_hub_name,
				ss.mr_status_action_name as status_send,
				sr.mr_status_action_name as status_receive
			from '.$this->_tableName.' as io
				left join mr_work_main as m on (m.mr_work_main_id = io.mr_work_main_id)
				left join mr_emp as e on (e.mr_emp_id = io.mr_emp_id)
				left join mr_department as d on (d.mr_department_id = e.mr_department_id)
				left join mr_branch as b on (b.mr_branch_id = io.mr_branch_id)
				left join mr_hub as  h on (h.mr_hub_id = b.mr_hub_id)
				left join mr_status as s on ( s.mr_status_id = m.mr_status_id )
				left join mr_floor as f on (f.mr_floor_id = io.mr_floor_id) 

				left join mr_user as up on (up.mr_user_id = io.mr_user_id)
				left join mr_emp as ep on (ep.mr_emp_id = up.mr_emp_id)

				left join mr_status_action as ss on (ss.mr_status_action_id = io.mr_status_send)
				left join mr_status_action as sr on (sr.mr_status_action_id = io.mr_status_receive)
			where io.mr_work_inout_id is not null
		';

		if(!empty($txt)) {
			$sql .= " and ( m.mr_work_barcode like ? or ";
			$sql .= " e.mr_emp_name like ? or ";
			$sql .= " e.mr_emp_lastname like ? or ";
			$sql .= " b.mr_branch_name like ? ) ";

			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
			array_push($params, (string)'%'.$txt.'%');
		}

		if(!empty($hub_id)) {
			$sql .= " and b.mr_hub_id = ? and b.mr_hub_id is not null ";
			array_push($params, (int)$hub_id);
		}

		$sql .= " and m.mr_status_id in (".$in_status_condition.") ";

		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		$sql .= "order by io.mr_work_main_id desc";

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_status_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}


	function getBranchHaveOrder($hub_id)
	{
		$params = array();
		$sql =
		'
			select 
				distinct 
					b.mr_branch_id,
					b.mr_branch_code,
					b.mr_branch_name
			from mr_branch b
			where b.mr_hub_id = ?
		';
		array_push($params, (int)$hub_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}

	function getBranchWithUserId($userId)
	{
		$params = array();
		$sql =
		'
			select 
				distinct 
					b.mr_branch_id,
					b.mr_branch_code,
					b.mr_branch_name
			from '.$this->_tableName.' io
				left join mr_work_main as m on (m.mr_work_main_id = io.mr_work_main_id)
				left join mr_branch as b on (b.mr_branch_id = io.mr_branch_id)
				left join mr_hub as h on (h.mr_hub_id = b.mr_hub_id)
			where io.messenger_user_id = ? and m.mr_status_id = 13
		';
		
		array_push($params, (int)$userId);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	function getBranchreseveWithUserId($hub_id)
	{
		$params = array();
		$sql =
		'
			select 
				distinct 
					b.mr_branch_id,
					b.mr_branch_code,
					b.mr_branch_name
			from mr_branch b
			where b.mr_hub_id = ?
			
		';
		
		array_push($params, (int)$hub_id);
		
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
         
        // Execute Query
        $stmt->execute($params);

		return $stmt->fetchAll();
	}
	
	
	
	function getReceiveBranchHub($branch_id, $workType, $status)
	{
		$params = array();
		$in_branch_id_params = array();
		$in_workType_params = array();

		if(!empty($branch_id)) {
			$in_branch_id_condition = array(); // condition: generate ?,?,?
            $in_branch_id_params = explode(',', $branch_id);
            $in_branch_id_condition = str_repeat('?,', count($in_branch_id_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}

		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
				e.mr_emp_code as receiver_code,
				e.mr_workplace as receiver_workplace,
				e.mr_workarea as receiver_workarea,
				ep.mr_emp_code as replace_code,
				ep.mr_workplace as replace_workplace,
				ep.mr_workarea as replace_workarea,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as receiver_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as receiver_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as receiver_tel,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as receiver_mobile,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'")  as receiver_email,

				AES_DECRYPT(ep.mr_emp_name, "'.$this->_secret->database->key.'") as replace_name,
				AES_DECRYPT(ep.mr_emp_lastname, "'.$this->_secret->database->key.'") as replace_lastname,
				AES_DECRYPT(ep.mr_emp_tel, "'.$this->_secret->database->key.'") as replace_tel,
				AES_DECRYPT(ep.mr_emp_mobile, "'.$this->_secret->database->key.'")  as replace_mobile,
				AES_DECRYPT(ep.mr_emp_email, "'.$this->_secret->database->key.'")  as replace_email,


				io.mr_status_send,
				io.mr_status_receive,
				b.mr_branch_id,
				b.mr_branch_name,
				b.mr_branch_code,
				b.mr_branch_type,
				s.mr_status_id,
				s.mr_status_name,
				s.mr_type_work_id,
				h.mr_hub_id,
				h.mr_hub_name,
				ss.mr_status_action_name as status_send,
				sr.mr_status_action_name as status_receive
			from '.$this->_tableName.' as io
				left join mr_work_main as m on (m.mr_work_main_id = io.mr_work_main_id)
				left join mr_emp as e on (e.mr_emp_id = io.mr_emp_id)
				left join mr_department as d on (d.mr_department_id = e.mr_department_id)
				left join mr_branch as b on (b.mr_branch_id = io.mr_branch_id)
				left join mr_hub as  h on (h.mr_hub_id = b.mr_hub_id)
				left join mr_status as s on ( s.mr_status_id = m.mr_status_id )
				left join mr_floor as f on (f.mr_floor_id = io.mr_floor_id) 

				left join mr_user as up on (up.mr_user_id = io.mr_user_id)
				left join mr_emp as ep on (ep.mr_emp_id = up.mr_emp_id)

				left join mr_status_action as ss on (ss.mr_status_action_id = io.mr_status_send)
				left join mr_status_action as sr on (sr.mr_status_action_id = io.mr_status_receive)
			where io.mr_work_inout_id is not null and io.messenger_user_id is null
		';

		if(!empty($status)){
			$sql .= " and m.mr_status_id = ? ";
			array_push($params, (int)$status);
		}

		if(!empty($branch_id)) {
			$sql .= " and io.mr_branch_id in (".$in_branch_id_condition.") ";
		}
		
		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_branch_id_params,$in_workType_params);
       
		$stmt->execute($params);
	
        return $stmt->fetchAll();
	}

	function getSendBranch($branch_id, $workType, $status, $userId)
	{
		$params = array();
		$in_branch_id_params = array();
		$in_workType_params = array();
		$in_status_params = array();

		if(!empty($branch_id)) {
			$in_branch_id_condition = array(); // condition: generate ?,?,?
            $in_branch_id_params = explode(',', $branch_id);
            $in_branch_id_condition = str_repeat('?,', count($in_branch_id_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($workType)) {
			$in_workType_condition = array(); // condition: generate ?,?,?
            $in_workType_params = explode(',', $workType);
            $in_workType_condition = str_repeat('?,', count($in_workType_params) - 1) . '?'; // example: ?,?,?
		}
		if(!empty($status)) {
			$in_status_condition = array(); // condition: generate ?,?,?
            $in_status_params = explode(',', $status);
            $in_status_condition = str_repeat('?,', count($in_status_params) - 1) . '?'; // example: ?,?,?
		}

		$sql =
		'
			select
				m.mr_work_main_id, 
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_topic,
				m.mr_work_remark,
				
				e.mr_emp_code as receiver_code,
				e.mr_workplace as receiver_workplace,
				e.mr_workarea as receiver_workarea,
				ep.mr_emp_code as replace_code,
				ep.mr_workplace as replace_workplace,
				ep.mr_workarea as replace_workarea,

				AES_DECRYPT(e.mr_emp_name, "'.$this->_secret->database->key.'") as receiver_name,
				AES_DECRYPT(e.mr_emp_lastname, "'.$this->_secret->database->key.'") as receiver_lastname,
				AES_DECRYPT(e.mr_emp_tel, "'.$this->_secret->database->key.'") as receiver_tel,
				AES_DECRYPT(e.mr_emp_mobile, "'.$this->_secret->database->key.'")  as receiver_mobile,
				AES_DECRYPT(e.mr_emp_email, "'.$this->_secret->database->key.'")  as receiver_email,

				AES_DECRYPT(ep.mr_emp_name, "'.$this->_secret->database->key.'") as replace_name,
				AES_DECRYPT(ep.mr_emp_lastname, "'.$this->_secret->database->key.'") as replace_lastname,
				AES_DECRYPT(ep.mr_emp_tel, "'.$this->_secret->database->key.'") as replace_tel,
				AES_DECRYPT(ep.mr_emp_mobile, "'.$this->_secret->database->key.'")  as replace_mobile,
				AES_DECRYPT(ep.mr_emp_email, "'.$this->_secret->database->key.'")  as replace_email,

				io.mr_status_send,
				io.mr_status_receive,
				b.mr_branch_id,
				b.mr_branch_name,
				b.mr_branch_code,
				b.mr_branch_type,
				s.mr_status_id,
				s.mr_status_name,
				s.mr_type_work_id,
				h.mr_hub_id,
				h.mr_hub_name,
				ss.mr_status_action_name as status_send,
				sr.mr_status_action_name as status_receive
			from '.$this->_tableName.' as io
				left join mr_work_main as m on (m.mr_work_main_id = io.mr_work_main_id)
				left join mr_emp as e on (e.mr_emp_id = io.mr_emp_id)
				left join mr_department as d on (d.mr_department_id = e.mr_department_id)
				left join mr_branch as b on (b.mr_branch_id = io.mr_branch_id)
				left join mr_hub as  h on (h.mr_hub_id = b.mr_hub_id)
				left join mr_status as s on ( s.mr_status_id = m.mr_status_id )
				left join mr_floor as f on (f.mr_floor_id = io.mr_floor_id) 

				left join mr_user as up on (up.mr_user_id = io.mr_user_id)
				left join mr_emp as ep on (ep.mr_emp_id = up.mr_emp_id)

				left join mr_status_action as ss on (ss.mr_status_action_id = io.mr_status_send)
				left join mr_status_action as sr on (sr.mr_status_action_id = io.mr_status_receive)
			where io.mr_work_inout_id is not null 
		';

		if(!empty($userId)){
			$sql .= " and io.messenger_user_id = ? ";
			array_push($params, (int)$userId);
		}

		if(!empty($branch_id)) {
			$sql .= " and io.mr_branch_id in (".$in_branch_id_condition.") ";
		}
		
		if(!empty($workType)) {
			$sql .= " and m.mr_type_work_id in (".$in_workType_condition.") ";
		}

		if(!empty($status)){
			$sql .= " and m.mr_status_id in (".$in_status_condition.") ";
		}

		// prepare statement SQL
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		// echo $sql;
        $params = array_merge($params,$in_branch_id_params,$in_workType_params,$in_status_params);
       
		$stmt->execute($params);
	
		return $stmt->fetchAll();
		
	}

	function select_work_inout($sql,$params){
		
		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
	
		// Execute Query
		$stmt->execute($params);
		
		return $stmt->fetchAll();
	}
	
	
	function updatework_successWithMainId($sql)
	{
		$this->_db->beginTransaction();
		try {
            //$id = $this->_db->update($this->_tableName, $data, 'mr_work_main_id in(' . $main_id.')');
			$id = $this->_db->query($sql);
            $this->_db->commit();
            return $id;
        } catch(Exception $e) {
            $this->_db->rollBack();
            return $e->getMessage();
        }
	}
}
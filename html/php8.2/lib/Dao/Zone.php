<?php
require_once 'Pivot/Dao.php';
require_once 'Dao/UserRole.php';


class Dao_zone extends Pivot_Dao
{
    function __construct()
    {
        parent::__construct('mr_zone');
    }	
    

	function getZone_UserAll()
	{
		$params = array();

		$sql =
			'
            SELECT 
                z.mr_zone_id,
                z.mr_floor_id,
                z.sys_time,
                f.name,
                GROUP_CONCAT(z.mr_user_id)  as g_mr_user_id,
                GROUP_CONCAT(u.mr_user_username) as g_mr_user_username
            FROM mr_zone z
            left join mr_user u on(u.mr_user_id = z.mr_user_id)
            left join mr_floor f on(f.mr_floor_id = z.mr_floor_id)
            GROUP BY z.mr_zone_id
            order by z.mr_zone_id desc
		';

		//array_push($params, "%".trim($txt)."%");
		//array_push($params, "%".trim($txt)."%");

		// prepare statement SQL
		$stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
		// Execute Query
		$stmt->execute($params);
		return $stmt->fetchAll();
		//echo $sql;
		// return $this->_db->fetchAll($sql);
	}

}
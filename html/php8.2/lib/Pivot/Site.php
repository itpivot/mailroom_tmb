<?php
require_once 'Pivot/Template.php';

class Pivot_Site
{
    public static function toLoginPage() {
        // header('Location: ../user/login.php?returnUrl=' . urlencode($_SERVER['SCRIPT_NAME']));
        header('Location: ../user/logout.php');
        exit();
    }
    
    public static function toDefaultPage() {
        header('Location: ../user/list.php');
        exit();
    }
    
	public static function toAdministratorPages() {
        header('Location: ../admin/news.php');
        exit();
    }
	
	    public static function toEmployeePages() {
        header('Location: ../data/profile.php');
        exit();
    }    
	
	public static function toMessengerPages() {
        header('Location: ../messenger/news.php');
        exit();
    }
	
	public static function toMailroomPages() {
        header('Location: ../mailroom/news.php');
        exit();
    }
	
	public static function toBranchPages() {
        header('Location: ../branch/index.php');
        exit();
    }
	
	public static function toManagerPages() {
        header('Location: ../manager/news.php');
        exit();
    }

    public static function toMessengerBranchPages() {
        header('Location: ../messenger/news.php');
        exit();
    }
	

    public static function toChangePassword($param) {
        header('Location: ../user/change_password.php?usr='.$param);
        exit();
    }
}
?>
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';
require_once 'Dao/Round_resive_work.php';
$auth 						= new Pivot_Auth();
$req 						= new Pivot_Request();
$work_logDao 				= new Dao_Work_log();
$work_mainDao 				= new Dao_Work_main();
$work_inoutDao 				= new Dao_Work_inout();
$send_workDao 				= new Dao_Send_work();
$round_resive_workDao 		= new Dao_Round_resive_work();
$user_id					= $auth->getUser();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$page 				= $req->get('page');
$barcode 			= $req->get('barcose_scan');
$scan_round_id  	= $req->get('scan_round_id');
$id  				= $req->get('id');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $scan_round_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}



if($page == "remove"){
	$re = $round_resive_workDao->remove($id);
	echo json_encode(array(
		'status' => 200,
		'data' => '',
		'msg' => 'ลบข้อมูลเรียบร้อย',
	));
	exit;

}elseif($page == "save"){

	if($barcode=="")	{
		echo json_encode(array(
			'status' => 500,
			'data' => '',
			'msg' => 'กรุณากรอกเลข barcode',
		));
		exit;
	}elseif($scan_round_id=="")	{
		echo json_encode(array(
			'status' => 500,
			'data' => '',
			'msg' => 'กรุณาเลือกรอบ',
		));
		exit;
	}else{
		$re = $round_resive_workDao->check_barcode($barcode);
		if(!empty($re))//mr_work_main_id
		{
			$save_round_resive = array();
			$save_round_resive['mr_work_main_id'] 	=  $re['mr_work_main_id'];
			$save_round_resive['mr_user_id'] 		=  $user_id;
			$save_round_resive['mr_round_id'] 		=  $scan_round_id;

			$main_update['mr_status_id']=10;
			
			$save_log['mr_user_id'] 									= $auth->getUser();
			$save_log['mr_status_id'] 									= 10;
			$save_log['mr_work_main_id'] 								= $re['mr_work_main_id'];
			$save_log['mr_round_id'] 									= $scan_round_id;
			$save_log['remark'] 										= "ห้อง Mailroom รับเอกสาร และเตรียมพิมพ์ใบนำส่ง ฺ(BOC)";

			if(!empty($re['mr_work_main_id'])){
				$id = $round_resive_workDao->save($save_round_resive);
				if($scan_round_id == 53 or $scan_round_id == 54){
					$ss = $work_mainDao->save($main_update,$re['mr_work_main_id']);
					$work_logDao->save($save_log);
				}

			}


			//echo json_encode($save_round_resive);
			echo json_encode(array(
				'status' => 200,
				'data' => $save_round_resive,
				'msg' => 'บันทึกสำเร็จ',
			));
			exit;
		}else{
			echo json_encode(array(
				'status' => 500,
				'data' => '',
				'msg' => 'ไม่พบ barcode ที่ท่านเลือก',
			));
			exit;
		}
	
	}

}else{

	$params2 = array();
	$sql_select2 = "
					SELECT 
						rw.mr_round_resive_work_id,
						m.mr_work_main_id,
						m.sys_timestamp,
						m.mr_work_barcode,
						tnt.mr_round as send_round,
						e.mr_emp_name as send_name,
						e.mr_emp_lastname  as send_lname,
						bre.mr_branch_code as re_branch_code,
						bre.mr_branch_name as re_branch_name,
						e2.mr_emp_name as re_name,
						e2.mr_emp_lastname  as re_lname,
						tnt.tnt_tracking_no,
						tw.mr_type_work_name
					FROM mr_round_resive_work rw
					
					
					LEFT JOIN mr_work_main m  ON ( m.mr_work_main_id = rw.mr_work_main_id )
					LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
					LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
					LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = e2.mr_branch_id )
					Left join mr_branch bre on ( bre.mr_branch_id = wb.mr_branch_id )
					Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
					Left join mr_round_send_work tnt on ( tnt.mr_work_main_id = m.mr_work_main_id )
					where m.mr_work_main_id is not null
					";
					
					if($scan_round_id !=''){
						$sql_select2 .= " and rw.mr_round_id = ? ";
						array_push($params2, (int)$scan_round_id);
					}
					$sql_select2 .= " and rw.sysdate like'".date('Y-m-d')."%'";
					$sql_select2 .= "  group by rw.mr_round_resive_work_id ";
					$sql_select2 .= "  order by rw.mr_round_resive_work_id desc ";

	$data = $send_workDao->select_send_work($sql_select2,$params2);




	if(!empty($data)){
		foreach($data as $i => $val_ ){
			$data[$i]['no1'] = $sql_select2;
			$data[$i]['no'] = $i+1;
			$data[$i]['remove'] 		= '<button onclick="remove_mr_round_resive_work('.$val_['mr_round_resive_work_id'].')" type="button" class="btn btn-secondary btn-sm">ลบ</button>';
			$data[$i]['barcode_tnt'] 		= $val_['barcode_tnt'];
			$data[$i]['name_send'] 			= $val_['send_name'].'  '.$val_['send_lname'];
			$data[$i]['name_receive'] 		= $val_['re_name'].'  '.$val_['re_lname'];
			$data[$i]['branch'] 			= $val_['re_branch_code'].' : '.$val_['re_branch_name'];
			$data[$i]['mr_status_name']='	
												<label class="custom-control custom-checkbox"  >
													<input value="'.$val_['mr_work_main_id'].'" id="select-all" name="select_data" id="ch_'.$val_['mr_work_main_id'].'" type="checkbox" class="custom-control-input">
													<span class="custom-control-indicator"></span>
													<span class="custom-control-description">เลือก</span>
												</label>
											';

		}

		echo json_encode(array(
			'status' => 200,
			'data' => $data
		));
	}else{
		echo json_encode(array('status' => 500, 'message' => 'ไม่มีข้อมูล'));
	}
}
?>

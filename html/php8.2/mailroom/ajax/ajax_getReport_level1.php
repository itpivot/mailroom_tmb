<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Work_inout.php';
require_once 'check_sla.php';
set_time_limit(-1);
$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$userDao 			= new Dao_User();   
$work_inoutDao 		= new Dao_Work_inout();
$class_check_sla 	= new Class_check_sla();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

//if($i == 5 or $i == 6 or $i == 12 or $i == 15)
$array_success		= array('5','6','12','15');

$sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round WHERE active = 1 ORDER BY mr_delivery_round_id ASC";
$max_round_sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round 
WHERE mr_delivery_round_id in(
	SELECT max(mr_delivery_round_id)-1 
	FROM mr_delivery_round 
	group by mr_branch_type_send, mr_branch_type_resive)
and active = 1
";
$min_round_sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round 
WHERE mr_delivery_round_id in(
	SELECT min(mr_delivery_round_id)
	FROM mr_delivery_round 
	group by mr_branch_type_send, mr_branch_type_resive)
and active = 1
";
$y = date('Y');
$sql_holiday = "SELECT * FROM mr_holiday WHERE year LIKE '%".$y."%' AND active = 1";

$delivery_round_all = $userDao->select($sql);
$max_round_all 		= $userDao->select($max_round_sql);
$min_round_all 		= $userDao->select($min_round_sql);
$holiday_all 		= $userDao->select($sql_holiday);


$delivery_round_data = array();
foreach($delivery_round_all as $v){
	$delivery_round_data[$v['mr_branch_type_send']][$v['mr_branch_type_resive']][]= $v;
}

$max_round_data = array();
foreach($max_round_all as $v2){
	$max_round_data[$v2['mr_branch_type_send']][$v2['mr_branch_type_resive']][]= $v2;
}
$min_round_data = array();
foreach($min_round_all as $v3){
	$min_round_data[$v3['mr_branch_type_send']][$v3['mr_branch_type_resive']][]= $v3;
}

$holiday_data = array();
foreach($holiday_all as $v4){
	$holiday_data[$v4['year']][$v4['month']][]= $v4['day'];
}

// echo json_encode($a);
// exit;

$branch_type 	= $req->get('branch_type');
$date_1 		= $req->get('date_1');
$date_2 		= $req->get('date_2');
$type 			= $req->get('type');

if(preg_match('/<\/?[^>]+(>|$)/', $branch_type)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $date_1)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $date_2)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $type)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

function TimeDiff($strTime1,$strTime2){
	return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}
function DateTimeDiff($strDateTime1,$strDateTime2){
return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}


function check_date_start($sla_date_end){
	$day = date ("D", strtotime($sla_date_end)); 
	if($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($sla_date_end))); 
	}elseif($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($sla_date_end))); 
	}else{
		$date_end = $sla_date_end;
	}
	return $date_end;
}

function wav_sla($sla_date_end){
	$day = date ("D", strtotime($sla_date_end)); 
	if($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($sla_date_end))); 
	}elseif($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($sla_date_end))); 
	}else{
		$date_end = $sla_date_end;
	}
	return $date_end;
}

// $today = '2020-01-24 00:00:01';
// $day = date ("D", strtotime($today)); 
// echo json_encode($day);
// exit;

if( $date_1 != ''){
	$date_1 	= date ("Y-m-d 17:00:01", strtotime("-1 day", strtotime($date_1)));
	$date_2 	= date ("Y-m-d 17:00:00", strtotime($date_2));
}


$resout = array();
$resout['date_1'] = $date_1;
$resout['date_2'] = $date_2;


$data_search['start_date']		= $date_1;
$data_search['end_date']		= $date_2; 
$data_search['type']			= $type; 
if($type == 'ho'){
	$type = 2; 
}else{
	$type = 3;
}
$data  =   $work_inoutDao->mailroom_getreport_level1($data_search);

$work_type5 = array();
$work_type2 = array();
$tt = array();
foreach($data as $key => $val){
	$sla_num 				= 0;
	
	$sys_time 				= $val['main_sys_time'];
	$time_mail_re 				= $val['time_mail_re'];
	$mr_topic 				= $val['mr_topic'];
	$time_send     			= date("H:i:s",strtotime($sys_time));
	$mr_branch_type_id 		= $val['mr_branch_type_id'];
	$mr_branch_type_id_re 	= $val['mr_branch_type_id_re'];
	$sys_time 				= check_date_start($sys_time);

//Sat
//Sun

	if($type == 3){
			//echo $val['mr_branch_type_id'].'<br>';
			if($val['mr_branch_type_id_re'] == 2 or $val['mr_branch_type_id_re'] == 3){
				//3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
				$type_re = $val['mr_branch_type_id_re'];
			}else{
				$type_re = 1;
			}

			if($val['mr_branch_type_id'] == 3){ // 3 สาขาต่างจังหวัดสังงาน ถ้าไม่เป็นปริมนฑน
				$type_send = $val['mr_branch_type_id'];
				
			}else{
				$type_send = 2;
			}

			$mr_status_id 	= $val['mr_status_id'];
			if (!in_array($mr_status_id,$array_success)) {
			//if($mr_status_id != 5 and $mr_status_id != 12){	
				
					
				if($mr_topic=='marketing' || $mr_topic=='Marketing') {
					if($time_mail_re != ''){
						$a = $class_check_sla->check_sla($type_send,$type_re,$time_mail_re,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);
						$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$a);
						if($dif_date<0 ){
							$val['mr_status_id'] = 20;
							//echo json_encode($tt);
							//exit;
						}
				}
	
				}else{
					$a = $class_check_sla->check_sla($type_send,$type_re,$sys_time,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);
					$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$a);

					if($dif_date<0){
						$val['mr_status_id'] = 20;
						//echo json_encode($tt);
						//exit;
					}
				}
	
			}







		
				if(isset($work_type5[$type_send][$type_re][$val['mr_status_id']] )){
					$work_type5[$type_send][$type_re][$val['mr_status_id']] += 1;
				}else{
					$work_type5[$type_send][$type_re][$val['mr_status_id']] = 1;
				}
				
			

			//$a = array(2, 4, 6, 8);




	}else{
		
		


		if($val['mr_branch_type_id_re'] == 2 or $val['mr_branch_type_id_re'] == 3){ //3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
			$type_re = $val['mr_branch_type_id_re'];
		}else{
			$type_re = 1;
		}
		$type_send 		= 1;
		$mr_status_id 	= $val['mr_status_id'];

		//if($mr_status_id != 5 and $mr_status_id != 12){	
			if (!in_array($mr_status_id,$array_success)) {
				
				if($mr_topic=='marketing' || $mr_topic=='Marketing') {
					if($time_mail_re != ''){
						$a = $class_check_sla->check_sla($type_send,$type_re,$time_mail_re,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);
						$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$a);
						if($dif_date<0 ){
							$val['mr_status_id'] = 20;
							//echo json_encode($tt);
							//exit;
						}
					}
	
				}else{
					$a = $class_check_sla->check_sla($type_send,$type_re,$sys_time,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);
					$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$a);

					if($dif_date<0){
						$val['mr_status_id'] = 20;
						//echo json_encode($tt);
						//exit;
					}
				}

			
		}
		
			
		
	
		if(isset($work_type2[$type_send][$type_re][$val['mr_status_id']] )){
			$work_type2[$type_send][$type_re][$val['mr_status_id']] += 1;
		}else{
			$work_type2[$type_send][$type_re][$val['mr_status_id']] = 1;
		}

	}
}
// echo json_encode($tt);
// exit;

//สาขาสั่งงาน
//สาขาสั่งงาน
//สาขาสั่งงาน
//สาขาสั่งงาน
//สาขาสั่งงาน

if($type == 3){
	
// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
 $b_ho 	= (isset($work_type5[3][1]))?array_sum($work_type5[3][1]):'0';
 $b_bh 	= (isset($work_type5[3][2]))?array_sum($work_type5[3][2]):'0';
 $b_b 	= (isset($work_type5[3][3]))?array_sum($work_type5[3][3]):'0';

 $bh_ho 	= (isset($work_type5[2][1]))?array_sum($work_type5[2][1]):'0';
 $bh_bh 	= (isset($work_type5[2][2]))?array_sum($work_type5[2][2]):'0';
 $bh_b 		= (isset($work_type5[2][3]))?array_sum($work_type5[2][3]):'0';

 $send_index = 3;
 for($i=1;$i<=15;$i++){
	if($i == 1){
		$suc_b_ho 	= 0;
		$suc_b_bh 	= 0;
		$suc_b_b 	= 0;
		$pen_b_ho 	= 0;
		$pen_b_bh 	= 0;
		$pen_b_b 	= 0;
	 }

	// if($i == 5 or $i == 6 or $i == 12 or $i == 15){
	if (in_array($i,$array_success)) {
		$suc_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		$suc_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		$suc_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
	 }else{
		$pen_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		$pen_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		$pen_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
	 }
 }

 $overSLA_b_ho 	= (isset($work_type5[$send_index][1][20]))?$work_type5[$send_index][1][20]:'0';
 $overSLA_b_bh 	= (isset($work_type5[$send_index][2][20]))?$work_type5[$send_index][2][20]:'0';
 $overSLA_b_b 	= (isset($work_type5[$send_index][3][20]))?$work_type5[$send_index][3][20]:'0';


// echo json_encode($work_type5);
//  exit;

$str_html = '<table id="tb_branch_1" ';

if($branch_type == 2){
	$str_html .= 'style="display: none;"';
}

$str_html .= 'class="table table-bordered">
<thead>
  <tr>
	<th scope="col">#</th>
	<th scope="col">สถานะงาน</th>
	<th scope="col">ส่งที่สำนักงานใหญ่</th>
	<th scope="col">ส่งที่สาขากรุงเทพมและปริมณฑล</th>
	<th scope="col">ส่งที่สาขาต่างจังหวัด</th>
  </tr>
</thead>
<tbody>
	  <tr>
		<th scope="row">1</th>
		<td>งานทั้งหมด</td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,1);">'.$b_ho.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,2);">'.$b_bh.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,3);">'.$b_b.' </button></td>
	  </tr>
	  <tr>
		<th scope="row">2</th>
		<td>งานค้างในระบบ(ตาม SLA)</td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,1);">'.$pen_b_ho.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,2);">'.$pen_b_bh.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,3);">'.$pen_b_b. '</button></td>
	  </tr>
	  <tr>
		<th scope="row">3</th>
		<td>งานค้างในระบบ(ไม่ตาม SLA)</td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,1);">'.$overSLA_b_ho.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,2);">'.$overSLA_b_bh.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,3);">'.$overSLA_b_b.' </button></td>
</tr>
	  <tr>
	  <th scope="row">4</th>
	  <td>งานสำเร็จ</td>
	  	<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,1);">'.$suc_b_ho.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,2);">'.$suc_b_bh.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,3);">'.$suc_b_b.' </button></td>
	</tr>
	</tbody>
 </table>';




//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน

$send_index = 2;
 for($i=1;$i<=15;$i++){
	 if($i == 1){
		$suc_b_ho 	= 0;
		$suc_b_bh 	= 0;
		$suc_b_b 	= 0;
		$pen_b_ho 	= 0;
		$pen_b_bh 	= 0;
		$pen_b_b 	= 0;



	 }
	 
	// if($i == 5 or $i == 6 or $i == 12 or $i == 15){
	if (in_array($i,$array_success)) {
		$suc_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		$suc_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		$suc_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
	 }else{
		$pen_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		$pen_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		$pen_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
	 }
 }
 $overSLA_b_ho 	= (isset($work_type5[$send_index][1][20]))?$work_type5[$send_index][1][20]:'0';
 $overSLA_b_bh 	= (isset($work_type5[$send_index][2][20]))?$work_type5[$send_index][2][20]:'0';
 $overSLA_b_b 	= (isset($work_type5[$send_index][3][20]))?$work_type5[$send_index][3][20]:'0';



 $str_html .= '<table id="tb_branch_2" ';

 if($branch_type == 3){
	 $str_html .= 'style="display: none;"';
 }
 
 $str_html .= 'class="table table-bordered">
<thead>
  <tr>
	<th scope="col">#</th>
	<th scope="col">สถานะงาน</th>
	<th scope="col">ส่งที่สำนักงานใหญ่</th>
	<th scope="col">ส่งที่สาขากรุงเทพมและปริมณฑล</th>
	<th scope="col">ส่งที่สาขาต่างจังหวัด</th>
  </tr>
</thead>
<tbody>
	  <tr>
		<th scope="row">1</th>
		<td>งานทั้งหมด</td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,1);">'.$bh_ho.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,2);">'.$bh_bh.'</button></td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,3);">'.$bh_b.' </button></td>
	  </tr>
	  <tr>
	  <th scope="row">2</th>
	  <td>งานค้างในระบบ(ตาม SLA)</td>
	  <td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,1);">'.$pen_b_ho.'</button></td>
	  <td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,2);">'.$pen_b_bh.'</button></td>
	  <td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,3);">'.$pen_b_b.' </button></td>
	</tr>
	<tr>
	  <th scope="row">3</th>
	  <td>งานค้างในระบบ(ไม่ตาม SLA)</td>
	  <td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,1);">'.$overSLA_b_ho.'</button></td>
	  <td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,2);">'.$overSLA_b_bh.'</button></td>
	  <td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,3);">'.$overSLA_b_b.' </button></td>
</tr>
	<tr>
	<th scope="row">4</th>
	<td>งานสำเร็จ</td>
		<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,1);">'.$suc_b_ho.'</button></td>
	  	<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,2);">'.$suc_b_bh.'</button></td>
	  	<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,3);">'.$suc_b_b.' </button></td>
  </tr>
	</tbody>
 </table>';




 //echo $str_html;

 $resout['html'] = $str_html;
 echo json_encode($resout);






 //สำนักงานใหญ่่สั่งงาน
 //สำนักงานใหญ่่สั่งงาน
 //สำนักงานใหญ่่สั่งงาน
 //สำนักงานใหญ่่สั่งงาน
}else{

	
	$ho_ho 	= (isset($work_type2[1][1]))?array_sum($work_type2[1][1]):'0';
	$ho_bh 	= (isset($work_type2[1][2]))?array_sum($work_type2[1][2]):'0';
	$ho_b 	= (isset($work_type2[1][3]))?array_sum($work_type2[1][3]):'0';

	
	$send_index = 1;
	for($i=1;$i<=15;$i++){
	   if($i == 1){
		   $suc_b_ho 	= 0;
		   $suc_b_bh 	= 0;
		   $suc_b_b 	= 0;
		   $pen_b_ho 	= 0;
		   $pen_b_bh 	= 0;
		   $pen_b_b 	= 0;
		}
   
		//if($i == 5 or $i == 6 or $i == 12 or $i == 15){
		if (in_array($i,$array_success)) {
			$suc_b_ho 	+= (isset($work_type2[$send_index][1][$i]))?$work_type2[$send_index][1][$i]:'0';
		   $suc_b_bh 	+= (isset($work_type2[$send_index][2][$i]))?$work_type2[$send_index][2][$i]:'0';
		   $suc_b_b 	+= (isset($work_type2[$send_index][3][$i]))?$work_type2[$send_index][3][$i]:'0';
		}else{
		   $pen_b_ho 	+= (isset($work_type2[$send_index][1][$i]))?$work_type2[$send_index][1][$i]:'0';
		   $pen_b_bh 	+= (isset($work_type2[$send_index][2][$i]))?$work_type2[$send_index][2][$i]:'0';
		   $pen_b_b 	+= (isset($work_type2[$send_index][3][$i]))?$work_type2[$send_index][3][$i]:'0';
		}
	}
	$overSLA_b_ho 	= (isset($work_type2[$send_index][1][20]))?$work_type2[$send_index][1][20]:'0';
	$overSLA_b_bh 	= (isset($work_type2[$send_index][2][20]))?$work_type2[$send_index][2][20]:'0';
	$overSLA_b_b 	= (isset($work_type2[$send_index][3][20]))?$work_type2[$send_index][3][20]:'0';
	// echo json_encode($work_type2);
	// exit;

$str_html = '<table id="tb_branch_3" class="table table-bordered">
				<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">สถานะงาน</th>
					<th scope="col">ส่งที่สำนักงานใหญ่</th>
					<th scope="col">ส่งที่สาขากรุงเทพมและปริมณฑล</th>
					<th scope="col">ส่งที่สาขาต่างจังหวัด</th>
				</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>งานทั้งหมด</td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,1);">'.$ho_ho.'	</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,2);">'.$ho_bh.'	</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(1,3);">'.$ho_b.' 	</button></td>
					</tr>
					<tr>
						<th scope="row">2</th>
						<td>งานค้างในระบบ(ตาม SLA)</td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,1);">'.$pen_b_ho.'	</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,2);">'.$pen_b_bh.'	</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(6,3);">'.$pen_b_b.'	</button></td>
					</tr>
					<tr>
						<th scope="row">3</th>
						<td>งานค้างในระบบ(ไม่ตาม SLA)</td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,1);">'.$overSLA_b_ho.'</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,2);">'.$overSLA_b_bh.'</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(20,3);">'.$overSLA_b_b.' </button></td>
					</tr>
					<tr>
					<th scope="row">4</th>
					<td>งานสำเร็จ</td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,1);">'.$suc_b_ho.'	</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,2);">'.$suc_b_bh.'	</button></td>
						<td><button type="button" class="btn btn-link" onclick="getDetail_level3(5,3);">'.$suc_b_b.'	</button></td>
					</tr>
					</tbody>
				</table>';
				$resout['html'] = $str_html;
				echo json_encode($resout);
}
//echo print_r($work_type2,true);
//echo 'end <br>';
?>
      
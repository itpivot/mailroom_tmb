<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Employee.php';

$auth 			    = new Pivot_Auth();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}
$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
$userDao 			= new Dao_User();                                 
$empDao 			= new Dao_Employee();                                 

$report = $userDao->getEmpDataForEdit();

foreach( $report as $num => $v ){
	if($v['mr_emp_code']!='Resign'){
		$report[$num]['re_password'] = '<button type="button" onclick="re_password(\''.$v['mr_user_id'].'\');" class="btn btn-outline-warning"> Reset </button>';
	}else{
		$report[$num]['re_password'] = '';
	}

	$report[$num]['no'] = $num +1;
	$txt = "";
	$txt = $v['mr_emp_code'].'  '.$v['mr_emp_name'].'  '.$v['mr_emp_lastname'].' E-mail : '.$v['mr_emp_email'];
	//$report[$num]['re_password'] = '<button class="btn btn-outline-warning" onclick="re_password('.$v['mr_user_id'].',\''.$txt.'\');">Reset Password </button>';
	//echo "<pre>".print_r($v['isFailed'],true)."</pre>"; 
	//exit();
	if ( $v['isFailed'] >= 1 ){
		$txt =  "";
		$txt .=  '<div class="btn-group" id="status" data-toggle="buttons">';
        $txt .=  '<label class="btn btn-default btn-on btn-sm" onclick="isFailed('.$v['mr_user_id'].',0);" >';
        $txt .=  '<input type="radio" value="0" name="multifeatured_module[module_id][status]">Unlock</label>';
        $txt .=  '<label class="btn btn-default btn-off btn-sm active" onclick="isFailed('.$v['mr_user_id'].',1);">';
        $txt .=  '<input type="radio" value="1" name="multifeatured_module[module_id][status]"  checked="checked" >Lock</label>';
        $txt .=  '</div>';
		
		
		$report[$num]['isFailed'] = $txt;
		
	}else if  ( $v['isFailed'] == 0 ){
		$txt =  "";
		$txt .=  '<div class="btn-group" id="status" data-toggle="buttons">';
        $txt .=  '<label class="btn btn-default btn-on btn-sm active" onclick="isFailed('.$v['mr_user_id'].',0);">';
        $txt .=  '<input type="radio" value="0" name="multifeatured_module[module_id][status]" checked="checked">Unlock</label>';
        $txt .=  '<label class="btn btn-default btn-off btn-sm " onclick="isFailed('.$v['mr_user_id'].',1);">';
        $txt .=  '<input type="radio" value="1" name="multifeatured_module[module_id][status]">Lock</label>';
        $txt .=  '</div>';
		
		
		$report[$num]['isFailed'] = $txt;
		
	}
	
	
	if ( $v['isLogin'] == 1 ){
		$txt =  "";
		$txt .=  '<div class="btn-group" id="status" data-toggle="buttons">';
        $txt .=  '<label class="btn btn-default btn-on btn-sm" onclick="isLogin('.$v['mr_user_id'].',0);" >';
        $txt .=  '<input type="radio" value="0" name="multifeatured_module[module_id][status]">Unlock</label>';
        $txt .=  '<label class="btn btn-default btn-off btn-sm active" onclick="isLogin('.$v['mr_user_id'].',1);">';
        $txt .=  '<input type="radio" value="1" name="multifeatured_module[module_id][status]"  checked="checked" >Lock</label>';
        $txt .=  '</div>';
		
		
		$report[$num]['isLogin'] = $txt;
		
	}else if  ( $v['isLogin'] == 0 ){
		$txt =  "";
		$txt .=  '<div class="btn-group" id="status" data-toggle="buttons">';
        $txt .=  '<label class="btn btn-default btn-on btn-sm active" onclick="isLogin('.$v['mr_user_id'].',0);">';
        $txt .=  '<input type="radio" value="0" name="multifeatured_module[module_id][status]" checked="checked">Unlock</label>';
        $txt .=  '<label class="btn btn-default btn-off btn-sm " onclick="isLogin('.$v['mr_user_id'].',1);">';
        $txt .=  '<input type="radio" value="1" name="multifeatured_module[module_id][status]">Lock</label>';
        $txt .=  '</div>';
		
		
		$report[$num]['isLogin'] = $txt;
		
	}
	
	
	
	//$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
	//$report[$num]['name_send'] = $value['mr_emp_name']." ".$value['mr_emp_lastname'];
	
}


$arr['data'] = $report;

echo json_encode($arr);
 ?>

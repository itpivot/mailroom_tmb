<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';
require_once 'Dao/Branch.php';
require_once 'Dao/Department.php';
require_once 'Dao/Floor.php';
require_once 'Dao/Position.php';

$auth 			= new Pivot_Auth();
$req 			= new Pivot_Request();
$branchDao		= new Dao_Branch();
$employeeDao 	= new Dao_Employee();
$departmentDao 	= new Dao_Department();
$floorDao 		= new Dao_Floor();
$positionDao 	= new Dao_Position();

if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$txt 			= $_GET['search'];
$type 			= $req->get('page'); 

if(preg_match('/<\/?[^>]+(>|$)/', $txt)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $type)) {
	echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
	exit();
}

if($type =='department'){
	$sql_department =
			'
			select * 
			from mr_department 
			where
				( mr_department_code LIKE ? or mr_department_name LIKE ? )
				ORDER BY mr_department_code ASC
				limit 10 
			
	';
	$params_department = array();
	array_push($params_department, (string)'%'.$txt.'%');
	array_push($params_department, (string)'%'.$txt.'%');

	$data = $departmentDao->select_department($sql_department,$params_department);
	foreach ($data as $key => $value) {
		$fullname = $value['mr_department_code']." - " .$value['mr_department_name'];
		//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
		$json[] = array(
			"id" => $value['mr_department_id'],
			"text" => $fullname
		);
	}
	echo json_encode($json);

}else if($type =='floor'){
	$sql_floor =
			'
			select * 
			from mr_floor 
			where
				(  name LIKE ? )
				ORDER BY floor_level ASC
				limit 10 
	';
	$params_floor = array();
	array_push($params_floor, (string)'%'.$txt.'%');
	$data = $floorDao->select_floor($sql_floor,$params_floor);

	foreach ($data as $key => $value) {
		$fullname = $value['name'];
		//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
		$json[] = array(
			"id" => $value['mr_floor_id'],
			"text" => $fullname
		);
	}
	echo json_encode($json);

	
}else if($type =='position'){
	$sql_position =
			'
			select * 
			from mr_position 
			where
				( mr_position_code LIKE ? or mr_position_name LIKE ? )
				ORDER BY mr_position_code ASC
				limit 10 
			
	';

	$params_position = array();
	array_push($params_position, (string)'%'.$txt.'%');
	array_push($params_position, (string)'%'.$txt.'%');
	$data = $positionDao->select_position($sql_position,$params_position);

	foreach ($data as $key => $value) {
		$fullname = $value['mr_position_code']." - " .$value['mr_position_name'];
		//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
		$json[] = array(
			"id" => $value['mr_position_id'],
			"text" => $fullname
		);
	}
	echo json_encode($json);

}else if($type =='hub'){
	$sql_floor =
			'
			SELECT * FROM mr_hub
			where
				(  mr_hub_name LIKE ? )
				ORDER BY mr_hub_id ASC
				limit 10 
	';
	$params_floor = array();
	array_push($params_floor, (string)'%'.$txt.'%');
	$data = $floorDao->select_floor($sql_floor,$params_floor);

	foreach ($data as $key => $value) {
		$fullname = $value['mr_hub_name'];
		//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
		$json[] = array(
			"id" => $value['mr_hub_id'],
			"text" => $fullname
		);
	}
	echo json_encode($json);

	
}else{
	$branch_data = $branchDao->getBranchTXT($txt);
	$json = array();
	foreach ($branch_data as $key => $value) {
			$fullname = $value['mr_branch_code']." - " .$value['mr_branch_name'];
			//$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
			$json[] = array(
				"id" => $value['mr_branch_id'],
				"text" => $fullname
			);
	}
	
	
	echo json_encode($json);
}

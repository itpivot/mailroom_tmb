
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Sub_district.php';
require_once 'Dao/Work_byhand.php';
require_once 'nocsrf.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
} else {

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$work_byhandDao 	= new Dao_Work_byhand();
$userDao 			= new Dao_User();
$sub_districtDao 	= new Dao_Sub_district();

$user_id			= $auth->getUser();


$page = $req->get('page');
$id = $req->get('id');
if( $page == 'cancle'){
	$data = array();
	$mainupdat['mr_status_id'] = 6;
	if($id!=''){
		$work_mainDao->save($mainupdat,$id);
		$save_log['mr_user_id'] 									= $user_id;
			$save_log['mr_status_id'] 									= 6;
			$save_log['mr_work_main_id'] 								= $id;
			$save_log['remark'] 										= "ยกเลิกรายการ";
			$work_logDao->save($save_log);
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);
		exit;
}else{

	$data	= $work_byhandDao->getdatatoday();
	foreach($data as $key=>$val){
		$data[$key]['num'] = $key+1;
		$data[$key]['action'] = '<button type="button" onclick="cancle_work('.$val['mr_work_main_id'].')" class="btn btn-danger btn-sm">ลบ</button>';
	}
	echo json_encode(
		array(
			'status' => 200, 
			'data' => $data, 
			'message' => 'สำเร็จ' 
			)
		);

			exit;

	}

}

 ?>
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Send_work.php';

$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();
$send_workDao 		= new Dao_Send_work();
if (!$auth->isAuth()) {
    echo json_encode(array('status' => 401, 'message' => 'Access denied.'));
   	exit();
}

$barcode 			= $req ->get('barcode');
$hub_id  			= $req->get('hub_id');
$mr_branch_id  		= $req->get('mr_branch_id');
$data_all  			= $req->get('data_all');

if(preg_match('/<\/?[^>]+(>|$)/', $barcode)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $hub_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $mr_branch_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $data_all)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

$mr_branch_id 		= '';

$sql_select = " SELECT 
					im.mr_tnt_barcode_import_id, 
					im.sys_date, 
					im.date_send, 
					im.mr_branch_id, 
					im.barcode_tnt, 
					im.active,
					b.mr_branch_name,
					b.mr_branch_code
				FROM mr_tnt_barcode_import im
					left join mr_branch b on(b.mr_branch_id = im.mr_branch_id) 
				WHERE im.active = 0
				";
$data  = array();
$data = $send_workDao->select($sql_select);

if(!empty($data)){

	foreach($data as $i => $val_ ){
		$data[$i]['no'] = $i+1;
		$data[$i]['sys_date'] 			= $val_['sys_date'];
		$data[$i]['date_send'] 			= $val_['date_send'];
		$data[$i]['barcode_tnt'] 		= $val_['barcode_tnt'];
		$data[$i]['mr_branch_code'] 	= $val_['mr_branch_code']." : ".$val_['mr_branch_name'];
		$data[$i]['status']='
											
											<label class="custom-control custom-checkbox"  >
												<input value="'.$val_['mr_tnt_barcode_import_id'].'" id="select-all" name="select_data" id="ch_'.$val_['mr_tnt_barcode_import_id'].'" type="checkbox" class="custom-control-input">
												<span class="custom-control-indicator"></span>
												<span class="custom-control-description">เลือก</span>
											</label>
											
							';
	}

	echo json_encode(array(
		'status' => 200,
		'data' => $data
	));

}else{

	echo json_encode(array(
		'status' => 501,
		'data' => $data,
		'message' => 'ไม่มีข้อมูล'
	));
}

 ?>

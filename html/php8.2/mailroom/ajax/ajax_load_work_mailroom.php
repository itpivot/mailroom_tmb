<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

ini_set('max_execution_time', '0');
ini_set('memory_limit', '-1');


$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$work_inoutDao 		= new Dao_Work_inout();

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$report = $work_inoutDao->getRecievMailroom();

foreach( $report as $num => $value ){

	$report[$num]['no'] = $num +1;
	$report[$num]['name_receive'] = $value['name_re']." ".$value['lastname_re'];
	$report[$num]['name_send'] = $value['mr_emp_name']." ".$value['mr_emp_lastname'];

}
//echo "<pre>".print_r($report[0]['mr_status_receive'],true)."</pre>"; 
//exit();

$arr['data'] = $report;
	  
echo json_encode($arr);
 ?>

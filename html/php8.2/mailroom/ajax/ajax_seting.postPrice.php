
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'nocsrf.php';
require_once 'Dao/Post_price.php';
require_once 'Dao/Floor.php';


$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
	echo json_encode(array('status' => 401, 'message' => 'กรุณา login' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	exit();
}

	



$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$userDao 			= new Dao_User();
$post_priceDao 			= new Dao_Post_price();

$user_id			= $auth->getUser();





$page  			=  $req ->get('page');//save

$re 			= array();
if($page == "addPrice"){
	try
	{
		// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
		NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
		// form parsing, DB inserts, etc.
		// ...
		$result = 'CSRF check passed. Form parsed.';
	}
	catch ( Exception $e )
	{
		// CSRF attack detected
	$result = $e->getMessage() . ' Form ignored.';
	echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	
	exit();
	}
	try
	{
		$add_type_post_id  	=  $req ->get('add_type_post_id');
		$min_weight  		=  $req ->get('min_weight');
		$max_weight  		=  $req ->get('max_weight');
		$description  		=  $req ->get('description_weight');
		$post_price  		=  $req ->get('post_price');
		$addType  			=  $req ->get('addType');
		$mr_post_price_id  		=  $req ->get('mr_post_price_id');


		$savPrice['mr_type_post_id']    = $add_type_post_id  ;
		$savPrice['weight']            	= $max_weight  ;
		$savPrice['min_weight']      	= $min_weight  ;
		$savPrice['max_weight']         = $max_weight  ;
		$savPrice['description']        = $description  ;
		$savPrice['post_price']         = $post_price  ;
		$savPrice['create_by']          = $user_id  ;

		if($addType == "edit"){
			if($mr_post_price_id == ''){
				echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
				exit();
			}else{
				$mr_post_price_id	= $post_priceDao->save($savPrice,$mr_post_price_id );
			}
		}else{
			$mr_post_price_id	= $post_priceDao->save($savPrice);
		}
		echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
		exit();
	}catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(
				array('status' => 505, 
				'message' => 'เกิดข้อผิดพลาด "กรุณาบันทึกข้อมูล" อีกครั้ง' ,
				'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}

}else if($page == "setpriceonly"){
	try
	{
		// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
		NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
		// form parsing, DB inserts, etc.
		// ...
		$result = 'CSRF check passed. Form parsed.';
	}
	catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
	}
	try
	{
		$id  							=  $req ->get('id');
		$post_price  					=  $req ->get('price');
		$savPrice['post_price']         = $post_price  ;

		if($id!='' and $post_price != ''){
			$mr_post_price_id	= $post_priceDao->save($savPrice,$id);
			echo json_encode(array('status' => 200, 'message' => 'ลบข้อมูลสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}else{
			echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();

		}
	}catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}
	exit();
	
}else if($page == "remove"){
	try
	{
		// Run CSRF check, on POST data, in exception mode, for 10 minutes, in one-time mode.
		NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
		// form parsing, DB inserts, etc.
		// ...
		$result = 'CSRF check passed. Form parsed.';
	}
	catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด CSRF กรุณากดปุ่ม "บันทึกข้อมูล" อีกครั้ง' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}
	try
	{
		$id  	=  $req ->get('id');
		if($id!=''){
			$mr_post_price_id	= $post_priceDao->remove($id);
			echo json_encode(array('status' => 200, 'message' => 'ลบข้อมูลสำเร็จ' ,'barcodeok' => $barcodeok ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();
		}else{
			echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด' ,'token'=>NoCSRF::generate( 'csrf_token' )));
			exit();

		}
	}catch ( Exception $e )
	{
		// CSRF attack detected
		$result = $e->getMessage() . ' Form ignored.';
		echo json_encode(array('status' => 505, 'message' => 'เกิดข้อผิดพลาด' ,'token'=>NoCSRF::generate( 'csrf_token' )));
		
		exit();
	}

}else if($page == "loadPrice"){
	$mr_type_post_id 		=  $req ->get('mr_type_post_id');
	$mr_post_price_id  		=  $req ->get('id');
	
	$data	= $post_priceDao->load_postPrice($mr_type_post_id,$mr_post_price_id);	
	foreach($data as $key=>$val){
		if($val['min_weight'] == ''){
			list($data[$key]['min_weight'],$data[$key]['max_weight']) = explode('-', $val['description']);
		}
		$data[$key]['no'] = $key+1;
		$data[$key]['action'] ='<div class="btn-group" role="group" aria-label="Action">';
		$data[$key]['action'] 	.='<button type="button" class="btn btn-warning btn-sm" onclick="edit_price('.$val['mr_post_price_id'].')" title="แก้ไขข้อมูล"><i class="material-icons">edit</i></button>';
		if($val['create_by'] != ''){
			$data[$key]['action'] 	.='<button type="button" class="btn btn-danger btn-sm" onclick="remove_price('.$val['mr_post_price_id'].')" title="ลบข้อมูล"><i class="material-icons">backspace</i></button>';
		}else{
			$data[$key]['action'] 	.='<button type="button" class="btn btn-danger btn-sm" title="ลบข้อมูล" disabled><i class="material-icons">backspace</i></button>';
		}
		$data[$key]['action'] .='</div">';//post_price

		$data[$key]['post_price'] ='
		<div class="input-group">
			<input id="txt-set-price-'.$val['mr_post_price_id'].'" type="number" class="form-control form-control-sm col-2" value="'.$val['post_price'].'" aria-describedby="basic-addon2" readonly style="width: 200px;">
			<div class="input-group-append">
				
				<button id="btn-set-save-'.$val['mr_post_price_id'].'" onclick="setpriceonly('.$val['mr_post_price_id'].')" class="btn btn-outline-success btn-sm" type="button" style="display: none;">
					<i class="material-icons">
						check
					</i>
				</button>
				<button id="btn-reset-save-'.$val['mr_post_price_id'].'" onclick="resetfrome('.$val['mr_post_price_id'].')" class="btn btn-outline-danger btn-sm" type="button" style="display: none;">
					<i class="material-icons">
					close
					</i>
				</button>
				
				<button id="btn-set-change-'.$val['mr_post_price_id'].'" onclick="check_click('.$val['mr_post_price_id'].')" type="button" class="btn btn-outline-info btn-sm" title="แก้ไขข้อมูล"><i class="material-icons">edit</i></button>		
			</div>
		</div>
		
		';
		//$data[$key]['action'] ='';
	}

	$is_empty = 1;
	if(empty($data)){
		$is_empty = 0;
	}
	echo json_encode(
		array(
			'status' 	=> 200, 
			'is_empty' 	=> $is_empty, 
			'data' 		=> $data, 
			'token' 	=> NoCSRF::generate( 'csrf_token' ), 
			'message' => 'สำเร็จ' 
			)
		);

}



 ?>
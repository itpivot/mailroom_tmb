<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$userDao 			= new Dao_User();   



$user_id = $req->get('user_id');
$status = $req->get('status');
$page = $req->get('page');

if(preg_match('/<\/?[^>]+(>|$)/', $user_id)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $status)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $page)) {
    echo json_encode(array('status' => 500, 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง'));
    exit();
}

if( $page == 1 ){


//echo "<pre>".print_r($barcode_full,true)."</pre>";
//exit();

$save_user['isFailed'] = $status;
$save_user['active'] = 1;
$userDao->save($save_user,$user_id);

}else if ( $page == 2 ){
	$save_user['isLogin'] = $status;
	$userDao->save($save_user,$user_id);
}else if( $page == 3 ){
	$userDao->updateStatus();
}

echo json_encode(array('status' => 200, 'message' => 'บันทึกสำเร็จ'));

// echo $result;

?>

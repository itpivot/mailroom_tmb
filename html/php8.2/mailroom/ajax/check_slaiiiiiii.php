<?php
class Class_check_sla
{
	//$holiday=array();

	function check_sla($type_send=null,$type_re=null,$date_start=null,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data){
		$day = date ("D", strtotime($date_start)); 
		$min_time = $min_round_data[$type_send][$type_re][0]['time_start'];
		$max_time = $max_round_data[$type_send][$type_re][0]['time_start'];
		$round_data = $delivery_round_data[$type_send][$type_re];
		//return $max_round_data[$type_send][$type_re];

		$date1  				= $this->check_date_start($holiday_data,$date_start,$min_time,$max_time);
		$round  				= $this->get_round_data($date1,$round_data);
		$date_end  				= $this->get_date_end($date1,$round);
		$date2  				= $this->check_date_end($holiday_data,$date_end,$min_time,$max_time);

		return $date2;
	}

	function get_date_end($sys_time,$round){
		if(empty($round)){
			
		}else{
			$time_sla  	 = $round['time_sla'];
			$numdate_sla = $round['numdate_sla'];
			if($numdate_sla > 0){
				$sla_date_end = date ("Y-m-d ".$time_sla, strtotime("+".$numdate_sla." day", strtotime($sys_time))); 
				$gg['e1']=$sla_date_end ;
			}else{
				$sla_date_end = date ("Y-m-d ".$time_sla, strtotime($sys_time)); 
			}
		}
		return $sla_date_end;
	}

	function get_round_data($date_start,$delivery_round){
		$time_send     			= date("H:i:s",strtotime($date_start));
		if(empty($delivery_round)){
			$delivery_round = array();
		}else{
			foreach($delivery_round as $key => $round ){
				$dif = TimeDiff($time_send,$round['time_start']);
				if($dif>=0){
					$delivery_round 	= $round;
					break;
				}
			}
			
		}
		return $delivery_round;
	}

	function check_date_start($holiday_data,$date_start,$min_time,$max_time){
		$time = date ("H:i:s", strtotime($date_start)); 
		$timediff = $this->TimeDiff($time,$max_time);
		if($timediff < 0){
			$date_start = date ("Y-m-d ".$min_time, strtotime("+1 day", strtotime($date_start))); 
		}else{
			$date_start = $date_start;
		}
		
		$y = date ("Y", strtotime($date_start));
		$m = date ("m", strtotime($date_start));
		$holiday = isset($holiday_data[$y][$m])?$holiday_data[$y][$m]:array();	
		$d = date ("d", strtotime($date_start));
		if(!empty($holiday)){
			if (in_array($d, $holiday)) {
				$date_start = date ("Y-m-d ".$min_time, strtotime("+1 day", strtotime($date_start))); 
			}
		}

		$time = date ("H:i:s", strtotime($date_start)); 
		$day = date ("D", strtotime($date_start)); 
		if($day == "Sat"){
			$date_start = date ("Y-m-d ".$min_time, strtotime("+2 day", strtotime($date_start))); 
		}elseif($day == "Sun"){
			$date_start = date ("Y-m-d ".$min_time, strtotime("+1 day", strtotime($date_start))); 
		}elseif($day == "Fri"){
			$timediff = $this->TimeDiff($time,$max_time);
			if($timediff < 0){
				$date_start = date ("Y-m-d ".$min_time, strtotime("+3 day", strtotime($date_start))); 
			}else{
				$date_start = $date_start;
			}
		}else{
			$date_start = $date_start;
		}

		$y = date ("Y", strtotime($date_start));
		$m = date ("m", strtotime($date_start));
		$holiday = isset($holiday_data[$y][$m])?$holiday_data[$y][$m]:array();
		$d = date ("d", strtotime($date_start));
		if(!empty($holiday)){
			if (in_array($d, $holiday)) {
				$date_start = date ("Y-m-d ".$min_time, strtotime("+1 day", strtotime($date_start))); 
			}
		}

		return $date_start;
	}


	function check_date_end($holiday_data,$date_start,$min_time,$max_time){
		$y = date ("Y", strtotime($date_start));
		$m = date ("m", strtotime($date_start));
		$holiday = isset($holiday_data[$y][$m])?$holiday_data[$y][$m]:array();	
		$d = date ("d", strtotime($date_start));
		if(!empty($holiday)){
			if (in_array($d, $holiday)) {
				$date_start = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($date_start))); 
			}
		}

		$time = date ("H:i:s", strtotime($date_start)); 
		$day = date ("D", strtotime($date_start)); 
		if($day == "Sat"){
			$date_start = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($date_start))); 
		}elseif($day == "Sun"){
			$date_start = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($date_start))); 
		}else{
			$date_start = $date_start;
		}

		$y = date ("Y", strtotime($date_start));
		$m = date ("m", strtotime($date_start));
		$holiday = isset($holiday_data[$y][$m])?$holiday_data[$y][$m]:array();
		$d = date ("d", strtotime($date_start));
		if(!empty($holiday)){
			if (in_array($d, $holiday)) {
				$date_start = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($date_start))); 
			}
		}

		return $date_start;
	}


	function TimeDiff($strTime1,$strTime2){
		return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
	}

	function DateTimeDiff($strDateTime1,$strDateTime2){
		return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
	}

	function wav_sla($sla_date_end){
		$day = date ("D", strtotime($sla_date_end)); 
		if($day == "Sat"){
			$date_end = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($sla_date_end))); 
		}elseif($day == "Sat"){
			$date_end = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($sla_date_end))); 
		}else{
			$date_end = $sla_date_end;
		}
		return $date_end;
	}


}
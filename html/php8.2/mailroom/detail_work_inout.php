<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();

$data = json_decode($req->get('param'), true);

$data['start_date'] = setFormatDate($data['start_date']);
$data['end_date'] = setFormatDate($data['end_date']);

//echo print_r($data);

function setFormatDate($date){
    $result = '';
    $old_date  = explode('/', $date);
    $result = $old_date[2]."-".$old_date[1]."-".$old_date[0];
    return $result;
}

$result = $work_inout_Dao->detailSummary($data);


foreach($result as $indx => $vals) {
    $result[$indx]['no'] = $indx+1;
}

$template = Pivot_Template::factory('mailroom/detail_work_inout.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'mess_data' => $mess_data,
    'users' => $users,
    'result' => $result,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));

?>
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
/* Check authentication */

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();

$data = json_decode($req->get('param'), true);

$result  = array();
$alert = '';

$data['start_date'] = setFormatDate($data['start_date']);
$data['end_date'] = setFormatDate($data['end_date']);
$data['mr_user_id'] =  base64_decode(urldecode($data['mr_user_id']));

function setFormatDate($date){
    $result = '';
    if(!empty($date)) {
         $old_date  = explode('/', $date);
        $result = $old_date[2]."-".$old_date[1]."-".$old_date[0];
        return $result;
    }
}

if(!empty($data['start_date'])){
    if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['start_date'])) {
        $alert = "
        $.confirm({
        title: 'Alert!',
        content: 'เกิดข้อผิดพลาด!',
        buttons: {
            OK: function () {
                location.href = 'summary_sender.php';
                }
            }
        });
            
        ";
    }
}
if(!empty($data['end_date'])){
    if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data['end_date'])) {
        $alert = "
        $.confirm({
        title: 'Alert!',
        content: 'เกิดข้อผิดพลาด!',
        buttons: {
            OK: function () {
                location.href = 'summary_sender.php';
                }
            }
        });
            
        ";
    }
}
if(preg_match('/<\/?[^>]+(>|$)/', $data['mr_user_id'])) {
    $alert = "
        $.confirm({
        title: 'Alert!',
        content: 'เกิดข้อผิดพลาด!',
        buttons: {
            OK: function () {
                location.href = 'summary_sender.php';
                }
            }
        });
            
        ";
}

if($alert == ""){

    $result = $work_inout_Dao->detailSummaryMessenger($data);
    $recieve_arr = array(1,2,6);
    foreach($result as $indx => $vals) {
        $result[$indx]['no'] = $indx+1;
        $result[$indx]['sys_timestamp'] = date('d/m/Y H:i', strtotime($vals['sys_timestamp']));
        if(in_array($vals['status'], $recieve_arr)) {
            $result[$indx]['mr_user_id'] = $vals['usr_main_id'];
            $result[$indx]['mr_code'] = $vals['username_main'];
        } else {
            $result[$indx]['mr_user_id'] = $vals['usr_inout_id'];
            $result[$indx]['mr_code'] = $vals['username_inout'];
        }
    }
}

$template = Pivot_Template::factory('mailroom/detail_work_messenger.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	'userRoles' => $userRoles,
	'success' => $success,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'mess_data' => $mess_data,
    'users' => $users,
    'result' => $result,
    'alert' => $alert,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));

?>
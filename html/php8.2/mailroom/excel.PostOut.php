<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_post.php';
require_once 'PHPExcel.php';
error_reporting(E_ALL & ~E_NOTICE);

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();     
$work_postDao 		= new Dao_Work_post();

 
$data_search		=  array();       
$start_date 		= $req->get('date_1');
$end_date 			= $req->get('date_2');

// echo $start_date."<br>";
// echo $end_date."<br>";

$data_search['start_date'] 	= $start_date;
$data_search['end_date'] 	= $end_date;

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}
$data = $work_postDao->ExportDataReportPrice($data_search);

// echo "<pre>".print_r($data,true)."</pre>";
// exit;

$arr_report1	= array();
$sheet1 		= 'Detail';
$headers1  		= array(
	 'NO',                                       
	 'วันที่สร้างรายการ',                                       
	 'วันที่ส่ง',                                       
	 'รอบ',                                       
	 'ประเภทการส่ง',                                       
	 'Barcode',                                       
	 'Barcode(ตอบรับ)',                                                               
	 'ผู้ส่ง',  
	 'หน่วยงานผู้ส่ง',                                  
	 'ผู้รับ',   
	 'ที่อยู่',
	 'จำนวน',
	 'ราคา', 
	 'ราคารวม', 
	 'หมายเหตุ'
);


$indexs = 2;
foreach($data as $keys => $vals) {
	
			$arr_report1[$keys][0]  	=	$keys+1;
			$arr_report1[$keys][1]  	=	$vals['sys_timestamp'];
			$arr_report1[$keys][2]  	=	$vals['d_send'];
			$arr_report1[$keys][3]  	=	$vals['mr_round_name'];
			$arr_report1[$keys][4]  	=	$vals['mr_type_post_name'];
			$arr_report1[$keys][5]  	=	"'".$vals['mr_work_barcode'];
			$arr_report1[$keys][6]  	=	$vals['sp_num_doc'];
			$arr_report1[$keys][7]  	=	$vals['mr_emp_code']." ".$vals['mr_emp_name']." ".$vals['mr_emp_lastname'];
			$arr_report1[$keys][8]  	=	$vals['dep_code_send']." ".$vals['dep_send'];
			$arr_report1[$keys][9]  	=	$vals['sresive_name'];
			$arr_report1[$keys][10]  	=	$vals['mr_address'];
			$arr_report1[$keys][11]  	=	$vals['quty'];
			$arr_report1[$keys][12]  	=	$vals['mr_post_price'];
			$arr_report1[$keys][13]  	=	$vals['mr_post_totalprice'];
			$arr_report1[$keys][14]  	=	$vals['mr_work_remark'];
	unset($data[$keys]);
}


//echo "ok";
//echo "<pre>".print_r($data[$keys],true)."</pre>";
//echo "<pre>".print_r($arr_report1[$keys],true)."</pre>";
//exit;




$time_end = microtime_float();
$time = $time_end - $time_start;

//echo "<br> Did nothing in $time seconds\n";
//exit;





$file_name = 'TMB-Report-Thai-PostOut'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($arr_report1 as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
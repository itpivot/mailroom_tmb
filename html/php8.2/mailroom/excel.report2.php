<?php
header('Content-Type: text/html; charset=utf-8');
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php'; 
require_once 'PHPExcel.php';
error_reporting(E_ALL & ~E_NOTICE);

include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');

$time_start = microtime(true);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

// echo "---";
// exit;


$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                     
 
$data_search		    		=  json_decode($req->get('params'),true);                                                  
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}

$sql = 'SELECT * FROM mr_hub ORDER BY mr_hub_id ASC';
$hub_data = $work_inoutDao->select_work_inout($sql,array());
$hub = array();
foreach($hub_data as $key => $val_hub){
	$hub[$val_hub['mr_hub_id']]	 =  $val_hub['mr_hub_name'];
}
// echo '>>>>>>>>>>..<pre>'.print_r($hub,true);
// exit;
$params 	= array();
$log		= array();
$log_q		= array();
$main_id    = array();


	$params1 	= array();
	$sql_log = 'SELECT
					l.mr_work_main_id,
					l.mr_status_id,
					l.sys_timestamp   ,
					e.mr_emp_code     ,
					e.mr_emp_name     ,
					e.mr_emp_lastname ,
    				CASE WHEN l.mr_status_id in(2,9) THEN l.sys_timestamp   ELSE NULL END as      time_st1,
    				CASE WHEN l.mr_status_id in(5,12) THEN l.sys_timestamp   ELSE NULL END as      time_st2
				FROM(
						SELECT
							l.mr_work_main_id
						FROM
							mr_work_log l
						LEFT JOIN mr_user u ON (u.mr_user_id = l.mr_user_id)
						LEFT JOIN mr_emp e ON (e.mr_emp_id = u.mr_emp_id)
						where l.sys_timestamp  BETWEEN  "2022-05-01" and "2022-05-31" 
						and l.mr_work_main_id is not null
						GROUP BY l.mr_work_main_id

					UNION ALL 

						SELECT 
							m.mr_work_main_id
						FROM mr_work_main m
						where m.sys_timestamp BETWEEN  "2022-05-01" and "2022-05-31" 
						and  m.mr_work_main_id is not null
						GROUP BY m.mr_work_main_id

					UNION ALL 

						SELECT 
						rsw.mr_work_main_id
						FROM mr_round_resive_work rsw
						where rsw.sysdate  BETWEEN   "2022-05-01" and "2022-05-31" 
						and rsw.mr_work_main_id is not null
						GROUP BY rsw.mr_work_main_id
					) ml
				LEFT JOIN mr_work_log l ON (ml.mr_work_main_id = l.mr_work_main_id)
				LEFT JOIN mr_user u ON (u.mr_user_id = l.mr_user_id)
				LEFT JOIN mr_emp e ON (e.mr_emp_id = u.mr_emp_id)
			';
		//$sql_log .= '   l.sys_timestamp  BETWEEN  ? and  ? ';
        $sql_log .= '  group by mr_work_main_id ';
       // $sql_log .= '  UNION ALL ';
		//$sql .= '  AND ( m.sys_timestamp BETWEEN "'.$data_search['start_date'].'" AND "'.$data_search['end_date'].'" )';
		array_push($params1, (string)$data_search['start_date']);
		array_push($params1, (string)$data_search['end_date']);
        $time_start = microtime(true);
		//$log_q = $work_inoutDao->select_work_inout($sql_log,$params1);
        $time_end = microtime(true);
        $time = $time_end - $time_start;

        //echo '>>>>>>>>>>..'.$time;
       // echo '>>>>>>>>>>..<pre>'.print_r(count($log_q),true);
       //exit;

      //  exit;
//  echo '>>>>>>>>>>..<pre>'.print_r($txt_main_id,true);
//  exit;



$sql =
		'
		SELECT 
				m.mr_work_main_id,
				m.sys_timestamp,
				m.mr_work_date_sent,
				m.mr_work_barcode,
				m.mr_work_remark,
				m.mr_type_work_id,
				m.mr_status_id,
				m.mr_user_id as m_user_id,
				m.mr_floor_id as sen_floor_id,
				m.mr_round_id,
				m.mr_work_date_success,
				m.mr_topic,
				m.mr_branch_id as sen_branch_id,
				m.mr_send_work_id,
				m.messenger_user_id,
				type.mr_type_work_name,
				status.mr_status_name,
				i.rate_remark,
				rs.mr_round_name,
				CASE
					WHEN i.rate_send is null  THEN "" 
					WHEN i.rate_send = 5 THEN "พึ่งพอใจ" 
					ELSE "ไม่พึ่งพอใจ"
				END as rate_send,
				';

				//sender data
		$sql .= ' 
		
				concat(dep_send.mr_department_code,":",dep_send.mr_department_name) as dep_send,
				CASE
					WHEN b_send1.mr_hub_id != "" 
					THEN b_send1.mr_hub_id
					ELSE b_send2.mr_hub_id
				END as send_hub_id,
				CASE
					WHEN b_send1.mr_branch_code != "" 
					THEN concat(b_send1.mr_branch_code,":",b_send1.mr_branch_name) 
					ELSE concat(b_send2.mr_branch_code,":",b_send2.mr_branch_name) 
				END as b_send,
				CASE
					WHEN u_send.mr_user_role_id = 2 THEN "สำนักงานใหญ่" 
					WHEN u_send.mr_user_role_id = 5 THEN "สาขา" 
					ELSE "admin"
				END as role_send,
				CASE
					WHEN bt_send.branch_type_name =  "" or bt_send.branch_type_name is null  THEN "สำนักงานใหญ่" 
					ELSE bt_send.branch_type_name
				END as bt_send,
				concat(emp_send.mr_emp_code,":",emp_send.mr_emp_name," ",emp_send.mr_emp_lastname) as emp_send,
				f_send.name as f_send,
				';
			
		// resive data
			$sql .= ' 
			
			
				concat(dep_resive.mr_department_code,":",dep_resive.mr_department_name) as dep_resive,
				CASE
					WHEN b_resive1.mr_hub_id != "" 
					THEN b_resive1.mr_hub_id
					ELSE b_resive2.mr_hub_id
				END as resive_hub_id,
				CASE
					WHEN b_resive1.mr_branch_code != "" 
					THEN concat(b_resive1.mr_branch_code,":",b_resive1.mr_branch_name) 
					ELSE concat(b_resive2.mr_branch_code,":",b_resive2.mr_branch_name) 
				END as b_resive,
				CASE
					WHEN u_resive.mr_user_role_id = 2 THEN "สำนักงานใหญ่" 
					WHEN u_resive.mr_user_role_id = 5 THEN "สาขา" 
					ELSE "admin"
				END as role_resive,
				CASE
					WHEN bt_resive.branch_type_name =  "" or bt_resive.branch_type_name is null THEN "สำนักงานใหญ่" 
					ELSE bt_resive.branch_type_name
				END as bt_resive,
				concat(emp_resive.mr_emp_code,":",emp_resive.mr_emp_name," ",emp_resive.mr_emp_lastname) as emp_resive,
				f_resive.name as f_resive,
			';
			
			// mess send data
			$sql .= ' 
				concat(emp_mess_send.mr_emp_code,":",emp_mess_send.mr_emp_name," ",emp_mess_send.mr_emp_lastname) as emp_mess_send,
			';
			
			// mess send data
			$sql .= ' 
				concat(emp_mess_resive.mr_emp_code,":",emp_mess_resive.mr_emp_name," ",emp_mess_resive.mr_emp_lastname) as emp_mess_resive
			';
				
			$sql .= ' 	
			FROM (
                SELECT
                    l.mr_work_main_id
                FROM  mr_work_log l
                LEFT JOIN mr_user u ON (u.mr_user_id = l.mr_user_id)
                LEFT JOIN mr_emp e ON (e.mr_emp_id = u.mr_emp_id)
                WHERE
                    l.sys_timestamp BETWEEN ? AND ?
                GROUP BY   l.mr_work_main_id
            UNION ALL
                SELECT
                    m.mr_work_main_id
                FROM mr_work_main m
                WHERE  m.sys_timestamp BETWEEN ? AND ?
                GROUP BY  m.mr_work_main_id
            UNION ALL
                SELECT
                    rsw.mr_work_main_id
                FROM  mr_round_resive_work rsw
                WHERE rsw.sysdate BETWEEN ? AND ?
                GROUP BY rsw.mr_work_main_id
            ) ml
			left join mr_work_main m on(m.mr_work_main_id = ml.mr_work_main_id)
			left join mr_work_inout i on(i.mr_work_main_id = m.mr_work_main_id)
			Left join mr_type_work type on( type.mr_type_work_id = m.mr_type_work_id )
			Left join mr_status status on (status.mr_status_id = m.mr_status_id)
			
			';
		//mr_round_resive_work
		;

        array_push($params, (string)$data_search['start_date']);
        array_push($params, (string)$data_search['end_date']);

        array_push($params, (string)$data_search['start_date']);
        array_push($params, (string)$data_search['end_date']);

        array_push($params, (string)$data_search['start_date']);
        array_push($params, (string)$data_search['end_date']);

		$sql .= '
			left join mr_round_resive_work rsw on(rsw.mr_work_main_id = m.mr_work_main_id)
			left join mr_round rs on(rs.mr_round_id = rsw.mr_round_id)
		';
		//sender data
		$sql .= ' 
			Left join mr_user u_send on ( u_send.mr_user_id = m.mr_user_id )
			Left join mr_emp emp_send on ( emp_send.mr_emp_id = u_send.mr_emp_id )
			Left join mr_department dep_send on ( dep_send.mr_department_id = emp_send.mr_department_id )
				left join mr_branch b_send1 on ( b_send1.mr_branch_id = m.mr_branch_id )
				left join mr_branch b_send2 on ( b_send2.mr_branch_id = emp_send.mr_branch_id )
				Left join mr_floor f_send on ( f_send.mr_floor_id = m.mr_floor_id )	
				Left join mr_branch_type bt_send on(bt_send.mr_branch_type_id = (
						CASE  WHEN b_send1.mr_branch_type != ""
								THEN b_send1.mr_branch_type
								ELSE b_send2.mr_branch_type
							END
						)
					)
			
			';
			
		// resive data
			$sql .= ' 
			Left join mr_emp emp_resive on ( emp_resive.mr_emp_id = i.mr_emp_id )
			Left join mr_user u_resive on ( u_resive.mr_emp_id = emp_resive.mr_emp_id )
			Left join mr_department dep_resive on ( dep_resive.mr_department_id = emp_resive.mr_department_id )
				left join mr_branch b_resive1 on ( b_resive1.mr_branch_id = i.mr_branch_id )
				left join mr_branch b_resive2 on ( b_resive2.mr_branch_id = emp_resive.mr_branch_id )
				Left join mr_floor f_resive on ( f_resive.mr_floor_id = i.mr_floor_id )	
				Left join mr_branch_type bt_resive on(bt_resive.mr_branch_type_id = (
						CASE  WHEN b_resive1.mr_branch_type != ""
								THEN b_resive1.mr_branch_type
								ELSE b_resive2.mr_branch_type
							END
						)
					)
			';
			
			// mess send data
			$sql .= ' 
			Left join mr_zone zone_send on ( zone_send.mr_floor_id = i.mr_floor_id)
			Left join mr_user u_mess_send on ( u_mess_send.mr_user_id = zone_send.mr_user_id )
			Left join mr_emp emp_mess_send on ( emp_mess_send.mr_emp_id = u_mess_send.mr_emp_id )
			
			';
			
			// mess resive data

			$sql .= ' 
			Left join mr_zone zone_resive on ( zone_resive.mr_floor_id = m.mr_floor_id)
			Left join mr_user u_mess_resive on ( u_mess_resive.mr_user_id = zone_resive.mr_user_id )
			Left join mr_emp emp_mess_resive on ( emp_mess_resive.mr_emp_id = u_mess_resive.mr_emp_id )
			
			';
			$sql .= ' 
			WHERE
				m.mr_status_id <> 0
		';
		
		
		// if(!empty($main_id)){
        //     $in_status_condition = array(); // condition: generate ?,?,?
        //     $in_status_condition = str_repeat('?,', count($main_id) - 1) . '?'; // example: ?,?,?
        //     $sql .= 'and   m.mr_work_main_id in('.$in_status_condition.') ';
        //     $params = array_merge($params,$main_id);
        // }
		if( $data_search['select_mr_contact_id'] != "" ){
			$sql .= ' AND i.mr_contact_id = ? ';
			array_push($params, (int)$data_search['select_mr_contact_id']);
		}
		if( $data_search['barcode'] != "" ){
			$sql .= ' AND m.mr_work_barcode = ? ';
			array_push($params, (string)$data_search['barcode']);
		}
		
		
		
		
		
		if( $data_search['sender'] != "" ){
			$sql .= ' AND emp_send.mr_emp_code = ? ';
			array_push($params, (string)$data_search['sender']);
		}
		
		if( $data_search['receiver'] != "" ){
			$sql .= ' AND emp_resive.mr_emp_code = ? ';
			array_push($params, (string)$data_search['receiver']);
		}
		
		
		
		
		if( $data_search['status'] != "" ){
			if( $data_search['status'] != "Pending" ){
				$sql .= ' AND m.mr_status_id = ? ';
				array_push($params, (int)$data_search['status']);
			}else{
				$sql .= ' AND DATE_FORMAT(m.sys_timestamp, "%H : %I : %S") >= "16:00:00"';
			}
		}
		$sql .= ' group by m.mr_work_main_id ';
		//$sql .= ' limit 0,20000 ';
	
			
//exit;
$time_start = microtime(true);
$workorder_data = $work_inoutDao->select_work_inout($sql,$params);
$time_end = microtime(true);

$time = $time_end - $time_start;
//echo '>>>>>>>>>>..'.$time;
$data = array();
// echo '>>>>>>>>>>..<pre>'.print_r($workorder_data,true);
//echo '>>>>>>>>>>..<pre>'.print_r(count($workorder_data),true);
//exit;

//$data = $work_inoutDao->searchMailroom_upQury($data_search);

// echo '>>>>>>>>>>..<pre>'.print_r($data[0],true);
 //exit;


$arr_report1	= array();
$sheet1 		= 'Detail';
$headers1  		= array(
	 'NO',                                       
	 'Barcode',                                       
	 'ชื่อผู้รับ',                                 
	 'สาขาผู้รับ',                              
	 'หน่วยงานผู้รับ',                              
	 'ชั้นผู้รับ',                              
	 'ประเภทสาขาผู้รับ',                     
	 'ศูนย์ผู้รับ',                     
	 'ชื่อผู้ส่ง',                                 
	 'สาขาผู้ส่ง',                              
	 'หน่วยงานผู้ส้ง',                              
	 'ชั้นผู้ส่ง',                              
	 'ประเภทสาขาผู้ส่ง',    
	 'ศูนย์ผู้ส่ง',    
	 'ชื่อเอกสาร',
	 'วันที่ส่ง',                    
	 'วันที่ห้อง Mailroom รับ',                    
	 'วันที่สำเร็จ',                          
	 'สถานะงาน',                           
	 'ประเภทการส่ง',                           
	 'หมายเหตุ',            
	 'ความพึงพอใจ',
	 'หมายเหตุความพึงพอใจ',
	 'ส่งจาก',
	 'พนักงานรับ',
	 'พนักงานส่ง',
	 'รอบ',
	 'จำนวน'
);



$indexs = 2;
foreach($data as $keys => $vals) {
	
			$arr_report1[$keys][]  	=	$keys+1;
			$arr_report1[$keys][]  	=	"'".$vals['mr_work_barcode'];
			$arr_report1[$keys][]  	=	isset($vals['emp_resive'])?$vals['emp_resive']:'';
			$arr_report1[$keys][]  	=	isset($vals['b_resive'])?$vals['b_resive']:'';
			$arr_report1[$keys][]  	=	isset($vals['dep_resive'])?$vals['dep_resive']:'';
			$arr_report1[$keys][]  	=	isset($vals['f_resive'])?$vals['f_resive']:'';
			$arr_report1[$keys][]  	=	isset($vals['bt_resive'])?$vals['bt_resive']:'';
			$arr_report1[$keys][]  	=	isset($hub[$vals['resive_hub_id']])?$hub[$vals['resive_hub_id']]:'';
			$arr_report1[$keys][]  	=	isset($vals['emp_send'])?$vals['emp_send']:'';
			$arr_report1[$keys][]  	=	isset($vals['b_send'])?$vals['b_send']:'';
			$arr_report1[$keys][]  	=	isset($vals['dep_send'])?$vals['dep_send']:'';
			$arr_report1[$keys][]  	=	isset($vals['f_send'])?$vals['f_send']:'';
			$arr_report1[$keys][]  	=	isset($vals['bt_send'])?$vals['bt_send']:'';
			$arr_report1[$keys][]  	=	isset($hub[$vals['send_hub_id']])?$hub[$vals['send_hub_id']]:'';
			$arr_report1[$keys][]  	=	isset($vals['mr_topic'])?$vals['mr_topic']:'';
			$arr_report1[$keys][]  	=	isset($vals['log_new'])?$vals['log_new']:'';
			$arr_report1[$keys][]  	=	isset($vals['log_mailroom'])?$vals['log_mailroom']:'';
			$arr_report1[$keys][]  	=	isset($vals['log_success'])?$vals['log_success']:'';
			$arr_report1[$keys][]  	=	isset($vals['mr_status_name'])?$vals['mr_status_name']:'';
			$arr_report1[$keys][]  	=	isset($vals['mr_type_work_name'])?$vals['mr_type_work_name']:'';
			$arr_report1[$keys][] 	=	isset($vals['mr_work_remark'])?$vals['mr_work_remark']:'';
			$arr_report1[$keys][]  	=	isset($vals['rate_send'])?$vals['rate_send']:'';//$rate_send;
			$arr_report1[$keys][]  	=	isset($vals['rate_remark'])?$vals['rate_remark']:'';//$rate_remark;
			$arr_report1[$keys][]  	=	'';//$mr_user_role;
			$arr_report1[$keys][]  	=	isset($vals['emp_mess_resive'])?$vals['emp_mess_resive']:'';
			$arr_report1[$keys][]  	=	isset($vals['emp_mess_send'])?$vals['emp_mess_send']:'';
			$arr_report1[$keys][]  	=	isset($vals['mr_round_name'])?$vals['mr_round_name']:'';
			$arr_report1[$keys][]  	=	isset($vals['count_qty'])?$vals['count_qty']:'';
	unset($data[$keys]);
}


//echo "ok";
//echo "<pre>".print_r($data[$keys],true)."</pre>";
$time_end = microtime(true);
$time = $time_end - $time_start;
// echo '>>>>>>>>>>..'.$time;
//echo "<pre>".print_r($arr_report1[$keys],true)."</pre>";
// exit;




$time_end = microtime_float();
$time = $time_end - $time_start;

//echo "<br> Did nothing in $time seconds\n";
//exit;





$file_name = 'TMB_Report'.DATE('y-m-d').'.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($arr_report1 as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
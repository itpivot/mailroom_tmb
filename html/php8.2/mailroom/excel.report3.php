<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'PHPExcel.php';
error_reporting(E_ALL & ~E_NOTICE);

ob_start();

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
 
$data_search		    		=  json_decode($req->get('params'),true);                                                  
   
$data = $work_inoutDao->searchMailroom( $data_search );

// echo "<pre>".print_r($data,true)."</pre>";
// exit;
function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}

//foreach( $data as $num => $d ){
//	if ( $d['group_user_id'] == 16 ){
//		if ( $data[$num]['status'] == "รอส่ง TMB" ) {
//			$data[$num]['status'] = "สำเร็จ";
//		}else{
//			$data[$num]['status'] = $d['status'];
//		}
//	}
//}
//echo '<pre>';
//echo print_r($data, true);
//echo '</pre>';
//exit();
			



$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma')->setSize(10);
$objPHPExcel->getProperties()->setCreator("Pivot Co.,Ltd")->setLastModifiedBy("Pivot")->setTitle("Report Agent")->setSubject("Report")->setDescription("Report document for Excel, generated using PHP classes.");

$columnIndexs = range('A','O');
$columnNames[0] = array(
		0 => 'No.',                           
		1 => 'Barcode',                            
		2 => 'ชื่อผู้รับ',                            
		3 => 'ที่อยู่',                            
		4 => 'ชื่อผู้ส่ง',                            
		5 => 'ที่อยู่',    
		6 => 'ชื่อเอกสาร',
		7 => 'วันที่ส่ง',                            
		8 => 'วันที่สำเร็จ',                            
		9 => 'สถานะงาน',                            
		10 => 'ประเภทการส่ง',                            
		11 => 'หมายเหตุ',
		12 => 'ความพึงพอใจ',
		13 => 'หมายเหตุความพึงพอใจ',
		13 => 'ส่งจาก'
			
);

// Set column widths
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setWidth(30);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('M')->setWidth(30);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('N')->setWidth(50);
    
$row = 1;
foreach ($columnNames as $key => $value) {
	for($index = 0; $index < count($columnIndexs); $index++) {
		$leadColumns[$key][$index]['index'] = $columnIndexs[$index].$row;
		$leadColumns[$key][$index]['name'] = $columnNames[$key][$index];
	}
	$row++;
}

foreach($leadColumns as $head) {
	foreach ($head as $headColumn) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($headColumn['index'], $headColumn['name'])->getStyle($headColumn['index']);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLACK);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->getStartColor()->setARGB('FFDCDCDC');
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);																																 
	}
}


 $indexs = 2;
// Add Data
$sumarya= array();
$sumarya2= array();
foreach($data as $keys => $vals) {
	$mr_user_role_id 		= $vals['mr_user_role_id'];
	$mr_type_work_id 		= $vals['mr_type_work_id'];
	$mr_status_id 		= $vals['mr_status_id'];
	$mr_branch_type 	= $vals['mr_branch_type1'];
	$mr_type_work_id 	= $vals['mr_type_work_id'];
	if($mr_branch_type == ''){
		$mr_branch_type = $vals['mr_branch_type2'];
	}
	if($mr_branch_type == ''){
		$mr_branch_type = 3;
	}


	if(!isset($sumarya[$mr_user_role_id][$mr_type_work_id][$mr_status_id])){
		      $sumarya[$mr_user_role_id][$mr_type_work_id][$mr_status_id] =0;
		      $sumarya2[$mr_user_role_id][$mr_type_work_id][$mr_status_id] =0;
	}
	
	if($mr_user_role_id==5){
		if($mr_branch_type==2){
			$mr_user_role = "สั่งจากสาขากรุงเทพมหานครและปริมณฑล";
			$sumarya2[$mr_user_role_id][$mr_type_work_id][$mr_status_id] +=1;
		}else{
			$mr_user_role = "สั่งจากสาขาต่างจังหวัด";
			$sumarya[$mr_user_role_id][$mr_type_work_id][$mr_status_id] +=1;
		}
	}elseif($mr_user_role_id==2){
		$mr_user_role = "สั่งจากสำนักงานใหญ";
		$sumarya[$mr_user_role_id][$mr_type_work_id][$mr_status_id] +=1;
		//echo $mr_status_id.":<br>";
	}else{
		$mr_user_role = "เกิดข้อผิดพลาด";
	}
			if( $vals['mr_work_date_sent'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_sent']);
				$date_order[$keys]['mr_work_date_sent'] = $d."/".$m."/".$y;
			}	
			
			if( $vals['mr_work_date_success'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_success']);
				$date_meet[$keys]['mr_work_date_success'] = $d."/".$m."/".$y;
			}	
			
			if(  $vals['name_re'] ){
				$name_receive[$keys]['name_re'] 				= $vals['name_re']." ".$vals['lastname_re'];
				$name_receive[$keys]['depart_receive'] 		= $vals['depart_code_receive']." - ".$vals['depart_name_receive']." ชั้น ".$vals['depart_floor_receive'];
				if($vals['depart_code_receive'] == ''){
					$name_receive[$keys]['depart_receive'] 	= $vals['re_branch_code']." - ".$vals['re_branch_name'];
					if($vals['re_branch_code'] == ''){
						$name_receive[$keys]['depart_receive'] 	= $vals['re_branch_code2']." - ".$vals['re_branch_name2'];
					}
				}
			}			
			
			if(  $vals['mr_emp_name'] ){
				$name_send[$keys]['send_name'] 				= $vals['mr_emp_name']." ".$vals['mr_emp_lastname'];
				$name_send[$keys]['depart_send'] 			= $vals['mr_department_code']." - ".$vals['mr_department_name']." ชั้น ".$vals['depart_floor_send'];
				if($vals['mr_department_code'] == ''){
					$name_send[$keys]['depart_send'] 	= $vals['send_branch_code']." - ".$vals['send_branch_name'];
					if($vals['send_branch_code']==''){
						$name_send[$keys]['depart_send'] 	= $vals['send_branch_code2']." - ".$vals['send_branch_name2'];
					}
				}
			}		
			
			if($vals['rate_send'] == 5  ){
				$rate_send[$keys]['rate_send']  = "พึ่งพอใจ";
			}elseif( $vals['rate_send'] == ""){
				$rate_send[$keys]['rate_send']  = "";
			}else{
				$rate_send[$keys]['rate_send']  = "ไม่พึ่งพอใจ";
			}
			
			if(  $vals['rate_remark'] != "" ){
				$rate_send[$keys]['rate_remark']  =  $vals['rate_remark'];
			}
			
		

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$indexs, ($keys+1))->getStyle('A'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$indexs, "'".$vals['mr_work_barcode'])->getStyle('B'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$indexs, $name_receive[$keys]['name_re'])->getStyle('C'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$indexs, $name_receive[$keys]['depart_receive'])->getStyle('D'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$indexs, $name_send[$keys]['send_name'])->getStyle('E'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$indexs, $name_send[$keys]['depart_send'])->getStyle('F'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$indexs, $vals['mr_topic'])->getStyle('G'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$indexs, $date_order[$keys]['mr_work_date_sent'])->getStyle('H'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$indexs, $date_meet[$keys]['mr_work_date_success'])->getStyle('I'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$indexs, $vals['mr_status_name'])->getStyle('J'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$indexs, $vals['mr_type_work_name'])->getStyle('K'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$indexs, $vals['mr_work_remark'])->getStyle('L'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$indexs, $rate_send[$keys]['rate_send'])->getStyle('M'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$indexs, $rate_send[$keys]['rate_remark'])->getStyle('N'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$indexs, $mr_user_role)->getStyle('N'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet(0)->getStyle('A'.$indexs.':O'.$indexs)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$indexs++;
}

//  echo "<pre>".print_r($sumarya,true)."</pre>";
//  exit;

if(isset($sumarya['5'][1])){

	$brabch_to_ho = $sumarya['5'][1];
	$h1 	= (isset($brabch_to_ho[1])?$brabch_to_ho[1]:0);
	$h2 	= (isset($brabch_to_ho[2])?$brabch_to_ho[2]:0);
	$h3 	= (isset($brabch_to_ho[3])?$brabch_to_ho[3]:0);
	$h4 	= (isset($brabch_to_ho[4])?$brabch_to_ho[4]:0);
	$h5 	= (isset($brabch_to_ho[5])?$brabch_to_ho[5]:0);
	$h6 	= (isset($brabch_to_ho[6])?$brabch_to_ho[6]:0);
	$h7 	= (isset($brabch_to_ho[7])?$brabch_to_ho[7]:0);
	$h8 	= (isset($brabch_to_ho[8])?$brabch_to_ho[8]:0);
	$h9 	= (isset($brabch_to_ho[9])?$brabch_to_ho[9]:0);
	$h10 	= (isset($brabch_to_ho[10])?$brabch_to_ho[10]:0);
	$h11 	= (isset($brabch_to_ho[11])?$brabch_to_ho[11]:0);
	$h12 	= (isset($brabch_to_ho[12])?$brabch_to_ho[12]:0);
	$h13 	= (isset($brabch_to_ho[13])?$brabch_to_ho[13]:0);
	$h14 	= (isset($brabch_to_ho[14])?$brabch_to_ho[14]:0);
	$h15 	= (isset($brabch_to_ho[15])?$brabch_to_ho[15]:0);

}else{

	$h1 	= 0;
	$h2 	= 0;
	$h3 	= 0;
	$h4 	= 0;
	$h5 	= 0;
	$h6 	= 0;
	$h7 	= 0;
	$h8 	= 0;
	$h9 	= 0;
	$h10 	= 0;
	$h11 	= 0;
	$h12 	= 0;
	$h13 	= 0;
	$h14 	= 0;
	$h15 	= 0;
	
}

if(isset($sumarya['5'][3])){

	$brabch_to_ho2 = $sumarya['5'][3];
	$h1 	+= (isset($brabch_to_ho2[1])?$brabch_to_ho2[1]:0);
	$h2 	+= (isset($brabch_to_ho2[2])?$brabch_to_ho2[2]:0);
	$h3 	+= (isset($brabch_to_ho2[3])?$brabch_to_ho2[3]:0);
	$h4 	+= (isset($brabch_to_ho2[4])?$brabch_to_ho2[4]:0);
	$h5 	+= (isset($brabch_to_ho2[5])?$brabch_to_ho2[5]:0);
	$h6 	+= (isset($brabch_to_ho2[6])?$brabch_to_ho2[6]:0);
	$h7 	+= (isset($brabch_to_ho2[7])?$brabch_to_ho2[7]:0);
	$h8 	+= (isset($brabch_to_ho2[8])?$brabch_to_ho2[8]:0);
	$h9 	+= (isset($brabch_to_ho2[9])?$brabch_to_ho2[9]:0);
	$h10 	+= (isset($brabch_to_ho2[10])?$brabch_to_ho2[10]:0);
	$h11 	+= (isset($brabch_to_ho2[11])?$brabch_to_ho2[11]:0);
	$h12 	+= (isset($brabch_to_ho2[12])?$brabch_to_ho2[12]:0);
	$h13 	+= (isset($brabch_to_ho2[13])?$brabch_to_ho2[13]:0);
	$h14 	+= (isset($brabch_to_ho2[14])?$brabch_to_ho2[14]:0);
	$h15 	+= (isset($brabch_to_ho2[15])?$brabch_to_ho2[15]:0);

}else{
	
	$h1 	+= 0;
	$h2 	+= 0;
	$h3 	+= 0;
	$h4 	+= 0;
	$h5 	+= 0;
	$h6 	+= 0;
	$h7 	+= 0;
	$h8 	+= 0;
	$h9 	+= 0;
	$h10 	+= 0;
	$h11 	+= 0;
	$h12 	+= 0;
	$h13 	+= 0;
	$h14 	+= 0;
	$h15 	+= 0;
	
}


if(isset($sumarya['5'][2])){
	$brabch_to_bronch = $sumarya['5'][2];
	$t1 	= (isset($brabch_to_bronch[1])?$brabch_to_bronch[1]:0);
	$t2 	= (isset($brabch_to_bronch[2])?$brabch_to_bronch[2]:0);
	$t3 	= (isset($brabch_to_bronch[3])?$brabch_to_bronch[3]:0);
	$t4 	= (isset($brabch_to_bronch[4])?$brabch_to_bronch[4]:0);
	$t5 	= (isset($brabch_to_bronch[5])?$brabch_to_bronch[5]:0);
	$t6 	= (isset($brabch_to_bronch[6])?$brabch_to_bronch[6]:0);
	$t7 	= (isset($brabch_to_bronch[7])?$brabch_to_bronch[7]:0);
	$t8 	= (isset($brabch_to_bronch[8])?$brabch_to_bronch[8]:0);
	$t9 	= (isset($brabch_to_bronch[9])?$brabch_to_bronch[9]:0);
	$t10 	= (isset($brabch_to_bronch[10])?$brabch_to_bronch[10]:0);
	$t11 	= (isset($brabch_to_bronch[11])?$brabch_to_bronch[11]:0);
	$t12 	= (isset($brabch_to_bronch[12])?$brabch_to_bronch[12]:0);
	$t13 	= (isset($brabch_to_bronch[13])?$brabch_to_bronch[13]:0);
	$t14 	= (isset($brabch_to_bronch[14])?$brabch_to_bronch[14]:0);
	$t15 	= (isset($brabch_to_bronch[15])?$brabch_to_bronch[15]:0);
}else{
	$t1 	= 0;
	$t2 	= 0;
	$t3 	= 0;
	$t4 	= 0;
	$t5 	= 0;
	$t6 	= 0;
	$t7 	= 0;
	$t8 	= 0;
	$t9 	= 0;
	$t10 	= 0;
	$t11 	= 0;
	$t12 	= 0;
	$t13 	= 0;
	$t14 	= 0;
	$t15 	= 0;
}
//echo "<pre>".print_r($t1,true)."</pre>";
//echo "<pre>".print_r($sumarya['br2'],true)."</pre>";
//exit;


$objPHPExcel->createSheet(2);
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('D')->setWidth(15);

$objPHPExcel->getActiveSheet(1)->setCellValue('A1',"สาขาต่างจังหวัด"							,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A2',"NO"							,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B2',"สถานะงาน"					,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C2',"ส่งสาขา"					 ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D2',"ส่งที่สำนักงานใหญ่"			    ,PHPExcel_Cell_DataType::TYPE_STRING);

$objPHPExcel->getActiveSheet(1)->setCellValue('A3',"1"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A4',"2"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A5',"3"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A6',"4"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A7',"5"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A8',"6"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A9',"7"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A10',"8"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A11',"9"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A12',"10"			    ,PHPExcel_Cell_DataType::TYPE_STRING);

$objPHPExcel->getActiveSheet(1)->setCellValue('B3',"งานใหม่"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B4',"รอพนักงานเดินเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B5',"รวมเอกสารนำส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B6',"แมสรับเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B7',"เอกสารถึง HUB PIVOT"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B8',"ห้องสารบัญกลางรับเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B9',"อยู่ระหว่างนำส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B10',"ยกเลิกการจัดส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B11',"เอกสารตีกลับ"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B12',"เอกสารถึงผู้รับเรียบร้อยแล้ว"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B13',"รวม"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B14',"รวมทั้งหมด"			    ,PHPExcel_Cell_DataType::TYPE_STRING);


//branch_to_branch
$branch1_sum = $t1+$t2+$t3+$t4+$t5+$t6+$t7+$t8+$t9+$t10+$t11+$t12+$t13+$t14+$t15;
$objPHPExcel->getActiveSheet(1)->setCellValue('C3',$t7			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C4',$t1			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C5',$t8			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C6',$t9+$t2		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C7',$t14	    	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C8',$t10+$t3	  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C9',$t11+$t4  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C10',$t6    		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C11',$t15			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C12',$t5+$t12			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C13',$branch1_sum			,PHPExcel_Cell_DataType::TYPE_STRING);



//branch_to_HO
$branch2_sum = $h1+$h2+$h3+$h4+$h5+$h6+$h7+$h8+$h9+$h10+$h11+$h12+$h13+$h14+$h15;
$objPHPExcel->getActiveSheet(1)->setCellValue('D3',$h7			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D4',$h1			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D5',$h8			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D6',$h9+$h2		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D7',$h14	    	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D8',$h10+$h3	  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D9',$h11+$h4  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D10',$h6    		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D11',$h15			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D12',$h5+$h12			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D13',$branch2_sum			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D14',$branch2_sum+$branch1_sum			,PHPExcel_Cell_DataType::TYPE_STRING);




















//สั่งจากสาขากรุงเทพมหานครและปริมณฑล
//สั่งจากสาขากรุงเทพมหานครและปริมณฑล
//สั่งจากสาขากรุงเทพมหานครและปริมณฑล
//สั่งจากสาขากรุงเทพมหานครและปริมณฑล
//สั่งจากสาขากรุงเทพมหานครและปริมณฑล
//สั่งจากสาขากรุงเทพมหานครและปริมณฑล

$brabch_to_ho =array();
$h1 		  =0;
$h2 		  =0;
$h3 		  =0;
$h4 		  =0;
$h5 		  =0;
$h6 		  =0;
$h7 		  =0;
$h8 		  =0;
$h9 		  =0;
$h10 		  =0;
$h11 		  =0;
$h12 		  =0;
$h13 		  =0;
$h14 		  =0;
$h15 		  =0;

$brabch_to_bronch 	= array();
$brabch_to_ho2 	= array();
$t1 				=0;
$t2 				=0;
$t3 				=0;
$t4 				=0;
$t5 				=0;
$t6 				=0;
$t7 				=0;
$t8 				=0;
$t9 				=0;
$t10 				=0;
$t11 				=0;
$t12 				=0;
$t13 				=0;
$t14 				=0;
$t15 				=0;










if(isset($sumarya2['5'][1])){

	$brabch_to_ho = $sumarya2['5'][1];
	$h1 		  = (isset($brabch_to_ho[1])?$brabch_to_ho[1]:0);
	$h2 		  = (isset($brabch_to_ho[2])?$brabch_to_ho[2]:0);
	$h3 		  = (isset($brabch_to_ho[3])?$brabch_to_ho[3]:0);
	$h4 		  = (isset($brabch_to_ho[4])?$brabch_to_ho[4]:0);
	$h5 		  = (isset($brabch_to_ho[5])?$brabch_to_ho[5]:0);
	$h6 		  = (isset($brabch_to_ho[6])?$brabch_to_ho[6]:0);
	$h7 		  = (isset($brabch_to_ho[7])?$brabch_to_ho[7]:0);
	$h8 		  = (isset($brabch_to_ho[8])?$brabch_to_ho[8]:0);
	$h9 		  = (isset($brabch_to_ho[9])?$brabch_to_ho[9]:0);
	$h10 		  = (isset($brabch_to_ho[10])?$brabch_to_ho[10]:0);
	$h11 		  = (isset($brabch_to_ho[11])?$brabch_to_ho[11]:0);
	$h12 		  = (isset($brabch_to_ho[12])?$brabch_to_ho[12]:0);
	$h13 		  = (isset($brabch_to_ho[13])?$brabch_to_ho[13]:0);
	$h14 		  = (isset($brabch_to_ho[14])?$brabch_to_ho[14]:0);
	$h15 		  = (isset($brabch_to_ho[15])?$brabch_to_ho[15]:0);

}else{

	$h1 	= 0;
	$h2 	= 0;
	$h3 	= 0;
	$h4 	= 0;
	$h5 	= 0;
	$h6 	= 0;
	$h7 	= 0;
	$h8 	= 0;
	$h9 	= 0;
	$h10 	= 0;
	$h11 	= 0;
	$h12 	= 0;
	$h13 	= 0;
	$h14 	= 0;
	$h15 	= 0;
	
}

if(isset($sumarya2['5'][3])){

	$brabch_to_ho2 	= 	$sumarya2['5'][3];
	$h1 			+= (isset($brabch_to_ho2[1])?$brabch_to_ho2[1]:0);
	$h2 			+= (isset($brabch_to_ho2[2])?$brabch_to_ho2[2]:0);
	$h3 			+= (isset($brabch_to_ho2[3])?$brabch_to_ho2[3]:0);
	$h4 			+= (isset($brabch_to_ho2[4])?$brabch_to_ho2[4]:0);
	$h5 			+= (isset($brabch_to_ho2[5])?$brabch_to_ho2[5]:0);
	$h6 			+= (isset($brabch_to_ho2[6])?$brabch_to_ho2[6]:0);
	$h7 			+= (isset($brabch_to_ho2[7])?$brabch_to_ho2[7]:0);
	$h8 			+= (isset($brabch_to_ho2[8])?$brabch_to_ho2[8]:0);
	$h9 			+= (isset($brabch_to_ho2[9])?$brabch_to_ho2[9]:0);
	$h10 			+= (isset($brabch_to_ho2[10])?$brabch_to_ho2[10]:0);
	$h11 			+= (isset($brabch_to_ho2[11])?$brabch_to_ho2[11]:0);
	$h12 			+= (isset($brabch_to_ho2[12])?$brabch_to_ho2[12]:0);
	$h13 			+= (isset($brabch_to_ho2[13])?$brabch_to_ho2[13]:0);
	$h14 			+= (isset($brabch_to_ho2[14])?$brabch_to_ho2[14]:0);
	$h15 			+= (isset($brabch_to_ho2[15])?$brabch_to_ho2[15]:0);

}else{
	
	$h1 	+= 0;
	$h2 	+= 0;
	$h3 	+= 0;
	$h4 	+= 0;
	$h5 	+= 0;
	$h6 	+= 0;
	$h7 	+= 0;
	$h8 	+= 0;
	$h9 	+= 0;
	$h10 	+= 0;
	$h11 	+= 0;
	$h12 	+= 0;
	$h13 	+= 0;
	$h14 	+= 0;
	$h15 	+= 0;
	
}


if(isset($sumarya2['5'][2])){
	$brabch_to_bronch 	= $sumarya2['5'][2];
	$t1 				= (isset($brabch_to_bronch[1])?$brabch_to_bronch[1]:0);
	$t2 				= (isset($brabch_to_bronch[2])?$brabch_to_bronch[2]:0);
	$t3 				= (isset($brabch_to_bronch[3])?$brabch_to_bronch[3]:0);
	$t4 				= (isset($brabch_to_bronch[4])?$brabch_to_bronch[4]:0);
	$t5 				= (isset($brabch_to_bronch[5])?$brabch_to_bronch[5]:0);
	$t6 				= (isset($brabch_to_bronch[6])?$brabch_to_bronch[6]:0);
	$t7 				= (isset($brabch_to_bronch[7])?$brabch_to_bronch[7]:0);
	$t8 				= (isset($brabch_to_bronch[8])?$brabch_to_bronch[8]:0);
	$t9 				= (isset($brabch_to_bronch[9])?$brabch_to_bronch[9]:0);
	$t10 				= (isset($brabch_to_bronch[10])?$brabch_to_bronch[10]:0);
	$t11 				= (isset($brabch_to_bronch[11])?$brabch_to_bronch[11]:0);
	$t12 				= (isset($brabch_to_bronch[12])?$brabch_to_bronch[12]:0);
	$t13 				= (isset($brabch_to_bronch[13])?$brabch_to_bronch[13]:0);
	$t14 				= (isset($brabch_to_bronch[14])?$brabch_to_bronch[14]:0);
	$t15 				= (isset($brabch_to_bronch[15])?$brabch_to_bronch[15]:0);
}else{
	$t1 	= 0;
	$t2 	= 0;
	$t3 	= 0;
	$t4 	= 0;
	$t5 	= 0;
	$t6 	= 0;
	$t7 	= 0;
	$t8 	= 0;
	$t9 	= 0;
	$t10 	= 0;
	$t11 	= 0;
	$t12 	= 0;
	$t13 	= 0;
	$t14 	= 0;
	$t15 	= 0;
}
//echo "<pre>".print_r($t1,true)."</pre>";
//echo "<pre>".print_r($sumarya['br2'],true)."</pre>";
//exit;


$objPHPExcel->getActiveSheet(1)->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('J')->setWidth(15);

$objPHPExcel->getActiveSheet(1)->setCellValue('G1',"กรุงเทพมหานครและปริมณฑล"							,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G2',"NO"							,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H2',"สถานะงาน"					,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I2',"ส่งสาขา"					 ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J2',"ส่งที่สำนักงานใหญ่"			    ,PHPExcel_Cell_DataType::TYPE_STRING);

$objPHPExcel->getActiveSheet(1)->setCellValue('G3',"1"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G4',"2"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G5',"3"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G6',"4"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G7',"5"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G8',"6"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G9',"7"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G10',"8"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G11',"9"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('G12',"10"			    ,PHPExcel_Cell_DataType::TYPE_STRING);

$objPHPExcel->getActiveSheet(1)->setCellValue('H3',"งานใหม่"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H4',"รอพนักงานเดินเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H5',"รวมเอกสารนำส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H6',"แมสรับเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H7',"เอกสารถึง HUB PIVOT"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H8',"ห้องสารบัญกลางรับเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H9',"อยู่ระหว่างนำส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H10',"ยกเลิกการจัดส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H11',"เอกสารตีกลับ"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H12',"เอกสารถึงผู้รับเรียบร้อยแล้ว"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H13',"รวม"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('H14',"รวมทั้งหมด"			    ,PHPExcel_Cell_DataType::TYPE_STRING);


//branch_to_branch
$branch1_sum = $t1+$t2+$t3+$t4+$t5+$t6+$t7+$t8+$t9+$t10+$t11+$t12+$t13+$t14+$t15;
$objPHPExcel->getActiveSheet(1)->setCellValue('I3',$t7			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I4',$t1			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I5',$t8			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I6',$t9+$t2		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I7',$t14	    	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I8',$t10+$t3	  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I9',$t11+$t4  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I10',$t6    		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I11',$t15			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I12',$t5+$t12			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('I13',$branch1_sum			,PHPExcel_Cell_DataType::TYPE_STRING);



//branch_to_HO
$branch2_sum = $h1+$h2+$h3+$h4+$h5+$h6+$h7+$h8+$h9+$h10+$h11+$h12+$h13+$h14+$h15;
$objPHPExcel->getActiveSheet(1)->setCellValue('J3',$h7			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J4',$h1			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J5',$h8			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J6',$h9+$h2		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J7',$h14	    	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J8',$h10+$h3	  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J9',$h11+$h4  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J10',$h6    		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J11',$h15			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J12',$h5+$h12			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J13',$branch2_sum			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('J14',$branch2_sum+$branch1_sum			,PHPExcel_Cell_DataType::TYPE_STRING);

























$objPHPExcel->getActiveSheet(1)->setTitle('สาขาสังงาน');







if(isset($sumarya['2'][1])){

	$ho_to_ho = $sumarya['2'][1];
	$ho1 	= (isset($ho_to_ho[1])?$ho_to_ho[1]:0);
	$ho2 	= (isset($ho_to_ho[2])?$ho_to_ho[2]:0);
	$ho3 	= (isset($ho_to_ho[3])?$ho_to_ho[3]:0);
	$ho4 	= (isset($ho_to_ho[4])?$ho_to_ho[4]:0);
	$ho5 	= (isset($ho_to_ho[5])?$ho_to_ho[5]:0);
	$ho6 	= (isset($ho_to_ho[6])?$ho_to_ho[6]:0);
	$ho7 	= (isset($ho_to_ho[7])?$ho_to_ho[7]:0);
	$ho8 	= (isset($ho_to_ho[8])?$ho_to_ho[8]:0);
	$ho9 	= (isset($ho_to_ho[9])?$ho_to_ho[9]:0);
	$ho10 	= (isset($ho_to_ho[10])?$ho_to_ho[10]:0);
	$ho11 	= (isset($ho_to_ho[11])?$ho_to_ho[11]:0);
	$ho12 	= (isset($ho_to_ho[12])?$ho_to_ho[12]:0);
	$ho13 	= (isset($ho_to_ho[13])?$ho_to_ho[13]:0);
	$ho14 	= (isset($ho_to_ho[14])?$ho_to_ho[14]:0);
	$ho15 	= (isset($ho_to_ho[15])?$ho_to_ho[15]:0);

}else{

	$ho1 	= 0;
	$ho2 	= 0;
	$ho3 	= 0;
	$ho4 	= 0;
	$ho5 	= 0;
	$ho6 	= 0;
	$ho7 	= 0;
	$ho8 	= 0;
	$ho9 	= 0;
	$ho10 	= 0;
	$ho11 	= 0;
	$ho12 	= 0;
	$ho13 	= 0;
	$ho14 	= 0;
	$ho15 	= 0;
	
}

if(isset($sumarya['2'][3])){

	$ho_to_ho2 = $sumarya['2'][3];
	$ho1 	+= (isset($ho_to_ho2[1])?$ho_to_ho2[1]:0);
	$ho2 	+= (isset($ho_to_ho2[2])?$ho_to_ho2[2]:0);
	$ho3 	+= (isset($ho_to_ho2[3])?$ho_to_ho2[3]:0);
	$ho4 	+= (isset($ho_to_ho2[4])?$ho_to_ho2[4]:0);
	$ho5 	+= (isset($ho_to_ho2[5])?$ho_to_ho2[5]:0);
	$ho6 	+= (isset($ho_to_ho2[6])?$ho_to_ho2[6]:0);
	$ho7 	+= (isset($ho_to_ho2[7])?$ho_to_ho2[7]:0);
	$ho8 	+= (isset($ho_to_ho2[8])?$ho_to_ho2[8]:0);
	$ho9 	+= (isset($ho_to_ho2[9])?$ho_to_ho2[9]:0);
	$ho10 	+= (isset($ho_to_ho2[10])?$ho_to_ho2[10]:0);
	$ho11 	+= (isset($ho_to_ho2[11])?$ho_to_ho2[11]:0);
	$ho12 	+= (isset($ho_to_ho2[12])?$ho_to_ho2[12]:0);
	$ho13 	+= (isset($ho_to_ho2[13])?$ho_to_ho2[13]:0);
	$ho14 	+= (isset($ho_to_ho2[14])?$ho_to_ho2[14]:0);
	$ho15 	+= (isset($ho_to_ho2[15])?$ho_to_ho2[15]:0);

}else{
	
	$ho1 	+= 0;
	$ho2 	+= 0;
	$ho3 	+= 0;
	$ho4 	+= 0;
	$ho5 	+= 0;
	$ho6 	+= 0;
	$ho7 	+= 0;
	$ho8 	+= 0;
	$ho9 	+= 0;
	$ho10 	+= 0;
	$ho11 	+= 0;
	$ho12 	+= 0;
	$ho13 	+= 0;
	$ho14 	+= 0;
	$ho15 	+= 0;
	
}


if(isset($sumarya['2'][2])){
	$ho_to_bronch = $sumarya['2'][2];
	$th1 	= (isset($ho_to_bronch[1])?$ho_to_bronch[1]:0);
	$th2 	= (isset($ho_to_bronch[2])?$ho_to_bronch[2]:0);
	$th3 	= (isset($ho_to_bronch[3])?$ho_to_bronch[3]:0);
	$th4 	= (isset($ho_to_bronch[4])?$ho_to_bronch[4]:0);
	$th5 	= (isset($ho_to_bronch[5])?$ho_to_bronch[5]:0);
	$th6 	= (isset($ho_to_bronch[6])?$ho_to_bronch[6]:0);
	$th7 	= (isset($ho_to_bronch[7])?$ho_to_bronch[7]:0);
	$th8 	= (isset($ho_to_bronch[8])?$ho_to_bronch[8]:0);
	$th9 	= (isset($ho_to_bronch[9])?$ho_to_bronch[9]:0);
	$th10 	= (isset($ho_to_bronch[10])?$ho_to_bronch[10]:0);
	$th11 	= (isset($ho_to_bronch[11])?$ho_to_bronch[11]:0);
	$th12 	= (isset($ho_to_bronch[12])?$ho_to_bronch[12]:0);
	$th13 	= (isset($ho_to_bronch[13])?$ho_to_bronch[13]:0);
	$th14 	= (isset($ho_to_bronch[14])?$ho_to_bronch[14]:0);
	$th15 	= (isset($ho_to_bronch[15])?$ho_to_bronch[15]:0);
}else{
	$th1 	= 0;
	$th2 	= 0;
	$th3 	= 0;
	$th4 	= 0;
	$th5 	= 0;
	$th6 	= 0;
	$th7 	= 0;
	$th8 	= 0;
	$th9 	= 0;
	$th10 	= 0;
	$th11 	= 0;
	$th12 	= 0;
	$th13 	= 0;
	$th14 	= 0;
	$th15 	= 0;
}
//echo "<pre>".print_r($t1,true)."</pre>";
//echo "<pre>".print_r($sumarya,true)."</pre>";
//exit;


$objPHPExcel->createSheet(3);
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet(1)->getColumnDimension('D')->setWidth(15);

$objPHPExcel->getActiveSheet(1)->setCellValue('A1',"NO"							,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B1',"สถานะงาน"					,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C1',"ส่งสาขา"					 ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D1',"ส่งที่สำนักงานใหญ่"			    ,PHPExcel_Cell_DataType::TYPE_STRING);

$objPHPExcel->getActiveSheet(1)->setCellValue('A2',"1"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A3',"2"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A4',"3"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A5',"4"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A6',"5"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A7',"6"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A8',"7"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A9',"8"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A10',"9"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('A11',"10"			    ,PHPExcel_Cell_DataType::TYPE_STRING);

$objPHPExcel->getActiveSheet(1)->setCellValue('B2',"งานใหม่"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B3',"รอพนักงานเดินเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B4',"รวมเอกสารนำส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B5',"แมสรับเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B6',"เอกสารถึง HUB PIVOT"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B7',"ห้องสารบัญกลางรับเอกสาร"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B8',"อยู่ระหว่างนำส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B9',"ยกเลิกการจัดส่ง"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B10',"เอกสารตีกลับ"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B11',"เอกสารถึงผู้รับเรียบร้อยแล้ว"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B12',"รวม"			    ,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('B13',"รวมทั้งหมด"			    ,PHPExcel_Cell_DataType::TYPE_STRING);


//branch_to_branch
$ho1_sum = $th1+$th2+$th3+$th4+$th5+$th6+$th7+$th8+$th9+$th10+$th11+$th12+$th13+$th14+$th15;
$objPHPExcel->getActiveSheet(1)->setCellValue('C2',$th7			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C3',$th1			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C4',$th8			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C5',$th9+$th2		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C6',$th14	    	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C7',$th10+$th3	  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C8',$th11+$th4  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C9',$th6    		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C10',$th15			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C11',$th5+$th12			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('C12',$ho1_sum			,PHPExcel_Cell_DataType::TYPE_STRING);


//branch_to_HO
$ho2_sum = $ho1+$ho2+$ho3+$ho4+$ho5+$ho6+$ho7+$ho8+$ho9+$ho10+$ho11+$ho12+$ho13+$ho14+$ho15;
$objPHPExcel->getActiveSheet(1)->setCellValue('D2',$ho7			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D3',$ho1			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D4',$ho8			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D5',$ho9+$ho2		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D6',$ho14	    	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D7',$ho10+$ho3	  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D8',$ho11+$ho4  	,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D9',$ho6    		,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D10',$ho15			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D11',$ho5+$ho12			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D12',$ho2_sum			,PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet(1)->setCellValue('D13',$ho1_sum+$ho2_sum		    ,PHPExcel_Cell_DataType::TYPE_STRING);




$objPHPExcel->getActiveSheet(1)->setTitle('สำนักงานใหญ่สั่งงาน');





//echo count($data);
// echo "<pre>".print_r($sumarya,true)."</pre>";
// exit;

$nickname = "report_daily(".date('Y-m-d').')';
$filename = $nickname.'.xls';

// Rename worksheet
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet(0)->setTitle('Daily Report');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$filename.'"'); 
header('Cache-Control: max-age=0');
header('Pragma: public');
ob_end_clean();
$objWriter->save('php://output');
exit();
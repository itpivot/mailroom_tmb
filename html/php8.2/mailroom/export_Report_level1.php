<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/User.php';
require_once 'Dao/Work_inout.php';
require_once 'ajax/check_sla.php';
include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');
error_reporting(E_ALL & ~E_NOTICE);





$auth 				= new Pivot_Auth();
$req 				= new Pivot_Request();
$work_logDao 		= new Dao_Work_log();
$work_mainDao 		= new Dao_Work_main();
$userDao 			= new Dao_User();   
$work_inoutDao 		= new Dao_Work_inout();
$class_check_sla 	= new Class_check_sla();



$sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round WHERE active = 1 ORDER BY mr_delivery_round_id ASC";
$max_round_sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round 
WHERE mr_delivery_round_id in(
	SELECT max(mr_delivery_round_id)-1 
	FROM mr_delivery_round 
	group by mr_branch_type_send, mr_branch_type_resive)
and active = 1
";
$min_round_sql = "
SELECT 
	mr_delivery_round_id,
	mr_round,
	mr_round_name,
	mr_branch_type_send, 
	mr_branch_type_resive, 
	time_start, 
	time_mailroom_resive, 
	numdate_mailroom_resive, 
	time_mailroom_send, 
	numdate_mailroom_send, 
	time_sla, 
	numdate_sla, 
	active
FROM mr_delivery_round 
WHERE mr_delivery_round_id in(
	SELECT min(mr_delivery_round_id)
	FROM mr_delivery_round 
	group by mr_branch_type_send, mr_branch_type_resive)
and active = 1
";

$sql_holiday = "SELECT * FROM mr_holiday WHERE  active = 1";

$delivery_round_all = $userDao ->select($sql);
$max_round_all 		= $userDao ->select($max_round_sql);
$min_round_all 		= $userDao ->select($min_round_sql);
$holiday_all 		= $userDao ->select($sql_holiday);


$delivery_round_data = array();
foreach($delivery_round_all as $v){
	$delivery_round_data[$v['mr_branch_type_send']][$v['mr_branch_type_resive']][]= $v;
}

$max_round_data = array();
foreach($max_round_all as $v2){
	$max_round_data[$v2['mr_branch_type_send']][$v2['mr_branch_type_resive']][]= $v2;
}
$min_round_data = array();
foreach($min_round_all as $v3){
	$min_round_data[$v3['mr_branch_type_send']][$v3['mr_branch_type_resive']][]= $v3;
}

$holiday_data = array();
foreach($holiday_all as $v4){
	$holiday_data[$v4['year']][$v4['month']][]= $v4['day'];
}




function TimeDiff($strTime1,$strTime2){
	return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}
function DateTimeDiff($strDateTime1,$strDateTime2){
return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}


function check_date_start($sla_date_end){
	$day = date ("D", strtotime($sla_date_end)); 
	if($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($sla_date_end))); 
	}elseif($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($sla_date_end))); 
	}else{
		$date_end = $sla_date_end;
	}
	return $date_end;
}

function wav_sla($sla_date_end){
	$day = date ("D", strtotime($sla_date_end)); 
	if($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+2 day", strtotime($sla_date_end))); 
	}elseif($day == "Sat"){
		$date_end = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($sla_date_end))); 
	}else{
		$date_end = $sla_date_end;
	}
	return $date_end;
}




$branch_type 	= $req->get('branch_type');
$date_1 	= $req->get('date_1');
$date_2 	= $req->get('date_2');
$type 		= $req->get('type');

//echo '<pre>'.print_r($reports,true).'/<pre>';
//exit;



$array_success		= array('5','6','12','15');


if( $date_1 != ''){
	$date_1 	= date ("Y-m-d 17:00:01", strtotime("-1 day", strtotime($date_1)));
	$date_2 	= date ("Y-m-d 17:00:00", strtotime($date_2));
}


$resout = array();
$resout['date_1'] = $date_1;
$resout['date_2'] = $date_2;




$data_search['start_date']		= $date_1;
$data_search['end_date']		= $date_2; 
$data_search['type']			= $type; 
if($type == 'ho'){
	$type = 2; 
}else{
	$type = 3;
}



$data  =   $work_inoutDao->mailroom_getreport_level1($data_search);

$work_type5 = array();
$work_type2 = array();
foreach($data as $key => $val){
	$sla_num 				= 0;
	$sys_time 				= $val['main_sys_time'];
	$time_send     			= date("H:i:s",strtotime($sys_time));
	//$sys_time 				= check_date_start($sys_time);



	//echo json_encode($sys_time);
	//exit;
	if($type == 3){
			//echo $val['mr_branch_type_id'].'<br>';
			if($val['mr_branch_type_id_re'] == 2 or $val['mr_branch_type_id_re'] == 3){
				//3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
				$type_re = $val['mr_branch_type_id_re'];
			}else{
				$type_re = 1;
			}

			if($val['mr_branch_type_id'] == 3){ // 3 สาขาต่างจังหวัดสังงาน ถ้าไม่เป็นปริมนฑน
				$type_send = $val['mr_branch_type_id'];
				
			}else{
				$type_send = 2;
			}

			
			$mr_status_id 	= $val['mr_status_id'];
			if (!in_array($mr_status_id,$array_success)) {
			//if($mr_status_id != 5 and $mr_status_id != 12){				
				$sla_date_end = $class_check_sla->check_sla($type_send,$type_re,$sys_time,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);
				//$val['sla_date_end'] = $sla_date_end;
					$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$sla_date_end);
					$gg['s']=$sys_time ;
					$gg['e']=$sla_date_end ;
					$gg['d']=$delivery_round ;
					$gg['dif']=$dif_date ;
					$gg['data']=$val ;
					// echo json_encode($gg);
					 //exit;
					if($dif_date<0){
						$val['mr_status_id'] = 20;
						$tt[] = $gg;
						//echo json_encode($tt);
						//exit;
					}
	
				
			}


			
			if(isset($work_type5[$type_send][$type_re][$val['mr_status_id']] )){
				$work_type5[$type_send][$type_re][$val['mr_status_id']] += 1;
			}else{
				$work_type5[$type_send][$type_re][$val['mr_status_id']] = 1;
			}
			// if($val['mr_status_id'] == 5 or $val['mr_status_id'] == 12){
			// 	if(isset($work_type5[$type_send][$type_re][$val['mr_status_id']] )){
			// 		$work_type5[$type_send][$type_re][$val['mr_status_id']] += 1;
			// 	}else{
			// 		$work_type5[$type_send][$type_re][$val['mr_status_id']] = 1;
			// 	}
			// }else{
			// 	if(isset($work_type5[$type_send][$type_re][$val['mr_status_id']] )){
			// 		$work_type5[$type_send][$type_re][$val['mr_status_id']] += 1;
			// 	}else{
			// 		$work_type5[$type_send][$type_re][$val['mr_status_id']] = 1;
			// 	}
				
			// }

			//$a = array(2, 4, 6, 8);




	}else{

		if($val['mr_branch_type_id_re'] == 2 or $val['mr_branch_type_id_re'] == 3){ //3 สาขาจ่างจังหวัดรับงาน 2 ปริมนฑน  ที่เหลือคือสำนักงานใหญ่
			$type_re = $val['mr_branch_type_id_re'];
		}else{
			$type_re = 1;
		}

			$type_send = 1;
			$mr_status_id 	= $val['mr_status_id'];
			//if($mr_status_id != 5 and $mr_status_id != 12){	
				if (!in_array($mr_status_id,$array_success)) {
					$sla_date_end = $class_check_sla->check_sla($type_send,$type_re,$sys_time,$delivery_round_data,$max_round_data,$min_round_data,$holiday_data);

					$dif_date = DateTimeDiff(date("Y-m-d H:i:s"),$sla_date_end);
					$gg['s']=$sys_time ;
					$gg['e']=$sla_date_end ;
					$gg['d']=$delivery_round ;
					$gg['dif']=$dif_date ;
					// echo json_encode($gg);
					 //exit;
					if($dif_date<0){
						$val['mr_status_id'] = 20;
						// echo json_encode($val);
						// exit;
					}
	
				}
			
	
		if(isset($work_type2[$type_send][$type_re][$val['mr_status_id']] )){
			$work_type2[$type_send][$type_re][$val['mr_status_id']] += 1;
		}else{
			$work_type2[$type_send][$type_re][$val['mr_status_id']] = 1;
		}

	}
}


//สาขาสั่งงาน
//สาขาสั่งงาน
//สาขาสั่งงาน
//สาขาสั่งงาน
//สาขาสั่งงาน
	// echo '<pre>'.print_r($work_type2,true).'/<pre>';
	// exit;

$brabType_sender = '';
if($type == 3){

// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
// ตางจังหวัดสี่งงาน
if($branch_type == 2){
	$bh_ho 	= (isset($work_type5[2][1]))?array_sum($work_type5[2][1]):'0';
	$bh_bh 	= (isset($work_type5[2][2]))?array_sum($work_type5[2][2]):'0';
	$bh_b 		= (isset($work_type5[2][3]))?array_sum($work_type5[2][3]):'0';
   
 $send_index = 2;
 for($i=1;$i<=15;$i++){
	if($i == 1){
		$suc_b_ho 	= 0;
		$suc_b_bh 	= 0;
		$suc_b_b 	= 0;
		$pen_b_ho 	= 0;
		$pen_b_bh 	= 0;
		$pen_b_b 	= 0;
	 }

	 //if($i == 5 or $i == 6 or $i == 12 or $i == 15){
	if (in_array($i,$array_success)) {
		$suc_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		$suc_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		$suc_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
	 }else{
		$pen_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		$pen_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		$pen_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
	 }
 }
 $overSLA_b_ho 	+= (isset($work_type5[$send_index][1][20]))?$work_type5[$send_index][1][20]:'0';
 $overSLA_b_bh 	+= (isset($work_type5[$send_index][2][20]))?$work_type5[$send_index][2][20]:'0';
 $overSLA_b_b 	+= (isset($work_type5[$send_index][3][20]))?$work_type5[$send_index][3][20]:'0';


	

	$brabType_sender = 'กรงเทพปริมนฑลสั่งงาน';
	//$brabType_sender = 'ตางจังหวัดสั่งงาน';
	$reports = array();
	$reports[] = array(
		'1',
		'งานทั้งหมด',
		$bh_ho,
		$bh_bh,
		$bh_b
	);
	$reports[] = array(
		'2',
		'งานค้างในระบบ(ตาม SLA)',
		$pen_b_ho,
		$pen_b_bh,
		$pen_b_b
	);
	$reports[] = array(
		'3',
		'งานค้างในระบบ(ไม่ตาม SLA)',
		$overSLA_b_ho,
		$overSLA_b_bh,
		$overSLA_b_b 
	);
	$reports[] = array(
		'4',
		'งานสำเร็จ',
		$suc_b_ho,
		$suc_b_bh,
		$suc_b_b
	);

//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน
//กรงเทพ ปริมนฑลสั่งงาน
}else{
	
	$b_ho 	= (isset($work_type5[3][1]))?array_sum($work_type5[3][1]):'0';
	$b_bh 	= (isset($work_type5[3][2]))?array_sum($work_type5[3][2]):'0';
	$b_b 	= (isset($work_type5[3][3]))?array_sum($work_type5[3][3]):'0';
   
   
	$send_index = 3;
	for($i=1;$i<=15;$i++){
		if($i == 1){
		   $suc_b_ho 	= 0;
		   $suc_b_bh 	= 0;
		   $suc_b_b 	= 0;
		   $pen_b_ho 	= 0;
		   $pen_b_bh 	= 0;
		   $pen_b_b 	= 0;
   
   
   
		}
		
		//if($i == 5 or $i == 6 or $i == 12 or $i == 15){
	   if (in_array($i,$array_success)) {
		   $suc_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		   $suc_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		   $suc_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
		}else{
		   $pen_b_ho 	+= (isset($work_type5[$send_index][1][$i]))?$work_type5[$send_index][1][$i]:'0';
		   $pen_b_bh 	+= (isset($work_type5[$send_index][2][$i]))?$work_type5[$send_index][2][$i]:'0';
		   $pen_b_b 	+= (isset($work_type5[$send_index][3][$i]))?$work_type5[$send_index][3][$i]:'0';
		}
	}
   
	$overSLA_b_ho 	= (isset($work_type5[$send_index][1][20]))?$work_type5[$send_index][1][20]:'0';
	$overSLA_b_bh 	= (isset($work_type5[$send_index][2][20]))?$work_type5[$send_index][2][20]:'0';
	$overSLA_b_b 	= (isset($work_type5[$send_index][3][20]))?$work_type5[$send_index][3][20]:'0';
   

	
	//$brabType_sender = 'กรงเทพปริมนฑลสั่งงาน';
	$brabType_sender = 'ตางจังหวัดสั่งงาน';
	$reports = array();
	$reports[] = array(
		'1',
		'งานทั้งหมด',
		$b_ho,
		$b_bh,
		$b_b
	);
	$reports[] = array(
		'2',
		'งานค้างในระบบ(ตาม SLA)',
		$pen_b_ho,
		$pen_b_bh,
		$pen_b_b
	);
	$reports[] = array(
		'3',
		'งานค้างในระบบ(ไม่ตาม SLA)',
		$overSLA_b_ho,
		$overSLA_b_bh,
		$overSLA_b_b 
	);
	$reports[] = array(
		'4',
		'งานสำเร็จ',
		$suc_b_ho,
		$suc_b_bh,
		$suc_b_b
	);
}









 //สำนักงานใหญ่่สั่งงาน
 //สำนักงานใหญ่่สั่งงาน
 //สำนักงานใหญ่่สั่งงาน
 //สำนักงานใหญ่่สั่งงาน
}else{
	$brabType_sender = 'สำนักงานใหญ่่สั่งงาน';
	
	$ho_ho 	= (isset($work_type2[1][1]))?array_sum($work_type2[1][1]):'0';
	$ho_bh 	= (isset($work_type2[1][2]))?array_sum($work_type2[1][2]):'0';
	$ho_b 	= (isset($work_type2[1][3]))?array_sum($work_type2[1][3]):'0';
   
	$send_index = 1;
	for($i=1;$i<=15;$i++){
	   if($i == 1){
		   $suc_b_ho 	= 0;
		   $suc_b_bh 	= 0;
		   $suc_b_b 	= 0;
		   $pen_b_ho 	= 0;
		   $pen_b_bh 	= 0;
		   $pen_b_b 	= 0;
		}
   
		//if($i == 5 or $i == 6 or $i == 12 or $i == 15){
		if (in_array($i,$array_success)) {
		   $suc_b_ho 	+= (isset($work_type2[$send_index][1][$i]))?$work_type2[$send_index][1][$i]:'0';
		   $suc_b_bh 	+= (isset($work_type2[$send_index][2][$i]))?$work_type2[$send_index][2][$i]:'0';
		   $suc_b_b 	+= (isset($work_type2[$send_index][3][$i]))?$work_type2[$send_index][3][$i]:'0';
		}else{
		   $pen_b_ho 	+= (isset($work_type2[$send_index][1][$i]))?$work_type2[$send_index][1][$i]:'0';
		   $pen_b_bh 	+= (isset($work_type2[$send_index][2][$i]))?$work_type2[$send_index][2][$i]:'0';
		   $pen_b_b 	+= (isset($work_type2[$send_index][3][$i]))?$work_type2[$send_index][3][$i]:'0';
		}
	}

	$overSLA_b_ho 	= (isset($work_type2[$send_index][1][20]))?$work_type2[$send_index][1][20]:'0';
	$overSLA_b_bh 	= (isset($work_type2[$send_index][2][20]))?$work_type2[$send_index][2][20]:'0';
	$overSLA_b_b 	= (isset($work_type2[$send_index][3][20]))?$work_type2[$send_index][3][20]:'0';


	
				
				$reports = array();
				$reports[] = array(
					'1',
					'งานทั้งหมด',
					$ho_ho,
					$ho_bh,
					$ho_b
				);
				$reports[] = array(
					'2',
					'งานค้างในระบบ(ตาม SLA)',
					$pen_b_ho,
					$pen_b_bh,
					$pen_b_b
				);
				$reports[] = array(
					'3',
					'งานค้างในระบบ(ไม่ตาม SLA)',
					$overSLA_b_ho,
					$overSLA_b_bh,
					$overSLA_b_b 
				);
				$reports[] = array(
					'4',
					'งานสำเร็จ',
					$suc_b_ho,
					$suc_b_bh,
					$suc_b_b
				);
				//exit;
				//echo json_encode($resout);
}

$sheet = 'Detail';
$headers1 = array(
    '',
    '',
    'รายงานการรับส่งเอกสาร ส่งจาก  "'.$brabType_sender.'"',
    '',
    ''
);
$headers2 = array(
	'',
    'วันที่ออกรายงาน',
    date('Y-m-d H:i:s'),
    '',
    ''
);

$headers3 = array(
	'',
    'วันที่ของข้อมูล',
    $date_1.'  ถึง  '.$date_2,
    '',
    ''
);
$headers4 = array(
    'No.',
    'สถานะงาน',
    'ส่งที่สำนักงานใหญ่	',
    'ส่งที่สาขากรุงเทพมและปริมณฑล',
    'ส่งที่สาขาต่างจังหวัด'
);

$file_name = 'TMB_Report.xlsx';
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet,$headers1,$styleRow);
$writer->writeSheetRow($sheet,$headers2,$styleRow);
$writer->writeSheetRow($sheet,$headers3,$styleRow);
$writer->writeSheetRow($sheet,$headers4,$styleHead);
foreach ($reports as $v) {
    $writer->writeSheetRow($sheet,$v,$styleRow);
 }
//exit;

$writer->writeToStdOut();

exit();
//echo print_r($work_type2,true);
//echo 'end <br>';
?>

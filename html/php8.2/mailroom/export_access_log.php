<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
// require_once 'PHPExcel-1.8.1/Classes/PHPExcel.php';
require_once 'PHPExcel.php';
require_once 'Dao/Access_status_log.php';
error_reporting(E_ALL & ~E_NOTICE);



$req = new Pivot_Request();
$access_status_log_Dao = new Dao_Access_status_log();

function PrettyDate($date)
{
    if ($date) {
        $old = explode('/', $date);
        return $old[2] . '-' . $old[1] . '-' . $old[0];
    } else {
        return null;
    }

}

$param = json_decode($req->get('data'), true);

$data = array();
$data['start_date'] = PrettyDate($param['start']);
$data['end_date'] = PrettyDate($param['end']);
$data['activity'] = $param['activity'];
$data['status'] = $param['status'];


$acc = $access_status_log_Dao->fetchAccess_logs($data);

usort($acc, function ($x, $y) {
    return $y['sys_timestamp'] > $x['sys_timestamp'];
});


$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma')->setSize(10);
$objPHPExcel->getProperties()->setCreator("Pivot Co.,Ltd")->setLastModifiedBy("Pivot")->setTitle("Report Agent")->setSubject("Report")->setDescription("Report document for Excel, generated using PHP classes.");

$columnIndexs = range('A', 'H');
$columnNames[0] = array(
    0 => '#',
    1 => 'รหัส',
    2 => 'ชื่อ-สกุล',
    3 => 'Terminal',
    4 => 'กิจกรรม',
    5 => 'ผลการเข้าสู่ระบบ',
    6 => 'วันเวลา',
    7 => 'หมายเหตุ',
);

// Set column widths
$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(30);

// Header
$row = 1;
foreach ($columnNames as $key => $value) {
    for ($index = 0; $index < count($columnIndexs); $index++) {
        $leadColumns[$key][$index]['index'] = $columnIndexs[$index] . $row;
        $leadColumns[$key][$index]['name'] = $columnNames[$key][$index];
    }
    $row++;
}

foreach ($leadColumns as $head) {
    foreach ($head as $headColumn) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($headColumn['index'], $headColumn['name'])->getStyle($headColumn['index']);
        $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLACK);
        $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->getStartColor()->setARGB('FFDCDCDC');
        $objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    }
}


// Push Data

$indexs = 2;
foreach($acc as $k => $v) {
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $indexs, ($k + 1))->getStyle('A' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $indexs, strtoupper($v['mr_user_username']))->getStyle('B' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $indexs, $v['mr_emp_name']." ".$v['mr_emp_lastname'])->getStyle('C' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $indexs, $v['terminal'])->getStyle('D' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $indexs, strtoupper($v['activity']))->getStyle('E' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $indexs, strtoupper($v['status']))->getStyle('F' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $indexs, date('d/m/y H:i', strtotime($v['sys_timestamp'])))->getStyle('G' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $indexs, $v['description'])->getStyle('H' . $indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet(0)->getStyle('A'.$indexs.':H'.$indexs)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $indexs++;
}


$nickname = "Access_Logs_".date('dmY');
$filename = $nickname . '.xls';

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Access Logs');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: public');
ob_end_clean();
$objWriter->save('php://output');
exit();

?>
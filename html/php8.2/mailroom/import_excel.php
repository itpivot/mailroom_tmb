<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';

require_once 'Dao/UserRole.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Atapy_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();


$req = new Pivot_Request();

$template = Pivot_Template::factory('mailroom/import_excel.tpl');
$template->display(array(
    //'error' => print_r($errors, true),
    //'debug' => $debug,
    'select' => '0',
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));

<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Send_work.php';
error_reporting(E_ALL & ~E_NOTICE);
/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 						= new Pivot_Request();
$userDao 					= new Dao_User();
$userRoleDao 				= new Dao_UserRole();
$send_workDao 				= new Dao_Send_work();

//$users 		= $userDao 		->fetchAll();
//$userRoles 	= $userRoleDao	->fetchAll();


$barcode_full 	= "TM".date("dmy");



$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$sql_select = "SELECT * FROM mr_branch";
$sql_selecthub = "SELECT * FROM mr_hub";
$sql_selectemp = "
					SELECT e.* ,
							u.mr_user_id,
							u.mr_user_username
					FROM mr_emp  e
					left join mr_user u using(mr_emp_id)
					where e.mr_hub_id is not null";
					
$branch = $send_workDao->select($sql_select);
$hub = $send_workDao->select($sql_selecthub);
$emp = $send_workDao->select($sql_selectemp);


$template = Pivot_Template::factory('mailroom/mailroom_import_barcode_TNT.tpl');
$template->display(array(
	'debug' 		=> print_r($enp,true),
	'branch' 			=> $branch,
	'hub' 			=> $hub,
	'emp' 			=> $emp,
	//'userRoles' 	=> $userRoles,
	'success' 		=> $success,
	'barcode_full' 	=> $barcode_full,
	//'userRoles' 	=> $userRoles,
	'user_data' 	=> $user_data,
	//'users' 		=> $users,
	'role_id' 		=> $auth->getRole(),
	'roles' 		=> Dao_UserRole::getAllRoles()
));
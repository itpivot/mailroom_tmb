<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
set_time_limit(0);

error_reporting(E_ALL & ~E_WARNING );


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuserid($user_id);
$alert = '';
$data  = array();
$mr_work_main_id = $req->get('data1');


if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_main_id)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else if($mr_work_main_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{

	$sql="SELECT 
					m.*,
					hub.*,
					wb.mr_branch_floor ,
					e.mr_emp_code as send_emp_code,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					e2.mr_emp_code as re_emp_code,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					d.mr_department_code as re_mr_department_code,
					d.mr_department_name as re_mr_department_name,
					b.mr_branch_code as re_mr_branch_code,
					b2.mr_branch_code as send_mr_branch_code,
					b.mr_branch_name as re_mr_branch_name,
					b2.mr_branch_name as send_mr_branch_name
			FROM mr_work_main m
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = wb.mr_branch_id )
					Left join mr_hub hub on ( hub.mr_hub_id = b.mr_hub_id )

					
					Left join mr_branch b2 on ( b2.mr_branch_id = e.mr_branch_id )
					where m.mr_work_barcode in ('151202006010001c')
					
			";	
		


		
	
	$data 		= $send_workDao->select($sql);
	//echo '<pre>'.print_r($data,true).'</pre>';
	$dataN = array();
	foreach($data as $i => $val_i){
		$mr_branch_code = $val_i['re_mr_branch_code'];
		if($mr_branch_code == ''){
			$mr_branch_code = "000";
			$val_i['re_mr_branch_code'] = '000';
			$val_i['re_mr_branch_name'] = 'ไม่พบข้อมูลสาขา';
		}

		$dataN[$mr_branch_code][$i] = $val_i; 

	}
	$newdata=array();
	$newdatahub=array();
	$index 	= 1;
	$page = 1;
	$no=1;
	foreach($dataN as $i => $data){
		foreach($data as $i => $val_i){
			
			
			$mr_branch_code = $val_i['re_mr_branch_code'];
			if($mr_branch_code == ''){
				$mr_branch_code = "nobranch";
				$val_i['re_mr_branch_code'] = '000';
				$val_i['re_mr_branch_name'] = 'ไม่พบข้อมูลสาขา';
			}
				
				// runpage*************************
				if(!isset($newdatahub[$mr_branch_code]['no'])){
					$newdatahub[$mr_branch_code]['no'] = 1;
					$newdatahub[$mr_branch_code]['check'] = 1;
				}else{
					$newdatahub[$mr_branch_code]['no'] += 1;
					$newdatahub[$mr_branch_code]['check'] += 1;
				}
				if(!isset($newdatahub[$mr_branch_code])){
					$newdatahub[$mr_branch_code]['count']=1;
					$newdatahub[$mr_branch_code]['dd'][$page]['head']['p'] = 1;
				}else{
					if(!isset($newdatahub[$mr_branch_code]['dd'][$page])){
						$newdatahub[$mr_branch_code]['count']+=1;
						$newdatahub[$mr_branch_code]['dd'][$page]['head']['p'] = $newdatahub[$mr_branch_code]['count'];
					}
				}
				
				// end runpage*************************
			
				$val_i['no'] = $newdatahub[$mr_branch_code]['no'];
				$val_i['qty'] = 1;
				$newdatahub[$mr_branch_code]['dd'][$page]['data'][$index] = $val_i;
				$newdatahub[$mr_branch_code]['dd'][$page]['head']['d']		= $val_i;
				$newdatahub[$mr_branch_code]['dd'][$page]['head']['date']		= date('d/m/Y');
				$newdatahub[$mr_branch_code]['dd'][$page]['head']['time']		= date('H:i:s');
				$no++;
				
				// runpage*************************
				if($newdatahub[$mr_branch_code]['check'] == 20){
					$index = 1;
					$page++;
				}else{
					$index++;
				}
				// end runpage*************************
			
			
			
		}
	}
}


























$html = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ใบรับสินค้า :: IN-1708-002</title>
        <style>    
			@page {
			  margin-header: 4mm;
			  margin-footer: 4mm;
			  header: html_myHeader;
			  footer: html_myFooter;
			}

			/*@page rotated { size: landscape;}
			table.rotated { page: rotated;page-break-before: right;}

			table { rotate:90; }*/

			/*.rotated { rotate: 90; page-break-before: right;}*/

			body { margin:0 auto; padding:0; font-size:14pt; line-height:14pt; color:#000; }

			.title { font-size:28pt; font-weight: bold;}
			.docno { font-size:20pt; font-weight: bold;}
			.time { font-size: 12pt; padding-top: 6pt;}

			.form_data { padding: 2pt;}

			.form_tbl td {  padding:0pt 2pt;}
			.tr_header td { border-top: 1pt solid #000; border-bottom: 1pt solid #000; border-collapse: collapse; padding:0pt 2pt; font-weight: bold;}
			.tr_footer td { border-bottom: 1pt solid #000;}
			.tr_total td { font-weight: bold; padding-top: 2pt; }

        </style>    
    </head>
    <body>
        
        <htmlpageheader name="myHeader">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top" class="title">ใบรับสินค้า</td>
                    <td align="right" valign="bottom" class="docno">IN-1708-002</td>
                </tr>  
            </table>
            <table cellpadding="0" cellspacing="0" class="form_data">
                <tr>
                    <td align="left" valign="top" width="60">วันที่:</td>
                    <td align="left" valign="top" width="150">04/08/2017</td>
                    <td align="left" valign="top" width="40">สาขา:</td>
                    <td align="left" valign="top" width="100">รัตนา PD</td>
                    <td align="left" valign="top" width="70">เลขที่อ้างอิง:</td>
                    <td align="left" valign="top">sdd5</td>
                </tr>
                <tr>
                    <td align="left" valign="top">หมายเหตุ:</td>
                    <td align="left" valign="top" colspan="3">add 2</td>
                    <td align="left" valign="top">ชื่อผู้ผลิต:</td>
                    <td align="left" valign="top">ABC&#039;id Co</td>
                </tr>
            </table>    
        </htmlpageheader>

        <htmlpagefooter name="myFooter">
		<table border="0" width="100%"   class="center">
		<tr>
			<td align="center">
			<p>(ลงชื่อ)...........................................ผู้ออกใบคุมงาน</p>
			<p>(..............................................)</p>
			<p>วันที่................................</p>
			</td>
			<td align="center">
			<p>(ลงชื่อ)...........................................ผู้เดินเอกสาร</p>
			<p>(..............................................)</p>
			<p>วันที่................................</p>
			</td>
		</tr>

		<tr>
			<td align="center">
			<p>(ลงชื่อ)...........................................ผู้ตรวจสอบงาน</p>
			<p>(..............................................)</p>
			<p>วันที่................................</p>
			</td>
			<td align="center">
			<p>(ลงชื่อ)...........................................ผู้รับงาน/รหัสพนักงาน</p>
			<p>(..............................................)</p>
			<p>วันที่................................</p>
			</td>
		</tr>
	</table>
        </htmlpagefooter>

        <table cellpadding="2" cellspacing="0" class="form_tbl rotated" width="100%">
            <thead>
                <tr class="tr_header">
                    <td align="center" valign="middle">ที่</td>
                    <td align="left" valign="middle" width="90">รหัส</td>
                    <td align="left" valign="middle" width="120">ชื่อสินค้า</td>
                    <td align="left" valign="middle" width="60">นน.มาตร</td>
                    <td align="right" valign="middle" width="60">จำนวน</td>
                    <td align="right" valign="middle" width="80">นน.รวม</td>
                    <td align="right" valign="middle" width="80">นน.จริง</td>
                </tr>
            </thead>
            <tfoot>
                <tr class="tr_footer">
                    <td align="center" valign="middle" height="1"></td>
                    <td align="left" valign="middle"></td>
                    <td align="left" valign="middle"></td>
                    <td align="left" valign="middle"></td>
                    <td align="right" valign="middle"></td>
                    <td align="right" valign="middle"></td>
                    <td align="right" valign="middle"></td>
                </tr>
            </tfoot>
            <tbody>
            
            <tr>
                <td valign="top" align="center">1</td>
                <td valign="top" align="left">K05</span></td>
                <td valign="top" align="left">ค 1 บ</td>
                <td valign="top" align="left">1 บ</td>
                <td valign="top" align="right">10</td>
                <td valign="top" align="right">152.00</td>
                <td valign="top" align="right">152.50</td>
            </tr>
            
            <tr>
                <td valign="top" align="center">2</td>
                <td valign="top" align="left">K06</span></td>
                <td valign="top" align="left">ค 2 บ</td>
                <td valign="top" align="left">2 บ</td>
                <td valign="top" align="right">10</td>
                <td valign="top" align="right">304.00</td>
                <td valign="top" align="right">304.20</td>
            </tr>
            
            <tr>
                <td valign="top" align="center">3</td>
                <td valign="top" align="left">K07</span></td>
                <td valign="top" align="left">ค 3 บ</td>
                <td valign="top" align="left">3 บ</td>
                <td valign="top" align="right">10</td>
                <td valign="top" align="right">456.00</td>
                <td valign="top" align="right">456.26</td>
            </tr>
            
            <tr>
                <td valign="top" align="center">4</td>
                <td valign="top" align="left">M05</span></td>
                <td valign="top" align="left">ม 1 บ</td>
                <td valign="top" align="left">1 บ</td>
                <td valign="top" align="right">15</td>
                <td valign="top" align="right">228.00</td>
                <td valign="top" align="right">228.45</td>
            </tr>
            
            <tr>
                <td valign="top" align="center">5</td>
                <td valign="top" align="left">M06</span></td>
                <td valign="top" align="left">ม 2 บ</td>
                <td valign="top" align="left">2 บ</td>
                <td valign="top" align="right">15</td>
                <td valign="top" align="right">456.00</td>
                <td valign="top" align="right">456.20</td>
            </tr>
            
            <tr>
                <td valign="top" align="center">6</td>
                <td valign="top" align="left">M07</span></td>
                <td valign="top" align="left">ม 3 บ</td>
                <td valign="top" align="left">3 บ</td>
                <td valign="top" align="right">15</td>
                <td valign="top" align="right">684.00</td>
                <td valign="top" align="right">683.98</td>
            </tr>
            
            <tr>
                <td valign="top" align="center">7</td>
                <td valign="top" align="left">M08</span></td>
                <td valign="top" align="left">ม 4 บ</td>
                <td valign="top" align="left">4 บ</td>
                <td valign="top" align="right">5</td>
                <td valign="top" align="right">304.00</td>
                <td valign="top" align="right">304.00</td>
            </tr>
            
            </tbody>
        </table>
        <table cellpadding="2" cellspacing="0" class="form_tbl rotated" width="100%">
            <tr class="tr_total">
                <td valign="top" align="center">&nbsp;</td>
                <td valign="top" align="left" width="90">&nbsp;</span></td>
                <td valign="top" align="left" width="120">&nbsp;</td>
                <td valign="top" align="left" width="60">รวม</td>
                <td valign="top" align="right" width="60">80</td>
                <td valign="top" align="right" width="80">2,584.00</td>
                <td valign="top" align="right" width="80">2,585.59</td>
            </tr>
        </table>    

    </body>
</html>
';

require_once 'ThaiPDF/thaipdf.php';
require_once('ThaiPDF/__RES__/mpdf.php');

$mpdf = new mPDF('th', 'A5', 0, 'angsa', 4, 4, 35, 30);

//$mpdf->forcePortraitHeaders = true;

$mpdf->AliasNbPages('[pagetotal]');
$mpdf->SetDisplayMode('fullpage');


//$mpdf->AddPage('L');
$mpdf->WriteHTML($html);

$mpdf->Output();

exit;


//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//exit;
$template = Pivot_Template::factory('mailroom/print_sort_branch_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';

error_reporting(E_ALL & ~E_WARNING );


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuserid($user_id);
$alert = '';
$data  = array();
$mr_work_main_id 	= $req->get('data_option');
$round_name 		= $req->get('round_name');
$date 				= $req->get('date');

	// echo '<pre>'.print_r($mr_work_main_id,true).'</pre>';
	// echo '<pre>'.print_r($round_name,true).'</pre>';
	// exit;


if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_main_id)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else if($mr_work_main_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{

	$sql="SELECT 
					m.*,
					count(rw.mr_round_resive_work_id) as count,
					hub.*,
					wb.mr_branch_floor ,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					d.mr_department_code as re_mr_department_code,
					d.mr_department_name as re_mr_department_name,
					b.mr_branch_code as re_mr_branch_code,
					b2.mr_branch_code as send_mr_branch_code,
					b.mr_branch_name as re_mr_branch_name,
					b2.mr_branch_name as send_mr_branch_name
			FROM mr_round_resive_work rw
			LEFT JOIN mr_work_main m ON ( m.mr_work_main_id = rw.mr_work_main_id )
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = wb.mr_branch_id )
					Left join mr_hub hub on ( hub.mr_hub_id = b.mr_hub_id )
					
					Left join mr_branch b2 on ( b2.mr_branch_id = e.mr_branch_id )
					where m.mr_work_main_id in (".$mr_work_main_id.")
					and rw.date_send BETWEEN '".$date."' AND '".$date."'
					group by wb.mr_branch_id
					order by b.mr_branch_code ,m.mr_work_barcode asc




					
			";	
		


		
	
	$data 		= $send_workDao->select($sql);
	$dataN= array();
	foreach($data as $i => $val_i){
		$mr_hub_id = $val_i['mr_hub_id'];
		if($mr_hub_id == '' or $mr_hub_id == null){
			$mr_hub_id = "000";
			$val_i['mr_hub_id'] = '000';
			$val_i['mr_hub_name'] = 'ไม่พบข้อมูล Hub';
		}
		
		$dataN[$mr_hub_id][$i] = $val_i;
	}
//	echo '<pre>'.print_r($dataN,true).'</pre>';
//	exit;
	$newdata=array();
	$newdatahub=array();
	
	$page = 1;
	$no=1;
foreach($dataN as $i => $data){
	$index 	= 1;
	foreach($data as $i => $val_i){


		$mr_hub_id = ($val_i['mr_hub_id']=='')?'xx99':$val_i['mr_hub_id'];
		if($mr_hub_id == ''){
			$mr_hub_id = "nobranch";
			$val_i['mr_hub_id'] = '000';
			$val_i['mr_hub_name'] = 'ไม่พบข้อมูล Hub';
		}
		if($val_i['re_mr_branch_code'] == ''){
			$val_i['re_mr_branch_code'] = '---';
			$val_i['re_mr_branch_name'] = 'ไม่พบข้อมูล';
		}
			
			// runpage*************************
			if(!isset($newdatahub[$mr_hub_id]['no'])){
				$newdatahub[$mr_hub_id]['no'] = 1;
				$newdatahub[$mr_hub_id]['count']=1;
			}else{
				$newdatahub[$mr_hub_id]['no'] += 1;
				if(!isset($newdatahub[$mr_hub_id]['dd'][$page])){
					if(empty($newdatahub[$mr_hub_id]['count'])){
					$newdatahub[$mr_hub_id]['count']=0;
					}
					$newdatahub[$mr_hub_id]['count']+=1;
				}
			}
			if(!isset($newdatahub[$mr_hub_id])){
				$newdatahub[$mr_hub_id]['count']=1;
				$newdatahub[$mr_hub_id]['dd'][$page]['head']['p'] = 1;
			}else{
				if(!isset($newdatahub[$mr_hub_id]['dd'][$page])){
					$newdatahub[$mr_hub_id]['dd'][$page]['head']['p'] = $newdatahub[$mr_hub_id]['count'];
				}
			}

			
			// end runpage*************************
			
			$val_i['no'] 												= $newdatahub[$mr_hub_id]['no'];
			$val_i['qty'] 												= 1;
			$newdatahub[$mr_hub_id]['dd'][$page]['data'][$index] 		= $val_i;
			$newdatahub[$mr_hub_id]['dd'][$page]['head']['d']			= $val_i;
			$newdatahub[$mr_hub_id]['dd'][$page]['head']['date']		= date('d/m/Y');
			$newdatahub[$mr_hub_id]['dd'][$page]['head']['time']		= date('H:i:s');
			if(!isset($newdatahub[$mr_hub_id]['trfooter'])){
				$newdatahub[$mr_hub_id]['trfooter'] = 0;  
			}
			$newdatahub[$mr_hub_id]['trfooter'] += $val_i['count'];
			$no++;
			
			// runpage*************************
			if($index == 30){
				$index = 1;
				$page++;
			}else{
				$index++;
			}
			// end runpage*************************
		
		
		
	}
}
}
//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//echo '<pre>'.print_r($data,true).'</pre>';
//exit;




$txt_html='
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Times New Roman", Times, serif;
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 16px
 }
 tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 12px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }


	@page {
		size: A4;
		margin: 0;
	  }
  
	  @media print {
	  #print_p{
			display:none;
		  }
		html,
		body {
		  width: 210mm;
		  height: 297mm;
		   font-family: "Times New Roman", Times, serif;
		}
		.page {
			width: 210mm;
			height: 297mm;
			min-height: 297mm;
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: unset;
		  }
	  }

	  
  </style>
</head>

<body>
<div class="book">
  ';
foreach( $newdatahub as $i => $pa){
foreach( $pa['dd'] as $dt){
  $txt_html.='

  <div class="page">
     <div class="subpage">
	 <table width="100%">
						<tr>
							<td align="center">
								<div class="mb-3 text-center">
								<h5 class="">ใบคุมการนำส่งเอกสาร ประจำธนาคารทหารไทยธนชาต จำกัด(มหาชน)</h5>		
								<h6 class="">'.$dt['head']['d']['mr_hub_name'].' ประจำวันที่  '.$dt['head']['date'].'</h6>				
								</div>
							</td>
						</tr>
					</table>
		<table width="100%">
			<tr>
				<td width="20%"></td>
				<td valign="top" align="center">
				
				</td>
				<td width="30%">
					<table width="100%">
						<tr>
							<td colspan="2" align="right">หน้า  :&nbsp;&nbsp;&nbsp;<b><u>'.$dt['head']['p'].'/'.$pa['count'].'&nbsp;&nbsp;&nbsp;</u></b></td>
						</tr>
						<tr>
							<td colspan="2" align="right"><p class="card-text">วันที่พิมพ์  <b><u>  '.$dt['head']['date'].' '.$dt['head']['time'].'</u></b></p></td>
						</tr>					  
						<tr>
						<td><p class="card-text"></td>
						<td align="right"><p class="card-text">รอบ  <b><u>&nbsp;&nbsp;&nbsp;'.$round_name.'&nbsp;&nbsp;&nbsp;</u></b> </p></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>


        <div class="table-responsive" style="height:880px;">
          <table   border="1" class="table table-sm">
            	<thead class="thead-light">
					<tr>
						<th>ลำดับ</th>
						<th>รหัสสาขา</th>
						<th>สาขา</th>
						<th>จำนวนซอง</th>
						<th>หมายเหตุ/ผู้รับ</th>
					</tr>
            	</thead>
            	<tbody>';
				foreach( $dt['data'] as $subd){
						$txt_html.='
							<tr>
								<td>'.$subd['no'].'</td>';
								
									$txt_html.='
									<td class="text-center">'.$subd['re_mr_branch_code'].'</td>
									<td>'.$subd['re_mr_branch_name'];
									if($subd['mr_branch_floor'] != ""){
										$txt_html.='/ ชั้น  '.$subd['mr_branch_floor'];
									}
									$txt_html.='</td>';
								
						$txt_html.='
								<td class="text-center" > '.$subd['count'].'</td>
								<td></td>
							</tr>';
					
			}
			if($dt['head']['p'] == $pa['count']){
				$txt_html.=' 
				<tr>
					<th></th>
					<th></th>
					<th class="text-right"><u>รวมจำนวนซองทั้งสิ้น</u></th>
					<th class="text-center" ><u>'.$pa['trfooter'].'</u></th>
					<th></th>
				</tr>';
				
			}
			$txt_html.=' 
            </tbody>
          </table>
        </div>
		<center>
			<table border="0" width="100%"   class="center">
			<tr>
				<td align="center">
				<p>(ลงชื่อ)...........................................พนักงานขับรถ</p>
				<p>(..............................................)</p>
				<p>วันที่................................</p>
				</td>
				<td align="center">
				<p>(ลงชื่อ)...........................................หัวหน้าศูนย์</p>
				<p>(..............................................)</p>
				<p>วันที่................................</p>
				</td>
			</tr>
		</table>
		</center>

    </div>
</div>

';
}
}
$txt_html.='		
</div>
</body>
</html>';





// echo '<pre>'.print_r($newdata,true).'</pre>';
//  echo $txt_html;
//  exit;
function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}


require_once 'ThaiPDF/thaipdf.php';
$left=1;
$right=1;
$top=1;
$bottom=1;
$header=0;
$footer=1;
$filename='file.pdf';
pdf_margin($left,$right,$top, $bottom,$header,$footer);
pdf_html($txt_html);
pdf_orientation('P');        
pdf_echo();

exit;
$template = Pivot_Template::factory('mailroom/print_hab_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
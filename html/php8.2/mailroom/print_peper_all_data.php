<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
set_time_limit(0);

error_reporting(E_ALL & ~E_Notice );


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getEmpDataByuserid($user_id);
$alert 			= '';

$data  			= array();
$mr_work_main_id 	= $req->get('data_option');
$date 				= $req->get('date');
$states 				= $req->get('states');
$date_report 				= $req->get('date');


if(!empty($states)){
	$states_id = implode(',',$states);
	$sql_mr_round_name = "SELECT GROUP_CONCAT(`mr_round_name`) as mr_round_name FROM `mr_round` WHERE `mr_round_id` IN (".$states_id.")";
	$round_data 		= $send_workDao->select($sql_mr_round_name);
	if(!empty($round_data)){
		$round_name  = $round_data[count($round_data)-1]['mr_round_name'];
	}
}


// echo '<pre>'.print_r($round_name,true).'</pre>';

//  exit;
if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_main_id)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else if($mr_work_main_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{
	$sql="
				SELECT
					m.*,
					count(rw.mr_round_resive_work_id) as quty,
					count(m.mr_work_main_id) as num_doc,
					rw.mr_round_resive_work_id,
					rw.date_send,
						m.mr_work_main_id,
						m.sys_timestamp,
						m.mr_work_barcode,
						tnt.mr_round as send_round,
						e.mr_emp_code as send_code,
						e.mr_emp_name as send_name,
						e.mr_emp_lastname  as send_lname,
						b.mr_branch_code as send_mr_branch_code,
						b.mr_branch_name as send_mr_branch_name,
						bre.mr_branch_code as re_mr_branch_code,
						bre.mr_branch_name as re_mr_branch_name,
						d.mr_department_code as re_mr_department_code,
						d.mr_department_name as re_mr_department_name,
						e2.mr_emp_id as re_emp_id,
						e2.mr_emp_code as re_code,
						e2.mr_emp_name as re_name,
						e2.mr_emp_lastname  as re_lname,
						tnt.tnt_tracking_no,
						tw.mr_type_work_name
					FROM mr_round_resive_work rw
					LEFT JOIN mr_work_main m  ON ( m.mr_work_main_id = rw.mr_work_main_id )
					LEFT JOIN mr_status st ON ( st.mr_status_id = m.mr_status_id )
					LEFT JOIN mr_type_work tw ON ( tw.mr_type_work_id = m.mr_type_work_id )
					LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = m.mr_branch_id )
					Left join mr_branch bre on ( bre.mr_branch_id = wb.mr_branch_id )
					Left join mr_send_work sw on ( sw.mr_send_work_id = m.mr_send_work_id )
					Left join mr_round_send_work tnt on ( tnt.mr_work_main_id = m.mr_work_main_id )
					where m.mr_work_main_id in (".$mr_work_main_id.")
					and rw.date_send like'".$date."%'
					group by m.mr_work_barcode
					order by b.mr_branch_code asc
					
			";	
		


		
	
	$data 		= $send_workDao->select($sql);
// echo "<pre>".print_r($date,true)."</pre>";
// echo "<pre>".print_r($data,true)."</pre>";
// exit;

	$datasorce 		= array();
	$arr_sum_price 	= array();
	$arr_total_price 	= array();
	$che_page = 0;
	foreach($data as $j => $data_j){
		$mr_send_emp_id 										= $data_j['re_emp_id'];
		if(!isset($datasorce[$mr_send_emp_id])){
			$che_page += 2;
		}else{
			$che_page += 1;
			
		}
		$datasorce[$mr_send_emp_id][$j] 								=  $data_j;

		if(isset($arr_total_price['s_quty'])){
			$arr_total_price['s_quty'] 						+= $data_j['quty'];	
		}else{
			$arr_total_price['s_quty'] 						= $data_j['quty'];	
		}
		if(isset($arr_sum_price[$mr_send_emp_id]['s_quty'])){
			$arr_sum_price[$mr_send_emp_id]['s_quty'] 						+= $data_j['quty'];	
		}else{
			$arr_sum_price[$mr_send_emp_id]['s_quty'] 						= $data_j['quty'];		

		}
	}
	
//$sumcount = array_sum ( $  ) ;
//$price = array_sum ( $arr_price ) ;
//$sum_price = array_sum ( $arr_sum_price ) ;
//echo "<pre>".print_r($datasorce,true)."</pre>";
//exit;
//echo $date_report;


	$newdata=array();
	$page = 1;
	$index 						= 1;
	$ll=0;
	foreach($datasorce as $m => $data_k){
		$no=1;
	
		
			$count  				= $arr_sum_price[$m]['s_quty'];
			foreach($data_k as $l 	=> $data_){
				$data_['no'] 							= $no;
				$data_['total'] 						= 0;
				$data_['count'] 						= $count;
				$data_['pr'] 							= $arr_sum_price[$m][$k]['s_mr_post_price'];
				$data_['sumpr'] 						= $arr_sum_price[$m][$k]['s_mr_post_totalprice'];

				$newdata['data'][$page]['data'][$m.$l] 	=  $data_;
				$newdata['data'][$page]['head']['data'] =  $data_;
				$newdata['allpage'] =  $page;
				$newdata['data'][$page]['head']['page'] =  $page;
				
				//$newdata['allpage'] =  $count_page;
				$no++;
				$count_page = ($che_page/35);
				if($page >= $count_page){
					$setpage = 30;
				}else{
					$setpage = 35;
				}
				
				if($index >= $setpage){
					if($count>0){
						$index =2;
					}else{
						$index =1;
					}
					$page++;
				}else{
					if($count>0){
						$index +=2;
					}else{
						$index +=1;
					}
					
				}
				$count = 0;
			}
		
		
			$ll++;
		
	}
		$total  					            = $arr_total_price['s_quty'];
		$data_['no'] 							= $no;
		$data_['total'] 						= $total;
		$data_['total_pr'] 						= $arr_total_price[$m]['s_mr_post_price'];
		$data_['total_sumpr'] 					= $arr_total_price[$m]['s_mr_post_totalprice'];

		$newdata['data'][$page]['data'][$l+1] 	=  $data_;
		$newdata['data'][$page]['head']['data'] =  $data_;
		$newdata['data'][$page]['head']['page'] =  $page;
		$newdata['allpage'] 					=  $page;
		$page++;
		$total = 0;
		
}





//echo "<pre>".print_r($newdata,true)."</pre>";
// echo $date_report;
// // echo ">>".$round_print;
//exit;















$txt_html='
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Times New Roman", Times, serif;
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 16px
 }
 tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 12px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

    @media print {
	#print_p{
		  display:none;
		}
      html,
      body {
        width: 210mm;
        height: 297mm;
		 font-family: "Times New Roman", Times, serif;
		 font-size:12px;
      }
      .page {
		width: 210mm;
		height: 297mm;
		min-height: 297mm;
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: unset;
      }
    }
  </style>
</head>

<body>
  <div class="book">
   
   ';
   $total = array();
foreach( $newdata['data'] as $i => $page){
//echo"kkkk<br>";
$txt_html.='  <div class="page">
      <div class="subpage ">
        <div class="header mb-3"></div>
		<table width="100%">
			<tr>
				<td width="70%" align="center">
					<h5 class="">สารบรรณกลางและโลจิสติกส์  บริหารทรัพยากรอาคาร</h5>
					<h6 class="">ใบนำส่ง-ตอบรับเอกสาร</h6>
				</td>
				
				<td  align="right">
					หน้า  :&nbsp;&nbsp;&nbsp;<b><u>'.$page['head']['page'].'/'.$newdata['allpage'].'&nbsp;&nbsp;&nbsp;</u></b><br>
					วันที่  <b><u> '.$date_report.' รอบ '.$round_name.'</u></b></p>
					<p class="card-text">เวลาพิมพ์  <b><u>'.date('Y-m-d').'   &nbsp;&nbsp;&nbsp;'.date('H:i:s').'&nbsp;&nbsp;&nbsp;</u></b> </p>
				</td>
				
            </tr>
            <tr>
				<td colspan="2" align="left">
				ใบนำส่ง-ตอบรับเอกสาร
				</td>
            </tr>
		 </table>
        <div class="table-responsive" style="height:700px;">
          <table   class="table table-sm">
            <thead class="thead-light">
			  <tr>
			 	<th width="75%" class="text-center">ผู้รับ / ผู้ส่ง</th>
                <th>Barcode</th>
				 <th>จำนวนซอง</th>
              </tr>
            </thead>';
           $txt_html.='<tbody>';
		   
		   foreach( $page['data'] as $subd){
			   //echo "<pre>".print_r($subd,true)."</pre>";
				//foreach( $emp as $subd){
					
					
						if($subd['total']>0){
							$total = $subd;
							//echo "1 <br>";
							// $txt_html.=' 
							// <tr>
							// 	<th class="text-left" colspan="1">รวมทั้งหมด  		</th>
							// 	<th class="text-center" colspan="0">'.$subd['total_pr'].'</th>
							// 	<th class="text-center" colspan="0">'.$subd['total'].'</th>
							// 	<th class="text-center" colspan="0">'.$subd['total_sumpr'].'</th>
							// </tr>';
						}else{
							//echo "0 <br>";
							if($subd['count']!=0){
								$txt_html.=' 
								<tr>
									<th class="text-left" colspan="2">'.$subd['re_code'].'   '.$subd['re_name'].' 	'.$subd['re_lname'].'   '.$subd['re_mr_branch_code'].'	'.$subd['re_mr_branch_name'].'  	'.$subd[' re_mr_department_code'].'    	'.$subd['re_mr_department_name'].'</th>
									<th class="text-center" colspan="0">'.$subd['count'].'</th>
								</tr>';
								$txt_html.=' 
								<tr>
									<td class="text-right">'.$subd['send_code'].'   '.$subd['send_name'].' 	'.$subd['send_lname'].'   '.$subd['send_mr_branch_code'].'	'.$subd['send_mr_branch_name'].'   	'.$subd['send_mr_department_code'].'    	'.$subd['send_mr_department_name'].'  </td>
									<td class="text-center">'.$subd['mr_work_barcode'].'</td>
									<td class="text-center">'.$subd['quty'].'</td>
								</tr>';

							}else{
								$txt_html.=' 
								<tr>
								<td class="text-right">'.$subd['send_code'].'   '.$subd['send_name'].' 	'.$subd['send_lname'].'   '.$subd['send_mr_branch_code'].'	'.$subd['send_mr_branch_name'].'   	'.$subd['send_mr_department_code'].'    	'.$subd['send_mr_department_name'].' </td>
									<td class="text-center">'.$subd['mr_work_barcode'].'</td>
									<td class="text-center">'.$subd['quty'].'</td>
								</tr>';

							}
						}
						
					
				//}
			}
			if(!empty($total)){
				if($total['total']>0){
				
					//echo "1 <br>";
					$txt_html.=' 
					<tr>
						<th class="text-left" colspan="1">รวมทั้งหมด  		</th>
						<th class="text-center" colspan="0">'.$total['total_pr'].'</th>
						<th class="text-center" colspan="0">'.$total['total'].'</th>
						<th class="text-center" colspan="0">'.$total['total_sumpr'].'</th>
					</tr>';
				}
			}
			//exit;
        $txt_html.='</tbody>';
	$txt_html.='</table>
	</div>';
	if($total['total']>0){
		$txt_html.='';
		$txt_html.='
			<br>
			<br>
			<br>';
			$txt_html.='<center>
							<table border="0" width="100%"   class="center">
							<tr>
								<td align="center">
								<p>(ลงชื่อ)...........................................ผู้ออกใบคุมงาน</p>
								<p>(..............................................)</p>
								<p>วันที่................................</p>
								</td>
								<td align="center">
								<p>(ลงชื่อ)...........................................ผู้เดินเอกสาร</p>
								<p>(..............................................)</p>
								<p>วันที่................................</p>
								</td>
							</tr>
							<tr>
								<td align="center">
								<p>(ลงชื่อ)...........................................ผู้ตรวจสอบงาน</p>
								<p>(..............................................)</p>
								<p>วันที่................................</p>
								</td>
								<td align="center">
								<p>(ลงชื่อ)...........................................ผู้รับงาน/รหัสพนักงาน</p>
								<p>(..............................................)</p>
								<p>วันที่................................</p>
								</td>
							</tr>
						</table>
					</center>';
	}
	
$txt_html.='	
    </div>
</div>';
}
		

$txt_html.='  
	</body>
</html>';




 //echo '<pre>'.print_r($newdata,true).'</pre>';
// echo $txt_html;
// exit;
function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}


require_once 'ThaiPDF/thaipdf.php';
$left=3;
$right=3;
$top=5;
$bottom=5;
$header=0;
$footer=5;
$filename='file.pdf';
pdf_margin($left,$right,$top, $bottom,$header,$footer);
pdf_html($txt_html);
pdf_orientation('P');        
pdf_echo();

exit;
//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//exit;
$template = Pivot_Template::factory('mailroom/print_sort_branch_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Send_work.php';
set_time_limit(0);

error_reporting(E_ALL & ~E_WARNING );


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_mainDao 	= new Dao_Work_main();
$send_workDao 	= new Dao_Send_work();
//$users 			= $userDao->fetchAll();
//$userRoles 		= $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getEmpDataByuserid($user_id);
$alert = '';
$data  = array();
$mr_work_main_id 	= $req->get('data_option');
$round_name 		= $req->get('round_name');
$date 				= $req->get('date');


// echo $round_name  ;
// exit;

if(preg_match('/<\/?[^>]+(>|$)/', $mr_work_main_id)) {
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else if($mr_work_main_id == ''){
	$alert = "
	$.confirm({
    title: 'Alert!',
    content: 'เกิดข้อผิดพลาด!',
    buttons: {
        OK: function () {
             location.href = 'mailroom_sendWork.php';
			}
		}
	});
		
	";
}else{

	$sql="SELECT 
					m.quty,
					m.mr_work_barcode,
					m.mr_topic,
					hub.*,
					wb.mr_branch_floor ,
					e.mr_emp_code as send_emp_code,
					e.mr_emp_name as send_name,
					e.mr_emp_lastname  as send_lname,
					e2.mr_emp_code as re_emp_code,
					e2.mr_emp_name as re_name,
					e2.mr_emp_lastname  as re_lname,
					d.mr_department_code as re_mr_department_code,
					d.mr_department_name as re_mr_department_name,
					b.mr_branch_code as re_mr_branch_code,
					b2.mr_branch_code as send_mr_branch_code,
					b.mr_branch_name as re_mr_branch_name,
					b2.mr_branch_name as send_mr_branch_name

			FROM mr_round_resive_work rw
			LEFT JOIN mr_work_main m  ON ( m.mr_work_main_id = rw.mr_work_main_id )
			LEFT JOIN mr_work_inout wb ON ( wb.mr_work_main_id = m.mr_work_main_id )
					LEFT JOIN mr_user u ON ( u.mr_user_id = m.mr_user_id )
					left JOIN mr_emp e ON ( e.mr_emp_id = u.mr_emp_id )
					LEFT JOIN mr_emp e2 ON ( e2.mr_emp_id = wb.mr_emp_id )
					Left join mr_department d on ( d.mr_department_id = e2.mr_department_id )
					Left join mr_branch b on ( b.mr_branch_id = wb.mr_branch_id )
					Left join mr_hub hub on ( hub.mr_hub_id = b.mr_hub_id )

					
					Left join mr_branch b2 on ( b2.mr_branch_id = e.mr_branch_id )
					where m.mr_work_main_id in (".$mr_work_main_id.")
					and rw.date_send BETWEEN '".$date."' AND '".$date."'
					group by rw.mr_round_resive_work_id
					order by  b.mr_branch_code ,m.mr_work_barcode asc
					
				
					
			";	
		


		
	
	$data 		= $send_workDao->select($sql);
	//echo '<pre>'.print_r($data,true).'</pre>';
	$dataN = array();
	foreach($data as $i => $val_i){
		$mr_branch_code = $val_i['re_mr_branch_code'];
		if($mr_branch_code == ''){
			$mr_branch_code = "000";
			$val_i['re_mr_branch_code'] = '000';
			$val_i['re_mr_branch_name'] = 'ไม่พบข้อมูลสาขา';
		}

		$dataN[$mr_branch_code][$i] = $val_i; 

	}
	$newdata=array();
	$newdatahub=array();
	
	$page = 1;
	$no=1;
	foreach($dataN as $i => $data){
		$index 	= 1;
		foreach($data as $i => $val_i){
			
			
			$mr_branch_code = $val_i['re_mr_branch_code'];
			if($mr_branch_code == ''){
				$mr_branch_code = "nobranch";
				$val_i['re_mr_branch_code'] = '000';
				$val_i['re_mr_branch_name'] = 'ไม่พบข้อมูลสาขา';
			}
				
				// runpage*************************
				if(!isset($newdatahub[$mr_branch_code]['no'])){
					$newdatahub[$mr_branch_code]['no'] = 1;
					$newdatahub[$mr_branch_code]['check'] = 1;
					$newdatahub[$mr_branch_code]['count']=1;
				}else{
					$newdatahub[$mr_branch_code]['no'] += 1;
					$newdatahub[$mr_branch_code]['check'] += 1;
					if(!isset($newdatahub[$mr_branch_code]['dd'][$page])){
						$newdatahub[$mr_branch_code]['count']+=1;
					}
				}
								
				if(isset($newdatahub[$mr_branch_code])){
					if(empty($newdatahub[$mr_branch_code]['count_qty'])){
						$newdatahub[$mr_branch_code]['count_qty']=0;
					}
					$newdatahub[$mr_branch_code]['count_qty']+=1;
				}else{
					$newdatahub[$mr_branch_code]['count_qty']=1;
				}

				if(!isset($newdatahub[$mr_branch_code])){

					$newdatahub[$mr_branch_code]['count']=1;
					$newdatahub[$mr_branch_code]['dd'][$page]['head']['p'] = 1;
				}else{
					if(!isset($newdatahub[$mr_branch_code]['dd'][$page])){
						$newdatahub[$mr_branch_code]['dd'][$page]['head']['p'] = $newdatahub[$mr_branch_code]['count'];
					}
				}

				// end runpage*************************
			
				$val_i['no'] = $newdatahub[$mr_branch_code]['no'];
				$val_i['qty'] = 1;
				$val_i['qty'] = 1;
				// if(strlen($val_i['mr_topic'])>100){
				// 	$val_i['mr_topic'] = substr($val_i['mr_topic'],0,100);
				// }

				$string = strip_tags($val_i['mr_topic']);
				if (strlen($string) > 100) {
					// truncatre string
					$string = trim(preg_replace('/\s\s+/', ' ', $string));
					$stringCut = mb_substr($string,0,100,"utf8"); 
					//substr($string, 0, 102);
					//$endPoint = strrpos($stringCut, ' ');

					//if the string doesn't contain any space then it will cut without word basis.
					$string = $stringCut;
					//$string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
					$string .= '...';
					$val_i['mr_topic'] =  $string ;
				}
				//echo $string;


				$newdatahub[$mr_branch_code]['dd'][$page]['data'][$index] = $val_i;
				$newdatahub[$mr_branch_code]['dd'][$page]['head']['d']		= $val_i;
				$newdatahub[$mr_branch_code]['dd'][$page]['head']['date']		= date('d/m/Y');
				$newdatahub[$mr_branch_code]['dd'][$page]['head']['time']		= date('H:i:s');
				$no++;
				
				// runpage*************************

				if($index == 20){
					$index = 1;
					$page++;
				}else{
					$index++;
				}

				// if($newdatahub[$mr_branch_code]['check'] == 20){
				// 	$index = 1;
				// 	$page++;
				// }else{
				// 	$index++;
				// }
				// end runpage*************************
			
			
			
		}
	}
}




























$txt_html='
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Times New Roman", Times, serif;
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 14px
 }
 tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 12px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

    @media print {
	#print_p{
		  display:none;
		}
      html,
      body {
        width: 210mm;
        height: 297mm;
		 font-family: "Times New Roman", Times, serif;
      }
      .page {
		width: 210mm;
		height: 297mm;
		min-height: 297mm;
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: unset;
      }
    }
  </style>
</head>

<body>
  <div class="book">
   
   ';
foreach( $newdatahub as $i => $pa){

foreach( $pa['dd'] as $dt){
//echo"kkkk<br>";
   $txt_html.='  <div class="page">
      <div class="subpage ">
        <div class="header mb-3">
          <h5 class="">สารบรรณกลางและโลจิสติกส์  บริหารทรัพยากรอาคาร</h5>
          <h6 class="">ใบนำส่ง-ตอบรับเอกสาร</h6>
	
		</div>
		<table width="100%">
			<tr>
				<td colspan="2" align="right">หน้า  :&nbsp;&nbsp;&nbsp;<b><u>'.$dt['head']['p'].'/'.$pa['count'].'&nbsp;&nbsp;&nbsp;</u></b></td>
            </tr>
              <tr>
				<td colspan="2" align="right"><p class="card-text">วันที่พิมพ์  <b><u>  '.$dt['head']['date']."  ".$dt['head']['time'].' </u></b></p></td>
              </tr>
			  
              <tr>
			  <td><p class="card-text"></td>
			  <td align="right"><p class="card-text">รอบ  <b><u>&nbsp;&nbsp;&nbsp;'. $round_name.'&nbsp;&nbsp;&nbsp;</u></b> </p></td>
			  </tr>
		 </table>
		 <div class="form-group">
            <label class="">ถึง : '.$dt['head']['d']['re_mr_branch_code'].'-'.$dt['head']['d']['re_mr_branch_name'].' </label>
        </div>
        <div class=" mb-2 p-2">
           
        </div>

        <div class="table-responsive" style="height:630px;">
          <table   border="1" class="table table-sm">
            <thead class="thead-light">
              <tr>
				<th>ลำดับ</th>
                <th width="200px">ผู้ส่งเอกสาร</th>
                <th>ชื่อเอกสาร</th>
                <th>จำนวนซอง</th>
                <th>Barcode</th>
              </tr>
            </thead>';
           
			foreach( $dt['data'] as $subd){
			$txt_html.='<tbody>';
			if($subd['no'] == "all"){
			$txt_html.='<tr>
				<th class="text-right"colspan="3"><u>รวมจำนวนซองทั้งสิ้น</u></th>
				<th class="text-center" ><u>{{subd.qty}}</u></th>
				<th></th>
			</tr>';

			}else{ 
				$txt_html.=' <tr>
                
                <td>'.$subd['no'].'</td>
				<td>'.$subd['send_emp_code'].' : '.$subd['send_name'].'  '.$subd['send_lname'].'		</td>
                <td style="
					overflow: hidden;
					text-overflow: ellipsis;
					display: -webkit-box;
					-webkit-line-clamp: 1; /* number of lines to show */
					-webkit-box-orient: vertical;
				">'.$subd['mr_topic'].'</td>
                <td class="text-center">'.$subd['qty'].'</td>
                <td>'.$subd['mr_work_barcode'].'</td>
              </tr>';
			}
			
            $txt_html.='';
			}
			if($dt['head']['p'] == $pa['count']){ 
				$txt_html.=' <tr>
                <td class="text-right" colspan="3">รวมจำนวนซองทั้งสิ้น</td>
                <td class="text-center" colspan="2">'.$pa['count_qty'].'</td>
              </tr>';
			}
			$txt_html.='
			</tbody> 
          </table>
        </div>
		<br>
				<br>
				<br>
				<center>
				<table border="0" width="100%"   class="center">
				<tr>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้ออกใบคุมงาน</p>
					<p>(..............................................)</p>
					<p>วันที่................................</p>
					</td>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้รับเอกสาร</p>
					<p>รหัสพนักงาน(..............................................)</p>
					<p>วันที่................................</p>
					</td>
				</tr>

				<tr>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้ตรวจสอบงาน</p>
					<p>(..............................................)</p>
					<p>วันที่................................</p>
					</td>
					<td align="center">
					<p>(ลงชื่อ)...........................................ผู้เดินเอกสาร/ผู้ส่งเอกสาร</p>
					<p>(..............................................)</p>
					<p>วันที่................................</p>
					</td>
				</tr>
			</table>
			</center>
			
      </div>
    </div>';
}
}
$txt_html.='  
  

 
</body>

</html>';




// echo '<pre>'.print_r($newdata,true).'</pre>';
// echo $txt_html;
// exit;
function ucfirst_utf8($str) {
    if (mb_check_encoding($str,'UTF-8')) {
        $first = mb_substr(
            mb_strtoupper($str, "utf-8"),0,1,'utf-8'
        );
        return $first.mb_substr(
            mb_strtolower($str,"utf-8"),1,mb_strlen($str),'utf-8'
        );
    } else {
        return $str;
    }
}


require_once 'ThaiPDF/thaipdf.php';
$left=1;
$right=1;
$top=1;
$bottom=1;
$header=0;
$footer=1;
$filename='file.pdf';
pdf_margin($left,$right,$top, $bottom,$header,$footer);
pdf_html($txt_html);
pdf_orientation('P');        
pdf_echo();

exit;


//echo '<pre>'.print_r($newdatahub,true).'</pre>';
//exit;
$template = Pivot_Template::factory('mailroom/print_sort_branch_all.tpl');
$template->display(array(
	//'debug' => print_r($newdatahub,true),
	'data' => $newdatahub,
	'alert' => $alert,
	'userRoles' => $userRoles,
	'user_data' => $user_data,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
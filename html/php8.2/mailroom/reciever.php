<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Round.php';

error_reporting(E_ALL & ~E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$roundDao 		= new Dao_Round();

$round			= $roundDao->getRoundworkHO();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();


$barcode_full 	= "TM".date("dmy");
$user_id		= $auth->getUser();
$user_data 		= $userDao->getempByuserid($user_id);
$template 		= Pivot_Template::factory('mailroom/reciever.tpl');

$template->display(array(
	//'debug' => print_r($path_pdf,true),
	//'userRoles' => $userRoles,
	//'success' => $success,
	'barcode_full' => $barcode_full,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	//'users' => $users,
	
	'select' 		=> 0,
	'round' 		=> $round,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
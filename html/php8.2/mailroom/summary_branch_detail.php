<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';
require_once 'Dao/Employee.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req 					= new Pivot_Request();
$userDao 				= new Dao_User();
$userRoleDao 			= new Dao_UserRole();
$status_Dao 			= new Dao_Status();
$employee_Dao 			= new Dao_Employee();



//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$mr_hub_id 	= $req->get('mr_hub_id');
$user_id 	= $req->get('user_id');
$status_id 	= $req->get('status_id');
$start 					= $req->get('date_start');
$end 					= $req->get('date_end');


if($start == ''){
	$start = date('Y-m-d 00:00:00');
}else{
	$start = date('Y-m-d 00:00:00',strtotime($start));
}

if($end == ''){
	$end = date('Y-m-d 23:59:59');
}else{
	$end = date('Y-m-d 23:59:59',strtotime($end));
}

$sql_order = "
				SELECT 
						m.messenger_user_id as re_messenger_user_id,
						wi.messenger_user_id as send_messenger_user_id,
						m.mr_work_barcode,
						m.mr_topic,
						m.mr_work_main_id,
						m.mr_work_remark,
						m.mr_type_work_id,
						m.mr_status_id,
						wt.mr_type_work_name,
						st.mr_status_name,
						emp_re.mr_emp_code	 as mr_emp_code_re	,
						emp_re.mr_emp_name	 as mr_emp_name_re	,
						emp_re.mr_emp_lastname as mr_emp_lastname_re,

						emp_send.mr_emp_code	 as mr_emp_code_send	,
						emp_send.mr_emp_name	 as mr_emp_name_send	,
						emp_send.mr_emp_lastname as mr_emp_lastname_send,

						br.mr_branch_name,
						br.mr_branch_code,

						bs.mr_branch_name as mr_branch_name_send,
						bs.mr_branch_code as mr_branch_code_send

						FROM mr_work_main m
						left join mr_type_work wt on(wt.mr_type_work_id = m.mr_type_work_id)
						left join mr_status st on(st.mr_status_id = m.mr_status_id)
						left join mr_work_inout wi on(wi.mr_work_main_id = m.mr_work_main_id)
						left join mr_branch br on(br.mr_branch_id = wi.mr_branch_id)
						left join mr_branch  bs on(bs.mr_branch_id = m.mr_branch_id)

						left join mr_user u_re on(u_re.mr_user_id = m.messenger_user_id)
						left join mr_user u_send on(u_send.mr_user_id = wi.messenger_user_id)

						left join mr_emp emp_re on(emp_re.mr_emp_id = u_re.mr_emp_id)
						left join mr_emp emp_send on(emp_send.mr_emp_id = u_send.mr_emp_id)
						left join mr_hub h on CASE
												   WHEN m.mr_type_work_id IN (2)
													   THEN h.mr_hub_id = br.mr_hub_id 
													   ELSE h.mr_hub_id = bs.mr_hub_id 
													END
						
						WHERE 
							m.mr_type_work_id IN (2,3) 
							and m.sys_timestamp >=  ? 
							and m.sys_timestamp <=  ?  

				";
				$params = array();
				$params[] = (string)$start;
				$params[] = (string)$end;


				
			//if($user_id != '' and $user_id != 0 ){
			//	$sql_order .=" and m.messenger_user_id = ? ";
			//	array_push($params,$user_id);
			//}else if($user_id == 0){
			//	$sql_order .=" and m.messenger_user_id is null ";
			//}

			if($mr_hub_id != '' and $mr_hub_id != 0 ){
				$sql_order .=" and h.mr_hub_id = ? ";
				array_push($params,$mr_hub_id);
			}else{
				$sql_order .=" and h.mr_hub_id is null ";
			}
			if($status_id != ''){
					$status_id_params = explode(',', $status_id);
					$status_id_condition = str_repeat('?,', count($status_id_params) - 1) . '?'; // example: ?,?,?

				$sql_order .=" and m.mr_status_id in(".$status_id_condition.")";
				$params = array_merge($params,$status_id_params);
			}
			$sql_order .= "group by m.mr_work_main_id";

$order 		= $employee_Dao->select_employee($sql_order,$params);


//echo "<pre>".print_r($user_id,true)."</pre>";
//echo "<pre>".print_r($sql_order,true)."</pre>";
//echo "<pre>".print_r($order,true)."</pre>";
//exit;

//$mess_data = $userDao->getMessSend();
//$status = $status_Dao->fetchAll();


$template = Pivot_Template::factory('mailroom/summary_branch_detail.tpl');
$template->display(array(
	'debug' => print_r($path_pdf,true),
	'order' => $order,
	'success' => $success,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	//'mess_data' => $mess_data,
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'status' => $status
));
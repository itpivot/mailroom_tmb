<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';

require_once 'Dao/UserRole.php';
error_reporting(E_ALL & ~E_NOTICE);


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}

$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);
$user_data_show = $userDao->getEmpDataByuseridProfile($user_id);



//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();

$req = new Pivot_Request();


$sqlt = 'SELECT * FROM `mr_branch_category`';
$branc_category = $userDao ->select($sqlt);
$txt_branc_category='<select  onchange="getData" class="select_chang_brabch" id="mr_branch_category_id">
<option value="" disabled selected>--</option>
';
foreach($branc_category as $key_i => $val_i){
	$txt_branc_category.='<option value="'.$val_i['mr_branch_category_id'].'">'.$val_i['mr_branch_category_name'].'</option>';
}      
$txt_branc_category.='</select>';




$sql='SELECT 
	b.mr_branch_id, 
	b.mr_branch_name, 
	b.mr_branch_code, 
	b.mr_hub_id, 
	b.mr_branch_type as mr_branch_type_id, 
	b.sys_date
FROM mr_branch b ';
$branchdata = $userDao ->select($sql);

$txt_branch='<select  onchange="getData" class="select_chang_brabch" id="mr_branch_id">
<option value="" disabled selected>--</option>
';
foreach($branchdata as $key_l => $val_l){
	$txt_branch.='<option value="'.$val_l['mr_branch_id'].'">'.$val_l['mr_branch_code'].' : '.$val_l['mr_branch_name'].'</option>';
}      
$txt_branch.='</select>';


$sql2 = 'SELECT * FROM mr_branch_type';
$branc_type = $userDao ->select($sql2);
$txt_branc_type='<select  onchange="getData" class="select_chang_brabch" id="mr_branch_type_id">
<option value="" disabled selected>--</option>
';
foreach($branc_type as $key_i => $val_i){
	$txt_branc_type.='<option value="'.$val_i['mr_branch_type_id'].'">'.$val_i['branch_type_name'].'</option>';
}      
$txt_branc_type.='</select>';


$sql3 = 'SELECT * FROM mr_hub';
$hub = $userDao ->select($sql3);
$txt_hub='<select  onchange="getData" class="select_chang_brabch" id="mr_hub_id">
<option value="" disabled selected>--</option>
';

$txt_hub2='<select  onchange="getData" class="select_chang_brabch" id="mr_hub_id2">
<option value="" disabled selected>--</option>
';
foreach($hub as $key_j => $val_kj){
	$txt_hub.='<option value="'.$val_kj['mr_hub_id'].'">'.$val_kj['mr_hub_name'].'</option>';
	$txt_hub2.='<option value="'.$val_kj['mr_hub_id'].'">'.$val_kj['mr_hub_name'].'</option>';
}  
$txt_hub.='</select>';
$txt_hub2.='</select>';

$template = Pivot_Template::factory('mailroom/update_branch.tpl');
$template->display(array(
    'error'             => print_r($errors, true),
    'debug'             => $debug,
    'branchdata'        => $branchdata,
    'branc_type'        => $branc_type,
    'hub'        		=> $hub,
    'txt_branch'        => $txt_branch,
    'txt_branc_type'    => $txt_branc_type,
    'txt_hub'           => $txt_hub,
	'user_data' => $user_data,
	'user_data_show' => $user_data_show,
    'txt_hub2'           => $txt_hub2,
    'txt_branc_category'  => $txt_branc_category,
    'select'            => '0',
	//'users'           => $users,
	'role_id'           => $auth->getRole(),
	'roles'             => Dao_UserRole::getAllRoles()
));

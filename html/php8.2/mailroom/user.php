<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Status.php';
error_reporting(E_ALL & ~E_NOTICE);

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$statusDao = new Dao_Status();

//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();
$status_data = $statusDao->fetchAll();

$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$template = Pivot_Template::factory('mailroom/user.tpl');
$template->display(array(
	//'debug' => print_r($status_data,true),
	'status_data' => $status_data,
	//'userRoles' => $userRoles,
	'success' => $success,
	//'userRoles' => $userRoles,
	'user_data' => $user_data,
	//'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Department.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Floor.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$users = $userDao->fetchAll();
$userRoles = $userRoleDao->fetchAll();
$user_id= $auth->getUser();
$user_data = $userDao->getempByuserid($user_id);

$department_Dao 		= new Dao_Department();
$work_inout_Dao 		= new Dao_Work_inout();
$floor_Dao 				= new Dao_Floor();

$start = $req->get('start_date');
$end = $req->get('end_date');

function changeFormatDate($date) {
    $result = '';
    if(!empty($date)) {
        $new_date = explode('/', $date);
        $result = $new_date[2]."-".$new_date[1]."-".$new_date[0];
        return $result;
    }
}

$data = array();

$data['start_date'] = changeFormatDate($start); 
$data['end_date'] = changeFormatDate($end); 

$dept = $floor_Dao->getFloor();
$work_inout = $work_inout_Dao->searchAutoSummary($data);


$resp = array();
$sum_newly = 0;
$sum_msg_receive = 0;
$sum_mailroom = 0;
$sum_msg_send = 0;
$sum_success = 0;
$sum_cancel = 0;

foreach($work_inout as $k => $v) {
    if($v['mr_department_sender_floor'] != "") {
        switch(intval($v['mr_status_id'])) {
            case 1:
                $sum_newly += 1;
                break;
            case 2:
                $sum_msg_receive += 1;
                break;
            case 3:
                $sum_mailroom += 1;
                break;
            case 4:
                $sum_msg_send += 1;
                break;
            case 5:
                $sum_success += 1;
                break;
            case 6:
                $sum_cancel += 1;
                break;
        }
    }
}


// echo print_r($work_inout);
// echo "<br>";
// echo $sum_newly." ".$sum_msg_receive." ".$sum_mailroom." ".$sum_msg_send." ".$sum_success." ".$sum_cancel;


foreach($dept as $key => $val) {
    $resp[$key]['no'] = $key + 1;
    $resp[$key]['mr_department_floor'] = $val['mr_department_floor'];
    $newly = 0;
    $msg_receive = 0;
    $mailroom = 0;
    $msg_send = 0;
    $success = 0;
    $cancel = 0;
    foreach($work_inout as $keys => $values) {
        if($values['mr_department_sender_floor'] != "") {
            if($values['mr_department_sender_floor'] == $val['mr_department_floor']) {
                if(intval($values['mr_status_id']) == 1) {
                        $newly += 1;
                } else if(intval($values['mr_status_id']) == 2) {
                        $msg_receive += 1;
                } else if(intval($values['mr_status_id']) == 3) {
                        $mailroom += 1;
                } else if(intval($values['mr_status_id']) == 4) {
                        $msg_send += 1;
                } else if(intval($values['mr_status_id']) == 5) {
                        $success += 1;
                } else if(intval($values['mr_status_id']) == 6) {
                        $cancel += 1;
                }
            }
        }
    }
    $resp[$key]['newly'] = $newly;
    $resp[$key]['msg_receive'] = $msg_receive;
    $resp[$key]['mailroom'] = $mailroom;
    $resp[$key]['msg_send'] = $msg_send;
    $resp[$key]['success'] = $success;
    $resp[$key]['cancel'] = $cancel;
    $resp[$key]['total'] = intval($newly) + intval($msg_receive) + intval($mailroom) + intval($msg_send) + intval($success) + intval($cancel);
}

$resp[count($resp)] = array(
    'no' => '',
    'mr_department_floor' => 'รวม',
    'newly' => ($sum_newly == null) ? 0 : $sum_newly,
    'msg_receive' => ($sum_msg_receive == null) ? 0 : $sum_msg_receive,
    'mailroom' => ($sum_mailroom == null) ? 0 : $sum_mailroom,
    'msg_send' => ($sum_msg_send == null) ? 0 : $sum_msg_send,
    'success' => ($sum_success == null ) ? 0 : $sum_success,  
    'cancel' => ($sum_cancel == null) ? 0 : $sum_cancel,
    'total' => $sum_newly + $sum_msg_receive + $sum_mailroom + $sum_msg_send + $sum_success + $sum_cancel
);

// echo print_r($resp);
echo json_encode($resp);
// exit();
?>
<?php
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/Employee.php';

$employeeDao = new Dao_Employee();
$req = new Pivot_Request();

$txt = $_GET['q'];

$emp_data = $employeeDao->getEmpDataWithTXTsearch($txt);


$json = array();
foreach ($emp_data as $key => $value) {
        $fullname = $value['mr_emp_code']." - ". $value['mr_emp_name']." ".$value['mr_emp_lastname'];
        //$json[] = [ "id" => $value['mr_emp_id'], "text" => $fullname  ];
		$json[] = array(
			"id" => $value['mr_emp_id'],
			"text" => $fullname
		);
}


echo json_encode($json);
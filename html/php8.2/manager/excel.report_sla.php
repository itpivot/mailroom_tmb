<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'PHPExcel.php';

ob_start();

$auth = new Pivot_Auth();
//if (!$auth->isAuth()) {
//    Pivot_Site::toLoginPage();
//}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();                                 
 
$month				=	$req->get('month');
$year				=	$req->get('year');
$user_id			= $auth->getUser();

$date 				= $year.'-'.$month;                                                 
   
$data = $work_inoutDao->reportAllMonthlySend($date);



function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = explode("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}

function setTime($date){
	$result = "";
	if( $date ){
		list( $d, $t ) = explode(" ", $date);
		$result = $t;
	}
	return $result;
}

function DateTimeDiff($strDateTime1,$strDateTime2){
	return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}




$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma')->setSize(10);
	
$objPHPExcel->getProperties()->setCreator("Pivot Co.,Ltd")->setLastModifiedBy("Pivot")->setTitle("Report Agent")->setSubject("Report")->setDescription("Report document for Excel, generated using PHP classes.");


$columnIndexs = range('A','P');
$columnNames[0] = array(
		0 => 'No.',                           
		1 => 'Barcode',                            
		2 => 'ชื่อผู้รับ',                            
		3 => 'ที่อยู่',                            
		4 => 'ชื่อผู้ส่ง',                            
		5 => 'ที่อยู่',    
		6 => 'ชื่อเอกสาร',
		7 => 'วันที่ส่ง',   
		8 => 'วันที่สำเร็จ',                            
		9 => 'สถานะงาน',                            
		10 => 'ประเภทการส่ง',                            
		11 => 'หมายเหตุ',                            
		12 => 'สั่งงาน',                            
		13 => 'พนักงานเดินเอกสารรับเอกสารแล้ว',                            
		14 => 'เวลาที่งานสำเร็จ',  
		15 => 'Result'
);

// Set column widths
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setWidth(30);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(40);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setWidth(20);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setWidth(30);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('M')->setWidth(30);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('N')->setWidth(40);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('O')->setWidth(30);
    $objPHPExcel->getActiveSheet(0)->getColumnDimension('P')->setWidth(30);

$row = 1;
foreach ($columnNames as $key => $value) {
	for($index = 0; $index < count($columnIndexs); $index++) {
		$leadColumns[$key][$index]['index'] = $columnIndexs[$index].$row;
		$leadColumns[$key][$index]['name'] = $columnNames[$key][$index];
	}
	$row++;
}

foreach($leadColumns as $head) {
	foreach ($head as $headColumn) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($headColumn['index'], $headColumn['name'])->getStyle($headColumn['index']);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLACK);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getFill()->getStartColor()->setARGB('FFDCDCDC');
		$objPHPExcel->getActiveSheet(0)->getStyle($headColumn['index'])->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);																																 
	}
}

$indexs = 2;

//echo "<pre>".print_r($data,true)."</pre>";
//exit();



foreach($data as $keys => $vals) { 
            if( $vals['mr_work_date_sent'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_sent']);
				$date_order[$keys]['mr_work_date_sent'] = $d."/".$m."/".$y;
			}	
			
			if( $vals['mr_work_date_success'] ){
				list( $y, $m, $d ) = explode("-", $vals['mr_work_date_success']);
				$date_meet[$keys]['mr_work_date_success'] = $d."/".$m."/".$y;
			}	
			
			if(  $vals['name_re'] ){
				$name_receive[$keys]['name_re'] 				= $vals['name_re']." ".$vals['lastname_re'];
				$name_receive[$keys]['depart_receive'] 		= $vals['depart_code_receive']." - ".$vals['depart_name_receive']." ชั้น ".$vals['depart_floor_receive'];
			}			
			
			if(  $vals['mr_emp_name'] ){
				$name_send[$keys]['send_name'] 				= $vals['mr_emp_name']." ".$vals['mr_emp_lastname'];
				$name_send[$keys]['depart_send'] 			= $vals['mr_department_code']." - ".$vals['mr_department_name']." ชั้น ".$vals['depart_floor_send'];
            }		
            
			
            $data[$keys]['time_order'] 				= setTime($data[$keys]['time_order']);
			
			
			$data_log = $work_logDao->SLAlog( $vals['mr_work_main_id'] );
			//echo "<pre>".print_r($data_log,true)."</pre>";
			//exit();
			
				foreach($data_log as $key_s => $val_s) { 	
					if ( $val_s['mr_status_id'] == 2 ){
						$data[$keys]['time_mess_re'] 			= setTime($val_s['sys_timestamp']);
					}
				
					
					if ( $val_s['mr_status_id'] == 5 ){
						$data[$keys]['time_success']			= setTime($val_s['sys_timestamp']);
					}
					
					
				}
	
	
	
			$diff = DateTimeDiff($data[$keys]['time_mess_re'],$data[$keys]['time_success']);
			if( $diff >= 1 || $diff == '' || $data[$keys]['time_success'] == '' ){
				$data[$keys]['result']	= "False";	
			}else{
				$data[$keys]['result']	= "Pass";	
			}
			//echo "<pre>".print_r($data,true)."</pre>";
			//exit();
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$indexs, ($keys+1))->getStyle('A'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$indexs, $vals['mr_work_barcode'])->getStyle('B'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$indexs, $name_receive[$keys]['name_re'])->getStyle('C'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$indexs, $name_receive[$keys]['depart_receive'])->getStyle('D'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$indexs, $name_send[$keys]['send_name'])->getStyle('E'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$indexs, $name_send[$keys]['depart_send'])->getStyle('F'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$indexs, $vals['mr_topic'])->getStyle('G'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$indexs, $date_order[$keys]['mr_work_date_sent'])->getStyle('H'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$indexs, $date_meet[$keys]['mr_work_date_success'])->getStyle('I'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$indexs, $vals['mr_status_name'])->getStyle('J'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$indexs, $vals['mr_type_work_name'])->getStyle('K'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$indexs, $vals['mr_work_remark'])->getStyle('L'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$indexs, $data[$keys]['time_order'] )->getStyle('M'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$indexs, $data[$keys]['time_mess_re'])->getStyle('N'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$indexs, $data[$keys]['time_success'])->getStyle('O'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$indexs, $data[$keys]['result'])->getStyle('P'.$indexs)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->getActiveSheet(0)->getStyle('A'.$indexs.':P'.$indexs)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$indexs++;
}


$nickname = "report_send_sla";
$filename = $nickname.'.xls';


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Sla Report');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$filename.'"'); 
header('Cache-Control: max-age=0');
header('Pragma: public');
ob_end_clean();
$objWriter->save('php://output');
exit();
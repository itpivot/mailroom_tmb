<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Employee.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$work_main_Dao = new Dao_Work_main();
$work_inout_Dao = new Dao_Work_inout();
$employee_Dao = new Dao_Employee();

$type = $req->get('type');

$userId = $auth->getUser();

$empUser = $userDao->getempByuserid($userId);

$empData = $employee_Dao->getEmpDataById($empUser['mr_emp_id']);



switch($type) {
    case 'get':
        // get status id 8 : รวมเอกสารนำส่ง
        $status = 8;
        $work_type = '2,3';
        $data_receive = $work_main_Dao->getSendBranch($status, $work_type, $empData['mr_hub_id']);

        if(count($data_receive) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_receive as $key => $val) {
                
                $sender_tel = '<a href="tel:'.$val['mr_emp_tel'].'">'.$val['mr_emp_tel'].'</a>';

                if(!empty($val['mr_emp_mobile'])) {
                    $sender_tel .= '| <a href="tel:'.$val['mr_emp_mobile'].'">'.$val['mr_emp_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    $branch = $val['mr_workplace'];
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้ส่ง : </b>'.$val['mr_emp_name']." ".$val['mr_emp_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$sender_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    
    case 'get_search':
        $txt = $req->get('txt');
        $status = 8;
        $work_type = '2,3';
        $data_receive = $work_main_Dao->getReceiveBranch($status, $work_type, $empData['mr_hub_id'], $txt);

        if(count($data_receive) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_receive as $key => $val) {
                
                $sender_tel = '<a href="tel:'.$val['mr_emp_tel'].'">'.$val['mr_emp_tel'].'</a>';

                if(!empty($val['mr_emp_mobile'])) {
                    $sender_tel .= '| <a href="tel:'.$val['mr_emp_mobile'].'">'.$val['mr_emp_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    $branch = $val['mr_workplace'];
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้ส่ง : </b>'.$val['mr_emp_name']." ".$val['mr_emp_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$sender_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    case 'update': 
        $work_main_id = $req->get('wId');
        $action = $req->get('action');

        if(intval($action) == 1) {
            // received
            $main['mr_status_id'] = 9; // แมสเข้ารับเอกสาร
            $main['messenger_user_id'] = $userId;
            
            $inout['mr_status_receive'] = intval($action);
        } else {
            // not found
            $main['messenger_user_id'] = $userId;

            $inout['mr_status_receive'] = intval($action);
        }
         
        $m_Id = $work_main_Dao->save($main, $work_main_id);
        $inout_Id = $work_inout_Dao->updateInOutWithMainId($inout, $work_main_id);

        $status = 8;
        $work_type = '2,3';
        $data_receive = $work_main_Dao->getReceiveBranch($status, $work_type, $empData['mr_hub_id']);

        if(count($data_receive) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_receive as $key => $val) {
                
                $sender_tel = '<a href="tel:'.$val['mr_emp_tel'].'">'.$val['mr_emp_tel'].'</a>';

                if(!empty($val['mr_emp_mobile'])) {
                    $sender_tel .= '| <a href="tel:'.$val['mr_emp_mobile'].'">'.$val['mr_emp_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    $branch = $val['mr_workplace'];
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้ส่ง : </b>'.$val['mr_emp_name']." ".$val['mr_emp_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$sender_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }
        echo json_encode($card);
        break;

    case 'get_mailroom':
        // get status id 10 : ห้องสารบรรณกลางรับเอกสาร
        $status = '3, 10';
        $work_type = 2;
        $data_mailroom = $work_inout_Dao->getReceiveBranchfromMailroom($status, $work_type, $empData['mr_hub_id']);

        if(count($data_mailroom) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_mailroom as $key => $val) {
                
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้ส่ง : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    case 'get_mailroom_search':
        $txt = $req->get('txt');
        $status = '3, 10';
        $work_type = 2;

        $data_mailroom = $work_inout_Dao->getReceiveBranchfromMailroom($status, $work_type, $empData['mr_hub_id'], $txt);

        if(count($data_mailroom) > 0) {
            $str = '';
            $counter = 0;
            foreach($data_mailroom as $key => $val) {
                
                $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                

                if(!empty($val['mr_emp_mobile'])) {
                    $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                }

                if(!empty($val['mr_emp_mobile'])) {
                    $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                }

                if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                    $card_color = "bg-warning";
                } else {
                    $card_color = "";
                }
                
                if(!empty($val['mr_branch_name'])) {
                    $branch = $val['mr_branch_name'];
                } else {
                    // $branch = $val['mr_workplace'];
                    $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                }

                $str .= '<div class="card '.$card_color.' mb-2">';
                    $str .= '<div class="card-body space-height">';
                        $str .= '<div class="row">';
                        $str .= '<div class="col-6 text-left">';
                            $str .= '<input type="checkbox" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                        $str .= '</div>';
                        $str .= '<div class="col-6 text-right">';
                            $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                        $str .= '</div>';
                        $str .= '</div>';
                        
                        $str .= '<p><b>ผู้ส่ง : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                        $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                        $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                        $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                        $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                        $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                        $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                        $str .= '<hr>';
                        $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                            $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';

                $counter++;
            }

            $card['data'] = $str;
            $card['counter'] = $counter;

            
        } else {
            $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
            $card['counter'] = 0;
        }

        echo json_encode($card);
        break;
    
    case 'update_mailroom':
        $work_main_id = $req->get('wId');
        $action = $req->get('action');

        if(intval($action) == 1) {
            // received
            $main['mr_status_id'] = 11; // แมสเข้ารับเอกสาร
            $main['messenger_user_id'] = $userId;
            
            $inout['mr_status_receive'] = intval($action);
        } else {
            // not found
            $main['messenger_user_id'] = $userId;

            $inout['mr_status_receive'] = intval($action);
        }
            $m_Id = $work_main_Dao->save($main, $work_main_id);
            $inout_Id = $work_inout_Dao->updateInOutWithMainId($inout, $work_main_id);

            $status = '3, 10';
            $work_type = 2;
            $data_mailroom = $work_inout_Dao->getReceiveBranchfromMailroom($status, $work_type, $empData['mr_hub_id']);

            if(count($data_mailroom) > 0) {
                $str = '';
                $counter = 0;
                foreach($data_mailroom as $key => $val) {
                    
                    $receiver_tel = '<a href="tel:'.$val['receiver_tel'].'">'.$val['receiver_tel'].'</a>';
                    $replace_tel = '<a href="tel:'.$val['replace_tel'].'">'.$val['replace_tel'].'</a>';
                    

                    if(!empty($val['mr_emp_mobile'])) {
                        $receiver_tel .= '| <a href="tel:'.$val['receiver_mobile'].'">'.$val['receiver_mobile'].'</a>';
                    }

                    if(!empty($val['mr_emp_mobile'])) {
                        $replace_tel .= '| <a href="tel:'.$val['replace_mobile'].'">'.$val['replace_mobile'].'</a>';
                    }

                    if($val['mr_status_receive'] == 2 || $val['mr_status_send'] == 2) {
                        $card_color = "bg-warning";
                    } else {
                        $card_color = "";
                    }
                    
                    if(!empty($val['mr_branch_name'])) {
                        $branch = $val['mr_branch_name'];
                    } else {
                        // $branch = $val['mr_workplace'];
                        $branch = (!empty($val['receiver_workplace']) ? $val['receiver_workplace'] : $val['replace_workplace'] );
                    }

                    $str .= '<div class="card '.$card_color.' mb-2">';
                        $str .= '<div class="card-body space-height">';
                            $str .= '<div class="row">';
                            $str .= '<div class="col-6 text-left">';
                                $str .= '<input type="checkbox" id="check_'.$key.'" onchange="selectedCard('.$val['mr_work_main_id'].', this);">';
                            $str .= '</div>';
                            $str .= '<div class="col-6 text-right">';
                                $str .= '<h5 class="card-title text-right">'.$val['mr_work_barcode'].'</h5>';
                            $str .= '</div>';
                            $str .= '</div>';
                            
                            $str .= '<p><b>ผู้ส่ง : </b>'.$val['receiver_name']." ".$val['receiver_lastname'].'</p>';
                            $str .= '<p><b>เบอร์ติดต่อ : </b>'.$receiver_tel.'</p>';
                            $str .= '<p><b>สาขา : </b>'.$branch.'</p>';
                            $str .= '<p><b>ฮับ : </b>'.$val['mr_hub_name'].'</p>';
                            $str .= '<p><b>วันที่ : </b>'.date('d/m/Y', strtotime($val['mr_work_date_sent'])).'</p>';
                            $str .= '<p><b>ชื่อเอกสาร : </b>'.$val['mr_topic'].'</p>';
                            $str .= '<p><b>หมายเหตุ : </b>'.$val['mr_work_remark'].'</p>';
                            $str .= '<hr>';
                            $str .= '<div class="row">';
                                $str .= '<div class="col-6 text-left btn-zone btn-sm"><button type="button" class="btn btn-warning right font-weight-bold" onclick="updateStatus('.$val['mr_work_main_id'].', 2);">ไม่พบเอกสาร</button></div>';
                                $str .= '<div class="col-6 text-right btn-zone btn-sm"><button type="button" class="btn btn-primary right" onclick="updateStatus('.$val['mr_work_main_id'].', 1);">รับเอกสาร</button></div>';
                            $str .= '</div>';
                        $str .= '</div>';
                    $str .= '</div>';

                    $counter++;
                }

                $card['data'] = $str;
                $card['counter'] = $counter;

                
            } else {
                $card['data'] = '<p class="text-center text-muted font-weight-bold">ไม่มีเอกสารที่ต้องเข้ารับ !</p>';
                $card['counter'] = 0;
            }

        echo json_encode($card);
        break;
    default: 
        $status = 8;
        $work_type = '2,3';
        $data_receive = $work_main_Dao->getReceiveBranch($status, $work_type, $empData['mr_hub_id']);
        break;
        
}





?>
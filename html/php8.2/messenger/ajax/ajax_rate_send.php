<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();

$id            = $req->get('id');
$rating_mess   = $req->get('rating_mess');
$remark        = $req->get('remark');

if(preg_match('/<\/?[^>]+(>|$)/', $id)) {
	echo json_encode(array('status' => 500,  'error' => array( 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง') ));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $rating_mess)) {
	echo json_encode(array('status' => 500,  'error' => array( 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง') ));
	exit();
}
if(preg_match('/<\/?[^>]+(>|$)/', $remark)) {
	echo json_encode(array('status' => 500,  'error' => array( 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง') ));
	exit();
}

$send_id = $req->get('id');

if(preg_match('/<\/?[^>]+(>|$)/', $send_id)) {
	echo json_encode(array('status' => 500,  'error' => array( 'message' => 'รูปแบบข้อมูลไม่ถูกต้อง') ));
	exit();
}

$all_id = explode(",", $send_id);

$data_save_inout['rate_send'] = $rating_mess;
$data_save_inout['rate_remark'] = $remark;
foreach($all_id as $key => $id){
	$resp = $work_inout_Dao->updateInOutWithMainId($data_save_inout,$id);
	
}

if(is_numeric ($resp)) {
   echo json_encode(array(
      'status' => 200,
      'error' => array( 
         'message' => 'บันทึกข้อมูลสำเร็จ'
      )
   ));
}else{
   echo json_encode(array(
      'status' => 500,
      'error' => array(
         'message' => 'บันทึกข้อมูลไม่สำเร็จ',
         'resp' => $resp
      )
   ));
}

?>
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();

$user_id = $auth->getUser();

$rev_id = json_decode($req->get('data'), true);

$resp = array();
foreach($rev_id as $key => $value) {
	$data_inout = $work_inout_Dao->getWorkByMainIDAll($value);
	//echo print_r($data_inout);
	//exit();
	
	
	
	if( $data_inout['mr_status_receive'] != 2 ){
    
	
		if( $data_inout['mr_status_id'] == 1 ){
			$data[$key]['mr_status_id'] = 2;
			$status = 2;
		}else if ( $data_inout['mr_status_id'] == 3 ){
			$data[$key]['mr_status_id'] = 4;
			$status = 4;
		}		
		
		//$data[$key]['mr_work_date_success'] = date('Y-m-d');
		$w_save_main[] = $work_main_Dao->save($data[$key], intval($value));
		
		
		$data_inout_save['mr_status_receive'] = 1;
		$resp[] = $work_inout_Dao->save($data_inout_save,$data_inout['mr_work_inout_id']);
    
	}
}


if(count($w_save_main) > 0) {
    foreach($w_save_main as $k => $v) {
        $logs[$k]['sys_timestamp'] = date('Y-m-d H:i:s');
        $logs[$k]['mr_user_id'] = $user_id;
        $logs[$k]['mr_status_id'] = $status;
        $logs[$k]['mr_work_main_id'] = intval($v);
        $work_log_Dao->save($logs[$k]);
    }
}

if(count($resp) > 0) {
    echo 'success';
} else {
    echo 'failed';
}
// $sendData = $work_inout_Dao->searchWorkdataSend($txt);

// echo print_r($sendData);
// exit();
// echo json_encode($sendData);
?>
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Confirm_Log.php';
require_once 'PHPMailer.php';

$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();

$work_inout_Dao = new Dao_Work_inout();
$work_main_Dao = new Dao_Work_main();
$work_log_Dao = new Dao_Work_log();
$confirm_log_Dao = new Dao_Confirm_Log();

$sendmail = new SendMail();

$user_id = $auth->getUser();


// $empData = $userDao->getempByuserid(intval($user_id));
//$data_uri = $req->get('sign');
$send_id = $req->get('id');
$all_id = explode(",", $send_id);
$username = trim($req->get('username'));
$getEmpId = $userDao->getempByEmpid($username);

//echo $send_id.'<11111>';

foreach($all_id as $kee => $v_id){
	
	if(is_numeric($v_id)){
		//$v_data = $v_id;
		$v_data = $v_id;
	}else{
		$v_data = base64_decode(urldecode($v_id));
	}
	
	
	
	//echo $v_data.'<+++++>';
	$send_id  = $v_data;	


	$data['mr_status_id'] = 5;
	$data['mr_work_date_success'] = date('Y-m-d');






	$w_main = $work_main_Dao->getBarcodeByid(intval($send_id));
	$barcode = $w_main['mr_work_barcode'];


	//$path = '../../signature/';
	//
	//
	//if(!is_dir($path)) {
	//    @mkdir($path);
	//   
	//} 

	//function saveImage($data_uri, $path, $barcode) {
	//	$uri = explode(',',$data_uri);
	//    $encoded_image = $uri[1];
	//    $decoded_image = base64_decode($encoded_image, true);
	//    file_put_contents($path.$barcode.".png", $decoded_image);
	//    if(file_exists($path.$barcode.".png")) {
	//        return true;
	//    } else {
	//        return false;
	//    }
	//}
	//
	//$chk = saveImage($data_uri, $path, $barcode);
	//
	//if($chk  == true) {
		$result = $work_main_Dao->save($data, intval($send_id));
		if($result != ""){
			$logs['sys_timestamp'] = date('Y-m-d H:i:s');
			$logs['mr_work_main_id'] = $result;
			$logs['mr_user_id'] = $user_id;
			$logs['mr_status_id'] = 5;
			$resp = $work_log_Dao->save($logs);
		}
	//}
	//$send_id = 5 ;
	$datasByEmail = $work_main_Dao->getDatasByEmail(intval($send_id));


	if(  $datasByEmail[0]['reciever_user_id'] != $datasByEmail[0]['recheck_user_id'])
	{
		$result_recheck = '(รับแทน)' ;
	}



	$barcode			=  $datasByEmail['barcode'] 			;
	$send_name			=  $datasByEmail['send_name'] 			;
	$send_lastname		=  $datasByEmail['send_lastname'] 		;
	$send_email			=  $datasByEmail['send_email'] 		    ;
	$reciever_name		=  $datasByEmail['reciever_name'] 		;
	$reciever_lastname  =  $datasByEmail['reciever_lastname'] 	;


	$Subjectmail = "รายการส่งเอกสารเลขที่ $barcode ผู้รับได้รับเอกสารเรียบร้อยแล้ว ";
	$body="";
	$body.="เรียน คุณ <b>$send_name $send_lastname </b><br><br>";
	$body.="รายการส่งเอกสารเลขที่  <b> $barcode  </b> ผู้รับ <b>$reciever_name $reciever_lastname </b> $result_recheck<br>";
	$body.="ได้รับเอกสารเรียบร้อยแล้วค่ะ<br>";
	$body.="<br>";
	$body.="Pivot MailRoom Auto-Response Message <br>";
	$body.="(ข้อความอัตโนมัติจากระบบรับส่งเอกสารออนไลน์)<br>";



	//$result_mail = $sendmail->mailNotice($body,$Subjectmail, $send_email);
	$result_mail ='success';
	$cfLogs = array();
	if( ($resp != "") &&  $result_mail == 'success' ) {
		$cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
		$cfLogs['mr_work_main_id'] = intval($send_id);
		$cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
		$cfLogs['mr_status'] = 'Success';
		$cfLogs['descriptions'] = "Receive ".$result_recheck." Success";
		
		$cf = $confirm_log_Dao->save($cfLogs);

		if($cf != "") {
			$sendData = "success";
		}
	} else {
		$cfLogs['sys_timestamp'] = date('Y-m-d H:i:s');
		$cfLogs['mr_work_main_id'] = intval($send_id);
		$cfLogs['mr_emp_id'] = $getEmpId['mr_emp_id'];
		$cfLogs['mr_status'] = 'Failed';
		$cfLogs['descriptions'] = "Receive ".$result_recheck." Failed";
		
		$cf = $confirm_log_Dao->save($cfLogs);
		
		if($cf != "") {
			$sendData = "false";
		}
	}
}
echo $sendData;
?>
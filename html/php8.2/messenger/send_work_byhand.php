<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Employee.php';


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth() && !$auth->getLoginStatus()) {
    Pivot_Site::toLoginPage();
}
$req 			= new Pivot_Request();
$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();
$work_inout_Dao = new Dao_Work_inout();
$employee_Dao 	= new Dao_Employee();


//$users = $userDao->fetchAll();
//$userRoles = $userRoleDao->fetchAll();

$user_id		= $auth->getUser();
$user_data 		= $userDao->getempByuserid($user_id);
$empData 		= $employee_Dao->getEmpDataById($user_data['mr_emp_id']);
//echo print_r($branch_data,true);


$template = Pivot_Template::factory('messenger/sebd_work_byhand.tpl');
$template->display(array(
	//'debug' => print_r($path_pdf,true),
	'user_data' => $user_data,
	//'user_data' => $branch_data,
	//'success' => $success,
	//'userRoles' => $userRoles,
	//'users' => $users,
	'select' => 0,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'serverPath' => $_CONFIG->site->serverPath
));

?>
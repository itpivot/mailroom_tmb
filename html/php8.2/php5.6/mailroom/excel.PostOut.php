<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Work_inout.php';
require_once 'Dao/Work_main.php';
require_once 'Dao/Work_log.php';
require_once 'Dao/Work_post.php';
require_once 'PHPExcel.php';
header('Content-Type: text/html; charset=utf-8');


include_once('xlsxwriter.class.php');
ini_set('memory_limit', '-1');


/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$req 				= new Pivot_Request();                                  
$work_logDao 		= new Dao_Work_log();                                   
$work_mainDao 		= new Dao_Work_main();                                  
$work_inoutDao 		= new Dao_Work_inout();     
$work_postDao 		= new Dao_Work_post();
$user_role 			= $auth->getRole();

 
$data_search		=  array();       
$start_date 		= $req->get('date_1');
$end_date 			= $req->get('date_2');
$mr_type_work 		= $req->get('mr_type_work');
$username 			= $auth->getUserName();



$data_search['start_date'] 	= $start_date;
$data_search['end_date'] 	= $end_date;

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function setDateToDB($date){
	$result = "";
	if( $date ){
		list( $d, $m, $y ) = split("/", $date);
		$result = $y."-".$m."-".$d;
	}
	return $result;
}


$arr_report1	= array();
if($mr_type_work == 6){
	$data = $work_postDao->ExportDataReportPrice($data_search);
	$file_name = $username.'-Report-Thai_PostOut'.DATE('y-m-d').'.xlsx';
	$sheet1    = 'Detail_ThaipostOut';

	
	
	$headers1[] = array();
	$headers1[] = 'NO';                                 
	$headers1[] = 'วันที่สร้างรายการ';                         
	$headers1[] = 'วันที่ส่ง';                                
	$headers1[] = 'รอบ';                                  
	$headers1[] = 'ประเภทการส่ง';                          
	$headers1[] = 'Barcode';                              
	$headers1[] = 'เลข ปณ.';                              
	$headers1[] = 'เลข ปณ.(ตอบรับ)';                       
	$headers1[] = 'ผู้ส่ง'; 
	$headers1[] = 'รหัสค่าใช้จ่าย';                           
	$headers1[] = 'รหัสหน่วยงานผู้ส่ง-4';                      
	$headers1[] = 'หน่วยงานผู้ส่ง';                           
	$headers1[] = 'ผู้รับ';  
	$headers1[] = 'ที่อยู่';
	$headers1[] = 'จำนวน';
	if($user_role==4){
		$headers1[] = 'น้ำหนัก';
		$headers1[] = 'ราคา';
		$headers1[] = 'ราคารวม';
	}
	$headers1[] = 'หมายเหตุ';






















	$indexs = 2;
	foreach($data as $keys => $vals) {
				$arr_report1[$keys][]  	=	$keys+1;
				$arr_report1[$keys][]  	=	$vals['sys_timestamp'];
				$arr_report1[$keys][]  	=	$vals['d_send'];
				$arr_report1[$keys][]  	=	$vals['mr_round_name'];
				$arr_report1[$keys][]  	=	$vals['mr_type_post_name'];
				$arr_report1[$keys][]  	=	"'".$vals['mr_work_barcode'];
				$arr_report1[$keys][]  	=	$vals['num_doc'];
				$arr_report1[$keys][]  	=	$vals['sp_num_doc'];
				$arr_report1[$keys][]  	=	$vals['mr_emp_code']." ".$vals['mr_emp_name']." ".$vals['mr_emp_lastname'];
				$arr_report1[$keys][]  	=	($vals['mr_cost_code1']!='')?$vals['mr_cost_code1']:$vals['mr_cost_code2'];
				$arr_report1[$keys][]  	=	$vals['dep_code_4'];
				$arr_report1[$keys][]  	=	$vals['dep_code_send']." ".$vals['dep_send'];
				$arr_report1[$keys][]  	=	$vals['sresive_name'];
				$arr_report1[$keys][]  	=	$vals['mr_address'];
				$arr_report1[$keys][]  	=	$vals['quty'];
				if($user_role==4){
					$arr_report1[$keys][]  	=	$vals['mr_post_weight'];
					$arr_report1[$keys][]  	=	$vals['mr_post_price'];
					$arr_report1[$keys][]  	=	$vals['mr_post_totalprice'];
				}
				$arr_report1[$keys][]  	=	$vals['mr_work_remark'];
		unset($data[$keys]);
	}

	
}else{
	$data = $work_postDao->getdataPostinReport($data_search);
	$file_name = $username.'-Report-Thai-PostIn'.DATE('y-m-d').'.xlsx';
	$sheet1    = 'Detail-ThaipostIN';
	$headers1  		= array(
		'ลำดับ',                                                  
		'วันที่',                                                  
		'เลขที่เอกสาร',                                       
		'เลขที่ ปณ.',                                       
		'รหัสค่าใช้จ่าย',  
		'รหัสหน่วยงานผู้ส่ง-4',                                         
		'หน่วยงานผู้รับ',                                       
		'ชั้นผู้รับ',                                       
		'รอบ',                                       
		'ผู้ส่ง',                                                               
		'หมายเหตุ',  
		'สาขาผู้รับ',           
		'ผู้รับ',           
		'จำนวน',           
   );
	foreach($data as $key=>$val){
		$arr_report1[$key][] = ($key+1);
		$arr_report1[$key][] = $val['d_send'];
		$arr_report1[$key][] = $val['mr_work_barcode'];
		$arr_report1[$key][] = $val['num_doc'];
		$arr_report1[$key][] = $val['mr_cost_code'].' : '.$val['mr_cost_name'];
		$arr_report1[$key][] = $val['dep_code_4'];
		$arr_report1[$key][] = $val['dep_code_resive'].' : '.$val['dep_resive'];
		$arr_report1[$key][] = $val['floor_name'];
		$arr_report1[$key][] = $val['mr_round_name'];
		$arr_report1[$key][] = $val['sendder_name'].' '.$val['sendder_lname'];
		$arr_report1[$key][] = $val['mr_work_remark'];
		$arr_report1[$key][] = $val['mr_branch_code'].' '.$val['mr_branch_name'];
		$arr_report1[$key][] = $val['mr_emp_code'].' : '.$val['mr_emp_name']." ".$val['mr_emp_lastname'];
		$arr_report1[$key][] = $val['quty'];
	}
	

}  


//echo "ok";
//echo "<pre>".print_r($data[$keys],true)."</pre>";
// echo "<pre>".print_r($arr_report1,true)."</pre>";
// exit;




// $time_end = microtime_float();
// $time = $time_end - $time_start;

//echo "<br> Did nothing in $time seconds\n";
//exit;






header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-disposition: attachment; filename="'.$file_name.'"');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Cache-Control: max-age=0');



$writer = new  XLSXWriter();
$styleHead = array('border'=>'left,right,top,bottom' ,'fill'=>'#000000','color'=>'#ffffff' );
$styleRow = array( 'border'=>'left,right,top,bottom' );
$writer->setAuthor('Some Author');
$writer->writeSheetRow($sheet1,$headers1,$styleHead);
foreach ($arr_report1 as $key => $v) {
	$writer->writeSheetRow($sheet1,$v,$styleRow);
 }
 
$writer->writeToStdOut();
exit;
<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Post_type.php';
require_once 'Dao/Building_office.php';
require_once 'Dao/Work_order_post.php';
require_once 'Dao/Cost_center.php';
require_once 'Dao/Country.php';



/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
$Post_type_dao = new Dao_Post_type();
$building_office = new Dao_Building_office();
$Work_order_post_dao  = new Dao_Work_order_post();
$cost_center_dao  = new Dao_Cost_center();
$country_dao  = new Dao_Country();



$workorder_id = $req->get('id');
if($workorder_id!=""){
	$datetime=date("Y-m-d H:i:s");
}
$users 			= $auth->getUser();
$userName		= $auth->getUserName();
$userRoles = $userRoleDao->fetchAll();
$post_type = $Post_type_dao->fetchAll();
$office = $building_office->fetchAll();
$center = $cost_center_dao->getCost_centername();
//echo "<pre>".print_r($country_dao->fetchAll())."</pre>";
$template = Pivot_Template::factory('supervisor/add_costcenter.tpl');
$template->display(array(
	//'debug' => print_r($workorder_id,true),
	'userRoles' 		=> $userRoles,
	'country' 			=> $country_dao->fetchAll(),
	'post_type' 		=> $post_type,
	'workorder_id' 		=> $workorder_id,
	'center' 			=> $center,
	'office' 			=> $office,
	'order_post_dao' 	=> $order_post_dao,
	'users' 			=> $users,
	'role_id' 			=> $auth->getRole(),
	'datetime' 			=> $datetime,
	'roles' 			=> Dao_UserRole::getAllRoles()
));

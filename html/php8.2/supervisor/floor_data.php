<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
require_once 'Dao/Building_office.php';

/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

$userDao 		= new Dao_User();
$userRoleDao 	= new Dao_UserRole();

$building_Dao = new Dao_Building_office();

$b_data = $building_Dao->fetchAll();

$template = Pivot_Template::factory('supervisor/floor_data.tpl');
$template->display(array(
	//  'debug' => print_r($getUser,true),
	'userRoles' => $userRoles,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles(),
	'building' => $b_data
));

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="icon" href="../themes/bootstrap/css/favicon.ico" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pivot Mailroom Services">
		<meta name="author" content="Pivot">
		<script src="../themes/bootstrap/js/jquery-11.1.js"></script>
		<script src="js/chosen.jquery.js" charset="utf-8"></script>
  <style type="text/css">
		html, body { 
		   height: 100%; /* ให้ html และ body สูงเต็มจอภาพไว้ก่อน */
		   margin: 0;
		   padding: 0;
		   font-family: Arial, Helvetica, sans-serif;
		   font-size : 12px;
		}
		.td_padding{
			padding:2px;
		}
		.wrapper {
			
		   display: block;
		   min-height: 100%; /* real browsers */
		   height: auto !important; /* real browsers */
		   height: 100%; /* IE6 bug */
		   margin-bottom: -110px; /* กำหนด margin-bottom ให้ติดลบเท่ากับความสูงของ footer */
		   padding-left:20px;
		}
		.footer { 
		   height: 110px; /* ความสูงของ footer */
		   display: block;
		   text-align: center;
		}
		.hed{	
			border-bottom		: 2px solid #000;
		}	
		.fotr{	
			font-size			:100px
		}
		.underline{
			border-bottom-style:dotted; 
			border-width: 2px;
			font-size:16px;
		}.underline_table{
			border-bottom-style:dotted; 
			border-right-style:solid; 
			border-width: 1px;
			font-size:14px;
			padding:5px;
		}
		.table_border {
			//border: solid;
			}
		td{
			//padding:2px;
		}
		//@page { size: landscape; }
		@page { size: portrait; }
	</style>
  </head>
  <body>
 
 
 
 
 
 
 
 {% for order_post1 in order_post %} 
 {% for order_post in order_post1.dada %} 
			<div class="wrapper" >
				<table class="tb" border="0" cellpadding="0" cellspacing="0" align="center" height="100%" width="100%">
					<tbody>
						<tr valign="top">
							<td valign="top" height='1%'>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1" >
									<tr valign="top">
										<td width="20%" valign="top">
											<img style="width:200px;	height:50px;" src="../themes/LogoPostcode.png">
										</td>
										<td width="60%" valign="top">
											<center><h3><b>รายงานลงรับไปรษณีย์ภัณฑ์</b></h3></center>											
										</td>
										<td width="20%" valign="bottom">
											<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1" >
												<tr>
													<td class="text-right">  วันที่   :
													</td >
													<td class="text-left"> {{today}}						
													</td>
												</tr>
												
												<tr>
													<td class="text-right">  หน้า   :
													</td >
													<td class="text-left"> {{order_post.heder.page}}/{{order_post1.cpage}}						
													</td>
												</tr>
												
											</table>
										</td>
										
									</tr>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<td valign="top" height='1%'>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1" >
									<tr valign="top">
										<td width="" valign="top">
											<label><b>บริการ</b><span class="underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ({{order_post.heder.scg_cost_center_code}}) {{order_post.heder.scg_cost_center_name}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></label>											
											<br><label><b>บริษัท</b><span class="underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Pivot Co.Th, Ltd   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></label>											
											<br><label><b>ขอนำส่งของทางไปรษณีย์ตามรายการดังนี้</b></label>											
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr valign="top">
							<td style="padding:0 5px;" valign="top">
							<div class="hed" valign="top"></div>
								<table width="100%" border="1" cellpadding="0" cellspacing="0"  class="table_border">
									<tr align="center" >
										<th rowspan="2">No</th>	
										<th rowspan="2">ผู้ส่ง</th>	
										<th rowspan="2">ผู้รับ</th>										
										<th rowspan="2">เลขทะเบียนที่</th>										
										<th colspan="3">ค่าไปรษณียากร</th>										
										<th rowspan="2" width="15%">SubCC</th>										
									</tr>
									<tr align="center" >
											
																		
										<th>จำนาน</th>										
										<th>น้ำหนัก/กก</th>										
										<th>ราคารวม</th>										
																		
									</tr>
									{% for order in order_post.data %}
									<tr align="center">
										<td class="td_padding">{{order.index}}</td>	
										<td class="td_padding">{{order.scg_employee_name}}</td>	
										<td class="td_padding">{{order.scg_customers_name}}</td>		
										<td class="td_padding">{{order.ems_barcode}}</td>	
										<td class="td_padding">{{order.ems_quantity}}</td>	
										<td class="td_padding">{{order.ems_weight * order.ems_quantity}}</td>	
										<td class="td_padding">{{order.scg_cost * order.ems_quantity}}</td>	
										<td class="td_padding">{{order.scg_cost_center_code}}{% if order.scg_sub_cost_center != "" %} - {{order.scg_sub_cost_center}} {% endif %}</td>	
									</tr>
									
									{% endfor %}{% if order_post.heder.page == order_post1.cpage %}
										<tr align="center">
											<td colspan="4" align="right"><b>  <u>  รวม  </u>  </b> &nbsp;&nbsp;</td>	
										
											<td><u> &nbsp;&nbsp; {{order_post1.num}} &nbsp;&nbsp; </u>ชิ้น</td>	
											<td class="">{{order_post1.weight}}</td>	
											<td class="">{{order_post1.price}}</td>	
											<td class=""></td>	
										</tr>
									{% endif %}
								</table><br>
							</td>
						</tr>
					</tbody>
				</table>
				
				
				</div>
				<div class="footer">
				<div class="hed" style="padding:5px;"></div><br><br><br>	
					<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" class="txt_small_then">
						<tr align="center">
							<td>(ลงชื่อ)..............................................</td>
							<td>(ลงชื่อ)..............................................</td>
							<td>ได้ตรวจสอบถูกต้องและรับฝากส่งไว้แล้ว</td>
						</tr>
						<tr align="center">
							<td>ผู้ฝากส่ง</td>
							<td>ผู้เจ้าหน้าที่รับฝาก</td>
							<td>ตราประจำวัน</td>
						</tr>
						
					</table>
					<!-- <table align="center" width="80%" border="0" cellpadding="0" cellspacing="0" class="txt_small_then">
						<tr align="center">
							<td style=" font-size:9px">
								Pivot Co., Ltd. 1000 / 67-74 อาคารลิเบอร์ตี้พลาซ่า ชั้น 3 ถ.สุขุมวิท 55 แขวงคลองตันเหนือ เขตวัฒนา กรุงเทพ ฯ 10110 Tel. 0-2391-3344 Fax. 02-381-9761
							</td>
						</tr>
					</table> -->
				</div>
 {% endfor %} 
 {% endfor %} 
	 <link href="../themes/jquery/jquery-ui.css" rel="stylesheet">
			<script type="text/javascript">	
			$(document).ready(function(){
				window.print();
			});
			</script> 
	
  </body>
</html>
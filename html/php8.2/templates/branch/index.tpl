{% extends "base_emp_branch.tpl" %}

{% block title %} - List{% endblock %}

{% block menu_1 %} active {% endblock %}
{% block styleReady %}

.card_ {
   // padding-top: 20px;
  //  margin: 10px 0 20px 0;
   // background-color: rgba(214, 224, 226, 0.2);
    border-top-width: 0;
    border-bottom-width: 2px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
	min-height:100%;
}

.card_ .card_-heading {
    padding: 0 20px;
    margin: 0;
}

.card_ .card_-heading.simple {
    font-size: 20px;
    font-weight: 300;
    color: #777;
    //border-bottom: 1px solid #e5e5e5;
}

.card_ .card_-heading.image img {
    display: inline-block;
    width: 46px;
    height: 46px;
    margin-right: 15px;
    vertical-align: top;
    border: 0;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}

.card_ .card_-heading.image .card_-heading-header {
    display: inline-block;
    vertical-align: top;
}

.card_ .card_-heading.image .card_-heading-header h3 {
    margin: 0;
    font-size: 14px;
    line-height: 16px;
    color: #262626;
}

.card_ .card_-heading.image .card_-heading-header span {
    font-size: 12px;
    color: #999999;
}

.card_ .card_-body {
    padding: 0 20px;
    margin-top: 20px;
}

.card_ .card_-media {
    padding: 0 20px;
    margin: 0 -14px;
}

.card_ .card_-media img {
    max-width: 100%;
    max-height: 100%;
}

.card_ .card_-actions {
    min-height: 30px;
    padding: 0 20px 20px 20px;
    margin: 20px 0 0 0;
}

.card_ .card_-comments {
    padding: 20px;
    margin: 0;
   // background-color: #f8f8f8;
}

.card_ .card_-comments .comments-collapse-toggle {
    padding: 0;
    margin: 0 20px 12px 20px;
}

.card_ .card_-comments .comments-collapse-toggle a,
.card_ .card_-comments .comments-collapse-toggle span {
    padding-right: 5px;
    overflow: hidden;
    font-size: 12px;
    color: #999;
    text-overflow: ellipsis;
    white-space: nowrap;
}

.card_-comments .media-heading {
    font-size: 13px;
    font-weight: bold;
}

.card_.people {
    position: relative;
    display: inline-block;
    width: 170px;
    height: 300px;
    padding-top: 0;
    margin-left: 20px;
    overflow: hidden;
    vertical-align: top;
}

.card_.people:first-child {
    margin-left: 0;
}

.card_.people .card_-top {
    position: absolute;
    top: 0;
    left: 0;
    display: inline-block;
    width: 170px;
    height: 150px;
    background-color: #ffffff;
}

.card_.people .card_-top.green {
    background-color: #53a93f;
}

.card_.people .card_-top.blue {
    background-color: #427fed;
}

.card_.people .card_-info {
    position: absolute;
    top: 150px;
    display: inline-block;
    width: 100%;
    height: 101px;
    overflow: hidden;
    background: #ffffff;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card_.people .card_-info .title {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 16px;
    font-weight: bold;
    line-height: 18px;
    color: #404040;
}

.card_.people .card_-info .desc {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 12px;
    line-height: 16px;
    color: #737373;
    text-overflow: ellipsis;
}

.card_.people .card_-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    display: inline-block;
    width: 100%;
    padding: 10px 20px;
    line-height: 29px;
    text-align: center;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card_.hovercard {
    position: relative;
    padding-top: 0;
    overflow: hidden;
    text-align: center;
   // background-color: rgba(214, 224, 226, 0.2);
}

.card_.hovercard .cardheader {
    //background: url("../themes/images/Artboard4.png");
    background-size: cover;
	background-color:#0074c0;
    height: 60px;
}

.card_.hovercard .avatar {
    position: relative;
    top: -50px;
    margin-bottom: -50px;
	
}

.card_.hovercard .avatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255,255,255,0.5);
}

.card_.hovercard .info {
    padding: 4px 8px 10px;
}

.card_.hovercard .info .title {
    margin-bottom: 4px;
    font-size: 24px;
    line-height: 1;
    color: #262626;
    vertical-align: middle;
}

.card_.hovercard .info .desc {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}

.card_.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
}
.btn_login{
 background-color:#0074c0;
}.btn_login:hover{
 background-color:#055d97;
}
.btn_logouts{
	background-color:#6d6d6d;
}.btn_logouts:hover{
	background-color:#4d4d4d;
}
{% endblock %}
{% block domReady %}
$('#name_emp').select2({
				dropdownParent: $('#editCon'),
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			$.ajax({
			  dataType: "json",
			  method: "POST",
			  url: "ajax/ajax_getdataemployee.php",
			  data: { employee_id : $('#name_emp').val() }
			}).done(function(data1){
				//alert('55555');
				$('#con_emp_tel').val(data1['mr_emp_tel']);
				$('#mr_emp_code').val(data1['mr_emp_code']);
				$('#con_emp_name').val(data1['mr_emp_name']+'  '+data1['mr_emp_lastname']);
				//console.log(data1);
				//console.log($('#name_emp').val());
			});
			
		});
		
$( "#btn_save" ).click(function() {
					var con_remark 			= $('#con_remark').val();
					var con_floor 			= $('#con_floor').val();
					var con_department_name 			= $('#con_department_name').val();
					var mr_emp_tel 			= $('#con_emp_tel').val();
					var con_emp_name 		= $('#con_emp_name').val();
					var mr_emp_code 		= $('#mr_emp_code').val();
					var mr_emp_id 			= $('#name_emp').val();
					var department_code 		= $('#department_code').val();
					var mr_contact_id 		= $('#mr_contact_id').val();
					var type_add 		= $('#type_add').val();
					
					$( "#mr_emp_tel" ).removeClass( "is-invalid" );
					$( "#mr_emp_code" ).removeClass( "is-invalid" );
					$( "#name_emp" ).removeClass( "is-invalid" );
					
					if(mr_emp_id == ''){
						$( "#name_emp" ).addClass( "is-invalid" );
						alertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
						return;
					}if(mr_emp_code == ''){
						$( "#mr_emp_code" ).addClass( "is-invalid" );
						alertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
						return;
					}if(mr_emp_tel == ''){
						$( "#mr_emp_tel" ).addClass( "is-invalid" );
						alertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
						return;
					}
					
	alertify.confirm("ตรวจสอบข้อมูล", "กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก",
			function(){
				$.ajax({
						  dataType: "json",
						  method: "POST",
						  url: "ajax/ajax_save_data_contact.php",
						  data: { 
								department_code 			 	 : department_code 			 	,
								con_emp_name 			 	 : con_emp_name 			 	,
								con_remark 			 	 : con_remark 			 	,
								con_floor 			 	 : con_floor 			 	,
								con_department_name 	 : con_department_name 	,
								type_add 	 : type_add 	,
								mr_emp_tel 	 : mr_emp_tel 	,
								mr_emp_code  : mr_emp_code ,
								mr_emp_id 	 : mr_emp_id 	,
								mr_contact_id: mr_contact_id
						  }
						}).done(function(data) {
							
							alertify.alert(data['st'],data['msg'],
							function(){
								window.location.reload();
							});
						});
		  },function(){
					alertify.error('Cancel');
	  });
	
	
	//alert( "Handler for .click() called." );
	//is-invalid
});
{% endblock %}

{% block javaScript %}

function load_modal(mr_contact_id,type) {	

 $('#editCon').modal('show');

$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_save_data_contact.php",
	  data: { 	
				mr_contact_id : mr_contact_id,
				page : 'getdata'
	  }
	}).done(function(data) {
		//alert('55555');
		$('#con_emp_tel').val(data['emp_tel']);
		if(type!= "add"){
			$('#mr_contact_id').val(data['mr_contact_id']);
			
		}
		$('#type_add').val(type);
		$('#mr_emp_code').val(data['emp_code']);
		$('#con_department_name').val(data['department_name']);
		$('#con_floor').val(data['floor']);
		$('#con_remark').val(data['remark']);
		$('#department_code').val(data['department_code']);
	});
	
}


{% endblock %}


		
		
{% block Content %} 

	<div class="row">
		<div class="col-lg-12 col-sm-12">

            <div class="card_ hovercard">
                <div class="cardheader">

                </div>
                <div class="avatar">
                    <img alt="" src="../themes/images/Asset1.png">
                </div>
                <div class="info">
				<br>
				<br>
                    <div class="title">
                        <h3 style="color:#0074c0;">ยินดีต้อนรับ</h3>
                    </div>
                    <div class="desc"><h5>รหัส {{ user_data.mr_user_username }} :  {{emp_data.0.mr_emp_name}}  {{emp_data.0.mr_emp_lastname}}</h5></div>
					<div class="desc" style="color:red;"><b><h5><p>สถานที่ปฎิบัติงาน  : 
					 {% if role_id == 5 %} 
					 	" สาขา/สาขาและอาคารอื่นๆ"  
					 {% else %} 
					 	"สำนักงาน(พหลโยธิน)"  
					 {% endif %}</p></h5></b></div>
					
				  <!--  
					<div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div>
                    <div class="desc"><p>Curious dous developerCurious developerCurious developerCurious developer</p></div> 
					-->
                    <div class="desc"><h5>สาขา :  {{emp_data.0.mr_branch_code}}-{{emp_data.0.mr_branch_name}}</b></h5></div>
						 <div class="desc"><h5>ชั้น : {{emp_data.0.mr_branch_floor}}</h5></div>
						 <div class="desc"><h5>เบอร์โทรศัพท์ : {{emp_data.0.mr_emp_tel}}</h5></div>
						 <div class="desc"><h5>เบอร์มือถือ :  {{emp_data.0.mr_emp_mobile}}</h5></div>
						 <div class="desc"><h5>Email :  {{emp_data.0.mr_emp_email }}</h5></div>
						
						 <div class="desc"><h5><b>
									<a class="btn btn-link" style="text-decoration: underline;" href="profile.php">
										แก้ไขข้อมูลส่วนตัว
									</a></b></h5></div>
						{% if con_data|length > 0 %}
						<hr>
						<h4>ข้อมูลหน่วยงานที่อยู่ในความรับผิดชอบรับเอกสาร</h4><br>
							
							<table class="table table-striped" align="center" border="1">
									<tr>
										<th  class="text-center">#</th>
										<th  class="text-center">พนักงาน</th>
										<th  class="text-center">รหัสหน่วยงาน</th>
										<th  class="text-center">ชื่อหน่วยงาน</th>
										<th  class="text-center">จัดการ</th>
									</tr>
								
								{% for c in con_data %}
									<tr>
										<td>{{ loop.index }}</td>
										<td>{{ c.emp_code }}  {{ c.mr_contact_name }}</td>
										<td>{{ c.department_code }}</td>
										<td>
										   {{         c.department_name  }} [{{         c.remark  }}]   
										</td>
										<td>
											<button {% if c.emp_code  != emp_data.0.mr_emp_code %} disabled {% endif %}
												onclick="load_modal('{{c.mr_contact_id}}',null);" class="btn btn-link" style="text-decoration: underline;">
												แก้ไขข้อมูล <i class="material-icons">create</i>
											</button>
											<button type="button" class="btn btn-link"
													onclick="load_modal('{{c.mr_contact_id}}','add');">
												เพิ่ม <i class="material-icons">add_circle</i>
											</button>
										</td>
									</tr>
								
							{% endfor %}
							</table>
						{% endif %}
                </div>
               
            </div>

        </div>
		

	</div>
	<div class="row justify-content-center justify-content-sm-center justify-content-md-center	justify-content-lg-center justify-content-xl-center">
		<!-- .col-	.col-sm-	.col-md-	.col-lg-	.col-xl- -->
		<div class="col-9 col-sm-6 col-md-5 col-lg-5 col-xl-4">
				 
						
						

						
						
			</div>
			<div class="col-lg-12 col-sm-12">
				<div class="bottom">
					{# <a class="btn_logouts btn btn-primary" rel="" href="https://plus.google.com/shahnuralam">
							ออกจากระบบ
					</a>#}
				 </div>
			</div>
	</div>
	


<!-- Modal -->
<div class="modal fade" id="editCon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div class="form-group">
				<label for="">ชื่อ - นามสกุล</label><br>
				 <select class="form-control" id="name_emp" style="width:100%;">
				  <option value="">{{con_data.0.emp_code}} : {{con_data.0.mr_contact_name}}</option>
				  {% for emp in all_emp %}
				  <option value="{{ emp.mr_emp_id }}">{{emp.mr_emp_code}} : {{emp.mr_emp_name}}{{emp.mr_emp_lastname}}</option>
				  {% endfor %}
				</select>
			  </div>
			 
			  
			  <div class="form-group">
				<label for="">เบอร์โทร</label>
				<input type="text" class="form-control" id="con_emp_tel" value="">
				<input type="hidden" class="form-control" id="type_add" value="">
				<input type="hidden" class="form-control" id="mr_contact_id" value="">
				<input type="hidden" class="form-control" id="mr_emp_code" value="">
				<input type="hidden" class="form-control" id="con_emp_name" value="">
			  </div>
			  <div class="form-group">
				<label for="">รหัสหน่วยงาน</label>
				<input type="text" class="form-control" id="department_code" value="" readonly>
			  </div>
			  <div class="form-group">
				<label for="">ชื่อหน่วยงาน</label>
				<input type="text" class="form-control" id="con_department_name" value="" readonly>
			  </div>
			  <div class="form-group">
				<label for="">ชั้น</label>
				<input type="text" class="form-control" id="con_floor" value=""  readonly>
			  </div>
			  <div class="form-group">
				<label for="">หมายเหตุ</label>
				 <textarea class="form-control" id="con_remark" rows="3"></textarea >
			  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
        <button type="button" id="btn_save" class="btn btn-primary">บันทึก</button>
      </div>
    </div>
  </div>
</div>
	
{% endblock %}
{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

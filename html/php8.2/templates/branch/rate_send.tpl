{% extends "base_emp2.tpl" %}

{% block title %} - List{% endblock %}
{% block styleReady %}
    
	#img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
		z-index: 1050;
    }
	#pic_loading {
        width: 600px;
        height: auto;
    }
	
	
	#rating_mess_0{
		margin: auto;
		width: 50%;
		
	}	
	
	#rating_mess_5{
		margin: auto;
		width: 50%;
	
	}	
	

{% endblock %}

{% block scriptImport %}

	
{% endblock %}




{% block domReady %}

{{alert}}

$('#rating_mess_0').on('click', function() {
			
			var send_id 					= $("#send_id").val();
			var remark 						= $("#remark").val();
			var rating_mess 				= 0;

			
		//alert(remark);
           if ( remark == "" ) {
				alert("โปรดกรอกคำแนะนำเพิ่มเติม");
				return;
		   }else{
				$.ajax({
					url: '../messenger/ajax/ajax_rate_send.php',
					type: 'POST',
					dataType: "json",
					data: {
						id			: send_id,
						rating_mess : rating_mess,
						remark		: remark,
					},
					cache: false,
						beforeSend: function() {
							$('#img_loading').show();
						},
					success: function(res){
						if(res.status == 200) {
							window.location.href = "../branch/receive_work.php";
						}else{
							alertify.alert('แจ้งเตือน',res.message); 
						}
					},
					complete: function() {
						
					}
				})
			}				
                
        });
			
			
   $('#rating_mess_5').on('click', function() {
			
			var send_id 					= $("#send_id").val();
			var remark 						= $("#remark").val();
			var rating_mess 				= 5;
			
			
			//alert(rating_mess);
           
				 $.ajax({
					url: '../messenger/ajax/ajax_rate_send.php',
					type: 'POST',
					dataType: "json",
					data: {
						id			: send_id,
						rating_mess : rating_mess,
						remark		: remark,
					},
					cache: false,
						beforeSend: function() {
							
						},
					success: function(res){
						if(res.status == 200) {
							window.location.href = "../branch/receive_work.php";
						}else{
							alertify.alert('แจ้งเตือน',res.message); 
						}
					},
					complete: function() {
					
					}
				})
							
                
            });
        
	
{% endblock %}



{% block Content %}

		<div class="page-header">
             <h5> คุณพอใจต่อการบริการของเราในครั้งนี้หรือไม่</h5>
        </div>
		<div class="card">
			<div class="card-body space-height">
				<input type="hidden" id="send_id" value="{{ send_id }}">
				<h5>คำแนะนำเพิ่มเติม</h5>
				<textarea style="height:50px;width:345px;margin-bottom:15px" id="remark" value=""></textarea>	

				<br>
				<div class="row">
					
					<div class="col-6" align="center" >
						<button type="button" class="btn btn-outline-success" id="rating_mess_5" value="5">
						<img src="../themes/images/smiling.png" height="42" width="42" />
						</button>
						<br>
						
						<h5 style="margin-top:7px;">พอใจ</h5>
					</div>
					
					
					<div class="col-6" align="center">
						<button type="button" class="btn btn-outline-warning" id="rating_mess_0" value="0">
						<img src="../themes/images/sad.png" height="42" width="42" />
						</button>
						<br>
						<h5 style="margin-top:7px;">ไม่พอใจ</h5>
						
					</div>
				</div>
				
			</div>
		</div>

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp_branch.tpl" %}

{% block title %} - List{% endblock %}

{% block menu_2 %} active {% endblock %}

{% block styleReady %}
#btn_update:hover,
#btn_cancel:hover,
#btn_back:hover {
    color: #FFFFFF;
    background-color: #055d97;
}


#btn_update,
#btn_cancel,
#btn_back {
    border-color: #0074c0;
    color: #FFFFFF;
    background-color: #0074c0;
}

#detail_sender_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_sender_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 20px;
}

#workNumber {
    color: #006cb7;
}

#detail_receiver_head h4{
    text-align:left;
    color: #006cb7;
    border-bottom: 3px solid #006cb7;
    display: inline;
}

#detail_receiver_head {
    border-bottom: 3px solid #eee;
    margin-bottom: 20px;
    margin-top: 40px;
}
.divTimes{
	position: absolute;
    top: -30px;
	width: 100%;
    //right: -20px;
	//transform: translateX(-5px) rotateZ(-45deg);
}

.coloe_compress:before{
	color:#fff;
	background-color:#2196F3 !important ;
	
}
.multi-steps > li.is-active:before, .multi-steps > li.is-active ~ li:before {
  content: counter(stepNum);
  font-family: inherit;
  font-weight: 700;
}
.multi-steps > li.is-active:after, .multi-steps > li.is-active ~ li:after {
  background-color: #ededed;
}

.multi-steps {
  display: table;
  table-layout: fixed;
  width: 100%;
}
.multi-steps > li {
  counter-increment: stepNum;
  text-align: center;
  display: table-cell;
  position: relative;
  color: #2196F3;
}
.multi-steps > li:before {
  content: counter(stepNum);
  display: block;
  margin: 0 auto 4px;
  background-color: #fff;
  width: 36px;
  height: 36px;
  line-height: 32px;
  text-align: center;
  font-weight: bold;
  border-width: 2px;
  border-style: solid;
  border-color: #2196F3;
  border-radius: 50%;
}
.multi-steps > li:after {
  content: '';
  height: 2px;
  width: 100%;
  background-color: #2196F3 ;
  position: absolute;
  top: 16px;
  left: 50%;
  z-index: -1;
}
.multi-steps > li:last-child:after {
  display: none;
}
.multi-steps > li.is-active:before {
  background-color: #fff;
  border-color: #2196F3 ;
}
.multi-steps > li.is-active ~ li {
  color: #808080;
}
.multi-steps > li.is-active ~ li:before {
  background-color: #ededed;
  border-color: #ededed;
}

{% endblock %}

{% block domReady %}
moment.locale('th');

var init = { id: '{{ data.receive_id }}', text: '{{ data.receive_name }}' };
var init_replace = { id: '{{ data.real_receive_id }}', text: '{{ data.real_receive_name }}' };
{% if data.mr_type_work_id == 2 %}
var type_work = '2';
{% else %}
var type_work = '1';
{% endif %}

var mr_branch_floor = '{{ data.mr_branch_floor }}';
var real_branch_floor = '{{ data.mr_branch_floor }}';

var branch_id = '{{ data.receive_branch_id }}';
var real_branch_id = '{{ data.receive_branch_id3 }}';
{% if data.mr_floor_id=='' %}
	var floor_id  = '{{ data.receive_floor_id }}';
{% else %}
	var floor_id  = '{{ data.mr_floor_id }}';
{% endif %}
var receiver_id = '{{ data.receive_id }}';

var real_receive_id = '{{ data.real_receive_id }}'
var real_receiver_name = '{{ data.real_receive_name }}';

var receiver_name = '{{ data.receive_name }}';
var status = '{{ data.mr_status_id }}';

// console.log(d.days()+" วัน "+ d.hours()+" ชม. "+d.minutes()+" นาที "+ d.seconds() + " วินาที");

// initial Page
initialPage(status);
setForm(init);
setFormReplace(init_replace);
check_type_send(parseInt(type_work));


//console.log(hoursArr);
//calculateDuration(timeArr,hoursArr);


if (!!branch_id) {
    $('#branch_send').val(parseInt(branch_id)).trigger('change');
}

if (!!real_branch_id) {
    $('#real_branch_send').val(parseInt(real_branch_id)).trigger('change');
}

if (!!floor_id) {
    $('#floor_send').val(parseInt(branch_id)).trigger('change');
}
if (!!mr_branch_floor) {
    $('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}

if (!!real_branch_floor) {
    $('#mr_branch_floor').val(parseInt(mr_branch_floor)).trigger('change');
}


$(".alert").alert();

$('#name_receiver_select').select2({
    placeholder: "ค้นหาผู้รับ",
   
    ajax: {
        url: "ajax/ajax_getdataemployee_select.php",
        dataType: "json",
        delay: 250,
        processResults: function (data) {
            return {
                results : data
            };
        },
        cache: true
    },
    
}).on('select2:select', function(e) {
    //console.log(e.params.data)
    setForm(e.params.data);
});

var frmValid = $('#frmBranch').validate({
    onsubmit: false,
    onkeyup: false,
    errorClass: "is-invalid",
    highlight: function (element) {
        if (element.type == "radio" || element.type == "checkbox") {
            $(element).removeClass('is-invalid')
        } else {
            $(element).addClass('is-invalid')
        }
    },
    rules: {
        telSender: {
            required: false
        },
        type_send: {
            required: true
        },
        nameReceiver: {
            required: true
        },
        telReceiver: {
            required: false
        },
        floorRealReceiver: {
            required: {
                depends: function (elm) {
                    var f = $('#floor_name').val();
                    var type = $('input[name="type_send"]:checked').val();
                    if( (f == " - " || f == "") && type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        branchRealReceiver: {
            required: {
                depends: function (elm) {
                    var b = $('#branch_receiver').val();
                    var type = $('input[name="type_send"]:checked').val();
                    if((b == " - " || b == "") && type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topic: {
            required: {
                depends: function (elm) {
                    var type = $('input[name="type_send"]:checked').val();
                    if(type == 1){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
        topicBranch: {
            required: {
                depends: function (elm) {
                    var type = $('input[name="type_send"]:checked').val();
                    if(type == 2){
                        return true
                    } else {
                        return false
                    }
                }
            }
        },
    },
    messages: {
        telSender: {
            required: 'ระบุเบอร์ติดต่อ'
        },
        type_send: {
            required: 'ระบุประเภทผู้รับ'
        },
        nameReceiver: {
            required: 'กรุณาเลือกผู้รับ'
        },
        telReceiver: {
            required: 'ระบุเบอร์ติดต่อผู้รับ'
        },
        branchRealReceiver: {
            required: 'ระบุสาขาผู้รับ'
        },
        topic: {
            required: 'ระบุชื่อเอกสาร'
        },
        topicBranch: {
            required: 'ระบุชื่อเอกสาร'
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});

$('#btn_update').click(function() {
     var _frm = $('#frmBranch');
    if($('#frmBranch').valid()) {
       
        $.ajax({
            url: _frm.attr('action'),
            type: 'POST',
            data: {
                form: _frm.serializeArray(),
                actions: 'update',
                wid: '{{ wid }}'
            },
            dataType: 'json',
            success: function(res) {
                if(res.status == "success") {
                    alertify.alert(res['messeges'], function() {
                        location.reload();
                    });
                }
            }
        })
    }
});


$("#btn_cancel").click(function(){
    alertify.confirm('ยืนยันจะยกเลิกรายการนำส่งนี้', function() {
        $.post('./ajax/ajax_updateWorkInfo.php', { actions: 'cancel', wid: '{{ wid }}'}, function(res){
            if(res.status == "success") {
                        alertify.alert(res['messeges'], function() {
                            location.reload();
                        });
                    }
        }, 'json');
    })
   
});


{% endblock %}



{% block javaScript %}


// $('#type_send_1').prop('checked',true);

function check_type_send( type_send ){
    if( type_send == 1 ){
        $("#type_1").show();
        $("#type_2").hide();

        $('#type_send_1').prop('checked', true);
        $('#type_send_2').prop('checked', false);
    }else{
        $("#type_1").hide();
        $("#type_2").show();
        
        $('#type_send_2').prop('checked', true);
        $('#type_send_1').prop('checked', false);
    }
}

function check_name_re() {
    var pass_emp = $("#pass_emp").val();
    if(pass_emp == "" || pass_emp == null){
        $("#div_re_text").hide();
        $("#div_re_select").show();
    }else{
        $("#div_re_text").show();
        $("#div_re_select").hide();
    }
}

function setForm(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    $.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            $("#depart_receiver").val(res['department']);
            $("#emp_id").val(res['mr_emp_id']);
            $("#tel_receiver").val(res['mr_emp_tel']);
            $("#place_receiver").val(res['mr_workarea']);
            $("#floor_name").val(res['mr_department_floor']);
            $("#branch_receiver").val(res['branch']);
        }
    })
}

function setFormReplace(data) {
    var emp_id = parseInt(data.id);
    var fullname = data.text;
    $.ajax({
        url: 'ajax/ajax_autocompress_name.php',
        type: 'POST',
        data: {
            name_receiver_select: emp_id
        },
        dataType: 'json',
        success: function (res) {
            console.log(res);
            // $("#depart_receiver").val(res['department']);
            // $("#emp_id").val(res['mr_emp_id']);
            // $("#tel_receiver").val(res['mr_emp_tel']);
            $("#real_place_receiver").val(res['mr_workarea']);
            $("#real_floor_name").val(res['mr_department_floor']);
            $("#real_branch_receiver").val(res['branch']);
        }
    })
}

var saveBranch = function (obj) {
    return $.post('ajax/ajax_save_work_branch.php', obj);
}

function initialPage(status) {
    //console.log(status)
    if(parseInt(status) != 7) {
        $('input').prop('disabled', true);
        $('select').prop('disabled', true);
        $('textarea').prop('disabled', true);
        $('#btn_update').prop('disabled', true);
        $('#btn_cancel').prop('disabled', true);
    }
}

{% endblock %}
{% block Content %}

<div class="container">



<br>
<br>
<br>

  
  
  
  





    <div class="" style="text-align: center;color:#0074c0;margin-top:20px;">
        <label>
            <h3><b>รายละเอียดรายการนำส่ง</b></h3>
        </label>
    </div>
    <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
    <input type="hidden" id="emp_id" value="">
    <input type="hidden" id="barcode" name="barcode" value="{{ barcode }}">
    
    <div>
        <p class="font-weight-bold"><span class="text-muted">เลขที่เอกสาร: </span> <span id="workNumber">{{ data.mr_work_barcode }}</span> <span class="text-muted">( {{ data.mr_status_name }} )</span></p>
    </div>
    

    <div class="form-group"  id="detail_sender_head">
        <h4>สถานะเอกสาร </h4>
    </div>

    {% if data.mr_status_id != 6 %}
    <div class="demo"><br><br><br><br>
        <ul class="list-unstyled multi-steps">
			 {% for st in status %}
							
				 {# {% if st.active != "" %} #}
					<li class="{% if end_step == loop.index %} is-active {% endif %}{% if st.active != "" %} coloe_compress {% endif %}">
						<strong>{{ st.emp_code }}</strong><br />
						<small>{{ st.name }}</small>
						<div class="divTimes">
								<small>{{st.date}}</small>
							</div>
					</li>

			{% endfor %}
			 </ul>
    </div>
    {% else %}
    <div style="text-align:center;">
        <p class="text-muted">
            <i class="material-icons">block</i>
            <br/>
            <strong>ยกเลิกรายการนำส่ง</strong>
        </p>
    </div>
	<br>
	<br>
    {% endif %}
        
     <!-- <div class='clearfix'></div> -->
    
     <form action="./ajax/ajax_updateWorkInfo.php" method="POST" id="frmBranch">
        <div class="form-group" style="" id="detail_sender_head">
            <h4>รายละเอียดผู้ส่ง </h4>
        </div>
        <div class="form-group" id="div_re_text">
            <div class="row">
                <div class="col-1" style="padding-right:0px">
                    สาขาผู้ส่ง :
                </div>
                <div class="col-5" style="padding-left:0px">
                    <input type="text" class="form-control form-control-sm" name="branchSender" id="brach_sender" placeholder="ชื่อสาขา" value="{{ user_data_show.mr_branch_code }} - {{ user_data_show.mr_branch_name }}"
                        readonly>
                </div>
                <div class="col-1" style="padding-right:0px">
                    เบอร์โทร :
                </div>
                <div class="col-5" style="padding-left:0px">
                    <input type="text" class="form-control form-control-sm" name="telSender" id="name_sender_tel" placeholder="เบอร์โทร" value="{{  user_data_show.mr_emp_tel  }}" readonly>
                </div>
            </div>
        </div>

        <div class="form-group" id="div_re_select">
            <div class="row">
                <div class="col-1" style="padding-right:0px">
                    ชื่อผู้ส่ง :
                </div>
                <div class="col-5" style="padding-left:0px">
                    <input type="text" class="form-control form-control-sm" name="nameSender" id="name_sender" value="{{  user_data_show.mr_emp_code  }} - {{  user_data_show.mr_emp_name  }} {{  user_data_show.mr_emp_lastname  }}"
                        readonly>
                </div>
                <div class="col-1" style="padding-right:0px">

                </div>
                <div class="col-5" style="padding-left:0px">

                </div>
            </div>
        </div>

        <div class="form-group" style="" id="detail_receiver_head">
            <h4>รายละเอียดผู้รับ</h4>
        </div>
        <div class="form-group" id="div_re_text">
            <div class="row">
                <div class="col-1" style="padding-right:0px">
                    ค้นหาผู้รับ :
                </div>

                <div class="col-2" style="padding-left:0px">
                    <div class="input-group">
                        <label class="btn btn-primary">
                            <input type="radio" aria-label="Radio button for following text input" name="type_send" id="type_send_1"
                                value="1" onclick="check_type_send(this.value);"> &nbsp;
                            ส่งสำนักงานใหญ่
                        </label>
                    </div>
                </div>
                <div class="col-2" style="padding-left:0px">
                    <div class="input-group">
                        <label class="btn btn-primary">
                            <input type="radio" aria-label="Radio button for following text input" name="type_send" id="type_send_2"
                                value="2" onclick="check_type_send(this.value);"> &nbsp;
                            ส่งสาขา
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group" id="div_re_text">
            <div class="row">
                <div class="col-1" style="padding-right:0px">
                    ค้นหาผู้รับ :
                </div>
                <div class="col-5" style="padding-left:0px">
                    <select class="form-control-lg" name="nameReceiver" id="name_receiver_select" style="width:100%;" data-error="#err_receiver_select">
                        {% if data.receive_id != "" %}
                        <option value="{{ data.receive_id }}">{{ data.receive_code }} - {{ data.receive_name }}</option>
                        {% endif %}
                    </select>
                    <span class="box_error" id="err_receiver_select"></span>
                </div>
                <div class="col-1" style="padding-right:0px">
                    เบอร์โทร :
                </div>
                <div class="col-5" style="padding-left:0px">
                    <input type="text" class="form-control form-control-sm" name="telReceiver" id="tel_receiver" value="{{ data.receive_tel }}"
                        placeholder="เบอร์โทร" readonly>
                </div>
            </div>
        </div>

        <div id="type_1" style="display:;">
            <div class="form-group" id="div_re_select">
                <div class="row">
                    <div class="col-1" style="padding-right:0px">
                        แผนก :
                    </div>

                    <div class="col-5" style="padding-left:0px">
                        <input type="text" class="form-control form-control-sm" name="departReceiver" id="depart_receiver" placeholder="ชื่อแผนก"
                            readonly>
                    </div>
                    <div class="col-1" style="padding-right:0px">
                        ชั้น :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <input type="text" class="form-control form-control-sm" name="floorReceiverName" id="floor_name" placeholder="ชั้น" readonly>
                    </div>
                </div>

            </div>

            <div class="form-group" id="div_re_select">
                <div class="row">
                    <div class="col-1" style="padding-right:0px">
                        ชั้นผู้รับ :
                    </div>

                    <div class="col-5" style="padding-left:0px">
                        <select class="form-control-lg" name="floorRealReceiver" id="floor_send" style="width:100%;" data-error="#err_floor_send">
                            <option value=""> เลือกชั้นปลายทาง</option>
                            {% for s in floor_data %}
                            <option value="{{ s.mr_floor_id }}"> {{ s.name }}</option>
                            {% endfor %}
                        </select>
                        <span class="box_error" id="err_floor_send"></span>
                    </div>
                    <div class="col-1" style="padding-right:0px">
                        ชื่อเอกสาร :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <input type="text" class="form-control form-control-sm" name="topic" id="topic_head" placeholder="ชื่อเอกสาร" value="{{ data.mr_topic }}">
                    </div>
                </div>

            </div>


            <div class="form-group" id="div_re_select">
                <div class="row">
                    <div class="col-1" style="padding-right:0px">
                        หมายเหตุ :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <textarea class="form-control form-control-sm" name="remark" id="remark_head" placeholder="หมายเหตุ">{{ data.mr_work_remark }}</textarea>
                    </div>
                    <div class="col-1" style="padding-left:0px">

                    </div>
                    <div class="col-5" style="padding-left:0px">

                    </div>
                </div>
            </div>
        </div> <!-- end type 1-->

        <div id="type_2" style="display:none;">

            <div class="form-group" id="div_re_select">
                <div class="row">
                    <div class="col-1" style="padding-right:0px">
                        สาขา :
                    </div>
                    <div class="col-5" style="padding-left:0px">

                        <input type="text" class="form-control form-control-sm" name="branchReceiver" id="branch_receiver" placeholder="ชื่อสาขา"
                            readonly>
                    </div>
                    <div class="col-1" style="padding-right:0px">
                        สถานที่อยู่ :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <input type="text" class="form-control form-control-sm" name="placeReceiver" id="place_receiver" placeholder="สถานที่อยู่"
                            readonly>
                    </div>
                </div>

            </div>


            <div class="form-group" id="div_re_select">
                <div class="row">
                    <div class="col-1" style="padding-right:0px">
                        สาขาผู้รับ :
                    </div>

                    <div class="col-5" style="padding-left:0px">
                        <select class="form-control-lg" name="branchRealReceiver" id="branch_send" style="width:100%;" data-error="#err_branch_send">
                            <option value=""> เลือกสาขาปลายทาง</option>
                            {% for b in branch_data %}
                            <option value="{{ b.mr_branch_id }}"> {{ b.branch }}</option>
                            {% endfor %}
                        </select>
                        <span class="box_error" id="err_branch_send"></span>
                    </div>
					
                    <div class="col-1" style="padding-right:0px">
                        ชื่อเอกสาร :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <input type="text" class="form-control form-control-sm" name="topicBranch" id="topic_branch" value="{{ data.mr_topic }}"
                            placeholder="ชื่อเอกสาร">
                    </div>
                </div>

            </div>


            <div class="form-group" id="div_re_select">
                <div class="row">
				<div class="col-1" style="padding-right:0px">
							ชั้นผู้รับ :
						</div>		
						<div class="col-5" style="padding-left:0px">
							<select class="form-control-lg" id="mr_branch_floor" style="width:100%;" name="mr_branch_floor">
									{% if data.mr_branch_floor == 1 %}
                                        <option value="1" selected>ชั้น 1</option>			
										<option value="2" >ชั้น 2</option>			
										<option value="3" >ชั้น 3</option>	                           
                                    {% elseif data.mr_branch_floor == 2 %}
                                        <option value="1" >ชั้น 1</option>			
										<option value="2" selected>ชั้น 2</option>			
										<option value="3" >ชั้น 3</option>			
                                    {% else %}
                                        <option value="1" >ชั้น 1</option>			
										<option value="2" >ชั้น 2</option>			
										<option value="3" selected>ชั้น 3</option>	
                                    {% endif %}	
							</select>
						</div>
                    <div class="col-1" style="padding-right:0px">
                        หมายเหตุ :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <textarea class="form-control form-control-sm" name="remarkBranch" id="remark_branch" placeholder="หมายเหตุ">{{ data.mr_work_remark }}</textarea>
                    </div>
                    <div class="col-1" style="padding-left:0px">

                    </div>
                    <div class="col-5" style="padding-left:0px">

                    </div>
                </div>
            </div>
        </div> 
		<!-- end type 2-->

        {% if received == "replace" %}
        <div class="form-group" style="" id="detail_receiver_head">
            <h4>รายละเอียดผู้รับ (แทน)</h4>
        </div>
         <div class="form-group" id="div_re_text">
            <div class="row">
                <div class="col-1" style="padding-right:0px">
                    ค้นหาผู้รับ :
                </div>
                <div class="col-5" style="padding-left:0px">
                    <select class="form-control-lg" name="nameReceiver" id="name_receiver_select" style="width:100%;" data-error="#err_receiver_select">
                        {% if data.receive_id != "" %}
                        <option value="{{ data.real_receive_id }}">{{ data.real_receive_code }} - {{ data.real_receive_name }}</option>
                        {% endif %}
                    </select>
                    <span class="box_error" id="err_receiver_select"></span>
                </div>
                <div class="col-1" style="padding-right:0px">
                    เบอร์โทร :
                </div>
                <div class="col-5" style="padding-left:0px">
                    <input type="text" class="form-control form-control-sm" name="telReceiver" id="tel_receiver" value="{{ data.real_receive_tel }}"
                        placeholder="เบอร์โทร" readonly>
                </div>
            </div>
        </div>
         <div id="type_2" >

            <div class="form-group" id="div_re_select">
                <div class="row">
                    <div class="col-1" style="padding-right:0px">
                        สาขา :
                    </div>
                    <div class="col-5" style="padding-left:0px">

                        <input type="text" class="form-control form-control-sm" name="branchReceiver" id="real_branch_receiver" placeholder="ชื่อสาขา"
                            readonly>
                    </div>
                    <div class="col-1" style="padding-right:0px">
                        สถานที่อยู่ :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <input type="text" class="form-control form-control-sm" name="placeReceiver" id="real_place_receiver" placeholder="สถานที่อยู่"
                            readonly>
                    </div>
                </div>

            </div>


            <div class="form-group" id="div_re_select">
                <div class="row">
                    <div class="col-1" style="padding-right:0px">
                        สาขาผู้รับ :
                    </div>

                    <div class="col-5" style="padding-left:0px">
                        <select class="form-control-lg" name="real_branch_send" id="real_branch_send" style="width:100%;" data-error="#err_branch_send">
                            <option value=""> เลือกสาขาปลายทาง</option>
                            {% for b in branch_data %}
                            <option value="{{ b.mr_branch_id }}"> {{ b.branch }}</option>
                            {% endfor %}
                        </select>
                        <span class="box_error" id="err_branch_send"></span>
                    </div>
					
                    <!--<div class="col-1" style="padding-right:0px">
                        ชื่อเอกสาร :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <input type="text" class="form-control form-control-sm" name="topicBranch" id="topic_branch" value="{{ data.mr_topic }}"
                            placeholder="ชื่อเอกสาร">
                    </div> -->
                </div>

            </div>


            <div class="form-group" id="div_re_select">
                <div class="row">
				<div class="col-1" style="padding-right:0px">
							ชั้นผู้รับ :
						</div>		
						<div class="col-5" style="padding-left:0px">
							<select class="form-control-lg" id="real_branch_floor" style="width:100%;" name="mr_branch_floor">
                                    {% if data.mr_branch_floor == 1 %}
                                        <option value="1" selected>ชั้น 1</option>			
										<option value="2" >ชั้น 2</option>			
										<option value="3" >ชั้น 3</option>	                           
                                    {% elseif data.mr_branch_floor == 2 %}
                                        <option value="1" >ชั้น 1</option>			
										<option value="2" selected>ชั้น 2</option>			
										<option value="3" >ชั้น 3</option>			
                                    {% else %}
                                        <option value="1" >ชั้น 1</option>			
										<option value="2" >ชั้น 2</option>			
										<option value="3" selected>ชั้น 3</option>	
                                    {% endif %}
												
							</select>
						</div>
                    <!-- <div class="col-1" style="padding-right:0px">
                        หมายเหตุ :
                    </div>
                    <div class="col-5" style="padding-left:0px">
                        <textarea class="form-control form-control-sm" name="remarkBranch" id="remark_branch" placeholder="หมายเหตุ">{{ data.mr_work_remark }}</textarea> 
                    </div>-->
                    <div class="col-1" style="padding-left:0px">

                    </div>
                    <div class="col-5" style="padding-left:0px">

                    </div>
                </div>
            </div>
        </div>
        {% endif %}

        <hr/>
        <div class="form-group" style="text-align:center;">
            <button type="button" class="btn btn-primary" id="btn_update">บันทึกรายการ</button>
            <button type="button" class="btn btn-primary" id="btn_cancel">ยกเลิกรายการนำส่ง</button>
            <a href="search_work.php" class="btn btn-primary" id="btn_back">กลับ</a>
        </div>
     </form>
</div>
{% endblock %}


{% block debug %}

{% if debug != '' %}
<pre>{{ debug }}</pre>
{% endif %}

{% endblock %}

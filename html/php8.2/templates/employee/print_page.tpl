{% extends "base.tpl" %}

{% block title %}' - รายการค้นหาข้อมูล{% endblock %}

{% block menu_1 %}
  <div id="menu_1_active">
    <div id="txtMenu">
      <a href="../admin/list.php"><font class="menuHead_active">แจ้งเตือน</font></a>
    </div>         
  </div>
{% endblock %}

{% block styleReady %}
	
	.btn_bg{
		padding:60px;
	
	}
{% endblock %}


 {% block cssImport %}
	
 {% endblock %} 
 
  {% block scriptImport %}
	<script src="js/order.js"></script>
	<script src="js/save_data_order.js"></script>
	
	

  {% endblock %}     

{% block topContent %}
	
{% endblock %}

{% block domReady %}
$('#exampleModal').modal()
 
{% endblock %} 

{% block javaScript %}
 function printDiv(divName) {
				 var printContents = document.getElementById(divName).innerHTML;
				 var originalContents = document.body.innerHTML;

				 document.body.innerHTML = printContents;

				 window.print();

				 document.body.innerHTML = originalContents;
			}
 	 
{% endblock %}  
{% block Content %}

		<center>
			<div class="page_print" id = "print"><br>
			{% for d in work_order %}
			<div align="center"><h3><b>MAILROOM SERVICES</b></h3></div><br>
				<table width="800" border="0" cellpadding="0" cellspacing="0" align="center">
					<tbody>
						<tr>
							<td width="100%" height="100px" valign="top">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1" >
									<tr>
										<td width="40%" >
											<img align="left" src="../themes/LogoPivot2.png" width="30%px">
										</td>
										<td>
											<img align="right" src="barcode.php?barcode={{ d.barcode }}&amp;width=350&amp;height=60" align="absmiddle">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:0 5px;">
								<table width="50%" border="0" cellpadding="0" cellspacing="0" class="tableborder1" >
									<tr>
										<td>
											<div align="left"><b>ที่อยุ่ผู้ส่ง</b></div>
											<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1 txt_small_then">
											<tbody>
												<tr valign="top"  style=" font-size:20px">
													<td><b>คุณ</b> </td><td>{{d.name}}  {{d.surname}}</td>
												</tr>
												<tr>
													<td>อาคาร</td><td>{{d.building_name}} ชั้น  {{d.address_floor}} แผนก  {{d.dname}}</td>
												</tr>
												<tr>
													<td>โทร</td><td>{{d.telephone_number}}</td>
												</tr>
											</tbody>
											</table><div style=" font-size:11px"><br>บริษัท ปูนซิเมนต์ไทย จำกัด (มหาชน) สำนักงานใหญ่ 1<br> ถนนปูนซิเมนต์ไทย บางซื่อ กรุงเทพฯ 10800 <br>โทรศัพท์: 0-2586-3333, 0-2586-4444</div>
										</td>
										
									</tr>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="txt_small_then p1">
								<tbody>
									<tr valign="top">
										<td width="50%" height="150px">	
										</td>
										<td>
											<div align="left"><b>ที่อยุ่ผู้รับ</b></div>
											<table  border="0" cellpadding="0" cellspacing="0" class="tableborder1 txt_small_then">
											<tbody>
												<tr valign="top"  style=" font-size:20px">
													<td ><b>คุณ&nbsp;</b> </td><td>{{d.receiver_name}}  {{d.receiver_surname}}</td>
												</tr>
												<tr valign="top">
													<td></td><td> &nbsp;{{d.receiver_address}}&nbsp;&nbsp;{{d.receiver_address_floor}}</td>
												</tr>
												<tr">
													<td>โทร</td><td>{{d.telephone_number}}</td>
												</tr>
											</tbody>
											</table>
										</td>
									</tr>
								</tbody>
								</table>
								
										<center><div style=" font-size:20px;color:red;padding:10px;">***หมายเหตุ***</div></center>
										<center><div style=" font-size:18px">{{d.remark}}</div></center><br><br><br>
									
							</td>
						</tr>
						<tr valign="top">
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="txt_small_then">
								<tbody>
	
									<tr align="center">
										<td style=" font-size:9px">Pivot Co., Ltd. 1000 / 67-74 อาคารลิเบอร์ตี้พลาซ่า ชั้น 3 ถ.สุขุมวิท 55 แขวงคลองตันเหนือ เขตวัฒนา กรุงเทพ ฯ 10110 Tel. 0-2391-3344 Fax. 02-381-9761</td>
									</tr>
								</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
				{% endfor %}
			</div>
			<div class ="btn_bg">
				
				<button TYPE="button"class="btn btn-danger" value="print"><span class = "glyphicon glyphicon-remove-circle"></span>  ยกเลิก  </button>
				<button TYPE="button"class="btn btn-success" onClick="printDiv('print');" value="print"><span class = "glyphicon glyphicon-print"></span>  พิมพ์  </button>
			</div>
		</center>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="exampleModalLabel">รหัสนำส่งงาน</h4>
        </div>
        <div class="modal-body">
				{{work_order.0.barcode}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


	
{% endblock %}

{% extends "base_emp2.tpl" %}

{% block title %}' - List{% endblock %}

{% block menu_e1 %} active {% endblock %}

{% block domReady %}
	{% if type_work == ''%}
		alertify.alert('เกิดข้อผิดพลาดไม่พลข้อมูลประเภทการส่ง!!',function(){
			location.href = "../data/profile.php";
		});
		
	{% endif %}	
	
{% endblock %}				
{% block javaScript %}
	$("#next").click(function(){
		var floor 			= $("#floor").val();
		if( floor == "" || floor == null){
					alertify.alert('กรุณาใส่ชั้นของคุณให้ถูกต้อง!!');
					location.href = "../data/profile.php";
		}else{
			{% if type_work == 1 %}
				location.href = "../employee/work_in.php";
			{% elseif type_work == 2 %}
			    location.href = "../employee/work_out.php";
			{% endif %}
		}
	
	})
	
	
				
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
			<div class="form-group" style="text-align: center;">
				 <label><h4> ข้อมูลผู้สั่งงาน {% if type_work == 1 %} (ส่งที่สำนักงานใหญ่) {% else %} (ส่งที่สาขา)  {% endif %}</h4></label>
			</div>	
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="{{ user_data.mr_emp_id }}">
	
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" placeholder="รหัสพนักงาน" value="{{ user_data.mr_emp_code }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name" placeholder="ชื่อ" value="{{ user_data.mr_emp_name }}" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						นามสกุล :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="last_name" placeholder="นามสกุล" value="{{ user_data.mr_emp_lastname }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์โทรศัพท์ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel" placeholder="เบอร์โทรศัพท์" value="{{ user_data.mr_emp_tel }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						เบอร์มือถือ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="tel_mobile" placeholder="เบอร์มือถือ" value="{{ user_data.mr_emp_mobile }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						Email :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="email" placeholder="email" value="{{ user_data.mr_emp_email }}" readonly>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						แผนก :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="department" style="width:100%;" disabled>
							{% for s in department_data %}
								<option value="{{ s.mr_department_id }}" {% if s.mr_department_id == user_data.mr_department_id %} selected="selected" {% endif %} >{{ s.mr_department_code }} - {{ s.mr_department_name}}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้น :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="floor" style="width:100%;" disabled>
							<option value=""> ไม่มีข้อมูลชั้น</option>
							{% for f in floor_data %}
								<option value="{{ f.mr_floor_id }}" {% if f.mr_floor_id == user_data.floor_emp %} selected="selected" {% endif %}>{{ f.name }}</option>
							{% endfor %}					
						</select>
					</div>		
				</div>			
			</div>
			
			
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="next">Next </button>
			</div>
			
			
	
	</div>
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

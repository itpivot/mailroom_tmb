{% extends "base_emp2.tpl" %}

{% block title %}' - List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
	  }
	  #loader{
		height:100px;
		width :100px;
		display:table;
		margin: auto;
		border-radius:50%;
	  }#bg_loader{
		  position:absolute;
		  top:0px;
		  left:0px;
		  background-color:rgba(255,255,255,0.7);
		  height:100%;
		  width:100%;
	  }

{% endblock %}
{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}
		var users_login = JSON.parse('{{ user_arr }}');
	
		$('.input-daterange').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		});	
		
		var table = $('#tb_work_order').DataTable({ 
			"responsive": true,
			//"searching": false,
			"language": {
				"emptyTable": "ไม่มีข้อมูล!"
			},
			"pageLength": 10, 
			'columns': [
				{ 'data':'no' },
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'main_sys_time'},
				{ 'data':'time_mail_re'},
				{ 'data':'mr_work_date_success'},
				{ 'data':'mr_type_work_name' },
				{ 'data':'mr_status_name' },
				{ 'data':'mr_work_remark' }
			]
		});
		
		<!-- Search -->
			$("#btn_search").click(function(){
			
			var name_send_select 	= $("#name_send_select").val();
			var send_branch_id 		= $("#send_branch_id").val();

			var name_receiver_select 	= $("#name_receiver_select").val();
			var receiver_branch_id 	= $("#receiver_branch_id").val();

			var barcode 			= $("#barcode").val();
			var start_date 		= $("#start_date").val();
			var end_date 		= $("#end_date").val();

			$.ajax({
				dataType: "json",
				method: "POST",
				url: "ajax/ajax_search_work_branch_manege.php",
				data: { 	
						'name_send_select'		: name_send_select,
						'send_branch_id'		: send_branch_id,
						'name_receiver_select'	: name_receiver_select,
						'receiver_branch_id'	: receiver_branch_id,
						'start_date'			: start_date,
						'end_date'				: end_date,
						'barcode'				: barcode
				  },
				 beforeSend: function( xhr ) {
							  $('#bg_loader').show();
							  
				  }
			  }).done(function( msg ) {
					$('#bg_loader').hide();
					$('#tb_work_order').DataTable().clear().draw();
					$('#tb_work_order').DataTable().rows.add(msg['data']).draw();
				});

			});
			<!-- end Search -->

			$('#name_receiver_select').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		});
			$('#name_send_select').select2({
				placeholder: "ค้นหาผู้ส่ง",
				ajax: {
					url: "./ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		});
		
{% endblock %}


{% block javaScript %}


{% endblock %}
{% block Content2 %}

		<div class="row" border="1">
			<div class="col">
				
			</div>
			<div class="col-12">
				<div class="card">
					<h4 class="card-header">ค้นหา</h4>
					<div class="card-body">
						<form method="POST" action="excel.report_branch_manege.php" target="_blank">
							<div class="row justify-content-between">
								<div class="card col-5" style="padding:0px;margin-left:20px">
									<h5 style="padding:5px 10px;">ผู้ส่ง</h5>
									<div class="card-body">
											<div class="row ">
												<div class="col-12">
													<select class="form-control-lg" id="name_send_select" name="name_send_select" style="width:100%;" >
														<option value = "" > ค้นหาผู้ส่ง </option>
													</select>
												</div>	
												<br>
												<br>
												<div class="col-12">
													<select class="form-control-lg" id="send_branch_id" name="send_branch_id" style="width:100%;" >
														<option value = "" > ชื่อสาขาผู้ส่ง </option>
														{% for b in mr_branch %}
															<option value="{{ b.mr_branch_id }}" > {{ b.mr_branch_code }} : {{ b.mr_branch_name }}</option>
														{% endfor %} #}
													</select>
												</div>	
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_send" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_sender" placeholder="ชื่อผู้ส่ง">
												</div> -->
											</div>
											<hr>
											<div class="row ">
													<h5 style="padding:5px 10px;">ผู้รับ</h5>
												<div class="col-12">
													<select class="form-control-lg" id="name_receiver_select" name="name_receiver_select" style="width:100%;" >
														<option value =""> ค้นหาผู้รับ</option>
														{% for e in emp_data %}
															<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
														{% endfor %} #}
													</select>
												</div>		
											<br>
											<br>
												<div class="col-12">
													<select class="form-control-lg" id="receiver_branch_id"  name="receiver_branch_id" style="width:100%;" >
														<option value = "">ชื่อสาขาผู้รับ</option>
														{% for b in mr_branch %}
															<option value="{{ b.mr_branch_id }}" > {{ b.mr_branch_code }} : {{ b.mr_branch_name }}</option>
														{% endfor %} #}
													</select>
												</div>	
												
											
												<!-- <div class="col-4">
													<input type="text" class="form-control mb-5 mb-sm-2" id="pass_emp_re" placeholder="รหัสพนักงาน">
												</div>
												<div class="col-8">
													<input type="text" class="form-control mb-5 mb-sm-0" id="name_receiver" placeholder="ชื่อผู้รับ">
												</div> -->
											</div>
											<div class="row" style="margin-top:40px;">
												<div class="col-6">
													<div class="input-daterange input-group" id="datepicker" data-date-format="yyyy-mm-dd">
														<input autocomplete="false" type="text" class="input-sm form-control" id="start_date" name="start_date" placeholder="From date"/>
														<label class="input-group-addon" style="border:0px;">to</label>
														<input autocomplete="false" type="text" class="input-sm form-control" id="end_date" name="end_date" placeholder="To date"/>
													</div>
												</div>
												
											</div>
											<div class="row" style="margin-top:10px;">
												<div class="col-6">
															<input type="text" class="form-control mb-5 mb-sm-2" id="barcode" name="barcode" placeholder="Tracking number">
												</div>
											</div>
											
											<div class="row" style="margin-top:10px;">
														<div class="col-6">
															<button type="button" class="btn btn-outline-primary btn-block" id="btn_search">ค้นหา</button>
														</div>
														<div class="col-6">
															<button type="sutmit" class="btn btn-outline-dark btn-block" id="btn_excel">Export Excel</button>
														</div>
											</div>
											<br>							
												
									</div>


								</div>
							</div>
							
							
							
							
							
						</form>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	
		 <div class="panel panel-default" style="margin-top:50px;">
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                        <thead>
                          <tr>
                            <th>#</th>
							<th>Bacode</th>
                            <th>ผู้ส่ง</th>
							<th>ผู้รับ</th>
                            <th>วันที่สร้างรายการ</th>
                            <th>วันที่ถึงสารบรรณกลาง</th>
                            <th>วันที่สำเร็จ</th>
                            <th>ประเภทงาน</th>
                            <th>สถานะงาน</th>
                            <th>หมายเหตุ</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
	
	
		<div id="bg_loader" class="card" style="display: none;">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

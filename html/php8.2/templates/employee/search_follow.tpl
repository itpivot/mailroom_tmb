{% extends "base_emp2.tpl" %}


{% block styleReady %}


.card {
       margin: 8px 0px;
    }

    #img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
    }
    
    .btn {
        border-radius: 0px;
    }
{% endblock %}



{% block domReady %}
				
				
	loadFollowEmp();

    $('#txt_search').on('keyup', function() {
        loadFollowEmp();
    });

{% endblock %}
{% block javaScript %}
function loadFollowEmp() {
	let txt = $('#txt_search').val();
        let str = "";
        $.ajax({
            url: "./ajax/ajax_follow_work_emp_curdate.php",
            type: "POST",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                $('#img_loading').show();
                $('#data_list').hide();
            },
            success: function(res){
                $('#count_all').html(res.length);
                if(res.length > 0) {
                    for(let i = 0; i < res.length; i++) {	
						
						str += '<div class="card">'; 
							str += '<div class="card-body ">';
								str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
								str += '<b>ผู้รับ : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'<br>';
                                str += '<b>เบอร์ติดต่อ : </b>';
								if( res[i]['mr_emp_mobile'] != "" && res[i]['mr_emp_mobile'] != 'NULL' && res[i]['mr_emp_mobile'] != null ){
									str += '<a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+' </a>';
                                }
                                if( res[i]['mr_emp_tel'] != "" && res[i]['mr_emp_tel'] != 'NULL' && res[i]['mr_emp_tel'] != null ){
									str += '/ <a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a>';
                                }
                                str += '<br>';

								if(res[i]['mr_type_work_id']=='2'){
									str += '<b>ส่งที่สาขา : </b>';	
                                    str += ''+res[i]['mr_branch_name']+'<br>';
                                    if(res[i]['mr_branch_floor'] != '' && res[i]['mr_branch_floor'] != null && res[i]['mr_branch_floor'] != "null" ){
                                        str += '<b>ชั้นที่ส่งเอกสาร : </b>ชั้น '+res[i]['mr_branch_floor']+'<br>';
                                    }
								}else{
									str += '<b>แผนก : </b>';	
									str += ''+res[i]['mr_department_name']+'<br>';
									str += '<b>ชั้นที่ส่งเอกสาร : </b>'+res[i]['floor_name']+'<br>';
								}
								str += '<b>วันที่ส่ง : </b>'+res[i]['sys_timestamp']+'<br>';
								str += '<b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'<br>';
								str += '<b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'<br>';
								
								str += '<b><h4>สถานะ : </b>'+res[i]['mr_status_name']+'</h4><br>';
								if( res[i]['mr_status_id'] == 1 ){
										str += '<button type="button" class="btn btn-danger" id="cancle_work" onclick="cancle_work('+res[i]['mr_work_main_id']+');" value="">Cancel</button><br>';
									}
									
								if ( res[i]['mr_status_id'] == 5 ){
									str += '<a href="detail_receive.php?barcode='+res[i]['mr_work_barcode']+'" class="btn btn-info"  >รายละเอียดการรับเอกสาร</a>';
										
								}
								str += '</div>';
						str += '</div>';
						
					}
					
                    $('#data_list').html(str);
                }else {
                    str = '<center>ไม่พบข้อมูล !</center>';
                    $('#data_list').html(str);
                }
            },
            complete: function() {
                $('#img_loading').hide();
                $('#data_list').show();
            }
        });
}
	
	
	function cancle_work(work_id) {	
		//var work_id 		= $("#cancle_work").val();
            alertify.confirm('ยืนยัน','ยืนยันการยกเลิกการส่งเอกสาร!',function(){
            $.ajax({
                url: './ajax/ajax_cancle_work.php',
                type: 'POST',
                data: {
                    work_id: work_id
                },
                success: function(res){
                    if(res == "success") {
                        location.reload();
                    }
                }
            });
      },function() {
       
    })
}	
	
	
	
	
	
	
	
	
	
{% endblock %}

{% block Content %}
<div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
    
    <div class="search_list">
        <form>
            <div class="form-group">
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหาจาก Tracking number, ชื่อ-สกุล, เบอร์ติดต่อ, วันที่">
            </div>
			<div class="form-group">
				<center>จำนวนเอกสาร : <span id="count_all"> {{ count_curdate }}</span>  ฉบับ </center>
            </div>
        </form>
    </div>
	<div id="data_list">
	</div>
	
	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

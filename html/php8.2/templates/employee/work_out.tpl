{% extends "base_emp2.tpl" %}

{% block title %}' - List{% endblock %}

{% block menu_e1 %} active {% endblock %}


{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
	<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}



{% block domReady %}

		check_name_re();
		$("#dataType_contact").select2({ width: '100%' });
		
		
	
var tb_con = $('#tb_con').DataTable({ 
	"searching": true,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
	"pageLength": 100, 
    'columns': [
        {'data': 'no'},
        {'data': 'department'},
        {'data': 'floor'},
        {'data': 'mr_contact_name'},
        {'data': 'emp_code'},
        {'data': 'emp_tel'},
        {'data': 'mr_position_name'},
        {'data': 'remark'}
    ]
});


$('#ClearFilter').on( 'click', function () {
	$('#search1').val('');
	$('#search2').val('');
	$('#search3').val('');
	$('#search4').val('');
	$('#search5').val('');
	$('#search6').val('');
	$('#search7').val('');
	tb_con.search( '' ).columns().search( '' ).draw();
});

$('#search1').on( 'keyup', function () {
		//table.search( this.value ).draw();
		tb_con.column(1).search(this.value, true, false).draw();
});
$('#search2').on( 'keyup', function () {
		//table.search( this.value ).draw();
		tb_con.column(2).search(this.value, true, false).draw();
});
$('#search3').on( 'keyup', function () {
		//table.search( this.value ).draw();
		tb_con.column(3).search(this.value, true, false).draw();
});
$('#search4').on( 'keyup', function () {
		//table.search( this.value ).draw();
		tb_con.column(4).search(this.value, true, false).draw();
});
$('#search5').on( 'keyup', function () {
		//table.search( this.value ).draw();
		tb_con.column(5).search(this.value, true, false).draw();
});
$('#search6').on( 'keyup', function () {
		//table.search( this.value ).draw();
		tb_con.column(6).search(this.value, true, false).draw();
});
$('#search7').on( 'keyup', function () {
		//table.search( this.value ).draw();
		tb_con.column(7).search(this.value, true, false).draw();
});


		$("#btn_save").click(function(){
				$( ".myErrorClass" ).removeClass( "myErrorClass" );
					var pass_emp 			= $("#pass_emp").val();
					var name_receiver 		= $("#name_receiver").val();
					var pass_depart 		= $("#pass_depart").val();
					var floor 				= $("#floor").val();
					var depart 				= $("#depart").val();
					var remark 				= $("#remark").val();
					var emp_id 				= $("#emp_id").val();
					var user_id 			= $("#user_id").val();
					var floor_send 			= $("#floor_send").val();
					var floor_id 			= $("#floor_id").val();
					var barcode 			= $("#barcode").val();
					var topic 				= $("#topic").val();
					var mr_branch_id 		= $("#mr_branch_id").val();
					var branch_send 		= $("#branch_send").val();
					var branch_floor 		= $("#branch_floor").val();
					var send_user_id 		= $("#send_user_id").val();
					var csrf_token	 			= $('#csrf_token').val();
					var qty	 			= $('#qty').val();
					var status 				= true;
					
						
						if(pass_emp == "" || pass_emp == null){
							status = false;
							$('#select2-name_receiver_select-container').addClass( "myErrorClass" );
							alertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลผู้รับ!');
							return;
						}
						if(topic == "" || topic == null){
							status = false;
							$('#topic').addClass( "myErrorClass" );
							alertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลชื่อเอกสาร !');
							return;
						}
						
						if(mr_branch_id == "" || mr_branch_id == null || mr_branch_id == 0 ){
							if(branch_send == 0 || branch_send == null){
								status = false;
								$('#select2-branch_send-container').addClass( "myErrorClass" );
								alertify.alert('เกิดข้อผิดพลาด','กรุณากรอกข้อมูลสาขาของผู้รับ!');
								return;
							}
						}
						if(branch_send == 1){
							status = false;
							//$('#mr_branch_name').addClass( "myErrorClass" );
							$('#select2-branch_send-container').addClass( "myErrorClass" );
							alertify.alert('เกิดข้อผิดพลาด','หน้าต่างนี้สำหรับส่งที่สาขาเท่านั้น!');
							return;
						}else	if(branch_send == "" || branch_send == null || branch_send == 0 ){
							if(mr_branch_id == 1){
								status = false;
								//$('#mr_branch_name').addClass( "myErrorClass" );
								$('#select2-branch_send-container').addClass( "myErrorClass" );
								alertify.alert('เกิดข้อผิดพลาด','หน้าต่างนี้สำหรับส่งที่สาขาเท่านั้น!');
								return;
							}
						}
						
						
						
						//console.log(floor_id);
						if( status === true ){
							var obj = {};
							obj = {
								pass_emp: pass_emp,
								emp_id: emp_id,
								remark: remark,
								user_id: user_id,
								floor_send: floor_send,
								floor_id: floor_id,
								mr_branch_id: mr_branch_id,
								branch_send: branch_send,
								mr_branch_floor: branch_floor,
								csrf_token: csrf_token,
								send_user_id: send_user_id,
								qty: qty,
								topic: topic
							}


							alertify.confirm("โปรตรวจสอบข้อมูลผู้รับให้ถูกต้อง", function() {

								//$.when(saveInOut(obj))
								$.ajax({
									url: url+'ajax/ajax_save_workout.php',
									type: 'POST',
									data: obj,
									dataType: 'json',
									success: function(res) {
										//console.log(res);
										//return;
										
										if(res) {
											
										$('#csrf_token').val(res['token']);
											var msg = '<h1><font color="red" ><b>' + res['mr_work_barcode'] + '<b></font></h1>';
											alertify.confirm('แจ้งเตือน','โปรดนำเลข BARCODE กรอกลงบนเอกสาร : '+ msg,
											function(){
														setTimeout(function(){ 
															location.reload();
															$("#name_receiver").val('');
															$("#pass_depart").val('');
															$("#floor").val('');
															$("#depart").val('');
															$("#pass_emp").val('');
															$("#topic").val('');
															$("#remark").val('');
															$("#floor_send").val(0).trigger('change');
															$("#name_receiver_select").val(0).trigger('change');
														}, 500);
														alertify.success('print');
														//window.location.href="../branch/printcoverpage.php?maim_id="+res['work_main_id'];
														var url ="../branch/printcoverpage.php?maim_id="+res['encode'];
														window.open(url,'_blank');
											},
											function() {
												window.location.reload();
													$("#name_receiver").val('');
													$("#pass_depart").val('');
													$("#floor").val('');
													$("#depart").val('');
													$("#pass_emp").val('');
													$("#topic").val('');
													$("#remark").val('');
													$("#floor_send").val(0).trigger('change');
													$("#name_receiver_select").val(0).trigger('change');
											}).set('labels', {ok:'พิมพ์ใบปะหน้า', cancel:'ตกลง'});


										}
									},
									error: function (error) {
									 alert('เกิดข้อผิดพลาด !!!');
									 window.location.reload();
									}
								});
							}).setHeader('<h5> ยืนยันการบันทึกข้อมูล </h5> ');
							
							
							$('#name_receiver ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#pass_depart ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#floor ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#pass_emp ').css({'border':' 1px solid rgba(0,0,0,.15)'});
							$('#depart ').css({'border': '1px solid rgba(0,0,0,.15)'});
							$('#topic ').css({'border': '1px solid rgba(0,0,0,.15)'});
						}
		});
		$("#pass_emp").keyup(function(){
			check_name_re();
			var pass_emp = $("#pass_emp").val();
			$.ajax({
				//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress2.php",
				url: url+"ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200) {
						if(res.data != false) {
							var fullname = res['data']['mr_emp_name'] + " " + res['data']['mr_emp_lastname'];
							$("#name_receiver_select").html('');
							var dataOption = {
								id: res['data']['mr_emp_id'],
								text: res['data']['mr_emp_code'] + " : "+ res['data']['mr_emp_name'] + " " + res['data']['mr_emp_lastname']
							};

							var newOption = new Option(dataOption.text, dataOption.id, false, false);
							$('#name_receiver_select').append(newOption).trigger('change');
							
							
							$("#name_receiver").val(fullname);
							$("#emp_id").val(res['data']['mr_emp_id']);
							$("#mr_branch_code").val(res['data']['mr_branch_code']);
							$("#mr_branch_name").val(res['data']['mr_branch_name']);
							$("#mr_branch_id").val(res['data']['mr_branch_id']);
							$("#send_user_id").val(res['data']['mr_user_id']);
						} else {
							$("#name_receiver").val("");
							$("#mr_branch_code").val("");
							$("#mr_branch_name").val("");
							$("#depart").val("");
							$("#emp_id").val("");
							$("#mr_branch_id").val("");
						}
					} else {
						$("#name_receiver").val("");
						$("#mr_branch_code").val("");
						$("#mr_branch_name").val("");
						$("#depart").val("");
						$("#emp_id").val("");
						$("#mr_branch_id").val("");
					}
				}
												
			});
									
		});
		
		$("#name_receiver_select").click(function(){
			var name_receiver_select = $("#name_receiver_select").val();
			$.ajax({
				//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php",
				url: url+"ajax/ajax_autocompress_name.php",
				type: "post",
				data: {
					'name_receiver_select': name_receiver_select,
				},
				dataType: 'json',
				success: function(res){
					
					if(res.status == 200) {
						if(res.data != false) {
							$("#mr_emp_code").val(res['data']['mr_emp_code']);
							$("#pass_depart").val(res['data']['mr_department_code']);
							$("#floor").val(res['data']['mr_department_floor']);
							$("#floor_id").val(res['data']['mr_floor_id']);
							$("#depart").val(res['data']['mr_department_name']);
							$("#emp_id").val(res['data']['mr_emp_id']);
						}
					}
					
					
				}
												
			});
									
		});

		$('#name_receiver_select').select2({
				placeholder: "ค้นหาผู้รับ",
				ajax: {
					//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php",
					url: url+"ajax/ajax_getdataemployee_select.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				}
		}).on('select2:select', function(e) {
			setForm(e.params.data);
		});

$("#modal_click").click(function(){
			$('#modal_showdata').modal({ backdrop: false})
		})
		
		$("#dataType_contact").on('select2:select', function(e) {
			 $('#tb_con').DataTable().clear().draw();
			 load_contact($(this).val());
		});
		
load_contact(3);		

$('#remark').keydown(function(e) {
        
	var newLines = $(this).val().split("\\n").length;
	  //</link>linesUsed.text(newLines);
	
	if(e.keyCode == 13 &&  newLines >= 3) {
			//linesUsed.css('color', 'red');
			return  false;
	}
	else {
			//linesUsed.css('color', '');
	}
});



$('#branch_send').on('select2:select', function (e) {
    console.log($('#branch_send').val());
	
	

    var branch_id = $('#branch_send').val();
	$.ajax({
		//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
		url: url+'ajax/ajax_autocompress_chang_branch.php',
		type: 'POST',
		data: {
			branch_id: branch_id
		},
		dataType: 'json',
		success: function(res) {
			if(res.status == 200) {
				$('#branch_floor').html(res.data);
			}
			
		}
	})

});


{% endblock %}				
{% block javaScript %}
var url = "https://www.pivot-services.com/mailroom_tmb/employee/";
url = "";
		function check_name_re(){
			$("#div_re_text").hide();
			var pass_emp = $("#pass_emp").val();
			 if(pass_emp == "" || pass_emp == null){ 
				//$("#div_re_text").hide();
				//$("#div_re_select").show();
			 }else{
				//$("#div_re_text").show();
				//$("#div_re_select").hide();
			 }
		}
		
		function setForm(data) {
				var emp_id = parseInt(data.id);
				var fullname = data.text;
				$.ajax({
					//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
					url: url+'ajax/ajax_autocompress_name.php',
					type: 'POST',
					data: {
						name_receiver_select: emp_id
					},
					dataType: 'json',
					success: function(res) {
						if(res.status == 200) {
							if(res.data != false) {
								$("#pass_emp").val(res['data']['mr_emp_code']);
								$("#mr_branch_code").val(res['data']['mr_branch_code']);
								$("#mr_branch_name").val(res['data']['mr_branch_name']);
								$("#emp_id").val(res['data']['mr_emp_id']);
								$("#mr_branch_id").val(res['data']['mr_branch_id']);
								$("#send_user_id").val(res['data']['mr_user_id']);
							}
						}
						
					}
				})
		}
		
		
		
		function name_re_chang(){
			var name_receiver_select = $("#name_receiver_select").val();
			$.ajax({
				//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php",
				url: url+"ajax/ajax_autocompress_name.php",
				type: "post",
				data: {
					'name_receiver_select': name_receiver_select,
				},
				dataType: 'json',
				success: function(res){
					//console.log(res);
					if(res.status == 200) {
						if(res.data != false){
							$("#pass_emp").val(res['data']['mr_emp_code']);
							$("#pass_depart").val(res['data']['mr_department_code']);
							$("#floor").val(res['data']['mr_department_floor']);
							$("#floor_id").val(res['data']['mr_floor_id']);
							$("#branchname").val(res['data']['mr_branch_name']);
							$("#emp_id").val(res['data']['mr_emp_id']);
							$("#send_user_id").val(res['data']['mr_user_id']);
						}
					}
				
					
				}
												
			});
		}


		var saveInOut = function(obj) {
			//return $.post('https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_save_inout.php', obj);
			return $.post(url+'ajax/ajax_save_workout.php', obj);
		}


function getemp(emp_code){
		$("#pass_emp").val(emp_code);
		$('#modal_showdata').modal('hide');
		$("#pass_emp").keyup();
}	

function load_contact(type){		
$.ajax({
   url : 'ajax/ajax_load_dataContact.php',
   dataType : 'json',
   type : 'POST',
   data : {
			'type': type ,
   },
   success : function(data) {
	   if(data.status == 200) {
		   if(data.data != false) {
			$('#tb_con').DataTable().clear().draw();
			$('#tb_con').DataTable().rows.add(data.data).draw();
		   }
	   } else {
		$('#tb_con').DataTable().clear().draw();
	   }
	  
	}
});
}
{% endblock %}		

{% block styleReady %}

.myErrorClass,ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #cc0000 !important;
    background-color: #f3d8d8 !important;
    background-image: url(http://goo.gl/GXVcmC) !important;
    background-position: 50% 50% !important;
    background-repeat: repeat !important;
}
ul.myErrorClass input {
    color: #666 !important;
}
label.myErrorClass {
    color: red;
    font-size: 11px;
    /*    font-style: italic;*/
    display: block;
}
.dataTables_filter {
display: none; 
}

	#btn_save:hover{
		color: #FFFFFF;
		background-color: #055d97;
	
	}
	#modal_click {
		cursor: help;
		color:#46A6FB;
		}
	
	#btn_save{
		border-color: #0074c0;
		color: #FFFFFF;
		background-color: #0074c0;
	}
 
	#detail_sender_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
	}
	
	#detail_sender_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 20px;
	}
	
	#detail_receiver_head h4{
		text-align:left;
		color: #006cb7;
		border-bottom: 3px solid #006cb7;
		display: inline;
		
		
	}
	
	#detail_receiver_head {
		border-bottom: 3px solid #eee;
		margin-bottom: 20px;
		margin-top: 40px;
		
	}	
 
	

		.modal-dialog {
			max-width: 2000px; 
			padding:50px;
		}
 
.valit_error{
	border:2px solid red;
	border-color: red;
	border-radius: 5px;
} 
		
{% endblock %}					
				
{% block Content %}

	
	<div class="container">
			<div class="form-group" style="text-align: center;">
				 <label><h4 >ข้อมูลผู้รับเอกสาร(สาขา)</h4></label>
			</div>	
			<input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf}}">
			 <input type="hidden" id="user_id" value="{{ user_data.mr_user_id }}">
			 <input type="hidden" id="emp_id" value="">
			 <input type="hidden" id="mr_branch_id" value="">
			 <input type="hidden" id="floor_id" value="">
			 <input type="hidden" id="send_user_id" value="">
			 <input type="hidden" id="barcode" value="{{ barcode }}">
			

			<div class="form-group" id="div_re_text">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
					ชื่อผู้รับ : 
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="name_receiver" placeholder="ชื่อผู้รับ">
					</div>		
				</div>			
			</div>
			
			
			<div class="form-group" id="div_re_select">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อผู้รับ <span style='color:red; font-size: 15px'>&#42;</span>:
					</div>	
					<div class="col-8 input-group" style="padding-left:0px">
						<select class="form-control-lg" id="name_receiver_select" style="width:100%;" >
							 {# < option value = "0" > ค้นหาผู้รับ < /option>
							 {% for e in emp_data %}
								<option value="{{ e.mr_emp_id }}" > {{ e.mr_emp_name }} {{ e.mr_emp_lastname }}</option>
							 {% endfor %} #}
						</select><div class="input-group-append">
							<button id="modal_click" style="height: 28px;" class="btn btn-outline-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="รายชื่อผู้ที่เกี่ยวข้องของแต่ละหน่วยงาน"><i class="material-icons">
								more_horiz
								</i></button>
						</div>
					</div>		
				</div>			
			</div>	
			
			
			
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสพนักงาน <span style='color:red; font-size: 15px'>&#42;</span> :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="pass_emp" readonly placeholder="รหัสพนักงาน">
					</div>		
				</div>			
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						รหัสสาขา :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="mr_branch_code" placeholder="รหัสหน่วยงาน/รหัสค่าใช้จ่าย" readonly>
					</div>		
				</div>			
			</div>
			
			
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อสาขา :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" class="form-control form-control-sm" id="mr_branch_name" placeholder="ชื่อหน่วยงาน" readonly>
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชื่อเอกสาร<span style='color:red; font-size: 15px'>&#42;</span> :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="text" maxlength="150" class="form-control form-control-sm" id="topic" placeholder="ชื่อเรื่อง, หัวข้อเอกสาร">
					</div>		
				</div>			
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						จำนวน<span style='color:red; font-size: 15px'>&#42;</span> :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<input type="number" value="1" class="form-control form-control-sm" id="qty" placeholder="จำนวนเอกสาร 1,2,3,...">
					</div>		
				</div>			
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						สาขาผู้รับ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<select class="form-control-lg" id="branch_send" style="width:100%;">
								<option value="0" > เลือกสาขาปลายทาง</option>
									{% for b in branch_data %}
										<option value="{{ b.mr_branch_id }}" > {{ b.mr_branch_code }}:{{ b.mr_branch_name }}</option>
									{% endfor %}					
							</select>
					</div>		
				</div><br>
				<div class="row">
					<div class="col-4" style="padding-right:0px">
						ชั้นผู้รับ :
					</div>	
					<div class="col-8" style="padding-left:0px">
						<div class="col-5" style="padding-left:0px">
							<select class="form-control-lg" id="branch_floor" style="width:100%;">
										<option value="1" selected>ชั้น 1</option>			
										<option value="2" >ชั้น 2</option>			
										<option value="3" >ชั้น 3</option>			
							</select>
						</div>
					</div>		
				</div>			
			</div>	
			
			
			
			
			<div class="form-group">
				<textarea maxlength="150" class="form-control form-control-sm" id="remark" placeholder="รายละเอียดของเอกสาร"></textarea>
			</div>
			{% if tomorrow == 1 %}
				<div class="form-group">
					<div class="row">
						<div class="col-12" style="color:red;">
							<center>เอกสารที่ส่งหลัง 16.00 น. จะทำการส่งในวันถัดไป </center>
						</div>			
					</div>			
				</div>	
			{% endif %}
			<div class="form-group">
				<button type="button" class="btn btn-outline-primary btn-block" id="btn_save"  >Save</button>
			</div>
			
			
	
	</div>
	

	
<div class="modal fade" id="modal_showdata">
  <div class="modal-dialog modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
			<select id="dataType_contact" class="form-control" width="2000px">
								<option value="3" selected> รายชื่อตามหน่วยงาน  </option>
								<option value="2"> รายชื่อตามสาขา </option>
								<option value="1"> รายชื่อติดต่อ อื่นๆ  </option>
			  </select>
		</h5><br>
			
		
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <div style="overflow-x:auto;">
        <table class="table" id="tb_con">
			  <thead>
				<tr>
				  <td><button type="button" id="ClearFilter" class="btn btn-secondary">ClearFilter</button></td>
				  <td><input type="text" class="form-control" id="search1"  placeholder="ชื่อหน่วยงาน"></td>
				  <td><input type="text" class="form-control" id="search2"  placeholder="ชั้น"></td>
				  <td><input type="text" class="form-control" id="search3"  placeholder="ผู้รับงาน"></td>
				  <td><input type="text" class="form-control" id="search4"  placeholder="รหัสพนักงาน"></td>
				  <td><input type="text" class="form-control" id="search5"  placeholder="เบอร์โทร"></td>
				  <td><input type="text" class="form-control" id="search6"  placeholder="ตำแหน่ง"></td>
				  <td><input type="text" class="form-control" id="search7"  placeholder="หมายเหตุ"></td>
				</tr>
				<tr>
				  <td>ลำดับ</td>
				  <td>ชื่อหน่วยงาน</td>
				  <td>ชั้น</td>
				  <td>ผู้รับงาน</td>
				  <td>รหัสพนักงาน</td>
				  <td>เบอร์โทร</td>
				  <td>ตำแหน่ง</td>
				  <td>หมายเหตุ</td>
				</tr>
				
			  </thead>
			  <tbody>
			  {% for d in contactdata %}
				<tr>
				  <td scope="row"><button type="button" class="btn btn-link" onclick="getemp('{{d.emp_code}}')">เลือก</button></td>
				  <td>{{d.department_code}}:{{d.department_name}}</td>
				  <td>{{d.floor}}</td>
				  <td>{{d.mr_contact_name}}</td>
				  <td>{{d.emp_code}}</td>
				  <td>{{d.emp_tel}}</td>
				  <td>{{d.mr_position_name}}</td>
				  <td>{{d.remark }}</td>
				</tr>
				{% endfor %}
			  </tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่างนี้</button>
      </div>
    </div>
  </div>
</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

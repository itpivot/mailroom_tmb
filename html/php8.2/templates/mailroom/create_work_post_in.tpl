{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>

<link rel="stylesheet" href="../themes/jquery/jquery-ui.css">
<script src="../themes/jquery/jquery-ui.js"></script>

<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="../themes/jquery/jquery.validate.min.js"></script>
<!-- dependencies for zip mode -->
	<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
	<!-- / dependencies for zip mode -->

	<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
	<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

	<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
	
	<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
	<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

{% endblock %}

{% block domReady %}	



$(document).keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          $('#btn_save').click();
        }
});




load_data_bydate();
$('#date_send').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	


var tbl_data = $('#tb_keyin').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'num'}, 
		{'data': 'action'},
        {'data': 'd_send'}, 
        {'data': 'mr_work_barcode'},
        {'data': 'mr_status_name'},
        {'data': 'mr_round_name'},
        {'data': 'sendder_name'},
        {'data': 'sendder_address'},
        {'data': 'mr_work_remark'},
        {'data': 'name_resive'}
    ]
});



  $('#myform_data_senderandresive').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'date_send': {
			required: true
		},
		'type_send': {
			required: true
		},
		'round': {
			required: true
		},
		'emp_id_re': {
			//required: true
		},
		'dep_re': {
			required: true
		},
		'building': {
			required: true
		},
		'name_send': {
			required: true
		},
		'lname_send': {
			//required: true
		},
		'tel_re': {
			required: true
		},
		'post_barcode': {
			required: true
		},
		'quty': {
			required: true,
		},
	  
	},
	messages: {
		'date_send': {
		  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
		},
		'type_send': {
		  required: 'กรุณาระบุ ประเภทการส่ง'
		},
		'round': {
		  required: 'กรุณาระบุ รอบจัดส่ง'
		},
		'emp_id_re': {
		  required: 'กรุณาระบุ ผู้รับ'
		},
		'dep_re': {
			required: 'กรุณาระบุ หน่วยงานผู้รับ'
		  },
		  'building': {
			required: 'กรุณาระบุ ช้นผู้รับ'
		  },
		'name_send': {
		  required: 'กรุณาระบุ ชื่อผู้ส่ง'
		},
		'lname_send': {
		  required: 'กรุณาระบุ นามสกุลผู้ส่ง'
		},
		'tel_re': {
		  required: 'กรุณาระบุ เบอร์โทร'
		},
		'address_send': {
		  required: 'กรุณาระบุ ที่อยู่'
		},
		'post_barcode': {
		  required: 'กรุณาระบุ เลขที่เอกสาร '
		},
		'quty': {
		  required: 'กรุณาระบุ จำนวน',
		  number: 'กรุณาระบุ เป็นตัวเลข'
		},
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });


$('#btn_save').click(function() {
	
	if($('#myform_data_senderandresive').valid()) {
	 	var form = $('#myform_data_senderandresive');
		  //</link>var serializeData = form.serializeArray();
      	var serializeData = form.serialize();
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_save_Work_post_in.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			$('#csrf_token').val(res['token']);
			if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				load_data_bydate();
				$('#csrf_token').val(res['token']);
				if($("#reset_send_form").prop("checked") == false){
					reset_send_form();
				}
				if($("#reset_resive_form").prop("checked") == false){
					reset_resive_form();
				}
				if($("#reset_detail_form").prop("checked") == false){
					reset_detail_form();
				}
				
				

				//token
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});


$('#date_report').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	

$('#dep_re').select2();
$('#building').select2();
$('#emp_id_re').select2({
	placeholder: "ค้นหาผู้รับ",
	ajax: {
		url: "./ajax/ajax_getdataemployee_select_search.php",
		dataType: "json",
		delay: 250,
		processResults: function (data) {
			return {
				results : data
			};
		},
		cache: true
	}
}).on('select2:select', function(e) {
	//console.log(e.params.data.data.mr_floor_id);
	//console.log(e.params.data.data.mr_department_id);
	$('#dep_re').val(e.params.data.data.mr_department_id).trigger('change');
	$('#building').val(e.params.data.data.mr_floor_id).trigger('change');
	//setForm(e.params.data.id);
});



function setForm(emp_code) {
			var emp_id = parseInt(emp_code);
			console.log(emp_id);
			$.ajax({
				url: './ajax/ajax_autocompress_name.php',
				type: 'POST',
				data: {
					name_sendceiver_select: emp_id
				},
				dataType: 'json',
				success: function(res) {
					console.log("++++++++++++++");
					if(res['status'] == 501){
						console.log(res);
					}else if(res['status'] == 200){
						$("#emp_send_data").val(res.text_emp);
					}else{
						alertify.alert('ผิดพลาด',"  "+res.message,function(){window.location.reload();});
					}
				}
			})
		}


		$("#name_send").autocomplete({
            source: function( request, response ) {
                
                $.ajax({
                    url: "ajax/ajax_getcustommer_WorkPost.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
						console.log(data);
                    }
                });
            },
            select: function (event, ui) {
                $('#name_send').val(ui.item.name); // display the selected text
                $('#lname_send').val(ui.item.lname); // display the selected text
                $('#address_send').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
				$('#name_send').val(ui.item.name); // display the selected text
                $('#lname_send').val(ui.item.lname); // display the selected text
                $('#address_send').val(ui.item.mr_address); // save selected id to input
                return false;
                return false;
            },
        });



{% endblock %}
{% block javaScript %}

function cancle_work(id) {
	alertify.confirm('ยืนยันการลบ', 'กด "OK" เพื่อยกเลิกการส่ง',
	function(){ 
		$.ajax({
		method: "POST",
		dataType:'json',
		data:{
			id 		: id,
			page	:'cancle'
		},
		url: "ajax/ajax_load_Work_post_in.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
			load_data_bydate();
	  });
	  
	}, function(){ 
		alertify.error('Cancel')
	});
}


function reset_detail_form() {
	$('#round').val("");
	$('#post_barcode').val("");
	$('#quty').val("1");
	$('#work_remark').val("");
}

function reset_send_form() {
	$('#name_send').val("");
	$('#lname_send').val("");
	$('#address_send').val("");
}

function reset_resive_form() {
	$('#emp_id_re').val("").trigger('change');
	$('#dep_re').val("").trigger('change');
	$('#building').val("").trigger('change');
}

function load_data_bydate() {
	$.ajax({
		method: "POST",
		dataType:'json',
		url: "ajax/ajax_load_Work_post_in.php",
		data:{
			date:$('#date_report').val(),
			round:$('#round_printreper').val()
		},
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res['data']).draw();

		}
	  });
}



function print_option(type){
	console.log(type);
	if(type == 1 ){
		//console.log(11);
		$("#form_print_post_in").attr('action', 'print_peper_post_in.php');
		$('#form_print_post_in').submit();
	}else if(type == 2 ){
		$("#form_print_post_in").attr('action', 'excel.PostIn.php');
		$('#form_print_post_in').submit();
	}else{
		alert('ไม่มีข้อมูล');
	}

}


function reset_emp_id_re(){
	$("#emp_id_re").empty().trigger('change')
	//console.log('reset_emp_id_re');
}

{% endblock %}
{% block Content2 %}

<div  class="container-fluid">

	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>สั่งงาน ปณ. ขาเข้า</b></h3></label><br>
				<label>การสั่งงาน > ปณ. ขาเข้า</label>
		   </div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<form id="myform_data_senderandresive">
					<div class="row">
						<div class="col-md-3">
							<h5 class="card-title">รอบการนำส่งเอกสารประจำวัน</h5>
							<h5 class="card-title">ข้อมูลเอกสาร</h5>
							<div class="table-responsive">
								<table style="width: 100%;" class="">
									<tr>
										<td style="width: 180px;" ><span class="text-muted font_mini" >วันที่นำส่ง:</span></td>
										<td>
											<span class="box_error" id="err_date_send"></span>
											<input data-error="#err_date_send" name="date_send" id="date_send" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}"></td>
									  </tr>
									<tr>
										<td><span class="text-muted font_mini" >รอบการรับส่ง:</span></td>
										<td>
										<span class="box_error" id="err_round"></span>
										<select data-error="#err_round" class="form-control form-control-sm" id="round" name="round">
												<option value="">กรุณาเลือกรอบ</option>
												{% for r in round %}
												<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
												{% endfor %}
											</select>
										</td>
									  </tr>
								</table>
								<hr>
								
							  </div>
							  
							
							
							
						</div>
						
						



						<div class="col-md-9">
							<h5 class="card-title">ผู้ส่ง</h5>
							<div class="table-responsive">
							<table style="width: 100%;">
							<tr>
									<td style="width: 180px;" ><span class="text-muted font_mini" >ชื่อผู้ส่ง:</span></td>
										<td>
											<span class="box_error" id="err_name_send"></span>
											<input name="name_send" id="name_send" data-error="#err_name_send" class="form-control form-control-sm" type="text" placeholder="ชื่อ นามสกุลผู้ส่ง">
										</td>
									  </tr>
									<tr>
										<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >เลขที่เอกสาร :</span></td>
										<td>
											<span class="box_error" id="err_post_barcode"></span>
											<input id="post_barcode" name="post_barcode" data-error="#err_post_barcode"  class="form-control form-control-sm" type="text" placeholder="-">
										</td>
									</tr>
									<tr>
										<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >จำนวน:</span></td>
										<td>
											<span class="box_error" id="err_quty"></span>
											<input id="quty" name="quty" data-error="#err_quty"  value="1" class="form-control form-control-sm" type="number" placeholder="-">
										</td>
									</tr>
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >รายละเอียด/หมายเหตุ:</span></td>
										<td>
											<span class="box_error" id="err_"></span>
											<textarea id="work_remark" name="work_remark" data-error="#"  class="form-control" id="" rows="2"></textarea>
										</td>
									</tr>
									
									<tr>
								</table>
								<input type="hidden" name="csrf_token" id="csrf_token" value="{{csrf_token}}">

								<!-- <hr> 
								<table class="" style="width: 100%;">
									<tr>
										<td class="align-top"><span class="text-muted font_mini" >ที่อยู่​:</span></td>
										<td>
											<span class="box_error" id="err_address_send"></span>
											<textarea id="address_send" name="address_send" data-error="#err_address_send"  class="form-control"  rows="2">-</textarea>
										</td>
									</tr>
								</table>-->
								
							  </div>
							  <hr>
							<h5 class="card-title">ผู้รับ</h5>
							<div class="table-responsive">
								<table class="" style="width: 100%;">
									<tr>
										<td style="width: 180px;"><span class="text-muted font_mini" >รหัสพนักงาน:</span></td>
										<td>
											<span class="box_error" id="err_emp_id_re"></span>
											<div class="input-group mb-3">
											<button class="btn btn-link btn-sm text-danger" type="button" onclick="reset_emp_id_re();">
															ล้างข้อมูลผู้รับ
												</button>
												<select data-error="#err_emp_id_re" class="form-control form-control-sm" id="emp_id_re" name="emp_id_re" style="width:100%;">
												</select>		
											</div>
										</td>
									</tr>
									<tr>
										<td style="width: 180px;"><span class="text-muted font_mini" >แผนก/ผ่าย:</span></td>
										<td>
											<span class="box_error" id="err_dep_re"></span>
											<select data-error="#err_dep_re" class="form-control form-control-sm" id="dep_re" name="dep_re" style="width:100%;">
												<option selected disabled value="">ระบุหน่วยงาน</option>
												{% for d in departmentdata %}
												<option value="{{d.mr_department_id}}">{{d.mr_department_code}} {{d.mr_department_name}}</option>
												{% endfor %}
											</select>
										</td>
									</tr>
									<tr>
										<td style="width: 180px;"><span class="text-muted font_mini" >ชัน/อาคาร:</span></td>
										<td>
											<span class="box_error" id="err_building"></span>
											<select data-error="#err_building" class="form-control form-control-sm" id="building" name="building" style="width:100%;">
												<option value="">ระบุอาคาร/ชั้น</option>
												{% for f in floordata %}
												<option value="{{f.mr_floor_id}}">{{f.name}}</option>
												{% endfor %}
											</select>
										</td>
									</tr>
									<tr>
								</table>
								
							  </div>
							<hr>
							<br>
							  <div class="form-group text-center">
							  
								<input class="" type="checkbox" id="reset_detail_form" value="option1">คงข้อมูลเอกสาร
								<input class="" type="checkbox" id="reset_send_form" value="option1">คงข้อมูลผู้ส่ง
								<input class="" type="checkbox" id="reset_resive_form" value="option1">คงข้อมูลผู้รับ
							<br>
							<br>
								<button type="button" class="btn btn-outline-primary btn-sm" id="btn_save">บันทึกข้อมูล </button>
								{#
								<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_send_form();">ล้างข้อมูลผู้ส่ง </button>
								<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_resive_form();">ล้างข้อมูลผู้รับ </button>
								#}

							</div>
						</div>	
					</div>
				</form>



<div class="row">
	<div class="col-md-12 text-right">
	<form id="form_print_post_in" action="#" method="post" target="_blank">
		<hr>
		<table>
			<tr>
			 	<td>
					<span class="box_error" id="err_date_report"></span>
					<input data-error="#err_date_report" name="date_report" id="date_report" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
				</td>
				<td>
					<span class="box_error" id="err_round"></span>
					<select data-error="#err_round" class="form-control form-control-sm" id="round_printreper" name="round_printreper">
						<option value="">กรุณาเลือกรอบ</option>
						{% for r in round %}
						<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
						{% endfor %}
					</select>
				</td>
				<td>
					<button onclick="load_data_bydate();" type="button" class="btn btn-sm btn-outline-secondary" id="">ค้นหา</button>
					<button onclick="print_option(1);" type="button" class="btn btn-sm btn-outline-secondary" id="">พิมพ์ใบงาน</button>
					<button onclick="print_option(2);" type="button" class="btn btn-sm btn-outline-secondary" id="">Excel</button>
				</td>
			</tr>
		</table>
	</form>
	</div>	
</div>





					<div class="row">
						<div class="col-md-12">
							<hr>
							<h5 class="card-title">รายการรับเข้่า</h5>
							<table class="table" id="tb_keyin">
								<thead class="thead-light">
								  <tr>
									<th width="5%" scope="col">#</th>
									<th width="5%" scope="col">Update</th>
									<th width="5%" scope="col">วันที่</th>
									<th width="10%" scope="col">เลขที่เอกสาร</th>
									<th width="10%" scope="col">สถานะ</th>
									<th width="5%" scope="col">รอบ</th>
									<th width="10%" scope="col">ชื่อผู้ส่ง</th>
									<th width="10%" scope="col">ที่อยู่</th>
									<th width="10%" scope="col">หมายเหตุ</th>
									<th width="30%"scope="col">ผู้รับ</th>
								  </tr>
								</thead>
								<tbody>
							
								</tbody>
							  </table>


						</div>
					</div>
					

				</div>
			  </div>
		
		</div>
	</div>

</div>







<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

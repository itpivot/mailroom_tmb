{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_3 %} active {% endblock %}

{% block scriptImport %}
        <link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<link rel="stylesheet" href="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.css"></link>

		<link rel="stylesheet" href="../themes/jquery/jquery-ui.css">
		<script src="../themes/jquery/jquery-ui.js"></script>

		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<script src="../themes/jquery/jquery.validate.min.js"></script>
		<!-- dependencies for zip mode -->
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
			<!-- / dependencies for zip mode -->

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/JQL.min.js"></script>
			<script type="text/javascript" src="../themes/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

			<script type="text/javascript" src="../themes/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
			
			<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
			<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
.alert_time {
    padding: 3px 15px 3px 15px; 
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: 20px;
}
.font_mini{
	font-size:14px;
}
.box_error{
	font-size:12px;
	color:red;
}
#loader{
	  height:100px;
	  width :100px;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:fixed;
		top:500px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

{% endblock %}

{% block domReady %}	

//console.log('55555');

$('#date_send').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	
$('#date_import').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	
$('#date_report').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true,
			format: 'yyyy-mm-dd'
});	
  $('#myform_data_senderandresive').validate({
	onsubmit: false,
	onkeyup: false,
	errorClass: "is-invalid",
	highlight: function (element) {
		if (element.type == "radio" || element.type == "checkbox") {
			$(element).removeClass('is-invalid')
		} else {
			$(element).addClass('is-invalid')
		}
	},
	rules: {
		'date_send': {
			required: true
		},
		'type_send': {
			required: true
		},
		'round': {
			required: true
		},
		'dep_id_send': {
			required: true
		},
		'name_re': {
			required: true
		},
		'lname_re': {
			//required: true
		},
		'tel_re': {
			//required: true
		},
		'address_re': {
			required: true
		},
		'sub_district_re': {
			//required: true
		},
		'district_re': {
			//required: true
		},
		'province_re': {
			//required: true
		},
		'post_code_re': {
			//required: true
		},
		'topic': {
			//required: true
		},
		'quty': {
			required: true,
			min: 1,
		},
		'weight': {
			required: true,
			min: 1,
		},
		'price': {
			required: true,
			min: 1,
		},
		'mr_type_post_id': {
			required: true,
		},
		'post_barcode': {
			required:  {
				depends: 
				  function(element){
					var mr_type_post_id = $('#mr_type_post_id').val();
					if(mr_type_post_id == 1){
						return false;
					} else {
						return true;
					}
				  }
				},
		},
		'sub_district_re': {
			required: true,
		},
		'district_re': {
			required: true,
		},
		'province_re': {
			required: true,
		},
		'post_barcode_re': {
			required:  {
				depends: 
				  function(element){
					var mr_type_post_id = $('#mr_type_post_id').val();
					if(mr_type_post_id == 3 || mr_type_post_id == 5){
						return true;
					} else {
						return false;
					}
				  }
				},
		},
	  
	},
	messages: {
		'date_send': {
		  required: 'กรุณาระบุ วันทสี่ส่งงาน.'
		},
		'type_send': {
		  required: 'กรุณาระบุ ประเภทการส่ง'
		},
		'round': {
		  required: 'กรุณาระบุ รอบจัดส่ง'
		},
		'dep_id_send': {
		  required: 'กรุณาระบุหน่วยงาน ผู้ส่ง'
		},
		'name_re': {
		  required: 'กรุณาระบุ ชื่อผู้รับ'
		},
		'lname_re': {
		  required: 'กรุณาระบุ นามสกุลผู้รับ'
		},
		'tel_re': {
		  required: 'กรุณาระบุ เบอร์โทร'
		},
		'address_re': {
		  required: 'กรุณาระบุ ที่อยู่'
		},
		'sub_district_re': {
		  //required: 'กรุณาระบุ แขวง/ตำบล'
		},
		'district_re': {
		  //required: 'กรุณาระบุ เขต/อำเภอ'
		},
		'province_re': {
		  //required: 'กรุณาระบุ จังหวัด'
		},
		'post_code_re': {
		  //required: 'กรุณาระบุ รหัสไปรษณีย์'
		},
		'topic': {
		  required: 'กรุณาระบุ หัวเรื่อง'
		},
		'mr_type_post_id': {
		  required: 'กรุณาระบุ ประเภทการส่ง'
		},
		'post_barcode': {
		  required: 'กรุณาระบุ เลขที่เอกสาร'
		},
		'quty': {
		  required: 'กรุณาระบุ จำนวน',
		  number: 'กรุณาระบุ เป็นตัวเลข',
		  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
		},
		'weight': {
		  required: 'กรุณาระบุ น้ำหนัก',
		  number: 'กรุณาระบุ เป็นตัวเลข',
		  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
		},
		'price': {
		  required: 'ไม่พบข้อมูลราคา',
		  min: 'ราคาต้องมีค่าตั้งแต่ 1 ขึนไป'
		},
		'sub_district_re': {
			required:  'กรุณาระบุ แขวง/ตำบล:',
		},
		'district_re': {
			required:  'กรุณาระบุ เขต/อำเภอ:',
		},
		'province_re': {
			required:  'กรุณาระบุ จังหวัด:',
		},
		'post_barcode_re': {
			required:  'กรุณาระบุ เลขตอบรับ :',
		},
		 
	},
	errorElement: 'span',
	errorPlacement: function (error, element) {
		var placement = $(element).data('error');
		//console.log(placement);
		if (placement) {
			$(placement).append(error)
		} else {
			error.insertAfter(element);
		}
	}
  });


$('#btn-show-form-edit').click(function() {
	$('#div_detail_receiver_sender').hide();
	$('#acction_update').hide();
	$('#acction_edit').show();
	$('#div_sender').show();
	$('#div_receiver').show();
	$("#page").val('edit');
});
$('#btn_cancle_edit').click(function() {
	$('#div_detail_receiver_sender').show();
	$('#acction_update').show();
	$('#acction_edit').hide();
	$('#div_sender').hide();
	$('#div_receiver').hide();
	$("#page").val('update_status');
});

$('#btn_save').click(function() {
	
	if($('#myform_data_senderandresive').valid()) {
	 	var form = $('#myform_data_senderandresive');
		  //</link>var serializeData = form.serializeArray();
      	var serializeData = form.serialize();
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_save_Work_post_out.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 401){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}else if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				//load_data_bydate();
				$('#csrf_token').val(res['token']);
				if($("#reset_price_form").prop("checked") == false){
					reset_price_form();
				}
				$("#detail_sender").val('');
				$("#detail_receiver").val('');
				$("#work_barcode").val('');
				$("#work_barcode").focus();
				
				

				//token
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});
$('#btn_edit').click(function() {
	
	if($('#myform_data_senderandresive').valid()) {
	 	var form = $('#myform_data_senderandresive');
		  //</link>var serializeData = form.serializeArray();
      	var serializeData = form.serialize();
		  $.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_save_Work_post_out.php",
			data: serializeData,
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
			if(res['status'] == 401){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}else if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				alertify.alert('สำเร็จ',"  แก้ไขข้อมูลเรียบร้อยแล้ว"
				,function(){
					window.location.reload();
				});
			}else{
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}
		  });
     
	}
});


$.Thailand({
  database: '../themes/jquery.Thailand.js/database/geodb.json',
$district: $('#sub_district_re'), // input ของตำบล
  $amphoe: $('#district_re'), // input ของอำเภอ
  $province: $('#province_re'), // input ของจังหวัด
  $zipcode: $('#post_code_re'), // input ของรหัสไปรษณีย์
  onDataFill: function (data) {
      $('#receiver_sub_districts_code').val('');
      $('#receiver_districts_code').val('');
      $('#receiver_provinces_code').val('');

      if(data) {
          $('#sub_districts_code_re').val(data.district_code);
          $('#districts_code_re').val(data.amphoe_code);
          $('#provinces_code_re').val(data.province_code);
		  //console.log(data);
      }
      
  }
});

$('#dep_id_send').select2();
$('#emp_id_send').select2({
	placeholder: "ค้นหาผู้ส่ง",
	ajax: {
		url: "./ajax/ajax_getdataemployee_select_search.php",
		dataType: "json",
		delay: 250,
		processResults: function (data) {
			return {
				results : data
			};
		},
		cache: true
	}
}).on('select2:select', function(e) {
	console.log(e.params.data.id);
	setForm(e.params.data.id);
});
$('#messenger_user_id').select2({
	placeholder: "ค้นหาผู้ส่ง",
	ajax: {
		url: "./ajax/ajax_getmessenger_select_search.php",
		dataType: "json",
		delay: 250,
		processResults: function (data) {
			return {
				results : data
			};
		},
		cache: true
	}
}).on('select2:select', function(e) {
	//console.log(e.params.data.id);
});

var tbl_data = $('#tb_keyin').DataTable({ 
	"searching": true,
	 "fixedHeader": {
        header: true,
    },
    "Info": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    },
    'columns': [
        {'data': 'num'},
        {'data': 'check'},
        {'data': 'action'},
        {'data': 'mr_type_post_name'},
        {'data': 'd_send'},
        {'data': 'mr_work_barcode'},
        {'data': 'mr_status_name'},
        {'data': 'name_send'},
        {'data': 'mr_round_name'},
        {'data': 'name_resive'},
        {'data': 'mr_address'},
        {'data': 'mr_cus_tel'},
        {'data': 'mr_work_remark'}
    ]
});
$('#select-all').on('click', function(){
	// Check/uncheck all checkboxes in the table
	var rows = tbl_data.rows({ 'search': 'applied' }).nodes();
	$('input[type="checkbox"]', rows).prop('checked', this.checked);
 });
load_data_bydate();



function setForm(emp_code) {
			var emp_id = parseInt(emp_code);
			console.log(emp_id);
			$.ajax({
				url: './ajax/ajax_autocompress_name.php',
				type: 'POST',
				data: {
					name_receiver_select: emp_id
				},
				dataType: 'json',
				success: function(res) {
					console.log("++++++++++++++");
					if(res['status'] == 501){
						console.log(res);
					}else if(res['status'] == 200){
						$("#emp_send_data").val(res.text_emp);
						$("#dep_id_send").val(res.data.mr_department_id).trigger('change');
					}else{
						alertify.alert('ผิดพลาด',"  "+res.message,function(){window.location.reload();});
					}
				}
			})
		}

		$("#name_re").autocomplete({
            source: function( request, response ) {
                
                $.ajax({
                    url: "ajax/ajax_getcustommer_WorkPost.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
						console.log(data);
                    }
                });
            },
            select: function (event, ui) {
                $('#name_re').val(ui.item.name); // display the selected text
                $('#lname_re').val(ui.item.lname); // display the selected text
                $('#address_re').val(ui.item.mr_address); // save selected id to input
                $('#tel_re').val(ui.item.mr_cus_tel); // save selected id to input
				$('#sub_districts_code_re').val(ui.item.mr_sub_districts_code); // save selected id to input
				$('#districts_code_re').val(ui.item.mr_districts_code); // save selected id to input
				$('#provinces_code_re').val(ui.item.mr_provinces_code); // save selected id to input
				$('#sub_district_re').val(ui.item.mr_sub_districts_name); // save selected id to input
				$('#district_re').val(ui.item.mr_districts_name); // save selected id to input
				$('#province_re').val(ui.item.mr_provinces_name); // save selected id to input
				$('#post_code_re').val(ui.item.zipcode); // save selected id to input
				$('#address_re').val(ui.item.mr_address); // save selected id to input
                return false;
            },
            focus: function(event, ui){
                return false;
            },
        });
		
		$('#work_barcode').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				var barcode = $(this).val();
				$.ajax({
					url: './ajax/ajax_get_data_bybacose_thaipost.php',
					type: 'POST',
					data: {
						barcode	: barcode,
						page	: 'getdataBybarcode',
						csrf_token	: $('#csrf_token').val()
					},
					dataType: 'json',
					success: function(res) {
						$('#csrf_token').val(res['token']);

						//console.log("++++++++++++++");
						if(res['status'] == 501){
							console.log(res);
						}else if(res['status'] == 200){
							if(res['count']>0){
								set_form_val_all(res['data'])
							}else{
								$('#btn-show-form-edit').attr('disabled','disabled');
								reset_price_form();
								reset_resive_form();
								reset_send_form();
								$("#detail_sender").val('');
								$("#detail_receiver").val('');
								$("#work_barcode").val('');
								$("#work_barcode").focus();
							}
						}else{
							alertify.alert('ผิดพลาด',"  "+res.message,function(){window.location.reload();});
						}
					}
				}); 
			}
		  });		
		
{% endblock %}
{% block javaScript %}

function load_data_bydate() {

	var form = $('#form_print');
	var serializeData = form.serialize();
	$.ajax({
		method: "POST",
		dataType:'json',
		data:serializeData,
		url: "ajax/ajax_load_Work_Post_out.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
		if(res['status'] == 200){
			$('#tb_keyin').DataTable().clear().draw();
			$('#tb_keyin').DataTable().rows.add(res.data).draw();

		}
	  });
}

function reset_send_form() {
	$('input[name="type_send"]').attr('checked', false);
	$('#round').val("");
	$('#emp_id_send').val("").trigger('change');
	$('#emp_send_data').val("");
}
function reset_price_form() {
	$("#weight").val('');
	$("#quty").val(1);
	$("#price").val('');
	$("#total_price").val('');
	$("#post_barcode").val('');
	$("#post_barcode_re").val('');
	$("#mr_type_post_id").val('');
}

function reset_resive_form() {
	$("#name_re").val("");
	$("#lname_re").val("");
	$("#tel_re").val("");
	$("#address_re").val("");
	$("#sub_districts_code_re").val("");
	$("#districts_code_re").val("");
	$("#provinces_code_re").val("");
	$("#sub_district_re").val("");
	$("#district_re").val("");
	$("#province_re").val("");
	$("#post_code_re").val("");
	$("#quty").val("1");
	$("#topic").val("");
	$("#work_remark").val("");
}

function cancle_work(id) {
	alertify.confirm('ยืนยันการลบ', 'กด "OK" เพื่อยกเลิกการส่ง',
	function(){ 
		$.ajax({
		method: "POST",
		dataType:'json',
		data:{
			id 		: id,
			page	:'cancle'
		},
		url: "ajax/ajax_load_Work_byHand_out.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
			load_data_bydate();
	  });
	  
	}, function(){ 
		alertify.error('Cancel')
	});
}

function print_option(type){
	console.log(type);
	if(type == 1 ){
		console.log(11);
		$("#form_print").attr('action', 'print_peper_thai_post.php');
		$('#form_print').submit();
	}else if(type == 2){
		console.log(22);
		$("#form_print").attr('action', 'print_cover_byhand.php');
		$('#form_print').submit();
	}else{
		alert('----');
	}

}

function chang_dep_id() {
	var dep = $('#dep_id_send').select2('data'); 
	var emp = $('#emp_id_send').select2('data'); 
	
	console.log(emp);
	if(emp[0].id!=''){
		$("#emp_send_data").val(emp[0].text+'\\n'+dep[0].text);
	}else{
		$("#emp_send_data").val(dep[0].text);
	}
	
}

function changPost_Type(type_id){
	//console.log(type_id);
	if(type_id == 3 || type_id == 5){
		$('#tr-post-barcode-re').show();
		console.log("if ::"+type_id);
	}else{
		$('#tr-post-barcode-re').hide();
		console.log("else ::"+type_id);
	}
	setPost_Price();
}
function setPost_Price(){
	var mr_type_post_id = $('#mr_type_post_id').val();
	var quty 			= $('#quty').val();
	var weight 			= $('#weight').val();
	var price 			= $('#price').val();
	var total_price 	= $('#totsl_price').val();

	if(mr_type_post_id != '' && mr_type_post_id != null && weight != '' && weight != null){
		//console.log('เข้า');
		$.ajax({
			method: "POST",
			dataType:'json',
			url: "ajax/ajax_get_post_price.php",
			data :{
				mr_type_post_id : mr_type_post_id,
				quty 		 	: quty 		,
				weight 		 	: weight 		,
				price 		 	: price 		,
				total_price 	: total_price
			},
			beforeSend: function() {
				// setting a timeout
				$("#bg_loader").show();
				$('#total_price').val('');
			},
			error: function (error) {
			  alert('error; ' + eval(error));
			  $("#bg_loader").hide();
			  $('#price').val('');
					$('#total_price').val('');
			// location.reload();
			}
		  })
		  .done(function( res ) {
			$("#bg_loader").hide();
				if(res['status'] == 200){
					$('#price').val(res['data']['post_price']);
					$('#total_price').val(res['data']['totalprice']);
				}else{
					$('#price').val('');
					$('#total_price').val('');
				}
		  });
	}else{
		//console.log('ไม่เข้า');
		
		$('#price').val('');
		$('#totsl_price').val('');
	}
	
}

function import_excel() {
	$('#div_error').hide();	
	var formData = new FormData();
	
	formData.append('file', $('#file')[0].files[0]);
	formData.append('csrf_token', $('#csrf_token').val());
	formData.append('date_import', $('#date_import').val());
	formData.append('import_round', $('#import_round').val());
	formData.append('page', 'Uploadfile');

	if($('#file').val() == ''){
		$('#div_error').html('กรุณาเลือกไฟล์อัปโหลด !!');
		$('#div_error').show();
		return;
	}else{
	 var extension = $('#file').val().replace(/^.*\./, '');
	 if(extension !='xlsx' && extension !='xls' && extension !='XLS' && extension !='XLSX'){
		 $('#div_error').html('กรุณาเลือกไฟล์ Excel เท้านั้น !!');
		$('#div_error').show();
		// console.log(extension);
		return;
	 }
	}
	$.ajax({
		   url : 'ajax/ajax_save_Work_post_out.php',
		   dataType : 'json',
		   type : 'POST',
		   data : formData,
		   processData: false,  // tell jQuery not to process the data
		   contentType: false,  // tell jQuery not to set contentType
		   success : function(res) {
			if(res['status'] == 401){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					window.location.reload();
				});
			}else if(res['status'] == 505){
				//console.log(res);
				$('#csrf_token').val(res['token']);
				alertify.alert('ผิดพลาด',"  "+res.message
				,function(){
					//window.location.reload();
				});
			}else if(res['status'] == 200){
				load_data_bydate();
				$('#csrf_token').val(res['token']);
				if($("#reset_send_form").prop("checked") == false){
					reset_send_form();
				}
				if($("#reset_resive_form").prop("checked") == false){
					reset_resive_form();
				}
				if($("#reset_resive_form").prop("checked") == false && $("#reset_send_form").prop("checked") == false){
					reset_resive_form();
					reset_send_form();
				}
			}
			   $('#bg_loader').hide();
		   }, beforeSend: function( xhr ) {
				$('#bg_loader').show();
				
			},
			error: function (error) {
				alert("เกิดข้อผิดหลาด");
				//location.reload();
			}
		});

}



function cancelwork() {
	var dataall = [];
	var tbl_data = $('#tb_keyin').DataTable();
	tbl_data.$('input[type="checkbox"]:checked').each(function(){
		 //console.log(this.value);
		dataall.push(this.value);
	});

	if(dataall.length < 1){
		alertify.alert("alert","ท่านยังไม่เลือกรายการ"); 
		return;
	}
	alertify.confirm('ยืนยันการลบ', 'กด "OK" เพื่อยกเลิกการส่ง',
	function(){ 
		var newdataall = dataall.join(",");
		$.ajax({
		method: "POST",
		dataType:'json',
		data:{
			id 		: newdataall,
			page	:'cancle_multiple'
		},
		url: "ajax/ajax_load_Work_Post_out.php",
		beforeSend: function() {
			// setting a timeout
			$("#bg_loader").show();
		},
		error: function (error) {
		  alert('error; ' + eval(error));
		  $("#bg_loader").hide();
		// location.reload();
		}
	  })
	  .done(function( res ) {
		$("#bg_loader").hide();
			load_data_bydate();
	  });
	  
	}, function(){ 
		alertify.error('Cancel')
	});
	

}	


function dowload_excel(){
	var r = confirm("ต้องการโหลดไฟล์!");
	if (r == true) {
		window.open("../themes/thai_post_template.xlsx", "_blank");
	} else {
		alertify.error('Cancel')
	}
	

}	


function set_form_val_all(data) {
	console.log(data);
	var new_emp_send = new Option(data.full_name_send, data.mr_send_emp_id, true, true);
	$('#emp_id_send').append(new_emp_send).trigger('change');
	$('#dep_id_send').val(data.mr_send_department_id).trigger('change');
	$("#emp_send_data").val(data.mr_send_emp_detail);
	$("#mr_type_post_id").val(data.mr_type_post_id);
	$("#name_re").val(data.name_resive);
	$("#lname_re").val(data.lname_resive);
	$("#address_re").val(data.mr_address);
	$("#sub_districts_code_re").val(data.mr_sub_districts_code);
	$("#districts_code_re").val(data.mr_districts_code);
	$("#provinces_code_re").val(data.mr_provinces_code);
	$("#post_barcode").val(data.num_doc);
	$("#post_barcode_re").val(data.sp_num_doc);
	$("#tel_re").val(data.mr_cus_tel);
	$("#sub_district_re").val(data.mr_sub_districts_name);
	$("#district_re").val(data.mr_districts_name);
	$("#province_re").val(data.mr_provinces_name);
	$("#post_code_re").val(data.mr_post_code);
	$("#topic").val(data.mr_topic);
	$("#work_remark").val(data.mr_work_remark);
	$("#mr_work_main_id").val(data.mr_work_main_id);
	$("#mr_work_post_id").val(data.mr_work_post_id);
	$("#weight").val(data.mr_post_weight);
	$("#quty").val(data.mr_post_amount);
	$("#price").val(data.mr_post_price);
	$("#total_price").val(data.mr_post_totalprice);
	$("#page").val('update_status');
	$("#round").val(data.mr_round_id);
	$("#tel_re").val(data.mr_cus_tel);
	
	$("#detail_sender").val(data.mr_send_emp_detail);
	var detail_receiver = '';
	detail_receiver += data.name_resive;
	detail_receiver += ' '+data.lname_resive;
	detail_receiver += ' '+data.mr_address;
	detail_receiver += ' '+data.mr_sub_districts_name;
	detail_receiver += ' '+data.mr_districts_name;
	detail_receiver += ' '+data.mr_provinces_name;
	detail_receiver += ' '+data.mr_post_code;
	detail_receiver += '  โทร: '+data.mr_cus_tel;
	$("#detail_receiver").val(detail_receiver);
	$('#btn-show-form-edit').removeAttr('disabled');
}
{% endblock %}
{% block Content2 %}

<div  class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="">
				<label><h3><b>ส่งออกไปรษณีย์ไทย</b></h3></label><br>
				
		   </div>	
		</div>
	</div>
	  
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
				  <a class="nav-link active" id="key-tab" data-toggle="tab" href="#key" role="tab" aria-controls="key" aria-selected="true"><label> คีย์คำสั่งงาน > ส่งออกไปรษณีย์ไทย </label></a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" id="import-tab" data-toggle="tab" href="#import" role="tab" aria-controls="import" aria-selected="false"><label> import excel คำสั่งงาน > ส่งออกไปรษณีย์ไทย </label></a>
				</li>
			  </ul>
			  
			  <div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="key" role="tabpanel" aria-labelledby="key-tab">
					<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-body">
										<form id="myform_data_senderandresive">
											<div class="form-row">
												<div class="col">
													<h5 class="card-title">รอบการนำส่งเอกสารประจำวัน</h5>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-2">
													<label for="date_send"><span class="text-muted font_mini" >วันที่นำส่ง:<span class="box_error" id="err_date_send"></span></span></label>
													<input data-error="#err_date_send" name="date_send" id="date_send" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
												  </div>
												 <div class="form-group col-md-2">
													<label for="round"><span class="text-muted font_mini" >รอบการรับส่ง:</span></label>
													<span class="box_error" id="err_round"></span>
													<select data-error="#err_round" class="form-control form-control-sm" id="round" name="round">
															<option value="" selected disabled>กรุณาเลือกรอบ</option>
															{% for r in round %}
															<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
															{% endfor %}
														</select>
													</div>
													<div class="form-group col-md-2">
														<label for="work_barcode"><span class="text-muted font_mini" >Barcode: <span class="box_error" id="err_work_barcode"></span></span></label>
														<input data-error="#err_work_barcode" name="work_barcode" id="work_barcode" class="form-control form-control-sm" type="text" value="" placeholder="Enter Barcode">
													  </div>
											</div>
											
										<div class="row">
											<div class="col-md-6">
												
												<h5 class="card-title">ข้อมูลรารา</h5>
												<div class="table-responsive">
													<table class="" style="width: 100%;">
													<tr>
															<th class="align-top"><span class="text-muted font_mini" >ประเภทการส่ง: </span></th>
															<td><span class="box_error" id="err_mr_type_post_id"></span>
																<select onchange="changPost_Type($(this).val());"  id="mr_type_post_id" name="mr_type_post_id" data-error="#err_mr_type_post_id" class="form-control form-control-sm" >
																	<option value="" selected disabled>กรุณาเลือกประเภทการส่ง</option>
																	{% for t in type_post %}
																	<option value="{{t.mr_type_post_id}}">{{t.mr_type_post_name}}</option>
																	{% endfor %}
																</select>
															</td>
														</tr>
														<tr>
															<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >เลขที่เอกสาร :</span></td>
															<td>
																<span class="box_error" id="err_post_barcode"></span>
																<input id="post_barcode" name="post_barcode" data-error="#err_post_barcode"  class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr> 
														<tr id="tr-post-barcode-re" style="display: none;">
															<th class="align-top"><span class="text-muted font_mini" >เลขตอบรับ : </span></th>
															<td>
																<span class="box_error" id="err_post_barcode_re"></span>
																<input id="post_barcode_re" name="post_barcode_re" data-error="#err_post_barcode_re"  class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr>
														<tr>
															<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >น้ำหนัก/กรัม:</span></td>
															<td>
																<span class="box_error" id="err_weight"></span>
																<input id="weight" name="weight" data-error="#err_weight"  value="0" onkeyup="setPost_Price();" onchange="setPost_Price();"  class="form-control form-control-sm" type="number" placeholder="-">
															</td>
														</tr>
														<tr>
															<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >จำนวน:</span></td>
															<td>
																<span class="box_error" id="err_quty"></span>
																<input id="quty" name="quty" data-error="#err_quty"  value="1" onkeyup="setPost_Price();" onchange="setPost_Price();" class="form-control form-control-sm" type="number" placeholder="-">
															</td>
														</tr>
														<tr>
															<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >ราคา:</span></td>
															<td>
																<span class="box_error" id="err_price"></span>
																<input id="price" name="price" data-error="#err_price"  value="0" class="form-control form-control-sm" type="number" readonly placeholder="-">
															</td>
														</tr>
														<tr>
															<td style="width: 180px;" class="align-top"><span class="text-muted font_mini" >ราคารวม:</span></td>
															<td>
																<span class="box_error" id="err_total_price"></span>
																<input id="total_price" name="total_price" data-error="#err_total_price"  value="0" class="form-control form-control-sm" type="number" readonly placeholder="-">
															</td>
														</tr>
													</table>
												</div>
												<div class="table-responsive" id="div_sender" style="display: none;">
												<h5 class="card-title">ข้อมูลผู้สั่งงาน</h5>
													<table class="" style="width: 100%;">
														<tr>
															<td style="width: 180px;"><span class="text-muted font_mini" >รหัสพนักงาน:</span></td>
															<td>
																<span class="box_error" id="err_emp_id_send"></span>
																<select data-error="#err_emp_id_send" class="form-control form-control-sm" id="emp_id_send" name="emp_id_send" style="width:100%;">
																	
																</select>
															</td>
														</tr>
														<tr>
															<td style="width: 180px;"><span class="text-muted font_mini" >หน่วยงาน:</span></td>
															<td>
																<span class="box_error" id="err_dep_id_send"></span>
																<select data-error="#err_dep_id_send" onchange="chang_dep_id();" class="form-control form-control-sm" id="dep_id_send" name="dep_id_send" style="width:100%;">
																	<option value="" selected disabled >กรุณาเลือกหน่วยงาน</option>
																		{% for d in department %}
																		<option value="{{ d.mr_department_id }}">{{ d.mr_department_code }} - {{ d.mr_department_name }}</option>
																		{% endfor %}
																</select>
															</td>
														</tr>
														<tr>
															<td class="align-top"><span class="text-muted font_mini" >รายละเอียดผู้สั่งงาน: </span></td>
															<td>
																<textarea class="form-control" id="emp_send_data" name="emp_send_data" rows="3" readonly></textarea>
															</td>
														</tr>
														<tr>
													</table>
												</div>

												
												<hr>
												

												<hr>
												
												
												
											</div>
											
											<div class="col-md-6">
												<div id="div_detail_receiver_sender">
													
													<h5 class="card-title">ผู้ส่งและผู้รับ <button type="button" id="btn-show-form-edit" class="btn btn-link btn-sm" disabled><i class="material-icons">edit</i></button></h5>
													<span class="text-muted font_mini" >ข้อมูลผู้สั่งงาน:</span>
													<textarea id="detail_sender" class="form-control"  rows="3" readonly></textarea>
													<span class="text-muted font_mini" >ข้อมูลผู้รับ:</span>
													<textarea id="detail_receiver" class="form-control"  rows="3" readonly></textarea>
												</div>
												<div class="table-responsive" style="display:none;" id="div_receiver">
													<h5 class="card-title">ข้อมูลผู้รับ</h5>
													<table class="" style="width: 100%;">
														
														<tr>
															<td style="width: 180px;" ><span class="text-muted font_mini" >ชื่อผู้รับ:</span></td>
															<td>
																<span class="box_error" id="err_name_re"></span>
																<input name="name_re" id="name_re" data-error="#err_name_re" class="form-control form-control-sm" type="text" placeholder="นามสกุล">
															</td>
														</tr>
														<tr>
															<td style="width: 180px;" ><span class="text-muted font_mini" >ชื่อผู้รับ:</span></td>
															<td>
																<span class="box_error" id="err_lname_re"></span>
																<input name="lname_re" id="lname_re" data-error="#err_lname_re" class="form-control form-control-sm" type="text" placeholder="นามสกุล">
															</td>
														</tr>

												
														<!-- 
														<tr>
															<td class="align-top"><span class="text-muted font_mini" >เบอร์มือถือ:</span></td>
															<td>
																<span class="box_error" id="err_tel_re"></span>
																<input id="tel_re" name="tel_re" data-error="#err_tel_re" class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr> 
														-->
														<tr>	
															<td class="align-top"><span class="text-muted font_mini" >เบอร์โทร:</span></td>
															<td>
																<span class="box_error" id="err_tel_re"></span>
																<input id="tel_re" name="tel_re" data-error="#err_tel_re"  class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr>
														<tr>
															<td class="align-top"><span class="text-muted font_mini" >ที่อยู่​:</span></td>
															<td>
																<span class="box_error" id="err_address_re"></span>
																<textarea id="address_re" name="address_re" data-error="#err_address_re"  class="form-control"  rows="2">-</textarea>
																<input type="hidden" name="sub_districts_code_re" id="sub_districts_code_re">
																<input type="hidden" name="districts_code_re" id="districts_code_re">
																<input type="hidden" name="provinces_code_re" id="provinces_code_re">
																<input type="hidden" name="csrf_token" id="csrf_token" value="{{csrf_token}}">
																<input type="hidden" name="mr_work_main_id" id="mr_work_main_id">
																<input type="hidden" name="mr_work_post_id" id="mr_work_post_id">
																<input type="hidden" name="page" id="page">
															</td>
														</tr>
														<tr>	
															<td class="align-top"><span class="text-muted font_mini" >แขวง/ตำบล:</span></td>
															<td>
																<span class="box_error" id="err_sub_district_re"></span>
																<input id="sub_district_re" name="sub_district_re" data-error="#err_sub_district_re"  class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr>
														
														<tr>
															<td class="align-top"><span class="text-muted font_mini" >เขต/อำเภอ:</span></td>
															<td>
																<span class="box_error" id="err_district_re"></span>
																<input id="district_re" name="district_re" data-error="#err_district_re"  class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr>
														<tr>
															<td class="align-top"><span class="text-muted font_mini" >จังหวัด:</span></td>
															<td>
																<span class="box_error" id="err_province_re"></span>
																<input id="province_re" name="province_re" data-error="#err_province_re"  class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr>
														<tr>
															<td class="align-top"><span class="text-muted font_mini" >รหัสไปรษณีย์:</span></td>
															<td>
																<span class="box_error" id="err_post_code_re"></span>
																<input id="post_code_re" name="post_code_re" data-error="#err_post_code_re"  class="form-control form-control-sm" type="text" placeholder="-">
															</td>
														</tr>

														<tr>
															<td class="align-top"><span class="text-muted font_mini" >รายละเอียด/หมายเหตุ:</span></td>
															<td>
																<span class="box_error" id="err_"></span>
																<textarea id="work_remark" name="work_remark" data-error="#"  class="form-control" id="" rows="4"></textarea>
															</td>
														</tr>

													</table>
													
												</div>{#
													<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_send_form();">ล้างข้อมูลผู้ส่ง </button>
													<button type="button" class="btn btn-outline-primary btn-sm" onclick="reset_resive_form();">ล้างข้อมูลผู้รับ </button>
													#}

												</div>
											</div>	
										</div>
										<div class="row">
											<div class="col-md-12">
												<hr>
												<br>
												<div class="form-group text-center" id="acction_update">
													<input class="" type="checkbox" id="reset_price_form" value="option1"> คงข้อมูลราคา
													<!-- <input class="" type="checkbox" id="reset_send_form" value="option1">คงข้อมูลผู้ส่ง
													<input class="" type="checkbox" id="reset_resive_form" value="option1">คงข้อมูลผู้รับ -->
													<br>
													<br>
													<button type="button" class="btn btn-outline-primary btn-sm" id="btn_save">บันทึกข้อมูล </button>
												</div>
												<div class="form-group text-center" id="acction_edit" style="display: none;">
													<!-- <input class="" type="checkbox" id="reset_send_form" value="option1">คงข้อมูลผู้ส่ง
													<input class="" type="checkbox" id="reset_resive_form" value="option1">คงข้อมูลผู้รับ -->
													<br>
													<br>
													<button type="button" class="btn btn-outline-primary btn-sm" id="btn_edit">บันทึกการแก้ไขข้อมูล </button>
													<button type="button" class="btn btn-outline-primary btn-sm" id="btn_cancle_edit">ยกเลิก </button>
												</div>
										</div>
									</form>
								</div>					
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="import" role="tabpanel" aria-labelledby="import-tab">
					<br>
						<form id="form_import_excel">
							<div class="form-group">
							<a onclick="$('#modal_showdata').modal({ backdrop: false});" data-toggle="tooltip" data-placement="top" title="เลือกไฟล์ Excel ของท่าน"><i class="material-icons">help</i></a>
								<label for="file">
									<div class="form-group" id="detail_receiver_head">
										<h4>Import File <button class="btn btn-link" onclick="dowload_excel();">Doowload Template</button></h4>  
									</div>
								</label><br>
								<label for="date_import">วันที่นำส่ง</label>
								<span class="box_error" id="err_date_import"></span>
								<input data-error="#err_date_import" name="date_import" id="date_import" class="form-control form-control-sm col-md-2 c0l-12" type="text" value="{{today}}" placeholder="{{today}}">
								<br>
								<br>
									<span class="box_error" id="err_import_round"></span>
									<select data-error="#err_import_round" class="form-control form-control-sm col-md-2 c0l-12" id="import_round" name="import_round">
										<option value="">กรุณาเลือกรอบ</option>
										{% for r in round %}
										<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
										{% endfor %}
									</select>
								<br>

								<input type="file" class="form-control-file" id="file">
							</div>
							<br>
							<button onclick="import_excel();" id="btn_fileUpload" type="button" class="btn btn-warning">Upload</button>
							<input type="hidden" id = "csrf_token" name="csrf_token" value="{{csrf}}"></input>
							<br>
							<hr>
							<br>
							<div id="div_error"class="alert alert-danger" role="alert" style="display: none;">
							  
							</div>
						</div>
						</form>
					<br>
				</div>
			  </div>
		</div>
	</div>
	

<div class="row">
	<div class="col-md-12 text-right">
	<form id="form_print" action="pp.php" method="post" target="_blank">
		<hr>
		<table>
			<tr>
				<td>
					<span class="box_error" id="err_round"></span>
					<select data-error="#err_round" class="form-control form-control-sm" id="round_printreper" name="round_printreper">
						<option value="">กรุณาเลือกรอบ</option>
						{% for r in round %}
						<option value="{{r.mr_round_id}}">{{r.mr_round_name}}</option>
						{% endfor %}
					</select>
				</td>
				
				<td>
					<span class="box_error" id="err_date_report"></span>
					<input data-error="#err_date_report" name="date_report" id="date_report" class="form-control form-control-sm" type="text" value="{{today}}" placeholder="{{today}}">
				</td>
				<td>
					<button onclick="load_data_bydate();" type="button" class="btn btn-sm btn-outline-secondary" id="">ค้นหา</button>
					<button onclick="print_option(1);" type="button" class="btn btn-sm btn-outline-info" id="">พิมพ์ใบงาน</button>
				<!-- 				
					<button onclick="print_option(2);" type="button" class="btn btn-sm btn-outline-secondary" id="">พิมพ์ใบคุม</button>
				-->
				</td>
			</tr>
		</table>
	</form>
	</div>	
</div>



<div class="row">
	<div class="col-md-12">
		<hr>
		<h5 class="card-title">รายการเอกสาร</h5>
		<table class="table" id="tb_keyin">
			<thead class="thead-light">
			  <tr>
				<th width="5%" scope="col">#</th>
			
				<th width="5%" scope="col">
							<label class="custom-control custom-checkbox">
								<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description"></span>
							</label>
						
						
					  </div>
					
				</th>
				<th width="5%" scope="col">
					<button onclick="cancelwork();" type="button" class="btn btn-danger btn-sm">
						ลบรายการ
					</button>
				</th>
				<th width="5%" scope="col">ประเภท</th>
				<th width="10%" scope="col">วันที่</th>
				<th width="10%" scope="col">เลขที่เอกสาร</th>
				<th width="10%" scope="col">สถานะ</th>
				<th width="10%" scope="col">ชื่อผู้ส่ง</th>
				<th width="10%" scope="col">รอบ</th>
				<th width="10%" scope="col">ผู้รับ</th>
				<th width="15%" scope="col">ที่อยู่</th>
				<th width="30%"scope="col">เบอร์โทร</th>
				<th width="30%"scope="col">หมายเหตุ</th>
			  </tr>
			</thead>
			<tbody>
		
			</tbody>
		</table>
	</div>
</div>
</div>





<div id="bg_loader" style="display: none;">
	<img id = 'loader'src="../themes/images/spinner.gif">
</div>




{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

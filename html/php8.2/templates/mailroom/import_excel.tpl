{% extends "base_emp2.tpl" %}


{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">

<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>


{% endblock %}

{% block styleReady %}
	.widthText {
		width: 300px;
	}

	
	#lst_warning {
		color: red;
	}
	.loading {
		position: fixed;
		top: 40%;
		left: 50%;
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
		z-index: 1000;
	}
	
	.img_loading {
		  width: 350px;
		  height: auto;
		  
	}
{% endblock %}

{% block domReady %}
//var url = "https://www.pivot-services.com/mailroom_tmb/employee/";
var url = "";



$('#name_emp').select2({
				placeholder: "ค้นหาผู้รับ",
				theme: 'bootstrap4',
				ajax: {
					//url: "https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_getdataemployee_select.php",
					url: "ajax/ajax_getdataemployee_select_search.php",
					dataType: "json",
					delay: 250,
					processResults: function (data) {
						return {
							 results : data
						};
					},
					cache: true
				},
				dropdownParent: $('#editCon')

		}).on('select2:select', function(e) {
			//console.log(e.params.data);
			setForm(e.params.data);
		});

		var table = $('#tb_work_order').DataTable({ 
			"responsive": true,
			"language": {
				"emptyTable": "ไม่มีข้อมูล!"
			},
			"pageLength": 10, 
			'columns': [
				{ 'data':'no' },
				{ 'data':'edit' },
				{ 'data':'mr_emp_name'},
				{ 'data':'mr_emp_lastname' },
				{ 'data':'mr_emp_code' },
				{ 'data':'mr_emp_email' },
				{ 'data':'mr_date_import' },
				{ 'data':'update_date' },
				{ 'data':'position_name' },
				{ 'data':'department_name' },
				{ 'data':'floor_name' },
				{ 'data':'branchname' }
			]
		});

		var table = $('#tb_data_contact').DataTable({ 
			"responsive": true,
			"language": {
				"emptyTable": "ไม่มีข้อมูล!"
			},
			"pageLength": 10, 
			'columns': [
				{ 'data':'no' },
				{ 'data':'edit' },
				{ 'data':'shows' },
				{ 'data':'department_code'},
				{ 'data':'department_name' },
				{ 'data':'emp_code' },
				{ 'data':'mr_contact_name' },
				{ 'data':'emp_tel' },
				{ 'data':'remark' },
				{ 'data':'con_sysdate' },
			]
		});

	getData();
	$('#btn_upload').on('click',function() {
		var data = $('#txt_upload').prop("files");
		var form_val = $('#txt_upload').prop("files")[0];
	
		// console.log(data);
		
			$('#btn_upload').text('Uploading...').attr('disabled', 'disabled');
			var form_data = new FormData();
			form_data.append('file', form_val);
			//if(data[0]['type'] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || data[0]['type'] == "application/vnd.ms-excel"){
					$.ajax({
					url: 'ajax_readFile_Emp_csv.php',
               		cache: false,
                	contentType: false,
                	processData: false,
                	data: form_data,                         
					type: 'post',
					dataType: 'html',
					success: function(res){
					
						if( res <= 0 ) {
							alert('ไม่มีข้อมูลนำเข้า');
							clearForm();
							$('#btn_upload').text('Upload').removeAttr('disabled');
							getData();
						} else {
							$('#btn_upload').text('Upload').removeAttr('disabled');
							clearForm();
							alert('การอัพโหลดรายชื่อพนักงานจำนวน '+ res +' รายชื่อ');
							getData();
						}
					}
			});
			
		//}
	});

	$('#btn_clear').click(function() {
        window.history.back();
	}); 
	
	$('#branch').select2({
		theme: 'bootstrap4',
		placeholder: "ค้นหาสาขา",
		ajax: {
			url: "ajax/ajax_getdata_branch_select.php",
			dataType: "json",
			delay: 250,
			data: function (params) {
				var query = {
				  search: params.term,
				  type: 'public',
				  page: 'branch'
				}
				return query;
			  }	,
			processResults: function (data) {
				return {
					 results : data
				};
			},
			cache: true
		}
	});

	$('#edit_branch').select2({
		theme: 'bootstrap4',
		placeholder: "ค้นหาสาขา",
		ajax: {
			url: "ajax/ajax_getdata_branch_select.php",
			dataType: "json",
			delay: 250,
			data: function (params) {
				var query = {
				  search: params.term,
				  type: 'public',
				  page: 'branch'
				}
				return query;
			  }	,
			processResults: function (data) {
				return {
					 results : data
				};
			},
			cache: true
		}
	});
	$('#department').select2({
		theme: 'bootstrap4',
		placeholder: "ค้นหาแผนก/หน่วยงาน",
		ajax: {
			url: "ajax/ajax_getdata_branch_select.php",
			dataType: "json",
			delay: 250,
			data: function (params) {
				var query = {
				  search: params.term,
				  type: 'public',
				  page: 'department'
				}
				return query;
			  },
			processResults: function (data) {
				return {
					 results : data
				};
			},
			cache: true
		}
	});
	$('#position').select2({
		theme: 'bootstrap4',
		placeholder: "ค้นหาตำแหน่ง",
		ajax: {
			url: "ajax/ajax_getdata_branch_select.php",
			dataType: "json",
			delay: 250,
			data: function (params) {
				var query = {
				  search: params.term,
				  type: 'public',
				  page: 'position'
				}
				return query;
			  },
			processResults: function (data) {
				return {
					 results : data
				};
			},
			cache: true
		}
	});
	$('#floor').select2({
		theme: 'bootstrap4',
		placeholder: "ค้นหาชั้น",
		ajax: {
			url: "ajax/ajax_getdata_branch_select.php",
			dataType: "json",
			delay: 250,
			data: function (params) {
				var query = {
				  search: params.term,
				  type: 'public',
				  page: 'floor'
				}
				return query;
			  },
			processResults: function (data) {
				return {
					 results : data
				};
			},
			cache: true
		}
	});

	$('#emptype').select2({
		theme: 'bootstrap4',
		width: '100%' 
	});
	$('#group').select2({ 
		theme: 'bootstrap4',
		theme: 'bootstrap4',
		width: '100%' 
	});

	$('#emp_hub').select2({
		theme: 'bootstrap4',
		placeholder: "ค้นหาสาขา",
		ajax: {
			url: "ajax/ajax_getdata_branch_select.php",
			dataType: "json",
			delay: 250,
			data: function (params) {
				var query = {
				  search: params.term,
				  type: 'public',
				  page: 'hub'
				}
				return query;
			  },
			processResults: function (data) {
				return {
					 results : data
				};
			},
			cache: true
		}
	});



				
$( "#btnCon_save" ).click(function() {
										
	var emp_hub 			= $('#emp_hub').val();
	var con_type 			= $('#con_type').val();
	var con_remark 			= $('#con_remark').val();
	var con_floor 			= $('#con_floor').val();
	var con_department_name = $('#con_department_name').val();
	var mr_emp_tel 			= $('#con_emp_tel').val();
	var con_emp_name 		= $('#con_emp_name').val();
	var mr_emp_code 		= $('#mr_emp_code').val();
	var mr_emp_id 			= $('#name_emp').val();
	var department_code 	= $('#department_code').val();
	var mr_contact_id 		= $('#mr_contact_id').val();
	var type_add 			= $('#type_add').val();
	
	var dataselect2 = $('#name_emp').select2('data');
	con_emp_name	= dataselect2[0].text;
	
	
	$( ".myErrorClass" ).removeClass( "myErrorClass" );
	
	if(mr_emp_id == ''){
		$( "#name_emp" ).addClass( "myErrorClass" );
		alertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ ชื่อพนักงาน');
		return;
	}if(mr_emp_code == ''){
		$( "#mr_emp_code" ).addClass( "myErrorClass" );
		alertify.alert('ข้อมูลไม่ครบถ้วน','ไม่พบ รหัสพนักงาน  กรุณาติดต่อห้องสารบัญกลาง');
		return;
	}if(mr_emp_tel == ''){
		$( "#mr_emp_tel" ).addClass( "myErrorClass" );
		alertify.alert('ข้อมูลไม่ครบถ้วน','กรุณาระบุ เบอร์โทร');
		return;
	}
	
alertify.confirm("ตรวจสอบข้อมูล", "กรุณาตรวจสอบข้อมูลให้ครับถ้วน  หากบันทึกแล้วจะไม่สามารถแก้ไขได้อีก",
function(){
$.ajax({
		  dataType: "json",
		  method: "POST",
		  url: "ajax/ajax_save_data_contact.php",
		  data: { 
			    emp_hub 			 	 	: con_type 			 	,
				con_type 			 	 	: con_type 			 	,
				department_code 		 	: department_code 			 	,
				con_emp_name 			 	: con_emp_name 			 	,
				con_remark 			 	 	: con_remark 			 	,
				con_floor 			 	 	: con_floor 			 	,
				con_department_name 	 	: con_department_name 	,
				type_add 	 				: type_add 	,
				mr_emp_tel 	 				: mr_emp_tel 	,
				mr_emp_code  				: mr_emp_code ,
				mr_emp_id 	 				: mr_emp_id 	,
				mr_contact_id				: mr_contact_id
		  }
		}).done(function(data) {
			$('#csrf_token').val(data.token);

			alertify.alert(data['st'],data['msg'],
			function(){
				getData2();
				$('#editCon').modal('hide');
			});
		});
},function(){
	alertify.error('Cancel');
});


//alert( "Handler for .click() called." );
//is-invalid
});




{% endblock %}

{% block javaScript %}

function edit_empdata(id,email,name,lname,mr_branch_id,branchname){
	$('#div_edit_email').slideDown('slow');
	$('#edit_email').val(email)
	$('#edit_name').val(name)
	$('#edit_lname').val(lname)
	$('#emp_id').val(id)
	
	var newOption = new Option(branchname, mr_branch_id, false, false);
	$('#edit_branch').append(newOption).trigger('change');
	$('#edit_branch').append(newOption).trigger('change');
	$('#edit_branch').val(mr_branch_id); // Select the option with a value of '1'
	$('#edit_branch').trigger('change'); // Notify any JS components that the value changed

	//$("#edit_branch").select2(mr_branch_id, branchname);
 console.log(newOption);
 console.log(mr_branch_id);
 console.log(branchname);

}


function getData2(){	
	$.ajax({
	method: "POST",
	dataType: "json",
	url: "ajax/ajax_get_contact.php",
	beforeSend: function( xhr ) {
		$('.loading').show();
	},
	error: function (xhr, ajaxOptions, thrownError) {
		alert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
		location.reload();
	}
	})
	.done(function( msg ) {
		$('.loading').hide();
		$('#tb_data_contact').DataTable().clear().draw();
		$('#tb_data_contact').DataTable().rows.add(msg.data).draw(); 
		
	});
}


function getData(){
	$.ajax({
	method: "POST",
	dataType: "json",
	url: "ajax/ajax_get_emp.php",
	beforeSend: function( xhr ) {
		$('.loading').show();
	},
	error: function (xhr, ajaxOptions, thrownError) {
		alert('เกิดข้อผิดผลาด ข้อมูลที่พบมีมากเกิน!');
		//location.reload();
	}
	})
	.done(function( msg ) {
		$('.loading').hide();
		$('#tb_work_order').DataTable().clear().draw();
		$('#tb_work_order').DataTable().rows.add(msg.data).draw(); 
		
	});
};


	var clearForm = function() {
		$('#txt_upload').val("");
	}
	function chang_emptype(){
		$('#group').val("");
		var emptype = $('#emptype').val();
		if(emptype == 2){
			$('#div_floor').hide();
		}else{
			$('#div_floor').show();
		}
		chang_group();
	}

	function chang_group(){
		var emptype = $('#emptype').val();
		var group = $('#group').val();
		if(group == "pivot"){
			$('.not_pivot').hide();
			if(emptype == 2){
				$('#div_hub').show();
			}else{
				$('#div_hub').hide();
			}
		}else{
			$('.not_pivot').show();
			$('#div_hub').hide();
		}
	}



	function resign_empdata(code){
		alertify.confirm('ลบพนักงานออกจากระบบ','ท่านต้องการลบนพนักงาน '+code+' ออกจากระบบ หรือไม่', 
			function(){
				$.ajax({
					url: "ajax/ajax_save_employee.php",
					dataType: "json",
					data: { 
						mr_emp_code 		: code 		, 
						page 				: 'resign' 	
					},
					beforeSend: function( xhr ) {
						$('.loading').show();
					},error: function (error) {
							alert('เกิดข้อผิดพลาดกรุณาลองใหม่');
							location.reload();
		
					},
				  })
					.done(function( data ) {
						//getData()
						$('.loading').hide();
						$('#btn_save').text('Save').removeAttr('disabled');
						if(data['error'] == 'success'){
							$('#alert_success').show(); 
							$('#alert_error').hide(); 
							getData();
						}else{
							$('#alert_success').hide(); 
							$('#alert_error').show(); 
							$('#text_error').text(data['msg']); 
						}
						
					});	
			},function() {
		}).set('labels', {ok:'ลบพนักงาน', cancel:'ไม่'});
	}
	function update_email(){
		var edit_email 	= $('#edit_email').val();
		var edit_name 	= $('#edit_name').val();
		var edit_lname 	= $('#edit_lname').val();
		var edit_branch 	= $('#edit_branch').val();
		var emp_id	 	= $('#emp_id').val();
		if(edit_email == ''){
			alert('กรุณาระบบุ E-mail');
			return 
		}
		
		$.ajax({
			url: "ajax/ajax_save_employee.php",
			dataType: "json",
			data: { 
				emp_id	 		: emp_id 	, 
				edit_email 		: edit_email 	, 
				edit_name 		: edit_name 	, 
				edit_lname 		: edit_lname 	, 
				edit_branch 	: edit_branch 	, 
				page 			: 'edit' 	
			},
			beforeSend: function( xhr ) {
				$('.loading').show();
			},error: function (error) {
					alert('เกิดข้อผิดพลาดกรุณาลองใหม่');
					location.reload();

			},
		  })
			.done(function( data ) {
				$('.loading').hide();
				if(data['error'] == 'success'){
					alert('บันทึกสำเร็จ');
					location.reload();
				}else{
					alert('เกิดข้อผิดพลาดกรุณาลองใหม่');
					location.reload();
				}
				
			});


	}
	function save_emp(){
		var emp_hub 			= $('#emp_hub').val();
		var emptype 	= $('#emptype').val();
		var code 		= $('#code').val();
		var name 		= $('#name').val();
		var lname 		= $('#lname').val();
		var email 		= $('#email').val();
		var branch 		= $('#branch').val();
		var department 	= $('#department').val();
		var position 	= $('#position').val();
		var tel 		= $('#tel').val();
		var floor 		= $('#floor').val();
		var group 		= $('#group').val();




		$.ajax({
			url: "ajax/ajax_save_employee.php",
			dataType: "json",
			data: { 
				emp_hub 	: emp_hub 	, 
				emptype 	: emptype 	, 
				group 		: group 	, 
				code 		: code 		, 
				name 		: name 		, 
				floor 		: floor 		, 
				lname 		: lname 		, 
				email 		: email 		, 
				branch 		: branch 		, 
				department 	: department 	, 
				tel 		: tel 	, 
				position 	: position 	
			},
			beforeSend: function( xhr ) {
				$('#btn_save').text('saving...').attr('disabled', 'disabled');
				$('.loading').show();
			},error: function (error) {
					alert('เกิดข้อผิดพลาดกรุณาลองใหม่');
					//location.reload();

			},
		  })
			.done(function( data ) {
				//getData()
				$('.loading').hide();
				$('#btn_save').text('Save').removeAttr('disabled');
				if(data['error'] == 'success'){
					$('#alert_success').show(); 
					$('#alert_success').text(data['msg']); 
					$('#alert_error').hide(); 
					//location.reload();
				}else{
					$('#alert_success').hide(); 
					$('#alert_error').show(); 
					$('#text_error').text(data['msg']); 
				}
				
			});


	}

			
function load_modal(mr_contact_id,type) {	
window.scrollTo(0, 0);
 $('#editCon').modal({
	 backdrop: false,
	 keyboard : true
	});

$.ajax({
	  dataType: "json",
	  method: "POST",
	  url: "ajax/ajax_save_data_contact.php",
	  data: { 	
				mr_contact_id : mr_contact_id,
				page : 'getdata'
	  }
	}).done(function(data) {
		//alert('55555');
		if(type!= "add"){
			$('#mr_contact_id').val(data['mr_contact_id']);
			
		}
		
		$('#type_add').val(type);
		$('#mr_emp_code').val(data['emp_code']);
		$('#con_emp_tel').val(data['emp_tel']);
		$('#con_type').val(data['type']);

		$('#con_department_name').val(data['department_name']);
		$('#con_floor').val(data['floor']);
		$('#con_remark').val(data['remark']);
		$('#department_code').val(data['department_code']);
	});
	
}


function setForm(data) {
	var emp_id = parseInt(data.id);
	var fullname = data.text;
	$.ajax({
		//url: 'https://www.pivot-services.com/mailroom_tmb/employee/ajax/ajax_autocompress_name.php',
		url: 'ajax/ajax_autocompress_name.php',
		type: 'POST',
		data: {
			name_receiver_select: emp_id
		},
		dataType: 'json',
		success: function(res) {
			if(res.status == 200) {
				if(res.data != false) {
					$("#con_emp_tel").val(res['data']['mr_emp_tel']);
					$("#mr_emp_code").val(res['data']['mr_emp_code']);
				}
			}
			
		}
	})
}



{% endblock %}
	
{% block Content2 %}

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" onclick="getData();" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Import Files</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" onclick="getData2();" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">รายชื่อติดต่อ</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">





<div class="page-header">
	<h1><strong>Import Files</strong></h1>
</div>	
<div class="card" >
  <div class="card-body">
	
		<div class="row">
			
			<br>
			<div class="col-md-5" style="">	
					<form action="#" method="POST" id="formUpload" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal">
						<div class="row">
							<div class="col-md-12">	 
								<div class="form-group">
									<br>
									<br>
									<div class="col-sm-6" style="left:50px;">
										<input type="file" name="txt_upload" id="txt_upload">
									</div>
								</div>
								<div class="form-group">
									<label for="btn_upload" class="label-control col-sm-2"></label>
									<div class="col-sm-12">
										
										<button type="button" class="btn btn-default" id="btn_clear"><i class="material-icons">reply_all</i> Back</button>
										<button type="button" class="btn btn-secondary" id="btn_add" onclick="$('#div_add').slideToggle('slow');"><i class="material-icons">person_add</i>  Add</button>
										<button type="button" class="btn btn-primary" name="btn_upload" id="btn_upload"><i class="material-icons">publish</i>Upload</button>
										<button onclick="$('#formExport').first().submit();"type="button" class="btn btn-primary" name="btn_export" id="btn_export"><i class="material-icons">archive</i>Export</button>
									
									</div>
								</div>
							</div>
						</div>	
					</form>
					<form id="formExport" action="export_excel_emp_data.php" method="POST" enctype="multipart/form-data" accept-charset="utf-8"></form>

			</div>
			<div class="col-md-7">
				<div id="lst_warning">
					<label for="warning_text"><strong>ข้อแนะนำ !</strong></label>
					<ul>
						<li>สามารถ upload ได้เฉพาะไฟล์นามสกุล .csv เท่านั้น</li>
						<li>โปรดตรวจสอบและจัดการไฟล์เอกสารก่อนการ upload ไฟล์ทุกครั้ง มิฉะนั้นอาจทำให้เกิดข้อผิดพลาดขึ้นได้</li>
						<li>หากข้อมูลไม่ครบ ระบบจะไม่ทำการเพิ่มรายชื่อพนักงานผู้นั้นเข้าสู่ระบบ </li>
					</ul>
				</div>
		<!-- 		<a href='./template_emp_data.xlsx' target="_blank" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลด template emp</a><br><br> -->
			</div>
			
			<br>
		</div>
	
		<div id="div_edit_email" style="display: none;">
					<div class="row">
						
							<div class="form-group col-md-3">
								<label for="edit_name">name :</label>
								<input type="text" class="form-control" id="edit_name">
							</div>
							<div class="form-group col-md-3">
								<label for="edit_lname">last name :</label>
								<input type="text" class="form-control" id="edit_lname">
							</div>
							<div class="form-group col-md-3">
								<label for="edit_email">Email :</label>
								<input type="hidden" class="form-control" id="emp_id">
								<input type="text" class="form-control" id="edit_email">
							</div>
							<div class="form-group col-md-3">
								<label for="edit_branch">ที่อยู่สาขา : </label>
								<select class="form-control-lg" id="edit_branch" style="width:100%;"></select>
							</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<br>
							<button type="button" class="btn btn-primary" onclick="update_email();">
								<i class="material-icons">unarchive	</i>save
							</button>
							<button type="button" class="btn btn-danger " onclick="$('#div_edit_email').slideToggle('slow');">
								<i class="material-icons">reply_all</i>cancle
							</button>
						
							<br>	
						</div>
					</div>
		</div>	
		<div id="div_add" style="display: none;">
			<div class="row">
				<div class="col-md-12">	 
							<br>
							<br>
							<br>
							<u><h4>เพิ่มข้อมูลพนักงาน</h4></u>
							<br>
								<form>	
									<div class="form-row">
										<div class="form-group col-md-4">
												<label for="emptype">สสถานที่ปฏิบัติงาน :</label>
												<select id="emptype" class="form-control" onchange="chang_emptype();">
													  <option value="1" selected>สำนักงานใหญ่</option>
													  <option value="2">สาขา</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="group">สังกัดพนักงาน :</label>
												<select id="group" class="form-control" onchange="chang_group();">
													  <option value="" selected>เลือกสังกัด :</option>
													  <option value="tmb">TMB</option>
													  <option value="outsource">Outsource</option>
													  <option value="pivot">Pivot</option>
												</select>
											</div>
									</div>
										<div class="form-row">
											<div class="form-group col-md-1">
												<label for="code">รหัสพนักงาน :</label>
												<input type="text" class="form-control" id="code">
											 </div>
										  <div class="form-group col-md-4">
											<label for="name">ชื่อ</label>
											<input type="text" class="form-control" id="name">
										  </div>
										  <div class="form-group col-md-4">
											<label for="lname">นามสกุล :</label>
											<input type="text" class="form-control" id="lname">
										  </div>
										 
										</div>
										
										<div class="form-row">
											<div class="form-group col-md-3 not_pivot">
												<label for="email">อีเมล :</label>
												<input type="email" class="form-control" id="email">
											</div>
											<div class="form-group col-md-3 not_pivot">
												<label for="tel">เบอร์โทร :</label>
												<input type="mr_emp_tel" class="form-control" id="tel">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-3 not_pivot">
												<label for="branch">ที่อยู่สาขา : </label>
												<select class="form-control-lg" id="branch" style="width:100%;"></select>
											</div>
										  <div class="form-group col-md-3 not_pivot">
											<label for="department">แผนก/หน่วยงาน : </label>
											<select class="form-control-lg" id="department" style="width:100%;"></select>
										  </div>
										  <div class="form-group col-md-1" id="div_floor">
													<label for="floor">ชั้น :</label>
													<select id="floor" class="form-control-lg" style="width:100%;">
													</select>
											</div>
											<div class="form-group col-md-1" id="div_hub" style="display: none;">
												<label for="emp_hub">ศูน(ฮับ) :</label style="width:100%;">
												<select id="emp_hub" class="form-control-lg" style="width:200px;">
												</select>
										</div>
										  <div class="form-group col-md-3 not_pivot">
												<label for="position">ตำแหน่ง :</label>
												<select id="position" class="form-control-lg" style="width:100%;">
												</select>
											</div>
											
										</div>
										
										<button type="button" class="btn btn-primary" onclick="save_emp();">
											<i class="material-icons">unarchive	</i>save
										</button>
										<button type="button" class="btn btn-danger " onclick="$('#div_add').slideToggle('slow');">
											<i class="material-icons">reply_all</i>cancle
										</button>
											<br>
											<br>
											<div id="alert_success"class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
												<strong>Success : </strong> บันทึกข้อมูลสำเร็จ


												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												  <span aria-hidden="true">&times;</span>
												</button>
											  </div>

											<div id="alert_error" class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
												<strong>Error!:  </strong><span id="text_error"></span>.


												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												  <span aria-hidden="true">&times;</span>
												</button>
											  </div>
											<br>
											<br>
									  </form>

						</div>
				</div>
			</div>
		
	</div>
</div>		

<div class="card" style="margin-top:10px;" >
  <div class="card-body">		
		
		<h3><strong>รายชื่อพนักงาน</strong></h3>
        <div class="table-responsive">
          <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
            <thead>
              <tr>
                <th>No</th>
				<th>#</th>
                <th>ชื่อ</th>
                <th>นามสกุล</th>
                <th>รหัสพนักงาน</th>
                <th>Email</th>
                <th>วันที่นำเข้า</th>
                <th>วันที่อัปเดทข้อมูลล่าสุด</th>
                <th>ตำแหน่ง</th>
                <th>แผนก</th>
                <th>ชั้น</th>
                <th>สาขา</th>
                
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>


          


        </div>
         
		
		
	</div>
</div>
</div>
<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
<table class="table table-bordered table-hover display responsive no-wrap" id="tb_data_contact">
            <thead>
              <tr>
                <th>No</th>
				<th>#</th>
				<th>แสดง</th>
                <th>รหัสหน่วยงาน</th>
                <th>ชื่อหน่วยงาน</th>
                <th>รหัสพนักงาน</th>
                <th>ชื่อผู้ติดต่อ</th>
                <th>โทร</th>
                <th>หมายเหตุ</th>
                <th>sysdate</th>
                
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
</div>
</div>

<div class='loading' style="display: none;">
	<img src="../themes/images/loading.gif" class='img_loading'>
</div>


	
<!-- Modal -->
<div class="modal fade" id="editCon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">เปลี่ยนข้อมูลผู้ติดต่อ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div class="form-group">
				<label for="">แสดงข้อมูลที่</label><br>
				<select id="con_type" class="form-control">
					<option value="1" selected>สำนักงานใหญ่</option>
					<option value="2">สาขา</option>
			  </select>
				
			  </div>
			<div class="form-group">
				<label for="">ชื่อ - นามสกุล</label><br>
				<select class="form-control" id="name_emp" style="width:100%;">
				  <option value="">{{con_data.0.emp_code}} : {{con_data.0.mr_contact_name}}</option>
				  {% for emp in all_emp %}
				  <option value="{{ emp.mr_emp_id }}">{{emp.mr_emp_code}} : {{emp.mr_emp_name}}{{emp.mr_emp_lastname}}</option>
				  {% endfor %}
				</select>
				
			  </div>
			 
			  
			 <div class="form-group">
				<label for="">เบอร์โทร</label>
				<input type="text" class="form-control" id="con_emp_tel" value="">
				<input type="hidden" class="form-control" id="type_add" value="">
				<input type="hidden" class="form-control" id="mr_contact_id" value="">
				<input type="hidden" class="form-control" id="mr_emp_code" value="">
				<input type="hidden" class="form-control" id="con_emp_name" value="">
			  </div>
			  <div class="form-group">
				<label for="">รหัสหน่วยงาน</label>
				<input type="text" class="form-control" id="department_code" value="">
			  </div>
			  <div class="form-group">
				<label for="">ชื่อหน่วยงาน</label>
				<input type="text" class="form-control" id="con_department_name" value="">
			  </div>
			  <div class="form-group">
				<label for="">ชั้น</label>
				<input type="text" class="form-control" id="con_floor" value="" >
			  </div>
			  <div class="form-group">
				<label for="">หมายเหตุ</label>
				 <textarea class="form-control" id="con_remark" rows="3"></textarea >
			  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btnCon_save" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
	
				
			
{% endblock %}


{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }
      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}
      #tb_work_order {
        font-size: 13px;
      }
	  .panel {
		margin-bottom : 5px;
	  }
#loader{
	  width:5%;
	  display:table;
	  margin: auto;
	  border-radius:50%;
	}#bg_loader{
		position:absolute;
		top:0px;
		background-color:rgba(255,255,255,0.7);
		height:100%;
		width:100%;
	}

{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">

<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>

{% endblock %}

{% block domReady %}
$('#bg_loader').hide()
		load_data();
	var table = $('#tb_work_order').DataTable({
			'responsive': true,
			'columns': [
				{ 'data':'no' },
				{ 'data':'sys_timestamp'},
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_type_work_name' },
				{ 'data':'branch' },
				{ 'data':'barcode_tnt' },
				{ 'data':'send_round' },
				{ 'data':'mr_status_name' }
			],
			
			'scrollCollapse': true 
		});
$('#select-all').on('click', function(){
   // Check/uncheck all checkboxes in the table
   var rows = table.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});
			
		
		
{% endblock %}


{% block javaScript %}
		
	function saveround() {
		var dataall 		= [];
		var mr_user_id      = $('#mr_user_id').val();
		var tnt_tracking    = $('#tnt_tracking').val();
		var tbl_data 		= $('#tb_work_order').DataTable();
		
		 tbl_data.$('input[type="checkbox"]:checked').each(function(){
			 //console.log(this.value);
			 dataall.push(this.value);
		  });
		  //console.log(tbl_data);
		  hub_id = $('#hub_id').val();
		  if(hub_id == 'all_hub'){
				 alertify.alert("alert","ท่านยังไม่ ระบุ Hub"); 
				 return;
			}

		  if(hub_id == 'all_branch'){
			   if(tnt_tracking == '' || tnt_tracking == null){
				 alertify.alert("alert","ท่านยังไม่ ระบุ TNT Tracking"); 
				 return;
			   }
			}else{
				if(mr_user_id == ''){
					alertify.alert("alert","ท่านยังไม่เลือก Mess"); 
					return;
				}
			}	
		  
		  if(dataall.length < 1){
			 alertify.alert("alert","ท่านยังไม่เลือกงาน"); 
			 return;
		  }
		  
		var newdataall = dataall.join(",");

		$.ajax({
			url: "ajax/ajax_saveround.php",
			type: "post",
			dataType:'json',
			data: {
				'main_id'		: newdataall,
				'mr_user_id'	: mr_user_id,
				'hub_id'		: hub_id,
				'tnt_tracking'	: tnt_tracking
				
			},
		   beforeSend: function( xhr ) {
				$('#bg_loader').show();
			}
		}).done(function( msg ) {
			if(msg.status == 500 || msg.status == 401){
				alertify.alert('ผิดพลาด',"บันทึกไม่สำเร็จ  "+msg.message,function(){window.location.reload();});
			}else{
				$('#bg_loader').hide();
				$('#mr_branch_id').val('all_branch').trigger('change');
				//$('#hub_id').val('');
				load_data();
			}
		});
	}
	
	function print_all() {
		var dataall = [];
		var tbl_data = $('#tb_work_order').DataTable();
		 tbl_data.$('input[type="checkbox"]:checked').each(function(){
			dataall.push(this.value);
		});
		if(dataall.length < 1){
			alertify.alert("alert","ท่านยังไม่เลือกงาน"); 
			return;
		}
		var newdataall = dataall.join(",");
		$('#data1').val(newdataall);
		$('#print_1').submit();
	}
	
	function print_byHub() {
		var dataall = [];
		var tbl_data = $('#tb_work_order').DataTable();
		tbl_data.$('input[type="checkbox"]:checked').each(function(){
			dataall.push(this.value);
		});

		if(dataall.length < 1){
			alertify.alert("alert","ท่านยังไม่เลือกงาน"); 
			return;
		}
		var newdataall = dataall.join(",");
		$('#data2').val(newdataall);
		$('#print_2').submit();
	}

	function changHub(){
		var hub_id = $('#hub_id').val();
		$.ajax({
			url: "ajax/ajax_load_branch_Byhub.php",
			type: "post",
			dataType:'html',
			data: {
				'hub_id': hub_id
			},
		beforeSend: function( xhr ) {
		}
		}).done(function( msg ) {
			if(msg.status == 500 || msg.status == 401){
				alertify.alert('ผิดพลาด',msg.message);
			}else{
				$('#mr_branch_id').html(msg.data);
				load_data();
			}
		});
	}
	function load_data(){
		var mr_branch_id = $('#mr_branch_id').val();
		var hub_id = $('#hub_id').val();
		var data_all = $("#data_all").prop("checked") ? '1' : '0';
		if(hub_id == 'all_branch'){
			$('#div_mr_user_id').hide();
			$('#div_tnt_tracking').show();
		}else{
			$('#div_mr_user_id').show();
			$('#div_tnt_tracking').hide();
		}
		$.ajax({
			url: "ajax/ajax_load_data_send_branch.php",
			type: "post",
			dataType:'json',
			data: {
				'mr_branch_id'	: mr_branch_id,
				'data_all'		: data_all,
				'hub_id'		: hub_id
			},
		   beforeSend: function( xhr ) {
				$('#bg_loader').show();		
			}
		}).done(function( msg ) {
			if(msg.status == 500 || msg.status == 401){
				alertify.alert('แจ้งเตือน',msg.message);
				$('#bg_loader').hide();
				$('#tb_work_order').DataTable().clear().draw();
			}else{
				$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
				$('#bg_loader').hide();
				$('#tb_work_order').DataTable().clear().draw();
				$('#tb_work_order').DataTable().rows.add(msg.data).draw();
			}
		});		
	}
{% endblock %}

{% block Content2 %}
<form id="print_1" action="print_sort_branch_all.php" method="post" target="_blank">
  <input type="hidden" id="data1" name="data1">
</form>
<form id="print_2" action="print_hab_all.php" method="post" target="_blank">
  <input type="hidden" id="data2" name="data2">
</form>
		<div class="row" border="1">
			<div class="col-12">
				<div class="card">
					<h4 class="card-header">ส่งออกเอกสาร</h4>
					<div class="card-body">
							
							<div class="row">
								<div class="col-md-4">
									 <div class="form-group col-md-12">
										<label for="exampleFormControlSelect1">select Hub</label>
										<select onchange="changHub();" name="hub_id" class="form-control" id="hub_id">
										  <option value="all_hub">ทุก Hub</option>
										  <option value="all_branch">สาขาต่างจังหวัดทั้งหมด</option>
										  {% for b in hub %}
											<option value="{{b.mr_hub_id}}">{{b.mr_hub_name}}</option>
										  {% endfor %}
										</select>
									  </div>									
								</div>
								<div class="col-md-6">
									 <div class="form-group col-md-12">
										<label for="exampleFormControlSelect1">select สาขา</label>
										<select onchange="load_data();" name="mr_branch_id" class="form-control" id="mr_branch_id">
										  <option value="">ทุกสาขา</option>
										  {% for b in branch %}
											<option value="{{b.mr_branch_id}}">{{b.mr_branch_code}} : {{b.mr_branch_name}}</option>
										  {% endfor %}
										</select>
									  </div>									
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									 <div class="form-group col-md-12" id="div_tnt_tracking">
										<label for="tnt_tracking">TNT Tracking</label>
										<input type="text" class="form-control" id="tnt_tracking" placeholder="Ttracking No"> 
									  </div>
									  <div class="form-group col-md-12" id="div_mr_user_id">
										<label for="mr_user_id">select Mess</label>
										<select name="mr_user_id" class="form-control" id="mr_user_id">
										  <option value="">เลือก Mess</option>
										  {% for b in emp %}
											<option value="{{b.mr_user_id}}">{{b.mr_emp_code}} : {{b.mr_emp_name}} {{b.mr_emp_lastname}}</option>
										  {% endfor %}
										</select>
									  </div>									
								</div>
								
								
							
							</div>
							<div class="row">
							<div class="col-md-4">
									 <label class="custom-control custom-checkbox">
										<input onclick="load_data();"id="data_all" name="data_all" type="checkbox" class="custom-control-input" value="1">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">แสดงงานที่สั่งวันนี้เท่านั้น</span>
									</label>								
								</div>
							</div>
							<div class="row">
							<div class="col-sm-12"><br>
								{#
								<button onclick="print_byHub();"type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบคุมใหญ่</button>
								<button onclick="print_all();" type="button" class="btn btn-outline-secondary" id="">พิมพ์ใบคุมย่อย</button>
								#}
								<button onclick="saveround();"type="button" class="btn btn-outline-primary" id="" onclick="">บันทึกข้อมูลรอบ</button> 
					
							</div>
						</div>
						<hr>
						
						<div class="table-responsive">
						  <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
							<thead>
							  <tr>
								<th>#</th>
								<th>Date/Time</th>
								<th>Bacode</th>
								<th>Sender</th>
								<th>Receiver</th>
								<th>Type Work</th>
								<th>Branch</th>
								<th>TNT</th>
								<th>SendRound</th>
								<th class="text-center">
									<label class="custom-control custom-checkbox">
										<input id="select-all" name="select_all" type="checkbox" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">เลือกทั้งหมด</span>
									</label>
								</th>
							  </tr>
							</thead>
							<tbody>
							</tbody>
						  </table>
						</div>
						
						<div class="row justify-content-center" style="margin-top:20px;">
								<div class="col-sm-12 text-center">
									
								</div>
							</div>
						<hr>
						
					</div>
				</div>
			</div>
		</div>
	<br>
		 
	
	
	
 <div id="bg_loader" class="card">
			<img id = 'loader'src="../themes/images/spinner.gif">
		</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

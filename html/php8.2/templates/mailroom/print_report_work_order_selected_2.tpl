<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<title>Pivot</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link href="../themes/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<script src="../themes/bootstrap/js/jquery-1.12.4.js"></script>

		<style type="text/css">
		html, body {
		   height: 100%; /* ให้ html และ body สูงเต็มจอภาพไว้ก่อน */
		   margin: 0;
		   padding: 0;
		}
		.wrapper {
		   display: block;
		   min-height: 100%; /* real browsers */
		   height: auto !important; /* real browsers */
		   height: 100%; /* IE6 bug */
		   margin-bottom: -130px; /* กำหนด margin-bottom ให้ติดลบเท่ากับความสูงของ footer */
       padding-left: 20px;
       padding-right: 30px;
		}
		.footer {
		   height: 130px; /* ความสูงของ footer */
		   display: block;
		   text-align: center;
       padding-left: 60px;
			 border-top: 1px solid #000;
		}
		.hed{
			border-bottom		: 2px solid #000;
		}
		.fotr{
			font-size			:10px;
			border-bottom: 1px solid #ddd;
		}


    #tbs {
    border-collapse: collapse;
    width: 100%;
    font-size: 10px;
    }
		#tbs > thead {
			border-bottom: 1px solid #000;
		}
		#tbs > tbody {
			border-bottom: 1px solid #000;
		}
		#trow {
			border-bottom: 2px dotted #ddd;
		}
		.tcols-2, .tcols-5, .tcols-6, .tcols-7 {
			height: 25px;
			font-size: 12px;
		}
		.tcols-1, .tcols-3, .tcols-4, .tcols-8, .tcols-9 {
			height: 25px;
			font-size: 10px;
		}
		#head_text {
			border-bottom: 1px solid #000;
			padding-top: 20px;
		}

		#signals {
			border: 1px solid #000;
		}

		#tb_signal {
			margin-top: 10px;
		}

		h6 {
			font-size: 13px;
		}
		</style>
	</head>
	<body>

		  {% for order in report %}

			<div class="wrapper" >
				<table class="tb" border="0" cellpadding="0" cellspacing="0" align="center" height="100%" width="100%">
					<tbody>
						<tr valign="top">
							<td height="110px">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1" >
									<tr >
										<!-- <td width="20%" valign="top">
											<img style="width:65px;	height:60px;" src="../themes/LogoPivot2.png">
										</td> -->
										<td width="70%" valign="top" id="head_text">
											<h4><b>Mail Delivery Report</b></h4>
										</td>
										<td width="20%" valign="top" id="head_text">
											<small><b>Date :</b> {{ date }}</small><br>
											<small><b>Page :</b> 1 of 1</small>
										</td>
										<!-- <td width="20%" valign="bottom">
											<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1" >
												<tr>
													<td class="text-right">  วันที่   :
													</td >
													<td class="text-left"> {{date}}
													</td>
												</tr>
												<tr>
													<td class="text-right">  เวลา   :
													</td >
													<td class="text-left"> {{time}}
													</td>
												</tr>
											</table>
										</td> -->
									</tr>
                  <tr>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableborder1">
                      <tr>
                        <td><h6><b>Company:</b>  Pivot Co., Ltd.</h6></td>
                        <td rowspan="2" ><h6><b></b></h6></td>
												<td rowspan="2" width="20%"></h5></td>
                      </tr>
                      <tr>
                        <td width='60%'><h6><b>Mess:</b> {{ order.mr_emp_name }}  {{ order.mr_emp_lastname }}</h6></td>
                      </tr>
                    </table>
                  </tr>
								</table>
							</td>
						</tr>
						<tr valign="top">
							<td style="padding:0 5px;" height="" valign="top">
							<div class="hed"></div>
								<table width="100%" border="0" cellpadding="1" cellspacing="0" id="tbs">
                  <thead>
                    <tr class="danger">
                      <th style="width:20px;">#</th>
                      <th>Barcode</th>
                      <th>Sender</th>
                      <th>Sender Department</th>
                      <th>Receiver</th>
					  <th>Receiver Department</th>
                      <th>Remark</th>
                      <th>License</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% for d in order.data %}
   									<tr id="trow">
   										<td class="tcols-1">{{ d.number}}  </td>
   										<td class="tcols-2">{{ d.mr_work_barcode }}</td>
   										<td class="tcols-3">{{ d.mr_emp_code }} - {{ d.mr_emp_name }} {{ d.mr_emp_lastname }}</td>
   										<td class="tcols-4">{{ d.mr_department_code }} - {{ d.mr_department_name }} {{ d.mr_department_floor }}</td>
   										<td class="tcols-5">{{ d.mr_emp_code_re }} - {{ d.name_re }}  {{ d.lastname_re }}</td>
											<td class="tcols-6">{{ d.depart_code_receive }} - {{ d.depart_name_receive }} {{ d.depart_floor_receive }}</td>
											
                       <td class="tcols-7">{{ d.mr_work_remark }}</td>
   										<td class="tcols-8">______________</td>
   									</tr>
   									 {% endfor %}
                  </tbody>
								</table><br>
							</td>
						</tr>

					</tbody>
				</table>
		</div>
		<div class="footer">
			<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" id="tb_signal">
        <tr>
          <td>
            <table border="0" id="signals" width="80%">
              <tr height="50px" >
                <td width="100px" style="text-align:center;"><h5><small>Messenger Name:</small></h5></td>
                <td>__________________________</td>
              </tr>
              <tr height="50px">
                <td style="text-align:center;" ><h5><small>Date: </small></h5></td>
                <td>__________________________</td>
              </tr>
            </table>
          </td>
          <td>
            <table border="0" id="signals" width="80%">
              <tr height="50px" style="padding:10px, 10px, 10px, 10px;">
                <td width="100px" style="text-align:center;">
										<h5><small>Mailroom Name:</small></h5>
								</td>
                <td> __________________________ </td>
              </tr>
              <tr height="50px">
                <td style="text-align:center;">
										<h5><small>Date:</small></h5>
								</td>
                <td style="padding-right:10px;"> __________________________ </td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- <tr align="center">
					<td style=" font-size:9px">Pivot Co., Ltd. 1000 / 67-74 อาคารลิเบอร์ตี้พลาซ่า ชั้น 3 ถ.สุขุมวิท 55 แขวงคลองตันเหนือ เขตวัฒนา กรุงเทพ ฯ 10110 Tel. 0-2391-3344 Fax. 02-381-9761</td>
				</tr> -->
			</table>
		</div>

				 {% endfor %}
				 {% block debug %}

 					{% if alert != '' %}
 						<pre>{{ alert }}</pre>
 					{% endif %}

 				{% endblock %}

	</body>
	<script type="text/javascript">
		$(document).ready(function(){
			window.print();
		});
	</script>
</html>

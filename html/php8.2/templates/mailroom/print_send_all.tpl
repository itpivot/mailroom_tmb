<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Print</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #FAFAFA;
      //font: 12pt "Times New Roman";
    font-family: "Times New Roman", Times, serif;
	//font-size: 14px

    }
body,tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 16px
 }
 tr,td,th{
	font-family: "Times New Roman", Times, serif;
	font-size: 14px
 }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }

    .page {
      width: 210mm;
      min-height: 297mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }

    .subpage {
      padding: 0.5cm;
      position: relative;
    }

    .barcode {
      position: absolute;
      right: 20px;
    }
td{
padding:5px;
}
    .header {
      text-align: center;
      margin-top: 30px;
    }

    .logo_position {
      position: absolute;
      left: 20px;
    }

    .logo {
      width: auto;
      height: 80px;
    }

    @page {
      size: A4;
      margin: 0;
    }

    @media print {
	#print_p{
		  display:none;
		}
      html,
      body {
        width: 210mm;
        height: 297mm;
		 font-family: "Times New Roman", Times, serif;
      }
      .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
      }
    }
  </style>
</head>

<body>
  <div class="book">
   {% if data|length > 0 %} {% for dt in data %}

    <div class="page">
      <div class="subpage ">

        <div class="logo_position">
          
        </div>
        <div>
          <svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="{{ stockout.order_no }}" jsbarcode-textmargin="0" jsbarcode-height="40"
            jsbarcode-fontsize="18">
          </svg>
        </div>
        <div class="header mb-3">
          <h5 class="">ใบส่งเอกสารประจำวัน</h5>
          <h6 class="">(สำหรับขาส่งเข้าสำนักงานใหญ่)</h6>
		  <img src="barcode.php?barcode={{dt.h.d.barcode}}&amp;width=350&amp;height=60" align="absmiddle">
        </div>
		 
        <div class="form-group">
            <label class="">เรียน : สารบรรณกลางและโลจิสติกส์ / บริหารทรัพยากรอาคาร</label>
        </div>
		<table width="100%">
			<tr>
				<td colspan="2" align="right">หน้า  :&nbsp;&nbsp;&nbsp;<b><u>{{dt.h.p}}/{{dt.h.all}}&nbsp;&nbsp;&nbsp;</u></b></td>
              </tr>
              <tr>
				<td colspan="2" align="right"><p class="card-text">วันที่  <b><u>  {{dt.h.d.sys_timestamp}} </u></b></p></td>
              </tr>
			  
              <tr>
			  <td><p class="card-text">สาขา / เขต /NOC / SME  <b><u>&nbsp;&nbsp;&nbsp;{{dt.h.d.mr_branch_name}}&nbsp;&nbsp;&nbsp;</u></b></p> </td>
			  <td align="right"><p class="card-text">รหัสสาขา/หน่วยงาน  <b><u>&nbsp;&nbsp;&nbsp;{{dt.h.d.mr_branch_code}}&nbsp;&nbsp;&nbsp;</u></b> โทรศัพท์ <b><u>&nbsp;&nbsp;&nbsp;{{dt.h.d.sender_tel}}&nbsp;&nbsp;&nbsp;</u></b></p></td>
			  </tr>
			  <tr>
			  <td align=""><p class="card-text">ขอนำส่งเอกสารให้หน่วยงาน / สาขา ดังนี้  </p></td>
			  <td align="right"></td>
			  </tr>
		 </table>
        <div class=" mb-2 p-2">
           
        </div>

        <div class="table-responsive">
          <table   border="1" class="table table-bordered">
            <thead class="thead-light">
              <tr>
				<th>ลำดับ</th>
                <th>ผู้รับเอกสาร</th>
                <th>ชื่อเอกสาร</th>
                <th>จำนวนซอง</th>
                <th>Barcode</th>
              </tr>
            </thead>
            {% for subd in dt.d %}
            <tbody>
			{% if subd.no == "all" %}
			<tr>
				<th class="text-right"colspan="3"><u>รวมจำนวนซองทั้งสิ้น</u></th>
				<th class="text-center" ><u>{{subd.qty}}</u></th>
				<th></th>
			</tr>
			{% else %}
              <tr>
                
                <td>{{subd.no}}</td>
				<td>{{subd.re_name}}  {{subd.re_lname}}  -  
				{% if subd.re_mr_branch_code == ""%}
					{{subd.re_mr_department_name}}
				{% else %}
					{{subd.re_mr_branch_name}}
						{% if subd.mr_branch_floor != '' %}
							/ ชั้น  {{subd.mr_branch_floor }}
						{% endif %}
					
				{% endif %}
					
				</td>
                <td>{{ subd.mr_topic }}</td>
                <td>{{ subd.qty }}</td>
                <td><img src="barcode.php?barcode={{subd.mr_work_barcode}}&amp;width=350&amp;height=40" align="absmiddle"></td>
              </tr>
			 {% endif %}
            </tbody>
            {% endfor %}
          </table>
        </div>
		<br>
				<br>
				<br>
<p>โปรดยืนยันการนำส่งเอกสารข้างต้นนี้ หากมีรายการผิดพลาดกรุณาแจ้งทันที</p><br>
		<center>
			<p>ลงชื่อ...........................................ผู้ส่ง  เลขที่พนักงาน.......................</p>
			<p>ลงชื่อ...........................................ผู้รับรอง  เลขที่พนักงาน........................</p>
		</center>
<p>หมายเหตุซรับเอกสารหลังเวลาที่กำหนด  สารบรรณกลางและโลจีสติกส์  จะส่งให้หน่วยงานในวันถัดไป</p>
			<div id="print_p">
				<br>
				<br>
				<br>
				<br>
					<center>
						<button onclick="window.print();">print</button>
					<center>
				</div>	
      </div>
    </div>

    
{% endfor %}  {% endif %}
  </div>
  {% if debug != "" %}
  <pre>
  {{debug}}
  </pre>
  {% endif %}
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

  <script>
  $(document).ready(function(){
	{{alertt}}
	{% if alertt == ''%}
	 //window.print()
	{% endif %} 
  });
  </script>
</body>

</html>
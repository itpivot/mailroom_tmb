{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
	  }


{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">

<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>
{% endblock %}

{% block domReady %}
		$("#barcode").focus();
		$("#btn_save").click(function(){
			load_data();
		});	
	var table = $('#tb_work_order').DataTable({
			'responsive': true,
			'pageLength': '100',
			'columns': [
				{ 'data':'no' },
				{ 'data':'sys_timestamp'},
				{ 'data':'supper_barcode' },
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_type_work_name' },
				{ 'data':'mr_status_name' },
				{ 'data':'acctiom' }
			],
			
			'scrollCollapse': true 
		});

			
			
		
		load_all_data();
{% endblock %}
{% block javaScript %}
		function cancle_work(main_id,remark){						
			var r = confirm("ยืนยันการยกเลิกการส่งเอกสาร!");
			if (r == true) {
				$.post(
					'./ajax/ajax_cancle_by_mailroom.php',
					{
						mr_work_main_id 		 : main_id,
						remark_can 				 : 'ยกเลิกการจัดส่ง(ไม่พบเอกสารมาจากสาขา)',
						remark 					 : remark,
					},
					function(res) {
						alert(res.message);
						var barcode 			= $("#barcode").val();
						var status 				= true;
						
						if(barcode == "" || barcode == null){
							//load_all_data();
						}else{
							load_data();
						}
					},'json'
				);
			}				
		}

		function update_send_work(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			
				if(barcode == "" || barcode == null){
					status = false;
					$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});

				}
			if( status === true ){
					$.ajax({
						url: "ajax/ajax_update_send_work.php",
						type: "post",
						dataType:'json',
						data: {
							'barcode': barcode
						},
					beforeSend: function( xhr ) {
						$('#bg_loader').show();
					}
					}).done(function( msg ) {

						if(msg.status == 200){
							$("#barcode").val('');
							load_data();
						}else{
							alertify.alert(msg.message);
						}
					});
					
				}
		}
		
	function load_data(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			
			if(barcode == "" || barcode == null){
				status = false;
				$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
			}
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_load_data_reciever_branch.php",
					type: "post",
					dataType:'json',
					data: {
						'barcode': barcode
					},
				   beforeSend: function( xhr ) {
						$('#bg_loader').show();		
					}
				}).done(function( msg ) {
					
					if(msg.status == 200){
						$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
						$('#tb_work_order').DataTable().clear().draw();
						$('#tb_work_order').DataTable().rows.add(msg.data).draw();
						//alert('22222');
					}else{
						$('#barcode').val('');
						$('#barcode').focus();
						$('#tb_work_order').DataTable().clear().draw();
						//alertify.alert(msg.message);
					}
				});
			}
		}

		function load_all_data(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_load_data_reciever_branch_all.php",
					type: "post",
					dataType:'json',
					data: {
						'barcode': barcode
					},
				   beforeSend: function( xhr ) {
						$('#bg_loader').show();
					}
				}).done(function( msg ) {
					if(msg.status == 200){
						$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
						//$('#bg_loader').hide();
						$('#tb_work_order').DataTable().clear().draw();
						$('#tb_work_order').DataTable().rows.add(msg.data).draw();
					}else{
						alertify.alert(msg.message);
					}
				});
			}
		}
		
		function update_barcode() {
			var barcode 			= $("#barcode").val();
			var sub_barcode 		= $("#sub_barcode").val();
			var mr_round_id 		= $("#mr_round_id").val();
			var status 				= true;

			
			if(sub_barcode == "" || sub_barcode == null){
				$('#sub_barcode').css({'color':'red','border-style':'solid','border-color':'red'});
				status = false;
			}else{
				status = true;
			}
			$("#sub_barcode").val('');
		
			//console.log(status);
			if( status === true ){
				$.ajax({
					url: "ajax/ajax_update_work_branch_by_barcode.php",
					type: "post",
					dataType: "json",
					data: {
						'mr_round_id': mr_round_id,
						'barcode': barcode,
						'sub_barcode': sub_barcode
					},
					success: function(res){
						var str = '';
						var str0 = '';
						var str1 = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
						var str2 = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
							str0+='<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
							str0+='<span aria-hidden="true">&times;</span>';
							str0+='</button>'
							str0+='<strong>  '+res.title+'  </strong>';
							str0+= res.message;
							str0+='</div>';
							if(res.status == 200){	
								$("#sub_barcode").val('');
								str = str1+str0;
								load_data();
							}else{
								str = str2+str0;
								load_data();
							}
							$('#alert_').html(str);	
						// if(!!res){
						// 	var str = '';
						// 	var str0 = '';
						// 	var str1 = '<div class="alert alert-success alert-dismissible fade show" role="alert">';
						// 	var str2 = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
						// 		str0+='<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
						// 		str0+='<span aria-hidden="true">&times;</span>';
						// 		str0+='</button>'
						// 		str0+='<strong>  '+res['st']+'  </strong>';
						// 		str0+= res['msg'];
						// 		str0+='</div>';
						// 		if(res['st']=='success'){	
						// 			$("#sub_barcode").val('');
						// 			str = str1+str0;
						// 			load_data();
						// 		}else{
						// 			str = str2+str0;
						// 		}
						// 		$('#alert_').html(str);	
						// }
						$('#sub_barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
					}								
				});	
			}
		}

		function keyEvent( evt, barcode )
		{
			if(window.event)
				var key = evt.keyCode;
			else if(evt.which)
				var key = evt.which;
				
			if( key == 13 ){
				load_data();
				$("#sub_barcode").focus();
			}
		}
		function keyEvent_subbarcode( evt )
		{
			if(window.event)
				var key = evt.keyCode;
			else if(evt.which)
				var key = evt.which;
				
			if( key == 13 ){
				update_barcode();
			}
		}


{% endblock %}

{% block Content2 %}
		<div class="row" border="1">
			<div class="col">
			</div>
			<div class="col-12">
				<div class="card">
					<h4 class="card-header">รับเอกสารจากสาขา</h4>
					<div class="card-body">
							<div class="row">
								<div class="col-sm-3">
								<label>วันที่</label>
								<input type="text" value="{{today}}" class="form-control mb-sm-0" id="today" placeholder="To day" readonly>	
								</div>
								<div class="col-sm-3">
									<label>รอบเอกสาร</label>
									 <select class="form-control" id="mr_round_id">
										 {% for r in round %}
										 <option value="{{r.mr_round_id}}">รับ{{r.mr_type_work_name}} {{r.mr_round_name}}</option>
										 {% endfor %}
										</select>
								</div>
							</div>
						<hr>
							<div class="row">
								<div class="col-sm-4">
									<label>รหัสบาร์โค้ดใบปะหน้าซองใหญ่</label>
									<div class="input-group">
										<input type="text" class="form-control mb-sm-0" id="barcode" placeholder="Barcode" onkeyup="keyEvent(event,this.value);">									  
										<div class="input-group-append">
										<button type="button" class="btn btn-outline-primary" id="btn_save">ค้นหา</button>
									  </div>
									</div>
								</div>
							</div>
						<hr>
						
						<div class="row justify-content-center">
							<div class="col-sm-4">
								<label>เลขที่เอกสาร</label>
								<div class="input-group">
									<input type="text" class="form-control mb-5 mb-sm-0" id="sub_barcode" placeholder="Barcode" onkeyup="keyEvent_subbarcode(event);"><br>
									<div class="input-group-append">
										<button type="button" class="btn btn-outline-primary" onclick="update_barcode();">บันทึก</button>
									</div>
								</div>
								
							</div>
						</div>
						<hr>
						<div class="col" id="alert_">
						</div>
						<div class="table-responsive">
						  <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
							<thead>
							  <tr>
								<th>#</th>
								<th>Date/Time</th>
								<th>ใบคุม</th>
								<th>Bacode</th>
								<th>Sender</th>
								<th>Receiver</th>
								<th>Type Work</th>
								<th>Status</th>
								<th>Actiom</th>
							  </tr>
							</thead>
							<tbody>
							</tbody>
						  </table>
						</div>
						<hr>
						<div class="row justify-content-center">
						{#<div class="col-sm-12 text-center">
								<button type="button" class="btn btn-outline-primary" id="" onclick="update_send_work();">บันทึก</button>
								<button type="button" class="btn btn-outline-secondary" id="">ยกเลิก</button>
							</div>#}
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	<br>
		 
	
	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

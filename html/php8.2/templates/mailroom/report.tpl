{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}

.loading {
	position: fixed;
	top: 40%;
	left: 50%;
	-webkit-transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%);
	transform: translate(-50%, -50%);
	z-index: 1000;
}

.img_loading {
	  width: 350px;
	  height: auto;
	  
}

{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}
		$('.input-daterange').datepicker({
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		});	
		var table = $('#tb_level3').DataTable({ 
			"scrollY": "800px",
			"scrollCollapse": true,
			"responsive": true,
			//"searching": false,
			"language": {
				"emptyTable": "ไม่มีข้อมูล!"
			},
			"pageLength": 10, 
			'columns': [
				{ 'data':'no' },
				{ 'data':'link_click' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_topic'},
				{ 'data':'mr_work_remark'},
				{ 'data':'main_sys_time'},
				{ 'data':'time_mail_re' },
				{ 'data':'sla_date_end' },
				{ 'data':'time_log' },
				{ 'data':'name_get' },
				{ 'data':'mr_status_name' },
				{ 'data':'mr_type_work_name' }
			]
		});
{% endblock %}


{% block javaScript %}
	 function getDetail_level1(){
			var date_1 = $('#date_1').val();
			var date_2 = $('#date_2').val();
			var type = $("input[name=type1]:checked").val();
			var branch_type = $("input[name=branch_type]:checked").val();
			if(date_1 == '' || date_2 == ''){
				alertify.alert("กิดข้อผิดผลาด !","กรุณาเลือกช่วงวันที่");
				return;	
			}
			if(type == 'ho'){
				$('#box_branch_type').hide();
				//console.log('hode');
			}else{
				$('#box_branch_type').show();
			}
			//console.log(type);
			$.ajax({
				method: "POST",
				dataType: "json",
				url: "ajax/ajax_getReport_level1.php",
				data: { 
					date_1		: date_1, 
					date_2		: date_2,
					type		: type,
					branch_type	: branch_type
				},
				beforeSend: function( xhr ) {
					$('.loading').show();
					$('#Detail_level1').hide();
					$('#Detail_level2').hide();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert('เกิดข้อผิดผลาด !');
					//location.reload();

				  }
			})
			.done(function( msg ) {
					$('.loading').hide();
					$('#Detail_level2').fadeIn('slow');
					$('#content_level2').html(msg['html']);
					$('#date_1_2').val(msg['date_1'])
					$('#date_2_2').val(msg['date_2'])
				  //alert( "Data Saved: " + msg );
			});
	}


	function getDetail_level3(status,branch_type_re){
		var date_1 = $('#date_1').val();
		var date_2 = $('#date_2').val();
		var type = $("input[name=type1]:checked").val();
		var branch_type = $("input[name=branch_type]:checked").val();

		if(type == 'ho'){
			$('#box_branch_type').hide();
			//console.log('hode');
		}else{
			$('#box_branch_type').show();
		}
		//console.log(type);
		$.ajax({
			method: "POST",
			dataType: "json",
			url: "ajax/ajax_getReport_level3.php",
			data: { 
				date_1				: date_1, 
				date_2				: date_2,
				status				: status,
				type				: type,
				branch_type			: branch_type,
				branch_type_re		: branch_type_re,
			},
			beforeSend: function( xhr ) {
				$('.loading').show();
				$('#Detail_level1').hide();
				$('#Detail_level2').hide();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert('เกิดข้อผิดผลาด !');
				//alertify.alert("กิดข้อผิดผลาด !");
				//location.reload();

			  }
		})
		.done(function( msg ) {
				$('.loading').hide();
				$('#Detail_level3').fadeIn('slow');
				$('#head_level3').html(msg['html_head']);
				$('#tb_level3').DataTable().clear().draw();
				$('#tb_level3').DataTable().rows.add(msg['data']).draw();

				var input1 = $("<input>", { type: "hidden", name: "status", 		value: status }); 
				var input2 = $("<input>", { type: "hidden", name: "branch_type_re",value: branch_type_re }); 
				$('#form_level_3').append(input1); 
				$('#form_level_3').append(input2); 


			//alert( "Data Saved: " + msg );
		});
}

	function click_back(){
		$('#Detail_level1').show();
		$('#Detail_level2').hide();
	}
	function click_backlevel3(){
		$('#Detail_level2').show();
		$('#Detail_level3').hide();
	}
	function click_branch_type_chang(){
		var branch_type = $("input[name=branch_type]:checked").val();
		//console.log(branch_type);
		if(branch_type == 3){
			$('#tb_branch_1').fadeIn(1000);
			$('#tb_branch_2').hide();
		}else{
			$('#tb_branch_2').fadeIn(1000);
			$('#tb_branch_1').hide();
		}

	}

	function click_export_level_1(){
			var date_1 = $('#date_1').val();
			var date_2 = $('#date_2').val();
			var type = $("input[name=type1]:checked").val();
			var branch_type = $("input[name=branch_type]:checked").val();


		var input1 = $("<input>", { type: "hidden", name: "date_1", 	value: date_1 }); 
		var input2 = $("<input>", { type: "hidden", name: "date_2", 	value: date_2 }); 
		var input3 = $("<input>", { type: "hidden", name: "type", 		value: type }); 
		var input4 = $("<input>", { type: "hidden", name: "branch_type",value: branch_type }); 

		$('#form_level_1').append(input1); 
		$('#form_level_1').append(input2); 
		$('#form_level_1').append(input3); 
		$('#form_level_1').append(input4); 
		$('#form_level_1').submit();
	}

function click_export_level_3(){
	
			var date_1 = $('#date_1').val();
			var date_2 = $('#date_2').val();
			var type = $("input[name=type1]:checked").val();
			var branch_type = $("input[name=branch_type]:checked").val();


		var input1 = $("<input>", { type: "hidden", name: "date_1", 	value: date_1 }); 
		var input2 = $("<input>", { type: "hidden", name: "date_2", 	value: date_2 }); 
		var input3 = $("<input>", { type: "hidden", name: "type", 		value: type }); 
		var input4 = $("<input>", { type: "hidden", name: "branch_type",value: branch_type }); 

		$('#form_level_3').append(input1); 
		$('#form_level_3').append(input2); 
		$('#form_level_3').append(input3); 
		$('#form_level_3').append(input4); 
		$('#form_level_3').submit();
	}
{% endblock %}





{% block Content2 %}
<br>
<div id ="Detail_level1">
<div class="row justify-content-center">
	<div class="col-lg-4 col-12">
		<h2 class="text-center">รายงานการรับส่งเอกสาร</h2>
	</div>
</div>
<br>
<div class="row justify-content-center">
	<div class="col-lg-4 col-12">
		<div class="input-daterange" id="datepicker" data-date-format="yyyy-mm-dd">
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่เริ่มต้น</label>
				<input type="text" class="input-sm form-control" id="date_1" name="date_1" onchange="$('#date_1_2').val(this.value)" placeholder="From date" autocomplete="off"/>
			</div>
			<br>
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่สิ้นสุด</label>
				<input type="text" class="input-sm form-control" id="date_2" name="date_2" onchange="$('#date_2_2').val(this.value)" placeholder="To date" autocomplete="off"/>
			</div>
		</div>
	</div>
</div>
<br>
<div class="row justify-content-center">
	<div class="btn-group btn-group-toggle" data-toggle="buttons">
		<label class="btn btn-outline-primary active">
		  <input type="radio" name="type1" id="option1" autocomplete="off" checked value="branch"> สาขาสั่งงาน
		</label>
		<label class="btn btn-outline-primary">
		  <input type="radio" name="type1" id="option2" autocomplete="off" value="ho"> สำนักงานใหญ่สั่งงาน
		</label>
	</div>
</div>
<br>
<hr>
<div class="row justify-content-center">
		<button type="button" class="btn btn-primary btn-lg" onclick="getDetail_level1();">ดูรายละเอียด</button>
</div>
</div>

<!-- Detail_level2 -->
<!-- Detail_level2 -->
<!-- Detail_level2 -->
<!-- Detail_level2 -->
<!-- Detail_level2 -->
<!-- Detail_level2 -->


<div id ="Detail_level2"  style="display: none;">
	<div class="row justify-content-center">
		<div class="col-lg-4 col-12">
			<h2 class="text-center">รายงานการรับส่งเอกสาร</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-12">
			<div class=" input-group">
				<label class="input-group-addon" style="border:0px;">วันที่ออกรายงาน :</label>
				<input type="text" class="input-sm form-control" placeholder="To date" autocomplete="off" readonly value="{{ today }}"/>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
			<div class="col-lg-4 col-12">
				<div class=" input-group">
					<label class="input-group-addon" style="border:0px;">วันที่ของข้อมูล :</label>
					<input type="text" class="input-sm form-control" id="date_1_2" name="date_1_2" placeholder="To date" autocomplete="off" readonly/>
					<label class="input-group-addon" style="border:0px;">  ถึง </label>
					<input type="text" class="input-sm form-control" id="date_2_2" name="date_2_2" placeholder="To date" autocomplete="off" readonly/>
				</div>
			</div>
	</div>
	<br>
	<div class="row" id="box_branch_type">
		<div class="col-lg-4 col-12">
			<div class="btn-group btn-group-toggle" data-toggle="buttons">
				<label class="btn btn-outline-primary active">
				<input type="radio" name="branch_type" id="option3" value="3" autocomplete="off" onchange="click_branch_type_chang();" checked> สั่งงานจากสาขาต่างจังหวัด
				</label>
				<label class="btn btn-outline-primary">
				<input type="radio" name="branch_type" id="option4" value="2" autocomplete="off" onchange="click_branch_type_chang();" > สั่งงานจากสาขากรุงเทพมและปริมณฑล
				</label>
			</div>
		</div>
		
	</div>
	<hr>
	<div id="content_level2">
	
		</div>

<br>
<br>
<br>
<div class="row justify-content-center">
		<div class="col-5 text-right">
				<button type="button" class="btn btn-primary btn-lg" onclick="click_back();">
						<i class="material-icons">reply_all</i>
						 ย้อนกลับ
					</button>  
		</div>
		<div class="col-5 text-left">
			<form id="form_level_1" action="export_Report_level1.php" target="_blank" type="post">
				<button type="button" onclick="click_export_level_1();" class="btn btn-primary btn-lg">
						<i class="material-icons">cloud_download</i>
					Export excel
				</button>
			</form>
		</div>
	  </div>
</div>

<!-- Detail_level3 -->
<!-- Detail_level3 -->
<!-- Detail_level3 -->
<!-- Detail_level3 -->
<!-- Detail_level3 -->
<!-- Detail_level3 -->


<div id ="Detail_level3" style="display: none;">
<div id ="head_level3">

</div>
<form id="form_level_3" action="export_Report_level3.php" target="_blank" type="post">
<button type="button" class="btn btn-primary btn-lg" onclick="click_backlevel3();">
		<i class="material-icons">reply_all</i>
		 ย้อนกลับ
	</button> 
	
		<button type="button" class="btn btn-primary btn-lg" onclick="click_export_level_3();">
				<i class="material-icons">cloud_download</i>
			Export excel
		</button>
	</form>
	<br> 
	<br> 
	<div class="table-responsive"></div>
		<table id="tb_level3" class="table table-bordered">
				<thead>
				<tr>
					<th scope="col">No.	</th>
					<th scope="col">Barcode	</th>
					<th scope="col">ชื่อผู้ส่ง</th>
					<th scope="col">ชื่อผู้รับ</th>
					<th scope="col">ชื่อเอกสาร	</th>
					<th scope="col">หมายเหตุ	</th>
					<th scope="col">วันที่สร้างรายการ	</th>
					<th scope="col">วันที่ห้องสารบัณกลางรับ	</th>
					<th scope="col">วันที่ครบ SLA</th>
					<th scope="col">วันที่สำเร็จ	</th>
					<th scope="col">ผู้ที่ลงชื่อรับงาน	</th>
					<th scope="col">สถานะงาน	</th>
					<th scope="col">ประเภทการส่ง	</th>

				</tr>
				</thead>
				<tbody>
					
					</tbody>
				</table>
	</div>
</div>
<div class='loading' style="display: none;">
                <img src="../themes/images/loading.gif" class='img_loading'>
            </div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

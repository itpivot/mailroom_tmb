{% extends "base_emp2.tpl" %}


{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">

<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
{% endblock %}

{% block styleReady %}
	.widthText {
		width: 300px;
	}

	input[type='text'], input[type='number'] {
		border-radius: 0;
		text-align: center;
		font-weight: bold;
	}


	#btn_upload, #btn_export, #btn_clear {
		border-radius: 0;
		text-align: center;
		font-weight: bold;
	}

	#lst_warning {
		color: red;
	}
{% endblock %}

{% block domReady %}
		var table = $("#tb_work_order").DataTable();
		
			
		table.destroy();
		table = $("#tb_work_order").DataTable({
			"ajax": {
				"url": "ajax/ajax_get_emp_resign.php",
				"type": "POST",
			},
		
			'columns': [
				{ 'data':'no' },
				{ 'data':'mr_emp_name'},
				{ 'data':'mr_emp_lastname' },
				{ 'data':'mr_emp_code' },
				{ 'data':'mr_emp_email' },
				{ 'data':'mr_date_import' }
			],
			'scrollCollapse': true
		});
	
	
	
	
	
	$('#btn_upload').on('click',function() {
		var data = $('#txt_upload').prop("files");
		var form_val = $('#txt_upload').prop("files")[0];
	
		// console.log(data);
		
			$('#btn_upload').text('Uploading...').attr('disabled', 'disabled');
			var form_data = new FormData();
			form_data.append('file', form_val);
			if(data[0]['type'] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || data[0]['type'] == "application/vnd.ms-excel"){
					$.ajax({
					url: 'ajax_readFile_Emp.php',
               		cache: false,
                	contentType: false,
                	processData: false,
                	data: form_data,                         
					type: 'post',
					dataType: 'html',
					success: function(res){
					
						if( res <= 0 ) {
							alert('ไม่มีข้อมูลนำเข้า');
							clearForm();
							$('#btn_upload').text('Upload').removeAttr('disabled');
							table.ajax.reload();
						} else {
							$('#btn_upload').text('Upload').removeAttr('disabled');
							clearForm();
							alert('การอัพโหลดรายชื่อพนักงานจำนวน '+ res +' รายชื่อ');
							table.ajax.reload();
						}
					}
			});
			
		}
	});

	$('#btn_clear').click(function() {
        window.history.back();
    }); 
{% endblock %}

{% block javaScript %}
	var clearForm = function() {
		$('#txt_upload').val("");
	}
{% endblock %}
	
{% block content %}
	
<div class="page-header">
	<h1><strong>พนักงานลาออก</strong></h1>
</div>	
<div class="card" >
  <div class="card-body">
	
		<div class="row">
			
			<br>
			<div class="col-md-5" style="background-color:;text-align:center;">	
					<form action="" method="POST" id="formUpload" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal">
						<div class="row">
							<div class="col-md-12">	 
								<div class="form-group">
									<label for="txt_upload" class="label-control col-sm-6"  style="right:100px;">File Upload</label>
									<div class="col-sm-6" style="left:50px;">
										<input type="file" name="txt_upload" id="txt_upload">
									</div>
								</div>
								<div class="form-group">
									<label for="btn_upload" class="label-control col-sm-2"></label>
									<div class="col-sm-10">
										<button type="button" class="btn btn-default" id="btn_clear">Back</button>
										<button type="button" class="btn btn-primary" name="btn_upload" id="btn_upload">Upload</button>
									</div>
								</div>
							</div>
						</div>	
					</form>
			</div>
			<div class="col-md-7">
				<div id="lst_warning">
					<label for="warning_text"><strong>ข้อแนะนำ !</strong></label>
					<ul>
						<li>สามารถ upload ได้เฉพาะไฟล์นามสกุล .xls หรือ .xlsx เท่านั้น</li>
						<li>โปรดตรวจสอบและจัดการไฟล์เอกสารก่อนการ upload ไฟล์ทุกครั้ง มิฉะนั้นอาจทำให้เกิดข้อผิดพลาดขึ้นได้</li>
						<li>หากข้อมูลไม่ครบ ระบบจะไม่ทำการเพิ่มรายชื่อพนักงานผู้นั้นเข้าสู่ระบบ </li>
					</ul>
				</div>
				<a href='./template_emp_data_resign.xlsx' target="_blank" ><span class='glyphicon glyphicon-save-file'></span> &nbsp;ดาวน์โหลด template emp</a><br><br>
			</div>
			
			<br>
		</div>
		
	</div>
</div>		

<div class="card" style="margin-top:10px;" >
  <div class="card-body">		
		
		<h3><strong>รายชื่อพนักงานที่ลาออก</strong></h3>
        <div class="table-responsive">
          <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
            <thead>
              <tr>
                <th>#</th>
                <th>ชื่อ</th>
                <th>นามสกุล</th>
                <th>รหัสพนักงาน</th>
                <th>Email</th>
                <th>วันที่ส่งข้อมูล</th>
                
              </tr>
            </thead>
            <tbody>
				
            </tbody>
          </table>
        </div>
         
		
		
	</div>
</div>
{% endblock %}


{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
	  }


{% endblock %}
{% block scriptImport %}
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
<link rel="stylesheet" href="css/chosen.css">

<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
<script src="js/chosen.jquery.js" charset="utf-8"></script>
<script src="js/daterange.js" charset="utf-8"></script>
{% endblock %}

{% block domReady %}
		
		$("#barcode").focus();
		$("#btn_save").click(function(){
			var barcode 			= $("#barcode").val();
			var status 				= true;
			
				if(barcode == "" || barcode == null){
					$('#barcode').css({'color':'red','border-style':'solid','border-color':'red'});
					status = false;
				}else{
					status = true;
				}

				if( status === true ){
					$.ajax({
						url: "ajax/ajax_update_work_by_barcode_sender.php",
						type: "post",
						dataType: "json",
						data: {
							'barcode': barcode,
						},
						success: function(res){
							if(res.status != 200){
								alertify.alert(res.message);
							}
							$("#barcode").val('');
						}							
					});	
					
					$('#barcode ').css({'border':' 1px solid rgba(0,0,0,.15)'});
					$('#tb_work_order').DataTable().ajax.reload();
				}
		});
		
		
		$('#btn_print').click(function() {
			var mess_id 			= $("#mess_id").val();
			$('#fmess_id').val(mess_id);
			$('#frome_print').submit();
			//console.log(mess_id);
			//window.open('print_report_work_order_by_select_2.php?mess_id='+mess_id);
			
		});
		
		
		
{% endblock %}


{% block javaScript %}
    function select_mess(){
		var mess_id = $('#mess_id').val();
		var table = $('#tb_work_order').DataTable({
			'responsive': true,
			'destroy': true,
			 "ajax": {
				"url": "./ajax/ajax_load_send_work_mailroom.php",
				"type": "POST",
				"dataType": 'json',
				"data": {
						'mess_id': mess_id,
					},
			},
			'columns': [
				{ 'data':'no' },
				{ 'data':'date_send'},
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_send' },
				{ 'data':'name_receive' },
				{ 'data':'mr_type_work_name' },
				{ 'data':'mr_status_name' }
			],
			
			'scrollCollapse': true 
		});

		
		
	}
	
{% endblock %}

{% block Content %}

<form id="frome_print" method="POST" action="print_report_work_order_by_select_2.php">
	<input type="hidden" value="0" id='fmess_id' name="mess_id">
</form>

		<div class="row" border="1">
			<div class="col">
				
			</div>
			<div class="col-9">
				<div class="card">
					<h4 class="card-header">ส่งออกเอกสาร</h4>
					<div class="card-body">
						<form>
							<!-- <div class="form-row justify-content-center">
								<div class="col-sm-6">
									<input type="text" class="form-control mb-5 mb-sm-0" id="barcode" placeholder="Barcode">
									
								</div>
								<div class="col-auto">
									<button type="button" class="btn btn-outline-primary btn-block" id="btn_save">บันทึก</button>
								</div>
							</div> -->
							
							<div class="form-row justify-content-center" style="margin-top:20px;">
								<div class="col-sm-6" style="padding-left:20px;">
									<select class="form-control-lg" id="mess_id" style="width:100%;" >
											<option value="0">เลือกทั้งหมด</option>
											{% for s in mess_data %}
												<option value="{{ s.mr_user_id }}">{{ s.mr_emp_name }} {{ s.mr_emp_lastname }}</option>
											{% endfor %}
											
									</select>
									
								</div>
							</div>
							<div class="form-row justify-content-center" style="margin-top:20px;">
								<div class="col-sm-3" style="padding-left:20px;">
										<button type="button" class="btn btn-outline-primary btn-block" id="btn_search" onclick="select_mess();">ค้นหา</button>
									
								</div>
								<div class="col-sm-3" style="padding-left:20px;">
										<button type="button" class="btn btn-outline-success btn-block" id="btn_print">พิมพ์ใบคุม</button>
								</div>
								
							</div>
							
							
							
						</form>
					</div>
				</div>
			</div>
			<div class="col">
				
			</div>
		</div>
	<br>
		 <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Sent : Date/Time</th>
                            <th>Bacode</th>
                            <th>Sender</th>
                            <th>Receiver</th>
                            <th>Type Work</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
	
	
	

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

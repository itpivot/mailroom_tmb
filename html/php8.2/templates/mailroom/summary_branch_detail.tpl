{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
      body {
       width: 100%;
        height:100%;
      }
      .table-hover tbody tr.hilight:hover {
          background-color: #555;
          color: #fff;
      }

      table.dataTable tbody tr.hilight {
        background-color: #7fff7f;
      }

      table.dataTable tbody tr.selected {
				background-color: #555;
				color: #fff;
				cursor:pointer;
			}

      #tb_work_order {
        font-size: 13px;
      }

	  .panel {
		margin-bottom : 5px;
      }
    
      .input-daterange {
          width: 450px;
      }

      #tb_summary,
      #tb_summary th{
          text-align:center;
          font-weight: bold;
      }

    .loading {
          position: fixed;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
          z-index: 1000;
          display: none;
      }


      .img_loading {
            width: 350px;
            height: auto;
            
      }
      
{% endblock %}
{% block scriptImport %}

<link rel="stylesheet" href="../themes/datepicker/bootstrap-datepicker3.min.css">
<script type='text/javascript' src="../themes/datepicker/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
	<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
{% endblock %}

{% block domReady %}
	$('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
	});

    var start = '';
    var end = '';
    //getSummary(start, end);

    $('#btn_search').on('click', function() {
        var start = $('#date_start').val();
        var end = $('#date_end').val();
        getSummary(start, end);
    }); 

    $('#btn_clear').on('click', function() {
        $('#date_start').val("");
        $('#date_end').val("");
        var start = '';
        var end = '';
         getSummary(start, end);
    });
	
var tbl_data = $('#tb_summary').DataTable({ 
	"searching": false,
    "language": {
        "emptyTable": "ไม่มีข้อมูล!"
    }
});
{% endblock %}


{% block javaScript %}
function detail_click(user,status){
    //console.log(status+'<<<>>>>'+user);  
    $('#user_id').val(user);
    $('#status_id').val(status);
    $('#form_detail').submit();
}
  
{% endblock %}

{% block Content2 %}    

      <div class="card">
            <div class='card-header'>
               <b>ติดตามงานจาก Mess</b>
            </div>
               
        <div class="card-body">
              
            </div>
      </div>

      <div class='card' style='margin-top: 20px;'>
        <div class='card-header'>
            <b>สถานะงาน</b>
        </div>        
        <div class='card-body'>
            <table id='tb_summary' class="table table-responsive table-bordered table-striped table-sm" cellspacing="0">
                <thead class="thead-inverse">
                    <tr>
                        <th>ลำดับ</th>
                        <th width="200">Barcode</th>
                        <th width="200">หัวเรื่อง</th>
                        <th>หมายเหตุ</th>
                        <th>สถานะ</th>
                        <th>ส่งที่</th>
                        <th>สาขาต้นทาง</th>
                        <th>สาขาปลายทาง</th>
                    </tr>
                </thead>
                <tbody id='show_data'>
                {% for tr in order %}
                <tr>
                    <td> {{ loop.index }} </td>
                    <td> <a href="../employee/work_info.php?id={{tr.mr_work_main_id }}" target="_blank">{{tr.mr_work_barcode }}</a>  </td>
                    <td> {{tr.mr_topic}} </td>
                    <td> {{tr.mr_work_remark}} </td>
                    <td> {{tr.mr_status_name}} </td>
                    <td> {{tr.mr_type_work_name}} </td>
                    <td> {{tr.mr_branch_code_send}} {{tr.mr_branch_name_send}} </td>
                    <td> {{tr.mr_branch_code}} {{tr.mr_branch_name}} </td>
                </tr>
                {% endfor %}
				</tbody>
            </table>
           <div class='loading'>
                <img src="../themes/images/loading.gif" class='img_loading'>
            </div>
        </div>
      </div>
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

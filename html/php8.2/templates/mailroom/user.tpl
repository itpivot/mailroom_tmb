{% extends "base_emp2.tpl" %}

{% block title %}- List{% endblock %}

{% block menu_e1 %} active {% endblock %}
{% block styleReady %}
    body {
      height:100%;
    }
    .table-hover tbody tr.hilight:hover {
        background-color: #555;
        color: #fff;
    }

    table.dataTable tbody tr.hilight {
		background-color: #7fff7f;
    }

    table.dataTable tbody tr.selected {
		background-color: #555;
		color: #fff;
		cursor:pointer;
	}

    #tb_work_order {
		font-size: 13px;
    }

	.panel {
		margin-bottom : 5px;
	}
	.loading {
		position: fixed;
		top: 40%;
		left: 50%;
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
		z-index: 1000;
	}
	
	.img_loading {
		width: 350px;
		height: auto;
	}
	
	
	
	.btn-default.btn-on.active{background-color: #5BB75B;color: white;}
	.btn-default.btn-off.active{background-color: #DA4F49;color: white;}
	
	.btn-default.btn-on-1.active{background-color: #006FFC;color: white;}
	.btn-default.btn-off-1.active{background-color: #DA4F49;color: white;}
	
	.btn-default.btn-on-2.active{background-color: #00D590;color: white;}
	.btn-default.btn-off-2.active{background-color: #A7A7A7;color: white;}
	
	.btn-default.btn-on-3.active{color: #5BB75B;font-weight:bolder;}
	.btn-default.btn-off-3.active{color: #DA4F49;font-weight:bolder;}
	
	.btn-default.btn-on-4.active{background-color: #006FFC;color: #5BB75B;}
	.btn-default.btn-off-4.active{background-color: #DA4F49;color: #DA4F49;}
	
	
	
{% endblock %}
{% block scriptImport %}

<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">

<link rel="stylesheet" href="../themes/bootstrap/css/bootstrap-datepicker.min.css">
<script src="../themes/bootstrap/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>

{% endblock %}

{% block domReady %}
		$('.input-daterange').datepicker({
			autoclose: true,
			clearBtn: true
		});	
		
		var table = $("#tb_work_order").DataTable();
		
		table.destroy();
		table = $("#tb_work_order").DataTable({
			"ajax": {
				"url": "ajax/ajax_get_user.php",
				"type": "POST",
			},
		
			'columns': [
				{ 'data':'no' },
				{ 'data':'mr_emp_name'},
				{ 'data':'mr_emp_lastname' },
				{ 'data':'mr_emp_code' },
				{ 'data':'mr_emp_email' },
				{ 'data':'isLogin' },
				{ 'data':'isFailed' },
				{ 'data':'mr_user_username' },
				{ 'data':'mr_user_role_name' },
				{ 'data':'re_password' }
			],
			'scrollCollapse': true
		});
		
		$("#pass_emp_send").keyup(function(){
			var pass_emp = $("#pass_emp_send").val();
			$.ajax({
				url: "ajax/ajax_autocompress2.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res.status == 200){
						$("#name_sender").val(res.data.mr_emp_name);
						$("#pass_depart_send").val(res.data.mr_department_code);
						$("#floor_send").val(res.data.mr_department_floor);
						$("#depart_send").val(vmr_department_name);
						$("#emp_id_send").val(vmr_emp_id);
					}else{
						alertify.alert('ผิดพลาด',"บันทึกไม่สำเร็จ  "+res.message,function(){window.location.reload();});
					}
				}							
			});					
		});			
		
		
		$("#unlock_all").click(function(){
			alertify.confirm("ปลดล็อคทุก User!", function() {
				$.ajax({
					url: "ajax/ajax_unlock_user.php",
					type: "post",
					data: {
						'status': 0,
						'user_id': 0,
						'page': 3,
					},
					dataType: 'json',
					success: function(res){
						if(res.status != 200){
							alertify.alert('ผิดพลาด',"บันทึกไม่สำเร็จ  "+res.message,function(){window.location.reload();});
						}
					}		
				});
			});			
		});
		
		
		$("#btn_excel").click(function() {
			var data = new Object();
			data['barcode'] 			= $("#barcode").val();
			data['sender'] 				= $("#pass_emp_send").val();
			data['receiver'] 			= $("#pass_emp_re").val();
			data['start_date'] 			= $("#start_date").val();
			data['end_date'] 			= $("#end_date").val();
			data['status'] 				= $("#status").val();
			var param = JSON.stringify(data);
		
			// window.open('excel.report.php?params='+param);
			location.href = 'excel.report.php?params='+param;
		
		});
	$('#success_msg').hide();	
	$("#re_password").click(function(){

	$('#success_msg').hide();
	$('.loading').hide();

	
	
	alertify.prompt( 'Reset Password', 'รหัสพนักงาน ที่ต้องการ reset', 'รหัสพนักงาน'
               , function(evt, value) { 
					//alertify.success('You entered: ' + value) 
					if(value != null || value != "") {
                                $.ajax({
                                    url: "./ajax/ajax_forget_password.php",
									type: 'POST',
									dataType: 'json',
                                    data: {
                                        emp_code: value
                                    },
                                    success: function(res) {
										$('.loading').hide();
										if(res.status == 200){
											alertify.success(res.message);
											$('#success_msg').show();
											$('#success_msg').html(res.html);
										}else{
											alertify.error(res.message);
										}
                                    },error: function (error) {
										alert('error; ' + eval(error));
										$('.loading').hide();
									},beforeSend: function() {
										$('#success_msg').hide();
										$('.loading').show();
									}
                                })
                        }
				}, function() { 
					alertify.error('Cancel') 
				});
		});
		
		
		
{% endblock %}


{% block javaScript %}
    /* function getData(){
			table.rows('.selected').remove().draw( false );
			$("#barcode").val("");
			table.destroy();
			var table = $("#tb_work_order").DataTable({
			"ajax": {
				"url": "ajax/ajax_load_work_mailroom.php",
				"type": "POST"
			},
			'columns': [
				{ 'data':'mr_work_main_id' },
				{ 'data':'time_test '},
				{ 'data':'mr_work_barcode' },
				{ 'data':'name_re' },
				{ 'data':'lastname_re' },
				{ 'data':'mr_emp_name' },
				{ 'data':'mr_emp_lastname' }
			],
			'order': [[ 0, "desc" ]],
			'scrollCollapse': true
		});
			
	};
 */




function re_password(mr_user_id){		
		$('#success_msg').hide();
		$('.loading').hide();
	
		alertify.prompt( 'Reset Password', 'ยืนยันรหัสพนักงาน ที่ต้องการ reset', 'รหัสพนักงาน'
				   , function(evt, value) { 
						//alertify.success('You entered: ' + value) 
						if(value != null || value != "") {
									$.ajax({
										url: "./ajax/ajax_forget_password.php",
										type: 'POST',
										dataType: 'json',
										data: {
											emp_code	: value,
											mr_user_id		: mr_user_id
										},
										success: function(res) {
											$('.loading').hide();
											if(res.status == 200){
												alertify.success(res.message);
												$('#success_msg').show();
												$('#success_msg').html(res.html);
											}else{
												alertify.error(res.message);
											}
										},error: function (error) {
											alert('error; ' + eval(error));
											$('.loading').hide();
										},beforeSend: function() {
											$('#success_msg').hide();
											$('.loading').show();
										}
									})
							}
					}, function() { 
						alertify.error('Cancel') 
					});
			};




 function isFailed( id , val ){
	$.ajax({
		url: "ajax/ajax_unlock_user.php",
		type: "post",
		data: {
			'status': val,
			'user_id': id,
			'page': 1,
		},
		dataType: 'json',
		success: function(res){
			if(res.status == 200){
				alertify.success(res.message);
			}else{
				alertify.error(res.message);
			}
		}							
	});
	
 };
 
 
 function isLogin( id , val ){
	$.ajax({
		url: "ajax/ajax_unlock_user.php",
		type: "post",
		data: {
			'status': val,
			'user_id': id,
			'page': 2,
		},
		dataType: 'json',
		success: function(res){
			if(res.status == 200){
				alertify.success(res.message);
			}else{
				alertify.error(res.message);
			}
		}							
	});
	
 };
 
 
 
{% endblock %}

{% block Content2 %}
			
	<div class="row">
        <div class="col-md-12">
		<div class='loading' style="display: none;">
                <img src="../themes/images/loading.gif" class='img_loading'>
</div>
			<h1><center>จัดการบัญชีผู้เข้าใช้งานระบบ</center></h1>
           
        </div>
    </div>	
	
	
			
				
		 <div class="panel panel-default">
                  <div class="panel-body">
				  <div id="success_msg" class="alert alert-primary" role="alert">
					 
					</div>
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover display responsive no-wrap" id="tb_work_order">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>ชื่อ</th>
                            <th>นามสกุล</th>
                            <th>รหัสพนักงาน</th>
                            <th>E-mail</th>
                            <th>ค้างอยู่ในระบบ</th>
                            <th>ล็อค ใส่รหัสผิด</th>
                            <th>username</th>
                            <th>สิทธิ์การเข้าใช้งาน {# <button type="button" class="btn btn-outline-warning" id="re_password">Reset Password </button></th>#}
                            <th>re_password</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
        </div>
		
		



		{#
<div class="row">
        <div class="col-md-1" style="margin-top:30px;">
			<button type="button" class="btn btn-outline-warning" id="unlock_all">Unlock User All </button>
           
        </div>
		<div class="col-md-9">
		</div>
		<div class="col-md-1" style="margin-top:30px;">
			<button type="button" class="btn btn-outline-warning" id="re_password">Reset Password </button>
           
        </div>
    </div>
			
	
	#}

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

{% extends "base_emp.tpl" %}

{% block title %} ยืนยันการส่ง {% endblock %}

{% block menu_msg2 %} active {% endblock %}

{% block scriptImport %}
<script src="../themes/bootstrap_emp/jquery/signature_pad.min.js"></script>
{% endblock %}
{% block styleReady %}
    #signature-pad {
        border: 1px solid #eee;
        width: 400px;
        height: 200px;
        margin: 10px 20px;
        background-color: rgb(233, 235, 238);
    }

    #lb_sign {
        color: #868e96;
        font-weight: bold;
    }

    @@media screen and (min-width: 480px) {
        #signature-pad {
            display: block;
            margin: auto;
            width: 50%;
        }

        #btn_contrainer {
            margin-top: 15px;
        }
      
    }
	#img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
    }
	#pic_loading {
        width: 600px;
        height: auto;
    }

{% endblock %}

{% block domReady %}

	$('#img_loading').hide();
   
    $('#btn_save').on('click', function() {
        let count_sign = signaturePad.toData();
        let signatureCM = signaturePad.toDataURL(); 
        let send_id = '{{ send_id }}';
        if(count_sign.length <= 0) {
            alert('ท่านยังไม่เซ็นชื่อ!');
            return;
        } else {
           $.ajax({
               url: './ajax/ajax_updateSendMailroom.php',
               type: 'POST',
               data: {
                   sign: signatureCM,
                   id: send_id
               },
               dataType: 'html',
			   cache: false,
				beforeSend: function() {
					$('#img_loading').show();
				},
               success: function(res){
                   //console.log(res);
                    if(res == "success") {
                        location.href = "../messenger/rate_send.php";
                    }
               },
			complete: function() {
				$('#img_loading').hide();
			}
           }) 
        }
    });

    $('#clear').on('click', function() {
        signaturePad.clear();
    })

{% endblock %}
{% block javaScript %}

var canvas = document.getElementById('signature-pad');
var signaturePad = new SignaturePad(canvas, {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    penColor: 'rgb(0, 0, 0)'
});

function resizeCanvas() {
    
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);

    signaturePad.clear();
}

window.onresize = resizeCanvas;
resizeCanvas();

// document.getElementById('clear').addEventListener('click', function () {
//     signaturePad.clear();
//   });


{% endblock %}


{% block Content %}
	<div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
    
    <h4>{{ barcode }}</h4>
    <label id="lb_sign">กรุณาเซ็นชื่อยืนยันการรับเอกสาร</label>
    <div class="row">
        <canvas id="signature-pad" class="signature-pad"></canvas>
    </div>
    <div class="text-center" id="btn_contrainer">
        <button id="clear" class='btn btn-danger'>ล้าง</button>
        <button id="btn_save" class='btn btn-primary'>บันทึก</button>
    </div>
     
	
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

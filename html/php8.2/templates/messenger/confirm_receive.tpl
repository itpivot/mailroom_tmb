{% extends "base_emp2.tpl" %}

{% block title %} - List{% endblock %}
{% block styleReady %}
    
	#img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
		z-index: 1050;
    }
	#pic_loading {
        width: 600px;
        height: auto;
    }

{% endblock %}

{% block domReady %}
 


$('#img_loading').hide();
    $('#btn_save').on('click', function() {
			var emp_id 				= $("#emp_id").val();
			var work_id 			= $("#work_id").val();
			var work_id2 			= $("#work_id2").val();
			var user_pass 			= $("#user_pass").val();
			var obj = { emp_id: emp_id, work_id: work_id, user_pass: user_pass };

			$.when(confirmReceive(obj)).then(function(res) {
				if(res == 1) {
					$('#img_loading').show();
					if(confirm("ยืนยันการส่งเอกสาร!")) {
						return updateWorkOrders(work_id, emp_id);
					}
				} else {
					alert('ข้อมูลไม่ถูกต้องถูกต้อง');
					$("#emp_id").val('');
					$("#user_pass").val('');
				}
			}).done(function(data) {
				if(data == 'success') {
					$('#img_loading').hide();
					window.location.href = "../messenger/rate_send.php?id=" + work_id2;
					
				} 
			});

			// confirmReceive(obj).then(function(res) {
			// 	if(res == 1) {
					
			// 		updateWorkOrders(work_id).then(function(res) {
			// 			if(res == "success") {
			// 				$('#img_loading').hide();
			// 				location.href = "../messenger/rate_send.php?id=" + work_id;
			// 			} else {
							
			// 			}
			// 		})
			// 	}	
			// })

            // $.ajax({
            //     url: './ajax/ajax_confirm_receive.php',
            //     type: 'POST',
            //     data: {
			// 		'emp_id': emp_id,
			// 		'work_id': work_id,
			// 		'user_pass': user_pass
            //     },
				
            //     success: function(res){
            //         if( res == 1 ) {
			// 			if(confirm("ยืนยันการส่งเอกสาร!")) {
			// 					 $.ajax({
			// 						url: './ajax/ajax_updateSendMailroom.php',
			// 						type: 'POST',
			// 						data: {
			// 							id: work_id
			// 						},
			// 						dataType: 'html',
			// 						cache: false,
			// 						beforeSend: function() {
			// 								$('#img_loading').show();
			// 							},
			// 						success: function(res){
			// 								if(res == "success") {
			// 									location.href = "../messenger/rate_send.php?id="+work_id;
			// 								}
			// 						},
			// 						complete: function() {
			// 							$('#img_loading').hide();
			// 						}
			// 					})
						
            //            }
            //         }else{
			// 			alert('ข้อมูลไม่ถูกต้องถูกต้อง');
			// 			$("#emp_id").val('');	
			// 			$("#user_pass").val('');	
			// 		}
            //     }
            // });
        
    });
{% endblock %}

{% block javaScript %}
	var confirmReceive = function(obj) {
		return $.post('../messenger/ajax/ajax_confirm_receive.php', obj);
	}

	var updateWorkOrders = function(id, emp_id) {
		return $.post('../messenger/ajax/ajax_updateSendMailroom.php', { id: id, username: emp_id });
	} 

{% endblock %}


{% block Content %}
<div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
				<label><h3>ยืนยันการรับเอกสาร</h3></label>
				<div class="form-group">
					<label>รหัสพนักงาน</label>
					<input type="text" class="form-control" id="emp_id" placeholder="รหัสพนักงาน">
					<input type="hidden" id="work_id" value="{{ work_id }}">
					<input type="hidden" id="work_id2" value="{{ work_id2 }}">
				</div>
				<div class="form-group">
					<label>รหัสผ่านการเข้าสู่ระบบ</label>
					<input type="password" class="form-control" id="user_pass" placeholder="Password">
				</div>
				<div class="row">
					<div class="col-3"></div>
					
					<div class="col-6">
						<button type="btn" class="btn btn-primary" style="width:100%;" id="btn_save">Submit</button>
					</div>
					
					<div class="col-3"></div>
				</div>
			

{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

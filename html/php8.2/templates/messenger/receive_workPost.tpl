{% extends "base_emp2.tpl" %}

{% block title %} Send List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .card {
       margin: 8px 0px;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 70%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .space-height p#departs {
       display: inline-block;
    }
    #img_loading {
        position: fixed;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
    }
    #pic_loading {
        width: 350px;
        height: auto;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
	
	.fixed-bottom {
		position: sticky;
		bottom: 0;
		//top: 250px;
		z-index: 1075;

	}

{% endblock %}

{% block domReady %}
    var receive_list = {};
    
    $.ajax({
        url: './ajax/ajax_get_receiveworkPost.php',
        type: 'POST',
        dataType: 'json',
        cache: false,
        beforeSend: function() {
            $('#img_loading').show();
        },
        success: function(res) {
            document.getElementById("badge-show").textContent=res.length;
			let set_style_time = '' ;
            let str = '';
			if( res != "" ){
				for(let i = 0; i < res.length; i++) {
                    receive_list[i] = res[i]['mr_work_main_id'];

                    if( res[i]['diff_time'] == 1 ){
                        set_style_time = 'bg-danger';
                    }else{
                        set_style_time = '';
                    }
						
						if( res[i]['mr_status_receive'] == 2 ){
								str += '<div class="card bg-warning">'; 
								str += '<div class="card-body space-height">';
									str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
									str += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
									
									str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+'</a> / <a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a></p>';
											
									str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
									str += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
									str += '<p><b>วันที่สร้างรายการ : </b>'+res[i]['sys_time']+'</p>';
									str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
									str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
									str += '<hr>';
									str += '<div class="row">'
									/* str += '<div class="col-6 text-left btn-zone"><a href="#" class="btn btn-danger right" onclick="updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );"><b>ยกเลิก</b></a></div>' */
									str += '<div class="col-12 text-right btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById('+res[i]['mr_work_main_id']+ ', 2 );"><b>รับเอกสาร</b></a></div>'
									str += '</div>';
									str += '</div>';
							str += '</div>';
						}else{
	
							str += '<div class="card '+set_style_time+'">'; 
								str += '<div class="card-body space-height">';
									str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
									str += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
									
									str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+' </a> / <a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a></p>';
									
									str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
									str += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
									str += '<p><b>วันที่สร้างรายการ : </b>'+res[i]['sys_time']+'</p>';
									str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
									str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
									str += '<hr>';
									str += '<div class="row">'
									str += '<div class="col-6 text-left btn-zone"><a href="#" class="btn btn-warning" onclick="updateNoReceiveById('+res[i]['mr_work_main_id']+');"><b>ไม่พบเอกสาร</b></a></div>'
									/* str += '<div class="col-4 text-right btn-zone"><a href="#" class="btn btn-danger right" onclick="updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );"><b>ยกเลิก</b></a></div>' */
									str += '<div class="col-6 text-right btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById(' + res[i]['mr_work_main_id'] + ', 2 );"><b>รับเอกสาร</b></a></div>'
									str += '</div>';
									str += '</div>';
							str += '</div>';
						}
	
	
				}
			}else{
				$('#btn_save_all').hide();
				str = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                 
			}
            $('#data_list').html(str);
        },
        complete: function() {
            $('#img_loading').hide();
        }
    });

    $('#txt_search').on('keyup', function() {
        receive_list = {};
        let txt = $(this).val();
        let str = "";
        $.ajax({
            url: "./ajax/ajax_searchReceiveMailroom.php",
            type: "POST",
            data: {
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                $('#img_loading').show();
                $('#data_list').hide();
            },
            success: function(res){
                document.getElementById("badge-show").textContent=res.length;
			let set_style_time = '' ;
            let str = '';
			if( res != "" ){
				for(let i = 0; i < res.length; i++) {
                    receive_list[i] = res[i]['mr_work_main_id'];

                    if( res[i]['diff_time'] == 1 ){
                        set_style_time = 'bg-danger';
                    }else{
                        set_style_time = '';
                    }
						
						if( res[i]['mr_status_receive'] == 2 ){
								str += '<div class="card bg-warning">'; 
								str += '<div class="card-body space-height">';
									str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
									str += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
									
									str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+'</a> / <a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a></p>';
											
									str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
									str += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
									str += '<p><b>วันที่สร้างรายการ : </b>'+res[i]['sys_time']+'</p>';
									str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
									str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
									str += '<hr>';
									str += '<div class="row">'
									/* str += '<div class="col-6 text-left btn-zone"><a href="#" class="btn btn-danger right" onclick="updateReceiveById('+res[i]['mr_work_main_id']+ ', 1 );"><b>ยกเลิก</b></a></div>' */
									str += '<div class="col-12 text-right btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById('+res[i]['mr_work_main_id']+ ', 2 );"><b>รับเอกสาร</b></a></div>'
									str += '</div>';
									str += '</div>';
							str += '</div>';
						}else{
	
							str += '<div class="card '+set_style_time+'">'; 
								str += '<div class="card-body space-height">';
									str += '<h5 class="card-title text-right">'+res[i]['mr_work_barcode']+'</h5>';
									str += '<p><b>ผู้ส่ง : </b>'+res[i]['mr_emp_name']+' '+res[i]['mr_emp_lastname']+'</p>';
									
									str += '<p><b>เบอร์ติดต่อ : </b><a href="tel:'+res[i]['mr_emp_mobile']+'">'+res[i]['mr_emp_mobile']+' </a> / <a href="tel:'+res[i]['mr_emp_tel']+'">'+res[i]['mr_emp_tel']+'</a></p>';
									
									str += '<p id="departs"><b>แผนก : </b>'+res[i]['mr_department_name']+'</p>';
									str += '<p><b>รับที่ชั้น : </b>'+res[i]['mr_department_floor']+'</p>';
									str += '<p><b>วันที่สร้างรายการ : </b>'+res[i]['sys_time']+'</p>';
									str += '<p><b>ชื่อเอกสาร : </b>'+res[i]['mr_topic']+'</p>';
									str += '<p><b>หมายเหตุ : </b>'+res[i]['mr_work_remark']+'</p>';
									str += '<hr>';
									str += '<div class="row">'
									str += '<div class="col-6 text-left btn-zone"><a href="#" class="btn btn-warning" onclick="updateNoReceiveById('+res[i]['mr_work_main_id']+');"><b>ไม่พบเอกสาร</b></a></div>'
									/* str += '<div class="col-4 text-right btn-zone"><a href="#" class="btn btn-danger right" onclick="updateReceiveById(' + res[i]['mr_work_main_id'] + ', 1 );"><b>ยกเลิก</b></a></div>' */
									str += '<div class="col-6 text-right btn-zone"><a href="#" class="btn btn-primary right" onclick="updateReceiveById(' + res[i]['mr_work_main_id'] + ', 2 );"><b>รับเอกสาร</b></a></div>'
									str += '</div>';
									str += '</div>';
							str += '</div>';
						}
	
	
				}
			}else{
				$('#btn_save_all').hide();
				str = '<center>ไม่มีเอกสารที่ต้องรับ !</center>';
                 
			}
            $('#data_list').html(str);
            },
            complete: function() {
                $('#img_loading').hide();
                $('#data_list').show();
            }
        });
    });

    $('#btn_save_all').on('click', function() {
		if(confirm("ยืนยันการรับงานทั้งหมด!")) {
            $.ajax({
                url: './ajax/ajax_updateReceiveAll.php',
                type: 'POST',
                data: {
                    data: JSON.stringify(receive_list)
                },
                success: function(res){
                    if(res == "success") {
                        location.reload();
                    }
                }
            });
        }
    });
{% endblock %}

{% block javaScript %}
	 function goPageSend()
    {
		window.location.href = 'send_list.php'; 
	}
	
    function updateReceiveById(id, work_status)
    {
        if( work_status == 1 ){
			if(confirm("ยืนยันการลบงาน!")) {
				//console.log('id :'+ id + ' status :' + work_status);
                $.ajax({
                    url: './ajax/ajax_updateReceiveWorkByhandById.php',
                    type: 'POST',
                    data: {
                        id: id,
                        work_status: work_status
                    },
                    success: function(res) {
                        if(res == "success") {
                            location.reload();
                        }
                    }
                })
				alert('ลบงานเรียบร้อยแล้ว');
				 
				
			}
        }else{
             $.ajax({
                    url: './ajax/ajax_updateReceiveWorkByhandById.php',
                    type: 'POST',
                    data: {
                        id: id,
                        work_status: work_status
                    },
                    success: function(res) {
                        if(res == "success") {
                            location.reload();
                        }
                    }
                })
        }

        
        
    }
	
	function updateNoReceiveById(id) {
        $.ajax({
            url: './ajax/ajax_updateNoReceiveworkByhandById.php',
            type: 'POST',
            data: {
                id: id
            },
            success: function(res) {
                if(res == "success") {
                    location.reload();
                }
            }
        })
    }
{% endblock %}

{% block Content %}
    <div id='img_loading'><img src="../themes/images/loading.gif" id='pic_loading'></div>
    <div class="search_list">
        <form>
            <div class="form-group">
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหา">
            </div>
        </form>
    </div>
    <div class="text-center">
        <label>จำนวนเอกสาร</label>
        
            <span id="badge-show" class="badge badge-secondary badge-pill badge-dark"></span>

         <label>ฉบับ</label>
    </div>
	<button type="button" class="btn btn-primary btn-block btn-lg" id="btn_save_all">รับเอกสารทั้งหมด</button>
	<div id="data_list">
	</div>
	<div class="fixed-bottom text-right">
		<button onclick="goPageSend();" type="button" class="btn btn-secondary btn-lg btn-block">ส่งเอกสาร</button>
	</div>


 </div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


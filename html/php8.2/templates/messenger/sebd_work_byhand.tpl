{% extends "base_emp2.tpl" %}

{% block title %} Send List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}
    <link rel="stylesheet" href="../themes/jquery/jquery.signaturepad.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js" charset="utf-8"></script>
{% endblock %}
{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
	
	.fixed-bottom {
		position: sticky;
		bottom: 0;
		//top: 250px;
		z-index: 1075;

	}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
	#loading{
			display: block;
			margin-left: auto;
			margin-right: auto;
			width: 50%;
		
	}
    #signature-pad {
        position: relative;
        width: 100%;
        height: 400px;
    }

    #sign {
        position: absoute;
        border: 1px solid #000;
        width: 100%;
        height: 100%;
    }

{% endblock %}

{% block domReady %}
    $('.block_btn').hide();
    var canvas = document.getElementById("sign");
    var signaturePad = new SignaturePad(canvas);
    var receive_list = {};
    getReceiveBranch();

    $('#txt_search').keyup(function(e) {
        var txt = e.target.value;

        $.ajax({
            url: './ajax/ajax_getSendByhand.php',
            type: 'POST',
            cache: false,
            data: {
                type: 'get_mailroom_search',
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function(resp) {
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                // console.log(resp['counter'])
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function() {
                // loading hide
				$('#loading').hide();
            }
        })

    });


    $('#signModal').modal({
        keyboard: false,
        backdrop: false,
        show: false
    });
    $('#signModal').on('shown.bs.modal', function() {
        resizeCanvas()
        $('.result_bch').hide();
		slideout.disableTouch();
        $('#clear-signature').on('click', function(){
            signaturePad.clear();
        });

    });
    $('#signModal').on('hidden.bs.modal', function() {
        signaturePad.clear();
        $('.result_bch').show();
        slideout.enableTouch();
    });


    $('#sender_status').on('change', function() {
        var st = $(this).val();
        if(st == 1){
            $('#div-signature').show();
            $('#div-img').hide();
            $('#div_resive_name').show();
            $('#div_resive_lname').show();
            $('#clear-signature').show();
        }else{
            $('#div_resive_name').hide();
            $('#div_resive_lname').hide();
            $('#div-signature').hide();
            $('#clear-signature').hide();
            $('#div-img').show();
        }
    });
    $('#btn-upload').on('click', function() {
       
        var formData        = new FormData();
        var sender_status     = $('#sender_status').val();
        var resive_name     = $('#ืresive_name').val();
        var resive_lname    = $('#ืresive_lname').val();
        var remark          = $('#remark').val();
        var barcode         = '{{ data.barcode }}';
        var wId             = JSON.stringify(selectChoice);
        var data            = signaturePad.toDataURL();
        var image_encode    = '';
        if(sender_status == 1){
            if (signaturePad.isEmpty()) {
                alertify.alert('ผิดพลาด','กรุณาให้ลูกค้าเซ็นยืนยัน');
                return;
            }
            image_encode    = data.replace(/^data:image\/(png|jpg);base64,/, "");
        }else{
            var selectedFile = $('#myfile')[0].files[0];
            if(selectedFile == null) {
                swal({
                    title: 'Warning!',
                    text: 'ยังไม่ได้เลือกรูป',
                    type: 'warning',
                    timer: 3000
                });
                return;
            }

            if(selectedFile['type'] !== 'image/jpg' && selectedFile['type'] !== 'image/jpeg') {
                   swal({
                        title: 'Alert',
                        text: 'ชนิดไฟล์ไม่ถูกต้อง กรุณาเลือกรูปเฉพาะที่เป็น JPEG',
                        type: 'error',
                        timer: 3000
                    });
                    return;
            }
            formData.append('fileToUpload', selectedFile);


        }


        formData.append('barcode', barcode);
        formData.append('sender_status', sender_status);
        formData.append('resive_name', resive_name);
        formData.append('resive_lname', resive_lname);
        formData.append('remark', remark);
        formData.append('wId', wId);
        formData.append('signature', image_encode);

        $.ajax({
            url: './ajax/ajax_save_signature.php',
            method: 'post',
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            beforeSend: function () {
                // loading show
               $('#loading').show();
               $('.result_bch').html('');
            },
            success: function(resp) {
                if(resp) {
                    if(resp.status == 200) {
                        $('#btn_checked').html('เลือกทั้งหมด');
                        $('.block_btn').hide();
                        signaturePad.clear();
                        document.getElementById('form-upload').reset();
                        $("#output").attr("src", '../themes/images/upload-icon.png');
                        $('#signModal').modal('hide');
                        getReceiveBranch();
                    } else {
                        // alert error
                        alertify.alert('เกิดข้อผิดพลาด',resp.error);
                        
                    }
                } else {
                     // alert error
                     alertify.alert('เกิดข้อผิดพลาด',resp.error);
                }
            }
        });
    });

   
{% endblock %}

{% block javaScript %}

File.prototype.convertToBase64 = function(callback){
    var reader = new FileReader();
    reader.onloadend = function (e) {
        callback(e.target.result, e.target.error);
    };   
    reader.readAsDataURL(this);
};

var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

function resizeCanvas() {
    var canvas = document.getElementById("sign");
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}
    var selectChoice = [];
    
    function branch_chang(b_name){
        var txt = b_name;

        $.ajax({
            url: './ajax/ajax_getSendByhand.php',
            type: 'POST',
            cache: false,
            data: {
                type: 'get_mailroom_search',
                txt: txt
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function(resp) {
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                // console.log(resp['counter'])
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function() {
                // loading hide
				$('#loading').hide();
            }
        })
    }
    function getReceiveBranch()
    {
        $.ajax({
            url: "./ajax/ajax_getSendByhand.php",
            type: "GET",
            cache: false,
            data: {
                type: 'get_mailroom'
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function() {
                // loading hide
				$('#loading').hide();
            } 
        });
    }


    function updateStatus(wId, actions)
    {
        $.ajax({
            url: './ajax/ajax_getSendByhand.php',
            type: 'POST',
            cache: false,
            data: {
                wId: wId,
                action: actions,
                type: 'update_mailroom'
            },
            dataType: 'json',
            beforeSend: function () {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function (resp) {
                // result
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function () {
                // loading hide
				$('#loading').hide();
            } 
        });
    }

     function selectedCard(wId, elm) {
         var foundArr = selectChoice.indexOf(wId);
         if ($('#' + elm.id).is(':checked')) {
             if (foundArr == -1) {
                 selectChoice.push(wId)
             }
         } else {
             if (foundArr != -1) {
                 selectChoice.splice(foundArr, 1);
             }
         }


         if (selectChoice.length > 0) {
             $('.block_btn').fadeIn();
             $('#btn_checked').html('ยกเลิก');
         } else {
             $('.block_btn').fadeOut();
             $('#btn_checked').html('เลือกทั้งหมด');
         }
         console.log(selectChoice)
     }

     function checkChoice() {

         $('.check_all').each(function (i, elm) {
             var wId = parseInt($('#' + elm.id).attr('data-value'));
             var foundArr = selectChoice.indexOf(wId);

             $(elm).prop('checked', !elm.checked);
             if (elm.checked) {
                 if (foundArr == -1) {
                     selectChoice.push(wId)
                 } else {

                 }
             } else {
                 if (foundArr != -1) {
                     selectChoice.splice(foundArr, 1);
                 }
             }
         });

         if (selectChoice.length > 0) {
             $('.block_btn').fadeIn();
             $('#btn_checked').html('ยกเลิก');
         } else {
             $('.block_btn').fadeOut();
             $('#btn_checked').html('เลือกทั้งหมด');
         }

     }

     function updateStatusAll(actions) {
         $.ajax({
             url: './ajax/ajax_getSendByhand.php',
             type: 'POST',
             cache: false,
             data: {
                 wId: JSON.stringify(selectChoice),
                 action: actions,
                 type: 'update_all_mailroom'
             },
             dataType: 'json',
             beforeSend: function () {
                 // loading show
				$('#loading').show();
				$('.result_bch').html('');
             },
             success: function (resp) {
                 // result
                 if(resp['status'] == 200){
                    $('#btn_checked').html('เลือกทั้งหมด');
                    $('.block_btn').hide();
                    getReceiveBranch();
                 }else{
                    alertify.alert('เกิดข้อผิดพลาด',resp['message']);
                 }
             },
             complete: function () {
                 // loading hide
				 $('#loading').hide();
             }
         });
     }
     function toggle_ico(x) {
      //console.log('------');
        var icon = $('#arrow_'+x);
        icon.toggleClass('up');
        if ( icon.hasClass('up') ) {
          icon.text('keyboard_arrow_down');
        }else {
          icon.text('keyboard_arrow_up');
        }
      }
{% endblock %}

{% block Content %}

<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="signModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <button id="clear-signature">ล้างเซ็นใหม่</button>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="form-upload">
            <div class="container">
            <div class="row">
               
                <div class="col" id="div-signature">
                    <div id="signature-pad">
                        <canvas id="sign"></canvas>
                    </div>
                </div>
                 <div class="col text-center" id="div-img" style="display: none;">
                    <input id="myfile" type="file" accept="image/*" onchange="loadFile(event)">
                    <img width="300px" id="output"/ src="../themes/images/upload-icon.png">CloudUpload
                </div>
            </div>
            <br>
            <div class="row justify-content-md-center">
                <div class="col">
                    <h5>ข้อมูลผู้รับ</h5>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="form-group col-md-10">
                    <label for="sender_status">สถานะการจัดส่งเอกสาร</label>
                    <select id="sender_status" class="form-control">
                      <option value="1" selected>สำเร็จ(มีผู้รับเอกสาร)</option>
                      <option value="2" >สำเร็จ(ไม่มีมีผู้รับเอกสาร)</option>
                    </select>
                  </div>

                <div class="form-group col-md-10" id="div_resive_name">
                    <label for="resive_name">ชื่อ</label>
                    <input type="text" class="form-control" id="resive_name" name="ืresive_name">
                </div>
                <div class="form-group col-md-10" id="div_resive_lname">
                    <label for="resive_lname">นามสกุล</label>
                    <input type="text" class="form-control" id="resive_lname" name="ืresive_lname">
                </div>
                <div class="form-group col-md-10">
                <div class="form-group">
                    <label for="remark">หมายเหตุ</label>
                    <textarea class="form-control" id="remark" rows="3"></textarea>
                  </div>
                  </div>
                </div>
            </div>
            </form>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ย้อนกลับ</button>
          <button id="btn-upload" type="button" class="btn btn-primary">บันทึก</button>
        </div>
      </div>
    </div>
  </div>
<div class="content_bch">
    <h3>ส่งเอกสาร BY HAND</h3><hr>
    <div class="header_bch">
        <div class="form-group">
            <p class="font-weight-bold text-muted">ทั้งหมด <span id="counter_works">0</span> งาน </p>
            <br>
            <input type="text" class="form-control" id="txt_search" placeholder="ค้นหาจากหมายเลขงาน, สาขา, และชื่อ-สกุลผู้ส่ง" />
        </div>
    </div>
    <button type="button" style="display:none;" class="btn btn-secondary btn-block my-2" id="btn_checked" onclick="checkChoice();">เลือกทั้งหมด</button>
    <div class="block_btn my-2">
        <button type="button" id="signModal" class="btn btn-primary btn-block"  data-toggle="modal" data-target="#signModal">ส่งเอกสาร</button>
        {#<button type="button" id="btn_not_found" class="btn btn-warning btn-block" onclick="updateStatusAll(2);">ไม่พบเอกสาร</button>#}
    </div>
	<img id="loading" src="../themes/images/loading.gif">
    <div class="result_bch">
        
    </div>
</div>
<div class="footer_bch">
    
</div>



{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


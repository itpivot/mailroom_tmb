{% extends "base_emp2.tpl" %}

{% block title %} Send List {% endblock %}

{% block menu_msg3 %} active {% endblock %}

{% block scriptImport %}{% endblock %}

{% block styleReady %}

    .content_bch {
        position: relative;
        margin: 0;
        padding-bottom: 4rem;
        min-height: 100%;
        
    }

    .header_bch {
        margin: 0 auto;
        text-align: center;
    }

    .result_bch {
        // background-color: #d3d3d3;
        position: relative;
        margin: 0;
        height: 100%;
        overflow: auto;
        overflow: overlay;  /* Chrome */
    }

    /**
    * Footer Styles
    */
    
    .footer_bch {
        position: fixed;
        right: 0;
        bottom: 0;
        left: 0;
        margin: 0;
        padding: 1rem;
        //background-color: #efefef;
        text-align: center;
    }

    .space-height {
        padding: 10px 15px;
        line-height: 40%;
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .btn-zone {
        margin-top: -10px;
		
    }

	.right {
		 // margin-right: -190px; 
		// position: absolute;
		right: 0px;
	}
		
    }

    .btn {
        border-radius: 0px;
    }

    .bg-danger{
        background-color: #dc3545!important;
        color: #fff!important;
    }
	
	.fixed-bottom {
		position: sticky;
		bottom: 0;
		//top: 250px;
		z-index: 1075;

	}

    .space-height p#departs {
       display: inline-block;
    }

    @media only screen and (max-width: 320px) {
    /* For mobile phones: */
        .content_bch * {
            font-size: 14px !important;
        }
    }
	#loading{
			display: block;
			margin-left: auto;
			margin-right: auto;
			width: 50%;
		
	}
{% endblock %}

{% block domReady %}
    $('.block_btn').hide();

    var receive_list = {};
    getSendData();

    

{% endblock %}

{% block javaScript %}
    var selectChoice = [];

    function getSendData(branch_id)
    {
        if(!branch_id){ 
            $('.block_btn').hide();
        }

        $.ajax({
            url: "./ajax/ajax_getReceiveHub.php",
            type: "GET",
            cache: false,
            data: {
                type: 'send',
                branch_id: branch_id
            },
            dataType: 'json',
            beforeSend: function() {
                // loading show
				$('#loading').show();
				$('.result_bch').html('');
            },
            success: function(resp) {
                // result
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            },
            complete: function() {
                // loading hide
				$('#loading').hide();
            } 
        });
    }


    function confirmApprove(wId)
    {
        var data = {
            type: 'one', 
            action: 1,
            main_id: wId
        };
       location.href = "confirmSendBranch.php?" + $.param(data);
    }

    function confirmCancel(wId) 
    {
        $.ajax({
            url: "./ajax/ajax_getReceiveHub.php",
            type: 'POST',
            data: {
                type: 'cancel',
                wId: wId
            },
            dataType: 'json',
            success: function (resp) {
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            }
        })

    }

    function checkChoice() {

        $('.check_all').each(function (i, elm) {
            var wId = parseInt($('#' + elm.id).attr('data-value'));
            var foundArr = selectChoice.indexOf(wId);

  
            $(elm).prop('checked', !elm.checked);
    
            if (elm.checked) {
                if (foundArr == -1) {
                    selectChoice.push(wId)
                } 
            } else {
                if (foundArr != -1) {
                    selectChoice.splice(foundArr, 1);
                }
            }
        });
       
        if (selectChoice.length > 0) {
            $('.block_btn').fadeIn();
            $('#btn_checked').html('ยกเลิก');
        } else {
            $('.block_btn').fadeOut();
            $('#btn_checked').html('เลือกทั้งหมด');
        }

    }

    function selectedCard(wId, elm)
    {
        var foundArr = selectChoice.indexOf(wId);
        if($('#'+elm.id).is(':checked')) {
            if (foundArr == -1) {
                selectChoice.push(wId)
            }
        } else {
            if (foundArr != -1) {
                selectChoice.splice(foundArr, 1);
            }
        }

        if (selectChoice.length > 0) {
            $('.block_btn').fadeIn();
            $('#btn_checked').html('ยกเลิก');
        } else {
            $('.block_btn').fadeOut();
            $('#btn_checked').html('เลือกทั้งหมด');
        }
        console.log(selectChoice)
    }

    function sendAll()
    {
        var data = {
            type: 'all', 
            action: 1,
            wId: selectChoice
        };
       location.href = "confirmSendBranch.php?" + $.param(data);
    }

    function cancelAll()
    {
        $.ajax({
            url: "./ajax/ajax_getReceiveHub.php",
            type: 'POST',
            data: {
                type: 'cancel_all',
                wId: JSON.stringify(selectChoice)
            },
            dataType: 'json',
            success: function(resp) {
                $('.result_bch').empty();
                $('p span#counter_works').text(resp['counter']);
                $('.result_bch').html(resp['data']);

                selectChoice.length = 0;
                $('#btn_checked').html('เลือกทั้งหมด');
                $('.block_btn').hide();
            }
        })
    }

{% endblock %}

{% block Content %}
<div class="content_bch">
<h4 class="text-center text-muted">ส่งเอกสารที่สาขา </h4>
    <div class="header_bch">
        <div class="form-group">
            <p class="font-weight-bold text-muted">ทั้งหมด <span id="counter_works">0</span> งาน </p>
            <select name="lst_branch" id="lst_branch" class="form-control" onchange="getSendData(this.value);">
                <option value="">-- เลือกสาขา --</option>
                {% for b in branch %}
                    <option value="{{ b.mr_branch_id }}">{{ b.mr_branch_name }}</option>
                {% endfor %}
            </select>
        </div>
    </div>
    <button type="button" class="btn btn-secondary btn-block my-2" id="btn_checked" onclick="checkChoice();">เลือกทั้งหมด</button>
    <div class="block_btn my-2">
        <button type="button" id="btn_receive" class="btn btn-success btn-block" onclick="sendAll();">ส่งเอกสาร</button>
        <button type="button" id="btn_not_found" class="btn btn-warning btn-block" onclick="cancelAll();">ไม่พบผู้รับ</button>
    </div>
	<img id="loading" src="../themes/images/loading.gif">
    <div class="result_bch">
        
    </div>
</div>
<div class="footer_bch">
    
</div>
{% endblock %}


{% block debug %} 

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


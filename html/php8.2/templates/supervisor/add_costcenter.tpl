{% extends "base.tpl" %}
{% block title %}{% parent %} - List{% endblock %}
{% block menu_sp1 %} active {% endblock %}
{% block scriptImport %}
	
		<script src="../themes/bootstrap/js/jquery-11.1.js"></script>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">

{% endblock %}

{% block styleReady %}
	.fieldset {
		border: 1px solid #ccc;
		padding: 10px;
		padding-top : 0px;
	}
	td{
		padding: 3px;
	}
	input[type=text] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		//background-color:yellow;
	}input[type=number] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		//background-color:yellow;
	}
	input:focus{ 
		background-color:#ffffbf;
	}input[type=number] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		//background-color:yellow;
	}
	textarea{
		border: 1px solid #ccc;
		border-radius: 4px;
	}
	body{
	//background-color:#bebebe;
	}
	.panel{
		background-color:#eee;
		padding:5px;
		margin-bottom: 5px;
	}
	legend{
		width:15%;
	}
	
	.setClass
	{
		margin-bottom: 5px;
	}
{% endblock %}
 
{% block domReady %}
var table = $('#tb_work_order').DataTable({
		"ajax": "ajax/ajax_select_cost_center.php",
		"columns": [
            { "data": "scg_cost_center_id" },
            { "data": "scg_cost_center_code" },
            { "data": "name" },
			{ "data": "address" },
			{ "data": "districts" },
			{ "data": "province" },
			{ "data": "zip_code" },
			{ "data": "tax_id" },
			{ "data": "tel1" },
			{ "data": "tel2" },
			{ "data": "fax" },
			{
				"data": null,
				"bSortable": false,
			   "mRender": function (o) { return '<button type="button"class="btn btn-warning btn-xs glyphicon glyphicon-pencil" onclick="edit_data(' + o.scg_cost_center_id + ');">edit</button>'; }
			}
        ],
		"order": [[ 0, "asc"]],
		//"scrollY":        "150px",
      	"scrollCollapse": true
});	
	$( "#btn_savedata" ).click(function(){
		var from_data = $( '#frm_cost_center_data' ).serializeArray();
			var text_json = '{' ;
		for(var i = 0 ; i < from_data.length;i++ ){
			if(i == from_data.length-1){
				text_json+='"'+from_data[i].name+'":"'+from_data[i].value+'"}';
			}else{
				text_json+=' "'+from_data[i].name+'":"'+from_data[i].value+'",';
			}
		}
			
			if($("#cost_center_code").val()==""){
				$("#cost_center_code").after("<span class=require style='color:red;'>« จำเป็นต้องกรอก</span>");
				$("#cost_center_code + span.require").fadeOut(4000);
			}
			else if($("#name").val()==""){
				$("#name").after("<span class=require style='color:red;'>« จำเป็นต้องกรอก</span>");
				$("#name + span.require").fadeOut(4000);
			}
			else if($("#address").val()==""){
				$("#address").after("<span class=require style='color:red;'>« จำเป็นต้องกรอก</span>");
				$("#address + span.require").fadeOut(4000);
			}
			else if($("#districts").val()==""){
				$("#districts").after("<span class=require style='color:red;'>« จำเป็นต้องกรอก</span>");
				$("#districts + span.require").fadeOut(4000);
			}
			else if($("#province").val()==""){
				$("#province").after("<span class=require style='color:red;'>« จำเป็นต้องกรอก</span>");
				$("#province + span.require").fadeOut(4000);
			}
			else if($("#zip_code").val()==""){
				$("#zip_code").after("<span class=require style='color:red;'>« จำเป็นต้องกรอก</span>");
				$("#zip_code + span.require").fadeOut(4000);
			}else{
			
				$.ajax({                                                                       
					type: 'post',                                                              
					url: 'ajax/ajax_save_costcenter_data.php',                                
					data: {                                                                    
							'text_json': text_json,                                            
					},                                                                         
					//dataType: 'json',                                                        
					success: function(res){                                                                                                                           
						 //table.ajax.reload();                                                                    
						// console.log(res); 
						$("#cost_center_code").val('');
						$("#name").val(		'');
						$("#address").val(	'');
						$("#districts").val('');
						$("#province").val(	'');
						$("#zip_code").val(	'');
						$("#tax_id").val(	'');
						$("#tel1").val(		'');
						$("#tel2").val(		'');
						$("#fax").val(		'');
						table.ajax.reload();
						$("#delete_data_workorder").fadeOut( "slow");
						$("#cancel").fadeOut( "slow");
					}                                                                          
				});
			}
	});	
	
{% endblock %}
{% block javaScript %}
	function delete_data(){
		var alerts = confirm("delete data!!");
		if (alerts == true) {
			$.ajax({                                                                       
				type: 'post',                                                              
				url: 'ajax/ajax_save_costcenter_data.php',                                
				data: {                                                                    
						'scg_cost_center_id'	: $("#scg_cost_center_id").val(),                                           
						'page'					: 'remove'                                          
				},                                                                         
				//dataType: 'json',                                                        
				success: function(res){                                                                                                                           
					window.location.reload();
				}
			});
		}
	}
	function edit_data(id){
		if(id=="out"){
			window.location.reload();
		}else{	
		//console.log(id);
			$("#scg_cost_center_id").val(id);
			//$("#delete_data_workorder").fadeIn( "slow");
			$("#cancel").fadeIn( "slow");
			$.ajax({                                                                       
				type: 'post',                                                              
				url: 'ajax/ajax_save_costcenter_data.php',                                
				data: {                                                                    
						'page': 'select',                                            
						'scg_cost_center_id': id,                                            
				},                                                                         
				//dataType: 'json',                                                        
				success: function(res){  
					var obj = jQuery.parseJSON( res );		
					for(var i = 0;i< obj.length;i++ ){
						//console.log(obj);
						$("#cost_center_code").val(	obj[i]['scg_cost_center_code']);
						$("#name").val(				obj[i]['name']);
						$("#address").val(			obj[i]['address']);
						$("#districts").val(		obj[i]['districts']);
						$("#province").val(			obj[i]['province']);
						$("#zip_code").val(			obj[i]['zip_code']);
						$("#tax_id").val(			obj[i]['tax_id']);
						$("#tel1").val(				obj[i]['tel1']);
						$("#tel2").val(				obj[i]['tel2']);
						$("#fax").val(				obj[i]['fax']);
					}
					//table.ajax.reload();
				}                                                                          
			});
		}
	}
{% endblock %}
{% block Content2 %}
	<div class="row">
		<form class="form-horizontal" style="margin-top:8px;" id="frm_cost_center_data">
				<div class="col-md-12">
				<fieldset class="panel">
				<legend>Add Cost Center</legend>
				<div class="col-md-4">
					<table class="" border='0' width="100%">
					
						<tr>
							<td width="15%">
								Code
							</td>
							<td>
								<input type="hidden" id="scg_cost_center_id" name="scg_cost_center_id" class="" placeholder="scg_cost_center_id" style="width:100%;">
								<input type="text" id="cost_center_code" name="cost_center_code" class="" placeholder="cost_center_code" style="width:100%;">
								
							</td>
						</tr>
						<tr>
							<td>
								Name
							</td>
							<td>
								<input type="text" id="name" name="name" class="" placeholder="name" style="width:100%;">
							</td>
						</tr>
						<tr>
							<td>
									Address
							</td>
							<td>
								<textarea id="address" name="address"rows="2" style="width:100%;"></textarea>
							</td>
						</tr>
						<tr>
							<td>
								Districts 
							</td>
							<td>
								<input type="text" id="districts" name="districts" class="" placeholder="districts" style="width:100%;">
							</td>
						</tr>
						<tr>
							<td>
								Province
							</td>
							<td>
								<input type="text" id="province" name="province" class="" placeholder="province" style="width:100%;">
							</td>
						</tr>
					</table>
				</div>
				<div class="col-md-4">
					<table class="" border='0' width="100%">
						<tr>
							<td width="15%">
								PostCode
							</td>
							<td>
								<input type="text" id="zip_code" name="zip_code" class=""   placeholder="post_code" style="width:100%;">
							</td>
						</tr>
						<tr>
							<td>
								Tax_id
							</td>
							<td>
								<input type="text" id="tax_id" name="tax_id" class=""   placeholder="tax_id" style="width:100%;">
							</td>
						</tr>
						<tr>
							<td>
								Tel1
							</td>
							<td>
								<input type="text" id="tel1" name="tel1" class="" placeholder="tel1" style="width:100%;">
							</td>
						</tr>
						<tr>
							<td>
								Tel2
							</td>
							<td>
								<input type="text" id="tel2" name="tel2" class="" placeholder="tel2" style="width:100%;">
							</td>
						</tr>
						<tr>
							<td>
								Fax
							</td>
							<td>
								<input type="text" id="fax" name="fax" class="" placeholder="fax" style="width:100%;">
							</td>
						</tr>
					</table>
				</div>
				<div class="setClass col-md-12" align="right">
						<button type="button" class="btn btn-primary" id="btn_savedata" name="btn_savedata">Save</button>
						<button type="button" class="btn btn-danger " id="delete_data_workorder" onclick="delete_data();" name="delete_data_workorder" style="display: none;">Delete</button>
						<button type="button" class="btn btn-default " id="cancel" name="cancel" onclick="edit_data('out');" style="display: none;">Refresh</button>	
					</div>
				</fieldset>
				
				</div>	
					<div class="col-md-12" id="div_table">
						<fieldset class="fieldset panel">
							<legend></legend>
								<table class="table table-striped table-bordered" id="tb_work_order" cellspacing="0" >
									<thead>
										<tr>
											<th class="text-center">#</th>
											<th	class="text-center">Code		</th>
											<th	class="text-center" width="20%">Name				</th>
											<th	class="text-center">Address				</th>
											<th	class="text-center">Districts			</th>
											<th	class="text-center">Province			</th>
											<th	class="text-center">ZipCode				</th>
											<th	class="text-center">Tax					</th>
											<th	class="text-center">Tel1				</th>
											<th	class="text-center">Tel2				</th>
											<th	class="text-center">Fax					</th>
											<th	class="text-center">edit</th>
											
											
										</tr>
									</thead>
									<tbody id="tbody_work_order" >
									
									</tbody>
								</table>
						</fieldset>
					</div>
				<!-- </div> -->
			
		</form>
	</div>
	
	
	
{% endblock %}

{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}

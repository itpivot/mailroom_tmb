{% extends "base.tpl" %}

{% block title %}{% parent %} - Building Data{% endblock %}

{% block menu_sp1 %} active {% endblock %}
{% block scriptImport %}
	
		<script src="../themes/bootstrap/js/jquery-11.1.js"></script>
		<script src="../themes/bootstrap/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="css/chosen.css">
		<link rel="stylesheet" href="../themes/bootstrap/css/jquery.dataTables.css">
		<script src="js/chosen.jquery.js" charset="utf-8"></script>

{% endblock %}
{% block styleReady %}
	#content {
		margin-top: 10px
	}

	.fieldset {
		border: 1px solid #ccc;
		padding: 10px;
		padding-top : 0px;
	}
	td{
		padding: 3px;
	}
	input[type=text] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		text-align: center;
		width: 300px;
	}input[type=number] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
		//background-color:yellow;
	}
	input:focus{ 
		background-color:#ffffbf;
	}
	input[type=number] {
		border: 1px solid #ccc;
		border-radius: 4px;
		padding-left :5px;
	}
	textarea{
		border: 1px solid #ccc;
		border-radius: 4px;
	}
	.panel{
		background-color:#eee;
		padding:5px;
		margin-bottom: 5px;
	}
	legend{
		width:15%;
	}
	
	.setClass
	{
		margin-bottom: 5px;
	}

	.label_txt{
		width: 100px;
		text-align: center;
	}

	table.dataTable tbody tr {
				cursor: pointer;
	}

	table.dataTable tbody tr.selected {
		background-color: #555;
		color: #fff;
		cursor:pointer;
	}

	.table-hover tbody tr:hover > td {
        background-color: #555;
        color: #fff;
     }


{% endblock %}

{% block domReady %}
	var bsave = $('#btn_save');
	var bdelete = $('#btn_delete');
	var code = $("#txt_code");
	
	$('#btn_delete').hide();

	$('#lst_type').chosen({width: "300px"});
	var tbuilding = $('#tb_building_show').DataTable({
		ajax: {
			url: "ajax/ajax_load_building_data.php",
			type: "POST"
		},
		columns: [
			{ 'data': null,
				'defaultContent': "",
				'searchable': false,
				'orderable': false
			},
			{ "data": "scg_building_office_id" },
			{ "data": "scg_building_code" },
			{ "data": "scg_building_name"},
			{ "data": "scg_building_type"}
		],
		order: [[ 1, "desc"]]
	});

	$('#tb_building_show tbody').on("click","tr",function() {
			if ( $(this).hasClass('selected') ) {
				  $('#btn_delete').fadeOut();
    			  $(this).removeClass('selected');
    	  	}
      	  	else {
      	  		$('#btn_delete').fadeIn();
                tbuilding.$('tr.selected').removeClass('selected');
          		$(this).addClass('selected');
        	}
	});
	
	$('#tb_building_show tbody').on("click","tr",function() { 

		var rows = tbuilding.row(this).data();
		var b_id = rows['scg_building_office_id'];
		$.ajax({
			url: "ajax/ajax_get_building_edit.php",
			type: "POST",
			data: {
				bid: b_id 
			},
			dataType: 'json',
			success: function(res){
				console.log(res);
				if(res.length > 0){
					$("#txt_code").val(res[0]['scg_building_code']);
				    $("#txt_name").val(res[0]['scg_building_name']);
					$("#lst_type").val(parseInt(res[0]['scg_building_type'])).trigger("chosen:updated");
					$('#old_id').val(res[0]['scg_building_office_id']);
				}
			}
		});
		
	});
	
	code.keyup(function(){
		<!-- console.log($(this).val()); -->
		var length_txt = $(this).val().length;
		if( length_txt < 2){
			$('#alert_code').html('ใช้2-3ตัวอักษร').css('color','red');
		}else{
			$.ajax({
				url: "ajax/ajax_check_duplicate_code_building.php",
				type: "POST",
				data: {
					bcode: $(this).val()
				},
				dataType: 'json',
				success: function(res){
					if(parseInt(res[0]['bcode']) > 0){
						$('#alert_code').html('code ซ้ำ').css('color','red');
					}else{
						$('#alert_code').html('สามารถใช้ code นี้ได้').css('color','green');
					}
				}
			});
		}
		
	});

	tbuilding.on( 'order.dt search.dt', function () {
		tbuilding.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
			cell.innerHTML = i+1;
		} );
	} ).draw();

	bsave.click(function(){
		var chkForm = Validated();
		var code = $("#txt_code").val();
		var name = $("#txt_name").val();
		var btype = $("#lst_type").val();
		var oid = $('#old_id').val();
		if(chkForm == true){
			var oid = $('#old_id').val();
			if(oid != ""){
				$.ajax({
					url: "ajax/ajax_save_building_data.php",
					type: "POST",
					data: {
						oid: oid,
						code: code,
						name: name,
						btype: btype
					},
					success: function(res){
						if(res == "Update Success"){
							ClearFrom();
							tbuilding.ajax.reload();
						}
					}
				});
			}else{
				$.ajax({
					url: "ajax/ajax_save_building_data.php",
					type: "POST",
					data: {
						oid: oid,
						code: code,
						name: name,
						btype: btype
					},
					success: function(res){
						if(res == "Save Success"){
							ClearFrom();
							tbuilding.ajax.reload();
						}
					}
				});
			}
		}
	});

	bdelete.click(function() {
		var oid = $('#old_id').val();
		if(confirm("ท่านต้องการลบรายการนี้หรือไม่")){
			$.ajax({
					url: "ajax/ajax_delete_building_data.php",
					type: "POST",
					data: {
							oid: oid
						},
					success: function(res){
							if(res == "Delete Success"){
								ClearFrom();
								tbuilding.ajax.reload();
							}
						}
			});
		}
		
	});
{% endblock %}

{% block javaScript %}
	function Validated(){
		var code = $("#txt_code").val();
		var name = $("#txt_name").val();
		var btype = $("#lst_type").val();
		var status = true;
		
		if(code == "" || code == null){
			$('#alert_code').html('จำเป็นต้องกรอก').css('color','red');
			status = false;
		}else{
			$('#alert_code').html('')
			status = true;
		}

		if(name == "" || name == null){
			$('#alert_name').html('จำเป็นต้องกรอก').css('color','red');
			status = false;
		}else{
			$('#alert_name').html('')
			status = true;
		}

		if(btype == "" || btype == null){
			$('#alert_type').html('จำเป็นต้องกรอก').css('color','red');
			status = false;
		}else{
			$('#alert_type').html('')
			status = true;
		}

		return status;
	}

	function ClearFrom(){
		$('#old_id').val("");
		$("#txt_code").val("");
		$("#txt_name").val("");
		$("#lst_type").val(0).trigger("chosen:updated");
	}
{% endblock %}

{% block Content %}
	<div class="container" id="content">
		<div class="row">
			<form action="" class="form-horizontal" id="frm_building" >
				<div class="col-md-12">
					<fieldset class="fieldset panel">
						<legend><b>Building</b></legend>
						 <div class="col-md-6 col-md-offset-3">
							<table id="tb_frm_building" >
								<input type="hidden" id="old_id">
								<tr>
									<td class="label_txt"><b>Code</b></td>
									<td>
										<input type="text" id="txt_code" name="txt_code" placeholder="Code" maxlength="3">
										<span id="alert_code"></span>
									</td>
								</tr>
								<tr>
									<td class="label_txt"><b>Name</b></td>
									<td>
										<input type="text" id="txt_name" name="txt_name" placeholder="Building Name" >
										<span id="alert_name"></span>
									</td>
								</tr>
								<tr>
									<td class="label_txt"><b>Type</b></td>
									<td>
										<select name="lst_type" id="lst_type">
											 <option value="" disabled selected>Choose Building Type</option>
											 <option value="1">01 : Building</option>
											 <option value="2">02 : Route</option>
										</select>
										<span id="alert_type"></span>
									</td>
								</tr>
								<tr>
									<td class="label_txt"></td>
									<td>
										<button type="button" id="btn_save" name="btn_save" class="btn btn-primary">Save</button>
										<button type="button" id="btn_delete" name="btn_delete" class="btn btn-danger">Delete</button>
										<button type="button" class="btn btn-default" onClick='window.location.reload();'>Clear</button>
									</td>
								</tr>
							</table>
						 </div>
					<fieldset>
				</div>
			</form>

			<div class="col-md-12">
				<fieldset class="fieldset panel">
					<table class="table table-striped table-bordered" id="tb_building_show">
						<thead>
							<tr>
								<th>#</th>
								<th>ID</th>
								<th>Code</th>
								<th>Building Name</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
{% endblock %}
{% block debug %}

	{% if debug != '' %}
		<pre>{{ debug }}</pre>
	{% endif %}

{% endblock %}


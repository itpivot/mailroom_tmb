{% extends "base_login.tpl" %}

{% block title %}{% parent %} - Authentication {% endblock %} 

{% block styleReady %}
 .form-control {
    background-color: #f3f4f9;
    font-weight: bold;
 }

input[type='text'],
input[type='password'] {
     border-radius: 0px;
 }

 .btn {
     border-radius: 0px;
 }

#loading {
    position: absolute;
    margin: 0;
    padding: 0;
    top: 0px;
    left: 0px;
    //background-image: url("../themes/images/spinner.gif");
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center;
    width: 100%;
    height: 100%;
    z-index: 1000;
    background-color: rgba(255, 255, 255, 0.8);

}
#img { 
    position: absolute;
 display: block;
  margin-left: auto;
  margin-right: auto;
  margin-top: auto;
  top : 50%;
  left :50%;
  margin-right: -50%;
 transform: translate(-50%, -50%)
}




 @media only screen and (min-width:769px) {
    input[type='text'],
    input[type='password'] {
        width: 250px;
    }

    #frm_register,
    #frm_password {
        margin-left: 25%;
     }
}


{% endblock %}

{% block domReady %}
$('#loading').hide();

$('#btn_search').on('click', function () {
    var empCode = $('#emp_id').val();
    var fname = $('#txt_firstName').val();
    var lname = $('#txt_lastName').val();
    var radio = $("input[name=radio]:checked").val();
	if(empCode == '' || empCode == null){
		 alertify.alert('alert','กรุณ่กรอกข้อมูลรหัสพนักงาน');
		 $('#emp_id').focus();
		 return;
	}
	
	  
	  
//alert(radio);
//return;
    $.ajax({
        url: './ajax/ajax_search_employee.php',
        method: 'POST',
        data: {
                radio 	: radio,
                empCode : empCode,
                fname   : fname,
                lname   : lname
        },
        beforeSend: function() {
            $("#loading").show();
        },
		
        success: function(res) {
            console.log(res);
            $('#loading').hide();
            var err = [];
            err[0] = '<b>ไม่มีข้อมูลพนักงาน กรุณาลองใหม่อีกครั้ง !</b>';
            err[1] = '<b>ท่านมีข้อมูลการเข้าใช้งานในระบบแล้ว</b>';
            err[2] = '<b>สำเร็จ</b>';
            err[3] = '<b>การส่งอีเมลผิดพลาดกรุณาติดต่อเจ้าหน้าที่</b>';
            err[4] = '<b>กรุณาเลือกสถานที่ปฎิบัติงาน</b>';
            if(parseInt(res) == 3) {
                $('#emp_id').prop('readonly',true);
                alertify.alert('ลงทะเบียนสำเร็จ',"การลงทะเบียนของท่านสำเร็จแล้ว กรุณาตรวจสอบข้อมูลการใช้งานที่ e-mail ค่ะ",function() {
                    window.location.href = 'login.php'; 
                });
            } else {
                alertify.alert('เกิดข้อผิดพลาด',err[res - 1]);
                document.getElementById("frm_register").reset();
                $('#emp_id').focus();
            }
        
        }
    });
       
});


$("#emp_id").keyup(function(){
			var pass_emp = $("#emp_id").val();
			$.ajax({
				url: "ajax/ajax_autocompress_regis.php",
				type: "post",
				data: {
					'pass_emp': pass_emp,
				},
				dataType: 'json',
				success: function(res){
					if(res != false) {
							$("#txt_firstName").val(res['mr_emp_name']);
							$("#txt_lastName").val(res['mr_emp_lastname']);
					}  else {
						$("#txt_firstName").val("");
						$("#txt_lastName").val("");
					}	
				}
												
			});
									
		});


{% endblock %}

{% block javascript %}{% endblock %}
   
{% block body %}
<div id='loading'>
<image id="img" src="../themes/images/spinner.gif" width="50px">
</div>
<div class='panel panel-default'>
    <div class='panel-body'>
        <center><h3>สมัครสมาชิก</h3></center>
        <hr>
                <form id='frm_register' class='form-horizontal'>
                    <div class='form-group'>
                    <label class='col-sm-2 control-label'>รหัสพนักงาน<span style='color:red; font-size: 15px'>&#42;</span> : &nbsp;&nbsp;</label>
                        <div class='col-sm-10'>
                            <input type='text' class='form-control' id='emp_id' autofocus>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>&nbsp;&nbsp;  ชื่อ<span style='color:red; font-size: 15px'>&#42;</span> : &nbsp;&nbsp;</label>
                        <div class='col-sm-10'>
                            <input type='text' class='form-control' id='txt_firstName' disabled>   
                        </div>
                    </div>
                    <div class='form-group'>
                            <label class='col-sm-2 control-label'>&nbsp;&nbsp;  นามสกุล<span style='color:red; font-size: 15px'>&#42;</span> : &nbsp;&nbsp;</label>
                        <div class='col-sm-10'>
                            <input type='text' class='form-control' id='txt_lastName' disabled> 
                        </div>
                    </div>
					<hr>
					<div class='form-group'>
                            <label class='col-sm-3 control-label'>  สถานที่ปฎิบัติงาน<span style='color:red; font-size: 15px'>&#42;</span> : </label>
                        <div class='col-sm-9'>
                            <input id='radio1' name ='radio' value='2' type='radio' ><label  for="radio1">&nbsp;  สำนักงานใหญ่(พหลโยธิน)</label>  &nbsp;&nbsp;
                            <input id='radio2' name ='radio' value='5' type='radio' ><label  for="radio2">&nbsp;  สาขาและอาคารอื่นๆ</label>
                        </div>
                    </div>
					
					
                    
                       
                 
                </form>   
                
                 <div class="col-sm-12 text-center">
                            <button type='button' class='btn btn-default' id='btn_search'><b>&nbsp;&nbsp;ยืนยัน&nbsp;&nbsp;</b></button>
                        </div>
    </div>
</div>
 

{% endblock %}
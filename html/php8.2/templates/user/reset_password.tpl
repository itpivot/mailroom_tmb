{% extends "base_login.tpl" %}

{% block title %}{% parent %} - Authentication {% endblock %} 

{% block styleReady %}
 .form-control {
    background-color: #f3f4f9;
    font-weight: bold;
 }

input[type='text'],
input[type='password'] {
     border-radius: 0px;
 }

 .btn {
     border-radius: 0px;
 }

#loading {
    position: absolute;
    margin: 0;
    padding: 0;
    top: 0px;
    left: 0px;
    background-image: url("../themes/images/spinner.gif");
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center;
    width: 100%;
    height: 100%;
    z-index: 1000;
    background-color: rgba(255, 255, 255, 0.8);

}



 @media only screen and (min-width:769px) {
    input[type='text'],
    input[type='password'] {
        width: 250px;
    }

    #frm_register,
    #frm_password {
        margin-left: 25%;
     }
}


{% endblock %}

{% block domReady %}
$('#loading').hide();
{% endblock %}

{% block javascript %}{% endblock %}
   
{% block body %}
<div id='loading'></div>
<div class='panel panel-default'>
    <div class='panel-body'>
        <h3>สมัครสมาชิก</h3>
        <hr>
                <form id='frm_register' class='form-horizontal'>
                    <div class='form-group'>
                    <label class='col-sm-2 control-label'>รหัสพนักงาน<span style='color:red; font-size: 15px'>&#42;</span> : &nbsp;&nbsp;</label>
                        <div class='col-sm-10'>
                            <input type='text' class='form-control' id='emp_id' autofocus>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-2 control-label'>&nbsp;&nbsp;  ชื่อ<span style='color:red; font-size: 15px'>&#42;</span> : &nbsp;&nbsp;</label>
                        <div class='col-sm-10'>
                            <input type='text' class='form-control' id='txt_firstName'>   
                        </div>
                    </div>
                    <div class='form-group'>
                            <label class='col-sm-2 control-label'>&nbsp;&nbsp;  นามสกุล<span style='color:red; font-size: 15px'>&#42;</span> : &nbsp;&nbsp;</label>
                        <div class='col-sm-10'>
                            <input type='text' class='form-control' id='txt_lastName'> 
                        </div>
                    </div>
                    <div class='form-group'>
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type='button' class='btn btn-default' id='btn_search'><b>ยืนยัน</b></button>
                        </div>
                    </div>    
                </form>   
                
                
    </div>
</div>
 

{% endblock %}
<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/Employees.php';
require_once 'Dao/UserRole.php';
error_reporting(E_ALL & ~E_NOTICE);
/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Atapy_Site::toLoginPage();
}

$req = new Pivot_Request();

/*
$isMyself = ($req->get('myself') == 1) ? true : false;

if ($isMyself) {

    $id = $auth->getUser();

} else {
    
    if (!$auth->hasAccess(array(
        Dao_UserRole::role('admin'),
        Dao_UserRole::role('project_manager')
        ))) {
        Pivot_Site::toDefaultPage();
    }

    $req = new Pivot_Request();
    $id = $req->get('id');
}
*/

$isEdit = false;
$changePassword = false;
$caption = 'add';
if (is_numeric($id)) {
    $caption = 'edit';
    $isEdit = true;
}

$employeesDao = new Dao_Employees();
$userDao = new Dao_User();
$errors = array();


/* Post back */
if ($req->get('action') == 'save') {
    $role_id = $req->get('role_id');

    /* Require field */
    $reqFields = array(
        'username' => 'ชื่อผู้ใช้',
        'name' => 'ชื่อ(ไทย)',
        'phone' => 'เบอร์โทรศัพท์',
        'email' => 'Email',
        'role_id' => 'หน้าที่'
    );
    /* Require password always when create */
    if (!$isEdit) {
        $reqFields['password'] = 'รหัสผ่าน';
    }
    
    foreach ($reqFields as $f => $m) {
        if ($req->get($f) == '') {
            $errors[$f] = 'โปรดระบุ' .  $m;
        }
    }
 
    /* Validate password and confirm password is match */
    if ($req->get('password') != $req->get('confirm_password')) {
        $errors['password'] = 'รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน';
    }

    $fields = array('username', 'role_id', 'password');
    foreach ($fields as $u) {
        $user[$u] = $req->get($u);
    }

    $fields = array('name', 'phone', 'email', 'role_id');	
    foreach ($fields as $u) {
        $employees[$u] = $req->get($u);
    }

    if (count($errors) == 0) {
        /* Validate user is not exists */
        if ($userDao->isExistsUser($user['username'], $id)) {
            $errors['username'] = 'ชื่อ '. $user['username'] . ' มีผู้ใช้แล้ว';
        }
    }

    if (count($errors) == 0) {
        $id = $userDao->save($user, $id);
        $user['id'] = $id;
        $employees['id'] = $id;
        $employeesDao->save($employees, $id);
        header ("Location: list.php");
        $message = 'success';
    } else {
        $user['id'] = $id;
    }
        
} elseif (is_numeric($id)) {
    $user = $userDao->get($id);
    $employees = $employeesDao->get($id);
    
} else {
    /* Project manager can create only project coordinator in the same department */
    if ($auth->getRole() == Dao_UserRole::role('project_manager')) {
        $user['role_id'] = Dao_UserRole::role('project_coordinator');
    
        $managerProfile = $employessDao->get( $auth->getUser() );
        $employees['role_id'] = $managerProfile['role_id'];
    }
}
     

$userRoleDao = new Dao_UserRole();
$userRoles = $userRoleDao->fetchAll();

$template = Pivot_Template::factory('user/edit.tpl');
$template->display(array(
    'caption' => $caption,
    'errors' => $errors,
    'isMyself' => $isMyself,
    'isEdit' => $isEdit,
    'user' => $user,
    'userRoles' => $userRoles,
    'employees' => $employees,
    'error' => print_r($errors, true),
    'message' => $message,
    'debug' => $debug,
    'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles()
));

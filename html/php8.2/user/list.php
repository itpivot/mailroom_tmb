<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Dao.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
//require_once 'Dao/Employees.php';
require_once 'Dao/UserRole.php';
error_reporting(E_ALL & ~E_NOTICE);
/* Check authentication */
$auth = new Pivot_Auth();
if (!$auth->isAuth()) {
    Pivot_Site::toLoginPage();
}

/* Check authorization 
if (!$auth->hasAccess(array(
    Dao_UserRole::role('admin'),
		Dao_UserRole::role('project_manager')
    ))) {
    Atapy_Site::toDefaultPage();
}
*/
$req = new Pivot_Request();
$userDao = new Dao_User();
$userRoleDao = new Dao_UserRole();
//$employeesDao = new Dao_Employees();

/* Post back */
if ($req->get('action') == 'delete') {
    $req = new Pivot_Request();
    $id = $req->get('id');
    $userDao->remove($id);
}

$users = $userDao->fetchAll();
//$employees = $employeesDao->fetchAll();

$userRoleDao = new Pivot_Dao('user_role');
//$userRoles = $userRoleDao->fetchAll();

$template = Pivot_Template::factory('user/list.tpl');
$template->display(array(
	//'employees' => $employees,
	'userRoles' => $userRoles,
	'users' => $users,
	'role_id' => $auth->getRole(),
	'roles' => Dao_UserRole::getAllRoles()
));
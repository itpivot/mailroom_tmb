<?php
require_once '../prepend.php';
require_once 'Pivot/Auth.php';
require_once 'Pivot/Request.php';
require_once 'Pivot/Site.php';
require_once 'Pivot/Template.php';
require_once 'Dao/User.php';
require_once 'Dao/UserRole.php';
error_reporting(E_ALL & ~E_NOTICE);
$auth 	= new Pivot_Auth();
$req 	= new Pivot_Request();

$userRole_Dao 	= new Dao_UserRole();

$template = Pivot_Template::factory('user/reset_password.tpl');
$template->display(array(
    'user' => $user,
    'userRoles' => $userRoles,
    'userProfile' => $userProfile,
    'departments' => $departments,
    'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles()
));

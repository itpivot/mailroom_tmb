<?php
require_once '../prepend.php';
require_once 'Atapy/Auth.php';
require_once 'Atapy/Request.php';
require_once 'Atapy/Site.php';
require_once 'Atapy/Template.php';
require_once 'Dao/Department.php';
require_once 'Dao/User.php';
require_once 'Dao/UserProfile.php';
require_once 'Dao/UserRole.php';
error_reporting(E_ALL & ~E_NOTICE);
/* Check authentication */
$auth = new Atapy_Auth();
if (!$auth->isAuth()) {
    Atapy_Site::toLoginPage();
}

/* Check authorization */
if (!$auth->hasAccess(array(
        Dao_UserRole::role('admin'),
        Dao_UserRole::role('account_executive'),
        Dao_UserRole::role('project_manager'),
        Dao_UserRole::role('project_coordinator')
    ))) {
    Atapy_Site::toDefaultPage();
}

$req = new Atapy_Request();
$id = $req->get('id');

$userProfileDao = new Dao_UserProfile();
$userDao = new Dao_User();
$errors = array();

$userProfile = $userProfileDao->get($id);
$user = $userDao->get($id);

$departmentDao = new Dao_Department();
$departments = $departmentDao->fetchAll();

$userRoleDao = new Dao_UserRole();
$userRoles = $userRoleDao->fetchAll(); /* override role */

$template = Atapy_Template::factory('user/view.tpl');
$template->display(array(
    'user' => $user,
    'userRoles' => $userRoles,
    'userProfile' => $userProfile,
    'departments' => $departments,
    'role_id' => $auth->getRole(),
    'roles' => Dao_UserRole::getAllRoles()
));
